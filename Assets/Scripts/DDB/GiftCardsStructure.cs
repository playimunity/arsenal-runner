using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;
using Facebook.Unity;

[DynamoDBTable("ars_bd_giftcards")]
public class GiftCardsStructure{

	[DynamoDBHashKey] 
	public string RecipientId;

	[DynamoDBProperty("senders")]
	public List<string> SendersIds;

	public GiftCardsStructure(){
		SendersIds = new List<string> ();
	}

}

