﻿using UnityEngine;
using System.Collections;

public class TutorialPoint : Collectable {


	bool triggered = false;

	//int tutorialPoint = 0;

	//WaitForFixedUpdate waitFixedUpdate = new WaitForFixedUpdate();


	void Start () {
		if (Tutorial.Instance.tutRetryed) {
			gameObject.SetActive (false);
		}
		type = CollectableTypes.TUT;
	}
	


	void OnTriggerEnter(Collider col){
		if (Tutorial.Instance.tutRetryed)	return;

		if (!triggered && col.transform.gameObject.tag == "Player") {
			triggered = true;
			Tutorial.Instance.PlayTutAnim ();
			GameManager.Instance.TutorialPointPause();

			//GameManager.RequestPause ();
			//StartCoroutine (ActivateTutorialPoint());
			//tutorialPoint++;
		}
	}



//	IEnumerator ActivateTutorialPoint(){
//		yield return waitFixedUpdate;
//		Tutorial.Instance.tutorialPointOn = true;
//	}



}
