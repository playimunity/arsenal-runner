﻿using UnityEngine;
using System.Collections;

public class EnemyRun : MonoBehaviour {

	Obstacle.ObstacleTypes type = Obstacle.ObstacleTypes.Enemy_Run;

	public Animator animator;

	public bool moveTriggered = false;
	bool colided = false;
	bool triggered = false;

	float ballRotationAngle = 15f;
	float moveSpeed = 6;

	public GameObject ball;

	Transform BallTransfrom;

	void Start(){
		BallTransfrom = ball.transform; 
	}

	void Update () {
		if (moveTriggered && !GameManager.TutorialPrePaused && !PlayerController.instance.isDead) {
			transform.Translate( Vector3.back * moveSpeed * Time.deltaTime );
			BallTransfrom.RotateAround (-transform.right , ballRotationAngle * Time.deltaTime);
		}

		if (GameManager.TutorialPrePaused && animator.speed > 0f) {
			animator.speed = Mathf.Lerp(animator.speed, 0f, 10f * Time.deltaTime);
		}else if(!GameManager.TutorialPrePaused && animator.speed < 1f){
			animator.speed = Mathf.Lerp(animator.speed, 1f, 20f * Time.deltaTime);
		}
	}

	
	void OnTriggerEnter(Collider col){
		if (triggered)
			return;

		if (col.gameObject.tag == "Player") {
			triggered = true;
			moveTriggered = true;
			animator.SetTrigger("run");
		}
	}


//	public void StartRun(){
//		moveTriggered = true;
//		animator.SetTrigger("run");
//	}



	void OnCollisionEnter(Collision col) {
		if (colided)
			return;

		if (col.gameObject.tag == "Player" && !colided) {
			colided = true;
			GetComponent<Collider> ().enabled = false;
			GetComponent<Rigidbody> ().isKinematic = true;
			moveTriggered = false;
			if (PlayerController.instance.sliding) {
				ball.SetActive(false);
				PlayerController.instance.CollectBall ();
				animator.SetTrigger("fall front");
				RunManager.OnTackleEnemy();
			} else {
				animator.SetTrigger("fall back");
				PlayerController.instance.hardDamage = true;
				PlayerController.instance.Damage(10);
			}

		}

	}




}
