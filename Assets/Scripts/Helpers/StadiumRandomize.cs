﻿using UnityEngine;
using System.Collections;

public class StadiumRandomize : MonoBehaviour {

	public GameObject[] leftSideFuild;
	public GameObject[] rightSideFuild;

	int leftRandonNum;
	int rightRandonNum;


	void Start () {
		leftRandonNum = Random.Range (0, 2);
		rightRandonNum = Random.Range (0, 2);

		leftSideFuild [leftRandonNum].SetActive (true);
		rightSideFuild [rightRandonNum].SetActive (true);
	}
	

}
