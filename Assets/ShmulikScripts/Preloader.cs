using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.IO;
using Amazon;
public class Preloader : MonoBehaviour {

	// Use this for initialization
	public static Preloader Instance;
	public Animator preloadAnimation;
	public GameObject LoadingBar;
	public RectTransform fillBar;
	public RectTransform fillLimitUI;
	public float gap;
	public float fillLimit;
	float fillStep;
	float fillBarwidth;
	public Text debugText;
	public Text zivDebug;
	float lastTime;
	bool flag;
	float timer = 0;
	public const string NOTIFICATION_ALLOWED = "NotificationsAllowed";


	void Start () {
		Instance = this;
//		if (!UniAndroidPermission.IsPermitted (AndroidPermission.READ_PHONE_STATE))
//			UniAndroidPermission.RequestPremission (AndroidPermission.READ_PHONE_STATE, () => {
//    			// add permit action
//    			print ("Permission true");
//			}, () => {
//    			// add not permit action
//				print ("Permission false");
//			});
		#if UNITY_IOS &&!UNITY_ANDROID
		UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Badge|UnityEngine.iOS.NotificationType.Sound);
		PlayerPrefs.SetInt (NOTIFICATION_ALLOWED, 1);
		#endif
		GameAnalyticsWrapper.DesignEvent (GameAnalyticsWrapper.FLOW_STEP.LOADING);
		fillLimit = fillLimitUI.rect.height;
		//fillStep = fillLimit / PlayersAtLoading.Count;
		fillStep = fillLimit / 9f;
		FillLoadingBar (1, "Start");
		//StartCoroutine (WaitToLoad ());
		fillBarwidth = fillBar.sizeDelta.x;
	}
		
	public Dictionary<int,string> PlayersAtLoading = new Dictionary<int,string> ()
	{
		{1,"Warming Up..."},
		{3,"Ramsey"},
		{4,"Walcott"},
		{5, "Alexis"},
		{6, "Giroud"},
		{7, "Cech"},
		{8, "Ozil"},
		{9, "Koscielny"},
		{10, "Lets Go Gunners!"},
        {11, "Enjoy!"}

	};
		
	IEnumerator WaitToLoad()
	{
		preloadAnimation.Play("scene_transition_glow");
		//yield return new WaitForSeconds (0.f);
		yield return new WaitUntil(()=> Instantiate(Resources.Load("GameContainer")));
		//FillLoadingBar (2);
		Destroy (preloadAnimation.transform.parent.gameObject);
		//yield return new WaitUntil(()=> Instantiate(Resources.Load("CrossScene UI")));
	}

	void Update () {
		if (GameManager.Instance && !flag) {
			flag = true;
			Preloader.Instance.FillLoadingBar (3, "ManagersLoaded");
		}
	}

	public void FillLoadingBar(int num, string phase)
	{
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("FillLoadingBar:"+num.ToString()+" "+PlayersAtLoading[num]);
		//Debug.Log("RON_______FillLoadingBar:"+num.ToString()+" "+PlayersAtLoading[num]+" Time:"+ DateTime.Now.ToLongTimeString());
		lastTime = Time.time - lastTime;
		//debugText.text = phase + ", " + lastTime;
		debugText.text = PlayersAtLoading[num]+"...";
		gap = timer;
	//	fillBar.sizeDelta = new Vector2 (fillBarwidth , ((fillLimit/fillBar.transform.localScale.y)/10)/num);
	//	fillBar.sizeDelta = new Vector2 (fillBarwidth , (fillLimit/num));
		//fillBar.sizeDelta = new Vector2 (fillBarwidth , (fillLimit - (fillStep*num)));
		//Debug.Log ("alon_______ fillStep: " + fillStep);
	}

//	void OnApplicationFocus(bool focusStatus) {
//		//Debug.Log("RON________________OnApplicationFocus start:"+focusStatus.ToString()+" Time:"+ DateTime.Now.ToLongTimeString());
//		if (!focusStatus)
//		{
//			if (GameManager.Instance == null || (GameManager.Instance != null && GameManager.curentGameState == GameManager.GameState.PRELOADER ))
//				UnityInitializer.Instance.saveDataToPHP ();
//			//DDB._.ReportErrorLog ();
//		}
//		//Debug.Log("RON________________OnApplicationFocus end:"+focusStatus.ToString()+" Time:"+ DateTime.Now.ToLongTimeString());
//	}

	public void PrintStatus(string message, bool isAdditive = true)
	{
		if (isAdditive)
			zivDebug.text +="\r\n*"+ message;
		else
			zivDebug.text = message;
	}
}
