using UnityEngine;
using System.Collections;
using Area730.Notifications;
using System;
using System.Collections.Generic;
using Amazon;

public class LocalNotifications : MonoBehaviour {

	public static Dictionary<string, DateTime> timers;
	static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log ("Local Notifications | " + msg);
	}

	void Awake(){
		timers = new Dictionary<string, DateTime> ();
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalNotifications awake start");
		ClearNotifications();
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalNotifications awake end");
	}


	void OnApplicationFocus(bool focusStatus) {
		if (!focusStatus) {
			OnFocusOut ();
		} else {
			OnFocusOn ();
		}
	}

	void OnFocusOn(){
		ClearNotifications();
	}

	void OnFocusOut(){
		ClearNotifications();
		SheduleAllNotifications ();
		print("FOCUS OUT");
	}

	void ClearNotifications(){
		#if UNITY_IOS
		//UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
		//if (UnityEngine.iOS.NotificationServices.localNotificationCount> 0)
		print ("***********NOTIFICATIONS COUNT Before Canceling******: "+UnityEngine.iOS.NotificationServices.scheduledLocalNotifications.Length);
		if (UnityEngine.iOS.NotificationServices.scheduledLocalNotifications.Length>0)
		{
		UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications ();
		print ("***********iOS NOTIFICATIONS COUNT After Canceling******: "+UnityEngine.iOS.NotificationServices.scheduledLocalNotifications.Length);
		}
		#elif UNITY_ANDROID
		AndroidNotifications.cancelAll();
		#endif

	}

	public static void SheduleNotification(int id, string msg, DateTime date){

		#if UNITY_ANDROID// && !UNITY_EDITOR

		NotificationBuilder builder = new NotificationBuilder(id, "AFC - Endless Football", msg);
		builder.setDelay( date - DateTime.Now);
        builder.setTicker ("AFC - Endless Football");
		//builder.setSmallIcon("fcb_icon").setLargeIcon("fcb_icon");
		builder.setLargeIcon("fcb_icon");
		builder.setSound("levelup");

		AndroidNotifications.scheduleNotification(builder.build());

		#elif UNITY_IOS //&& !UNITY_EDITOR

		UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();
		notif.fireDate = date;
		notif.alertBody = msg;
		notif.soundName = "levelup";
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (notif);

		#endif
		Log ("Notification Sheduled! ## Message: " + msg + " ## To date: " + date.ToString ());

	}

	// ----------------------


	PlayerForNotification GetPlayerForNotification(){
		string[] plData; 

		string[] selectedPData = DAO.GetPlayerData (0).Split (new char[1]{ '|' }, StringSplitOptions.RemoveEmptyEntries); //data of the first player as string
		int SelectedPid = -1;

		// Find Players With Half Fitness
		foreach (string pid in DAO.PurchasedPlayers.Split (new char[1]{'|'}, StringSplitOptions.RemoveEmptyEntries)) {
			plData = DAO.GetPlayerData (int.Parse (pid)).Split (new char[1]{ '|' }, StringSplitOptions.RemoveEmptyEntries); //get player data of every owned player 


			if (int.Parse (plData [0]) <= (DAO.Settings.DefaultMaxFitness + int.Parse (plData [1])) / 2) { // if current player fitness smaller/equal than defult max fitness plus level editional fitness divided by 2

				// Check if this player hase more fitness than last player
				if (SelectedPid > -1) {
					if (int.Parse (plData [0]) > int.Parse (selectedPData [0])) {
						selectedPData = plData;
						SelectedPid = int.Parse (pid);
					}
				} else {
					selectedPData = plData;
					SelectedPid = int.Parse (pid);
				}

			}
		}

		if (SelectedPid < 0){
			return new PlayerForNotification();
		}else {

			int fitnessMissing = (DAO.Settings.DefaultMaxFitness + int.Parse (selectedPData [1])) - int.Parse (selectedPData [0]);
			float minutesToFullFitness = (float)fitnessMissing / DAO.Settings.FitnessPerMinute;

			return new PlayerForNotification (
				PrefabManager.instanse.PlayerStructures [SelectedPid].PlayerName,
				DateTime.Now.AddMinutes (Mathf.Round(minutesToFullFitness))
			);

		}
	}


	// ----------------------

	void SheduleAllNotifications ()
	{
		if (!DAO.IsNotificationsEnabled)
			return;


		string msg;

		// TEST NOTIFICATION
		//SheduleNotification (11, "TEST!", DateTime.Now.AddMinutes(1)); 

		if (!HeyzapWrapper.isPlayingAd) {
		// FIRST NOTIFICATION
		if (UserData.Instance.userType == UserData.UserType.NEW_USER) {
			msg = DAO.Language.GetString ("ln-fn");
			SheduleNotification (1, msg, DateTime.Now.AddHours (2)); 
		}


		// PLAYER BACK TO ENERGY
		PlayerForNotification plfn = GetPlayerForNotification ();
		if (!plfn.IsEmpty) {
			msg = DAO.Language.GetString ("ln-btfe").Replace ("%PLAYERNAME%", plfn.PlayerName);
			SheduleNotification (2, msg, plfn.FullFitnessDate.AddMinutes (5)); 
		}


		// NO QUEST STARTED
		if (DAO.NumOfCompletedCups == 0 && DAO.ActiveRunID == -1 && UserData.Instance.userType != UserData.UserType.NEW_USER) {
			msg = DAO.Language.GetString ("ln-nqs");
			SheduleNotification (3, msg, DateTime.Now.AddHours (23)); 
		}


		// BACK ON DAY 7
		msg = DAO.Language.GetString ("ln-bd7");
		SheduleNotification (4, msg, DateTime.Now.AddDays (7)); 


		// BACK ON DAY 2
		msg = DAO.Language.GetString ("ln-bd2");
		SheduleNotification (5, msg, DateTime.Now.AddDays (2)); 
		//Free Gifts Are Ready
			if (timers.ContainsKey ("time_to_claim_free_gift")) {
				if (timers ["time_to_claim_free_gift"] > DateTime.Now) {
					msg = DAO.Language.GetString ("text_for_claim_free_gift");
					SheduleNotification (20, msg, timers ["time_to_claim_free_gift"]);
				}
			}
		}
        //daily gifts reminder
        msg = DAO.Language.GetString("dg-reminder");
        if (!DAO.IsFreePlayerClaimed)
        {
            if (DAO.DailyGiftsLast!= new DateTime())
            {
                SheduleNotification (6, msg, DAO.DailyGiftsLast.AddHours (20)); 
            }
        }
	}



	public class PlayerForNotification{
		public bool IsEmpty;
		public string PlayerName;
		public DateTime FullFitnessDate;

		public PlayerForNotification(){
			IsEmpty = true;
		}

		public PlayerForNotification(string name, DateTime fullFitnessDate){
			IsEmpty = false;
			PlayerName = name;
			FullFitnessDate = fullFitnessDate;
		}
	}


}