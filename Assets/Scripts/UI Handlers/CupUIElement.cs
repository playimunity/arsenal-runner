﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class CupUIElement : MonoBehaviour {

	public Text PrizeStr;
	public Text MinimumStr;
	public Text CompletedStr;
	public Text RankStr;
	public Text RestartBTNStr;
	public Text OngoingStr;
	public Text ContinueBTNStr;
	public Text AvailableStr;
	public Text StartBTNStr;
	public Text LockedBtnStr;


	public GameObject BG_BlackTransparent;
	public GameObject BG_Black;
	public GameObject BG_Yellow;
	public GameObject BG_Grey;

	public GameObject SBG_Yellow;
	public GameObject SBG_Black;
	public GameObject SBG_White;

	public GameObject Status_Completed;
	public GameObject Status_Ongoing;
	public GameObject Status_Available;
	public GameObject Status_Locked;

	public Text CupIcon;
	public Text nameTXT;
	public Text prizeTXT;
	public Text prizeValueTXT;
	public Text playersRequredMinTXT;
	public Text playersRequredIcon;
	public Text playersRequredXTXT;
	public Text playersRequredTXT;

	public Button actionBTN;
	public Text remainTimeTXT;
	public Text rankTXT;

	public Text star1;
	public Text star2;
	public Text star3;

	public Color Color_BlackTransparent;
	public Color Color_Black;
	public Color Color_Yellow;
	public Color Color_Grey;
	public Color Color_White;

	public Cup cup;
	public ButtonClickAnimation BCAnimation;

	WaitForSeconds waitMinute;

	void Awake(){
		waitMinute = new WaitForSeconds(1f);

	}

	void OnDisable(){
		StopAllCoroutines ();
	}



	public void SetView(Cup c){

		cup = c;

		CupIcon.text = cup.icon;
		nameTXT.text = cup.name;
		playersRequredTXT.text = cup.requeredPlayers.ToString();
		prizeValueTXT.text = cup.prize.ToString();

		actionBTN.onClick.RemoveAllListeners ();

		if (cup.status == Cup.CupStatus.NOT_STARTED) {
			SetView_Available ();
		} else if (cup.status == Cup.CupStatus.IN_PROGRESS) { 
			SetView_Ongoing ();
			//StartCoroutine( UpdateRemainTime() );
		} else if (cup.status == Cup.CupStatus.NOT_AVAILABLE) { 
			SetView_Locked ();
		} else if (cup.status == Cup.CupStatus.COMPLETED) {
			SetView_Completed ();
		}
			
		SetStrings ();
	}

	void SetStrings(){

		PrizeStr.text = DAO.Language.GetString ("prize");
		MinimumStr.text = DAO.Language.GetString ("minimum");
		CompletedStr.text = DAO.Language.GetString ("completed");
		RankStr.text = DAO.Language.GetString ("rnak");
		RestartBTNStr.text = DAO.Language.GetString ("restart");
		OngoingStr.text = DAO.Language.GetString ("ongoing");
		ContinueBTNStr.text = DAO.Language.GetString ("continue");
		AvailableStr.text = DAO.Language.GetString ("available");
		StartBTNStr.text = DAO.Language.GetString ("start");
		LockedBtnStr.text = DAO.Language.GetString ("locked");

	}



	void SetView_Completed(){
		BG_BlackTransparent.SetActive(true);
		BG_Black.SetActive(false);
		BG_Yellow.SetActive(false);
		BG_Grey.SetActive(false);

		SBG_Yellow.SetActive(true);
		SBG_Black.SetActive(false);
		SBG_White.SetActive(false);

		Status_Completed.SetActive(true);
		Status_Ongoing.SetActive(false);
		Status_Available.SetActive(false);
		Status_Locked.SetActive(false);

		CupIcon.color = Color_BlackTransparent;
		nameTXT.color = Color_Yellow;
		prizeTXT.color = Color_BlackTransparent;
		prizeValueTXT.color = Color_BlackTransparent;
		playersRequredMinTXT.color = Color_BlackTransparent;
		playersRequredIcon.color = Color_BlackTransparent;
		playersRequredXTXT.color = Color_BlackTransparent;
		playersRequredTXT.color = Color_BlackTransparent;


		rankTXT.text = "B";
		star1.text = "8";
		star2.text = "8";
		star3.text = "8";

		if (cup.CompleteRank == 2){
			star1.text = "7";
			rankTXT.text = "B+";
		}
		if (cup.CompleteRank == 3) {
			star1.text = "7";
			star2.text = "7";
			rankTXT.text = "A-";
		}
		if (cup.CompleteRank > 4) {
			star1.text = "7";
			star2.text = "7";
			star3.text = "7";
			rankTXT.text = "A";
		}

		actionBTN.onClick.AddListener (() => {
			BCAnimation.OnClicked();
			CupsManager.Instance.OnCupClicked(cup);
			AudioManager.Instance.OnBTNClick();
		});
	}

	void SetView_Ongoing(){
		BG_BlackTransparent.SetActive(false);
		BG_Black.SetActive(true);
		BG_Yellow.SetActive(false);
		BG_Grey.SetActive(false);

		SBG_Yellow.SetActive(true);
		SBG_Black.SetActive(false);
		SBG_White.SetActive(false);

		Status_Completed.SetActive(false);
		Status_Ongoing.SetActive(true);
		Status_Available.SetActive(false);
		Status_Locked.SetActive(false);

		CupIcon.color = Color_Black;
		nameTXT.color = Color_Yellow;
		prizeTXT.color = Color_Black;
		prizeValueTXT.color = Color_Black;
		playersRequredMinTXT.color = Color_Black;
		playersRequredIcon.color = Color_Black;
		playersRequredXTXT.color = Color_Black;
		playersRequredTXT.color = Color_Black;

		actionBTN.onClick.AddListener (() => {
			BCAnimation.OnClicked();
			CupsManager.Instance.OnCupClicked(cup);
			AudioManager.Instance.OnBTNClick();
		});
	}

	void SetView_Available(){
		BG_BlackTransparent.SetActive(false);
		BG_Black.SetActive(false);
		BG_Yellow.SetActive(true);
		BG_Grey.SetActive(false);

		SBG_Yellow.SetActive(false);
		SBG_Black.SetActive(true);
		SBG_White.SetActive(false);

		Status_Completed.SetActive(false);
		Status_Ongoing.SetActive(false);
		Status_Available.SetActive(true);
		Status_Locked.SetActive(false);

		CupIcon.color = Color_Yellow;
		nameTXT.color = Color_Black;
		prizeTXT.color = Color_Yellow;
		prizeValueTXT.color = Color_Yellow;
		playersRequredMinTXT.color = Color_Yellow;
		playersRequredIcon.color = Color_Yellow;
		playersRequredXTXT.color = Color_Yellow;
		playersRequredTXT.color = Color_Yellow;

		actionBTN.onClick.AddListener (() => {
			BCAnimation.OnClicked();
			CupsManager.Instance.OnCupClicked(cup);
			AudioManager.Instance.OnBTNClick();
		});
	}

	void SetView_Locked(){
		BG_BlackTransparent.SetActive(false);
		BG_Black.SetActive(false);
		BG_Yellow.SetActive(false);
		BG_Grey.SetActive(true);

		SBG_Yellow.SetActive(false);
		SBG_Black.SetActive(false);
		SBG_White.SetActive(true);

		Status_Completed.SetActive(false);
		Status_Ongoing.SetActive(false);
		Status_Available.SetActive(false);
		Status_Locked.SetActive(true);

		CupIcon.color = Color_Grey;
		nameTXT.color = Color_White;
		prizeTXT.color = Color_Grey;
		prizeValueTXT.color = Color_Grey;
		playersRequredMinTXT.color = Color_Grey;
		playersRequredIcon.color = Color_Grey;
		playersRequredXTXT.color = Color_Grey;
		playersRequredTXT.color = Color_Grey;

		actionBTN.onClick.AddListener (() => {
			AudioManager.Instance.OnBTNClick();
			BCAnimation.OnClicked();
			GenericUIMessage msg = new GenericUIMessage();
			msg.Title = DAO.Language.GetString("cup-locked");
			msg.Message = DAO.Language.GetString("recruit-more-players-msg");

			msg.NegativeBTNAction = ()=>{
				CrossSceneUIHandler.Instance.hideGenericMessage();
			};

			msg.PositiveBTNAction = ()=>{
				CrossSceneUIHandler.Instance.hideGenericMessage();
				MainScreenUiManager.instance.CupScrnAnimToggle(false);
				//LockerRoom_Actions.instance.PlayerChooseScrnAnim(true);
				PlayerChooseManager.instance.SwitchToPreviewMode();
			};

			msg.PositiveButtonText = DAO.Language.GetString("reqruit-player");
			msg.NegativeButtonText = DAO.Language.GetString("not-now");

			CrossSceneUIHandler.Instance.showGennericMessage(msg);
		});
	}

}
