﻿using UnityEngine;
using System.Collections;

public class RandomBuilding : MonoBehaviour {

	public GameObject[] buildings;
	GameObject chosenBuilding;

	Transform buildingTransform;

	int randomNum;

	public bool randomRotation = false;

	Vector3 rot;
	float[] angles = new float[]{0f, 90f, 180f, 270f}; 
	int randomAngle;

	void Start () {
		randomNum = Random.Range (0, buildings.Length);
		chosenBuilding = Instantiate (buildings [randomNum]);
		buildingTransform = chosenBuilding.transform;
		buildingTransform.parent = transform;
		buildingTransform.localPosition = Vector3.zero;
		if (randomRotation) {
			randomAngle = Random.Range (0, angles.Length);
			rot.y = angles [randomAngle];
			buildingTransform.localEulerAngles = rot;
			//Debug.Log ("alon______ RandomBuilding - angles length: " + angles.Length);
		} else {
			buildingTransform.localEulerAngles = Vector3.zero;
		}
	}
	

}
