﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using Facebook.Unity;
using System.Collections.Generic;

public class ShareCamera : MonoBehaviour
{
	bool grab = true;

	Texture2D shareTexture;

	public RawImage tempRawImage;

	public GameObject shareScrn;
	public RawImage userImage;

	byte[] savedImage;

	string title = "My FCB-Ultimate Rush Team!";
	string subject = "My FCB-Ultimate Rush Team!";
	string text = "http://www.gamehour.com/";

	string destination;

	public Diffusion diffusion;


	private void Start ()
	{
		shareScrn.SetActive (true);
		//Login ();
		//shareTexture = new Texture2D (Screen.width, Screen.width, TextureFormat.RGB24, true);

		//StartCoroutine (SaveScreenshot());
	}

	private void OnPostRender ()
	{
		if (grab) {
			shareTexture = new Texture2D (Screen.width, Screen.width, TextureFormat.RGB24, false);
			shareTexture.ReadPixels (new Rect (0, 0, Screen.width, Screen.width), 0, 0);
			shareTexture.Apply ();
			savedImage = shareTexture.EncodeToJPG ();
			//tempRawImage.texture = shareTexture;
			grab = false;
		}

		destination = Path.Combine (Application.persistentDataPath, "fcb-team.jpg");
		File.WriteAllBytes (destination, savedImage);
		FBShare ();



		shareScrn.SetActive (false);
		//gameObject.SetActive (false);

	}

	public void Login ()
	{
		FB.LogInWithPublishPermissions (new List<string> () { "publish_actions" }, LoginCallback);
		FB.ActivateApp ();
	}

	public void LoginCallback (ILoginResult result)
	{
		if (FB.IsLoggedIn) {
			FBModule.Instance.LoginCallback (result);
			StartCoroutine (ShareOnFacebook ());

		}
	}

	public IEnumerator ShareOnFacebook ()
	{
	 	yield return new WaitForEndOfFrame();
		string captionToPost = "Look at my stats on FC Barcelona Ultimate Rush! Play Now - " + DAO.Instance.stroeAppLink ;
		yield return new WaitForEndOfFrame ();
		var wwwForm = new WWWForm ();
		wwwForm.AddBinaryData ("image", savedImage, "fcb-team.jpg");
		wwwForm.AddField ("caption", captionToPost);
		FB.API ("me/photos", HttpMethod.POST, SharedCallback, wwwForm);
//		File.WriteAllBytes (Path.Combine (Application.persistentDataPath, "Share.png"),snapshot);
//		print (Application.persistentDataPath);
		//FBModule.Instance.ShareLink ("http://www.gamehour.com/", "test", "testSUB", "file://"+Path.Combine (Application.persistentDataPath, "Share.png"), "");
		yield return new WaitForEndOfFrame ();
	}

	private void SharedCallback (IGraphResult result)
	{
		print (result.ToString ());
		CrossSceneUIHandler.Instance.ShowNotification( "Shared On Facebook!", 3f );
	}

	void FBShare ()
	{
		//tempRawImage.texture = shareTexture;

//		WWWForm wwwForm = new WWWForm ();
//
//		string imageName = "FCB_Ultimate_" + Time.time + ".png";
//
//		wwwForm.AddBinaryData ("image", savedImage, imageName);
//		wwwForm.AddField ("name", "FCB image post");
//		FBModule.Instance.PostPhoto (wwwForm);
//
//		MainMenu_Actions.instance.DebugLog ("FBShare()");

		#if UNITY_ANDROID && !UNITY_EDITOR
	

		// block to open the file and share it ------------START
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent"); // yes
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");  // yes
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));  // yes
		//intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND_MULTIPLE"));  // test

		// transfer text :
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), title);  // yes
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text);  // yes
		intentObject.Call<AndroidJavaObject> ("setType", "text/plain");  // yes

		// transfer data :
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse","file://" + destination);

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

		intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");  // yes
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");  // yes
		currentActivity.Call("startActivity", intentObject);  // yes

		//MainMenu_Actions.instance.DebugLog ("FB_Share()_beatit_like");

		#elif  UNITY_IPHONE && !UNITY_EDITOR

		diffusion.Share (text, destination);

		#endif




		gameObject.SetActive (false);

	}







}