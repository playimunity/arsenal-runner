﻿using UnityEngine;
using System.Collections;

public class ObstacleHardMoving : Obstacle {


//	public GameObject carLight1;
//	public GameObject carLight2;
//	bool flickering = false;
	bool localMovingTriggered = false;
	WaitForSeconds waitDeathCheck = new WaitForSeconds(0.5f);
	public BoxCollider trigger;
	Vector3 triggerSize;
	float triggerMinXsize = 40f;
	float playerMixSpeed = 17f;
	float sizeIncreaseFactor;
	float playerCurrentSpeed;
    public Animator animator;
	

//	void FixedUpdate () {
//		if (move == Obstacle.MoveTypes.MOVE && moveTriggered) {
//			transform.Translate (Vector3.back * moveSpeed * Time.deltaTime);
//			if (!flickering && Vector3.Distance (transform.position, player.transform.position) <= distanseToHorn) {
//				flickering = true;
//				StartCoroutine (Flicker ());
//				PlayHornSFX ();
//			}
//		} else {
//			flickering = false;
//		}
//	}

//	void FixedUpdate () {
//		if (localMovingTriggered && PlayerController.instance.isDead) {
//			moveTriggered = false;
//		}
//	}

	void Start(){
		sizeIncreaseFactor = triggerMinXsize / playerMixSpeed;
		playerCurrentSpeed = PlayerController.instance.runningSpeed;
		triggerSize = trigger.size;
		triggerSize.x = playerCurrentSpeed * sizeIncreaseFactor;
		trigger.size = triggerSize;

//		Debug.Log ("alon______ sizeIncreaseFactor= " + sizeIncreaseFactor);
//		Debug.Log ("alon______ playerCurrentSpeed= " + playerCurrentSpeed);
//		Debug.Log ("alon______ triggerSize= " + triggerSize);
//		Debug.Log ("alon______ triggerSize.x= " + triggerSize.x);
//		Debug.Log ("alon______ trigger.size= " + trigger.size);
	}

	void OnTriggerEnter(Collider col){

		if (!localMovingTriggered && col.gameObject.tag == "Player") {
			localMovingTriggered = true;
			moveTriggered = true;
            if (animator)
            {
                animator.Play("garbageTruckAnim");
            }
			StartCoroutine (CheckPlayerDeath());
		}
	}

	IEnumerator CheckPlayerDeath(){
		if (localMovingTriggered && PlayerController.instance.isDead) {
			moveTriggered = false;
			yield break;
		}
		yield return waitDeathCheck;
		StartCoroutine (CheckPlayerDeath());
	}



}
