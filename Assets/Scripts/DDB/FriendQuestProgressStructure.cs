﻿using UnityEngine;
using Amazon.DynamoDBv2.DataModel;
using System;


[DynamoDBTable("ars_bd_users_in_quests")]
public class FriendQuestProgressStructure{

	[DynamoDBHashKey] 
	public string user_id { get; set; }

	[DynamoDBProperty]
	public int current_quest { get; set; }


	public FriendQuestProgressStructure(){
		user_id = UserData.Instance.userID;
		current_quest = DAO.ActiveRunID;
	}
}
