﻿using UnityEngine;
using System.Collections;

public class Male : MonoBehaviour {

	Obstacle.ObstacleTypes type = Obstacle.ObstacleTypes.Male;

	public Texture[] textures;
	int randomTexture;

	public Mesh[] meshes;
	int randomMesh;

	public string[] animations;
	int randomAnimation;

	public string[] endAnimations;
	int randomEndAnimation;

	public Animator animator;

	public SkinnedMeshRenderer mesh;

	bool colided = false;

	public bool endSection = false;




	void Start () {
		StartCoroutine(BeKinematic());

		randomTexture = Random.Range (0, textures.Length);
		mesh.material.mainTexture = textures [randomTexture];

		randomMesh = Random.Range (0, meshes.Length);
		mesh.sharedMesh = meshes [randomMesh];

		if (!endSection) {
			randomAnimation = Random.Range (0, animations.Length);
			animator.Play (animations [randomAnimation], 0, 0);
		} else {
			randomEndAnimation = Random.Range (0, endAnimations.Length);
			animator.Play (endAnimations [randomEndAnimation], 0, 0);
		}
	
	}


	void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "Player" && !colided  && !endSection) {
			colided = true;
			Tackle();
			AudioManager.Instance.OnHitMale ();
		}
	}


	public void Tackle(){
		GetComponent<Collider> ().enabled = false;
		GetComponent<Rigidbody> ().isKinematic = true;
		animator.Play ("fall" , 0 , 0);
		//animator.SetTrigger("fall back");
		//PlayerController.instance.hardDamage = true;
		PlayerController.instance.Damage(1);
	}


	IEnumerator BeKinematic(){
		yield return new WaitForSeconds (1.5f);
		GetComponent<Rigidbody> ().isKinematic = true;
	}

	


}
