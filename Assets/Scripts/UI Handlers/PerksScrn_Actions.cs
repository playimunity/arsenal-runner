﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PerksScrn_Actions : MonoBehaviour {

	public RectTransform unOwnedPerks;
	//public RectTransform unOwnedPerksContainer;
	public RectTransform getMoreSkillsTxt;
	public RectTransform scroll;
	public static PerksScrn_Actions Instance;


	Vector2 unOwnedPerksPos;
	Vector2 unOwnedPerksStartPos;
	Vector2 scrollSize;

	public Transform ownedPerks;
	public PerkItem[] perks;

	//public Text PlayerName;

	float perkItemHeight;
	int ownedPerksCounter = 0;

	void Awake(){
		Instance = this;
	}


	void Start () {

		unOwnedPerksStartPos = unOwnedPerks.anchoredPosition;
		perkItemHeight = perks[0].GetComponent<RectTransform>().rect.height;

		for (int i=0; i<perks.Length; i++) {
			perks[i].SetGenericView( DAO.Perks[i]["id"].Value );
		}

		ResetPerksItems ();
	}

//	void OnEnable(){
//		OnBeforeShow ();
//	}

	public void ResetPerksItems(){
		unOwnedPerksPos = unOwnedPerksStartPos;

		ownedPerksCounter = 0;

		// Unparenting
//		foreach (PerkItem perk in perks) {
//			perk.transform.SetParent(TMPPerkContainer ,false);
//		}

		foreach (PerkItem perk in perks) {
			if (perk.owned == true){
				ownedPerksCounter++;
				perk.transform.SetParent(ownedPerks , false);
				unOwnedPerksPos.y -= perkItemHeight;
				unOwnedPerks.anchoredPosition = getMoreSkillsTxt.anchoredPosition = unOwnedPerksPos;

			}else{
				perk.transform.SetParent(unOwnedPerks , false);
			}

		}

		if (ownedPerksCounter >= 3) {
			unOwnedPerks.gameObject.SetActive (false);
			getMoreSkillsTxt.gameObject.SetActive (false);
			scrollSize = scroll.sizeDelta;
			//scrollSize.y = 1297f;
			scrollSize.y = perkItemHeight * 4;
			scroll.sizeDelta = scrollSize;
		} else {
			unOwnedPerks.gameObject.SetActive (true);
			scrollSize = scroll.sizeDelta;
			//scrollSize.y = 4309f;
			scrollSize.y = perkItemHeight * (perks.Length + 1);
			scroll.sizeDelta = scrollSize;
		}
			
	}


	public Player pl;

	public void OnBeforeShow(){
		//Debug.Log ("alon___ OnBeforeShow()");
		if (PlayerChooseManager.instance.currentPlayerScript == null) {
			return;
		}

		pl = PlayerChooseManager.instance.currentPlayerScript;

		//Debug.Log ("alon____ PerksScrn - player perks: " + pl.data.Perk_1.type.ToString () + " , " + pl.data.Perk_2.type.ToString () + " , " + pl.data.Perk_3.type.ToString ());
		//PlayerName.text = pl.playerName;

		for (int i=0; i<perks.Length; i++) {
			perks[i].SetPlayerCpecificView(pl);
		}
		ResetPerksItems ();
	}
	

}
