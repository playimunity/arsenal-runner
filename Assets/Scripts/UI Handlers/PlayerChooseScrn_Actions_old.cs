﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerChooseScrn_Actions_old : MonoBehaviour {

	//bool buyScrnOn = false;
	public static PlayerChooseScrn_Actions_old instance;

	//public RectTransform playerIdPrefab;
	public RectTransform scrollArea;

	public Transform scrollAreaTransform;
	//public Transform playerToBuyInfoPos;
	float playerBtnHeight = 328;

	Vector2 scrollAreaSize;
	//float spacing = 0.1f;
	public List<GameObject> playerIds = new List<GameObject>();

	int[] playersWithoutHeads = new int[] { 21 , 24 }; //players without heads:
	List<int> playersWithoutHeadsList = new List<int>();



	void Awake(){
		instance = this;
		scrollAreaSize = scrollArea.sizeDelta;
		foreach (int player in playersWithoutHeads) {
			playersWithoutHeadsList.Add (player);
		}
		for (int i = 0; i < DAO.Settings.HidePlayers.Count; i++) {
			int playerId;
			bool res = int.TryParse(DAO.Settings.HidePlayers[i], out playerId);
			if (res) {
				playersWithoutHeadsList.Add (playerId);
			}
		}
	}



//	void Start () {
//		foreach (Transform playerId in scrollAreaTransform) {    //creates a list with all the players:
//			playerIds.Add (playerId.gameObject);
//		}
//
//		foreach (int i in playersWithoutHeads) {	//remove the players without the heads:
//			foreach (GameObject playerId in playerIds) {
//				if (playerId.GetComponent<PlayerId> ().PlayerPrefabIndex == i) {
//					playerIds.Remove (playerId);
//					Destroy(playerId.gameObject);
//				}
//			}
//		}
//
//		Debug.Log ("alon___ list of players created !");
//	}

	public void CreatePlayersIdsList(){
		foreach (Transform playerId in scrollAreaTransform) {    //creates a list with all the players:
			playerIds.Add (playerId.gameObject);
		}
		//Debug.Log ("alon___ list of players created !");

		foreach (int i in playersWithoutHeadsList) {	//remove the players without the heads:
			//Debug.Log ("alon___ foreach (int i in playersWithoutHeads) !");
			foreach (Transform playerId in scrollAreaTransform) {
				if(playerId.GetComponent<PlayerId>().PlayerPrefabIndex == i){
					playerIds.Remove(playerId.gameObject);
					Destroy(playerId.gameObject);
					scrollAreaSize.y -= playerBtnHeight;
					scrollArea.sizeDelta = scrollAreaSize;
				}
			}
		}
		//Debug.Log ("alon___ players without heads removed !");
		//PlayerChooseManager.instance.maxSits = playerIds.Count;
		gameObject.SetActive (false);
	}
	




	public void RemovePlayerId(int index){
		foreach (Transform playerId in scrollAreaTransform) {
			if(playerId.GetComponent<PlayerId>().PlayerPrefabIndex == index){
				playerIds.Remove (playerId.gameObject);
				Destroy(playerId.gameObject);
				scrollAreaSize.y -= playerBtnHeight;
				scrollArea.sizeDelta = scrollAreaSize;
			}
		}
	}







}
