﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;
using System;
using Facebook.Unity;
using OnePF;

public class Ads : MonoBehaviour {

	public static Ads _;
	private WWW ajax;
	private string ADS_URL;
	List<Ad> TMPAds;
	Ad TMPAd;
	JSONNode TMP_JSONNode;
	bool IsMatch;

	private const string LIVE_DATA_URL_BASE = "https://ars.gamehour.com/data/get.php?data=ads";
	private const string STAGING_DATA_URL_BASE = "https://aesstage.gamehour.com/data/get.php?data=ads";
	private const string LOCAL_DATA_URL_BASE = "http://imaslo.dev/data/get.php?data=ads";

	private const string IP_INFO_URL = "http://api.ipinfodb.com/v3/ip-country/?key=656fcbc74e4eb21761c95cf5c563c6eb2217652668a3ca02ee878856f15e6f46&format=json";

	// ---------------

	public List<Ad> ads;
	public Ad CurrentAd;
	public string CurrentPlacement;
	public string CountryCode;
	public bool Initialized = false;

	void Awake(){
		_ = this;
		Debug.Log ("RON_____________________Ads Awake");
	}

	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log("Ads Module | "+msg);
	}

	// ---------------

	public void Init(){
		ads =  new List<Ad> ();
		TMPAds = new List<Ad> ();
		GetCountryCode ();
		Debug.Log ("RON_____________________Ads Init");
	}
		
	void SetHosts(){
		if (!AppSettings._.LOAD_DATA_FROM_LOCAL_SERVER) {
			if (DAO.IsInDebugMode) {
				ADS_URL = STAGING_DATA_URL_BASE;
			} else {
				ADS_URL = LIVE_DATA_URL_BASE;
			}
		} else {
			ADS_URL = LOCAL_DATA_URL_BASE;
		}

		ADS_URL += "&cc=" + CountryCode + "&lang="+DAO.AppLanguage;
	}


	public void GetCountryCode(){
		if (DAO.CountryCode != "WW") {
			CountryCode = DAO.CountryCode;
			OnCountryCodeReady ();
		} else {
			StartCoroutine (GetCountryCodeAjax ());
		}

	}

	private IEnumerator GetCountryCodeAjax(  ){
		ajax = new WWW(  IP_INFO_URL  );

		while(ajax.isDone == false){
			yield return null;
		}

		if (ajax.error == null) {
			CountryCode = SimpleJSON.JSON.Parse (ajax.text)["countryCode"].Value;
			OnCountryCodeReady ();
			Log ("country code ajax recieved: " + CountryCode);
		} else {
			Log ("Country Code Ajax Error: " + ajax.error);
		}
	}

	private IEnumerator GetAdsSetup(){
		Debug.Log ("RON_____________________GetAdsSetup");
		Log ("Calling Ads AJAX from: " + ADS_URL);

		ajax = new WWW(  ADS_URL );

		while(ajax.isDone == false){
			yield return null;
		}

		if (ajax.error == null) {
			Log ("Ads Recieved: " + ajax.text);
			OnAdsDataReady (ajax.text);
		} else {
			Log ("Ajax Error: " + ajax.error);
		}
	}

	void OnCountryCodeReady(){
		DAO.CountryCode = CountryCode;
		DAO.Country = CountryCode;
		SetHosts ();
		StartCoroutine (GetAdsSetup ());
	}

	void OnAdsDataReady(string ads_setup){
		try
		{
		TMP_JSONNode = SimpleJSON.JSON.Parse (ads_setup);

		for (int i = 0; i < TMP_JSONNode.Count; i++) {
			TMPAd = new Ad (TMP_JSONNode [i]);
			if(TMPAd.countries.Contains (CountryCode) || TMPAd.countries.Count == 0) ads.Add ( TMPAd );
			}
			Initialized = true;
		}
		catch (Exception e)
		{
			Debug.Log ("OnDataVersionRecieved invalid version id: " + e.Message);
		}
		OnMainMenu ();
	}

	void FindAd(){
		TMPAd = null;
		if (TMPAds != null)
			TMPAds.Clear ();
		else
			TMPAds = new List<Ad> ();
		CurrentAd = null;

		if (ads != null & ads.Count > 0) {
			foreach (Ad _ad in ads) {
				IsMatch = true;

				// Check Placement & Check If pass enough time (according to interval) to show the ad
				if (_ad.Placements.Contains (CurrentPlacement) && _ad.CanBeShown ()) {

					// Check Conditions
					foreach (AdCondition cond in _ad.Conditions) {
						IsMatch = cond.IsTrue ();
						if (!IsMatch)
							break;
					}

				} else {
					IsMatch = false;
				}

				if (IsMatch)
					TMPAds.Add (_ad);
			}
		}
		//Debug.Log ("alon______________ TMPAds.Count " + TMPAds.Count);
		if (TMPAds.Count == 0) {
			//Debug.Log ("alon______________ TMPAds.Count - TMPAds.Count == 0" + TMPAds.Count);
			return;
		}

		// Check Prioriy if There is more than 1 Match
		if (TMPAds.Count > 0) {
			TMPAd = TMPAds [0];
		}

		if (TMPAds.Count > 1) {
			foreach (Ad _ad in TMPAds) {
				if (_ad.Priority < TMPAd.Priority) TMPAd = _ad;
			}

		} 

		CurrentAd = TMPAd;
	}


	// ----------


	public void OnMainMenu(){

		if (!Initialized) {
			if ((DAO.Instance != null) && (!DAO.Instance.NO_INTERNET)) {
				Init ();
				return;
			} else
				return;
		}

		// -----

		CurrentPlacement = "MENU";
		if (MainMenu_Actions.instance != null && MainMenu_Actions.instance.dailyGifts != null && MainMenu_Actions.instance.dailyGifts.gameObject != null) {
			if (!MainMenu_Actions.instance.dailyGifts.gameObject.activeSelf) {
//				Debug.Log ("alon____________ MainMenu_Actions.instance.dailyGifts.gameObject.activeSelf= " + MainMenu_Actions.instance.dailyGifts.gameObject.activeSelf);
//				Debug.Log ("alon____________ MainMenu_Actions.instance.dailyGifts.gameObject.name= " + MainMenu_Actions.instance.dailyGifts.gameObject.name);
				FindAd ();
				if (CurrentAd != null)
					StartCoroutine (PrepareAdAndShow (CurrentAd));
			}
		} 
		else 
		{
			FindAd ();
			if (CurrentAd != null)
				StartCoroutine (PrepareAdAndShow (CurrentAd));
		}
			
	}

	public void OnBench(){
		if (!Initialized) {
			if ((DAO.Instance != null) && (!DAO.Instance.NO_INTERNET)) {
				Init ();
				return;
			} else
				return;
		}

		CurrentPlacement = "BENCH";
		FindAd ();

		if (CurrentAd != null) StartCoroutine (PrepareAdAndShow (CurrentAd));
	}

	public IEnumerator PrepareAdAndShow(Ad ad){
		WWW www = new WWW(ad.ImageUrl);

		while (!www.isDone) {
			yield return null;
		}

		if (www.error != null) {
			//Debug.Log ("alon_________________ ad wont shoe - NO INTERNET");
			yield break;
		} else {
			ad.ImageTexture = www.texture;
			ad.Show ();
			//Debug.Log ("alon_________________ ad sould be shown");
		}
	}

	public void OnAdClicked(){
		if (CurrentAd!=null)
			CurrentAd.OnClicked ();
		//OnAdClosed ();
	}

	public void OnAdClosed(){
		CurrentAd = null;
		CrossSceneUIHandler.Instance.hideGenericAd ();
		GM_Input._.AdModulOn = false;
	}

}




// ====================================================================================================================
// ====================================================================================================================



/****
 * 
 * 		Ad
 * 
 */
[System.Serializable]
public class Ad{

	private string sTitleCode;
	private string sCancelTXTCode;
	private string sCATTXTCode;
	private string sPromoTXTCode;
	public string Title;
	public string CancelTXT;
	public string CATTXT;
	public string ImageUrl;
	public string PromoTXT;
	public string HashName;
	public string IAPPrice;
	public Texture ImageTexture;
	public string IAPSKU;

	public int Interval;
	public int Priority;

	public List<string> countries;
	public List<string> Placements;
	public List<AdCTA> CTAs;
	public List<AdCondition> Conditions;
	public bool ShowPrivacy;

	public Ad(JSONNode _ad ){
		sTitleCode = _ad ["Title"].Value;
		sCATTXTCode = _ad ["CTA_TXT"].Value;
		sPromoTXTCode = _ad ["Text"].Value;
		sCancelTXTCode = _ad ["CANCEL_TXT"].Value;

		ImageUrl = _ad["Image"].Value;
		Interval = _ad["Interval"].AsInt;
		Priority = 	_ad["Priority"].AsInt;
		ShowPrivacy = false;

		if (_ad ["Privacy"].AsInt == 1)
			ShowPrivacy = true;
		HashName = System.Convert.ToBase64String( System.Text.Encoding.UTF8.GetBytes(Title+ImageUrl) );

		// Countries
		countries = new List<string> ();
		for(int i=0; i < _ad ["Country"].Count; i++) countries.Add ( _ad ["Country"][i].Value );

		// CTA
		CTAs = new List<AdCTA> ();
		for(int i=0; i < _ad ["CTA"].Count; i++) CTAs.Add ( new AdCTA(_ad ["CTA"][i]) );

		// Conditions
		Conditions = new List<AdCondition> ();
		for (int i = 0; i < _ad ["Conditions"].Count; i++) Conditions.Add ( new AdCondition(_ad ["Conditions"][i]) );

		// Placements
		Placements = new List<string>();
		for (int i = 0; i < _ad ["Placement"].Count; i++) Placements.Add ( _ad ["Placement"][i].Value );

		// Check if this is an IAP promotion
		foreach(AdCTA cta in CTAs){
			if (cta.CTAType == AdCTA.Types.TRIGGER_IAP) {
				IAPSKU = cta.Param;
				IAPProduct ip = IAP.Instance.GetIAPProducatBySKU (IAPSKU);
				IAPPrice = ip.GetPriceLabel ();
				break;
			}
		}
	}

	public void Show(){
		
		if (Ads._.CurrentPlacement == "BENCH" && GameManager.curentGameState != GameManager.GameState.LOCKER_ROOM) return;
	//	if (Ads._.CurrentPlacement == "MENU" && GameManager.curentGameState != GameManager.GameState.MAIN_MENU) return;

		Title = DAO.Language.GetString(sTitleCode);
		CATTXT = DAO.Language.GetString(sCATTXTCode);
		PromoTXT = DAO.Language.GetString(sPromoTXTCode);

		if (!string.IsNullOrEmpty(sCancelTXTCode)) {
			CancelTXT = DAO.Language.GetString(sCancelTXTCode);
		} else {
			CancelTXT = "";
		}
		// ---
			
		PlayerPrefs.SetString(HashName, DateTime.Now.ToString());
		CrossSceneUIHandler.Instance.showGennericAd (this);

		GM_Input._.AdModulOn = true;

	}

	public bool CanBeShown(){
		if ( !PlayerPrefs.HasKey (HashName) ) return true;

		return (DateTime.Now - DateTime.Parse (PlayerPrefs.GetString (HashName))).TotalMinutes >= Interval;
	}


	public void OnClicked(){
		bool IsIAPCtaExists = false;
		bool IsIPPCtaExists = false;
		int Price = 0;
		int index = 0;

		for (index=0; index < CTAs.Count; index++ ) {
			if (CTAs [index].CTAType == AdCTA.Types.CAN_SEND_EMAIL) {
				CTAs [index].Invoke ();	
			}
			if (CTAs [index].CTAType == AdCTA.Types.PLAYER_TO_BENCH) {
				IsIPPCtaExists = true;
			} else if (CTAs [index].CTAType == AdCTA.Types.REMOVE_X_COINS) {
				Price = int.Parse (CTAs [index].Param);
			} 
					
		}

		//if (IsIPPCtaExists && (playerPrice > DAO.TotalCoinsCollected)) {
		if (Price > DAO.TotalCoinsCollected) {
			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
			return;
		}
			
		// Check If there is IAP CTA
		for (index=0; index < CTAs.Count; index++ ) {
			if (CTAs [index].CTAType == AdCTA.Types.TRIGGER_IAP) {
				IsIAPCtaExists = true;
				break;
			}
		}


		if (IsIAPCtaExists) {
			// Invoke IAP CTA and store the rest to invoke them after 'purchase succeded event'
			AdCTA iapCta = CTAs [index];
			CTAs.RemoveAt (index);
			IAP.Instance.AdModuleCallBacks = CTAs;

			iapCta.Invoke ();
		} else {
		
			foreach (AdCTA cta in CTAs) {
				cta.Invoke ();
			}

		}
		Ads._.OnAdClosed ();

	}

 
}

/****
 * 
 * 		Ad CTA
 * 
 */
[System.Serializable]
public class AdCTA{

	public enum Types{
		ADD_X_COINS,
		ADD_X_GIFTCARDS,
		TRIGGER_IAP,
		TRIGGER_RATEUS,
		ADD_X_X2,
		ADD_X_MAGNETS,
		ADD_X_GODMODE,
		ADD_X_ENERGYDRINKS,
		TRIGGER_INVITEFRIEND,
		REMOVE_X_COINS,
		OPEN_URL,
		TRIGGER_FB_LOGIN,
		PLAYER_TO_BENCH,
		CAN_SEND_EMAIL
	}

	public Types CTAType;
	public string Param;

	public AdCTA(JSONNode cta){

		switch (cta[0].Value) {
			case "AXC":{
				CTAType = Types.ADD_X_COINS;
				Param = cta [1].Value;
				break;	
			}
			case "AXG":{
				CTAType = Types.ADD_X_GIFTCARDS;
				Param = cta [1].Value;
				break;	
			}
			case "IAP":{
				CTAType = Types.TRIGGER_IAP;
				Param = cta [1].Value;
				break;	
			}
			case "TRU":{
				CTAType = Types.TRIGGER_RATEUS;
				break;	
			}
			case "":{
				CTAType = Types.ADD_X_COINS;
				Param = cta [1].Value;
				break;	
			}
			case "AXX2":{
				CTAType = Types.ADD_X_X2;
				Param = cta [1].Value;
				break;	
			}
			case "AXM":{
				CTAType = Types.ADD_X_MAGNETS;
				Param = cta [1].Value;
				break;	
			}
			case "AXGM":{
				CTAType = Types.ADD_X_GODMODE;
				Param = cta [1].Value;
				break;	
			}
			case "AXED":{
				CTAType = Types.ADD_X_ENERGYDRINKS;
				Param = cta [1].Value;
				break;	
			}
			case "TIF":{
				CTAType = Types.TRIGGER_INVITEFRIEND;
				break;	
			}
			case "RXC":{
				CTAType = Types.REMOVE_X_COINS;
				Param = cta [1].Value;
				break;	
			}
			case "URL":{
				CTAType = Types.OPEN_URL;
				Param = cta [1].Value;
				break;	
			}
			case "CFB":{
				CTAType = Types.TRIGGER_FB_LOGIN;
				break;	
			}
			case "PTB":{
				CTAType = Types.PLAYER_TO_BENCH;
				Param = cta [1].Value;
				break;	
			}
		    case "CSE":{
				CTAType = Types.CAN_SEND_EMAIL;
				break;	
			}
		}
	}

	public void Invoke(){


		switch (CTAType) {
			case Types.ADD_X_COINS:{
				//DAO.TotalCoinsCollected += int.Parse (Param);
				CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (10 , int.Parse (Param));
				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (int.Parse (Param), "Ads Module Prize", "FCB Coins", "", "", "");
				GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,int.Parse (Param), GameAnalyticsWrapper.REWARD,"CoinsReward");

				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 

			case Types.ADD_X_ENERGYDRINKS:{
				DAO.TotalMedikKitsCollected += int.Parse (Param);
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
			case Types.ADD_X_GIFTCARDS:{
				DAO.TotalGiftCardsCollected += int.Parse (Param);

				if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {
					CardsScrn_Actions.instance.UpdateGiftCardsCount ();
				}
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
			case Types.ADD_X_GODMODE:{
				DAO.TotalSpeedBoostsCollected += int.Parse (Param);
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
			case Types.ADD_X_MAGNETS:{
				DAO.TotalMagnetsCollected += int.Parse (Param);
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
			case Types.ADD_X_X2:{
				DAO.TotalX2CoinsCollected += int.Parse (Param);
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
			case Types.OPEN_URL:{
				Application.OpenURL (Param);
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 

			case Types.REMOVE_X_COINS:{
				DAO.TotalCoinsCollected -= int.Parse (Param);
				CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Withdraw (int.Parse (Param), "Ad Module Price", "FCB Coins", "", "", "");
				GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, int.Parse (Param), GameAnalyticsWrapper.BUY_PLAYER, "AdModule");
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 

			case Types.TRIGGER_IAP:{
                IAP.Instance.OpenIAPStatus();
				OpenIAB.purchaseProduct( Param );

				break;
			}

			case Types.TRIGGER_INVITEFRIEND:{
				CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.INVITE_FRIENDS);
				break;
			} 

			case Types.TRIGGER_FB_LOGIN:{
				FBModule.Instance.FBButonClicked = true;
				FBModule.Instance.Login ();
				break;
			} 

		case Types.CAN_SEND_EMAIL:{
				DAO.CanSendEmail = true;
				break;
			} 

			case Types.TRIGGER_RATEUS:{
				Application.OpenURL(DAO.Instance.stroeAppLink);
				break;
			} 
			case Types.PLAYER_TO_BENCH:{
				int iParam;
				if (int.TryParse (Param.Trim(), out iParam) && (!DAO.Instance.IsPlayerAllReadyPurchased(Param.Trim()))) {
					DAO.PurchasedPlayers += Param.Trim() + "|";
					IAP.Instance.updateLegendPlayerPerks (iParam);
					DAO.Instance.LOCAL.SaveAll ();
					string playerName = "Arda Turan";
					foreach (PlayerStructure playerStract in PrefabManager.instanse.PlayerStructures) {
						if (playerStract.PlayerID.ToString () == Param) {
							playerName = playerStract.PlayerName;
						}
					}
					//if(GameManager.Instance.isPlayscapeLoaded)	
					//BI._.Inventory_Upgrade (1, "Player ", "Bench", playerName);
					GameAnalyticsWrapper.BuyNewPlayer (playerName, PlayerChooseManager.instance.CurrentPlayerPrice);
					string[] pp = DAO.PurchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries);
					UnitedAnalytics.LogEvent ("Player bought " + pp.Length.ToString (), "buy", UserData.Instance.userType.ToString (), DAO.PlayTimeInMinutes);
				}
				break;
			} 
		}

	}


}



/****
 * 
 * 		Ad Condition
 * 
 */
[System.Serializable]
public class AdCondition{
	public enum Types {
		MIN_PLAYTIME,
		MAX_PLAYTIME,
		USER_TYPE,
		COINS_AMOUNT,
		IS_CONNECTED_TO_FB,
		IS_STARTERPACK_PURCHASED,
		IS_LEGEND_PURCHASED,
		IS_SPECIFIC_PLAYER_PURCHASED,
		IS_CAN_SEND_EMAIL
	};


	public Types type;
	public bool TheBool;
	public string Param;

	public AdCondition(JSONNode cond){

		TheBool = cond [1].AsBool;
		Param = cond [2].Value;

		switch( cond[0].Value ){
			
			case "MINPT":{
				type = Types.MIN_PLAYTIME;
				break;
			}

			case "MAXPT":{
				type = Types.MAX_PLAYTIME;
				break;
			}

			case "UT":{
				type = Types.USER_TYPE;
				break;
			}

			case "COA":{
				type = Types.COINS_AMOUNT;
				break;
			}

			case "IFB":{
				type = Types.IS_CONNECTED_TO_FB;
				break;
			}

			case "ISP":{
				type = Types.IS_STARTERPACK_PURCHASED;
				break;
			}

			case "ILP":{
				type = Types.IS_LEGEND_PURCHASED;
				break;
			}

			case "IPP":{
				type = Types.IS_SPECIFIC_PLAYER_PURCHASED;
				break;
			}

	    	case "CSE":{
				type = Types.IS_CAN_SEND_EMAIL;
				break;
			}

		}

	}

	public bool IsTrue(){
		

		switch (type) {
			case Types.COINS_AMOUNT:{
					
				if (TheBool) {
					return (DAO.TotalCoinsCollected >= int.Parse (Param));
				} else {
					return (DAO.TotalCoinsCollected < int.Parse (Param));
				}

			}
			case Types.IS_CONNECTED_TO_FB:{

				if (TheBool) {
					return FB.IsLoggedIn;
				} else {
					return !FB.IsLoggedIn;
				}
			}

			case Types.IS_LEGEND_PURCHASED:{

				if (TheBool) {
					return DAO.IsLegendPurchased;
				} else {
					return !DAO.IsLegendPurchased;
				}
			}

			case Types.IS_SPECIFIC_PLAYER_PURCHASED:{
				//string[] players = DAO.PurchasedPlayers.Split(DAO.char_separator, StringSplitOptions.RemoveEmptyEntries);
				bool thereIsPlayer = DAO.Instance.IsPlayerAllReadyPurchased (Param.Trim ());
				/*
				foreach(string pId in players){
					//Debug.Log ("alon______________ IS_SPECIFIC_PLAYER_PURCHASED - pId: " + pId);
					if (pId == Param) {
						thereIsPlayer = true;
						break;
					}
				}
				*/

				if (TheBool) {
					return thereIsPlayer;
				} else {
					return !thereIsPlayer;
				}
			}

			case Types.IS_STARTERPACK_PURCHASED:{

				if (TheBool) {
					return DAO.StarterPackBought;
				} else {
					return !DAO.StarterPackBought;
				}
			}

		case Types.IS_CAN_SEND_EMAIL:{
				if (TheBool) {
					return DAO.CanSendEmail;
				} else {
					return !DAO.CanSendEmail;
				}
			}

			case Types.MAX_PLAYTIME:{
				return ( DAO.PlayTimeInMinutes < int.Parse (Param) );
			}
			case Types.MIN_PLAYTIME:{
				return ( DAO.PlayTimeInMinutes > int.Parse (Param) );
			}

			case Types.USER_TYPE:{
				UserData.UserType ut;
				if ( "PAYING_USER" == Param.ToUpper () ) ut = UserData.UserType.PAYING_USER; 
				else if ("ENGAGED_A" == Param.ToUpper ()) ut = UserData.UserType.ENGAGED_A; 
				else if ("ENGAGED_B" == Param.ToUpper ()) ut = UserData.UserType.ENGAGED_B; 
				else if ("ENGAGED_C" == Param.ToUpper ()) ut = UserData.UserType.ENGAGED_C; 
				else if ("ENGAGED_D" == Param.ToUpper ()) ut = UserData.UserType.ENGAGED_D; 
				else ut = UserData.UserType.NEW_USER; 

				if (TheBool) {
					return (UserData.Instance.userType >= ut); 
				} else {
					return (UserData.Instance.userType < ut); 
				}

			}


		}

		return false;

	}

}
	