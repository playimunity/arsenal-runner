﻿using UnityEngine;
using System.Collections;

public class SecretAreaBall : MonoBehaviour { //the ball side colliders:

	public SecterAreaHandler SecterAreaScript;

	public bool triggered = false;
	public bool isFirst = false;
	//bool tutorialNeeded = false;
	//bool tutorialOn = false;

	public Transform ballMesh;
	public Transform playerTransform;

	public GameObject ball;

	Vector3 ballParentPos;
	Vector3 ballPos;

	float[] randomXPos = new float[] {0f,-2f,2f};
	float distance;

	int randomNum;


	void Start(){
		ballParentPos = transform.localPosition;

		if (!isFirst) {
			randomNum = Random.Range (0, 3);
			ballParentPos.x = randomXPos [randomNum];
			transform.localPosition = ballParentPos;
		} else {
			if (DAO.FirstSecterAreaBall == 0) {
				ball.GetComponent<BoxCollider> ().enabled = true;
				//tutorialNeeded = true;
				StartCoroutine (CentrlizePlayer());

			} else {
				randomNum = Random.Range (0, 3);
				ballParentPos.x = randomXPos [randomNum];
				transform.localPosition = ballParentPos;
			}




		}



		ballPos = ball.transform.localPosition;

	}


	void FixedUpdate(){
		if (!SecterAreaScript.tutorialOn && !triggered && Vector3.Distance (transform.position, playerTransform.position) < 100) {

			ball.SetActive (true);

			ballMesh.RotateAround (ballMesh.transform.up, 0.2f);

			ballParentPos.z -= 0.3f;
			transform.localPosition = ballParentPos;

			distance = Vector3.Distance (transform.position , playerTransform.position);
			ballPos.y = distance / 15f;
			ball.transform.localPosition = ballPos;


		}
	}



	void OnTriggerEnter(Collider col){
		if (!triggered && col.transform.gameObject.tag == "Player") {
			triggered = true;
			//SecterAreaHandler.instance.LastHeaderSwitcher (false);
			SecterAreaHandler.instance.didHeaderIndex = 0;
			SecterAreaHandler.instance.LosePoint ();
		}
	}


	IEnumerator CentrlizePlayer(){
		yield return new WaitForSeconds (1.0f);

		PlayerController.instance.MovePlayerToCenter ();
		PlayerController.instance.secretAreaTutPoint = true;

	}



//	public void ActivateTutorial(){
//		//PlayerController.instance.Pause ();
//		GameManager.Instance.TutorialPointPause();
//		SecterAreaScript.tutorialOn = true;
//		//CupRunController.Instance.SecretAreaTutToggle (true);
//	}
//
//
//	public void FinishTutorial(){
//		if (!SecterAreaScript.tutorialOn)	return;
//		
//		GameManager.Instance.TutorialPointUnpause();
//		SecterAreaScript.tutorialOn = false;
//	}



}
