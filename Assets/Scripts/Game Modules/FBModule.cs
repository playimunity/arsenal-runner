using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;
using System;
using SimpleJSON;
using Amazon;


public class FBModule : MonoBehaviour {

	public static FBModule Instance;

	public static event Action OnFacebookLoggedIn;
	public static event Action OnFacebookProfileRecieved;

	public bool UserPhotoLoaded = false;
	public bool UserProfileLoaded = false;
	public bool Ready = false;
	public bool FBButonClicked = false;
	int attemptsCounter = 0;

	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log ("FB Module | " + msg);
	}

	void Awake(){   
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("FBModule Awake");
		Instance = this;
		//FB.Init(SetInit, OnHideUnity);  
		StartCoroutine (FBModuleSignIn());
	}

	public IEnumerator FBModuleSignIn() {
		int timeCountdown = 0;
		FB.Init(SetInit, OnHideUnity); 
		while ((!Ready) && timeCountdown <= 8) {
			yield return new WaitForSeconds (1.0f);
			timeCountdown++;
		}
		if (!Ready) {
			Ready = true;
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("FBModuleSignIn Ready ");
			Instance = this;
			DAO.Instance.OnFBReady ();
		}


	}

	private void SetInit(){    
		Ready = true;
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("FBModule SetInit Ready Time");
		if (FB.IsLoggedIn)OnLoggedIn ();
		else DAO.Instance.OnFBReady ();

	}                                                                                            
	
	private void OnHideUnity(bool isGameShown){                                                                                         
		if (!isGameShown)Time.timeScale = 0;                                                                                                                                           
		else  Time.timeScale = 1;                                                                                                                                             
	}    
	

	
	public void LoginCallback(ILoginResult result){
		if (FB.IsLoggedIn){  
			Log( "Succesfully Logged in to Facebook" );

			foreach (string perm in result.AccessToken.Permissions) {
				Log( "Permission Granted: " + perm );
			}


			CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("connected-to-fb"), 3f );
			OnLoggedIn();                                                                      
		}    
	}
	
	private void OnLoggedIn(){
		if (OnFacebookLoggedIn != null) OnFacebookLoggedIn ();
		// Get User Profile and Friends that also playing the game 
		FB.API("/me?fields=id,first_name,last_name,email,gender,locale,friends.limit(20).fields(first_name,last_name,id,picture.type(large))", HttpMethod.GET, APICallback);
		//FB.API("/me?fields=id,first_name,last_name, friends.limit(20).fields(first_name,last_name,id,picture.type(large)),invitable_friends.limit(20).fields(first_name,last_name,id,picture.type(large))", HttpMethod.GET, APICallback);
		// Get User Image
		Utils.Instance.LoadPictureFromFBAPI(Utils.GetFBPictureURL ("me", 256, 256), UserPictureLoadedCallback);
        #if UNITY_EDITOR
        MainScreenUiManager.instance.imageBlock.SetActive(false);
        #endif
        print("FB IS LOGGED IN");


	}

	private void APICallback(IGraphResult result){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("FB - APICallback(IGraphResult result) Start");
		//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) Start" );
		// Re-try on error
		if (result.Error != null) {
			//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) - if (result.Error != null)" );
			if (attemptsCounter > 2) {
				attemptsCounter = 0;
				return;
			}
			attemptsCounter++;
			//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) - if (result.Error != null) - FB.API() before" );
			//FB.API("/me?fields=id,first_name,last_name,email,gender,locale,friends.limit(20).fields(first_name,last_name,id,picture.type(large)),invitable_friends.limit(20).fields(first_name,last_name,id,picture.type(large))", HttpMethod.GET, APICallback); 
			FB.API("/me?fields=id,first_name,last_name,email,gender,locale,friends.limit(20).fields(first_name,last_name,id,picture.type(large))", HttpMethod.GET, APICallback);
		//	FB.API("/me?fields=id,first_name,last_namefriends.limit(20).fields(first_name,last_name,id,picture.type(large)),invitable_friends.limit(20).fields(first_name,last_name,id,picture.type(large))", HttpMethod.GET, APICallback);
			//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) - if (result.Error != null) - FB.API() after" );
			return;                                                                                                                
		}
		attemptsCounter = 0;
		// Process results
		UserProfileLoaded = true;

		JSONNode userProfile = JSON.Parse (result.RawResult);
//		Log ("User Profile Recieved: " + result.RawResult.ToString() );
//		Debug.Log ("Ron___________________Lang:" + Application.systemLanguage);

		UserData.Instance.UserName = userProfile ["first_name"].Value +" "+ userProfile ["last_name"].Value;
		UserData.Instance.userID = userProfile ["id"].Value;
		DAO.Email = userProfile ["email"].Value;
		DAO.FirstName = userProfile ["first_name"].Value;
		DAO.LastName = userProfile ["last_name"].Value;
		DAO.Gender = userProfile ["gender"].Value;
		DAO.Lang = DAO.AppLanguage; //userProfile ["locale"].Value; // Application.systemLanguage
		if (!string.IsNullOrEmpty(DAO.CountryCode))
			DAO.Country = DAO.CountryCode;


		//Log( " Friends:  " +userProfile["friends"]["data"].ToString() );
		//Log( " Invitable Friends:  " +userProfile["invitable_friends"]["data"].ToString() );
		//Log( " User Email:  " +userProfile["email"].ToString() );

		FriendsWithGame = new List<FBFriend> ();

		//Debug.Log ("alon___________________FB - APICallback(IGraphResult result) - taking FriendsWithGame List");
		for (int i=0; i<userProfile ["friends"]["data"].Count; i++) {
			FriendsWithGame.Add( new FBFriend(userProfile ["friends"]["data"][i], true) );
		}
		//Debug.Log ("alon___________________FB - APICallback(IGraphResult result) - taking InvitableFriends List before");
		InvitableFriends = new List<FBFriend> ();
//		for (int i=0; i<userProfile ["invitable_friends"]["data"].Count; i++) {
//			InvitableFriends.Add( new FBFriend(userProfile ["invitable_friends"]["data"][i]) );
//		}
		for (int i=0; i<userProfile ["friends"]["data"].Count; i++) {
			InvitableFriends.Add( new FBFriend(userProfile ["friends"]["data"][i]) );
		}
		//Debug.Log ("alon___________________FB - APICallback(IGraphResult result) - taking InvitableFriends List after");
		if (OnFacebookProfileRecieved != null && UserPhotoLoaded) {
			//Debug.Log ("alon___________________FB - APICallback(IGraphResult result) - if (OnFacebookProfileRecieved != null && UserPhotoLoaded)");
			OnFacebookProfileRecieved ();
		} else {
			//Debug.Log ("alon___________________FB - APICallback(IGraphResult result) - if (OnFacebookProfileRecieved != null && UserPhotoLoaded) - else");
		}

		if (!DAO.FBConnected) {
			//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) - if (!DAO.FBConnected) start" );
			DAO.FBConnected = true;
			//string first_player_name = PrefabManager.instanse.PlayerStructures[ int.Parse(DAO.PurchasedPlayers.Split('|')[0]) ].PlayerName;
			//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) - if (!DAO.FBConnected) - UnitedAnalytics.LogEvent" );
			//UnitedAnalytics.LogEvent ("Signed in to facebook", UserData.Instance.userType.ToString (), GameManager.Instance.firstPlayerName , FriendsWithGame.Count);
			UnitedAnalytics.LogEvent ("Signed in to facebook", UserData.Instance.userType.ToString (), "" , FriendsWithGame.Count);
			//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) - if (!DAO.FBConnected) - string first_player_name" );
			AppsFlyerManager._.ReportFBLogin ();
			//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) - if (!DAO.FBConnected) - AppsFlyerManager._.ReportFBLogin ()" );
		}
		//Debug.Log( "alon___________________FB - APICallback(IGraphResult result) Finished" );

		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("FB - APICallback(IGraphResult result) end");
		if ( DDB._ != null && FBButonClicked)
		{
		   DAO.Instance.InitFacebookWithDDB = true;
		   DDB._.ReadData ();
    	   DDB._.CheckGiftCards ();     //temp for debugging
  	       DDB._.CheckGiftCardsRequests ();
		   
		}
	}

	public void UserPictureLoadedCallback( Texture texture ){
		UserData.Instance.UserImageTexture = (Texture2D)texture;
		UserPhotoLoaded = true;

		if (OnFacebookProfileRecieved != null && UserProfileLoaded) OnFacebookProfileRecieved ();
	}



	// ---------------------------------------------------------------------------------------------------------------------------

	public List<FBFriend> FriendsWithGame;
	public List<FBFriend> InvitableFriends;


	public void Login(){
		//FB.LogInWithPublishPermissions ( new List<string>(){"public_profile", "email", "user_friends"}, LoginCallback );
		FB.LogInWithReadPermissions ( new List<string>(){"public_profile", "email", "user_friends"}, LoginCallback );
	}

	public void SendGiftCards(List<string> selectedFriends){

		FB.AppRequest(
			DAO.Language.GetString("fb-send-giftcard-msg"),						// Message
			selectedFriends,													// Recipient
			null,
			null,
			null,
			"",																	// data
			DAO.Language.GetString("fb-send-giftcards-screen-title"),	 		// Title above the FB popup

			delegate(IAppRequestResult result) {// Callback
				if(result.Error != null){  
					FBModule.Log(" Error Sending Gift Cards: " + JSON.Parse (result.RawResult)["error"]);
				}else{
					DDB._.SendGiftCards(selectedFriends);
					FBModule.Log("Gift Card Sent, Result: " + result.RawResult);
				}
			});
	}

	public void RequestGiftCards(List<string> selectedFriends){

		FB.AppRequest(
			DAO.Language.GetString("fb-request-giftcard-msg"),						// Message
			selectedFriends,														// Recipient
			null,
			null,
			null,
			"",																		// data
			DAO.Language.GetString("fb-request-giftcards-screen-title"),	 		// Title above the FB popup

			delegate(IAppRequestResult result) {// Callback
				if(result.Error != null){  
					FBModule.Log(" Error Requesting Gift Cards: " + JSON.Parse (result.RawResult)["error"]);
				}else{
					DDB._.RequestGiftCards(selectedFriends);
					FBModule.Log("Gift Cards Requested, Result: " + result.RawResult);
				}
			});
	}

	public void InviteFriends(List<string> selectedFriends){

		FB.AppRequest (
			DAO.Language.GetString ("fb-request-giftcard-msg"),						// Message
			selectedFriends,														// Recipient
			null,
			null,
			null,
			"",																		// data
			DAO.Language.GetString ("fb-request-giftcards-screen-title"),	 		// Title above the FB popup

			delegate(IAppRequestResult result) {// Callback

				Log(" Invite Result Achieved is null: " + (result == null) );


				if(result.Error != null){  
					FBModule.Log(" Error Inviting Friends: " + JSON.Parse (result.RawResult)["error"]);
				}else{
					CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("invitation sent"), 3f );
					FBModule.Log("Friends Invite, Result: " + result.RawResult);

					DAO.TotalFriendsInvited += selectedFriends.Count;

					if(DAO.TotalFriendsInvited >= 100){
						NativeSocial._.ReportAchievment(SocialConstants.achievement_social_smart);
					}
				}
			});



	}



	public void ShareLink(string downloadUrl,string Title, string txt, string urlPic, string mediaSource){
//		FB.ShareLink (
//			new Uri("http://www.gamehour.com/"),
//			"FCB Ultimate Rush",
//			"This is my Team",
//			new Uri("http://www.gamehour.com/assets/img/logo-light.png"),
//			callback: ShareCallback
//		);


		FB.FeedShare(
			"",
			new Uri(downloadUrl),
			Title,
			txt,
			txt + "2",
			new Uri(urlPic),
			mediaSource,
			callback: ShareCallback
		);


	}

	private void ShareCallback (IShareResult result) {
		if (result.Cancelled || !String.IsNullOrEmpty(result.Error)) {
			Log("ShareLink Error: "+result.Error);
		} else if (!String.IsNullOrEmpty(result.PostId)) {
			// Print post identifier of the shared content
			Log(result.PostId);
		} else {
			// Share succeeded without postID
			Log("ShareLink success!");
		}
	}



	public void PostPhoto(WWWForm wwwForm){
		FB.API("me/photos", Facebook.Unity.HttpMethod.POST , PostPhotoCallback , wwwForm);

		Log ("Unity_PostPhoto(WWWForm wwwForm)");
		//MainMenu_Actions.instance.DebugLog ("PostPhoto(WWWForm wwwForm)");
	}

	public void PostScreenShot(WWWForm wwwForm){
		FB.API("me/photos", HttpMethod.POST, PostScreenShotCallBack, wwwForm);
	}

	void PostScreenShotCallBack(IResult result)
	{
		if (result == null) {
			//Debug.Log ("alon______ FBModule - PostScreenShotCallBack() - result == null");
			return;
		} else {
			//Debug.Log ("alon______ FBModule - PostScreenShotCallBack() - result != null");
		}
	}


	private Texture2D lastResponseTexture;
	private string lastResponse = "";
	private string ApiQuery = "";

	void PostPhotoCallback(IGraphResult result)
	{
		lastResponseTexture = null;
		if (result.Error != null) {
			lastResponse = "Error Response:\n" + result.Error;
			//MainMenu_Actions.instance.DebugLog ("Error Response: " + result.Error);
		} else if (!ApiQuery.Contains ("/picture")) {
			lastResponse = "Success Response:\n" + result.ToString ();
			//MainMenu_Actions.instance.DebugLog ("Success Response: " + result.ToString ());
		}
		else
		{
			lastResponseTexture = result.Texture;
			lastResponse = "Success Response:\n";
		}
	}


}







[System.Serializable]
public class FBFriend{

	public string id;
	public string name;
	public Texture photo;
	public string photo_url;
	public int current_quest;
	public bool registered;

	public FBFriend(){}

	public FBFriend(JSONNode friend, bool isRegistered = false){
		registered = isRegistered;
		id = friend ["id"].Value;
		name = friend ["first_name"].Value;
		photo_url = friend["picture"]["data"]["url"];

		// --
		GetPhoto ();
		if (registered) GetCurrentQuest ();
	}

	void GetPhoto(){
		Utils.Instance.LoadPictureFromURL(photo_url, (Texture texture)=>{
			photo = (Texture2D)texture;
		});
	}

	void GetCurrentQuest(){
		current_quest = -1;
		DDB._.GetFriendsProgress (id, (quest_id) => {
			current_quest = quest_id;
		});
	}
}
