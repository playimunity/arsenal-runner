﻿using UnityEngine;
using System.Collections;

public class EnergyDrink : Collectable {


	public GameObject MeshObject;
	Transform MeshTransform;

	void Start(){
		MeshTransform = MeshObject.transform;
	}

	void FixedUpdate () {
		MeshTransform.Rotate( 0f,0f, 180f*Time.deltaTime);
	}


	void OnTriggerEnter(Collider col){
		if (col.transform.gameObject.tag == "Player") {
			ParticleCollectGenerator.instance.PlayParticle(transform.position , transform.eulerAngles);
			RunManager.OnSheildCollected();
			//AudioManager.Instance.OnPowerUpBTN ();
			gameObject.SetActive(false);//Destroy(gameObject);

		}
	}
}
