using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayerChooseManager : MonoBehaviour {

	public static PlayerChooseManager instance;

	public enum SelectMode { CHOOSE,BUY };

	public SelectMode selectMode = SelectMode.CHOOSE;

	public string[] pp;

	public int currentPlayerIndex = 0;
	public int previewPlayerIndex = 0;

	Quaternion playerRotation = Quaternion.Euler(0,-180,0);

	public GameObject currentPlayer;
	public GameObject lastOwnedPlayer;
	public GameObject previewPlayerToDelete;

	public Player currentPlayerScript;
	public Player lastOwnedPlayerScript;

	public Transform playersParent;
	public Transform playersToBuyParent;

	public List<GameObject> ownedPlayersList = new List<GameObject>();

	//[HideInInspector]
	public List<int> playersToBuyIds;

	bool fullPlayers = false;
	public bool onAction = false;
	public bool tutPopupsDisabled = false;
	public bool isFreePlayer = false;
	bool perkBought = false;
	bool startingCupRun = false;
	bool startingPracticeRun = false;
	bool startingFirstCupRun = false;
	bool needToShowPlusBtn = false;

	public int CurrentPlayerPrice;
    public Text playerPrice;

	WaitForSeconds waitOneSec = new WaitForSeconds (1);




	void Awake(){
		instance = this;
		//playersToBuyIds = new List<int> () {0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18,19};
		playersToBuyIds = new List<int> () {0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12};
		CurrentPlayerPrice = DAO.GetPlayersPrice ( GetNumOfPurchasedPlayers() );
	}


	void Start () {
		if (onAction)
			return;
		
		onAction = true;

		StartCoroutine (StartDelay());
	}


	#if UNITY_EDITOR
	void Update(){
//		if (Input.GetKeyUp (KeyCode.R)) {
//			LockerRoom_Actions.instance.ShowRateUsPopup ();
//		}
	}
	#endif




	IEnumerator StartDelay(){
		yield return waitOneSec;
		InstantiatePurchasedPlayers ();
		//Debug.Log ("alon___________ Start () - gameobject: " + name + "  scene: " + SceneManager.GetActiveScene().name);
		CheckCoinsCollected ();

		//MainScreenUiManager.instance.PlayerUiAnimToggle (false);
		MainScreenUiManager.instance.PlayerNameAnimToggle (false);

		//Ads._.OnBench ();
	}




	public void InstantiatePurchasedPlayers(){
		if (DAO.PurchasedPlayers != "") {
			pp = DAO.PurchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries);
			int avalablePlayersCount = playersToBuyIds.Count;
			for (int i = 0; i < pp.Length; i++) {
				if (i < avalablePlayersCount) {
					foreach (int ptb in playersToBuyIds) {
						if (ptb == int.Parse (pp [i])) {
							AddNewPlayerImideately (int.Parse (pp [i]));
							break;
						}
					}
				}
			}
		}
		StartCoroutine (OnStartEvent());
	}




	public void AddNewPlayerImideately(int playerId){

		// instantiate the player:
		currentPlayer = (GameObject)Instantiate(PrefabManager.instanse.BenchPlayerPrefab , Vector3.zero , playerRotation);
		currentPlayer.transform.parent = playersParent;
		currentPlayerScript = currentPlayer.GetComponent<Player> ();
		currentPlayerScript.SetPlayerStructure (playerId);
		currentPlayerScript.placeInBench = ownedPlayersList.Count;
		currentPlayerScript.data.UpdateHealthCondition ();

		ownedPlayersList.Add (currentPlayer);
		playersToBuyIds.Remove (currentPlayerScript.PlayerID);
		//Debug.Log ("alon___________ AddNewPlayerImideately() -  playersToBuyIds.count: " + playersToBuyIds.Count);
		currentPlayer.SetActive (false);
		if (playersToBuyIds.Count <= 0) {
			fullPlayers = true;
			CrossSceneUIHandler.Instance.fullPlayers.SetActive (true);
		}
	}




	public GameObject GetOwnedPlayerByPlayerId(int id){
		foreach (GameObject pl in ownedPlayersList) {
			if (pl.GetComponent<Player> ().PlayerID == id) {
				return pl;
			}
		}
		return ownedPlayersList [0];
	}





	IEnumerator OnStartEvent(){
		//yield return waitOneSec;
		//Debug.Log ("alon___________ OnStartEvent() -  playersToBuyIds.count: " + playersToBuyIds.Count);
		if (ownedPlayersList.Count > 0) {
			//Debug.Log ("alon___________ OnStartEvent() -  pp.Length: " + pp.Length);
			if (PrefabManager.instanse.chosenPlayer.PlayerName == "") {
				//Debug.Log ("alon___________ OnStartEvent() -  pp[0]: " + pp [0]);
				currentPlayer = GetOwnedPlayerByPlayerId (int.Parse (pp [0]));
				currentPlayerIndex = 0;
				currentPlayerScript = currentPlayer.GetComponent<Player> ();
				//Debug.Log ("alon___________ OnStartEvent() -  currentPlayerScript name: " + currentPlayerScript.playerName);
				PrefabManager.instanse.ChoosePlayer (currentPlayerScript.PlayerID);
				currentPlayer.SetActive (true);
				currentPlayerScript.ChangeWalkSpeed (false);
				currentPlayerScript.WalkIn (false);
			} else {
				bool goodChosenPlayer = false;
				//Debug.Log ("alon___________ OnStartEvent() -  pp.Length: " + pp.Length);
				for (int i = 0 ; i < pp.Length ; i++) {
					//Debug.Log ("alon___________ OnStartEvent() - chosenPlayer.PlayerID: "+ PrefabManager.instanse.chosenPlayer.PlayerID.ToString () + " , pp[i]: " + pp [i]);
					if (PrefabManager.instanse.chosenPlayer.PlayerID.ToString () == pp [i]) {
						currentPlayer = GetOwnedPlayerByPlayerId (PrefabManager.instanse.chosenPlayer.PlayerID);
						currentPlayerIndex = i;
						goodChosenPlayer = true;
						currentPlayerScript = currentPlayer.GetComponent<Player> ();
						//PrefabManager.instanse.ChoosePlayer (currentPlayerScript.PlayerID);
						currentPlayer.SetActive (true);
						currentPlayerScript.ChangeWalkSpeed (false);
						currentPlayerScript.WalkIn (false);
						break;
					}
				}
				if (!goodChosenPlayer) {
					currentPlayer = GetOwnedPlayerByPlayerId (int.Parse (pp [0]));
					currentPlayerIndex = 0;
					currentPlayerScript = currentPlayer.GetComponent<Player> ();
					PrefabManager.instanse.ChoosePlayer (currentPlayerScript.PlayerID);
					currentPlayer.SetActive (true);
					currentPlayerScript.ChangeWalkSpeed (false);
					currentPlayerScript.WalkIn (false);
				}
			}
			yield return waitOneSec;

			if (Tutorial.IsNeedToShowSwipingHand) {
				MainScreenUiManager.instance.SwipingHand (true);
				LocalDataInterface.Instance.swipingHand++;
				if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.PRESS_BUY_PLAYER );
			}

			MainScreenUiManager.instance.PlayerNameAnimToggle ();
			MainScreenUiManager.instance.PlayerUiAnimToggle ();
			MainScreenUiManager.instance.LowerBarToggle (true);
//			MainScreenUiManager.instance.PlayerUiAnimToggle (true);
			//Debug.Log ("alon___________ OnStartEvent() -  PlayerUiAnimToggle()");

			MainScreenUiManager.instance.ArrowsToggle (2);
			MainScreenUiManager.instance.PerksScreen.SetActive (true);
			PerksScrn_Actions.Instance.OnBeforeShow ();
			yield return waitOneSec;
			MainScreenUiManager.instance.PerksScreen.SetActive (false);
			MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
			//yield return waitOneSec;
			CheckTutorialPopups ();
            CrossSceneUIHandler.Instance.energyTimer.enabled = true;
		} else {
			MainScreenUiManager.instance.LowerBarToggle (false);
			MainScreenUiManager.instance.PlusBtnToggle (true);
			CupsManager.Instance.SetFirstCupAndRun ();
			MainScreenUiManager.instance.ArrowsToggle (0);
			MainScreenUiManager.instance.StartPracticeRunBtnToggle (false);
			Debug.Log ("alon___________ Start Main screen with no players");
		}



		if (IsNeedToShowRateUsPopup && GameManager.Instance.needToShowRateUsPopup) {
			MainScreenUiManager.instance.ShowRateUsPopup ();
			GameManager.Instance.needToShowRateUsPopup = false;
		}

	}



	public void Swipe(int direction){
		if (!onAction) {
			if (selectMode == SelectMode.CHOOSE) {
				//onAction = true;
				SwipeBetweenPlayers (direction);
			}
			else if(selectMode == SelectMode.BUY)
				SwipePlayerPreview (direction);
		}
	}



	void SwipeBetweenPlayers(int direction){
		if (onAction) {
			return;
		}

		if (direction < 0 && currentPlayerIndex >=0) {
			if (currentPlayerIndex == 0 && fullPlayers) {
				onAction = true;
				if (currentPlayerIndex < ownedPlayersList.Count) {
					currentPlayer.GetComponent<Player> ().WalkOut (false);
					lastOwnedPlayer = currentPlayer;
					currentPlayer = null;
				}
				currentPlayerIndex = ownedPlayersList.Count - 1;
				PlayerEnter (ownedPlayersList [currentPlayerIndex], true);
				MainScreenUiManager.instance.ArrowsToggle (2);
				MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
				return;
			}

			onAction = true;
            if (currentPlayerIndex < ownedPlayersList.Count && currentPlayer) {
				currentPlayer.GetComponent<Player> ().WalkOut (false);
				lastOwnedPlayer = currentPlayer;
				currentPlayer = null;
			}
			currentPlayerIndex -= 1;

			if (currentPlayerIndex < 0) {
				//PlayerEnter (ownedPlayersList[0] , true , true);
				needToShowPlusBtn = true;
				MainScreenUiManager.instance.ArrowsToggle (-1);
				MainScreenUiManager.instance.StartPracticeRunBtnToggle (false);
				//onAction = false;
			} else{
				PlayerEnter (ownedPlayersList [currentPlayerIndex], true);
				MainScreenUiManager.instance.ArrowsToggle (2);
				MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
			}
		}
		else if (direction > 0 && currentPlayerIndex < ownedPlayersList.Count) {
			if (currentPlayerIndex == ownedPlayersList.Count - 1 && fullPlayers) {
				onAction = true;
				if (currentPlayerIndex > -1) {
					currentPlayer.GetComponent<Player> ().WalkOut (true);
					lastOwnedPlayer = currentPlayer;
					currentPlayer = null;
				}
				currentPlayerIndex = 0;
				PlayerEnter (ownedPlayersList[currentPlayerIndex] , false);
				MainScreenUiManager.instance.ArrowsToggle (2);
				MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
				return;
			}

			onAction = true;
			if (currentPlayerIndex > -1) {
				currentPlayer.GetComponent<Player> ().WalkOut (true);
				lastOwnedPlayer = currentPlayer;
				currentPlayer = null;
			}

			currentPlayerIndex += 1;

			if (currentPlayerIndex >= ownedPlayersList.Count) {
				//PlayerEnter (ownedPlayersList[0] , false , true);
				needToShowPlusBtn = true;
				MainScreenUiManager.instance.ArrowsToggle (1);
				MainScreenUiManager.instance.StartPracticeRunBtnToggle (false);
				//onAction = false;
			} else {
				PlayerEnter (ownedPlayersList[currentPlayerIndex] , false);
				MainScreenUiManager.instance.ArrowsToggle (2);
				MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
			}
		}

		MainScreenUiManager.instance.SwipingHand (false);

	}



	void PlayerEnter(GameObject player , bool mirror){
//		if (plusBtn) {
//			MainScreenUiManager.instance.PlusBtnToggle (true);
//			MainScreenUiManager.instance.PlayerNameAnimToggle ();
//		} else {
		MainScreenUiManager.instance.PlusBtnToggle (false);
		// Activate and bring the player in:
		currentPlayer = player;
//		if (currentPlayerScript != null) {
//			currentPlayerScript = player.GetComponent<Player> ();
//			PrefabManager.instanse.ChoosePlayer (currentPlayerScript.PlayerID);
//		}
		currentPlayerScript = player.GetComponent<Player> ();
		PrefabManager.instanse.ChoosePlayer (currentPlayerScript.PlayerID);

		currentPlayer.SetActive (true);

		currentPlayerScript.ChangeWalkSpeed (true);
		currentPlayerScript.WalkIn (mirror);

		MainScreenUiManager.instance.PlayerNameAnimToggle ();
		MainScreenUiManager.instance.PlayerUiAnimToggle ();

//		CrossSceneUIHandler.Instance.PlayerInfoUpdate();
//		MainScreenUiManager.instance.SetViewWithCurrentPlayer ();
	}



	public void OnPlayerStand(){   // driver by player enter animation event
		//Debug.Log ("alon___________ playerChooseManager - PreviewStandIdle() - start");

		if (selectMode == SelectMode.BUY) {
			if (previewPlayerToDelete != null) {
				Destroy (previewPlayerToDelete);
			}
			MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);
		} else {
			MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
		}
		MainScreenUiManager.instance.ArrowsToggle (2);
		onAction = false;

		//Debug.Log ("alon___________ playerChooseManager - PreviewStandIdle() - end");
	}





	public void OnPlayerWalkedOut(){ //drivern By animations event:
		//onAction = false;
		if (lastOwnedPlayer != null) {
			lastOwnedPlayer.SetActive (false);
			if (selectMode == SelectMode.CHOOSE) {
				lastOwnedPlayer = null;
			}
			if (needToShowPlusBtn) {
				MainScreenUiManager.instance.PlusBtnToggle (true);
				MainScreenUiManager.instance.PlayerNameAnimToggle ();
				//currentPlayerScript = null;
				needToShowPlusBtn = false;
				onAction = false;
			}
		}
		if (startingCupRun || startingFirstCupRun) {
			//GameManager.SwitchState (GameManager.GameState.CUP_RUN);
		} else if (startingPracticeRun) {
			GameManager.SwitchState (GameManager.GameState.INFINITY_RUN);
		}
	}





	public void SwitchToPreviewMode(){
		if (selectMode == SelectMode.BUY || fullPlayers)
			return;
		
		selectMode = SelectMode.BUY;
		MainScreenUiManager.instance.StartPracticeRunBtnToggle (false);
		if (currentPlayer != null) { // if we press on the add player btn
			lastOwnedPlayer = currentPlayer;
			lastOwnedPlayerScript = currentPlayerScript;
			lastOwnedPlayerScript.ChangeWalkSpeed (true);
			currentPlayer = null;
			StartCoroutine (RemoveOwnedPlayer());
		}
			
		GM_Input._.PlayerPreview = true;
		previewPlayerIndex = 0;
		PreviewPlayerEnter (playersToBuyIds [0], false);
		MainScreenUiManager.instance.PlusBtnToggle (false);
//		MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);
//		MainScreenUiManager.instance.ArrowsToggle (2);
	}





	IEnumerator RemoveOwnedPlayer(){
		if (lastOwnedPlayerScript != null) {
			lastOwnedPlayerScript.WalkOut (true);
		}
		yield return new WaitForSeconds (0.7f);
		if (lastOwnedPlayer != null) {
			lastOwnedPlayer.SetActive (false);
		}
	}




	void PreviewPlayerEnter(int index , bool mirror){
		// Instantiate the player:
		currentPlayer = (GameObject)Instantiate(PrefabManager.instanse.BenchPlayerPrefab , Vector3.zero , playerRotation);
		currentPlayerScript = currentPlayer.GetComponent<Player> ();
		currentPlayerScript.SetPlayerStructure (index);

		currentPlayer.transform.parent = playersToBuyParent;

		currentPlayerScript.ChangeWalkSpeed (true);
		currentPlayerScript.WalkIn (mirror);

		MainScreenUiManager.instance.PreviewPlayerCardSwapAnim ();
		MainScreenUiManager.instance.PlayerNameAnimToggle ();
		MainScreenUiManager.instance.PlayerUiAnimToggle ();      
        playerPrice.text = string.Format("{0:n0}",CurrentPlayerPrice);
	}



	void SwipePlayerPreview(int direction){
		if (onAction || playersToBuyIds.Count < 2)
			return;

		onAction = true;
		if (direction < 0) {

			currentPlayerScript.WalkOut (false);
			previewPlayerToDelete = currentPlayer;

			previewPlayerIndex -= 1;

			if (previewPlayerIndex < 0) {
				previewPlayerIndex = playersToBuyIds.Count - 1;
			}

			PreviewPlayerEnter (playersToBuyIds [previewPlayerIndex] , true);
		}
		else if (direction > 0) {

			currentPlayerScript.WalkOut (true);
			previewPlayerToDelete = currentPlayer;

			previewPlayerIndex += 1;

			if (previewPlayerIndex >= playersToBuyIds.Count) {
				previewPlayerIndex = 0;
			}

			PreviewPlayerEnter (playersToBuyIds [previewPlayerIndex] , false);
		}
	}



	public void BringSpecificPlayerToPreview(int index){
		if (index != currentPlayerScript.PlayerID) {
			currentPlayerScript.WalkOut (true);
			previewPlayerToDelete = currentPlayer;
			previewPlayerIndex = index;
			PreviewPlayerEnter (index , false);
		}
		MainScreenUiManager.instance.PlayerChooseScrnAnim (false);
	}








	public void CancelPlayerPreview(){
		selectMode = SelectMode.CHOOSE;

		StartCoroutine (RemovePlayerToWatch());
		GM_Input._.PlayerPreview = false;
		MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (false);
        //currentPlayerIndex = -1;
	}




	IEnumerator RemovePlayerToWatch(){
		
		currentPlayerScript.WalkOut (true);

		previewPlayerToDelete = currentPlayer;

		previewPlayerIndex = 0;
		yield return new WaitForSeconds (1f);

		Destroy (previewPlayerToDelete);
		previewPlayerToDelete = null;

		if (lastOwnedPlayer != null) {
			currentPlayer = lastOwnedPlayer;
			if (lastOwnedPlayerScript != null) {
				lastOwnedPlayerScript.ChangeWalkSpeed (false);
				currentPlayerScript = lastOwnedPlayerScript;
			}
			PlayerEnter (currentPlayer, false);
			lastOwnedPlayer = null;
			lastOwnedPlayerScript = null;
			//MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
			//MainScreenUiManager.instance.ArrowsToggle (2);
		} else {
//			if (currentPlayerIndex == -1) {
//				currentPlayerIndex = 0;
//			} else if (currentPlayerIndex == ownedPlayersList.Count) {
//				currentPlayerIndex = ownedPlayersList.Count - 1;
//			}
			//PlayerEnter (currentPlayer, false, true);
			MainScreenUiManager.instance.PlusBtnToggle (true);
			MainScreenUiManager.instance.PlayerNameAnimToggle ();
			MainScreenUiManager.instance.StartPracticeRunBtnToggle (false);
			if (currentPlayerIndex < 0) {
				MainScreenUiManager.instance.ArrowsToggle (-1);
			} else if (currentPlayerIndex >= ownedPlayersList.Count) {
				MainScreenUiManager.instance.ArrowsToggle (1);
			} else {
				MainScreenUiManager.instance.ArrowsToggle (0);
			}
			//Debug.Log ("alon_____ RemovePlayerToWatch - no last owned player");
		}
			

	}




	public void OnPlayerBuyClickedFromPreview(){
		if (onAction)
			return;

		int price = CurrentPlayerPrice;

		if (currentPlayerScript.playerNumber == 0) {
			price = DAO.GetCoachPrice();
		}

		if (price > DAO.TotalCoinsCollected) {
			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
			IAP.AdditionalIAPCallback += OnIAPMAdeAfterPlayerBuyClicked;
		} else {
			onAction = true;
			DAO.TotalCoinsCollected -= price;

			if (price > 0)
				GameAnalyticsWrapper.BuyNewPlayer(currentPlayerScript.playerName,price);
			
			GameManager.Instance.HandleRankScoreChange ();
			AudioManager.Instance.OnLevelUp ();

			BuyPlayer ();

			if (DAO.NumOfPurchasedPlayers == 0)
				GameAnalyticsWrapper.DesignEvent (GameAnalyticsWrapper.FLOW_STEP.PRESS_BUY_PLAYER);
			
			CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
		}

	}



	void OnIAPMAdeAfterPlayerBuyClicked(string sku){
		IAP.AdditionalIAPCallback -= OnIAPMAdeAfterPlayerBuyClicked;

		UnitedAnalytics.LogEvent ("IAP from players", sku, UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers);
	}





	public void BuyPlayer(){
		currentPlayer.transform.parent = playersParent;
		//currentPlayerScript = currentPlayer.GetComponent<Player> ();
		currentPlayerScript.placeInBench = ownedPlayersList.Count;
		currentPlayerScript.ChangeWalkSpeed (false);

		playersToBuyIds.Remove (currentPlayerScript.PlayerID);
		ownedPlayersList.Add (currentPlayer);
		PrefabManager.instanse.ChoosePlayer (currentPlayerScript.PlayerID);
		//OnPlayerBuyGeneral1 ();
		if (currentPlayerScript.DefaultPerk == Perk.PerkType.MANAGER || currentPlayerScript.DefaultPerk == Perk.PerkType.GAMBLER) {
			TimeEventsManager.Instance.SetPerksBonusTime ();
		}
		if (!DAO.Instance.IsPlayerAllReadyPurchased(currentPlayerScript.PlayerID.ToString())) {
			DAO.PurchasedPlayers += currentPlayerScript.PlayerID + "|";
			pp = DAO.PurchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries);
		}
		if (playersToBuyIds.Count < 1) {
			fullPlayers = true;
			CrossSceneUIHandler.Instance.fullPlayers.SetActive (true);
		}
			
		//OnPlayerBuyGeneral2 ();
		selectMode = SelectMode.CHOOSE;
		GM_Input._.PlayerPreview = false;

		CupsManager.Instance.ResetCupsAvailability ();

		UpdatePlayerPrices ();

		if (ownedPlayersList.Count == 1) {
			StartCoroutine(CupsManager.Instance.StartFirstRun ());
			startingFirstCupRun = true;
			currentPlayerScript.standType = 5;
		} else {
			currentPlayerScript.standType = 2;
		}
//		currentPlayerScript.standType = 2;
		currentPlayerScript.Stand ();
		currentPlayerIndex = ownedPlayersList.Count - 1;
		if (ownedPlayersList.Count >= 2) {
			DAO.BuySecondPlayerPopup1 = 1;
			DAO.BuySecondPlayerPopup2 = 1;
		} else if (ownedPlayersList.Count >= 3) {
			DAO.BuyThirdPlayerPopup = 1;
		}
		DAO.Instance.LOCAL.SaveAll ();

		UnitedAnalytics.LogEvent ("Player bought " + ownedPlayersList.Count.ToString (), "buy", UserData.Instance.userType.ToString (), DAO.PlayTimeInMinutes);

		if (ownedPlayersList.Count == 1) {
			UnitedAnalytics.LogEvent ("First player picked ", "purchased", currentPlayerScript.name);
		}else if (ownedPlayersList.Count == 3) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_we_have_a_club);
		}else if (ownedPlayersList.Count == 10) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_top_ten);
		}else if (ownedPlayersList.Count == 12) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_full_squad);
		}
		MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (false);
		GameAnalyticsWrapper.BuyNewPlayer(currentPlayerScript.playerName,CurrentPlayerPrice);

		if (isFreePlayer) {
			isFreePlayer = false;
		}

		MainScreenUiManager.instance.StartPracticeRunBtnToggle (true);
	}






	public void CheckFitnessOfSelectedPlayer(){
		if (currentPlayer != null) {
			currentPlayer.GetComponent<Player> ().data.UpdateHealthCondition ();
		}
	}





	public bool IsSelectedPlayerCanRunCup(){
		if (GameManager.PreselectedGameState == GameManager.GameState.INFINITY_RUN || GameManager.PreselectedGameState == GameManager.GameState.TUTORIAL_RUN) return true;

		if (GameManager.ActiveCupCompleted) {
			return (currentPlayer.GetComponent<Player> ().data.fitness >= GameManager.ActiveCompletedCup.cost);
		} else {
			return (currentPlayer.GetComponent<Player> ().data.fitness >= GameManager.ActiveCup.cost);
		}
	}




	public int GetNumOfPurchasedPlayers(){
		return DAO.PurchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries).Length;
	}




	public void UpdatePlayerPrices(){
		CurrentPlayerPrice = DAO.GetPlayersPrice ( GetNumOfPurchasedPlayers() );
	}







	public void StartCupRun ()
	{
		if (onAction == true)
			return;
		if (!DAO.Instance.IsRunsDataReady) {
			GameManager.Instance.PromtAppNoInternet ();
			return;
		}
		if (!IsSelectedPlayerCanRunCup ()) {
			MainScreenUiManager.instance.MidekKitsScreenToggle (true);
			return;
		}
		onAction = true;
		startingCupRun = true;
		if (PrefabManager.instanse.chosenPlayer.PlayerName == "") {
			PrefabManager.instanse.ChoosePlayer (currentPlayerScript.PlayerID);
		}
//		currentPlayerScript.standType = 5;
//		currentPlayerScript.Stand ();  // player walk out
        GameManager.SwitchState (GameManager.GameState.CUP_RUN);

		GameManager.BenchInPreselectedRunMode = false;
		MainScreenUiManager.instance.ScreenBlocker.SetActive (true);
	}



	public void StartPracticeRun ()
	{
		if (onAction == true)
			return;
		if (!DAO.Instance.IsRunsDataReady) {
			GameManager.Instance.PromtAppNoInternet ();
			return;
		}
		onAction = true;
		startingPracticeRun = true;
		currentPlayerScript.WalkOut (false);

		UnitedAnalytics.LogEvent ("Practice Started", "From Bench", UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers);
		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE (BI.FLOW_STEP.STARTED_PRACTICE);

		if (PrefabManager.instanse.chosenPlayer.PlayerName == "") {
			PrefabManager.instanse.ChoosePlayer (PlayerChooseManager.instance.currentPlayerScript.PlayerID);
		}
		MainScreenUiManager.instance.ScreenBlocker.SetActive (true);
	}





	public void EnergyCallback()
	{
		GameObject updatePlayer = null;
		if (currentPlayer != null)
			updatePlayer = currentPlayer;
		else if (lastOwnedPlayer != null)
			updatePlayer = lastOwnedPlayer;

		if (updatePlayer != null && updatePlayer.GetComponent<Player> () != null) {
			var pl = updatePlayer.GetComponent<Player> ();
			MainScreenUiManager.instance.energyToAdd = 1;
			pl.UpdateFitness ();
			MainScreenUiManager.instance.UpdateProgressBar ();

			pl.OnFitnessUpdate += MainScreenUiManager.instance.UpdateProgressBar;

			PlayerChooseManager.instance.CheckFitnessOfSelectedPlayer ();
		}
		else
			Debug.Log("Ron_______________EnergyCallback Error");
	}






	public void OnPerkBuyClicked(Perk perk){

		// Get Current Perk Prize
		Player pl = currentPlayer.GetComponent<Player> ();
		int numOfPurchasedPlayer = pl.data.GetNumOfPurchasedPerks ();
		int perkPrize = 0;

		if (numOfPurchasedPlayer == 1) {
			perkPrize = DAO.Settings.Perk2Cost;
		} else {
			perkPrize = DAO.Settings.Perk3Cost;
		}
			
		// Check If There is enouth money
		if (DAO.TotalCoinsCollected >= perkPrize) {
			onAction = true;
			DAO.TotalCoinsCollected -= perkPrize;
			CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

			AppsFlyerManager._.ReportVirtualEconomy ("Skill", perk.type.ToString (), perkPrize.ToString(), "1");
			GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, perkPrize, GameAnalyticsWrapper.BUY_UPGREADE, "SkillBought");

			if (numOfPurchasedPlayer == 1) {
				pl.data.Perk_2 = perk;
				NativeSocial._.ReportAchievment (SocialConstants.achievement_in_shape);
			} else {
				pl.data.Perk_3 = perk;
			}

			pl.Save();
			DAO.Instance.LOCAL.SaveAll();

			PerksScrn_Actions.Instance.OnBeforeShow();

			if (perk.type == Perk.PerkType.MANAGER || perk.type == Perk.PerkType.GAMBLER) {
				TimeEventsManager.Instance.SetPerksBonusTime();
			}

			perkBought = true;

			AudioManager.Instance.OnLevelUp ();

			UnitedAnalytics.LogEvent ("Perk Upgraded or Added", "added", perk.type.ToString (), perk.level);

		} else {
			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
			IAP.AdditionalIAPCallback += OnIAPMadeAfterPerkClicked;
		}
	}



	public void OnPerkUpgradeClicked(Perk perk){

		// Get Current Perk Prize
		Player pl = currentPlayer.GetComponent<Player> ();
		int perkPrize = 0;

		if (perk.level == 1) {
			perkPrize = DAO.Settings.PerkUpgrade2Cost;
		} else {
			perkPrize = DAO.Settings.PerkUpgrade3Cost;
		}
			
		// Check If There is enouth money
		if (DAO.TotalCoinsCollected >= perkPrize) {
			onAction = true;
			DAO.TotalCoinsCollected -= perkPrize;
			CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

			GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, perkPrize, GameAnalyticsWrapper.BUY_UPGREADE, "SkillBought");

			pl.data.LevelUpPerk( perk.type );

			pl.Save();
			DAO.Instance.LOCAL.SaveAll();

			PerksScrn_Actions.Instance.OnBeforeShow();

			perkBought = true;

			AudioManager.Instance.OnLevelUp ();

		} else {
			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
			IAP.AdditionalIAPCallback += OnIAPMadeAfterPerkClicked;
		}
	}




	void OnIAPMadeAfterPerkClicked(string sku){
		IAP.AdditionalIAPCallback -= OnIAPMadeAfterPerkClicked;
		UnitedAnalytics.LogEvent ("IAP from Perks", sku, UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers);
	}





	public void PerkGlowFX(){
		if (perkBought) {
			perkBought = false;
			onAction = true;
			StartCoroutine ("PerkGlowFxCoroutine");
		} else {
			onAction = false;
		}
	}



	public GameObject starGlowParent;
	public Color sparksColor;
	public Transform sparkTransform1;
	public Transform sparkTransform2;
	public Transform sparkTransform3;
	public Transform sparkTransform4;
	public Transform sparkTransform5;
	WaitForSeconds waitPerkSparksFX = new WaitForSeconds(0.25f);



	IEnumerator PerkGlowFxCoroutine(){
		currentPlayer.GetComponent<Player> ().standType = 4;
		currentPlayer.GetComponent<Player> ().Stand ();
		onAction = true;
		starGlowParent.SetActive (true);
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform1.position , sparksColor , 30 , 250 , 250 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform2.position , sparksColor , 30 , 250 , 250 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform3.position , sparksColor , 30 , 250 , 250 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform4.position , sparksColor , 30 , 250 , 250 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform5.position , sparksColor , 30 , 250 , 250 , false);

		yield return waitOneSec;
		yield return waitOneSec;
		onAction = false;
		yield return waitOneSec;
		CrossSceneUIHandler.Instance.sparclesParticleSystemObj.SetActive (false);
		starGlowParent.SetActive (false);

	}


	public void StopPerkGlowFX(){
		StopCoroutine ("PerkGlowFxCoroutine");
		starGlowParent.SetActive (false);
		onAction = false;
		CrossSceneUIHandler.Instance.sparclesParticleSystemObj.SetActive (false);
	}




	int minCoinsAmountToThirdPlayer = 500;


	void CheckTutorialPopups(){

		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE ( BI.FLOW_STEP.LOCKER_ROOM_ENTERED );

		if (tutPopupsDisabled || IsNeedToShowRateUsPopup)	return;

		if (Tutorial.IsNeedToShowHealPopup && currentPlayer.GetComponent<Player> ().data.fitness <= 1) { //heal popup:
			MainScreenUiManager.instance.LowEnergyPopup (true);
			DAO.HealPopoup = 1;
		}
		else if (Tutorial.IsNeedToShowBuySecondPlayer1Popup && DAO.TotalCoinsCollected >= CurrentPlayerPrice) { //buy second player popup 1:
			MainScreenUiManager.instance.RecruitSecondPopup (true); // recruit
			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE ( BI.FLOW_STEP.ELIGIBLE_FOR_2ND_PLAYER );
		}
		else if (Tutorial.IsNeedToShowBuySecondPlayer2Popup && DAO.TotalCoinsCollected >= CurrentPlayerPrice * 1.25f) { //buy second player popup 2:
			MainScreenUiManager.instance.RecruitSecondPopup (true);
		}
		else if (ownedPlayersList.Count == 2 && Tutorial.IsNeedToShowBuyThirdPlayerPopup && DAO.TotalCoinsCollected < CurrentPlayerPrice && DAO.TotalCoinsCollected + minCoinsAmountToThirdPlayer >= CurrentPlayerPrice) { //buy third player popup:
			MainScreenUiManager.instance.RecruitThirdPopup (true , CurrentPlayerPrice - DAO.TotalCoinsCollected); // recruit another
		}
		else if(Tutorial.IsNeedToShowPlayPracticePopup){
			MainScreenUiManager.instance.PracticePopup (true);
		}
		else if(Tutorial.IsNeedToShowPlayCupPopup && !GameManager.BenchInPreselectedRunMode){
			MainScreenUiManager.instance.CupPopup (true);
		}


	}


	public static bool IsNeedToShowRateUsPopup{   
		get{ 
			return (DAO.RateUsPopup == 0 && DAO.Instance.settings.showRateUs == 1 && (DateTime.Now - DAO.RateUsPopupDate).TotalMinutes >= DAO.RateUsTimeFactor);
			//return (DAO.RateUsPopup == 0 && (DateTime.Now - DAO.RateUsPopupDate).TotalMinutes >= DAO.RateUsTimeFactor);
			//return true;
		}
	}



	public void CheckCoinsCollected(){
		if (DAO.TotalCoinsCollected > 1000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_need_bigger_bag);
		}

		if (DAO.TotalCoinsCollected > 5000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_money_in_the_bank);
		}

		if (DAO.TotalCoinsCollected > 10000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_r_for_rich);
		}

		if (DAO.TotalCoinsCollected > 15000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_m_for_millioner);
		}

		if (DAO.TotalCoinsCollected > 25000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_fcbillion);
		}

		if (DAO.TotalCoinsCollected > 50000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_rollin_in_the_deep);
		}
	}



}
