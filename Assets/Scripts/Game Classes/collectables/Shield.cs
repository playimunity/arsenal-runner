﻿using UnityEngine;
using System.Collections;

public class Shield : Collectable {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider col){
        if (col.transform.gameObject.tag == "Player") {
            ParticleCollectGenerator.instance.PlayParticle(transform.position , transform.eulerAngles);
            RunManager.OnSheildCollected();
            //RunManager.instance.ActivateSpeedBoost(false);
            AudioManager.Instance.OnPowerUpPick ();
            gameObject.SetActive (false);
        }
    }
}
