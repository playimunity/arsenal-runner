﻿using UnityEngine;
using System.Collections;

public class CharacterAnimationsEvents : MonoBehaviour {

	public Player player;


	public void JumpStartEvent(){
		//PlayerController.instance.JumpStartEvent ();
	}

	public void JumpEndEvent(){
		//PlayerController.instance.JumpEndEvent();
	}

	public void JumpColliderEvent(){
		//PlayerController.instance.JumpColliderEvent();
	}

	public void SlideEndEvent(){
		PlayerController.instance.SlideEndEvent();
	}

	public void StartRun(){
		PlayerController.instance.StartRun ();
	}

//	public void KickEnd(){
//		PlayerController.instance.KickEnd ();
//	}

	public void StumbleEnd(){
		//PlayerController.instance.StumbleEnd ();
	}

//	public void OnPlayerStand(){
//		PlayerChooseManager.instance.OnPlayerStand ();
//	}

	public void CallBallHandler(){
		PlayerController.instance.CallBallHandler ();
	}

//	public void Stand(){
//		player.Stand ();
//	}

//	public void SitIdle(){
//		player.SitIdle ();
//	}

	public void StartRunAfterWalkOut(){
		PlayerChooseManager.instance.OnPlayerWalkedOut ();
	}

	public void UnPause(){
		GameManager.RequestUnpause ();
	}

	public void DeactivateObject(){
		gameObject.SetActive (false);
	}



	public void BackFromHeader(){
		PlayerController.instance.BackFromHeader ();
	}
}
