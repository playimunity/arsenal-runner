﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CupWonScreen_Actions : MonoBehaviour {

	public static CupWonScreen_Actions Instance;

	
	public Text greatWorkUserName;
	//public Text CupName;
	public Text CupReward;
	public Text sendGiftBTn;
	//public Text Prize;

	public GameObject ConnectToFB_BTN;
	public GameObject SendGift_BTN;


	void Awake(){
		Instance = this;
	}

	void Start(){
		sendGiftBTn.text = DAO.Language.GetString("send-gift-btn");
	}

	void OnEnable(){
		Show ();
		Invoke ("CloseCoinsCounter", 0.5f);
	}

	void OnDisable(){
		GM_Input._.CupWon = false;
	}

	public void Show(){
		greatWorkUserName.text = DAO.Language.GetString("great-work") + " " +  UserData.Instance.UserName;
		//CupName.text = GameManager.ActiveCup.name;
		CupReward.text = ""+ GameManager.ActiveCup.prize.ToString() + " Coins";
		//Prize.text = GameManager.ActiveCup.prize.ToString();

		if (!true) { // Connected to facebook
			ConnectToFB_BTN.SetActive (true);
		} else {
			SendGift_BTN.SetActive(true);
		}

//		LockerRoom_Actions.instance.WinScrnToggle ();

	}

	public void Hide(){
		MainScreenUiManager.instance.WinScrnToggle (false);
	}

	void CloseCoinsCounter(){
		CrossSceneUIHandler.Instance.hideCoinsCounter (true);
	}
}
