using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;
using UnityEngine.UI;
using Facebook.Unity;
using RTLService;
using Amazon;

using System.Collections.Generic;


public class DAO : MonoBehaviour {

	float SAVE_LOCAL_DATA_EVERY_SECONDS = 30f;

	public static event Action onDataReady;
	public static DAO Instance;
	bool IsServerDataReady = false;
	public bool IsAllDataReady = false;
	public bool IsRunsDataReady = false;
	public bool InitFacebookWithDDB = false;
	public bool InitWhaleWithDDB = false;
	public enum SaveMePriceTag {CUP, ENDLESS};

	public LocalDataInterface LOCAL;
	public ServerDataInterface SERVER;
	WaitForSeconds DataSaveInterval;
	public static char[] char_separator;

	public Text console;
	public bool NO_INTERNET = false;
	public int teamMaxSize = 13;
	public string stroeAppLink = "http://m.onelink.me/4f762775";
	public string GoogleStore = "GPlay";
	public string IOSStore = "IOS";
	public static string currency = "";
	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log("DAO | "+msg);

		//DAO.Instance.writeToConsole (msg);
	}

	public void writeToConsole(string msg){
		console.text += msg+"\n";
	}

	void Awake () {
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("DAO");
		Instance = this;
		LOCAL.Init();
		SERVER.onVersionRecieved += OnDataVersionRecieved;
		SERVER.onDataRecieved += OnDataRecieved;
		SERVER.onDataRunsRecieved += onDataRunsRecieved;
		SERVER.onError += OnAjaxError;
		char_separator = new char[1]{ '|' };
		UIManager.OnChangeScene += OnChangeScene;
//		SERVER.GetData ();

	}

	void Start(){
		SERVER.GetVersion ();
		DataSaveInterval = new WaitForSeconds (SAVE_LOCAL_DATA_EVERY_SECONDS);
		//StartCoroutine ( SaveLocalData() );
		//initVersion = LOCAL.dataVersion;
		teamMaxSize = 13;
	}

	void OnDataVersionRecieved(string version){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("OnDataVersionRecieved start");
		Preloader.Instance.FillLoadingBar (4,"DataVersionRecieved");
		int iVersion = 0;
		try {
			iVersion = int.Parse (version);
		}
		catch (Exception e)
		{
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("OnDataVersionRecieved exception "+e.Message);
			Debug.Log ("OnDataVersionRecieved invalid version id: " + e.Message);
		}
		Log ("Comparing 'Loacal setup data' Version ("+LOCAL.dataVersion+") with 'Server setup data' Version ("+version+")");
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("OnDataVersionRecieved local version:" + LOCAL.dataVersion.ToString() + " iVersion:" + iVersion);
		if (LOCAL.dataVersion < iVersion || AppSettings._.ALWAYS_LOAD_DATA_FROM_SERVER || (!IsLocalDataExist())) {
			Log ("The versions are different or local data does not exist, need to update setup");
			LOCAL.dataVersion = iVersion;
			//Preloader.Instance.PrintStatus ("DataVersion recieved by remote");
			if (LOCAL.dataRunsVersion > 0) {
				SERVER.prepareDataRuns( LOCAL.dataRuns );
				IsRunsDataReady = true;
			}
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("OnDataVersionRecieved bring new data!");
			SERVER.GetData();
		} else {
			Log ("The versions are the same. Loading Local setup");
			SERVER.prepareData( LOCAL.data );
			SERVER.prepareDataRuns( LOCAL.dataRuns );
			IsRunsDataReady = true;
			//Preloader.Instance.PrintStatus ("DataVersion recieved by remote");
			ServerDataReady ();
		}
		Preloader.Instance.FillLoadingBar (5,"DataRecieved");

	}

	void OnDataRecieved(string data){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("OnDataRecieved:");
		//Preloader.Instance.PrintStatus ("DataRecieved is fired");
		Log ("'Setup Recieved' event fired");
		//LOCAL.data = data;
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("OnDataRecieved: before save");
		PlayerPrefs.SetString ("data", data);
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("OnDataRecieved: after save");
		Log ("Finished to download data" + data);
		Log ("Finished to download data" + DateTime.Now.ToString());

		LOCAL.SaveAll ();
		ServerDataReady ();
	}
		
	void onDataRunsRecieved(string data){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("onDataRunsRecieved end:");
		Log ("'Setup Recieved' event fired");
		StartCoroutine(CheckStateForDataRunsUpdate(data));
		//LOCAL.data = data;
		//PlayerPrefs.SetString ("data", data);
		//LOCAL.SaveAll ();
		//ServerDataReady ();

	}

	IEnumerator CheckStateForDataRunsUpdate(string data)
	{
		yield return new WaitUntil (() => (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM)
		|| (GameManager.curentGameState == GameManager.GameState.PRELOADER));

		if (isNeedToGetDataRuns())
	    {
				//Debug.Log ("Alon ________________ CheckStateForDataRunsUpdate updating LocalDataRuns"); 
				PlayerPrefs.SetString ("dataRuns", data);
			    LOCAL.dataRunsVersion = LOCAL.dataVersion;
			    PlayerPrefs.SetInt ("dataRuns_version", LOCAL.dataRunsVersion);
				//Log ("Finished to download Runs" + data);
				//Log ("Finished to download Runs data!!! " + DateTime.Now.ToString ());
				IsRunsDataReady = true;
			}
	}

	void OnAjaxError(string err){
		Log ("Unable to connect the server! loading local game setings");
		SERVER.prepareData( LOCAL.data );
		SERVER.prepareDataRuns( LOCAL.dataRuns );
		IsRunsDataReady = true;
		ServerDataReady ();

	}

	void ServerDataReady(){
		IsServerDataReady = true;
		Log("Server Data Ready!");
		AllDataReady ();
	}

	public void OnFBReady(){
		AllDataReady ();
	}

	public void DDBDataReady(){
		if (IsAllDataReady) return;

		AllDataReady ();
	}

	void AllDataReady(){
		//if (IsAllDataReady)	return;
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("AllDataReady start:");
		if (!IsServerDataReady) {
			Log ("AllDataReady Called, but ServerData is not ready");
			return;
		}
		if (!FBModule.Instance.Ready) {
			Log ("AllDataReady Called, but FBModule is not ready");
			return;

		if (!FBModule.Instance.Ready) {
			Log ("AllDataReady Called, but FBModule is not ready");
			return;
		} 
/*
else {

			if (FB.IsLoggedIn && !DDB._.DataReady && !NO_INTERNET) {
				Log ("AllDataReady Called, FB is Conneced, but DDB Data is not ready");
				return;
			}
*/
		}

		// -------------------------
		Preloader.Instance.FillLoadingBar (6,"AllDataLoaded1");
		AppLaunchCount++;

		Log ("All Data Ready Final calling GameManager.");

		IsAllDataReady = true;
		language = new Language (SERVER.strings);
		settings = new Settings (SERVER.settings);

		SERVER.DATA = null;

		IAP.Pack_1_Value = DAO.Settings.Market [0].AsInt;
		IAP.Pack_2_Value = DAO.Settings.Market [1].AsInt;
		IAP.Pack_3_Value = DAO.Settings.Market [2].AsInt;
		IAP.Pack_4_Value = DAO.Settings.Market [3].AsInt;
		IAP.Pack_5_Value = DAO.Settings.Market [7].AsInt;
		IAP.Pack_6_Value = DAO.Settings.Market [8].AsInt;
        IAP.Pack_starter_OldValue = DAO.Settings.StarterDealOldValue;
		IAP.Pack_starter_Value = DAO.Settings.Market [4].AsInt;
		IAP.Pack_fcb_Value = DAO.Settings.Market [5].AsInt;
		IAP.Pack_fcbmega_Value = DAO.Settings.Market [6].AsInt;
		if (onDataReady != null) onDataReady ();
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("AllDataReady end");
		StartCoroutine (GameManager.Instance.LoadUIPT2 ());
		//GameManager.Instance.CompleteOnallDataLoaded();


		if (isNeedToGetDataRuns()) {
			SERVER.GetDataRuns ();
		}
	}

	private bool isNeedToGetDataRuns()
	{
		// do not change check LOCAL.dataRunsVersion <  LOCAL.dataVersion - the dataVersion represent the new version arrived
		return (LOCAL.dataRunsVersion <  LOCAL.dataVersion || LOCAL.dataRunsVersion == 0 || AppSettings._.ALWAYS_LOAD_DATA_FROM_SERVER || (!IsLocalDataExist ()));
	}

#if UNITY_EDITOR
	void Update () {

		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKeyDown (KeyCode.D)) {
			PlayerPrefs.DeleteAll();
			LOCAL.BuildDefaultData();
			DAO.Log ("All local data deleted!");
		}

		if (Input.GetKey (KeyCode.LeftAlt)  &&  Input.GetKeyDown (KeyCode.T)) {
			LocalDataInterface.Instance.TempResetTutPopups ();
			DAO.Log ("Tut Popups local data deleted!");
		}
	}
#endif

	//public List<Player> own = new List<Player>();

	void OnApplicationQuit() {
		LOCAL.SaveAll();



	}

	void OnApplicationFocus(bool focusStatus) {
		if(!focusStatus){
			LOCAL.SaveAll();


			GetPlayersConditions ();

		}
	}


	public void GetPlayersConditions(){   // for exit/closed app while has hurt players analytic event:

		bool hasHurtPlayer = false;
		bool hasHealthyPlayer = false;

		string[] pp = DAO.PurchasedPlayers.Split (char_separator, System.StringSplitOptions.RemoveEmptyEntries);

		for (int i=0; i<pp.Length; i++) {

			string[] pd = GetPlayerData (int.Parse (pp [i])).Split (char_separator, System.StringSplitOptions.RemoveEmptyEntries);


			if (pd [3] == "WOUNDED") {
				hasHurtPlayer = true;
			} else {
				hasHealthyPlayer = true;
			}
		}

		if (hasHurtPlayer && hasHealthyPlayer) {
			UnitedAnalytics.LogEvent ("lose focus app while hurt", "all players hurt - No", UserData.Instance.userType.ToString () , DAO.TotalCoinsCollected);
		} else if (hasHurtPlayer && !hasHealthyPlayer) {
			UnitedAnalytics.LogEvent ("lose focus app while hurt", "all players hurt - Yes", UserData.Instance.userType.ToString () , DAO.TotalCoinsCollected);
		}

	}


	IEnumerator SaveLocalData(){
		yield return DataSaveInterval;

		LOCAL.SaveAll ();
		StartCoroutine ( SaveLocalData() );
	}

	void OnChangeScene(){
		GM_Input._.Lock = true;
		LOCAL.SaveAll ();
	}

	bool IsLocalDataExist()
	{
		return (!String.IsNullOrEmpty (LOCAL.data) && LOCAL.data.Trim () != "[]" &&
		!String.IsNullOrEmpty (LOCAL.dataRuns) && LOCAL.dataRuns.Trim () != "[]");
	}
	
// ===============================================================================================================================
// =============================================================================================================================== Server Variables
// ===============================================================================================================================


	public Language language;
	public Settings settings;

	[Serializable]
	public class Language{
		public static JSONNode lang;
		public static string Loading;
		public static string Rank_A;
		public static string Rank_B;
		public static GoalDescription RunGoal_CollectCoins;
		public static GoalDescription RunGoal_CollectCoinsHard;
		//public static GoalDescription RunGoal_CompleteRun;
		public static GoalDescription RunGoal_KickBallAtSpaceShip;
		public static GoalDescription RunGoal_KickBallAtSpaceShipHard;
		//public static GoalDescription RunGoal_CollectEnergyDrinks;
		public static GoalDescription RunGoal_DontGetHit;
		//public static GoalDescription RunGoal_TackleEnemies;
		public static GoalDescription RunGoal_CompleteRunInLessXSec;
		public static GoalDescription RunGoal_CompleteRunInLessXSecHard;
		public static GoalDescription RunGoal_Tutorial;
		public static GoalDescription RunGoal_Test;

		public static JSONNode Perks;
		public static JSONNode PreloaderTips;


		public Language(JSONNode lang){
			DAO.Language.lang = lang;
            try{
			Loading 							   	= Utils.FixRTLString(lang["loading"].Value);
			Rank_A									= Utils.FixRTLString(lang["rank_a"].Value);
			Rank_B									= Utils.FixRTLString(lang["rank_b"].Value);

			RunGoal_CollectCoins					= new GoalDescription(lang["rgd_cc"]);	
			RunGoal_CollectCoinsHard				= new GoalDescription(lang["rgd_cc"]);	//needs to be added to the php
			//RunGoal_CompleteRun 					= new GoalDescription(lang["rgd_cr"]);
			RunGoal_KickBallAtSpaceShip 			= new GoalDescription(lang["rgd_kb"]);
			RunGoal_KickBallAtSpaceShipHard 		= new GoalDescription(lang["rgd_kb"]);  //needs to be added to the php
			//RunGoal_CollectEnergyDrinks			= new GoalDescription(lang["rgd_ce"]);
            RunGoal_DontGetHit 						= new GoalDescription(lang["rgd_cr"]);
			//RunGoal_TackleEnemies 				= new GoalDescription(lang["rgd_te"]);
			RunGoal_CompleteRunInLessXSec 			= new GoalDescription(lang["rgd_ctil"]);
			RunGoal_CompleteRunInLessXSecHard 		= new GoalDescription(lang["rgd_ctil"]);   //needs to be added to the php
			RunGoal_Tutorial 						= new GoalDescription(lang["rgd_tut"]);
			RunGoal_Test 							= new GoalDescription(lang["rgd_tst"]);

			Perks									= lang["perks"];
			PreloaderTips							= lang["preloader-tips"];
            }
            catch(Exception e)
            {

            }
		}

		public static string GetString(string placeholder){
			try {
			return Utils.FixRTLString(lang[placeholder]);
			} catch (Exception e) {	
				Debug.Log ("GetString invalid placeholder: " +placeholder+"exception:"+ e.Message);
				return "";
			}
		}

		public static string GetRandomTip(){
			return Utils.FixRTLString( PreloaderTips [UnityEngine.Random.Range (0, PreloaderTips.Count - 1)].Value );
		}
	}

	[Serializable]
	public class Settings{

		
		public static int 
			TimeToEngagedUserA,
			TimeToEngagedUserB,
			TimeToEngagedUserC,
			TimeToEngagedUserD,
			DefaultAmountOfHitsToDie,
			DefaultMaxFitness,
			MagnetActiveTime,
			SheildActiveTime,
			SpeedBoostActiveTime,
			X2ActiveTime,
			Perk2Cost,
			Perk3Cost,
			PerkUpgrade2Cost,
			PerkUpgrade3Cost,
			RetryRunCost,
			TutorialBonus,
			SecondsFor1Fitness,
			NumOfSectionsToIncreaseSpeed,
			IOSUserTypeForSaveMeByVideo,
			NumberOfAdsForSaveMeCup,
			NumberOfAdsForSaveMeEndless,
		    TimeToMonitorLog,
		    DebugStageOn,
		    ShowRateUs,
            StarterDealOldValue;

		public static string TimeToClaimGift
		{
		get{
				return LocalDataInterface.Instance.timeToClaimGift;
		}
		set{
				LocalDataInterface.Instance.timeToClaimGift = value;
			}
		}
		public static string EnergyInterval;

		public int 
			timeToEngagedUserA,
			timeToEngagedUserB,
			timeToEngagedUserC,
			timeToEngagedUserD,
			defaultAmountOfHitsToDie,
			defaultMaxFitness,
			magnetActiveTime,
			godModeActiveTime,
			speedBoostActiveTime,
			x2ActiveTime,
			perk2Cost,
			perk3Cost,
			perkUpgrade2Cost,
			perkUpgrade3Cost,
			retryRunCost,
			tutorialBonus,
			secondsFor1Fitness,
			numOfSectionsToIncreaseSpeed,
			iOSUserTypeForSaveMeByVideo,
			numberOfAdsForSaveMeCup,
			numberOfAdsForSaveMeEndless,
		    timeToMonitorLog,
		    debugStageOn,
		    showRateUs,
            starterDealOldValue;

		public string timeToClaimGift,
		energyInterval;




		public static float 
			PlayerRunSpeed,
			PlayerMaxSpeed,
			SpeedIncreaseStep,
			PlayerGodModeAdditionalSpeed,
			PlayerJumpHeight,
			BallShotDistance,
			SpeedBoostFactor,
			FitnessPerMinute;


		public float 
			playerRunSpeed,
			playerMaxSpeed,
			speedIncreaseStep,
			playerGodModeAdditionalSpeed,
			playerJumpHeight,
			ballShotDistance,
			speedBoostFactor,
			fitnessPerMinute;



		public static JSONNode PlayerLevelsSettings;
		public static JSONNode PlayerPrices;
		public static JSONNode MedikKits;
		public static JSONNode SpecialOffers;
		public static JSONNode Market;
		public static JSONNode AvailableLanguages;
		public static JSONNode HidePlayers;
		public static JSONNode SaveMePrices;
		public static JSONNode SaveMePricesEndless;
		public static JSONNode DevicesWithoutAds;
		public static JSONNode OSWithoutAds;
		public static JSONNode DailyGiftsTable1;
		public static JSONNode DailyGiftsTable2;

		//----------------------


		public Settings(JSONNode settings){
			TimeToEngagedUserA 										= timeToEngagedUserA 									= settings["time_to_engaged_user_a"].AsInt;
			TimeToEngagedUserB 										= timeToEngagedUserB 									= settings["time_to_engaged_user_b"].AsInt;
			TimeToEngagedUserC 										= timeToEngagedUserC 									= settings["time_to_engaged_user_c"].AsInt;
			TimeToEngagedUserD 										= timeToEngagedUserD 									= settings["time_to_engaged_user_d"].AsInt;
			DefaultAmountOfHitsToDie 								= defaultAmountOfHitsToDie 								= settings["defoult_amount_of_hits_to_die"].AsInt;
			DefaultMaxFitness 										= defaultMaxFitness 									= settings["default_max_fitness"].AsInt;
			PlayerRunSpeed 											= playerRunSpeed 										= settings["run_speed"].AsFloat;
			PlayerMaxSpeed 											= playerMaxSpeed 										= settings["max_run_speed"].AsFloat;
			SpeedIncreaseStep										= speedIncreaseStep										= settings["speed_increase_step"].AsFloat;
			PlayerGodModeAdditionalSpeed							= playerGodModeAdditionalSpeed							= settings["godmode_additional_speed"].AsFloat;	
			PlayerJumpHeight 										= playerJumpHeight 										= settings["jump_height"].AsFloat;
			BallShotDistance 										= ballShotDistance 										= settings["ball_shot_distance"].AsFloat;
			FitnessPerMinute										= fitnessPerMinute										= settings["fitness_per_minute"].AsFloat;
			SpeedBoostFactor										= speedBoostFactor										= settings["speedboost_factor"].AsFloat;

			MagnetActiveTime										= magnetActiveTime										= settings["magnet_active_time_in_seconds"].AsInt;
			SheildActiveTime										= godModeActiveTime										= settings["godmode_active_time_in_seconds"].AsInt;
			SpeedBoostActiveTime									= speedBoostActiveTime									= settings["speedboost_active_time_in_seconds"].AsInt;
			X2ActiveTime											= x2ActiveTime											= settings["x2_active_time_in_seconds"].AsInt;
			RetryRunCost											= retryRunCost											= settings["retry_run_cost"].AsInt;
			SecondsFor1Fitness										= secondsFor1Fitness									= settings["secondsFor1Fitness"].AsInt;

			Perk2Cost												= perk2Cost												= settings["perks_pricing"]["perk_2_cost"].AsInt;
			Perk3Cost												= perk3Cost												= settings["perks_pricing"]["perk_3_cost"].AsInt;
			PerkUpgrade2Cost										= perkUpgrade2Cost										= settings["perks_pricing"]["upgrade_2_cost"].AsInt;
			PerkUpgrade3Cost										= perkUpgrade3Cost										= settings["perks_pricing"]["upgrade_3_cost"].AsInt;
			TutorialBonus											= tutorialBonus											= settings["tutorial_bonus"].AsInt;
			NumOfSectionsToIncreaseSpeed							= numOfSectionsToIncreaseSpeed							= settings["number_of_sections_to_speed_increase"].AsInt;
			TimeToMonitorLog 										= timeToMonitorLog 									    = settings["time_to_monitor_log"].AsInt;
			DebugStageOn    										= debugStageOn 									        = settings["debug_stage_on"].AsInt;
			ShowRateUs    										    = showRateUs 									        = settings["show_rate_us"].AsInt;

			TimeToClaimGift											= timeToClaimGift										= settings["time_to_claim_free_gift"].ToString().Replace("\"","");
			IOSUserTypeForSaveMeByVideo								= iOSUserTypeForSaveMeByVideo							= settings["ios_user_type_for_saveme_by_video"].AsInt;
			NumberOfAdsForSaveMeCup									= numberOfAdsForSaveMeCup								= settings["number_of_ads_for_saveme_cup"].AsInt;
			NumberOfAdsForSaveMeEndless								= numberOfAdsForSaveMeEndless							= settings["number_of_ads_for_saveme_endless"].AsInt;
			EnergyInterval											= energyInterval										= settings["energy_interval"].ToString().Replace("\"","");
            StarterDealOldValue                                     = starterDealOldValue                                   = settings["specail_offers"]["starter-pack"]["old-value"].AsInt;



			PlayerLevelsSettings									= settings["player_levels"];
			PlayerPrices											= settings["player_prices"];
			MedikKits												= settings["medikkits"];
			AvailableLanguages										= settings["available_languages"];
			Market													= settings["market"];
			SpecialOffers											= settings["specail_offers"];
			HidePlayers											    = settings["hide_players"];
			SaveMePrices											= settings["save_me_prices"];
			SaveMePricesEndless										= settings["save_me_prices_endless"];
			DevicesWithoutAds										= settings["device_without_ad_list"];
			OSWithoutAds											= settings["operating_system_without_ad_list"];
			DailyGiftsTable1										= settings["table_daily_gifts1"];
			DailyGiftsTable2										= settings["table_daily_gifts2"];


		}
	}
		

	public static JSONNode getCupRunByIndex(int index){
		return ServerDataInterface.Instance.cup_runs [index-1];
	}

	/*
    //return value = -2 if add should be shown
	                 -1 if it's out of range
	//
	*/
	public static int getSaveMePricesByIndex(int index, SaveMePriceTag saveMePriceTag){
		JSONNode node;
		if (saveMePriceTag == SaveMePriceTag.CUP)
			node = DAO.Settings.SaveMePrices;
		else
			node = DAO.Settings.SaveMePricesEndless;
		
		if (node.Count == 0)
			return DAO.Settings.RetryRunCost;
		if (node.Count > index) {
			return node [index].AsInt;
		} else {
			return -1;
		}
	}

	public static List<string> getDevicesWithoutAds()
	{
		var li = new List<string> ();
		if (DAO.Settings.DevicesWithoutAds!=null)
			foreach (var item in DAO.Settings.DevicesWithoutAds.Children) {
				li.Add (item.Value.ToLower());
		}
		return li;
	}

	public static List<string> getOSWithoutAds()
	{
		var li = new List<string> ();
		if (DAO.Settings.OSWithoutAds!=null)
			foreach (var item in DAO.Settings.OSWithoutAds.Children) {
				li.Add (item.Value.ToLower());
		}
		return li;
	}

	public static JSONNode getEasyCupRunByIndex(int index){
		if ((ServerDataInterface.Instance.cup_runs_easy == null) || (ServerDataInterface.Instance.cup_runs_easy.Count == 0)) {
			 return ServerDataInterface.Instance.cups [index];
		}
		if ( (ServerDataInterface.Instance.cup_runs_easy.Count) < index) {
			return ServerDataInterface.Instance.cup_runs_easy [0];
		}
		return ServerDataInterface.Instance.cup_runs_easy [index-1];
	}

	public static JSONNode getCupByIndex(int index){
		return ServerDataInterface.Instance.cups [index];
	}

	public static JSONNode getPracticeRunByIndex(int index){
		//Debug.Log ("alon___________________ ServerDataInterface.Instance.practice_runs: " + ServerDataInterface.Instance.practice_runs.Count);
		if (index <= ServerDataInterface.Instance.practice_runs.Count) {
			return ServerDataInterface.Instance.practice_runs [index];
		} else {
			return ServerDataInterface.Instance.practice_runs [ServerDataInterface.Instance.practice_runs.Count];
		}
	}

	public static JSONNode Cups{
		get{
			return ServerDataInterface.Instance.cups;
		}
	}

	public static JSONNode Perks{
		get{
			return ServerDataInterface.Instance.perks;
		}
	}


	public static JSONNode TutorialRun{
		get{
			return ServerDataInterface.Instance.tutorial_run;
		}
	}

	public static JSONNode TestRun{
		get{
			return ServerDataInterface.Instance.test_run;
		}
	}


// ===============================================================================================================================
// =============================================================================================================================== Local Variables
// ===============================================================================================================================

	public int CalcNumOfPurchasedPlayers(string sPurchasedPlayers)
	{
		return sPurchasedPlayers.Split (char_separator, System.StringSplitOptions.RemoveEmptyEntries).Length;
	}

	public static int NumOfPurchasedPlayers{
		get{ 
			return PurchasedPlayers.Split (char_separator, System.StringSplitOptions.RemoveEmptyEntries).Length;
		}
	}

	public static int NumOfCompletedCups{
		get{ 
			return WinnedCups.Split (new char[1]{ '%' }, System.StringSplitOptions.RemoveEmptyEntries).Length;
		}
	}

	// -------------



	public static UserData.UserType UserType{
		get{
			return LocalDataInterface.Instance.userType;
		}
		set{
			LocalDataInterface.Instance.userType = value;
		}
	}

	public static int AppLaunchCount{
		get{
			if (LocalDataInterface.Instance != null)
				return LocalDataInterface.Instance.appLaunchCount;
			else
				return 1;
		}
		set{
			LocalDataInterface.Instance.appLaunchCount = value;
		}
	}

	public static int PlayTimeInMinutes{
		get{
			return LocalDataInterface.Instance.playTimeInMinutes ;
		}
		set{
			LocalDataInterface.Instance.playTimeInMinutes = value;
		}
	}
	public static int PlayTimeInDays{
		get{
			return LocalDataInterface.Instance.playTimeInDays ;
		}
		set{
			LocalDataInterface.Instance.playTimeInDays = value;
		}
	}

	public static float MapScrollY{
		get{ 
			return LocalDataInterface.Instance.MapScrollY;
		}
		set{ 
			LocalDataInterface.Instance.MapScrollY = value;
		}
	}

	public static int TotalCoinsCollected{
		get{
			return LocalDataInterface.Instance.totalCoinsCollected ;
		}
		set{
			LocalDataInterface.Instance.totalCoinsCollected = value;
		}
	}

	public static int TotalMagnetsCollected{
		get{
			return LocalDataInterface.Instance.totalMagnetsCollected ;
		}
		set{
			LocalDataInterface.Instance.totalMagnetsCollected = value;
		}
	}

	public static int TotalSpeedBoostsCollected{
		get{
			return LocalDataInterface.Instance.totalSpeedBoostsCollected ;
		}
		set{
			LocalDataInterface.Instance.totalSpeedBoostsCollected = value;
		}
	}

	public static int TotalSheildsCollected{
		get{
			return LocalDataInterface.Instance.totalSheildsCollected ;
		}
		set{
			LocalDataInterface.Instance.totalSheildsCollected = value;
		}
	}

	public static bool StarterPackBought{
		get{
			return (LocalDataInterface.Instance.starterPackBought == 1);
		}
		set{
			LocalDataInterface.Instance.starterPackBought = value ? 1 : 0;
		}
	}

	public static bool FCBMegaDealBought{
		get{
			return (LocalDataInterface.Instance.FCBMegaDealBought == 1);
		}
		set{
			LocalDataInterface.Instance.FCBMegaDealBought = value ? 1 : 0;
		}
	}


	public static int TotalX2CoinsCollected{
		get{
			return LocalDataInterface.Instance.totalX2CoinsCollected ;
		}
		set{
			LocalDataInterface.Instance.totalX2CoinsCollected = value;
		}
	}

	public static float TotalGiftCardsCollected{
		get{
			return LocalDataInterface.Instance.totalGiftCardsCollected;
		}
		set{
			LocalDataInterface.Instance.totalGiftCardsCollected = value;
		}
	}

	public static int TotalMedikKitsCollected{
		get{
			return LocalDataInterface.Instance.totalMedikKitsCollected;
		}
		set{
			LocalDataInterface.Instance.totalMedikKitsCollected = value;
		}
	}

	public static int ActiveRunID{
		get{
			return LocalDataInterface.Instance.activeRunId;
		}
		set{
			LocalDataInterface.Instance.activeRunId = value;
		}
	}

	public static int IAPPoints{
		get{
			return LocalDataInterface.Instance.iapPoints;
		}
		set{
			LocalDataInterface.Instance.iapPoints = value;
		}
	}

	public static string ActiveRunData{
		get{
			return LocalDataInterface.Instance.activeRunData;
		}
		set{
			LocalDataInterface.Instance.activeRunData = value;
		}
	}


	public static string CompletedRunsData{
		get{
			return LocalDataInterface.Instance.completedRunsData;
		}
		set{
			LocalDataInterface.Instance.completedRunsData = value;
		}
	}

	public static string WinnedCups{
		get{
			return LocalDataInterface.Instance.winnedCups;
		}
		set{
			LocalDataInterface.Instance.winnedCups = value;
		}
	}

	public static string PurchasedPlayers{
		get{
			return LocalDataInterface.Instance.purchasedPlayers;
		}
		set{
			LocalDataInterface.Instance.purchasedPlayers = value;
		}
	}
//    public static Dictionary<int, KeyValuePair<string,int>> MedalsPerQuest
//    {
//        get{
//            return LocalDataInterface.Instance.medalsPerQuest;
//        }
//        set{
//            LocalDataInterface.Instance.medalsPerQuest = value;
//        }
//    }

	public bool IsPlayerAllReadyPurchased(string playerToBuy){
		string[] existingPlayers = PurchasedPlayers.Split(char_separator, StringSplitOptions.RemoveEmptyEntries);
		bool thereIsPlayer = false;

		foreach(string pId in existingPlayers){
			if (pId == playerToBuy) {
				thereIsPlayer = true;
				break;
			}
		}
		return thereIsPlayer;
	}

	public static string OpenedRegions{
		get{
			return LocalDataInterface.Instance.openedRegions;
		}
		set{
			LocalDataInterface.Instance.openedRegions = value;
		}
	}

	public static string PerksBonusTime{
		get{
			return LocalDataInterface.Instance.perkBonusTime;
		}
		set{
			LocalDataInterface.Instance.perkBonusTime = value;
		}
	}

	public static string CountryCode{
		get{
			if (LocalDataInterface.Instance.countryCode == null || LocalDataInterface.Instance.countryCode == "") {
				LocalDataInterface.Instance.countryCode = "WW";
			}
			return LocalDataInterface.Instance.countryCode;
		}
		set{
			LocalDataInterface.Instance.countryCode = value;
		}
	}

	public static string GetPlayerData(int number){
		return LocalDataInterface.Instance.playerData [number];
	}

	public static void SetPlayerData(int number, string data){
		LocalDataInterface.Instance.playerData [number] = data;
	}

	public static int GetPlayersPrice(int purchased_players){
		if (purchased_players > Settings.PlayerPrices.Count-2) {
			return Settings.PlayerPrices [Settings.PlayerPrices.Count - 1].AsInt;
		}else return Settings.PlayerPrices [purchased_players + 1].AsInt;
	}

	public static int GetCoachPrice(){
		return Settings.PlayerPrices [0].AsInt;
	}

	public static bool IsMusicEnabled{
		get{ return (LocalDataInterface.Instance.isMusicEnabled == 1); }
		set{ 
			LocalDataInterface.Instance.isMusicEnabled = (value) ? 1 : 0;
		}
	}

	public static bool IsSFXEnabled{
		get{ return (LocalDataInterface.Instance.isSFXEnabled == 1); }
		set{ 
			LocalDataInterface.Instance.isSFXEnabled = (value) ? 1 : 0;
		}
	}

	public static bool IsNotificationsEnabled{
		get{
			if (!PlayerPrefs.HasKey("notifications_enabled")) {
				LocalDataInterface.Instance.isNotificationsEnabled = 1; 
			}
			return (LocalDataInterface.Instance.isNotificationsEnabled == 1);
		}
		set{ 
			LocalDataInterface.Instance.isNotificationsEnabled = (value) ? 1 : 0;
		}
	}

	public static string AppLanguage{
		get{ 
			return LocalDataInterface.Instance.appLanguage;
		}
		set{ 
			LocalDataInterface.Instance.appLanguage = value;
		}

	}

	public static int TutorialState{
		get{ 
			return LocalDataInterface.Instance.tutorialState;
		}
		set{ 
			LocalDataInterface.Instance.tutorialState = value;
		}

	}

	public static DateTime InstallDate{
		get{
			return  DateTime.Parse (LocalDataInterface.Instance.installDate);
		}
	}

	public static int HealPopoup{
		get{ 
			return LocalDataInterface.Instance.popupHeal;
		}
		set{ 
			LocalDataInterface.Instance.popupHeal = value;
		}
	}

	public static int BuySecondPlayerPopup1{
		get{ 
			return LocalDataInterface.Instance.popupBuySecondPlayer1;
		}
		set{ 
			LocalDataInterface.Instance.popupBuySecondPlayer1 = value;
		}
	}

	public static int BuySecondPlayerPopup2{
		get{ 
			return LocalDataInterface.Instance.popupBuySecondPlayer2;
		}
		set{ 
			LocalDataInterface.Instance.popupBuySecondPlayer2 = value;
		}
	}
		
	public static DateTime RecruitSecondPlayerPopup2Date{
		get{
			return  DateTime.Parse (LocalDataInterface.Instance.recruitSecondPlayerPopup2Date);
		}
	}

	public static DateTime RateUsPopupDate{
		get{
			if (LocalDataInterface.Instance.rateUsPopupDate == null || LocalDataInterface.Instance.rateUsPopupDate == "") {
				return DateTime.Now;
			}

			return  DateTime.Parse (LocalDataInterface.Instance.rateUsPopupDate);
		}
		set{
			LocalDataInterface.Instance.rateUsPopupDate = value.ToString();
		}
	}

	public static int RateUsTimeFactor{
		get{ 
			return LocalDataInterface.Instance.rateUsTimeFactor;
		}
		set{ 
			LocalDataInterface.Instance.rateUsTimeFactor = value;
		}
	}

	public static int BuyThirdPlayerPopup{
		get{ 
			return LocalDataInterface.Instance.popupBuyThirdPlayer;
		}
		set{ 
			LocalDataInterface.Instance.popupBuyThirdPlayer = value;
		}
	}

	public static int PlayPracticePopup{
		get{ 
			return LocalDataInterface.Instance.popupPlayPractice;
		}
		set{ 
			LocalDataInterface.Instance.popupPlayPractice = value;
		}
	}

	public static int PlayCupPopup{
		get{ 
			return LocalDataInterface.Instance.popupPlayCup;
		}
		set{ 
			LocalDataInterface.Instance.popupPlayCup = value;
		}
	}

	public static DateTime PlayCupPopupDate{
		get{
			return  DateTime.Parse (LocalDataInterface.Instance.playCupPopupDate);
		}
	}

	public static int OfferPopup{
		get{ 
			return LocalDataInterface.Instance.popupOffer;
		}
		set{ 
			LocalDataInterface.Instance.popupOffer = value;
		}
	}

	public static DateTime SpecialOfferPopupDate{
		get{
			return  DateTime.Parse (LocalDataInterface.Instance.offerPopupDate);
		}
	}

	public static int FacebookPopup{
		get{ 
			return LocalDataInterface.Instance.facebookPopup;
		}
		set{ 
			LocalDataInterface.Instance.facebookPopup = value;
		}
	}

	public static int RateUsPopup{
		get{ 
			return LocalDataInterface.Instance.rateUsPopup;
		}
		set{ 
			LocalDataInterface.Instance.rateUsPopup = value;
		}
	}

	public static int SwipingHand{
		get{ 
			return LocalDataInterface.Instance.swipingHand;
		}
		set{ 
			LocalDataInterface.Instance.swipingHand = value;
		}
	}

	public static int CardsHandTut{
		get{ 
			return LocalDataInterface.Instance.cardsHandTut;
		}
		set{ 
			LocalDataInterface.Instance.cardsHandTut = value;
		}
	}

	public static int CardsPowerUpsTut{
		get{ 
			return LocalDataInterface.Instance.cardsPowerUpsTut;
		}
		set{ 
			LocalDataInterface.Instance.cardsPowerUpsTut = value;
		}
	}

	public static int TotalGiftCardsSent{
		get{ 
			return LocalDataInterface.Instance.totalGiftCardsSent;
		}
		set{ 
			LocalDataInterface.Instance.totalGiftCardsSent = value;
		}
	}

	public static int TotalGiftCardsRecieved{
		get{ 
			return LocalDataInterface.Instance.totalGiftCardsRecieved;
		}
		set{ 
			LocalDataInterface.Instance.totalGiftCardsRecieved = value;
		}
	}


	public static bool FBConnected{
		get{ 
			return (LocalDataInterface.Instance.fbConnected == 1) ;
		}
		set{ 
			if(value) LocalDataInterface.Instance.fbConnected = 1;
			else LocalDataInterface.Instance.fbConnected = 0;
		}
	}
		

	public static bool IsLegendPurchased{
		get{ 
			return (LocalDataInterface.Instance.isLegendPurchased == 1) ;
		}
		set{ 
			if(value) LocalDataInterface.Instance.isLegendPurchased = 1;
			else LocalDataInterface.Instance.isLegendPurchased = 0;
		}
	}

	public static int WoundedHealed{
		get{ 
			return LocalDataInterface.Instance.woundedHealed;
		}
		set{ 
			LocalDataInterface.Instance.woundedHealed = value;
		}
	}

	public static int MagnetsUsed{
		get{ 
			return LocalDataInterface.Instance.magnetsUsed;
		}
		set{ 
			LocalDataInterface.Instance.magnetsUsed = value;
		}
	}

	public static int SheildsUsed{
		get{ 
			return LocalDataInterface.Instance.sheildsUsed;
		}
		set{ 
			LocalDataInterface.Instance.sheildsUsed = value;
		}
	}

	public static int SpeedBoostsUsed{
		get{ 
			return LocalDataInterface.Instance.speedBoostsUsed;
		}
		set{ 
			LocalDataInterface.Instance.speedBoostsUsed = value;
		}
	}

	public static int TotalBallShooted{
		get{ 
			return LocalDataInterface.Instance.totalBallShooted;
		}
		set{ 
			LocalDataInterface.Instance.totalBallShooted = value;
		}
	}


	public static int TotalFriendsInvited{
		get{ 
			return LocalDataInterface.Instance.totalFriendsInvited;
		}
		set{ 
			LocalDataInterface.Instance.totalFriendsInvited = value;
		}		
	}

	public static int BestPracticeScore{
		get{ 
			return LocalDataInterface.Instance.bestPracticeScore;
		}
		set{ 
			LocalDataInterface.Instance.bestPracticeScore = value;
		}		
	}

	public static int FirstSecterAreaBall{
		get{ 
			return LocalDataInterface.Instance.secretAreaBall;
		}
		set{ 
			LocalDataInterface.Instance.secretAreaBall = value;
		}		
	}


	public static int TotalGoldMedals{
		get{ return LocalDataInterface.Instance.totalGoldMedals; }
		set{ LocalDataInterface.Instance.totalGoldMedals = value; }
	}

	public static int TotalSilverMedals{
		get{ return LocalDataInterface.Instance.totalSilverMedals; }
		set{ LocalDataInterface.Instance.totalSilverMedals = value; }
	}

	public static int TotalFailedRuns{
		get{ return LocalDataInterface.Instance.totalFailedRuns; }
		set{ LocalDataInterface.Instance.totalFailedRuns = value; }
	}

	public static int TotalPracticeTime{
		get{ return LocalDataInterface.Instance.totalPracticeTime; }
		set{ LocalDataInterface.Instance.totalPracticeTime = value; }
	}

	public static int RankScore{
		get{ return LocalDataInterface.Instance.rankScore; }
		set{ LocalDataInterface.Instance.rankScore = value; }
	}
    public static bool isRestoredPurchases{
        get{ return (LocalDataInterface.Instance.isRestoredPurchases == 1); }
        set{ LocalDataInterface.Instance.isRestoredPurchases = value?1:0; }
    }

	public static bool IsInDebugMode{
		get{ return (LocalDataInterface.Instance.isDebugMode == "YES"); }
		set{ LocalDataInterface.Instance.isDebugMode = (value) ? "YES":"NO"; }
	}


	public static int BILevelUp{
		get{ 
			return LocalDataInterface.Instance.bILevelUp;
		}
		set{ 
			LocalDataInterface.Instance.bILevelUp = value;
		}		
	}

	public static int BIDistance{
		get{ 
			return LocalDataInterface.Instance.bIDistance;
		}
		set{ 
			LocalDataInterface.Instance.bIDistance = value;
		}		
	}

	public static DateTime DailyGiftsStart{
	get
		{
			return LocalDataInterface.Instance.dailyGiftsStart;
		}
	set
		{
			LocalDataInterface.Instance.dailyGiftsStart = value;
		}
	}
	public static DateTime DailyGiftsLast{
	get
		{
			return LocalDataInterface.Instance.dailyGiftsLast;
		}
	set
		{
			LocalDataInterface.Instance.dailyGiftsLast = value;
		}
	}
	public static bool IsFreePlayerClaimed{
		get{ 
			return (LocalDataInterface.Instance.isFreePlayerClaimed == 1) ;
		}
		set{ 
			if(value) LocalDataInterface.Instance.isFreePlayerClaimed = 1;
			else LocalDataInterface.Instance.isFreePlayerClaimed = 0;
		}
	}

	public static string Email{
		get{
			return LocalDataInterface.Instance.email;
		}
		set{
			LocalDataInterface.Instance.email = value;
		}
	}

	public static string Gender{
		get{
			return LocalDataInterface.Instance.gender;
		}
		set{
			LocalDataInterface.Instance.gender = value;
		}
	}

	public static string FirstName{
		get{
			return LocalDataInterface.Instance.firstName;
		}
		set{
			LocalDataInterface.Instance.firstName = value;
		}
	}

	public static string LastName{
		get{
			return LocalDataInterface.Instance.lastName;
		}
		set{
			LocalDataInterface.Instance.lastName = value;
		}
	}

	public static string Lang{
		get{
			return LocalDataInterface.Instance.lang;
		}
		set{
			LocalDataInterface.Instance.lang = value;
		}
	}

	public static string Country{
		get{
			return LocalDataInterface.Instance.country;
		}
		set{
			LocalDataInterface.Instance.country = value;
		}
	}

	public static bool CanSendEmail{
		get{ 
			return (LocalDataInterface.Instance.canSendEmail == 1) ;
		}
		set{ 
			if(value) LocalDataInterface.Instance.canSendEmail = 1;
			else LocalDataInterface.Instance.canSendEmail = 0;
		}
	}

	public static string Ftd{
		get{
			return LocalDataInterface.Instance.Ftd;
		}
		set{
			LocalDataInterface.Instance.Ftd = value;
		}
	}

	public static string whaleID{
		get{
			return LocalDataInterface.Instance.whaleID;
		}
		set{
			LocalDataInterface.Instance.whaleID = value;
		}
	}

	public static int maxRunID{
		get{
			return LocalDataInterface.Instance.maxRunID;
		}
		set{
			LocalDataInterface.Instance.maxRunID = value;
		}
	}

	public static void createWhaleID()
	{
		if (String.IsNullOrEmpty (whaleID)) {
			try {
				whaleID = SystemInfo.deviceUniqueIdentifier;
			} catch (Exception e) {
				Debug.Log ("createWhaleID deviceUniqueIdentifier not found: " + e.Message);
				if (FB.IsLoggedIn) {
					whaleID = UserData.Instance.userID;
				} 
				else
					whaleID = DateTime.Now.ToString ("yyyyMMddHHmmssfff");
			}	
		}
	}


}


[System.Serializable]
public class GoalDescription{

	public string goal_A;
	public string goal_B;

	public GoalDescription( JSONNode desc ){
		goal_A = Utils.FixRTLString(desc [0].Value);
		goal_B = Utils.FixRTLString(desc [1].Value);
	}

	public string Rank_A_Goal{
		get{ return goal_A; }
	}

	public string Rank_A_Full{
		get{ return DAO.Language.Rank_A +" - " + goal_A; }
	}

	public string Rank_B_Goal{
		get{ return goal_B; }
	}
	
	public string Rank_B_Full{
		get{ return DAO.Language.Rank_B +" - " + goal_B; }
	}

	public string FullDescription{
		get{ return Rank_A_Full +"\n"+Rank_B_Full; }
	}
}





