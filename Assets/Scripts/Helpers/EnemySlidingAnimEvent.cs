﻿using UnityEngine;
using System.Collections;

public class EnemySlidingAnimEvent : MonoBehaviour {

	public void EnemySlideEnd(){
		EnemySlide.instanse.StopSlide ();
	}
}
