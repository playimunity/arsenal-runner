﻿using UnityEngine;
using System.Collections;

public class StadiumPortalManager : MonoBehaviour {

	public CollectableBox collectableBox;

	public GameObject alternativeCollectable;

	WaitForSeconds waitForSec_1 = new WaitForSeconds(1f);


	void Start () {
		if (RunManager.instance.wasSecretArea) {
			collectableBox.collectable = alternativeCollectable;
		} else {
			StartCoroutine (CheckSecretArea());
		}
	}
	

	IEnumerator CheckSecretArea(){
		yield return waitForSec_1;

		if (RunManager.instance.wasSecretArea) {
			collectableBox.collectable = alternativeCollectable;
		} else {
			StartCoroutine (CheckSecretArea());
		}
	}
}
