﻿using UnityEngine;
using System.Collections;

public class StaticObstacle : MonoBehaviour {

	//GameObject player;
	//GameObject obstacle;
	//bool isActive = true;
	
	void OnEnable () {
		//player = GameObject.FindGameObjectWithTag ("Player");
		//obstacle = 
	}
	

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Player") {
			if (gameObject.tag == "Wall"){
				PlayerController.instance.Damage(2);
				gameObject.SetActive(false);


			}else if (gameObject.tag == "Pile" && PlayerController.instance.sliding == true) {
				gameObject.SetActive(false);
				PlayerController.instance.CollectBall();
			}else{
				PlayerController.instance.Damage(1);
				gameObject.SetActive(false);
			}
			//StartCoroutine(BringObsticleBack());
		}
		else if (collision.gameObject.tag == "Ball" && (gameObject.tag == "Wall" || gameObject.tag == "Pile")) {
			gameObject.SetActive(false);
			//StartCoroutine(BringObsticleBack());
		}
	}

	IEnumerator BringObsticleBack(){
		yield return new WaitForSeconds (5f);
		gameObject.SetActive(true);
	}
}
