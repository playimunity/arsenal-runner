﻿using UnityEngine;
using System.Collections;

public class CoinsRawShowHide : MonoBehaviour {

	int randomNum;

	public Transform[] coinTransforms;

	public GameObject coinPrefab;
	GameObject coinClone;


	void Start () {
		randomNum = Random.Range (0, 3);

		if (randomNum == 0) {
			gameObject.SetActive (true);
			foreach (Transform coinPos in coinTransforms) {
				coinClone = (GameObject)Instantiate (coinPrefab, coinPos.position, coinPos.rotation);
				coinClone.transform.parent = transform;
			}
		} else {
			gameObject.SetActive (false);
		}
	}
	

}
