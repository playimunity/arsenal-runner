﻿using UnityEngine;
using System.Collections;

public class Magnet : Collectable {

	public GameObject MeshObject;
	Transform MeshTransform;

	void Start () {
		type = CollectableTypes.MAGNET;
		MeshTransform = MeshObject.transform;
	}


	void Update () {
		//MeshTransform.Rotate( 0f, 180f*Time.deltaTime, 0f);
	}

	void OnTriggerEnter(Collider col){
		if (col.transform.gameObject.tag == "Player") {
			ParticleCollectGenerator.instance.PlayParticle(transform.position , transform.eulerAngles);
			RunManager.OnMagnetCollected ();
			gameObject.SetActive(false);//Destroy(gameObject);
		}
	}
}
