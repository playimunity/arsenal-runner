﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldSectionOld : MonoBehaviour {

	float COIN_HEIGHT = 0.5f;

	public enum RegionType{generic, gothic, metro, guell, ramb , stadium};
	public RegionType currentRegionType;

	public enum SectionType{G5, G5T , G5TR , G5TL , G5B , G5U , G5D , G5T3 , G3, G3T , G3TR , G3TL , G3B , G3U , G3D , G3T5 , GT3 , GT3T , GT3TR , GT3TL , GT3EN , GT3EX , GT3TS , S3 , S5 , S3EN , S3EX , S3O , R3EN , R3 , R3EX , GEND , P3G , P3W , P3G2W , P3W2G , P3EX , P3EN , ST5 , STEX };
	public SectionType currentSectionType;

	public Transform objectsContainer;
	public Transform buildingsContainer;

	float[] heightMap = {5f, 4.964f, 4.923f, 4.872f, 4.767f, 4.643f, 4.487f, 4.309f, 4.107f, 3.892f, 
		3.66f, 3.433f, 3.19f, 2.97f, 2.729f, 2.507f, 2.285f, 2.07f, 1.865f, 1.621f, 
		1.397f, 1.177f, 0.96f, 0.753f, 0.555f, 0.383f, 0.252f, 0.135f, 0.054f, 0.015f, 0f};


	List<GameObject> buildings = new List<GameObject>();

	List<GameObject> Buildings5m;
	List<GameObject> Buildings10m;
	List<GameObject> Buildings15m;

	public List<Transform> Locators5M = new List<Transform>();
	public List<Transform> Locators10M = new List<Transform>();
	public List<Transform> Locators15M = new List<Transform>();


	int zFactor;

	Quaternion globalBuildingRotation;

	float globalYRotation;
	float globalAvalableSpace;
	float globalStaticPosition;
	float globalDinamicPosition;

	Vector3 vz = Vector3.zero;
	Vector3 vz2 = Vector3.zero;
	GameObject goz;


	public bool buildLow = true;
	Vector3 Rot;
	Vector3 TargetPos;
	Vector3 TMPos;

	public int sectionNumber;

	static WaitForEndOfFrame _weof;
	static WaitForEndOfFrame weof{
		get{
			if (_weof == null)
				_weof = new WaitForEndOfFrame ();

			return _weof;
		}
	}

	IEnumerator MoveUp(){

		while (TMPos.y < -20f) {
			TMPos.y += 10f * Time.deltaTime;
			transform.position = TMPos;
			yield return weof;
		}

		Rot = transform.eulerAngles;
		Rot.x += 40f;
		transform.eulerAngles = Rot;

		while (TMPos.y < TargetPos.y) {
			TMPos.y += 10f * Time.deltaTime;
			transform.position = TMPos;

			Rot.x -= 20f * Time.deltaTime;
			transform.eulerAngles = Rot;
			yield return weof;
		}

//		while (Rot.x > 0f) {
//			Rot.x -= 15f * Time.deltaTime;
//			transform.eulerAngles = Rot;
//
//			yield return new WaitForEndOfFrame ();
//		}

		Rot.x = 0f;
		transform.eulerAngles = Rot;


		TMPos.y = TargetPos.y;
		transform.position = TMPos;
	}

	void Start () {

//		if(buildLow){
//			TargetPos = TMPos = transform.position;
//			TMPos.y -= 30f;
//			transform.position = TMPos;
//
//			StartCoroutine (MoveUp());
//		}

		switch (currentRegionType) {
		case RegionType.generic:
			
//			Buildings5m = PrefabManager.instanse.buildingsGeneric5m;
//			Buildings10m = PrefabManager.instanse.buildingsGeneric10m;
//			Buildings15m = PrefabManager.instanse.buildingsGeneric15m;

			break;
		case RegionType.gothic:

//			Buildings5m = PrefabManager.instanse.buildingsGothic5m;
//			Buildings10m = PrefabManager.instanse.buildingsGothic10m;
//			Buildings15m = PrefabManager.instanse.buildingsGothic15m;

			break;
		case RegionType.guell:
//			Buildings5m = PrefabManager.instanse.buildingsGeneric5m;
//			Buildings10m = PrefabManager.instanse.buildingsGeneric10m;
//			Buildings15m = PrefabManager.instanse.buildingsGeneric15m;
			break;
		case RegionType.metro:

//			Buildings5m = PrefabManager.instanse.buildingsGothic5m;
//			Buildings10m = PrefabManager.instanse.buildingsGothic10m;
//			Buildings15m = PrefabManager.instanse.buildingsGothic15m;

			break;
		case RegionType.ramb:

//			Buildings5m = PrefabManager.instanse.buildingsRambla5m;
//			Buildings10m = PrefabManager.instanse.buildingsRambla10m;
//			Buildings15m = PrefabManager.instanse.buildingsRambla15m;

			break;
		}


		foreach (GameObject building in Buildings5m) {
			buildings.Add (building);
		}

		foreach (GameObject building in Buildings10m) {
			buildings.Add (building);
		}

		foreach (GameObject building in Buildings15m) {
			buildings.Add (building);
		}


		switch (currentSectionType) {
		case SectionType.G5:
		case SectionType.R3:
			FiveRoutesBuildingPlacer();
			break;
		case SectionType.G5T:
			FiveTBuildingPlacer();
			break;
		case SectionType.G5TR:
			FiveTurnBuildungPlacer();
			break;
		case SectionType.G5TL:
			FiveTurnBuildungPlacer();
			break;
		case SectionType.G5B:
			FiveRoutesBlocedBuildingPlacer();
			break;
		case SectionType.G5U:
			RoadUpDown();
			break;
		case SectionType.G5D:
			RoadUpDown();
			break;
		case SectionType.G5T3:
			FiveToThreeBuildingPlacer();
			break;
		case SectionType.G3:
		case SectionType.GT3:
		//case SectionType.R3:
		//case SectionType.GT3EX:
		//case SectionType.S3EN:
		//case SectionType.S3EX:
			ThreeRoutesBuildingPlacer ();
			break;
		case SectionType.G3T:
			ThreeTBuildingPlacer();
			break;
		case SectionType.G3TR:
		case SectionType.GT3TR:
			FiveTurnBuildungPlacer();
			break;
		case SectionType.G3TL:
		case SectionType.GT3TL:
			FiveTurnBuildungPlacer();
			break;
		case SectionType.G3B:
			ThreeRoutesBlocedBuildingPlacer();
			break;
		case SectionType.G3U:
			RoadUpDown();
			break;
		case SectionType.G3D:
			RoadUpDown();
			break;
		case SectionType.G3T5:
			ThreeToFiveBuildingPlacer();
			break;
		case SectionType.GT3TS:
		case SectionType.GT3T:
		case SectionType.R3EN:
		case SectionType.R3EX:
		case SectionType.P3EX:
			ThreeTSpecialBuildingPlacer();
			break;
		}


	}
	

	public void FiveRoutesBuildingPlacer(){
		foreach (Transform childBuilding in buildingsContainer) {
			GameObject.Destroy(childBuilding.gameObject);
		}

		globalStaticPosition = -5f;
		globalYRotation = 0f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 30f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
		
		globalStaticPosition = 5f;
		globalYRotation = 180f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 30f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
	}





	public void ThreeRoutesBuildingPlacer(){
		foreach (Transform childBuilding in buildingsContainer) {
			GameObject.Destroy(childBuilding.gameObject);
		}

		globalStaticPosition = -3f;
		globalYRotation = 0f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 30f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);


		globalStaticPosition = 3f;
		globalYRotation = 180f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 30f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
	}






	public void FiveTBuildingPlacer(){

		for (int i = 0; i < Locators5M.Count; i ++) {
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , Locators5M[i].transform.position , Locators5M[i].transform.rotation);
			my5MBuilding.transform.parent = Locators5M[i];
		}

		globalStaticPosition = 5f;
		globalYRotation = 90f;
		globalDinamicPosition = -15f;
		globalAvalableSpace = 30f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
	}



	public void ThreeTBuildingPlacer(){
		
		for (int i = 0; i < Locators5M.Count; i ++) {
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , Locators5M[i].transform.position , Locators5M[i].transform.rotation);
			my5MBuilding.transform.parent = Locators5M[i];
		}
		
		globalStaticPosition = 3f;
		globalYRotation = 90f;
		globalDinamicPosition = -15f;
		globalAvalableSpace = 30f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
	}


	public void ThreeTSpecialBuildingPlacer(){
		
		for (int i = 0; i < Locators5M.Count; i ++) {
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , Locators5M[i].transform.position , Locators5M[i].transform.rotation);
			my5MBuilding.transform.parent = Locators5M[i];
		}

		for (int i = 0; i < Locators10M.Count; i ++) {
			int random10MBuilding = Random.Range(0,Buildings10m.Count);
			GameObject my10MBuilding = (GameObject)Instantiate(Buildings10m[random10MBuilding] , Locators10M[i].transform.position , Locators10M[i].transform.rotation);
			my10MBuilding.transform.parent = Locators10M[i];
		}
		
//		globalStaticPosition = 3f;
//		globalYRotation = 90f;
//		globalDinamicPosition = -15f;
//		globalAvalableSpace = 30f;
//		
//		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
	}





	public void FiveTurnBuildungPlacer(){
		foreach (Transform locator in Locators5M){
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , locator.transform.position , locator.transform.rotation);
			my5MBuilding.transform.parent = locator;
		}
		foreach (Transform locator in Locators10M){
			int random10MBuilding = Random.Range(0,Buildings10m.Count);
			GameObject my10MBuilding = (GameObject)Instantiate(Buildings10m[random10MBuilding] , locator.transform.position , locator.transform.rotation);
			my10MBuilding.transform.parent = locator;
		}
		foreach (Transform locator in Locators15M){
			int random15MBuilding = Random.Range(0,Buildings15m.Count);
			GameObject my15MBuilding = (GameObject)Instantiate(Buildings15m[random15MBuilding] , locator.transform.position , locator.transform.rotation);
			my15MBuilding.transform.parent = locator;
		}
	}



	public void RoadUpDown(){
		foreach (Transform locator in Locators5M){
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , locator.transform.position , locator.transform.rotation);
			my5MBuilding.transform.parent = locator;
		}
		foreach (Transform locator in Locators10M){
			int random10MBuilding = Random.Range(0,Buildings10m.Count);
			GameObject my10MBuilding = (GameObject)Instantiate(Buildings10m[random10MBuilding] , locator.transform.position , locator.transform.rotation);
			my10MBuilding.transform.parent = locator;
		}
		foreach (Transform locator in Locators15M){
			int random15MBuilding = Random.Range(0,Buildings15m.Count);
			GameObject my15MBuilding = (GameObject)Instantiate(Buildings15m[random15MBuilding] , locator.transform.position , locator.transform.rotation);
			my15MBuilding.transform.parent = locator;
		}
	}

	


	public void FiveToThreeBuildingPlacer(){
		
		for (int i = 0; i < Locators5M.Count; i ++) {
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , Locators5M[i].transform.position , Locators5M[i].transform.rotation);
			my5MBuilding.transform.parent = Locators5M[i];
		}

		globalStaticPosition = -3f;
		globalYRotation = 0f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 10f;

		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);

		globalStaticPosition = 3f;
		globalYRotation = 180f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 10f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);

		globalStaticPosition = -5f;
		globalYRotation = 0f;
		globalDinamicPosition = 0f;
		globalAvalableSpace = 15f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);

		globalStaticPosition = 5f;
		globalYRotation = 180f;
		globalDinamicPosition = 0f;
		globalAvalableSpace = 15f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);

	}





	public void ThreeToFiveBuildingPlacer(){
		
		for (int i = 0; i < Locators5M.Count; i ++) {
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , Locators5M[i].transform.position , Locators5M[i].transform.rotation);
			my5MBuilding.transform.parent = Locators5M[i];
		}
		
		globalStaticPosition = -5f;
		globalYRotation = 0f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 10f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
		globalStaticPosition = 5f;
		globalYRotation = 180f;
		globalDinamicPosition = 15f;
		globalAvalableSpace = 10f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
		globalStaticPosition = -3f;
		globalYRotation = 0f;
		globalDinamicPosition = 0f;
		globalAvalableSpace = 15f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
		globalStaticPosition = 3f;
		globalYRotation = 180f;
		globalDinamicPosition = 0f;
		globalAvalableSpace = 15f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
	}




	public void ThreeRoutesBlocedBuildingPlacer(){
		
		for (int i = 0; i < Locators5M.Count; i ++) {
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , Locators5M[i].transform.position , Locators5M[i].transform.rotation);
			my5MBuilding.transform.parent = Locators5M[i];
		}
		
		globalStaticPosition = -3f;
		globalYRotation = 0f;
		globalDinamicPosition = 5f;
		globalAvalableSpace = 20f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
		globalStaticPosition = 3f;
		globalYRotation = 180f;
		globalDinamicPosition = 5f;
		globalAvalableSpace = 20f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
	}


	public void FiveRoutesBlocedBuildingPlacer(){
		
		for (int i = 0; i < Locators5M.Count; i ++) {
			int random5MBuilding = Random.Range(0,Buildings5m.Count);
			GameObject my5MBuilding = (GameObject)Instantiate(Buildings5m[random5MBuilding] , Locators5M[i].transform.position , Locators5M[i].transform.rotation);
			my5MBuilding.transform.parent = Locators5M[i];
		}
		
		globalStaticPosition = -5f;
		globalYRotation = 0f;
		globalDinamicPosition = 5f;
		globalAvalableSpace = 20f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
		globalStaticPosition = 5f;
		globalYRotation = 180f;
		globalDinamicPosition = 5f;
		globalAvalableSpace = 20f;
		
		BuildingPlacerRec(globalAvalableSpace , globalDinamicPosition , globalYRotation , globalStaticPosition , buildings);
		
	}




	int randomBuilding;
	GameObject myBuilding;
	Quaternion buildingRotation;

	void BuildingPlacerRec(float avalableSpace , float DinamicPosition , float yRotation , float staticPosition , List<GameObject> currentBuildings){

		int directionFactor = -1;

		if (avalableSpace < 5){
			avalableSpace = 0;
			return;
		}
		
		else if (avalableSpace < 10 && avalableSpace >= 5) {
			for (int i = 0; i < currentBuildings.Count; i ++) {
				if (currentBuildings[i].gameObject.tag == "Building10" || currentBuildings[i].gameObject.tag == "Building15") {
					currentBuildings.RemoveAt (i);
					i = 0;
				}
			}
		}
		
		else if (avalableSpace < 15 && avalableSpace >= 10) {
			for (int i = 0; i < currentBuildings.Count; i ++) {
				if (currentBuildings[i].gameObject.tag == "Building15") {
					currentBuildings.RemoveAt (i);
					i = 0;
				}
			}
		}
		
		randomBuilding = Random.Range (0, currentBuildings.Count);
		
		myBuilding = (GameObject)Instantiate(currentBuildings[randomBuilding] , Vector3.zero , transform.rotation);
		
		myBuilding.transform.parent = buildingsContainer;

		buildingRotation = Quaternion.Euler(0, yRotation, 0);

		myBuilding.transform.localRotation =  buildingRotation;

		if (yRotation == 90) {
			directionFactor = 1;
			myBuilding.transform.localPosition = new Vector3 (DinamicPosition, 0, globalStaticPosition);
		}

		if (yRotation == 180) {
			directionFactor = -1;
			myBuilding.transform.localPosition = new Vector3 (globalStaticPosition, 0, DinamicPosition );
		}

		
		if (currentBuildings [randomBuilding].gameObject.tag == "Building5") {
			zFactor = 5;
			avalableSpace = avalableSpace - zFactor;
			DinamicPosition = DinamicPosition + (zFactor * directionFactor);
		} else if (currentBuildings [randomBuilding].gameObject.tag == "Building10") {
			zFactor = 10;
			avalableSpace = avalableSpace - zFactor;
			DinamicPosition = DinamicPosition + (zFactor * directionFactor);
		} else if (currentBuildings [randomBuilding].gameObject.tag == "Building15") {
			zFactor = 15;
			avalableSpace = avalableSpace - zFactor;
			DinamicPosition = DinamicPosition + (zFactor * directionFactor);
		}

		if (yRotation == 0) {
			directionFactor = -1;
			myBuilding.transform.localPosition = new Vector3 (globalStaticPosition , 0, DinamicPosition);
		}

		
		BuildingPlacerRec(avalableSpace , DinamicPosition , yRotation , staticPosition , currentBuildings);
		
	}



	float GetCollectableHeightByZ(float z){

		if (currentSectionType == WorldSectionOld.SectionType.G3U || currentSectionType == WorldSectionOld.SectionType.G5U) {
			return (heightMap [ (int)Mathf.Ceil (z) ] + heightMap [(int)Mathf.Floor (z)]) / 2f + COIN_HEIGHT;
		} else if(currentSectionType == WorldSectionOld.SectionType.G3D || currentSectionType == WorldSectionOld.SectionType.G5D){
			return (heightMap [ 30 - (int)Mathf.Ceil (z) ] + heightMap [ 30 - (int)Mathf.Floor (z)]) / 2f + COIN_HEIGHT;
		}else{
			return COIN_HEIGHT;
		}
	}

	float GetObstacleHeightByZ(float z){
		
		if (currentSectionType == WorldSectionOld.SectionType.G3U || currentSectionType == WorldSectionOld.SectionType.G5U) {
			return (heightMap [ (int)Mathf.Ceil (z) ] + heightMap [(int)Mathf.Floor (z)]) / 2f + 0.2f;
		} else if(currentSectionType == WorldSectionOld.SectionType.G3D || currentSectionType == WorldSectionOld.SectionType.G5D){
			return (heightMap [ 30 - (int)Mathf.Ceil (z) ] + heightMap [ 30 - (int)Mathf.Floor (z)]) / 2f + 0.2f;
		}else{
			return 0.2f;
		}
	}


	public void PlaceObjects(List<CollectableAbstract> collectables, List<ObstacleAbstract> obstacles){
		if(collectables.Count > 0) PlaceCollectables (collectables);
		if(obstacles.Count > 0) PlaceObstacles (obstacles);
	}

	Transform TMP_TRANS;
	public void PlaceCollectables(List<CollectableAbstract> collectables){


		foreach (CollectableAbstract col in collectables) {

			if (col.type == Collectable.CollectableTypes.COIN) {

				vz.x =  Mathf.Round(col.x/10f);
				vz.z = - (col.z/10f);
				vz.y = GetCollectableHeightByZ(col.z/10f);

				col.ObjectOnScene = CoinPool._.GetCoin ();

				TMP_TRANS = col.ObjectOnScene.transform;
				TMP_TRANS.parent = objectsContainer;
				TMP_TRANS.localPosition = vz;
				TMP_TRANS.rotation = objectsContainer.transform.rotation;

				continue;
			}

			switch(col.type){
//				case Collectable.CollectableTypes.COIN : {
//					goz = PrefabManager.instanse.coin;
//					break;
//				}
				case Collectable.CollectableTypes.MAGNET : {
					if(!PlaceMagnetOrNot()) continue;
					goz = PrefabManager.instanse.magnet;
					break;
				}
//				case Collectable.CollectableTypes.ENERGY : {
//					goz = PrefabManager.instanse.energyDrink;
//					break;
//				}

				case Collectable.CollectableTypes.BALL_STACK : {
					goz = PrefabManager.instanse.ballStack;
					break;
				}

				case Collectable.CollectableTypes.WHISTLE : {
					goz = PrefabManager.instanse.whistle;
					break;
				}

				case Collectable.CollectableTypes.TUT : {
					goz = PrefabManager.instanse.tutorialPoint;
					break;
				}

				case Collectable.CollectableTypes.SPEEDBOOSTER : {
					goz = PrefabManager.instanse.speedBooster;
					break;
				}

				case Collectable.CollectableTypes.X2COINS : {
					goz = PrefabManager.instanse.x2Coins;
					break;
				}

//				case Collectable.CollectableTypes.STADIUM_PORTAL : {
//					goz = PrefabManager.instanse.StadiumPortal;
//					break;
//				}
                case Collectable.CollectableTypes.SHIELD : {
                    goz = PrefabManager.instanse.shield;
                    break;
                }

//				default:{
//					goz = PrefabManager.instanse.coin;
//					break;
//				}
			}

			vz.x =  Mathf.Round(col.x/10f);
			vz.z = - (col.z/10f);
			vz.y = GetCollectableHeightByZ(col.z/10f);
			
			col.ObjectOnScene = (GameObject)Instantiate (goz , Vector3.zero, objectsContainer.transform.rotation );
			col.ObjectOnScene.transform.parent = objectsContainer;
			col.ObjectOnScene.transform.localPosition = vz;

		}
	}

	public bool PlaceMagnetOrNot(){

		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) return true;

		if (Random.value < PlayerController.instance.Perk_ChanceToMeetMagnetFactor) return true;
		return false;
	
	}

	public void PlaceObstacles(List<ObstacleAbstract> obstacles){



		foreach (ObstacleAbstract obst in obstacles){

			switch(obst.type){
//				case Obstacle.ObstacleTypes.BCNETA 						: {goz = PrefabManager.instanse.BCNeta;break;}
//				case Obstacle.ObstacleTypes.STREET_LAMP 				: {goz = PrefabManager.instanse.StreetLamp;break;}
//				case Obstacle.ObstacleTypes.WHEELBARROW 				: {goz = PrefabManager.instanse.Wheelbarrow;break;}
//			case Obstacle.ObstacleTypes.WHEELBARROW2 				: {goz = PrefabManager.instanse.Wheelbarrow2;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_1 		: {goz = PrefabManager.instanse.UnderConstraction1;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_2 		: {goz = PrefabManager.instanse.UnderConstraction2;break;}
//				case Obstacle.ObstacleTypes.BICYCLE 					: {goz = PrefabManager.instanse.Bicycle;break;}
//				case Obstacle.ObstacleTypes.BICYCLE_ROW 				: {goz = PrefabManager.instanse.BicycleRow;break;}
//				//case Obstacle.ObstacleTypes.BUS 						: {goz = PrefabManager.instanse.Bus;break;}
//				case Obstacle.ObstacleTypes.BUS_STOP 					: {goz = PrefabManager.instanse.BusStop;break;}
//			case Obstacle.ObstacleTypes.CROWD 					: {goz = PrefabManager.instanse.crowd;break;}
//				case Obstacle.ObstacleTypes.CAFE1 						: {goz = PrefabManager.instanse.Cafe1;break;}
//				case Obstacle.ObstacleTypes.CAFE2 						: {goz = PrefabManager.instanse.Cafe2;break;}
//			case Obstacle.ObstacleTypes.Cafe3 						: {goz = PrefabManager.instanse.Cafe3;break;}
//			case Obstacle.ObstacleTypes.Cafe4 						: {goz = PrefabManager.instanse.Cafe4;break;}
//				case Obstacle.ObstacleTypes.MAP_SIGN1 					: {goz = PrefabManager.instanse.MapSign1;break;}
//				case Obstacle.ObstacleTypes.MAP_SIGN2 					: {goz = PrefabManager.instanse.MapSign2;break;}
//				case Obstacle.ObstacleTypes.MINIBUS 					: {goz = PrefabManager.instanse.Minibus;break;}
//				case Obstacle.ObstacleTypes.MOPED 						: {goz = PrefabManager.instanse.Moped;break;}
//				case Obstacle.ObstacleTypes.SEDAN 						: {goz = PrefabManager.instanse.Sedan;break;}
//				case Obstacle.ObstacleTypes.TRAFFIC_LIGHT1 				: {goz = PrefabManager.instanse.TrafficLight;break;}
//				case Obstacle.ObstacleTypes.TRASH_CAN_BIG 				: {goz = PrefabManager.instanse.TrashCanBig;break;}
//			case Obstacle.ObstacleTypes.TRASH_CAN_BIG2 				: {goz = PrefabManager.instanse.TrashCanBig2;break;}
//			case Obstacle.ObstacleTypes.TRASH_CAN_BIG3 				: {goz = PrefabManager.instanse.TrashCanBig3;break;}
//				case Obstacle.ObstacleTypes.TRASH_CAN_SMALL 			: {goz = PrefabManager.instanse.TrashCanSmall;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_3 		: {goz = PrefabManager.instanse.UnderConstraction3;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_4 		: {goz = PrefabManager.instanse.UnderConstraction4;break;}
//			case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_5 		: {goz = PrefabManager.instanse.UnderConstraction5;break;}
//			case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_6 		: {goz = PrefabManager.instanse.UnderConstraction6;break;}

//				case Obstacle.ObstacleTypes.BUSH 					    	: {goz = PrefabManager.instanse.Bush;break;}
//			case Obstacle.ObstacleTypes.Bush2 					    	: {goz = PrefabManager.instanse.Bush2;break;}
//			case Obstacle.ObstacleTypes.Bush3 					    	: {goz = PrefabManager.instanse.Bush3;break;}
//				case Obstacle.ObstacleTypes.Car_Hatchback 					: {goz = PrefabManager.instanse.Hatchback;break;}
//				case Obstacle.ObstacleTypes.Car_Taxi 				    	: {goz = PrefabManager.instanse.Taxi;break;}
//				case Obstacle.ObstacleTypes.Enemy_Run 				    	: {goz = PrefabManager.instanse.EnemyRun;break;}
//				case Obstacle.ObstacleTypes.Enemy_Slide 					: {goz = PrefabManager.instanse.enemySlide;break;}
//				case Obstacle.ObstacleTypes.Female 					    	: {goz = PrefabManager.instanse.Female;break;}
//				case Obstacle.ObstacleTypes.Male 				  			: {goz = PrefabManager.instanse.Male;break;}
//				case Obstacle.ObstacleTypes.Flower_Pot 						: {goz = PrefabManager.instanse.Flowers;break;}
//			case Obstacle.ObstacleTypes.Flower_Pot2 						: {goz = PrefabManager.instanse.Flowers2;break;}
//			case Obstacle.ObstacleTypes.Flower_Pot3 						: {goz = PrefabManager.instanse.Flowers3;break;}
//				case Obstacle.ObstacleTypes.Flower_Stall_1 					: {goz = PrefabManager.instanse.FlowerStall1;break;}
//				case Obstacle.ObstacleTypes.Flower_Stall_2 					: {goz = PrefabManager.instanse.FlowerStall2;break;}
//				case Obstacle.ObstacleTypes.Food_Stall_1 					: {goz = PrefabManager.instanse.FoodStall1;break;}
//				case Obstacle.ObstacleTypes.Food_Stall_2 					: {goz = PrefabManager.instanse.FoodStall2;break;}
//				case Obstacle.ObstacleTypes.Moped_Moving 					: {goz = PrefabManager.instanse.MopedMoving;break;}
//				case Obstacle.ObstacleTypes.People_With_Glass 				: {goz = PrefabManager.instanse.GlassPeople;break;}
//				case Obstacle.ObstacleTypes.Pillar 							: {goz = PrefabManager.instanse.Pillar;break;}
//				case Obstacle.ObstacleTypes.Truck_Food 						: {goz = PrefabManager.instanse.TruckFood;break;}
//				case Obstacle.ObstacleTypes.Truck_Small 					: {goz = PrefabManager.instanse.TruckSmall;break;}
//			case Obstacle.ObstacleTypes.T_Sign 							: {goz = PrefabManager.instanse.TSign;break;}
//			case Obstacle.ObstacleTypes.T_Sign2 						: {goz = PrefabManager.instanse.TSign2;break;}
//			case Obstacle.ObstacleTypes.Barrier1 						: {goz = PrefabManager.instanse.Barrier1;break;}
//			case Obstacle.ObstacleTypes.Barrier2 						: {goz = PrefabManager.instanse.Barrier2;break;}
//			case Obstacle.ObstacleTypes.Barrier3 						: {goz = PrefabManager.instanse.Barrier3;break;}
//			case Obstacle.ObstacleTypes.Blocking_Road 					: {goz = PrefabManager.instanse.BlockingRoad;break;}
//			case Obstacle.ObstacleTypes.FallingTree 					: {goz = PrefabManager.instanse.FallingTree;break;}
//			case Obstacle.ObstacleTypes.Gate2 							: {goz = PrefabManager.instanse.Gate2;break;}
//			case Obstacle.ObstacleTypes.Electric_Gate_Close 			: {goz = PrefabManager.instanse.ElectricGateClose;break;}
//			case Obstacle.ObstacleTypes.Electric_Gate_Open 				: {goz = PrefabManager.instanse.ElectricGateOpen;break;}

//				case Obstacle.ObstacleTypes.Door1 				        	: {goz = PrefabManager.instanse.door1;break;}
//				case Obstacle.ObstacleTypes.Door2 							: {goz = PrefabManager.instanse.door2;break;}
//				case Obstacle.ObstacleTypes.Half_Hanging1 					: {goz = PrefabManager.instanse.halfHanging1;break;}
//				case Obstacle.ObstacleTypes.Half_Hanging2 					: {goz = PrefabManager.instanse.halfHanging2;break;}
//				case Obstacle.ObstacleTypes.Hanging1 						: {goz = PrefabManager.instanse.hanging1;break;}
//				case Obstacle.ObstacleTypes.Hanging2 						: {goz = PrefabManager.instanse.hanging2;break;}
//				case Obstacle.ObstacleTypes.Side_Building 					: {goz = PrefabManager.instanse.sideBuilding;break;}
//				case Obstacle.ObstacleTypes.Sign1 							: {goz = PrefabManager.instanse.sign1;break;}
//				case Obstacle.ObstacleTypes.Sign2 							: {goz = PrefabManager.instanse.sign2;break;}

//			case Obstacle.ObstacleTypes.HuemanStatue1 					: {goz = PrefabManager.instanse.huemanStatue1;break;}
//			case Obstacle.ObstacleTypes.HuemanStatue2 					: {goz = PrefabManager.instanse.huemanStatue2;break;}
//			case Obstacle.ObstacleTypes.HuemanStatue3 					: {goz = PrefabManager.instanse.huemanStatue3;break;}

//				case Obstacle.ObstacleTypes.Train 							: {goz = PrefabManager.instanse.train;break;}

//			case Obstacle.ObstacleTypes.BenchMosaic1 				        : {goz = PrefabManager.instanse.BenchMosaic1;break;}
//			case Obstacle.ObstacleTypes.BenchMosaic2 						: {goz = PrefabManager.instanse.BenchMosaic2;break;}
//			case Obstacle.ObstacleTypes.BenchMosaic3 						: {goz = PrefabManager.instanse.BenchMosaic3;break;}
//			case Obstacle.ObstacleTypes.BenchMosaic4 						: {goz = PrefabManager.instanse.BenchMosaic4;break;}
//			case Obstacle.ObstacleTypes.Guell_Boulder 						: {goz = PrefabManager.instanse.Boulder;break;}
//			case Obstacle.ObstacleTypes.Guell_Bush1 						: {goz = PrefabManager.instanse.GuellBush1;break;}
//			case Obstacle.ObstacleTypes.Guell_Bush2 						: {goz = PrefabManager.instanse.GuellBush2;break;}
//			case Obstacle.ObstacleTypes.Guell_Bush3 						: {goz = PrefabManager.instanse.GuellBush3;break;}
//			case Obstacle.ObstacleTypes.Guell_Trash 						: {goz = PrefabManager.instanse.GuellTrash;break;}
//			case Obstacle.ObstacleTypes.GuellTrunk 							: {goz = PrefabManager.instanse.GuellTrunk;break;}
//			case Obstacle.ObstacleTypes.PalmTree 							: {goz = PrefabManager.instanse.PalmTree;break;}
//			case Obstacle.ObstacleTypes.StoneBench 							: {goz = PrefabManager.instanse.StoneBench;break;}

			}



			// Align Goth Obstacles
			if( obst.type ==  Obstacle.ObstacleTypes.Hanging1 || obst.type ==  Obstacle.ObstacleTypes.Hanging2
				|| obst.type ==  Obstacle.ObstacleTypes.Half_Hanging2 || obst.type ==  Obstacle.ObstacleTypes.Half_Hanging1)
			{

				vz.x = 15f;

			}else if(obst.type ==  Obstacle.ObstacleTypes.Side_Building
			         || obst.type ==  Obstacle.ObstacleTypes.Door1
			         || obst.type ==  Obstacle.ObstacleTypes.Door2
			){

				if(obst.x < 150f) vz.x = 13f;
				else vz.x = 17f;

			}else if(obst.type ==  Obstacle.ObstacleTypes.Sign1 || obst.type ==  Obstacle.ObstacleTypes.Sign2){

				if(obst.x < 150f) vz.x = 12.5f;
				else vz.x = 17.5f;

			}else{
				vz.x =  Mathf.Round(obst.x/10f);

			}

			vz.z = - (obst.z/10f);
			vz.y = GetObstacleHeightByZ(obst.z/10f);

			// rotation
			vz2.y = (int)obst.rotate;


			obst.ObjectOnScene = (GameObject)Instantiate (goz , Vector3.zero, Quaternion.Euler(transform.rotation.eulerAngles + vz2) );
			obst.ObjectOnScene.transform.parent = objectsContainer;
			obst.ObjectOnScene.transform.localPosition = vz;

			if(obst.move == Obstacle.MoveTypes.MOVE){
				obst.ObjectOnScene.GetComponent<Obstacle>().move = obst.move;
			}



			//if(obst.ObjectOnScene.GetComponent<Obstacle>().smashable == Obstacle.SmashableTypes.SMASHABLE)
		}

	}




}
