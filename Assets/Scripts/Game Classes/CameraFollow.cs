﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	[SerializeField]
	private Transform playerCollider;
	[SerializeField]

	//private Vector3 targetOffset;
	//public float smoothTime = 0.3F;
	//private float yVelocity = 0.0F;
	public CameraLookatTarget camLookatTargetScript;

	//public Transform playerCollider;

	public bool jumping = false;
	public bool sliding = false;
	//public bool turning = false;
	//public bool justAfterTurn = false;
	public bool debugBool = false;
	//public bool startTurn = false;
	bool goingDown = false;
	//public bool isDead = false;
	public bool downUp = false;

	public Transform lookAtTarget;

	WaitForSeconds pauseForEndTurn;

	//float newPositionX;
	//float newPositionY;
	float newPositionZ;
	public float staticYPos = 0;

	//float originalX;
	int turnDirection;

	Vector3 pos;
	Vector3 offset;
	float originalYoffset = 0f;
	public float turnOffset = 0f;

	float cameraXspeed = 50f;

	Coroutine SlideCoroutine;
	Coroutine JumpCoroutine;

	public static CameraFollow instance;

	//UnityStandardAssets.ImageEffects.MotionBlur blur;
	//UnityStandardAssets.ImageEffects.Grayscale grayscale;

	void Start () {
		instance = this;

		pauseForEndTurn = new WaitForSeconds (1.5f);

		//blur = GetComponent<UnityStandardAssets.ImageEffects.MotionBlur> ();
		//grayscale= GetComponent<UnityStandardAssets.ImageEffects.Grayscale> ();

		offset = transform.localPosition - playerCollider.localPosition;
		originalYoffset = offset.y;
		//newPositionZ = offset.z - 3f; //for going backwards after death

		StartCoroutine ("ResetPositions");
	}
	

	void Update () {

		//pos.x = transform.localPosition.x;
		//pos.z = transform.localPosition.z;

//		if (turning) {
//			turnOffset = Mathf.Lerp (turnOffset, 2f, 4f * Time.deltaTime);
//			//turnOffset = 1f;
//		} else {
//			if (turnOffset > 0f) {
//				turnOffset = Mathf.Lerp (turnOffset, 0f, 5f * Time.deltaTime);
//				//turnOffset = 0f;
//			}
//		} 

		if (downUp) {
			//pos.y = playerCollider.localPosition.y;
			pos.y = Mathf.Lerp (pos.y, playerCollider.localPosition.y, 8f * Time.deltaTime);
			camLookatTargetScript.downUp = true;
		} else {
			camLookatTargetScript.downUp = false;
		}

		//pos.z = offset.z;

		//pos.x = playerCollider.localPosition.x + (turnOffset * turnDirection);
		//pos.x = Mathf.Lerp (pos.x , playerCollider.localPosition.x + (turnOffset * turnDirection) , 20f * Time.deltaTime);
		pos.x = Mathf.Lerp (pos.x , playerCollider.localPosition.x*0.75f , 20f * Time.deltaTime);

		transform.localPosition = pos;

		//targetOffset = target.position;
		//targetOffset.y += 1.0f;

	}


	IEnumerator ResetPositions(){   
		yield return new WaitForSeconds (1f);
		transform.position = playerCollider.position;
	}



	public void CrossTurn(int direction){
		turnDirection = direction;
		cameraXspeed = 4f;
		//turning = true;
		//startTurn = true;
		//originalX = transform.localPosition.x;
		//StartCoroutine (CrossTurnIenumerator());
	}

	public void CrossTurnEnd(){
		//startTurn = false;
		//turning = false;
		cameraXspeed = 4f;
		StartCoroutine (endTurn());
	}

	IEnumerator endTurn(){
		for(float i=4f; i < 50f; i+= 60f * Time.deltaTime){
			cameraXspeed = i;

			yield return null;
		}

		cameraXspeed = 50f;

		//		yield return pauseForEndTurn;
		//		justAfterTurn = false;
	}


	public void CameraBackToPlace(){
		camLookatTargetScript.TargetBackToPlace (staticYPos);

		if (Mathf.Abs (pos.y - staticYPos) <= 0.05f) {
			return;
		}
		StartCoroutine (CameraBackToPlaceCoroutine());
	}

	IEnumerator CameraBackToPlaceCoroutine(){
		
		while (Mathf.Abs (pos.y - staticYPos) > 0.05f) {
			pos.y = Mathf.Lerp (pos.y , staticYPos , 3.5f * Time.deltaTime);
			yield return null;
		}
		pos.y = staticYPos;

	}








	
//	IEnumerator BackFromJump(){
//		yield return new WaitForSeconds (0.6f);
//		jumping = false;
//	}
//	
//	IEnumerator BackFromSlide(){
//		yield return new WaitForSeconds (0.8f);
//		sliding = false;
//	}
//
//	public void JumpOn(){
//		sliding = false;
//		jumping = true;
//	}
//
//	public void SlideOn(){
//		jumping = false;
//		sliding = true;
//	}
//
//
//	void goDownJump(){
//		if (goingDown) return;
//
//		goingDown = true;
//
//		if(SlideCoroutine != null) StopCoroutine (SlideCoroutine);
//		JumpCoroutine = StartCoroutine (goDownCoroutine(0.2f));
//	}
//
//	void goDownSlide(){
//		if (goingDown) return;
//		
//		goingDown = true;
//		if(JumpCoroutine != null) StopCoroutine (JumpCoroutine);
//		SlideCoroutine = StartCoroutine (goDownCoroutine(0.25f));
//	}
//
//
//	IEnumerator goDownCoroutine(float timeToGoUp){
//		
//		for (float i = 0f; i < 1f; i += 2f * Time.deltaTime) {
//			offset.y = Mathf.Lerp(offset.y, 0.6f, i);
//			yield return new WaitForEndOfFrame();
//		}
//		StartCoroutine ( goUpCourotine(timeToGoUp) );
//	}
//	
//	IEnumerator goUpCourotine(float timeToGoUp){
//		yield return new WaitForSeconds (timeToGoUp);
//
//		for (float i = 0f; i < 1f; i += 1.5f * Time.deltaTime) {
//			offset.y = Mathf.Lerp(offset.y, originalYoffset, i);
//			yield return new WaitForEndOfFrame();
//		}
//
//		while(offset.y < originalYoffset){
//			offset.y += 3f * Time.deltaTime;
//			yield return new WaitForEndOfFrame();
//		}
//	}
//
//	public void EnableGodModeEffects(){
//		//blur.enabled = true;
//	}
//
//	public void DisableGodModeEffects(){
//		//blur.enabled = false;
//	}

	public void GrayScale(){
		//grayscale.enabled = true;
	}





}
