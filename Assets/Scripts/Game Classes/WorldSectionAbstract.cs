﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

[System.Serializable]
public class WorldSectionAbstract{

	public string id;
	public GameObject ObjectOnScene;
	public GameObject ObjectOnSceneCopy;
    public bool dontSetActiveFalse;
	public List<CollectableAbstract> collectables;
	public List<ObstacleAbstract> obstacles;

	public int sectionNumber;

	public WorldSectionAbstract(JSONNode section){
		id = section["id"];

		JSONNode colls = section["collectables"];
		JSONNode obsts = section["obstacles"];

		collectables = new List<CollectableAbstract> ();
		for (var i=0; i < colls.Count; i++) {
			collectables.Add( new CollectableAbstract( (JSONNode)colls[i] ) );
		}

		obstacles = new List<ObstacleAbstract> ();
		for (var i=0; i < obsts.Count; i++) {
			obstacles.Add( new ObstacleAbstract( (JSONNode)obsts[i] ) );
		}
	}

	public WorldSectionAbstract(string _id){
		id = _id;
		collectables = new List<CollectableAbstract> ();
		obstacles = new List<ObstacleAbstract> ();
	}

}





[System.Serializable]
public class CollectableAbstract{
	public string id;
	public GameObject ObjectOnScene;
	public Collectable.CollectableTypes type;

	public float x;
	public float z;

	public CollectableAbstract(JSONNode collectable){
		id = collectable ["type"];

		if( id.Equals("magnet") ) type = Collectable.CollectableTypes.MAGNET;
		else if( id.Equals("coin") ) type = Collectable.CollectableTypes.COIN;
		else if( id.Equals("energy_drink") ) type = Collectable.CollectableTypes.ENERGY;
		else if( id.Equals("balls_stack") ) type = Collectable.CollectableTypes.BALL_STACK;
		else if( id.Equals("whistle") ) type = Collectable.CollectableTypes.WHISTLE;
		else if( id.Equals("sbooster") ) type = Collectable.CollectableTypes.SPEEDBOOSTER;
        else if( id.Equals("shield") ) type = Collectable.CollectableTypes.SHIELD;
		else if( id.Equals("x2") ) type = Collectable.CollectableTypes.X2COINS;
		else if( id.Equals("sPortal") ) type = Collectable.CollectableTypes.STADIUM_PORTAL;
		else if( id.Equals("tut") ) type = Collectable.CollectableTypes.TUT;
		//else if( id.Equals("spaceship") ) type = Collectable.CollectableTypes.s;

		x = collectable["x"].AsFloat;
		z = collectable["y"].AsFloat;
	}

}



[System.Serializable]
public class ObstacleAbstract{
	public string id;
	public GameObject ObjectOnScene;
	public Obstacle.ObstacleTypes type;
	public Obstacle.MoveTypes move;

	public float x;
	public float z;
	public int rotate;

	public ObstacleAbstract(JSONNode obstacle){
		id = obstacle["type"];

		if (id.Equals ("bcneta"))
			type = Obstacle.ObstacleTypes.BCNETA;
		else if (id.Equals ("streetlamp"))
			type = Obstacle.ObstacleTypes.STREET_LAMP;
		else if (id.Equals ("wheelbarrow"))
			type = Obstacle.ObstacleTypes.WHEELBARROW;
		else if (id.Equals ("wheelbarrow2"))
			type = Obstacle.ObstacleTypes.WHEELBARROW2;
		else if (id.Equals ("uc1"))
			type = Obstacle.ObstacleTypes.UNDER_CONSTRACTION_1;
		else if (id.Equals ("uc2"))
			type = Obstacle.ObstacleTypes.UNDER_CONSTRACTION_2;
		else if (id.Equals ("bic"))
			type = Obstacle.ObstacleTypes.BICYCLE;
		else if (id.Equals ("bicrow"))
			type = Obstacle.ObstacleTypes.BICYCLE_ROW;
		else if (id.Equals ("bustop"))
			type = Obstacle.ObstacleTypes.BUS_STOP;
		else if (id.Equals ("crowd"))
			type = Obstacle.ObstacleTypes.CROWD;
		else if (id.Equals ("cafe1"))
			type = Obstacle.ObstacleTypes.CAFE1;
		else if (id.Equals ("cafe2"))
			type = Obstacle.ObstacleTypes.CAFE2;
		else if (id.Equals ("cafe3"))
			type = Obstacle.ObstacleTypes.Cafe3;
		else if (id.Equals ("cafe4"))
			type = Obstacle.ObstacleTypes.Cafe4;
		else if (id.Equals ("map1"))
			type = Obstacle.ObstacleTypes.MAP_SIGN1;
		else if (id.Equals ("map2"))
			type = Obstacle.ObstacleTypes.MAP_SIGN2;
		else if (id.Equals ("minibus"))
			type = Obstacle.ObstacleTypes.MINIBUS;
		else if (id.Equals ("moped"))
			type = Obstacle.ObstacleTypes.MOPED;
		else if (id.Equals ("sedan"))
			type = Obstacle.ObstacleTypes.SEDAN;
		else if (id.Equals ("tlight"))
			type = Obstacle.ObstacleTypes.TRAFFIC_LIGHT1;
		else if (id.Equals ("trashbig"))
			type = Obstacle.ObstacleTypes.TRASH_CAN_BIG;
		else if (id.Equals ("trashbig2"))
			type = Obstacle.ObstacleTypes.TRASH_CAN_BIG2;
		else if (id.Equals ("trashbig3"))
			type = Obstacle.ObstacleTypes.TRASH_CAN_BIG3;
		else if (id.Equals ("trashsmall"))
			type = Obstacle.ObstacleTypes.TRASH_CAN_SMALL;
		else if (id.Equals ("uc3"))
			type = Obstacle.ObstacleTypes.UNDER_CONSTRACTION_3;
		else if (id.Equals ("uc4"))
			type = Obstacle.ObstacleTypes.UNDER_CONSTRACTION_4;
		else if (id.Equals ("uc5"))
			type = Obstacle.ObstacleTypes.UNDER_CONSTRACTION_5;
		else if (id.Equals ("uc6"))
			type = Obstacle.ObstacleTypes.UNDER_CONSTRACTION_6;
		else if (id.Equals ("hatchback"))
			type = Obstacle.ObstacleTypes.Car_Hatchback;
		else if (id.Equals ("enemyrun"))
			type = Obstacle.ObstacleTypes.Enemy_Run;
		else if (id.Equals ("enemyslide"))
			type = Obstacle.ObstacleTypes.Enemy_Slide;
		else if (id.Equals ("female"))
			type = Obstacle.ObstacleTypes.Female;
		else if (id.Equals ("male"))
			type = Obstacle.ObstacleTypes.Male;
		else if (id.Equals ("flowers"))
			type = Obstacle.ObstacleTypes.Flower_Pot;
		else if (id.Equals ("flowers2"))
			type = Obstacle.ObstacleTypes.Flower_Pot2;
		else if (id.Equals ("flowers3"))
			type = Obstacle.ObstacleTypes.Flower_Pot3;
		else if (id.Equals ("flowerstall1"))
			type = Obstacle.ObstacleTypes.Flower_Stall_1;
		else if (id.Equals ("flowerstall2"))
			type = Obstacle.ObstacleTypes.Flower_Stall_2;
		else if (id.Equals ("foodstall1"))
			type = Obstacle.ObstacleTypes.Food_Stall_1;
		else if (id.Equals ("foodstall2"))
			type = Obstacle.ObstacleTypes.Food_Stall_2;
		else if (id.Equals ("mopedmoving"))
			type = Obstacle.ObstacleTypes.Moped_Moving;
		else if (id.Equals ("glasspeople"))
			type = Obstacle.ObstacleTypes.People_With_Glass;
		else if (id.Equals ("pillar"))
			type = Obstacle.ObstacleTypes.Pillar;
		else if (id.Equals ("truckfood"))
			type = Obstacle.ObstacleTypes.Truck_Food;
		else if (id.Equals ("trucksmall"))
			type = Obstacle.ObstacleTypes.Truck_Small;
		else if (id.Equals ("tsign"))
			type = Obstacle.ObstacleTypes.T_Sign;
		else if (id.Equals ("tsign2"))
			type = Obstacle.ObstacleTypes.T_Sign2;
		else if (id.Equals ("barrier1"))
			type = Obstacle.ObstacleTypes.Barrier1;
		else if (id.Equals ("barrier2"))
			type = Obstacle.ObstacleTypes.Barrier2;
		else if (id.Equals ("barrier3"))
			type = Obstacle.ObstacleTypes.Barrier3;
		else if (id.Equals ("gateopen"))
			type = Obstacle.ObstacleTypes.Electric_Gate_Open;
		else if (id.Equals ("gateclose"))
			type = Obstacle.ObstacleTypes.Electric_Gate_Close;
		else if (id.Equals ("fallingtree"))
			type = Obstacle.ObstacleTypes.FallingTree;
		else if (id.Equals ("gate2"))
			type = Obstacle.ObstacleTypes.Gate2;
		else if (id.Equals ("blockingroad"))
			type = Obstacle.ObstacleTypes.Blocking_Road;
		else if (id.Equals ("door1"))
			type = Obstacle.ObstacleTypes.Door1;
		else if (id.Equals ("door2"))
			type = Obstacle.ObstacleTypes.Door2;
		else if (id.Equals ("halfh1"))
			type = Obstacle.ObstacleTypes.Half_Hanging1;
		else if (id.Equals ("halfh2"))
			type = Obstacle.ObstacleTypes.Half_Hanging2;
		else if (id.Equals ("hang1"))
			type = Obstacle.ObstacleTypes.Hanging1;
		else if (id.Equals ("hang2"))
			type = Obstacle.ObstacleTypes.Hanging2;
		else if (id.Equals ("sideb"))
			type = Obstacle.ObstacleTypes.Side_Building;
		else if (id.Equals ("sign1"))
			type = Obstacle.ObstacleTypes.Sign1;
		else if (id.Equals ("sign2"))
			type = Obstacle.ObstacleTypes.Sign2;
		else if (id.Equals ("statue1"))
			type = Obstacle.ObstacleTypes.HuemanStatue1;
		else if (id.Equals ("statue2"))
			type = Obstacle.ObstacleTypes.HuemanStatue2;
		else if (id.Equals ("statue3"))
			type = Obstacle.ObstacleTypes.HuemanStatue3;
		else if (id.Equals ("train"))
			type = Obstacle.ObstacleTypes.Train;
		else if (id.Equals ("bus"))
			type = Obstacle.ObstacleTypes.BUS;
		else if (id.Equals ("car1"))
			type = Obstacle.ObstacleTypes.CAR1;
		else if (id.Equals ("car2"))
			type = Obstacle.ObstacleTypes.CAR2;
		else if (id.Equals ("car3"))
			type = Obstacle.ObstacleTypes.CAR3;
		else if (id.Equals ("car4"))
			type = Obstacle.ObstacleTypes.CAR4;
		else if (id.Equals ("police"))
			type = Obstacle.ObstacleTypes.CAR_POLICE;
		else if (id.Equals ("taxi"))
			type = Obstacle.ObstacleTypes.TAXI;
		else if (id.Equals ("truck1"))
			type = Obstacle.ObstacleTypes.TRUCK1;
		else if (id.Equals ("truck2"))
			type = Obstacle.ObstacleTypes.TRUCK2;
        else if (id.Equals ("truck3"))
            type = Obstacle.ObstacleTypes.TRUCK3;
        else if (id.Equals ("truck4"))
            type = Obstacle.ObstacleTypes.TRUCK4;
        else if (id.Equals ("truck5"))
            type = Obstacle.ObstacleTypes.TRUCK5;
		else if (id.Equals ("van1"))
			type = Obstacle.ObstacleTypes.VAN1;
		else if (id.Equals ("van2"))
			type = Obstacle.ObstacleTypes.VAN2;
		else if (id.Equals ("van_police"))
			type = Obstacle.ObstacleTypes.VAN_POLICE;
		else if (id.Equals ("garden"))
			type = Obstacle.ObstacleTypes.GARDEN;
		else if (id.Equals ("camera_post"))
			type = Obstacle.ObstacleTypes.CAMERA_POST;
		else if (id.Equals ("flag_rope"))
			type = Obstacle.ObstacleTypes.FLAG_ROPE;
		else if (id.Equals ("roadblock_jump"))
			type = Obstacle.ObstacleTypes.ROADBLOCK_JUMP;
		else if (id.Equals ("roadblock_3_lane"))
			type = Obstacle.ObstacleTypes.ROADBLOCK_3_LANE;
		else if (id.Equals ("roadblock_slide"))
			type = Obstacle.ObstacleTypes.ROADBLOCK_SLIDE;
		else if (id.Equals ("spaceship"))
			type = Obstacle.ObstacleTypes.SPACESHIP;
        else if (id.Equals ("bush1"))
            type = Obstacle.ObstacleTypes.BUSH;
        else if (id.Equals ("bush2"))
            type = Obstacle.ObstacleTypes.Bush2;
        else if (id.Equals ("bush3"))
            type = Obstacle.ObstacleTypes.Bush3;
        else if (id.Equals ("bush4"))
            type = Obstacle.ObstacleTypes.BUSH4;
        else if (id.Equals ("clothes_cart"))
            type = Obstacle.ObstacleTypes.CLOTHES_CART;
        else if (id.Equals ("clothes_stall"))
            type = Obstacle.ObstacleTypes.CLOTHES_STALL;
        else if (id.Equals ("concrete_truck1"))
            type = Obstacle.ObstacleTypes.CONCRETE_TRUCK1;
        else if (id.Equals ("concrete_truck2"))
            type = Obstacle.ObstacleTypes.CONCRETE_TRUCK2;
        else if (id.Equals ("food_van"))
            type = Obstacle.ObstacleTypes.FOOD_VAN;
        else if (id.Equals ("garbage_truck"))
            type = Obstacle.ObstacleTypes.GARBAGE_TRUCK;
        else if (id.Equals ("garden1"))
            type = Obstacle.ObstacleTypes.GARDEN1;
        else if (id.Equals ("hotdogs_stall"))
            type = Obstacle.ObstacleTypes.HOTDOGS_STALL;
        else if (id.Equals ("icecream_stall"))
            type = Obstacle.ObstacleTypes.ICECREAME_STALL;
        else if (id.Equals ("tree1"))
            type = Obstacle.ObstacleTypes.TREE1;
        else if (id.Equals ("tree2"))
            type = Obstacle.ObstacleTypes.TREE2;
//

		move = (obstacle ["m"].AsInt == 1) ? Obstacle.MoveTypes.MOVE : Obstacle.MoveTypes.STATIC;

		x = obstacle["x"].AsFloat;
		z = obstacle["y"].AsFloat;
		rotate = obstacle ["r"].AsInt;

	}


}















