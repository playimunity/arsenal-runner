﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PeopleWithGlass : MonoBehaviour {

	public Obstacle.ObstacleTypes type = Obstacle.ObstacleTypes.People_With_Glass;
	public Obstacle.SmashableTypes smashable = Obstacle.SmashableTypes.SMASHABLE;

	public GameObject player;
	public GameObject ballTarget;

	public Animator animator;
	public Animator targetAnimator;

	public float moveSpeed;
	float startOpacity = 0.9f;
	float startDistance = 25;
	float decreaceFactor;

	bool moveTriggered = false;
	bool colided = false;
	bool triggered = false;
	bool targetOn = false;
	bool staticColor = false;

	Vector3 menScale = new Vector3 (1, 1, 1);
	Vector3 menScaleNegative = new Vector3 (-1, 1, 1);

	Color alphaColor;

	public List<Material> myMaterials = new List<Material>();

	public Renderer[] myRenderers;

	Transform playerTrans;

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerTrans = player.transform;
	}


	public void Start(){
		if (transform.localPosition.x > 15f) {
			moveSpeed = -moveSpeed;
			transform.localScale = menScaleNegative;
		} else {
			//moveSpeed = moveSpeed;
			transform.localScale = menScale;
		}
	}
	

	void FixedUpdate () {
		if (moveTriggered &&  !GameManager.TutorialPrePaused) {
			transform.Translate (Vector3.right * moveSpeed * Time.deltaTime);
		}

		if (GameManager.TutorialPrePaused && animator.speed > 0f) {
			animator.speed = Mathf.Lerp(animator.speed, 0f, 10f * Time.deltaTime);
		}else if(!GameManager.TutorialPrePaused && animator.speed < 1f){
			animator.speed = Mathf.Lerp(animator.speed, 1f, 20f * Time.deltaTime);
		}




		if ((PlayerController.instance.withBall == true || PlayerController.instance.ballShooted == true) && Vector3.Distance (transform.position, playerTrans.position) <= 50f && ballTarget != null && !colided) {
			targetOn = true;
			ballTarget.SetActive (true);
		} else if (targetOn && ballTarget != null) {
			//StartCoroutine ("DeactivateTarget");
			targetOn = false;
			ballTarget.SetActive (false);
		}
	}


	void OnTriggerEnter(Collider col){
		if (!triggered && col.gameObject.tag == "Player") {
			triggered = true;
			StartWalk ();
		}
	}


	void OnCollisionEnter(Collision col) {
		if (colided)
			return;

		if (col.gameObject.tag == "Player") {
			colided = true;
			Crash ();
			PlayerController.instance.hardDamage = true;
			PlayerController.instance.Damage (2);
			AudioManager.Instance.OnBreakGlass ();

			NativeSocial._.ReportAchievment (SocialConstants.achievement_kaboom);
		}
	}


	public void StartWalk () {
		//animator.SetFloat("walkSpeed",1);

		moveTriggered = true;
		animator.SetTrigger ("walk");
	}


	public void Crash(){

		moveTriggered = false;
		GetComponent<Rigidbody> ().isKinematic = true;
		GetComponent<BoxCollider> ().enabled = false;
		animator.SetTrigger ("crash");

//		if(targetAnimator != null) targetAnimator.Play ("target_anim_down");

//		foreach (BoxCollider b in GetComponents<BoxCollider>()) {
//			b.enabled = false;
//		}
	}


	IEnumerator DeactivateTarget(){
		if (!PlayerController.instance.withBall && targetAnimator != null) {
			targetAnimator.Play ("target_anim_down");

			yield return new WaitForSeconds (0.8f);
			ballTarget.SetActive (false);
		}
	}

	static WaitForSeconds _wfs;
	static WaitForSeconds wfs{
		get{
			if (_wfs == null)
				_wfs = new WaitForSeconds (1f);

			return _wfs;
		}
	}

	IEnumerator BeKinematic(){
		yield return wfs;
		GetComponent<Rigidbody> ().isKinematic = true;

	}


}
