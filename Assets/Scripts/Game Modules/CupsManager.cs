using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class CupsManager : MonoBehaviour {

	JSONNode cupsSetup;
	JSONNode tmpRunJSON;
	public static CupsManager Instance;

	public List<Cup> cups;
	//public Cup activeCup;

	public Text CS_CupName;
	public Text CS_CupPize;
	public Text CS_PuzzlesCollected;

	public GameObject CS_CupPrizeContainer;
	public GameObject CS_CupPrizeClaimedContainer;

	public GameObject OngoingOBJ;
	public Text OG_OgTXT;
	public Text Og_CupName;

	public GameObject CupRunsScreen;

	WaitForSeconds waitMinute;

	public bool cupUIElementInitialized = false;
	public bool cupListInitiolized = false;
	bool shouldScroll = false;

	public List<QuestUIItem> QuestUIItems;
	public GameObject WS_AlreadyCompletedTXT;
	public GameObject WS_PrizeCoinIcon;
	public GameObject WS_PrizeX;
	public GameObject WS_PrizeCoinsAmount;
    public static int cupsManagerCount;
    public bool isQuestUiElementsInstantiated;




	void Awake(){
		Instance = this;


		shouldScroll = false;
		waitMinute = new WaitForSeconds (60f);
        
	}

	void OnEnable(){
		GameManager.OnCupTimeUpdate += UpdateOngoingCup;
		if (GameManager.ActiveCup != null && GameManager.ActiveCup.exist) {
			UpdateOngoingCup ();
		} else {
			OngoingOBJ.SetActive (false);
		}
	}

	void OnDisable(){
		GameManager.OnCupTimeUpdate -= UpdateOngoingCup;
	}

	#if UNITY_EDITOR
	void Update(){

		if (Input.GetKeyUp (KeyCode.W)) {
			DebugFinishCup ();
		}
	}
	#endif

	void Start(){
        
		if (GameManager.ActiveCupCompleted && GameManager.ActiveCompletedCup != null) {
			//Debug.Log ("alon____________ CupsManager - Start() - playing in a completed cup: " + GameManager.ActiveCompletedCup.id.ToString ());
		} else if (GameManager.ActiveCup != null) {
			//Debug.Log ("alon____________ CupsManager - Start() - playing in a in progress cup: " + GameManager.ActiveCup.id.ToString ());
		} else {
			//Debug.Log ("alon____________ CupsManager - Start() - no cup is in progress" );
		}
		BuildCups ();
//		if( activeCup.exist ) StartCoroutine( checkForActiveCupExpiration() );

		//InitializeQuestUIElements ();

		//InitializeCupsUIElements ();
		CheckIfCupIsCompleted ();

	}

	//=======================================================

	public void UpdateOngoingCup(){
		OngoingOBJ.SetActive (true);
		Og_CupName.text = GameManager.ActiveCup.name;
	}

	void CheckIfCupIsCompleted(){
		
//		if (GameManager.ActiveCup != null) {
//			Debug.Log ("alon____________ CupsManager - CheckIfCupIsCompleted() - cup status: " + GameManager.ActiveCup.status.ToString ());
//		}

		//if (GameManager.ActiveCup == null || (!GameManager.ActiveCup.exist || GameManager.ActiveCup.selectedRuns.Count < 1) || GameManager.ActiveCupCompleted) {
		//if (GameManager.ActiveCup == null || (GameManager.ActiveCup != null && GameManager.ActiveCup.selectedRuns.Count < 1) || GameManager.ActiveCupCompleted) {
		if (GameManager.ActiveCup == null || GameManager.ActiveCup.selectedRuns == null || GameManager.ActiveCupCompleted) {
			//Debug.Log ("alon____________ CupsManager - CheckIfCupIsCompleted() - no active cup");
			return;
		}

		int numOfCompletedRuns = 0;
		foreach(RunAbstract ra in GameManager.ActiveCup.selectedRuns){
			if(ra.status == RunAbstract.RunStatus.COMPLETED) numOfCompletedRuns++;
		}

     	if (numOfCompletedRuns >= GameManager.ActiveCup.numOfRuns) {
//			Debug.Log ("alon____________ CupsManager - CheckIfCupIsCompleted() - numOfCompletedRuns >= GameManager.ActiveCup.numOfRuns");
//			Debug.Log ("alon____________ CupsManager - numOfCompletedRuns: " + numOfCompletedRuns);
//			Debug.Log ("alon____________ CupsManager - ActiveCup.numOfRuns: " + GameManager.ActiveCup.numOfRuns);
//			Debug.Log ("alon____________ CupsManager - ActiveCup.id: " + GameManager.ActiveCup.id);
			HandleCupWon ();
			//GameManager.Instance.needToShowRateUsPopup = true;
		}
	}

	public void HandleCupWon(){
		
		if (GameManager.ActiveCup.GetPrevCompleteRank () <= -1) {

			WS_AlreadyCompletedTXT.SetActive (false);
			WS_PrizeCoinIcon.SetActive (true);
			WS_PrizeX.SetActive (true);
			WS_PrizeCoinsAmount.SetActive (true);

			DAO.TotalCoinsCollected += GameManager.ActiveCup.prize;
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (GameManager.ActiveCup.prize, "Quest completed", GameManager.ActiveCup.name, "", "", "");
			GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,GameManager.ActiveCup.prize, GameAnalyticsWrapper.REWARD,"QuestCompleted");
			CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

		} else {
			WS_AlreadyCompletedTXT.SetActive (true);
			WS_PrizeCoinIcon.SetActive (false);
			WS_PrizeX.SetActive (false);
			WS_PrizeCoinsAmount.SetActive (false);
		}



		MainScreenUiManager.instance.WinScrnToggle (true);
		GameManager.ActiveCup.AddToWinnedCups ();

		//MainScreenUiManager.instance.RunsScrnAnimToggle (false);
		AudioManager.Instance.OnCupWon ();
		ResetCupsUIElements ();


		DAO.PlayCupPopup = 1;
	}


	void BuildCups(){
		cupsSetup = DAO.Cups;

		for(int i=0; i < cupsSetup.Count; i++) {
			cups.Add( new Cup(cupsSetup[i], i) );
		}

		cupListInitiolized = true;
	}

	//bool initiolazed = false;
	public void InitializeQuestUIElements()
    {
        if (cupUIElementInitialized || !cupListInitiolized)
            return;
       if (!isQuestUiElementsInstantiated)
       { 
        GameObject questUiItemZona;
        int xZona = 0;
        int yZona = 400;
        for (int i = 0; i < cupsSetup.Count - 3; i++)
        {
            if (i % 2 == 0)
            {
                questUiItemZona = Instantiate(QuestUIItems[1].gameObject) as GameObject;
                xZona = 21;
            }
            else
            {
                questUiItemZona = Instantiate(QuestUIItems[2].gameObject) as GameObject;
                xZona = 401;
            }
            questUiItemZona.transform.SetParent(QuestUIItems[0].transform.parent);
            questUiItemZona.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                questUiItemZona.GetComponent<RectTransform>().anchoredPosition = new Vector2(xZona, 1138+yZona * (i + 2));
            QuestUIItems.Add(questUiItemZona.GetComponent<QuestUIItem>());
        }
        isQuestUiElementsInstantiated = true;
    }
		cupUIElementInitialized = true;
		for(int i =0; i < QuestUIItems.Count; i++) {
            if (cups[i].status == Cup.CupStatus.COMPLETED)
            {
                QuestUIItems [i].mostMedalsInQuest = cups[i].GetMostMedalsPerQuest();
            }
			QuestUIItems [i].Set(cups [i]);
		}

	}


	public void ResetCupsUIElements(){
		cupUIElementInitialized = false;
		//CupScrn_Actions.Instance.Reset ();
		InitializeQuestUIElements();
	}

	public void OnCupClicked(Cup cup){


//		if (GameManager.ActiveCup != null && GameManager.ActiveCup.exist && GameManager.ActiveCup.status == Cup.CupStatus.IN_PROGRESS && cup.status != Cup.CupStatus.IN_PROGRESS) {
			
//			GenericUIMessage gm = new GenericUIMessage();
//			gm.Title = DAO.Language.GetString ("are-you-sure");
//			gm.Message = DAO.Language.GetString ("cup-in-progress-msg");
//
//			gm.PositiveButtonText = DAO.Language.GetString ("back");
//			gm.PositiveBTNAction = ()=>{
//				CrossSceneUIHandler.Instance.hideGenericMessage();
//			};
//			gm.NegativeButtonText = DAO.Language.GetString ("im-sure");
//			gm.NegativeBTNAction = ()=>{
//				CrossSceneUIHandler.Instance.hideGenericMessage();
//				ShowCupScreen(cup);
//			};
//			CrossSceneUIHandler.Instance.showGennericMessage (gm);
//		}
	ShowCupScreen(cup);
	}



	public void ShowCupScreen(Cup cup){

		QuestMap.instance.DifineMapScrollY ();

		// Preparing Cup according his status (not_started, in_progress, completed, not_available)
//		if (cup.status != Cup.CupStatus.IN_PROGRESS) {
//		
//			if( GameManager.ActiveCup != null && GameManager.ActiveCup.exist ) GameManager.ActiveCup.End();
//			cup.Start();
//			GameManager.Instance.NewCupStarted ();
//			ResetCupsUIElements();
//		}

//		if (cup.status == Cup.CupStatus.COMPLETED) {
//			GameManager.ActiveCup = cup;
//			cup.SetRuns ();
//		}else if (cup.status == Cup.CupStatus.IN_PROGRESS) {
//			GameManager.ActiveCup = cup;
//			cup.SetRuns ();
//		}

		if (cup.status == Cup.CupStatus.NOT_STARTED) {
			cup.Start ();
			GameManager.Instance.NewCupStarted ();
			ResetCupsUIElements ();
			//Debug.Log ("alon_________ CupsManager - ShowCupScreen() - cup.status == Cup.CupStatus.NOT_STARTED");
			SetCupScreenRuns (GameManager.ActiveCup);
		} else if (cup.status == Cup.CupStatus.IN_PROGRESS) {
			//GameManager.ActiveCup = cup;
			cup.CheckIfItInProgress ();
			GameManager.ActiveCupCompleted = false;
			//Debug.Log ("alon_________ CupsManager - ShowCupScreen() - Cup.CupStatus.IN_PROGRESS");
			SetCupScreenRuns (GameManager.ActiveCup);
		}else{
			//GameManager.ActiveCup = cup;
			cup.SetCompletedCup ();
			//Debug.Log ("alon_________ CupsManager - ShowCupScreen() - Cup.CupStatus.COMPLETED");
			SetCupScreenRuns (GameManager.ActiveCompletedCup);
		}
			

		// Show screen:
		MainScreenUiManager.instance.RunsScrnAnimToggle(true);
	}

	public void SetCupScreenRuns(Cup activeCup){
//		Debug.Log ("alon______________ CupsManager - SetCupScreenRuns() - start - cup status: " + activeCup.status.ToString () + " , cup name: " + activeCup.name.ToString ());
		CupRunsScreen.SetActive (true);
		RunsScrn_Actions.Instance.Reset ();
		// Parsing cup runs for UI elements

		CS_CupName.text = activeCup.name;
		if (activeCup.status == Cup.CupStatus.COMPLETED) {
			CS_PuzzlesCollected.text = activeCup.numOfRuns + "/" + activeCup.numOfRuns;
		} else {
			CS_PuzzlesCollected.text = activeCup.NumOfCompletedRuns + "/" + activeCup.numOfRuns;
		}
		CS_CupPize.text = activeCup.prize.ToString();


		if (activeCup.GetPrevCompleteRank () <= -1) {
			CS_CupPrizeContainer.SetActive (true);
			CS_CupPrizeClaimedContainer.SetActive (false);
		} else {
			CS_CupPrizeContainer.SetActive (false);
			CS_CupPrizeClaimedContainer.SetActive (true);
		}
		List<RunAbstract> thisSelectedRuns = new List<RunAbstract> ();
		if (activeCup.status == Cup.CupStatus.COMPLETED) {
			thisSelectedRuns = activeCup.completedSelectedRuns;
		} else {
			thisSelectedRuns = activeCup.selectedRuns;
		}

		RunUIElement.Index = 1;
		foreach (RunAbstract ra in thisSelectedRuns) {
			tmpRunJSON = DAO.getCupRunByIndex(ra.id);
			ra.goal = RunGoal.GetGoalTypeByGoalString( tmpRunJSON["goal"] );

			RunsScrn_Actions.Instance.AddRunUiEllement(ra);

			if (ra.IndexInQuest > 2 && ra.status == RunAbstract.RunStatus.COMPLETED) {
				RunsScrn_Actions.Instance.DefineScrollPosition (ra.IndexInQuest - 2);
			}

			RunUIElement.Index++;
		}
			
		//RunsScrn_Actions.Instance.ScrollTo ();
//		Debug.Log ("alon______________ CupsManager - SetCupScreenRuns() - finish - cup status: " + activeCup.status.ToString () + " , cup name: " + activeCup.name.ToString ());
	}


	public void StartRun(RunAbstract run){

		Cup activeCup;
		List <RunAbstract> thisSelectedRuns = new List <RunAbstract> ();

		if (GameManager.ActiveCupCompleted) {
			activeCup = GameManager.ActiveCompletedCup;
			thisSelectedRuns = activeCup.completedSelectedRuns;
		} else {
			activeCup = GameManager.ActiveCup;
			thisSelectedRuns = activeCup.selectedRuns;
		}

		if (!GameManager.ActiveCupCompleted && run.IndexInQuest > 0) {
			if (thisSelectedRuns [run.IndexInQuest - 1].status != RunAbstract.RunStatus.COMPLETED) {
				// LAST RUN NOT COMPLETED, CAN'T START THIS
				GameManager.Log( "Prev Run Not Completed" );
				return;
			}
		}

		GameManager.CurrentRun = run;

		GameManager.BenchInPreselectedRunMode = true;
		GameManager.PreselectedGameState = GameManager.GameState.CUP_RUN;
		//Bench.instance.CheckFitnessOfSelectedPlayer ();
		PlayerChooseManager.instance.onAction = false;



		// Check If Player Have enought energy to run
		if (PlayerChooseManager.instance.currentPlayerScript.data.fitness >= activeCup.cost) {      
			PlayerChooseManager.instance.StartCupRun ();
		} else {
            MainScreenUiManager.instance.RunsScrnAnimToggle (false);
            MainScreenUiManager.instance.CupScrnAnimToggle (false);
			MainScreenUiManager.instance.MidekKitsScreenToggle (true);
		}

		//Debug.Log ("alon__________ CupsManager - StartRun() - run status: " + run.status);
	}

	public void SetActiveCup(Cup cup){
			
		GameManager.ActiveCup = cup;
		GameManager.Instance.activeCupInstance = GameManager.ActiveCup;
	}


	public void ResetCupsAvailability(){

		foreach (Cup cp in cups) {
			cp.CheckAndSetCupStatus();
		}
		//Debug.Log ("alon_____________ CupsManager - ResetCupsAvailability()");
		ResetCupsUIElements ();
	}



//	public void TestCupLoose(){
//		GameManager.Instance.HandleCupLoose ();
//	}


	public void SetFirstCupAndRun(){
		StartCoroutine (StartCupCoroutine());
	}

	IEnumerator StartCupCoroutine(){
		yield return new WaitForSeconds (2);
//		Debug.Log ("alon_______ CupsManager - StartCupCoroutine()-------");
		cups[0].Start ();
		GameManager.Instance.NewCupStarted ();
		ResetCupsUIElements ();
		//yield return new WaitForFixedUpdate ();
		//SetCupScreenRuns (cups[0]);
	}

	public IEnumerator StartFirstRun(){
		if (!DAO.Instance.IsRunsDataReady) {
			GameManager.Instance.PromtAppNoInternet ();
			yield break;
		}
		SetCupScreenRuns (GameManager.ActiveCup);
		yield return new WaitForFixedUpdate ();
		GameManager.CurrentRun = GameManager.ActiveCup.selectedRuns[0];
		GameManager.BenchInPreselectedRunMode = true;
		PlayerChooseManager.instance.onAction = false;
		yield return new WaitForSeconds (1.5f);
		//GameManager.PreselectedGameState = GameManager.GameState.CUP_RUN;
		PlayerChooseManager.instance.StartCupRun();

		//Debug.Log ("alon_______ StartFirstRun()-----------------------");
		//Debug.Log ("alon_______ active cup id: " + GameManager.ActiveCup.id + " active cup name: " + GameManager.ActiveCup.name);
//		foreach (RunAbstract r in GameManager.ActiveCup.selectedRuns) {
//			Debug.Log ("alon_______ active cup - run id: " + r.id + " , active cup - run index: " + r.IndexInQuest + " , active cup - run goal: " + r.goal);
//		}

		//PlayerChooseManager.instance.StartCupRun ();
	}


	public void DebugFinishCup(){
		if (GameManager.ActiveCupCompleted) {
			return;
		}

		foreach(RunAbstract run in GameManager.ActiveCup.selectedRuns){
			run.status = RunAbstract.RunStatus.COMPLETED;
            run.GoalAchieved = 2;//Random.Range(1,2);
		}

		DAO.ActiveRunData = GameManager.ActiveCup.EncodeRuns ();
		DAO.Instance.LOCAL.SaveAll ();
		GameManager.ActiveCup.SaveProgress ();
		HandleCupWon ();
	}
		
}
