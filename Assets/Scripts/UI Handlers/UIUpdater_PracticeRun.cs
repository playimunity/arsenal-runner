﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIUpdater_PracticeRun : MonoBehaviour
{
	
	public static UIUpdater_PracticeRun Instance;
	int FRAMERATE = 10;
	int CUR_FRAME = 0;
	float PB_WIDTH = 696f;
	//float PB_HEIGHT = 116;

	public Text Whistles;
	public Text totalMagnetsTXT;
	public Text totalEnergyDrinksTXT;
	public Text PB_Text;
	public Image PB_Bar;
	//public Text LevelStringTXT;
	public Image LevelIntTXT;
	public PlayerData curPlayerData;

//	public Text PS_PlayerNumberAndName;
//	public Text PS_Level;
//	public Text PS_MaxFitness;
//	public Text PS_MetersTXT;
//	public Text PS_MetersToNextLevelTXT;

	public Text LS_PlayerNumber;
	public Text LS_PlayerName;
	//public Text LS_Level;
	//public Text LS_MaxFitness;
	public Text LS_ScoreMeters;
	public Text LS_CollectedCoins;
	public Text LS_BestScoreMeters;
	//public GameObject LS_RetryBtn;
	//public GameObject PS_ContinueBtn;
	public GameObject LS_BtnsParent;
	public GameObject PS_BtnsParent;
	public GameObject PS_Header;
	public GameObject GOS_Header;

	#region DoubleYourCoins

	public GameObject coinsContainer;
	public GameObject coinsWithAdsContainer;
	public GameObject watchAdBTN;
	public Text doubleYourCoinsContainerTXT;
	public Text coinsCollectedTXT;
	public Text doubleCoinsButtonTXT;
	public Text doubleCoinsHeaderTXT;
	public Text beforeDoubleCoins;
	public Text afterDoubleCoins;

	public GameObject ReplayVideoBTN;
	public GameObject StopRecordingAndWatchVideoBTN;
	bool ThereISRecord = false;

	//public Text DistanceCounter;
	//public Outline DCOutline;
	public Color DCHSOColor;

	//public Text saveMeFreeTXT;

	float PB_MIN_WIDTH = 110f;
	Vector2 TMP_Vector;

	bool updatePB = false;
	float PB_UpdateStep;

	string PB_Prefix;

	int curWhistlesAmount = 0;

	void Awake ()
	{
		RunManager.totalMeters = 0;
		Instance = this;
		PB_Prefix = DAO.Language.GetString ("meters-to-next-level");
	}

	void Start ()
	{
        //Transfer to NEW UI!!!!!
        LevelIntTXT = HeaderManager.instance.levelValueIMG;
        PB_Bar = HeaderManager.instance.levelFill;
		PB_Text = CrossSceneUIHandler.Instance.energyValue;
        //END
		doubleCoinsButtonTXT.text = "2X "+DAO.Language.GetString("only-coins");
		doubleCoinsHeaderTXT.text = DAO.Language.GetString("duc");
		//saveMeFreeTXT.text = DAO.Language.GetString ("free");
		curPlayerData = PlayerController.instance.data;
		//TMP_Vector = PB_Bar.sizeDelta;


		SetProgressBar ();
		//SetPauseScreen ();
		SetLooseScreen (true);
	}

	void FixedUpdate ()
	{
		if (CUR_FRAME < 0)
			CUR_FRAME = FRAMERATE;
		
		if (CUR_FRAME == 0)
			UpdateUI ();
		CUR_FRAME--;

		if (updatePB) {
			
//			TMP_Vector.x = Mathf.Lerp (TMP_Vector.x, PB_UpdateStep * curPlayerData.metersPassedToNextLevel, 2f * Time.deltaTime);

			TMP_Vector.x = PB_WIDTH - PB_UpdateStep * curPlayerData.metersPassedToNextLevel;
			if (TMP_Vector.x < PB_MIN_WIDTH)
				TMP_Vector.x = PB_MIN_WIDTH;
			//PB_Bar.sizeDelta = TMP_Vector;
            PB_Bar.fillAmount = PB_UpdateStep;
		}
	}

	
	//	public void SetPauseScreen(){
	//		PS_PlayerNumberAndName.text = "" + curPlayerData.player.playerNumber + " " + curPlayerData.player.playerName;
	//		PS_Level.text = DAO.Language.GetString("level") + " " + curPlayerData.level;
	//		PS_MaxFitness.text = curPlayerData.maxFitness.ToString();
	//		PS_MetersToNextLevelTXT.text = "" + DAO.Language.GetString("only") + " " + (curPlayerData.NextLevelSetup.MetersToThisLevel - curPlayerData.metersPassedToNextLevel) + " " + DAO.Language.GetString("meters-to-next-level");
	//		//PS_MetersTXT.text = DAO.Language.GetString("meters-to-next-level") +" " + (curPlayerData.NextLevelSetup.MetersToThisLevel - curPlayerData.metersPassedToNextLevel);
	//		PS_MetersTXT.text ="" + curPlayerData.metersPassedToNextLevel + DAO.Language.GetString("meters");
	//
	//	//	curPlayerData.NextLevelSetup.MetersToThisLevel  //  total meters in this level
	//	//	curPlayerData.metersPassedToNextLevel // how much i passed
	//	}





	public void DoubleYourCoinsClicked ()
	{
		#if UNITY_EDITOR
		DoubleCoins ();
		HeyzapWrapper.CallBackAdditive();
		#endif
		HeyzapWrapper.listener = delegate(string adState, string adTag) {
			if (adState.Equals ("incentivized_result_complete")) {
			DoubleCoins ();
			HeyzapWrapper.CallBackAdditive();
			}
			if (adState.Equals ("incentivized_result_incomplete")) {
				// The user did not watch the entire video and should not be given a   reward.
			}
		};
		HeyzapWrapper.SetListener ();
		HeyzapWrapper.Show ();
	}

	public void DoubleCoins ()
	{
		print ("****Coins Are Doubled!!****");
		//doubleYourCoinsContainerTXT.SetActive (true);
		doubleYourCoinsContainerTXT.gameObject.SetActive (true);
		watchAdBTN.SetActive (false);
		doubleYourCoinsContainerTXT.text = "X 2 = "+(RunManager.instance.coinsCollectedInThisRun * 2).ToString ("##,#");
		//afterDoubleCoins.text = LS_CollectedCoins.text = (RunManager.instance.coinsCollectedInThisRun * 2).ToString ("##,#");
		//DAO.TotalCoinsCollected += RunManager.instance.coinsCollectedInThisRun;
		CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (6 , RunManager.instance.coinsCollectedInThisRun);
		UnitedAnalytics.LogEvent ("Double Coins Clicked", UserData.Instance.userType.ToString());
	}

	public void CloseDoubleYourCoinsPopUp ()
	{
		
	}

	#endregion

	public void SetLooseScreen (bool pauseOn = false)
	{
		if (!pauseOn&&HeyzapWrapper.AreAdsAllowedForUser(HeyzapWrapper.AdType.DoubleYourCoins))
		{
		coinsWithAdsContainer.SetActive(HeyzapWrapper.IsAvailable);
		coinsContainer.SetActive(!HeyzapWrapper.IsAvailable);
		doubleYourCoinsContainerTXT.gameObject.SetActive (false);
		watchAdBTN.SetActive (true);
		}
		else
		{
		coinsWithAdsContainer.SetActive(false);
		coinsContainer.SetActive(true);
		}
			
		PS_BtnsParent.SetActive (false);
		LS_BtnsParent.SetActive (false);
		PS_Header.SetActive (false);
		GOS_Header.SetActive (false);
		SetScore ();

		LS_PlayerNumber.text = curPlayerData.player.playerNumber.ToString(); 
		LS_PlayerName.text = curPlayerData.player.playerName; 
		//LS_Level.text = DAO.Language.GetString("level") + " " + curPlayerData.level;
		//LS_Level.text = "LVL " + curPlayerData.level;
		//LS_MaxFitness.text = curPlayerData.maxFitness.ToString () + " " + DAO.Language.GetString ("fitness");
		if (RunManager.totalMeters != 0) {
			LS_ScoreMeters.text = RunManager.totalMeters.ToString ("##,#") + "m";
		} else {
			LS_ScoreMeters.text = "0m";
		}
		coinsCollectedTXT.text = LS_CollectedCoins.text = RunManager.instance.coinsCollectedInThisRun.ToString ();
		LS_BestScoreMeters.text = DAO.BestPracticeScore.ToString ("##,#") + "m";

		if (pauseOn) {
			//PS_ContinueBtn.SetActive (true);
			PS_BtnsParent.SetActive (true);
			PS_Header.SetActive (true);
		} else {
			//LS_RetryBtn.SetActive (true);
			LS_BtnsParent.SetActive (true);
			GOS_Header.SetActive (true);
		}

	}

	void SetScore ()
	{
		if (RunManager.totalMeters > DAO.BestPracticeScore) {
			DAO.BestPracticeScore = RunManager.totalMeters;
			NativeSocial._.PostScore (DAO.BestPracticeScore); // report to leaderboard
		}
	}

	
	void UpdateUI ()
	{
		totalMagnetsTXT.text = DAO.TotalMagnetsCollected.ToString ();
		totalEnergyDrinksTXT.text = DAO.TotalSpeedBoostsCollected.ToString ();

		UpdateWhistles ();
	}

	
	public void RestartScene ()
	{
		GameManager.SwitchState (GameManager.GameState.INFINITY_RUN);
	}

	public void SetProgressBar ()
	{
		if (curPlayerData.level >= curPlayerData.MaxLevel) {
			SetProgressBarToMax ();
			return;
		}
		LevelIntTXT.sprite = HeaderManager.instance.levelImages[curPlayerData.level];
        PB_UpdateStep = (float)curPlayerData.metersPassedToNextLevel / (float)curPlayerData.NextLevelSetup.MetersToThisLevel;
		//PB_Text.text = PB_Prefix + " " + (curPlayerData.NextLevelSetup.MetersToThisLevel - curPlayerData.metersPassedToNextLevel);
		PB_Text.text = "" + curPlayerData.fitness + "/" + curPlayerData.maxFitness;
//		updateDistanceCounter ();
		updatePB = true;
	}

	public void SetProgressBarToMax ()
	{
		updatePB = false;
        LevelIntTXT.sprite = HeaderManager.instance.levelImages[curPlayerData.level];
		//PB_UpdateStep = PB_WIDTH / curPlayerData.NextLevelSetup.MetersToThisLevel;
		TMP_Vector.x = PB_WIDTH;
		//PB_Bar.sizeDelta = TMP_Vector;
        PB_Bar.fillAmount = PB_UpdateStep;
		PB_Text.text = DAO.Language.GetString ("max-lvl");
		//updateDistanceCounter ();
	}

	public void UpdateProgressBar ()
	{
		PB_Text.text = PB_Prefix + (curPlayerData.NextLevelSetup.MetersToThisLevel - curPlayerData.metersPassedToNextLevel);
	}


	bool DCChangedColor = false;

//	public void updateDistanceCounter ()
//	{
//		if (RunManager.totalMeters == 0)
//			DistanceCounter.text = "0m";
//		else
//			DistanceCounter.text = RunManager.totalMeters.ToString ("##,#") + " m";

//		if (RunManager.totalMeters > DAO.BestPracticeScore && !DCChangedColor) {
//			DCChangedColor = true;
//			DCOutline.effectColor = DCHSOColor;
//		}
//	}


	public GameObject[] whistlesArray;
	public void UpdateWhistles ()
	{
		if (curWhistlesAmount == PlayerController.instance.HitsRemain)
			return;

		curWhistlesAmount = PlayerController.instance.HitsRemain;

		for (int i = 0; i < whistlesArray.Length; i++) {
			if (i < curWhistlesAmount) {
				whistlesArray [i].SetActive (true);
			} else {
				whistlesArray [i].SetActive (false);
			}
		}

		//Whistles.text = "";
		//for (int i = 0; i < curWhistlesAmount; i++) {
			//Whistles.text += "E ";

		//}

	}





}
