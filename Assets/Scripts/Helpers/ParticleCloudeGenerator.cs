﻿using UnityEngine;
using System.Collections;

public class ParticleCloudeGenerator : MonoBehaviour {

	public static ParticleCloudeGenerator instance;

	public ParticleSystem particleCloud;
	public ParticleSystem particleWhistle;
	public ParticleSystem particleOnlyWhistle;
	public ParticleSystem particleBoom;
	public ParticleSystem particleCoin;

	bool active = false;

	void Awake(){
		instance = this;
	}


	void Update(){
		if (active) {
			transform.Translate(Vector3.forward * 8f * Time.deltaTime);
		}
	}


	public void ParticleWhistle(Vector3 pos , Vector3 rot){
		transform.position = pos;
		transform.eulerAngles = rot;
		active = true;
		particleWhistle.gameObject.SetActive (true);
		particleWhistle.Play ();
		StartCoroutine (WaitForDeactivate());
	}

	public void ParticleOnlyWhistle(Vector3 pos , Vector3 rot){
		transform.position = pos;
		transform.eulerAngles = rot;
		active = true;
		particleOnlyWhistle.gameObject.SetActive (true);
		particleOnlyWhistle.Play ();
		StartCoroutine (WaitForDeactivate());
	}

	public void ParticleBoom(Vector3 pos , Vector3 rot){
		transform.position = pos;
		transform.eulerAngles = rot;
		active = true;
		particleBoom.gameObject.SetActive (true);
		particleBoom.Play ();
		StartCoroutine (WaitForDeactivate());
	}

	public void ParticleCloud(Vector3 pos){
		transform.position = pos;
		//active = true;
		particleCloud.gameObject.SetActive (true);
		//particleCloud.Play ();
		StartCoroutine (WaitForDeactivate());
	}

	public void ParticleCoin(Vector3 pos){
		transform.position = pos;
		//active = true;
		particleCoin.gameObject.SetActive (true);
		//particleCloud.Play ();
		StartCoroutine (WaitForDeactivate());
	}

	static WaitForSeconds _wfs;
	static WaitForSeconds wfs{
		get{
			if (_wfs == null)
				_wfs = new WaitForSeconds (1f);

			return _wfs;
		}
	}

	IEnumerator WaitForDeactivate(){
		yield return wfs;
		//particleCloud.Stop ();
		particleCloud.gameObject.SetActive (false);
		//particleWhistle.Stop ();
		particleWhistle.gameObject.SetActive (false);
		particleOnlyWhistle.gameObject.SetActive (false);
		particleBoom.gameObject.SetActive (false);
		particleCoin.gameObject.SetActive (false);
		active = false;
	}
	

}
