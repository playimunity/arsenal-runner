﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class QuestMap : MonoBehaviour {

	public static QuestMap instance;

	public RectTransform scrollContent;
	public ScrollRect sr;

	Vector2 pos;

	void OnEnable(){
        InvokeRepeating ("CloseCoinsCounter", 0.2f, 1.5f);

		//DefineScrollPos ();
    }

    void OnDisable(){
        CancelInvoke ("CloseCoinsCounter");
		MainScreenUiManager.instance.FreeGiftBtnAnimToggle (MainScreenUiManager.instance.isFreeGifts);
		//Debug.Log ("alon________ QuestMap - OnDisable() - isFreeGifts = " + GameManager.Instance.isFreeGifts);
    }

    void CloseCoinsCounter(){
		//Debug.Log ("alon________ ###### CLOSE COINS!!!");
        CrossSceneUIHandler.Instance.hideCoinsCounter ();
		MainScreenUiManager.instance.FreeGiftBtnAnimToggle (false);
    } 


	void Awake(){
		instance = this;
	}


	public void DifineMapScrollY(){
//		Debug.Log ("alon________ DifineMapScrollY() - DAO.MapScrollY: " + DAO.MapScrollY);
//		sr.onValueChanged.AddListener ((Vector2 new_pos) => {
//			DAO.MapScrollY = scrollContent.anchoredPosition.y;
//			//Debug.Log ("alon________ DifineMapScrollY() - inside - DAO.MapScrollY: " + DAO.MapScrollY);
//		});
		DAO.MapScrollY = scrollContent.anchoredPosition.y;
//		Debug.Log ("alon________ DifineMapScrollY() - after - DAO.MapScrollY: " + DAO.MapScrollY);
	}

	public void DefineScrollPos(){
		pos = scrollContent.anchoredPosition;

		if (DAO.MapScrollY < 0f) {
			ScrollToStart ();
		} else {
			ScrollTo ( DAO.MapScrollY );
//			Debug.Log ("alon________ DAO.MapScrollY: " + DAO.MapScrollY);
		}

	}

	public void ScrollToStart(){
		Canvas.ForceUpdateCanvases();
		sr.verticalNormalizedPosition = 0f;
//		Debug.Log ("alon_____________ ScrollToStart()");
	}

	public void ScrollTo(float y){
		pos.y = y;
		Canvas.ForceUpdateCanvases();
		scrollContent.anchoredPosition = pos;
//		Debug.Log ("alon_____________ ScrollTo(float y) - pos.y: " + pos);
	}

}
