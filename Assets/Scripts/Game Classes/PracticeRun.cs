using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

[System.Serializable]
public class PracticeRun{

	public List<WorldSectionAbstract> sections;

	public PracticeRun(JSONNode run){

		sections = new List<WorldSectionAbstract>();
		for (int i=0; i < run.Count; i++) {
			sections.Add( new WorldSectionAbstract( run[i] ) );
		}

	}

}
