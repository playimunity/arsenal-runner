﻿using UnityEngine;
using System.Collections;

public class WorldSectionTrigger : MonoBehaviour {


	public bool isEnabled = true;
	//public bool sectionOf3;
	public bool isJumpAllowed = true;
	public bool is_T_Section = false;
	public bool isSubway = false;
	//public WorldSection.RegionType Region;
    public WorldSection section;

	public bool up;
	public bool down;

	static bool preEndSectionAchived = false;
	static bool endSectionAchived = false;

	public int direction = 0;

	WaitForSeconds waitForStaticPos = new WaitForSeconds (1.5f);

//	int numberOfSections = 5;
//	int SectionsIndex = 0;
//	float speedIncreeseFactor = 1f;
//	float maxSpeed = 30f;
    void Start()
    {
		preEndSectionAchived = false;
		endSectionAchived = false;

        section = GetComponentInParent<WorldSection>();

		if (direction == 11 && !endSectionAchived) {
			endSectionAchived = true;
			RunManager.instance.SetEndingScenePosition(transform);
//			Debug.Log ("alon_________ should activate ending scene !!! ");
		}
    }
	
	void OnTriggerEnter(Collider col)
    {

        if (!isEnabled || !col.gameObject.tag.Equals("Player"))
            return;

        isEnabled = false;
	
        PlayerController.instance.canJump = isJumpAllowed;



		switch (section.currentRegionType) {
		case (WorldSection.RegionType.HR):
			{
//				if (section.currentRegionType == RunManager.currentRegion)
//					return;
				//CameraLookAt.instance.SetCameraPosition (4f, -4.5f, 5f);
				CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
				//CameraFollow.instance.downUp = false;
				//CameraFollow.instance.CameraBackToPlace ();
				break;
			}
		case (WorldSection.RegionType.OS):
			{
				CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
				break;
			}
		case (WorldSection.RegionType.TB):
			{
//				if (up) {
//					CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
//					CameraFollow.instance.downUp = true;
//					CameraFollow.instance.staticYPos += 5;
//					//StartCoroutine (ChangePlayerStaticY(5));
//					//PlayerController.instance.staticMinYPos += 5;
//				} else if (down) {
//					CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
//					CameraFollow.instance.downUp = true;
//					CameraFollow.instance.staticYPos -= 5;
//					//StartCoroutine (ChangePlayerStaticY(-5));
//					PlayerController.instance.staticMinYPos -= 5;
//				} else {
//					CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
//					CameraFollow.instance.downUp = false;
//					CameraFollow.instance.CameraBackToPlace ();
//				}
				CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
				//CameraFollow.instance.downUp = false;
				//CameraFollow.instance.CameraBackToPlace ();
				break;
			}
		case WorldSection.RegionType.SJP:
                
			if (section.currentSectionType == WorldSection.SectionType.SJP_Bridge) {
				CameraLookAt.instance.SetCameraPosition (5.8f, -5f, 15f);
				//CameraFollow.instance.downUp = false;
				//CameraFollow.instance.CameraBackToPlace ();
			} else {
				CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
				//CameraFollow.instance.downUp = false;
				//CameraFollow.instance.CameraBackToPlace ();
			}
			break;

		default:
			{
				CameraLookAt.instance.SetCameraPosition (4.8f, -5f, 15f);
				//CameraFollow.instance.downUp = false;
				//CameraFollow.instance.CameraBackToPlace ();
				break;
			}
		}



//		if (sectionOf3)
//			PlayerController.instance.sectionOf3 = true;
//		else
//			PlayerController.instance.sectionOf3 = false;


		if (direction == 10 && !preEndSectionAchived) {
			preEndSectionAchived = true;
//			Debug.Log ("alon___________ preEndSectionAchived = true ");
			RunManager.instance.PreEndSectionAchieved();
		} else if(direction == 11){
			RunManager.instance.EndSectionAchieved();
		}else{
			RunManager.OnSectionAchieved (direction, is_T_Section, section.currentRegionType);
		}
			
		PlayerController.instance.IncreaseSpeed ();

//		if (section.currentSectionType == WorldSection.SectionType.Alley) {
//			if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
//				CupRunController.Instance.RedFrameToggle ();
//			}
//			if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
//				PracticeRunController.Instance.RedFrameToggle ();
//			}
//		}
	}




	IEnumerator ChangePlayerStaticY(int value){
		yield return waitForStaticPos;
		PlayerController.instance.staticMinYPos += value;
	}

}
