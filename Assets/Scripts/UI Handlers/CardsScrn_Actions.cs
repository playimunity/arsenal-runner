using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Facebook.Unity;

public class CardsScrn_Actions : MonoBehaviour {

	public static CardsScrn_Actions instance;


	public bool isDrawAllowed = false;

	public Animator uiAnimator;
	public Animator cardsAnimator;

	public int cardsCount;
	public Text cardsCountText;
	public ButtonClickAnimation cardsBtnAnim;

	float cardGap = 20f;

	public RectTransform emptyCard;
	RectTransform card1;
	RectTransform card2;
	RectTransform card3;

	GameObject tempCard;
	public GameObject cardPrefab;
	public GameObject claimPrizeBtn;
	public GameObject tutPopupCollectBtn;
	public GameObject tutPopupSendBtn;
	//public GameObject lastCardPopup;

	public Transform card1Parent;
	public Transform card2Parent;
	public Transform card3Parent;
	public Transform movingCard;

	Vector2 card1Pos;
	Vector2 card2Pos;
	Vector2 card3Pos;


	public GameObject RequestGiftBtn;
	public GameObject ClaimGiftBtn;
	public Text ClaimCounter;
	public GameObject SendGiftBtn;
	public GameObject SendBackGiftBtn;
	public Text RequestsCounter;

	public GameObject NC_RequestBtn;
	public GameObject NC_CollectBtn;
	public Text NC_CollectCounter;


	public GameObject ClaimScreen;
	//public RectTransform ClaimScreenIconContainer;

	void Awake(){
		instance = this;
		UpdateGiftCardsCount ();
	}

	public void UpdateGiftCardsCount(){
		if (DAO.TotalGiftCardsCollected < 0f) DAO.TotalGiftCardsCollected = 0;

		cardsCount = (int)DAO.TotalGiftCardsCollected;
		cardsCountText.text = cardsCount.ToString ();

//		if (cardsCount > 0) {
//			cardsBtnAnim.BounceToggle (true);
//		} else {
//			cardsBtnAnim.BounceToggle (false);
//		}
	}

	void Start () {

		card1Pos = emptyCard.anchoredPosition;
		card2Pos = new Vector2 (card1Pos.x - cardGap, card1Pos.y - cardGap);
		card3Pos = new Vector2 (card2Pos.x - cardGap, card2Pos.y - cardGap);

		CreateCards ();
	
	}

	public void OnEnable(){
		InvokeRepeating ("UpdateButtonsAndCounters", 0f, 7f);
	}

	public void OnDisable(){
		CancelInvoke ("UpdateButtonsAndCounters");
	}

	public void UpdateButtonsAndCounters(){
		if (DDB._.RecievedGiftCards.Count > 0) {
			RequestGiftBtn.SetActive (false);
			ClaimGiftBtn.SetActive (true);
			//NC_CollectBtn.SetActive (true);
			//NC_RequestBtn.SetActive (false);
			tutPopupCollectBtn.SetActive (true);
			tutPopupSendBtn.SetActive (false);
			NC_CollectCounter.text = DDB._.RecievedGiftCards.Count.ToString ();
			ClaimCounter.text = DDB._.RecievedGiftCards.Count.ToString ();
		} else {
			RequestGiftBtn.SetActive (true);
			ClaimGiftBtn.SetActive (false);
			//NC_CollectBtn.SetActive (false);
			//NC_RequestBtn.SetActive (true);
			tutPopupCollectBtn.SetActive (false);
			tutPopupSendBtn.SetActive (true);
		}

		if (DDB._.RecievedGiftCardsRequests.Count > 0) {
			SendGiftBtn.SetActive (false);
			SendBackGiftBtn.SetActive (true);
			RequestsCounter.text = DDB._.RecievedGiftCardsRequests.Count.ToString ();
		} else {
			SendGiftBtn.SetActive (true);
			SendBackGiftBtn.SetActive (false);
		}
	}

//	bool cardScrnOn = false;
//	public void CardScrnToggle(bool on){
//		if (on && !cardScrnOn) {
//			cardScrnOn = true;
//			uiAnimator.Play ("cards_screen_in");
//			Debug.Log ("alon_______ open gift cards !");
//			CrossSceneUIHandler.Instance.FreeGiftBtnAnimToggle (false);
//			GM_Input._.GiftCards = true;
//		} else if (!on && cardScrnOn) {
//			cardScrnOn = false;
//			GM_Input._.GiftCards = false;
//			uiAnimator.Play ("cards_screen_out");
//			Debug.Log ("alon_______ close gift cards !");
//			CrossSceneUIHandler.Instance.FreeGiftBtnAnimToggle (GameManager.Instance.isFreeGifts);
//
//			if (DAO.TotalGiftCardsSent > 10) {
//				NativeSocial._.ReportAchievment (SocialConstants.achievement_scratch_my_back);
//			}
//			if (DAO.TotalGiftCardsSent > 50) {
//				NativeSocial._.ReportAchievment (SocialConstants.achievement_donator);
//			}
//			if (DAO.TotalGiftCardsSent > 100) {
//				NativeSocial._.ReportAchievment (SocialConstants.achievement_bff);
//			}
//
//			if (DAO.TotalGiftCardsRecieved > 10) {
//				NativeSocial._.ReportAchievment (SocialConstants.achievement_friendly_player);
//			}
//			if (DAO.TotalGiftCardsRecieved > 50) {
//				NativeSocial._.ReportAchievment (SocialConstants.achievement_rising_star);
//			}
//			if (DAO.TotalGiftCardsRecieved > 100) {
//				NativeSocial._.ReportAchievment (SocialConstants.achievement_people_just_loves_me);
//			}
//		}
//	}



	public void CreateCards(){

		UpdateGiftCardsCount ();

		isDrawAllowed = true;

		if (cardsCount > 2) {

			emptyCard.gameObject.SetActive (false);
			//LastCardAnimToggle (false);

			if(card3 != null) return;
			
			tempCard = (GameObject)Instantiate (cardPrefab, transform.position, transform.rotation);
			card3 = tempCard.GetComponent<RectTransform> ();
			card3.transform.SetParent (card3Parent, false);
			card3.anchoredPosition = card3Pos;

			if(card2 != null) return;
			
			tempCard = (GameObject)Instantiate (cardPrefab, transform.position, transform.rotation);
			card2 = tempCard.GetComponent<RectTransform> ();
			card2.transform.SetParent (card2Parent, false);
			card2.anchoredPosition = card2Pos;

			if(card1 != null) return;

			tempCard = (GameObject)Instantiate (cardPrefab, transform.position, transform.rotation);
			card1 = tempCard.GetComponent<RectTransform> ();
			card1.transform.SetParent (card1Parent, false);
			card1.anchoredPosition = card1Pos;
			card1.GetComponent<GiftCard>().enabled = true;
			card1.GetComponent<GiftCard> ().Build ();
			return;
		}

		switch (cardsCount) {
		
		case 2:
			emptyCard.gameObject.SetActive(false);
			//LastCardAnimToggle (false);

			if(card2 != null) return;

			tempCard = (GameObject)Instantiate(cardPrefab , transform.position , transform.rotation);
			card2 = tempCard.GetComponent<RectTransform>();
			card2.transform.SetParent (card2Parent , false);
			card2.anchoredPosition = card2Pos;

			if(card1 != null) return;

			tempCard = (GameObject)Instantiate(cardPrefab , transform.position , transform.rotation);
			card1 = tempCard.GetComponent<RectTransform>();
			card1.transform.SetParent (card1Parent , false);
			card1.anchoredPosition = card1Pos;
			card1.GetComponent<GiftCard>().enabled = true;
			card1.GetComponent<GiftCard> ().Build ();
			break;

		case 1:
			emptyCard.gameObject.SetActive(true);
			//LastCardAnimToggle (false);

			if(card1 != null) return;

			tempCard = (GameObject)Instantiate(cardPrefab , transform.position , transform.rotation);
			card1 = tempCard.GetComponent<RectTransform>();
			card1.transform.SetParent (card1Parent , false);
			card1.anchoredPosition = card1Pos;
			card1.GetComponent<GiftCard>().enabled = true;
			card1.GetComponent<GiftCard> ().Build ();
			break;

		case 0:
			emptyCard.gameObject.SetActive (true);
			//Debug.Log ("alon___ CreateCards() - 0 cards");
			//LastCardAnimToggle (true);
			isDrawAllowed = false;

			card1 = null;
			
			card2 = null;
			
			card3 = null;
			break;
		}
	}




	public void Win(GiftCard.Prizes prize){
		DAO.TotalGiftCardsCollected--;
		card1.GetComponent<GiftCard>().enabled = false;
		isDrawAllowed = false;
		StartCoroutine (WinCoroutine(prize));
		//Debug.Log ("alon___ card1.GetComponent<GiftCard>().enabled = false");
	}

	IEnumerator WinCoroutine(GiftCard.Prizes prize){
		yield return new WaitForSeconds (2f);

		ShowClaimScreen (prize);
	}

	public void ShowClaimScreen(GiftCard.Prizes prize){
        
		ClaimScreen.SetActive (true);
		ClaimScreenHandler._.SetView (prize);
		ClaimScreenHandler._.isGiftCard = true;
		uiAnimator.Play("ClaimScreen_Show");
	}

	public void HideClaimScreen(){
		uiAnimator.Play("ClaimScreen_Hide");
	}


	public void OnPrizeClaimed(){
		
//		if (cardsCount == 0) {
//			//emptyCard.gameObject.SetActive (true);
//			LastCardAnimToggle (true);
//		}

		//uiAnimator.Play("card win btn press", 1);
		StartCoroutine (UpdateCards());
		ClaimScreen.SetActive (false);

	}



	IEnumerator UpdateCards(){
		

		card1.transform.SetParent (movingCard, false);

		if (cardsCount > 1) {
			card2.GetComponent<GiftCard> ().enabled = true;
		}

		yield return new WaitForSeconds (0.5f);
		cardsAnimator.Play("card_out");
		yield return new WaitForSeconds (0.5f);
		Destroy(card1.gameObject);

		UpdateGiftCardsCount ();

		yield return new WaitForFixedUpdate ();

		if (cardsCount > 2) {
			tempCard = (GameObject)Instantiate (cardPrefab, transform.position, transform.rotation);
			tempCard.transform.SetParent (card3Parent, false);
			tempCard.GetComponent<RectTransform> ().anchoredPosition = card3Pos;

			card1 = card2;
			card1.transform.SetParent (card1Parent, false);
			card1.GetComponent<GiftCard> ().Build ();

			card2 = card3;
			card2.transform.SetParent (card2Parent, false);

			StartCoroutine (CardSwitchAnim (card1, card1Pos));
			StartCoroutine (CardSwitchAnim (card2, card2Pos));
			
			card3 = tempCard.GetComponent<RectTransform> ();
			
			yield break;
		}
		
		switch (cardsCount) {
			
		case 2:

			card1 = card2;
			card1.transform.SetParent (card1Parent, false);
			card1.GetComponent<GiftCard> ().Build ();

			card2 = card3;
			card2.transform.SetParent (card2Parent, false);

			card3 = null;
			
			StartCoroutine (CardSwitchAnim (card1, card1Pos));
			StartCoroutine (CardSwitchAnim (card2, card2Pos));
			
			break;
			
		case 1:
			card1 = card2;
			card1.transform.SetParent (card1Parent, false);
			card1.GetComponent<GiftCard> ().Build ();

			card2 = null;
			
			StartCoroutine (CardSwitchAnim (card1, card1Pos));

			break;
			
		case 0:
			card1 = null;
			isDrawAllowed = false;
			LastCardAnimToggle (true);
			break;
		}
	}

	WaitForEndOfFrame _weof;
	WaitForEndOfFrame weof{
		get{ 
			if (_weof == null) _weof = new WaitForEndOfFrame ();
			return _weof;
		}
	}

	IEnumerator CardSwitchAnim(RectTransform obj , Vector2 targetPos){
		for (float i=0f; i <= 1f; i += (2f * Time.deltaTime)) {
			obj.anchoredPosition = Vector2.Lerp (obj.anchoredPosition , targetPos , i);
			if (i >= 0.4f) i = 1f;
			yield return _weof;
		}
		if (cardsCount == 1) {
			emptyCard.gameObject.SetActive (true);
		}
		isDrawAllowed = true;
	}



//	public Transform GetClaimScreenIconContainer(){
//		return ClaimScreenIconContainer;
//	}




	public void OnSendGiftsClicked(){

		if (FB.IsLoggedIn) {
			CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.SEND_GIFTCARDS);
		} else {
			FBModule.OnFacebookProfileRecieved += OnFBLoggedIn_SendGift;
			FBModule.Instance.FBButonClicked = true;
			FBModule.Instance.Login ();
		}
	}

	public void OnRequestGiftsClicked(){

		if (FB.IsLoggedIn) {
			CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.ASK_FOR_GIFTCARDS);
		} else {
			FBModule.OnFacebookProfileRecieved += OnFBLoggedIn_RequestGift;
			FBModule.Instance.FBButonClicked = true;
			FBModule.Instance.Login ();
		}
	}

	public void OnClaimGiftsClicked(){
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.CLAIM_GIFTCARDS);
	}

	public void OnGiftRequestsGiftsClicked(){
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.GIFTCARDS_REQUESTS);
	}



	void OnFBLoggedIn_SendGift(){
		FBModule.OnFacebookProfileRecieved -= OnFBLoggedIn_SendGift;
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.SEND_GIFTCARDS);
	}

	void OnFBLoggedIn_RequestGift(){
		FBModule.OnFacebookProfileRecieved -= OnFBLoggedIn_RequestGift;
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.ASK_FOR_GIFTCARDS);
	}


	public void ShowTutFillLuckyPopup(){
		if (Tutorial.IsNeedToShowCardsHandTut) {
			GCTutPopupFillLuckyToggle (true);
			//CardsScrn_Actions.instance.isDrawAllowed = false;
			//ShowCardsHandTut ();
		}
	}

	public void ShowCardsHandTut(){    // driven by animation event
		cardsAnimator.Play ("Tutorial_anim_in");
		//DAO.CardsHandTut = 0;
	}

	public void HideCardsHandTut(){
		cardsAnimator.Play ("Tutorial_anim_out");
		//LocalDataInterface.Instance.cardsHandTut = 1;
		DAO.CardsHandTut = 1;
//		Debug.Log ("alon____________ should aliminate the hand!");
	}

	public void TutPopupClaimPlay(bool powerUps){
		StartCoroutine (TutPopupClaimCoroutine (powerUps));
	}

	IEnumerator TutPopupClaimCoroutine(bool powerUps){
		yield return new WaitForSeconds (0.25f);
		if (powerUps && Tutorial.IsNeedToShowCardsPowerUpsTut) {
			GCTutPopupPowerUpsToggle (true);
		}

//		if (powerUps && Tutorial.IsNeedToShowCardsPowerUpsTut) {
//			GCTutPopupPowerUpsToggle (true);
//		} else if (Tutorial.IsNeedToShowCardsHandTut) {
//			GCTutPopupGetFreeToggle (true);
//		}
	}

	bool lastCardOn = false;
	public void LastCardAnimToggle(bool on){
		if (on && !lastCardOn) {
			cardsAnimator.Play ("GC_tut_Get_Free_in");
			lastCardOn = true;
		} else if (!on && lastCardOn) {
			cardsAnimator.Play ("GC_tut_Get_Free_out");
			lastCardOn = false;
		}
	}

	public void GCTutPopupFillLuckyToggle(bool on){
		if (on) {
			cardsAnimator.Play ("GC_tut_fill_lucky_in");
		} else {
			cardsAnimator.Play ("GC_tut_fill_lucky_out");
		}
	}

	public void GCTutPopupPowerUpsToggle(bool on){
		if (on) {
			cardsAnimator.Play ("GC_tut_Power_Ups_in");
			DAO.CardsPowerUpsTut = 1;
		} else {
			cardsAnimator.Play ("GC_tut_Power_Ups_out");
		}
	}


}
