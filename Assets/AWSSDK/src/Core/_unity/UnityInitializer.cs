﻿//
// Copyright 2014-2015 Amazon.com, 
// Inc. or its affiliates. All Rights Reserved.
// 
// Licensed under the Amazon Software License (the "License"). 
// You may not use this file except in compliance with the 
// License. A copy of the License is located at
// 
//     http://aws.amazon.com/asl/
// 
// or in the "license" file accompanying this file. This file is 
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, express or implied. See the License 
// for the specific language governing permissions and 
// limitations under the License.
//
using UnityEngine;

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;

using Amazon;
using Amazon.Runtime.Internal.Util;
using Amazon.Runtime.Internal;
using Amazon.Util;
using System.Threading;
using Amazon.Util.Internal;
using Logger = Amazon.Runtime.Internal.Util.Logger;

#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Amazon
{ 
    /// <summary>
    /// A singleton instance of which will initialize the Main thread dispatcher & logger.
    /// This class is linked to the AWSPrefab so that user can make any configuration 
    /// changes and instance will be included in every scene via the prefab.
    /// </summary>
    public class UnityInitializer : MonoBehaviour
    {

        private static UnityInitializer _instance = null;
        private static object _lock = new object();
        private Logger _logger; 
		private string _monitor;
		private bool _oneTime;
		private bool _trackLogActive;
        
        #region constructor
        private UnityInitializer(){}
        #endregion

        #region public api
        public static UnityInitializer Instance
        {
            get
            {
                return _instance;
            }
        }

        public void Awake()
        {

            lock (_lock)
            {
                if (_instance == null)
                {
                    // singleton instance
                    _instance = this;
					_monitor = "";
					_oneTime = false;
					_trackLogActive = true;
                    if (_mainThread == null || !_mainThread.Equals(Thread.CurrentThread))
                        _mainThread = Thread.CurrentThread;

					//UnityEngine.Debug.Log("RON____________xUnityInitializer start Time:"+ DateTime.Now.ToLongTimeString());
					addToMonitorLog("UnityInitializer start");
                    // preventing the instance from getting destroyed between scenes
                   // DontDestroyOnLoad(this);

                    //initialize the logger
                    TraceListener tracelistener = new UnityDebugTraceListener("UnityDebug");
                    AWSConfigs.AddTraceListener("Amazon", tracelistener);

                    // Associate the main thread dispatcher
                    _instance.gameObject.AddComponent<UnityMainThreadDispatcher>();

                    // add callback for Editor Mode change
#if UNITY_EDITOR
                    IsEditorPlaying = EditorApplication.isPlaying;
                    EditorApplication.playmodeStateChanged += this.HandleEditorPlayModeChange;
#endif
                    AmazonHookedPlatformInfo.Instance.Init();

                }
                else
                {
                    if (this != _instance)
                    {
#if UNITY_EDITOR
                        DestroyImmediate(this.gameObject);
#else
                            Destroy (this.gameObject);
#endif
                    }
                }

            }
        }
        #endregion

        private static Thread _mainThread;
        /// <summary>
        /// Checks if the thread is a game/main/unity thread
        /// </summary>
        /// <returns>true if the thread is the game/main/unity thread, else false</returns>
        public static bool IsMainThread()
        {
            if(null == _mainThread)
            {
                  throw new Exception("Main thread has not been set, is the AWSPrefab on the scene?");
            }
            return Thread.CurrentThread.Equals(_mainThread);
        }

		public void addToMonitorLog(string sError)
		{
			if (_trackLogActive)
				_monitor = String.Format ("{0} {1}::{2}\n", _monitor, sError, Time.unscaledTime.ToString ());
				//_monitor += sError + "::" + Time.unscaledTime.ToString () + "\n";
		}

		public string getMonitorLog()
		{
			return _monitor;
		}

		public void CleanMonitorLog()
		{
			_monitor="";
		}

		public void StopTracking()
		{
			_monitor="";
			_trackLogActive = false;
			_oneTime = true;
		}

		public void StartTracking()
		{
			_trackLogActive = true;
			_oneTime = false;
		}

		public void saveDataToPHP(){
			StartCoroutine (SendLogData ());
		}

		private IEnumerator SendLogData(){
			//UnityEngine.Debug.Log ("RON_______SendLogData  Time:" + DateTime.Now.ToLongTimeString ());
			if ((!_oneTime) && (!String.IsNullOrEmpty(getMonitorLog())) &&
				((DAO.Instance == null) || (DAO.Instance != null && DAO.Instance.settings.timeToMonitorLog > 0 && (Time.unscaledTime < float.Parse(DAO.Instance.settings.timeToMonitorLog.ToString()))))) 
			{
				//UnityEngine.Debug.Log("RON_______SendLogData inside:"+ DateTime.Now.ToLongTimeString());
				_oneTime = true;
				string sDatetime = DateTime.Now.ToString ("yyyyMMdd_HHmmss");
				string LocalVersion;
				if (DAO.Instance != null) {
					LocalVersion = DAO.Instance.LOCAL.dataVersion.ToString ();
				} else
					LocalVersion = "NoDAO";
			

				WWWForm form = new WWWForm ();
				form.AddField ("log", "log");
				form.AddField ("msg", String.Format ("{0} \n {1} \n {2} \n {3} \n {4} \n", SystemInfo.deviceType, SystemInfo.operatingSystem, SystemInfo.systemMemorySize.ToString(), LocalVersion, getMonitorLog ()));
				form.AddField ("time", String.Format ("{0}_{1}",sDatetime ,SystemInfo.deviceModel.ToString()));
				form.AddField ("user", SystemInfo.deviceUniqueIdentifier);

				byte[] rawData = form.data;
				string url = "https://fcb.gamehour.com/data/logExceptionp2.php";
				if ((DAO.Instance != null) && (DAO.IsInDebugMode)) {
					url = "https://fcbstage.gamehour.com/data/logExceptionp2.php";
				} 
			   
				WWW www = new WWW (url, rawData);
				//UnityEngine.Debug.Log ("RON_______SendLogData  Time:" + url);
				yield return www;

			}
		}

#if UNITY_EDITOR
        public static bool IsEditorPlaying
        {
            get;
            private set;
        }

        public static bool IsEditorPaused
        {
            get;
            private set;
        }

        private void HandleEditorPlayModeChange()
        {
            IsEditorPlaying = EditorApplication.isPlaying;
            IsEditorPaused = EditorApplication.isPaused;
        }
#endif
    }
}
