﻿using UnityEngine;
using System.Collections;

public class RagDollToggle : MonoBehaviour {

	public static RagDollToggle instance;

	Rigidbody[] bodies;
	CapsuleCollider[] capsuleColliders;
	BoxCollider[] boxColliders;
	SphereCollider[] sphereColliders;

	void Awake(){
		instance = this;
	}

	void Start(){
		bodies = GetComponentsInChildren<Rigidbody>();

		capsuleColliders = GetComponentsInChildren<CapsuleCollider>();
		boxColliders = GetComponentsInChildren<BoxCollider>();
		sphereColliders = GetComponentsInChildren<SphereCollider>();

		EnableColliders (false);
	}

	public void EnableColliders(bool newValue)
	{
		foreach (Rigidbody rb in bodies)
		{
			rb.Sleep();
			rb.useGravity = newValue;
		}

		foreach (CapsuleCollider cc in capsuleColliders)
		{
			cc.enabled = newValue;
		}
		foreach (BoxCollider bc in boxColliders)
		{
			bc.enabled = newValue;
		}
		foreach (SphereCollider sc in sphereColliders)
		{
			sc.enabled = newValue;
		}
	}
}
