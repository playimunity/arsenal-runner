﻿using UnityEngine;
using System.Collections;

public class EndPazzle : MonoBehaviour {

	public GameObject puzzleObject;
	public GameObject particleObj;
	public GameObject collectPartcleFX;
	public GameObject yellowRaysParent;
	public GameObject[] yellowRays;

	int frameIndex = 0;

	bool triggered = false;

	WaitForEndOfFrame _waitFrame = new WaitForEndOfFrame();

	Transform PuzzleTransform;
	Transform YellowRaysTransform;

	void Start(){
		PuzzleTransform = puzzleObject.transform;
		YellowRaysTransform = yellowRaysParent.transform;

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN && GameManager.CurrentRun.status == RunAbstract.RunStatus.COMPLETED) {
			puzzleObject.SetActive (false);
			yellowRaysParent.gameObject.SetActive (false);
			GetComponent<BoxCollider> ().enabled = false;
		}
	}


	void Update () {
		if (!triggered) {
			PuzzleTransform.Rotate (0f, 120f * Time.deltaTime, 0f);
			YellowRaysTransform.Rotate (0f, 0f, 60f * Time.deltaTime);
		}
	}


	void OnTriggerEnter(Collider col){
		if (col.transform.gameObject.tag == "Player") {
			EndLineEvent ();
			AudioManager.Instance.OnPowerUpPick ();
		}
	}


	public void EndLineEvent(){
		triggered = true;
		puzzleObject.SetActive (false);//Destroy (puzzleObject);
		foreach (GameObject ray in yellowRays) {
			StartCoroutine(EndLineCoroutine(ray));
		}
	}

	IEnumerator EndLineCoroutine(GameObject ray){
		while (frameIndex < 200) {
			ray.transform.Translate (Vector3.forward * -15f * Time.deltaTime);
			frameIndex++;
			yield return _waitFrame;
		}
		ray.SetActive (false);//Destroy (ray);
	}

}
