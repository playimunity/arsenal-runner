﻿using UnityEngine;
using System.Collections;

public class EndingSceneSoundEvents : MonoBehaviour {





	public void AlienBlueWon4(){
		AudioManager.Instance.AlienBlueWon4 ();
	}

	public void AlienRed1(){
		AudioManager.Instance.AlienRed1 ();
	}

	public void AlienBlueHit3(){
		AudioManager.Instance.AlienBlueHit3 ();
	}

	public void SpaceShipLiftoff(){
		AudioManager.Instance.SpaceShipLiftoff ();
	}

	public void AlienBlueLost2(){
		AudioManager.Instance.AlienBlueLost2 ();
	}

	public void SpaceShipExplosion(){
		AudioManager.Instance.SpaceShipExplosion ();
	}

	public void GunCannonFuse(){
		AudioManager.Instance.GunCannonFuse ();
	}

	public void CannonExplosion(){
		AudioManager.Instance.CannonExplosion ();
	}

	public void AlienBlueLost4(){
		AudioManager.Instance.AlienBlueLost4 ();
	}

	public void AlienBlueLaughing(){
		AudioManager.Instance.AlienBlueLaughing ();
	}

	public void OnKick(){
		AudioManager.Instance.OnKick ();
	}




}
