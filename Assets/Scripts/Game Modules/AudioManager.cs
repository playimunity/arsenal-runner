﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public static AudioManager Instance;

	// ---- Sources

	public AudioSource MUS_Menu;
	public AudioSource MUS_Run1;
	public AudioSource MUS_Run2;
	public AudioSource MUS_Bench;

	public AudioSource SFX_Kick;
	public AudioSource SFX_Jump;
	public AudioSource SFX_TurnLeft;
	public AudioSource SFX_TurnRight;
	public AudioSource SFX_Slide;
	public AudioSource SFX_BreakGlass;
	public AudioSource SFX_CoinPick;
	public AudioSource SFX_CoinCounter;
	public AudioSource SFX_HardHit1;
	public AudioSource SFX_HardHit2;
	public AudioSource SFX_GodHit;
	public AudioSource SFX_SoftHit1;
	public AudioSource SFX_SoftHit2;
	public AudioSource SFX_HornBigCar;
	public AudioSource SFX_HornCar1;
	public AudioSource SFX_HornCar2;
	public AudioSource SFX_HornBike;
	public AudioSource SFX_HornTaxi;
	public AudioSource SFX_PowerUpBTN;
	public AudioSource SFX_PowerUpPick;
	public AudioSource SFX_MagnetPowerUpPick;
	public AudioSource SFX_RankA_Achieved;
	public AudioSource SFX_RankB_Achieved;
	public AudioSource SFX_Whistle;
	public AudioSource SFX_Run;
	public AudioSource SFX_RunGodMode;
	public AudioSource SFX_EndRun1;
	public AudioSource SFX_EndRun2;
	public AudioSource SFX_PlayerPositive2;
	public AudioSource SFX_PlayerWoohoo;
	public AudioSource SFX_X2PowerUp;
	public AudioSource SFX_DoubleCoin;
	public AudioSource SFX_RunCompleted;
	public AudioSource SFX_SpeedBoost;
	public AudioSource SFX_ScratchFX;

	public AudioSource SFX_ClosePopup;
	public AudioSource SFX_GameLost;
	public AudioSource SFX_GenericPopup;
	public AudioSource SFX_Purchase;
	public AudioSource SFX_BTNClick;
	public AudioSource SFX_SettingsBTN;
	public AudioSource SFX_CupWonPopup;

	public AudioSource SFX_HitMale1;
	public AudioSource SFX_HitMale2;
	public AudioSource SFX_HitFemale1;
	public AudioSource SFX_HitFemale2;

	public AudioSource SFX_Applause1;
	public AudioSource SFX_Applause2;

	public AudioSource SFX_Smash1;
	public AudioSource SFX_Smash2;
	public AudioSource SFX_Smash3;

	public AudioSource SFX_TrainHorn;
	public AudioSource SFX_TrainPass;

	public AudioSource SFX_CameraClick;
	public AudioSource SFX_LevelUp;

	public AudioSource SFX_StopWatch;
	public AudioSource SFX_SecretAreaEnter;

	void OnLevelWasLoaded(int level) {
		switchMusicOnLevelLoad(GameManager.getGameStateBySceneIndex (level));

	}

	void Awake(){
		Instance = this;
	}

	// ------------------

	public void OnJump(){
		if (!DAO.IsSFXEnabled) return;
		SFX_Jump.Play ();
	}

	public void OnKick(){
		if (!DAO.IsSFXEnabled) return;
		SFX_Kick.Play ();
	}

	public void OnTurnLeft(){
		if (!DAO.IsSFXEnabled) return;
		SFX_TurnLeft.Play ();
	}

	public void OnTurnRight(){
		if (!DAO.IsSFXEnabled) return;
		SFX_TurnRight.Play ();
	}

	public void OnSlide(){
		if (!DAO.IsSFXEnabled) return;
		SFX_Slide.Play ();
	}

	public void OnBreakGlass(){
		if (!DAO.IsSFXEnabled) return;
		SFX_BreakGlass.Play ();
	}

	public void OnCoinPick(){
		if (!DAO.IsSFXEnabled) return;
		SFX_CoinPick.Play ();
	}

	public void CoinCounter(bool play){
		if (!DAO.IsSFXEnabled) return;
		if (play) {
			SFX_CoinCounter.Play ();
		} else {
			SFX_CoinCounter.Stop ();
		}
	}

	public void OnHardHit(){
		if (!DAO.IsSFXEnabled) return;

		if (Random.Range (0, 10) >= 5) {
			SFX_HardHit1.Play ();
		} else {
			SFX_HardHit2.Play ();
		}
	}

	public void OnSoftHit(){
		if (!DAO.IsSFXEnabled) return;

		if (Random.Range (0, 10) >= 5) {
			SFX_SoftHit1.Play ();
		} else {
			SFX_SoftHit2.Play ();
		}
		OnWhistle ();
	}

	public void OnHitGod(){
		if (!DAO.IsSFXEnabled) return;

		SFX_GodHit.Play ();
	}

	public void OnHornBigCar(){
		if (!DAO.IsSFXEnabled) return;
		SFX_HornBigCar.Play ();
	}

	public void OnHornSmallCar(){
		if (!DAO.IsSFXEnabled) return;

		if (Random.Range (0, 10) >= 5) {
			SFX_HornCar1.Play ();
		} else {
			SFX_HornCar2.Play ();
		}

	}

	public void OnHornBike(){
		if (!DAO.IsSFXEnabled) return;

		SFX_HornBike.Play ();
	}

	public void OnHornTaxi(){
		if (!DAO.IsSFXEnabled) return;

		SFX_HornTaxi.Play ();
	}

	public void OnPowerUpBTN(){
		if (!DAO.IsSFXEnabled) return;
		SFX_PowerUpBTN.Play ();
	}

	public void OnMagnetPowerUp(){
		if (!DAO.IsSFXEnabled) return;
		SFX_MagnetPowerUpPick.Play ();
	}

	public void OnPowerUpPick(){
		if (!DAO.IsSFXEnabled) return;
		SFX_PowerUpPick.Play ();
	}

	public void OnRankA_Achieved(){
		if (!DAO.IsSFXEnabled) return;
		SFX_RankA_Achieved.Play ();
	}

	public void OnRankB_Achieved(){
		if (!DAO.IsSFXEnabled) return;
		SFX_RankB_Achieved.Play ();
	}

	public void OnWhistle(){
		if (!DAO.IsSFXEnabled) return;
		SFX_Whistle.Play ();
	}

	public void OnRun(){
		if (!DAO.IsSFXEnabled) return;
		if (SFX_RunGodMode.isPlaying) SFX_RunGodMode.Stop ();

		SFX_Run.Play ();
	}

	public void OnRunGodeMode(){
		if (!DAO.IsSFXEnabled) return;
		if (SFX_Run.isPlaying) SFX_Run.Stop ();

		SFX_RunGodMode.Play ();
	}

	public void MuteRun(){
		if (SFX_Run.isPlaying) SFX_Run.Stop ();
		if (SFX_RunGodMode.isPlaying) SFX_RunGodMode.Stop ();
	}
		
	public void OnClosePopup(){
		if (!DAO.IsSFXEnabled) return;

		SFX_ClosePopup.Play ();
	}

	public void OnGameLost(){
		if (!DAO.IsSFXEnabled) return;


		if (MUS_Run1.isPlaying)
			MUS_Run1.Stop ();

		if (MUS_Run2.isPlaying)
			MUS_Run2.Stop ();

		SFX_GameLost.Play ();
	}

	public void OnGenericPopup(){
		if (!DAO.IsSFXEnabled) return;

		SFX_GenericPopup.Play ();
	}

	public void OnPurchase(){
		if (!DAO.IsSFXEnabled) return;

		SFX_Purchase.Play ();
	}

	public void OnBTNClick(){
		if (!DAO.IsSFXEnabled) return;

		SFX_BTNClick.Play ();
	}

	public void OnSettingsBTN(){
		if (!DAO.IsSFXEnabled) return;

		SFX_SettingsBTN.Play ();
	}

	public void OnCupWon(){
		if (!DAO.IsSFXEnabled) return;

		SFX_CupWonPopup.Play ();
	}

	public void OnHitMale(){
		if (!DAO.IsSFXEnabled) return;

		if (Random.Range (0, 10) >= 5) {
			SFX_HitMale1.Play ();
		} else {
			SFX_HitMale2.Play ();
		}
	}

	public void OnHitFemale(){
		if (!DAO.IsSFXEnabled) return;

		if (Random.Range (0, 10) >= 5) {
			SFX_HitFemale1.Play ();
		} else {
			SFX_HitFemale2.Play ();
		}
	}

	public void OnEndSection(){
		if (!DAO.IsSFXEnabled) return;

		if (Random.Range (0, 10) >= 5) {
			SFX_Applause1.Play ();
		} else {
			SFX_Applause2.Play ();
		}
	}

	public void OnBallSmash(){
		if (!DAO.IsSFXEnabled) return;

		int res = Random.Range (0, 9);

		if (res <= 3) {
			SFX_Smash1.Play ();
		} else if (res <= 6 ){
			SFX_Smash2.Play ();
		}else{
			SFX_Smash3.Play ();
		}
	}

	public void OnContractSign(){
		if (!DAO.IsSFXEnabled) return;

		SFX_CameraClick.Play ();
	}

	public void OnLevelUp(){
		if (!DAO.IsSFXEnabled) return;

		SFX_LevelUp.Play ();
	}

	public void OnTrainHorn(){
		if (!DAO.IsSFXEnabled || SFX_TrainHorn.isPlaying) return;

		SFX_TrainHorn.Play ();
	}

	public void OnTrainPass(){
		if (!DAO.IsSFXEnabled) return;

		SFX_TrainPass.Play ();
	}

	int randomNum = 0;
	public void OnEndRunCrowd(){
		if (!DAO.IsSFXEnabled) return;

		randomNum = Random.Range (0, 2);
		if (randomNum == 0) {
			SFX_EndRun1.Play ();
		} else {
			SFX_EndRun2.Play ();
		}
	}
		
	public void PreperToEndRunCrowd(bool on){
		if (!DAO.IsSFXEnabled) return;

		if (on) {
			randomNum = Random.Range (0, 2);
			if (randomNum == 0) {
				SFX_EndRun1.Play ();
			} else {
				SFX_EndRun2.Play ();
			}
		} else {
			if (randomNum == 0) {
				SFX_EndRun1.Pause ();
			} else {
				SFX_EndRun2.Pause ();
			}
		}
	}

	public void PlayerPositive2(){
		if (!DAO.IsSFXEnabled) return;

		SFX_PlayerPositive2.Play ();
	}

	public void PlayerWoohoo(){
		if (!DAO.IsSFXEnabled) return;

		SFX_PlayerWoohoo.Play ();
	}

	public void X2PowerUp(){
		if (!DAO.IsSFXEnabled) return;

		SFX_X2PowerUp.Play ();
	}

	public void DoubleCoin(){
		if (!DAO.IsSFXEnabled) return;

		SFX_DoubleCoin.Play ();
	}

	public void RunCompleted(){
		if (!DAO.IsSFXEnabled) return;

		SFX_RunCompleted.Play ();
	}

	public void SpeedBoost(){
		if (!DAO.IsSFXEnabled) return;

		SFX_SpeedBoost.Play ();
	}

	bool scratchStarted = false;
	public void ScratchFX(bool play){
		if (!DAO.IsSFXEnabled) return;

		if (play) {
			SFX_ScratchFX.UnPause ();
			if (!scratchStarted) {
				scratchStarted = true;
				SFX_ScratchFX.Play ();
			}
		} else {
			SFX_ScratchFX.Pause ();
		}
	}

	public void StopWatch(bool on){
		if (!DAO.IsSFXEnabled) return;

		if (on) {
			SFX_StopWatch.Play ();
		} else {
			SFX_StopWatch.Stop ();
		}
	}

	public void SecretAreaEnter(){
		if (!DAO.IsSFXEnabled) return;

		SFX_SecretAreaEnter.Play ();
	}



	// Ending Scene:

	public AudioSource SFX_AlienBlueWon4;
	public AudioSource SFX_AlienRed1;
	public AudioSource SFX_AlienBlueHit3;
	public AudioSource SFX_SpaceShipLiftoff;
	public AudioSource SFX_AlienBlueLost2;
	public AudioSource SFX_SpaceShipExplosion;
	public AudioSource SFX_GunCannonFuse;
	public AudioSource SFX_CannonExplosion;
	public AudioSource SFX_AlienBlueLost4;
	public AudioSource SFX_AlienBlueLaughing;


	public void AlienBlueWon4(){
		if (!DAO.IsSFXEnabled) return;

		SFX_AlienBlueWon4.Play ();
	}

	public void AlienRed1(){
		if (!DAO.IsSFXEnabled) return;

		SFX_AlienRed1.Play ();
	}

	public void AlienBlueHit3(){
		if (!DAO.IsSFXEnabled) return;

		SFX_AlienBlueHit3.Play ();
	}

	public void SpaceShipLiftoff(){
		if (!DAO.IsSFXEnabled) return;

		SFX_SpaceShipLiftoff.Play ();
	}

	public void AlienBlueLost2(){
		if (!DAO.IsSFXEnabled) return;

		SFX_AlienBlueLost2.Play ();
	}

	public void SpaceShipExplosion(){
		if (!DAO.IsSFXEnabled) return;

		SFX_SpaceShipExplosion.Play ();
	}

	public void GunCannonFuse(){
		if (!DAO.IsSFXEnabled) return;

		SFX_GunCannonFuse.Play ();
	}

	public void CannonExplosion(){
		if (!DAO.IsSFXEnabled) return;

		SFX_CannonExplosion.Play ();
	}

	public void AlienBlueLost4(){
		if (!DAO.IsSFXEnabled) return;

		SFX_AlienBlueLost4.Play ();
	}

	public void AlienBlueLaughing(){
		if (!DAO.IsSFXEnabled) return;

		SFX_AlienBlueLaughing.Play ();
	}


	// ====================================================================================
	// ====================================================================================




	public void MuteMusic(){
		if (MUS_Menu.isPlaying) StartCoroutine (FadeStop (MUS_Menu));
		if (MUS_Run1.isPlaying) StartCoroutine (FadeStop (MUS_Run1));
		if (MUS_Run2.isPlaying) StartCoroutine (FadeStop (MUS_Run2));
	}


	public void switchMusicOnLevelLoad(GameManager.GameState gs){
		//if(MUS_Bench.isPlaying) MUS_Bench.Stop ();
		if (SFX_Run.isPlaying) SFX_Run.Stop ();
		if (SFX_RunGodMode.isPlaying) SFX_RunGodMode.Stop ();

		if (!DAO.IsMusicEnabled) return;


		switch (gs) {

			case GameManager.GameState.LOCKER_ROOM:
			//case GameManager.GameState.MAIN_MENU:
			//case GameManager.GameState.PRELOADER:
			{

				if (!MUS_Menu.isPlaying) {
					if(MUS_Run1.isPlaying) StartCoroutine ( FadeSwitch( MUS_Run1, MUS_Menu ) );
					else if(MUS_Run2.isPlaying) StartCoroutine ( FadeSwitch( MUS_Run2, MUS_Menu ) );
					else StartCoroutine ( FadePlay( MUS_Menu ) );
				}

//				if (gs == GameManager.GameState.LOCKER_ROOM && !MUS_Bench.isPlaying) {
//					MUS_Bench.Play ();
//				}

				break;
			} 


			case GameManager.GameState.CUP_RUN:
			case GameManager.GameState.INFINITY_RUN:
			case GameManager.GameState.TUTORIAL_RUN:
			{

				if (MUS_Run1.isPlaying || MUS_Run2.isPlaying) return;


				if (Random.Range (0, 10) >= 5) {
					StartCoroutine (FadeSwitch (MUS_Menu, MUS_Run1));
				} else {
					StartCoroutine (FadeSwitch (MUS_Menu, MUS_Run2));
				}

				break;
			}
		}

	}



	IEnumerator FadeStop( AudioSource src ){
		
		while (src.volume > 0f) {
			src.volume -= 1f * Time.deltaTime;
            print("src " + src.name + " volume: " + src.volume);
			yield return null;
		}

		src.Stop ();
		src.volume = 1f;
	}

	IEnumerator FadePlay( AudioSource src ){

		src.volume = 0f;
		src.Play ();

		while (src.volume < 1f) {
			src.volume += 1f * Time.deltaTime;
			yield return null;
		}

		src.volume = 1f;
	}



	IEnumerator FadeSwitch( AudioSource from, AudioSource to ){

		while (from.volume > 0f) {
			from.volume -= 1f * Time.deltaTime;
			yield return null;
		}

		from.Stop ();
		from.volume = 1f;

		// ------

		to.volume = 0f;
		to.Play ();

		while (to.volume < 1f) {
			to.volume += 1f * Time.deltaTime;
			yield return null;
		}

		to.volume = 1f;
	}



	int musicIndex = 0;

	public void OnApplicationPause(bool pauseStatus)
	{
		//Check if this is Pause
		if (pauseStatus) {
			if (DAO.IsMusicEnabled) {
				//Debug.Log ("alon_______ music sould be Paused");
				//AudioManager.Instance.MuteMusic ();
//				if (MUS_Bench.isPlaying) {
//					MUS_Bench.Pause ();
//					musicIndex = 1;
//				}
				if (MUS_Menu.isPlaying) {
					MUS_Menu.Pause ();
					musicIndex = 2;
				}
				if (MUS_Run1.isPlaying) {
					MUS_Run1.Pause ();
					musicIndex = 3;
				}
				if (MUS_Run2.isPlaying) {
					MUS_Run2.Pause ();
					musicIndex = 4;
				}
				//Debug.Log ("alon_______ music sould be Paused");
			}
		}

		//Check if this is Resume
		if (!pauseStatus) {
			if (DAO.IsMusicEnabled) {
				//Debug.Log ("alon_______ music sould be unpaused");
				//AudioManager.Instance.switchMusicOnLevelLoad( GameManager.curentGameState );
				switch (musicIndex) {
				case 1:
					{
						//MUS_Bench.UnPause ();
						break;
					}
				case 2:
					{
						MUS_Menu.UnPause ();
						break;
					}
				case 3:
					{
						MUS_Run1.UnPause ();
						break;
					}
				case 4:
					{
						MUS_Run2.UnPause ();
						break;
					}
				}
				//Debug.Log ("alon_______ music sould be unpaused");
			}
		}
	}





}
