﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class draw : MonoBehaviour {

	RectTransform container;
	public RawImage img;

	Texture2D texture;
	bool drawC = false;
	Vector2 rf;
	int CIRCLE_RADIUS = 10;

	Color[] pixels;
	int countPixel = 0;

	void Start(){
		container = GetComponent<RectTransform> ();

		texture = new Texture2D (128, 128);
		img.texture = texture;

		for (int y = 0; y < texture.height; y++) {
			for (int x = 0; x < texture.width; x++) {
				texture.SetPixel(x,y, Color.clear);
			}
		}
		texture.Apply (); 

	}
	
	void Update(){

		if (Input.GetMouseButtonDown (0))
			drawC = true;
		if (Input.GetMouseButtonUp (0))
			drawC = false;

		if(drawC){

			Vector2 lp;
			bool res = RectTransformUtility.ScreenPointToLocalPointInRectangle(container, Input.mousePosition, null, out lp);

			if(!res) return;

			int x = (int)(lp.x * ((float)texture.width/container.sizeDelta.x));
			int y = (int)(lp.y * ((float)texture.height/container.sizeDelta.y));

			Circle(x, y);

		}

		
	}

	public void Circle(int cx, int cy){

		int x, y, px, nx, py, ny, d, r = CIRCLE_RADIUS;
		for (x = 0; x <= r; x++){
			d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
			for (y = 0; y <= d; y++){
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;
				
				if(px > 0 && px < texture.width && py > 0 && py < texture.height) texture.SetPixel(px, py, Color.red);
				if(nx > 0 && nx < texture.width && py > 0 && py < texture.height) texture.SetPixel(nx, py, Color.red);
				if(px > 0 && px < texture.width && ny > 0 && ny < texture.height) texture.SetPixel(px, ny, Color.red);
				if(nx > 0 && nx < texture.width && ny > 0 && ny < texture.height) texture.SetPixel(nx, ny, Color.red);

//				texture.SetPixel(px, py, Color.red);
//				texture.SetPixel(nx, py, Color.red);
//				texture.SetPixel(px, ny, Color.red);
//				texture.SetPixel(nx, ny, Color.red);
				
			}
		}    
		texture.Apply (); 
	}

	public void OnImageDiscovered(){
		for (int y = 0; y < texture.height; y++) {
			for (int x = 0; x < texture.width; x++) {
				texture.SetPixel(x,y, Color.red);
			}
		}
		texture.Apply (); 
	}
	
}
