using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StringProvider_MainMenu : MonoBehaviour {

	// ---------------------------------- Menu

//	public Text GiftCards;
//	public Text Wellcome;
	//public Text Play;
//	public Text Stats;

	// ---------------------------------- Settings 

	//public Text RestorePurchases;
	//public Text WatchComics;
//	public Text Music;
//	public Text SFX;
//	public Text Notifications;
//	public Text RateUs;
//	public Text Leaderboard;
//	public Text Achievements;
//	public Text FBConnectOportunity;
//	public Text ConnectToFB;
//	public Text GetFreeGiftCard;
//	public Text FBInviteOportunity;
//	public Text InviteBtnTxt;
//	public Text SettingsHeaderTxt;

	// ---------------------------------- Tutorial

//	public Text HelloCoach;
//	public Text ShareAndCompare;
//	public Text SaveYourProgress;
//	public Text LogInWithFacebook;
//	public Text NotNow;
//	public Text Connect;
//	public Text Free;

	// ---------------------------------- Gift Cards




//	public Text GC_CollectGiftsBtn;
//	public Text GC_AskGiftBtn;
//	public Text GC_SendGiftsBTn;
//	public Text GC_RequestGiftsBTn;

//	public Text GC_FromFriend;
//	public Text GC_ToFriend;
//	public Text GC_ClaimPrizeTxt;
//	public Text GC_NC_Text1;
//	public Text GC_NC_Text2;
//	public Text GC_NC_RequestBtn;
//	public Text GC_NC_CollectBtn;
//	public Text GC_Title;
//	public Text GC_YouScored;
//	public Text GC_NC_Title;
//	public Text GC_TutFillLuckyTitle;
//	public Text GC_GetFree;
//	public Text GC_SendReceive;
//	public Text GC_Get;
//	public Text GC_Request;
//	public Text GC_TutGetFreeTitle;
//	public Text GC_FillLucky;
//	public Text GC_GetThree;
//	public Text GC_WantMore;
//	public Text GC_GotIt_Lucky;
//	public Text GC_TutPowerUpsTitle;
//	public Text GC_UsePowerUps;
//	public Text GC_GotIt_PowerUps;
//	public Text GC_LastCard_header;


	// ---------------------------------- Stats Scrn

	//public Text SS_ThisIsYouTeam;
//	public Text SS_Rank;
//	public Text SS_FoundationD;
//	public Text SS_FCBTeam;
//	public Text SS_HighScore;
//	public Text SS_TrainingTime;
//	public Text SS_QuestsCompleted;
//	public Text SS_MyStats;
//	public Text SS_FriendsStats;
//	public Text SS_ShareBtn;
//	public Text SS_Runs;
//	public Text SS_Gold;
//	public Text SS_Silver;
//	public Text SS_Failed;
//	public Text SS_FavoritePlayer;
//	public Text SS_HighScoreFavorite;
//	public Text SS_Level;
//	public Text SS_Perks;

//	public Text SS_header;
//	public Text SS_ThisIsMyTeamShare;
//	public Text SS_RankShare;
//	public Text SS_FoundationDShare;
//	public Text SS_TeamSizeShare;
//	public Text SS_TotalMetersShare;
//	public Text SS_TotalPracticeShare;
//	public Text SS_QuestsCompletedShare;
//	public Text SS_RunsShare;
//	public Text SS_RankAShare;
//	public Text SS_RankBShare;
//	public Text SS_FailedShare;
//	public Text SS_FavoritePlayerShare;
	//public Text SS_HighScoreFavoriteShare;
//	public Text SS_TotalDistance;
//	public Text SS_TotalDistanceShare;
//	public Text SS_LevelShare;
//	public Text SS_PerksShare;



	void Awake () {
//		GiftCards.text = DAO.Language.GetString ("gift-box");
//		Wellcome.text = DAO.Language.GetString ("wellcome-back") + ",";
		//Play.text = DAO.Language.GetString ("play");
//		Stats.text = DAO.Language.GetString ("stats");

		//RestorePurchases.text = DAO.Language.GetString ("restore-purchases");
		//WatchComics.text = DAO.Language.GetString ("watch-comics");
//		Music.text = DAO.Language.GetString ("music");
//		SFX.text = DAO.Language.GetString ("sfx");
//		Notifications.text = DAO.Language.GetString ("notifications");
//		RateUs.text = DAO.Language.GetString ("rate-us");
//		Leaderboard.text = DAO.Language.GetString ("leaderboard");
//		Achievements.text = DAO.Language.GetString ("achievements");
//		FBConnectOportunity.text = DAO.Language.GetString ("fb-connect-oportunity");
//		ConnectToFB.text = DAO.Language.GetString ("connect-to-fb");
//		GetFreeGiftCard.text = "+ " + DAO.Language.GetString ("get-free-gift-cards");
//		FBInviteOportunity.text = DAO.Language.GetString ("invite-oportunity");
//		InviteBtnTxt.text = DAO.Language.GetString ("invite-a-friend");
//		SettingsHeaderTxt.text = DAO.Language.GetString ("settings");

//		HelloCoach.text = DAO.Language.GetString ("hello-coach");
//		ShareAndCompare.text = DAO.Language.GetString ("share-and-compare");
//		SaveYourProgress.text = DAO.Language.GetString ("save-your-progress");
//		LogInWithFacebook.text = DAO.Language.GetString ("login-with");
//		NotNow.text = DAO.Language.GetString ("not-now");
//		Connect.text = DAO.Language.GetString ("connect");
//		Free.text = DAO.Language.GetString ("free");



//		GC_FromFriend.text = DAO.Language.GetString ("gc-from-friend");
//		GC_ToFriend.text = DAO.Language.GetString ("gc-to-friend");

//		GC_CollectGiftsBtn.text = DAO.Language.GetString ("gc-collect-gift-btn");
//		GC_AskGiftBtn.text = DAO.Language.GetString ("gc-nc-request-btn");
//		GC_SendGiftsBTn.text = DAO.Language.GetString ("send");
//		GC_RequestGiftsBTn.text = DAO.Language.GetString ("gift-requests-title");

//		GC_NC_Title.text = DAO.Language.GetString ("gc-nc-title");
//		GC_NC_Text1.text = DAO.Language.GetString ("gc-nc-text1");
//		GC_NC_Text2.text = DAO.Language.GetString ("gc-nc-text2");
//		GC_NC_RequestBtn.text = DAO.Language.GetString ("gc-nc-request-btn");
//		GC_NC_CollectBtn.text = DAO.Language.GetString ("gc-nc-collect-btn");

//		GC_ClaimPrizeTxt.text = DAO.Language.GetString ("collect");
//		GC_Title.text = DAO.Language.GetString ("gift-cards");
//		GC_LastCard_header.text = DAO.Language.GetString ("gift-cards");
//		GC_YouScored.text = DAO.Language.GetString ("gc-you-scored");

//		GC_TutFillLuckyTitle.text = DAO.Language.GetString ("gift-cards");
//		GC_TutGetFreeTitle.text = DAO.Language.GetString ("gift-cards");
//		GC_TutPowerUpsTitle.text = DAO.Language.GetString ("gift-cards");
//		GC_GetFree.text = DAO.Language.GetString ("get-free");
//		GC_SendReceive.text = DAO.Language.GetString ("send-receive");
//		GC_Get.text = DAO.Language.GetString ("get");
//		GC_Request.text = DAO.Language.GetString ("request");
//		GC_FillLucky.text = DAO.Language.GetString ("fill-lucky");
//		GC_GetThree.text = DAO.Language.GetString ("get-three");
//		GC_WantMore.text = DAO.Language.GetString ("want-more");
//		GC_GotIt_Lucky.text = DAO.Language.GetString ("got-it");
//		GC_UsePowerUps.text = DAO.Language.GetString ("use-power-ups");
//		GC_GotIt_PowerUps.text = DAO.Language.GetString ("got-it");


//		SS_Rank.text = SS_RankShare.text = DAO.Language.GetString ("rank");
//		SS_FoundationD.text = SS_FoundationDShare.text = DAO.Language.GetString ("f-date");
//		SS_FCBTeam.text = SS_TeamSizeShare.text = DAO.Language.GetString ("t-size");
//		SS_HighScore.text = SS_TotalMetersShare.text = DAO.Language.GetString ("high-score");
//		SS_TrainingTime.text = SS_TotalPracticeShare.text = DAO.Language.GetString ("training-time");
//		SS_QuestsCompleted.text = SS_QuestsCompletedShare.text = DAO.Language.GetString ("quests-completed");
//		SS_MyStats.text = DAO.Language.GetString ("my-stats");
//		SS_FriendsStats.text = DAO.Language.GetString ("friends-stats");
//		SS_ShareBtn.text = DAO.Language.GetString ("share");
//		SS_Runs.text = SS_RunsShare.text = DAO.Language.GetString ("runs-title");
//		SS_Gold.text = SS_RankAShare.text = DAO.Language.GetString ("gold");
//		SS_Silver.text = SS_RankBShare.text = DAO.Language.GetString ("silver");
//		SS_Failed.text = SS_FailedShare.text = DAO.Language.GetString ("failed");
//		SS_FavoritePlayer.text = SS_FavoritePlayerShare.text = DAO.Language.GetString ("favorite-player");
//		SS_TotalDistance.text = SS_TotalDistanceShare.text = DAO.Language.GetString ("run");
//		SS_Perks.text = SS_PerksShare.text = DAO.Language.GetString ("perks-str");
//		SS_ThisIsMyTeamShare.text = DAO.Language.GetString ("this-is-my-team");
//		SS_header.text = DAO.Language.GetString ("stats");

	}



}
