
using UnityEngine;
using System.Collections;
using System;

public class UnitedAnalytics : MonoBehaviour {

	public static UnitedAnalytics _;

	void Awake(){
		
		_ = this;

		GoogleAnalytics.StartTracking();
		//Debug.Log("RON____________xUnitedAnalytics end Time:"+ DateTime.Now.ToLongTimeString());
	}


	// ---------------------------------------------------------------------------------------------


	public static void LogEvent(string Category, string Action, string Label, int Value){
		//Debug.Log("RON____________xUnitedAnalytics LogEvent:"+ DateTime.Now.ToLongTimeString()+"Category:"+ Category + "Action:" +Action + "Label:"+Label);
		#if UNITY_EDITOR
		return;
		#endif


		GoogleAnalytics.Client.CreateHit(GoogleAnalyticsHitType.EVENT);

		GoogleAnalytics.Client.SetEventCategory(Category);
		GoogleAnalytics.Client.SetEventAction(Action);
		GoogleAnalytics.Client.SetEventLabel(Label);
		GoogleAnalytics.Client.SetEventValue(Value);

		GoogleAnalytics.Client.Send();

	}

	public static void LogEvent(string Category, string Action, string Label){

		#if UNITY_EDITOR
		return;
		#endif

		GoogleAnalytics.Client.CreateHit(GoogleAnalyticsHitType.EVENT);

		GoogleAnalytics.Client.SetEventCategory(Category);
		GoogleAnalytics.Client.SetEventAction(Action);
		GoogleAnalytics.Client.SetEventLabel(Label);

		GoogleAnalytics.Client.Send();

	}

	public static void LogEvent(string Category, string Action){

		#if UNITY_EDITOR
	    return;
		#endif

		GoogleAnalytics.Client.CreateHit(GoogleAnalyticsHitType.EVENT);

		GoogleAnalytics.Client.SetEventCategory(Category);
		GoogleAnalytics.Client.SetEventAction(Action);

		GoogleAnalytics.Client.Send();

	}


}
