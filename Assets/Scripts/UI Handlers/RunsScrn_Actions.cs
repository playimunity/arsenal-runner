﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class RunsScrn_Actions : MonoBehaviour {



	public static RunsScrn_Actions Instance;

	public RectTransform scrollContent;
	public Transform RunsContainer;
	public GameObject RunUiElementPrefab;
	public RectTransform RunUiElementRect;
	GameObject tempRunUiElement;
	
	float runUiElementHeight;
	float gap = 10f;
	
	//Vector3 runUiElementCurrentPos;
	
	Vector2 scrollContentSize;
	Vector2 scrollContentPos;

	void Awake(){
		Instance = this;
		runUiElementHeight = RunUiElementRect.sizeDelta.y + gap;
//		Debug.Log ("alon__________ RunUiElementRect.sizeDelta.y: " + RunUiElementRect.sizeDelta.y + " , gap : " + gap);
		scrollContentSize = scrollContent.sizeDelta;
		//scrollContentSize.y = runUiElementHeight;
		scrollContentSize.y = 0f;
		scrollContent.sizeDelta = scrollContentSize;
		//runUiElementCurrentPos = new Vector3 (0f, -155f, 0f);
		scrollContentPos = scrollContent.anchoredPosition;
		scrollContentPos.y = 0f;
	}

	void Start () {
		
	}

	public void Reset(){
		foreach (Transform child in RunsContainer) {
			GameObject.Destroy(child.gameObject);
		}

		//runUiElementCurrentPos = new Vector3 (0f, -155f, 0f);
		//scrollContentSize.y = runUiElementHeight;
		scrollContentSize.y = 0f;
		scrollContent.sizeDelta = scrollContentSize;
	}

	
	public void AddRunUiEllement(RunAbstract run){
		scrollContentSize.y += runUiElementHeight;
		scrollContent.sizeDelta = scrollContentSize;
		//Debug.Log ("alon__________ runUiElementHeight: " + runUiElementHeight);
		tempRunUiElement = (GameObject)Instantiate (RunUiElementPrefab , transform.position , transform.rotation);
		tempRunUiElement.transform.SetParent (RunsContainer , false);
		//tempRunUiElement.GetComponent<RectTransform>().anchoredPosition = runUiElementCurrentPos;
		//runUiElementCurrentPos.y -= runUiElementHeight;

		tempRunUiElement.GetComponent<RunUIElement> ().SetView (run);
	}

	public void DefineScrollPosition(int runIndex){
		scrollContentPos.y = runUiElementHeight * runIndex;
		scrollContent.anchoredPosition = scrollContentPos;
		//Debug.Log ("alon___ scrollContentPos.y: " + scrollContentPos.y);
	}

	public void ScrollTo(){
		scrollContent.anchoredPosition = scrollContentPos;
	}


}
