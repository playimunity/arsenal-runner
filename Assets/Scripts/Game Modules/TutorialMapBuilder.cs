using UnityEngine;
using System.Collections;

public class TutorialMapBuilder : MonoBehaviour {
	int NUM_OF_SECTIONS_ON_SCENE = 6;
	public float SECTION_REMOVE_DELAY_IN_SECONDS = 1f;
	public float SECTION_WIDTH = 30f;
	public float SECTION_HEIGHT_LEVEL = 5f;
	
	public Run run;	

	public int CurrentNumOfSectionsOnScene = 0;

	private static TutorialMapBuilder instance;
	int currentRunHeight = 0;
	int nextSection = 0;
	int previosSectionToDelete = -1;
	Vector3 nextSectionPosition = Vector3.zero;
	Vector3 nextSectionPositionOffset;
	Vector3 nextSectionRotation = Vector3.zero;
	Vector3 vectorZona;
	WorldSectionAbstract section;
	bool instantiateDoubleSection = false;
	WaitForSeconds removeDelay;
	
	static bool needToTurnLeft = false;
	static bool needToTurnRight = false;
	static bool needNewSection = false;
	static bool needToRemoveSection = false;
	bool waitnigForBuildTSections = false;

	static bool FreezeBuilder = false;
	static WorldSection.RegionType curRegion = WorldSection.RegionType.HR;
	
	int sectionIndexToDelete = 0;

	public static bool secretAreaPosOffset = false;
	static bool waitingToRemoveSection = false;
	bool needToChangeSectionsPosition = false;



	
	public static TutorialMapBuilder Instance{
		get{return Instance;}
	}
	
	void Log(string msg){
		if (!GameManager.DEBUG) return;
	}
	
	// -------------------------------------------------------------------------------------------------

	void Start () {
		removeDelay = new WaitForSeconds (SECTION_REMOVE_DELAY_IN_SECONDS);
		instance = this;

		FreezeBuilder = false;
		needToTurnLeft = false;
		needToTurnRight = false;
		needNewSection = false;
		needToRemoveSection = false;

//		Utils.SetFogColor (curRegion);
		BuildFirstSections ();
		
	}
	
	
	void Update(){
		
		if(needToTurnLeft){
			nextSectionPosition -= nextSectionPositionOffset;
			TurnBuilderLeft();
			nextSectionPosition += nextSectionPositionOffset;
			nextSectionPosition += nextSectionPositionOffset;
			
			needToTurnLeft = false;
//			needNewSection = true;
			needToRemoveSection = true;
            print("_+±_+±_+±NEED TO TURN LEFT_+±_+±_+± <==========");
		}
		
		if(needToTurnRight){
			nextSectionPosition -= nextSectionPositionOffset;
			TurnBuilderRight();
			nextSectionPosition += nextSectionPositionOffset;
			nextSectionPosition += nextSectionPositionOffset;
			
			needToTurnRight = false;
//			needNewSection = true;
			needToRemoveSection = true;
            print("_+±_+±_+±NEED TO TURN RIGHT_+±_+±_+±  =========>");
		}
		
//		if (needToRemoveSection) {
//			if (PlayerController.instance.isDead) return;
//			needToRemoveSection = false;
//			StartCoroutine( RemoveUsedSections() );
//		}

		if (needToRemoveSection) {
			needToRemoveSection = false;
			if (PlayerController.instance.isDead) {
				previosSectionToDelete = sectionIndexToDelete;
			} else {
				waitingToRemoveSection = true;
				StartCoroutine ("RemoveUsedSections");
			}
		}

//		if (CurrentNumOfSectionsOnScene < NUM_OF_SECTIONS_ON_SCENE && !FreezeBuilder) {
//			AddNewSection ();
//		}

		if (CurrentNumOfSectionsOnScene < NUM_OF_SECTIONS_ON_SCENE && !FreezeBuilder) {
			if (waitnigForBuildTSections && needToChangeSectionsPosition) {
				needToChangeSectionsPosition = false;
				waitnigForBuildTSections = false;
				SecretAreaNewSectionsPosition (SecterAreaHandler.numOfSectionsToMove);
				//Debug.Log ("alon___ secter area - new sections position - T");
			}
			AddNewSection ();
		}


		if (secretAreaPosOffset) {
			secretAreaPosOffset = false;
			if (waitingToRemoveSection) {
				needToChangeSectionsPosition = true;
				RemoveSectionInstantly ();
				//Debug.Log ("alon___ secter area - secretAreaPosOffset - waitingToRemoveSection");
			} else {
				SecretAreaNewSectionsPosition (SecterAreaHandler.numOfSectionsToMove);
				//Debug.Log ("alon___ secter area - secretAreaPosOffset - not waitingToRemoveSection");
			}
			//			SecretAreaNewSectionsPosition (SecterAreaHandler.numOfSectionsToMove);
		}


		
	}
	
	
	
	// --------------------------------------------------------------------------------------------------------------------------------------------


	
	
	public static void OnSectionAchieved(int direction, bool Is_T_Section, WorldSection.RegionType region){
		if (direction < 0) { // turn left
			needToTurnLeft = true;
		} else if (direction > 0) { // turn right
			needToTurnRight = true;
		} else {
			needToRemoveSection = true;
		}

		if( Is_T_Section && FreezeBuilder){
			FreezeBuilder = false;
		} 

		if (curRegion != region) {
			curRegion = region;
//			Utils.SetFogColor (curRegion);
		}

	}
	
	void BuildFirstSections(){
		
		nextSectionPositionOffset = new Vector3 (0f, 0f, SECTION_WIDTH);
		
		if (NUM_OF_SECTIONS_ON_SCENE > run.sections.Count) NUM_OF_SECTIONS_ON_SCENE = run.sections.Count;
		
		for (int i=0; i<NUM_OF_SECTIONS_ON_SCENE; i++) {
			AddNewSection((i==0), true);
		}
		

	}
	
	void AddNewSection(bool disableTrigger = false, bool isAfirsSection = false){
		if (nextSection >= run.sections.Count) return;
		StartCoroutine(AddSection(disableTrigger, isAfirsSection));
	}
	
	IEnumerator AddSection(bool disableTrigger = false, bool isAfirsSection = false){
		//Debug.Log ("alon___ AddSection() Coututine !");
		section = run.sections [nextSection];
		
		if (instantiateDoubleSection) {
			instantiateDoubleSection = false;
			
			run.sections [nextSection].ObjectOnScene = (GameObject)Instantiate (PrefabManager.instanse.GetPrefabBySectionID(section.id), nextSectionPosition, Quaternion.Euler( nextSectionRotation) );
            
			if(section.collectables.Count > 0 || section.obstacles.Count > 0) section.ObjectOnScene.GetComponent<WorldSection>().PlaceObjects( section.collectables, section.obstacles );
			
			TurnBuilderRight();
			TurnBuilderRight();
			nextSectionPosition += nextSectionPositionOffset;
			nextSectionPosition += nextSectionPositionOffset;
			section.ObjectOnSceneCopy = (GameObject)Instantiate ( PrefabManager.instanse.GetPrefabBySectionID(section.id), nextSectionPosition, Quaternion.Euler( nextSectionRotation) );
			if(section.collectables.Count > 0 || section.obstacles.Count > 0) section.ObjectOnSceneCopy.GetComponent<WorldSection>().PlaceObjects( section.collectables, section.obstacles );
			
			nextSectionPosition -= nextSectionPositionOffset;
			TurnBuilderLeft();

			// --

			if (disableTrigger) {
				section.ObjectOnScene.GetComponentsInChildren<WorldSectionTrigger> () [0].isEnabled = false;
				section.ObjectOnSceneCopy.GetComponentsInChildren<WorldSectionTrigger> () [0].isEnabled = false;
			}

			if (isAfirsSection) {
				section.ObjectOnScene.GetComponent<WorldSection> ().buildLow = false;
				section.ObjectOnSceneCopy.GetComponent<WorldSection> ().buildLow = false;
			}

			waitnigForBuildTSections = true;
			FreezeBuilder = true;

			//Debug.Log ("alon___ instantiateDoubleSection");
		} else {
			section.ObjectOnScene = (GameObject)Instantiate ( PrefabManager.instanse.GetPrefabBySectionID(section.id), nextSectionPosition, Quaternion.Euler( nextSectionRotation) );
			if(section.collectables.Count > 0 || section.obstacles.Count > 0) section.ObjectOnScene.GetComponent<WorldSection>().PlaceObjects( section.collectables, section.obstacles );
		
			if (disableTrigger) section.ObjectOnScene.GetComponentsInChildren<WorldSectionTrigger>()[0].isEnabled = false;
			if (isAfirsSection) section.ObjectOnScene.GetComponent<WorldSection>().buildLow = false;

			waitnigForBuildTSections = false;
			//Debug.Log ("alon___ not instantiateDoubleSection");
		}
		
        run.sections[nextSection].dontSetActiveFalse = run.sections[nextSection].ObjectOnScene.GetComponent<WorldSection>().dontSetActiveFalse;
		
		
		if (Utils.IsTurnLeftSection(section.id)) TurnBuilderLeft ();
		if (Utils.IsTurnRightSection(section.id)) TurnBuilderRight ();
		
		if (Utils.IsTurnUpSection(section.id)) TurnBuilderUp ();
		if (Utils.IsTurnDownSection(section.id)) {
			TurnBuilderDown ();
			vectorZona = section.ObjectOnScene.transform.position;
			vectorZona.y -= SECTION_HEIGHT_LEVEL;
			section.ObjectOnScene.transform.position = vectorZona;
			
			if(section.ObjectOnSceneCopy != null){
				vectorZona = section.ObjectOnSceneCopy.transform.position;
				vectorZona.y -= SECTION_HEIGHT_LEVEL;
				section.ObjectOnSceneCopy.transform.position = vectorZona;
			}
			
		}
		
		if (Utils.IsTSection(section.id)) {
			instantiateDoubleSection = true;
			TurnBuilderLeft();
		}
		
		
		nextSectionPosition += nextSectionPositionOffset;
		
		
		nextSection++;
		CurrentNumOfSectionsOnScene++;


		if (needToChangeSectionsPosition) {
			needToChangeSectionsPosition = false;
			SecretAreaNewSectionsPosition (SecterAreaHandler.numOfSectionsToMove);
			//Debug.Log ("alon___ should move the section position");
		}
		
		yield return null;
	}
	


	IEnumerator RemoveUsedSections()
    {
        yield return removeDelay;
        waitingToRemoveSection = false;
        if (!PlayerController.instance.isDead)
        {
            if (previosSectionToDelete > -1)
            {
                RemovePreviosSection();
            }
            if (!run.sections[sectionIndexToDelete].dontSetActiveFalse)
            {
                run.sections[sectionIndexToDelete].ObjectOnScene.SetActive(false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnScene);
                run.sections[sectionIndexToDelete].ObjectOnScene = null;
            }

			if (run.sections [sectionIndexToDelete].ObjectOnSceneCopy != null) {
				run.sections [sectionIndexToDelete].ObjectOnSceneCopy.SetActive (false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnSceneCopy);
				run.sections [sectionIndexToDelete].ObjectOnSceneCopy = null;
			}

			sectionIndexToDelete++;
			CurrentNumOfSectionsOnScene--;
		} else {
			previosSectionToDelete = sectionIndexToDelete;
		}
	}

	void RemovePreviosSection(){

		run.sections [previosSectionToDelete].ObjectOnScene.SetActive (false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnScene);
		run.sections [previosSectionToDelete].ObjectOnScene = null;

		if (run.sections [previosSectionToDelete].ObjectOnSceneCopy != null) {
			run.sections [previosSectionToDelete].ObjectOnSceneCopy.SetActive (false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnSceneCopy);
			run.sections [previosSectionToDelete].ObjectOnSceneCopy = null;
		}

		sectionIndexToDelete++;
		CurrentNumOfSectionsOnScene--;
		previosSectionToDelete = -1;
	}

	void RemoveSectionInstantly(){
		StopCoroutine ("RemoveUsedSections");
		waitingToRemoveSection = false;
		run.sections [sectionIndexToDelete].ObjectOnScene.SetActive (false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnScene);
		run.sections [sectionIndexToDelete].ObjectOnScene = null;
		if (run.sections [sectionIndexToDelete].ObjectOnSceneCopy != null) {
			run.sections [sectionIndexToDelete].ObjectOnSceneCopy.SetActive (false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnSceneCopy);
			run.sections [sectionIndexToDelete].ObjectOnSceneCopy = null;
		}
		sectionIndexToDelete++;
		CurrentNumOfSectionsOnScene--;
		//StartCoroutine (LateNewSectionsPosition());
		//Debug.Log ("alon___ secter area - RemoveSectionInstantly()");
	}

	
	void TurnBuilderLeft(){
		nextSectionRotation.y -= 90f;
		
		if (nextSectionPositionOffset.z != 0f) {
			nextSectionPositionOffset.x = -nextSectionPositionOffset.z;
			nextSectionPositionOffset.z = 0f;
		} else {
			nextSectionPositionOffset.z = nextSectionPositionOffset.x;
			nextSectionPositionOffset.x = 0f;
		}
		
	}
	
	void TurnBuilderRight(){
		nextSectionRotation.y += 90f;
		
		if (nextSectionPositionOffset.z != 0f) {
			nextSectionPositionOffset.x = nextSectionPositionOffset.z;
			nextSectionPositionOffset.z = 0f;
		} else {
			nextSectionPositionOffset.z = -nextSectionPositionOffset.x;
			nextSectionPositionOffset.x = 0f;
		}
		
	}
	
	void TurnBuilderUp(){
		nextSectionPosition.y += SECTION_HEIGHT_LEVEL;
	}
	
	void TurnBuilderDown(){
		nextSectionPosition.y -= SECTION_HEIGHT_LEVEL;
	}


	public void SecretAreaNewSectionsPosition(int numberOfSections){
		nextSectionPosition += run.sections [RunManager.LastSectionNumber - 1].ObjectOnScene.transform.forward * 30 * numberOfSections; //the new position for the sections
	}

}
