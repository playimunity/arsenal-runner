using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefabManager : MonoBehaviour{
	public static PrefabManager instanse;

	public GameObject[] holloway_section_prefabs;
	public GameObject[] saint_james_park_section_prefabs;
	public GameObject[] oxford_section_prefabs;
	public GameObject[] tower_bridge_section_prefabs;
	public GameObject[] camden_section_prefabs;
	public GameObject[] mall_section_prefabs;

//	public List<GameObject> buildingsGeneric5m;
//	public List<GameObject> buildingsGeneric10m;
//	public List<GameObject> buildingsGeneric15m;

//	public List<GameObject> buildingsGothic5m;
//	public List<GameObject> buildingsGothic10m;
//	public List<GameObject> buildingsGothic15m;

//	public List<GameObject> buildingsRambla5m;
//	public List<GameObject> buildingsRambla10m;
//	public List<GameObject> buildingsRambla15m;

	//collectables:
	public GameObject coin;
	public GameObject magnet;
	public GameObject ballStack;
	//public GameObject energyDrink;
	public GameObject tutorialPoint;
	public GameObject whistle;
	public GameObject speedBooster;
    public GameObject shield;
	public GameObject x2Coins;
	//public GameObject StadiumPortal;

	// obstacles
//	public GameObject BCNeta;
//	public GameObject StreetLamp;
//	public GameObject Wheelbarrow;
//	public GameObject Wheelbarrow2;
//	public GameObject UnderConstraction1;
//	public GameObject UnderConstraction2;
//	public GameObject UnderConstraction3;
//	public GameObject UnderConstraction4;
//	public GameObject UnderConstraction5;
//	public GameObject UnderConstraction6;
//	public GameObject Bicycle;
//	public GameObject BicycleRow;
//	public GameObject Bus;
//	public GameObject BusStop;
//	public GameObject crowd;
//	public GameObject Cafe1;
//	public GameObject Cafe2;
//	public GameObject Cafe3;
//	public GameObject Cafe4;
//	public GameObject MapSign1;
//	public GameObject MapSign2;
//	public GameObject Minibus;
//	public GameObject Moped;
//	public GameObject Sedan;
//	public GameObject TrafficLight;
//	public GameObject TrashCanBig;
//	public GameObject TrashCanBig2;
//	public GameObject TrashCanBig3;
//	public GameObject TrashCanSmall;
//	public GameObject Bush;
//	public GameObject Bush2;
//	public GameObject Bush3;
//	public GameObject Hatchback;
//	public GameObject Taxi;
//	public GameObject EnemyRun;
//	public GameObject enemySlide;
//	public GameObject Female;
//	public GameObject Male;
//	public GameObject Flowers;
//	public GameObject Flowers2;
//	public GameObject Flowers3;
//	public GameObject FlowerStall1;
//	public GameObject FlowerStall2;
//	public GameObject FoodStall1;
//	public GameObject FoodStall2;
//	public GameObject MopedMoving;
//	public GameObject GlassPeople;
//	public GameObject Pillar;
//	public GameObject TruckFood;
//	public GameObject TruckSmall;
//	public GameObject TSign;
//	public GameObject TSign2;
//	public GameObject Barrier1;
//	public GameObject Barrier2;
//	public GameObject Barrier3;
//	public GameObject Gate2;
//	public GameObject BlockingRoad;
//	public GameObject ElectricGateClose;
//	public GameObject ElectricGateOpen;
//	public GameObject FallingTree;

	// Arsenal Obstacle:
	public GameObject Ars_Bus;
	public GameObject Ars_Car1;
	public GameObject Ars_Car2;
	public GameObject Ars_Car3;
	public GameObject Ars_Car4;
	public GameObject Ars_Car_Police;
	public GameObject Ars_Taxi;
	public GameObject Ars_Truck1;
	public GameObject Ars_Truck2;
    public GameObject Ars_Truck3;
    public GameObject Ars_Truck4;
    public GameObject Ars_Truck5;
	public GameObject Ars_Van1;
	public GameObject Ars_Van2;
	public GameObject Ars_Van_Police;
	public GameObject Ars_Roadblock_Slide;
	public GameObject Ars_RoadBlock_Jump;
	public GameObject Ars_Camera_Post;
	public GameObject Ars_Flag_Rope;
	public GameObject Ars_Spaceship;
	public GameObject Ars_Roadblock_3_Lane;
	public GameObject Ars_Garden;
    public GameObject Ars_Garden1;
    public GameObject Ars_Clothes_Cart;
    public GameObject Ars_Clothes_Stall;
    public GameObject Ars_Concrete_Truck1;
    public GameObject Ars_Concrete_Truck2;
    public GameObject Ars_Bush1;
    public GameObject Ars_Bush2;
    public GameObject Ars_Bush3;
    public GameObject Ars_Bush4;
    public GameObject Ars_FoodVan;
    public GameObject Ars_GarbageTruck;
    public GameObject Ars_HotDogStall;
    public GameObject Ars_Icecream_Stall;
    public GameObject Ars_Tree1;
    public GameObject Ars_Tree2;

	// Gothic Obstacle:
//	public GameObject door1;
//	public GameObject door2;
//	public GameObject halfHanging1;
//	public GameObject halfHanging2;
//	public GameObject hanging1;
//	public GameObject hanging2;
//	public GameObject sideBuilding;
//	public GameObject sign1;
//	public GameObject sign2;

	// LaRambla Obstacle:
//	public GameObject huemanStatue1;
//	public GameObject huemanStatue2;
//	public GameObject huemanStatue3;

	// Subway Obstacle:
//	public GameObject train;

	// Guell Obstacle:
//	public GameObject BenchMosaic1;
//	public GameObject BenchMosaic2;
//	public GameObject BenchMosaic3;
//	public GameObject BenchMosaic4;
//	public GameObject Boulder;
//	public GameObject GuellBush1;
//	public GameObject GuellBush2;
//	public GameObject GuellBush3;
//	public GameObject GuellTrash;
//	public GameObject GuellTrunk;
//	public GameObject PalmTree;
//	public GameObject StoneBench;



	//Players:
	public PlayerStructure chosenPlayer;


	public List<PlayerStructure> PlayerStructures;
	public GameObject PlayerPrefab;
	public GameObject BenchPlayerPrefab;

	// UI

	public GameObject cupItemElement;
//	public GameObject runItemElement;
	public GameObject FriendItemElement;

	void Awake(){
		DontDestroyOnLoad(gameObject);
		instanse = this;
	}


	public void ChoosePlayer(int playerID){
		chosenPlayer = PlayerStructures [playerID];
	}


	public GameObject GetPrefabBySectionID(string id)
    {

        // HOLLOWAY
        if (id.Equals("hrstr"))
            return PrefabManager.instanse.holloway_section_prefabs[0];
        if (id.Equals("hraly"))
            return PrefabManager.instanse.holloway_section_prefabs[1];
        if (id.Equals("hrcrs"))
            return PrefabManager.instanse.holloway_section_prefabs[2];
        if (id.Equals("hrtr"))
            return PrefabManager.instanse.holloway_section_prefabs[3];
        if (id.Equals("hrtl"))
            return PrefabManager.instanse.holloway_section_prefabs[4];
        if (id.Equals("hrex"))
            return PrefabManager.instanse.holloway_section_prefabs[5];
        if (id.Equals("hrpreend"))
            return SetAsPreOrEnd(PrefabManager.instanse.holloway_section_prefabs[6],10);
        if (id.Equals("hrend"))
            return SetAsPreOrEnd(PrefabManager.instanse.holloway_section_prefabs[7],11);

        // SAINT JAMES PARK
        if (id.Equals("spstr"))
            return PrefabManager.instanse.saint_james_park_section_prefabs[0];
        if (id.Equals("spen"))
            return PrefabManager.instanse.saint_james_park_section_prefabs[1];
        if (id.Equals("spbridge"))
            return PrefabManager.instanse.saint_james_park_section_prefabs[2];
        if (id.Equals("sphouse"))
            return PrefabManager.instanse.saint_james_park_section_prefabs[3];
        if (id.Equals("spex"))
            return PrefabManager.instanse.saint_james_park_section_prefabs[4];
        if (id.Equals("sppreend"))
            return SetAsPreOrEnd(PrefabManager.instanse.saint_james_park_section_prefabs[5],10);
        if (id.Equals("spend"))
            return SetAsPreOrEnd(PrefabManager.instanse.saint_james_park_section_prefabs[6],11);

        // OXFORD
        if (id.Equals("osstr"))
            return PrefabManager.instanse.oxford_section_prefabs[0];
        if (id.Equals("osaly"))
            return PrefabManager.instanse.oxford_section_prefabs[1];
        if (id.Equals("oscrs"))
            return PrefabManager.instanse.oxford_section_prefabs[2];
        if (id.Equals("ostr"))
            return PrefabManager.instanse.oxford_section_prefabs[3];
        if (id.Equals("ostl"))
            return PrefabManager.instanse.oxford_section_prefabs[4];
        if (id.Equals("osex"))
            return PrefabManager.instanse.oxford_section_prefabs[5];
        if (id.Equals("ospreend"))
        {
            return SetAsPreOrEnd(PrefabManager.instanse.oxford_section_prefabs[6], 10);
        }
        if( id.Equals("osend") ) 
        {
            return SetAsPreOrEnd(PrefabManager.instanse.oxford_section_prefabs[7], 11);
        }       

		// TOWER BRIDGE
        if( id.Equals("tben") ) 		return PrefabManager.instanse.tower_bridge_section_prefabs[0];
        if( id.Equals("tbempty") ) 		return PrefabManager.instanse.tower_bridge_section_prefabs[1];
        if( id.Equals("tbex1") )        return PrefabManager.instanse.tower_bridge_section_prefabs[2];
        if( id.Equals("tbex2") )        return PrefabManager.instanse.tower_bridge_section_prefabs[3];
        if( id.Equals("tbex3") )        return PrefabManager.instanse.tower_bridge_section_prefabs[4];
        if( id.Equals("tbstr") )        return PrefabManager.instanse.tower_bridge_section_prefabs[5];
        if( id.Equals("tbpreend") )     return SetAsPreOrEnd(PrefabManager.instanse.tower_bridge_section_prefabs[6],10);
        if( id.Equals("tbend") )      return SetAsPreOrEnd(PrefabManager.instanse.tower_bridge_section_prefabs[7],11);

		// CAMDEN
        if( id.Equals("ctstr") ) 		return PrefabManager.instanse.camden_section_prefabs[0];
        if( id.Equals("ctaly") ) 		return PrefabManager.instanse.camden_section_prefabs[1];
        if( id.Equals("cttr") ) 		return PrefabManager.instanse.camden_section_prefabs[2];
        if( id.Equals("cttl") ) 		return PrefabManager.instanse.camden_section_prefabs[3];
        if( id.Equals("ctex1") ) 		return PrefabManager.instanse.camden_section_prefabs[4];
        if( id.Equals("ctex2") ) 		return PrefabManager.instanse.camden_section_prefabs[5];
        if( id.Equals("ctpreend") )     return SetAsPreOrEnd(PrefabManager.instanse.camden_section_prefabs[6],10);
        if( id.Equals("ctend") )        return SetAsPreOrEnd(PrefabManager.instanse.camden_section_prefabs[7],11);

		// MALL
        if( id.Equals("msstr") ) 			return PrefabManager.instanse.mall_section_prefabs[0];
        if( id.Equals("msaly") ) 			return PrefabManager.instanse.mall_section_prefabs[1];
        if( id.Equals("msex1") )            return PrefabManager.instanse.mall_section_prefabs[2];
        if( id.Equals("msex2") )            return PrefabManager.instanse.mall_section_prefabs[3];
        if( id.Equals("mspreend") )         return SetAsPreOrEnd(PrefabManager.instanse.mall_section_prefabs[4],10);
        if( id.Equals("msend") )            return SetAsPreOrEnd(PrefabManager.instanse.mall_section_prefabs[5],11);




		// Default
        string prefix = id.Substring(0, 2);
        GameObject[] regionSections;
        switch (prefix)
        {
            case "hr":
                regionSections = holloway_section_prefabs;
                break;
            case "sp":
                regionSections = saint_james_park_section_prefabs;
                break;
            case "os":
                regionSections = oxford_section_prefabs;
                break;
            case "tb":
                regionSections = tower_bridge_section_prefabs;
                break;
            case "ms":
                regionSections = mall_section_prefabs;
                break;
            case "ct":
                regionSections = camden_section_prefabs;
                break;
            default:
                regionSections = holloway_section_prefabs;
                break;
        }
		return regionSections [0];
	}
    private GameObject SetAsPreOrEnd(GameObject section, int direction)
    {
        section.GetComponentInChildren<WorldSectionTrigger>().direction = direction;
        return section;
    }
}

[System.Serializable]
public struct PlayerStructure{

	public string PlayerName;
	public int PlayerID;
	public Sprite PlayerPhoto;
	public int PlayerNumber;
	public Perk.PerkType DefaultPerk;
	public Texture PlayerTexture;
	public Texture ClothTexture;
	public Texture PlayerTrainingTexture;
	public Texture ClothTrainingTexture;
	public GameObject PlayerHead;
	//public bool isLedgend;
	public PlayerData.PlayerRole playerRole;

}
