﻿using UnityEngine;
using System.Collections;

public class SpeedBooster : Collectable {


	bool triggered = false;




	void OnTriggerEnter(Collider col){
		if (col.transform.gameObject.tag == "Player") {
			//ParticleCollectGenerator.instance.PlayParticle(transform.position , transform.eulerAngles);
			RunManager.OnSpeedBoostCollected();
            //RunManager.instance.ActivateSpeedBoost(false);
			AudioManager.Instance.OnPowerUpPick ();
            gameObject.SetActive (false);
		}
	}
}
