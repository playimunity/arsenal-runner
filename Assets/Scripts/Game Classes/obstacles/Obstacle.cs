using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Obstacle : MonoBehaviour {

	public enum ObstacleTypes {TRASH_CAN_SMALL , TRASH_CAN_BIG , BCNETA, STREET_LAMP, WHEELBARROW, UNDER_CONSTRACTION_1, UNDER_CONSTRACTION_2, UNDER_CONSTRACTION_3 , UNDER_CONSTRACTION_4 , MINIBUS ,BICYCLE_ROW, BICYCLE , BUS_STOP, CAFE1 , CAFE2 , MAP_SIGN1 , MAP_SIGN2 , MOPED , SEDAN , TRAFFIC_LIGHT1 , BUSH , Car_Hatchback , Car_Taxi , Moped_Moving , Flower_Pot , Flower_Stall_1 , Flower_Stall_2 , Food_Stall_1 , Food_Stall_2 , Pillar , Truck_Food , Truck_Small , T_Sign , Enemy_Run , Enemy_Slide , People_With_Glass , Male , Female , Hanging1 , Hanging2 , Door1 , Door2 , Half_Hanging1 , Half_Hanging2 , Side_Building , Sign1 , Sign2 , Train , Bush2 , Bush3 , Cafe3 , Cafe4 , Flower_Pot2 , Flower_Pot3 , TRASH_CAN_BIG2 , T_Sign2 , WHEELBARROW2 , Barrier1 , Barrier2 , Barrier3 , UNDER_CONSTRACTION_5 , UNDER_CONSTRACTION_6 , Blocking_Road , Electric_Gate_Close , Electric_Gate_Open , FallingTree , Gate2 , Guell_Bush1 , Guell_Bush2 , Guell_Bush3 , Guell_Trash , Guell_Boulder , BenchMosaic1 , BenchMosaic2 , BenchMosaic3 , BenchMosaic4 , PalmTree , StoneBench , GuellTrunk , HuemanStatue1 , HuemanStatue2 , HuemanStatue3 , TRASH_CAN_BIG3 , CROWD
        , BUS , CAR1 , CAR2 , CAR3, CAR4, CAR_POLICE, TAXI, TRUCK1, TRUCK2,TRUCK3,TRUCK4,TRUCK5, VAN1, VAN2, VAN_POLICE, ROADBLOCK_SLIDE , ROADBLOCK_JUMP , CAMERA_POST , FLAG_ROPE , SPACESHIP, ROADBLOCK_3_LANE , GARDEN, GARDEN1, GARDEN2,BUSH4, CLOTHES_CART,
        TREE1, TREE2, CLOTHES_STALL, CONCRETE_TRUCK1,CONCRETE_TRUCK2,FOOD_VAN,GARBAGE_TRUCK,HOTDOGS_STALL,ICECREAME_STALL};
	
	public enum MoveTypes { STATIC,MOVE };
	public enum HardnesTypes { SOFT, HARD };
	public enum SmashableTypes {  NOT_SMASHABLE, SMASHABLE, NOT_SMASHABLE_EVEN_BY_GOD};

	public ObstacleTypes type;
	public SmashableTypes smashable = SmashableTypes.NOT_SMASHABLE;
	public MoveTypes move = MoveTypes.STATIC;
	//public MoveTypes tempMove;
	public HardnesTypes hardness = HardnesTypes.SOFT;

	public GameObject player;
	public GameObject carLight1;
	public GameObject carLight2;
	//public GameObject ballTarget;
	public GameObject ObstacleMesh;

	//public Animator targetAnimator;

	public float moveSpeed;

	protected bool moveTriggered = false;
	bool colided = false;
	bool ballCollided = false;
	bool flickering = false;
	bool staticColor = false;
	bool targetOn = false;
	
	protected Vector3 tempPos;

	Vector3 obstacleScale;
	int randomRotation;
	float randomRotationFloat;
	int[] ints = new int[2];

	public Renderer[] myRenderers;
	public List<Material> myMaterials = new List<Material>();
	Color alphaColor;
	Color tempColor;
	float decreaceFactor;
	float startOpacity = 0.9f;
	float endOpacity = 0.1f;
	float startDistance = 25;

	public float distanseToHorn = 20f;

	bool hornd = false;

	Transform PlayerTransform;

//	public LODGroup lod;
//	public float lod1Val = 37f;
//	public float lodCullVal = 10f;



	void Awake () {
		//StartCoroutine(BeKinematic());
		player = GameObject.FindGameObjectWithTag ("Player");

		PlayerTransform = player.transform;

	}



	void FixedUpdate(){


		if (move == Obstacle.MoveTypes.MOVE && moveTriggered) {
			transform.Translate (Vector3.back * moveSpeed * Time.deltaTime); //moving the object.
			if (!flickering && Vector3.Distance (transform.position, PlayerTransform.position) <= distanseToHorn) { //flickering.
				flickering = true;
				StartCoroutine (Flicker ());
				PlayHornSFX ();
			} 
		}

	}




	void OnCollisionEnter(Collision col) {

		if (col.gameObject.tag == "Player" && !colided) {
			colided = true;

			if (smashable == SmashableTypes.NOT_SMASHABLE_EVEN_BY_GOD) {
				PlayerController.instance.DeadlyCrush ();
				AudioManager.Instance.OnHardHit ();
				PlayerController.instance.obstacleToSmash = this;
				return;
			}
			
			if (hardness == HardnesTypes.HARD && !PlayerController.instance.sheildOn) {
				move = MoveTypes.STATIC;
				PlayerController.instance.hardDamage = true;
				GetComponent<Rigidbody> ().isKinematic = true;
				//GetComponent<BoxCollider>().enabled = false;
				PlayerController.instance.Damage (10);
				AudioManager.Instance.OnHardHit ();
				flickering = false;
				PlayerController.instance.obstacleToSmash = this;
				if (type == ObstacleTypes.BUS) {
					NativeSocial._.ReportAchievment (SocialConstants.achievement_missed_the_bus);
				}
				
			} else if (hardness == HardnesTypes.SOFT && !PlayerController.instance.sheildOn) {
				GetComponentInChildren<BoxCollider> ().enabled = false;
				PlayerController.instance.Damage (1);
				AudioManager.Instance.OnSoftHit ();
				ParticleCloudeGenerator.instance.ParticleWhistle (transform.position, transform.eulerAngles);
				//Debug.Log ("alon__________ obstacle name: " + gameObject.name);
				gameObject.SetActive (false);//Destroy (gameObject);
			} else {
				ParticleCloudeGenerator.instance.ParticleBoom(transform.position, transform.eulerAngles);
				//PlayerController.instance.GodSphereFX ();
				AudioManager.Instance.OnHitGod ();
				PlayerController.instance.sheildModeDamage = true;
				//RunManager.instance.StopSheild ();
				gameObject.SetActive (false);//Destroy (gameObject);
			}
				
		}else if (move == MoveTypes.MOVE && col.transform.tag != "Section" && col.transform.tag != "Ball") {
			move = MoveTypes.STATIC;
			GetComponentInChildren<Rigidbody> ().isKinematic = true;

		}
		//LocalOnCollisionEnter(col);
	}

	public void SmashObstacle(){   //call from player controller:
		ParticleCloudeGenerator.instance.ParticleBoom(transform.position, transform.eulerAngles);
		AudioManager.Instance.OnHitGod ();
		gameObject.SetActive (false);//Destroy (gameObject);
	}


//	void OnTriggerEnter(Collider col) {
//		if (col.gameObject.tag == "Ball" && !colided) {
//
//			colided = true;
//			StartCoroutine (DestroyTarget());
//			PlayerController.instance.ballHitTheTarget = true;
//		}
//	}



	public void StartDestroyTarget(){
		StartCoroutine (DestroyTarget());
	}

	IEnumerator DestroyTarget(){

		//GameManager.Instance.SlowMo_End ();

		//if(targetAnimator != null) targetAnimator.Play ("target_anim_down");
		AudioManager.Instance.OnBallSmash ();
		ParticleCloudeGenerator.instance.ParticleCloud (transform.position);
//		GetComponent<BoxCollider>().enabled = false;
//		ballTarget.SetActive (false);
//		ObstacleMesh.SetActive (false);
//		yield return new WaitForSeconds (0.3f);
//		PlayerController.instance.ballShooted = false;
		RunManager.OnTargetShotByBall ();
		//yield return new WaitForSeconds (0.5f);
		gameObject.SetActive (false);//Destroy (gameObject);
		yield return null;
	}


	static WaitForSeconds _wfs_1_5;
	static WaitForSeconds wfs_1_5{
		get{
			if (_wfs_1_5 == null)
				_wfs_1_5 = new WaitForSeconds (1.5f);

			return _wfs_1_5;
		}
	}

	static WaitForSeconds _wfs_0_2;
	static WaitForSeconds wfs_0_2{
		get{
			if (_wfs_0_2 == null)
				_wfs_0_2 = new WaitForSeconds (0.2f);

			return _wfs_0_2;
		}
	}

	IEnumerator BeKinematic(){
		//Debug.Log ("_wfs_1_5: " + _wfs_1_5);
		yield return wfs_1_5;
		if (move == MoveTypes.STATIC) {
			GetComponent<Rigidbody> ().isKinematic = true;
		}
	}



//	IEnumerator DeactivateTarget(){
//		if (!PlayerController.instance.withBall && targetAnimator != null) {
//			targetAnimator.Play ("target_anim_down");
//			yield return new WaitForSeconds (0.5f);
//			ballTarget.SetActive (false);
//		}
//	}


	public void PlayHornSFX(){
		if (hornd) return;

		hornd = true;


		switch (type) {
			case ObstacleTypes.BCNETA:
			case ObstacleTypes.BUS:
			case ObstacleTypes.MINIBUS:
			case ObstacleTypes.Truck_Small:
			{
				AudioManager.Instance.OnHornBigCar ();
				break;
			}

			case ObstacleTypes.Car_Hatchback:
			case ObstacleTypes.SEDAN:
			case ObstacleTypes.TRUCK1:
			{
				AudioManager.Instance.OnHornSmallCar();
				break;
			}
		    case ObstacleTypes.TRUCK2:
			{
				AudioManager.Instance.OnHornSmallCar();
				break;
			}


			case ObstacleTypes.TAXI:
			{
				AudioManager.Instance.OnHornTaxi();
				break;
			}

			case ObstacleTypes.Moped_Moving:
			{
				AudioManager.Instance.OnHornBike();
				break;
			}

			case ObstacleTypes.Train:
			{
				AudioManager.Instance.OnTrainHorn ();
				break;
			}
		}

	}


	IEnumerator Flicker(){
		carLight1.SetActive (true);
		carLight2.SetActive (true);
		yield return wfs_0_2;
		carLight1.SetActive (false);
		carLight2.SetActive (false);
		yield return wfs_0_2;
		if (flickering) {
			StartCoroutine (Flicker());
		}
	}




	}