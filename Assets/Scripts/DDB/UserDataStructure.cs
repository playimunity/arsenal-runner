using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;
using Facebook.Unity;
using System;

[DynamoDBTable("ars_bd_users")]
public class UserDataStructure{

	[DynamoDBHashKey] 
	public string user_id { get; set; }

	[DynamoDBProperty]
	public string name { get; set; }

	[DynamoDBProperty]
	public int iapPoints { get; set; }

	[DynamoDBProperty]
	public int playTimeInMinutes { get; set; }

	[DynamoDBProperty]
	public int totalCoins { get; set; }

	[DynamoDBProperty]
	public int totalMagnets { get; set; }

	[DynamoDBProperty]
	public int totalEnergyDrinks { get; set; }

	[DynamoDBProperty]
	public float totalGiftCards { get; set; }

	[DynamoDBProperty]
	public int totalMedikKits { get; set; }

	[DynamoDBProperty]
	public int activeCupId { get; set; }

	[DynamoDBProperty]
	public string activeCupData { get; set; } 

	[DynamoDBProperty]
	public string completedRunsData { get; set; } 

	[DynamoDBProperty]
	public string activeCupExpiration { get; set; } 

	[DynamoDBProperty]
	public string winnedCups { get; set; }

	[DynamoDBProperty]
	public string purchasedPlayers { get; set; }

	[DynamoDBProperty]
	public string openedRegions { get; set; }

	[DynamoDBProperty("players")]
	public List<PlayerItem> players { get; set; }

	[DynamoDBProperty]
	public string foundationDate { get; set; }

	[DynamoDBProperty]
	public int highScore { get; set; }

	[DynamoDBProperty]
	public int trainingTime { get; set; }

	[DynamoDBProperty]
	public int totalGoldMedals { get; set; }

	[DynamoDBProperty]
	public int totalSilverMedals { get; set; }

	[DynamoDBProperty]
	public int totalFailedRuns { get; set; }

	[DynamoDBProperty]
	public int rankScore { get; set; }

	[DynamoDBProperty]
	public int bILevelUp { get; set; }

	[DynamoDBProperty]
	public string userType { get; set; }

	[DynamoDBProperty]
	public string email { get; set; }

	[DynamoDBProperty]
	public string gender { get; set; }

	[DynamoDBProperty]
	public string firstName { get; set; }

	[DynamoDBProperty]
	public string lastName { get; set; }

	[DynamoDBProperty]
	public string lang { get; set; }

	[DynamoDBProperty]
	public string country { get; set; }

	[DynamoDBProperty]
	public int canSendEmail { get; set; }

	[DynamoDBProperty]
	public string Ftd { get; set; }

	[DynamoDBProperty]
	public string whaleID { get; set; }

	// -----------------------------------------------------------------------------

	PlayerItem pi;

	public UserDataStructure(){
		user_id = UserData.Instance.userID;
		name = UserData.Instance.UserName;
		iapPoints = DAO.IAPPoints;
		playTimeInMinutes = DAO.PlayTimeInMinutes;
		totalCoins = DAO.TotalCoinsCollected;
		totalMagnets = DAO.TotalMagnetsCollected;
		totalEnergyDrinks = DAO.TotalSpeedBoostsCollected;
		totalGiftCards = DAO.TotalGiftCardsCollected;
		totalMedikKits = DAO.TotalMedikKitsCollected;
		activeCupId = DAO.ActiveRunID;
		activeCupData = DAO.ActiveRunData;
		completedRunsData = DAO.CompletedRunsData;
		winnedCups = DAO.WinnedCups;
		purchasedPlayers = DAO.PurchasedPlayers;
		openedRegions = DAO.OpenedRegions;
	
		players = new List<PlayerItem> ();
		for (int i = 0; i < DAO.Instance.LOCAL.playerData.Length; i++) {
			pi = new PlayerItem ();
			pi.Set (i);
			players.Add ( pi );
		}

		foundationDate = DAO.InstallDate.ToShortDateString();
		highScore = DAO.BestPracticeScore;
		trainingTime = DAO.TotalPracticeTime;
		totalGoldMedals = DAO.TotalGoldMedals;
		totalSilverMedals = DAO.TotalSilverMedals;
		totalFailedRuns = DAO.TotalFailedRuns;
		rankScore = DAO.RankScore;
		bILevelUp = DAO.BILevelUp;
		userType = DAO.UserType.ToString();
		email = DAO.Email;
		gender = DAO.Gender;
		firstName = DAO.FirstName;
		lastName = DAO.LastName;
		lang = DAO.Lang;
		country = DAO.Country;
		if (DAO.CanSendEmail)
			canSendEmail = 1;
		else
			canSendEmail = 0;
		Ftd = DAO.Ftd;
		whaleID = DAO.whaleID;
	}

	public void ReplaceLocalData(){
		if (DDB._ != null) {
			DDB._.SaveOldProgressData ();
		}
		DAO.IAPPoints = iapPoints;
		DAO.PlayTimeInMinutes = playTimeInMinutes;
		DAO.TotalCoinsCollected = totalCoins;
		DAO.TotalMagnetsCollected = totalMagnets;
		DAO.TotalSpeedBoostsCollected = totalEnergyDrinks;
		DAO.TotalGiftCardsCollected = totalGiftCards;
		//if(totalMedikKits != null) DAO.TotalMedikKitsCollected = totalMedikKits;
		DAO.TotalMedikKitsCollected = totalMedikKits;
		DAO.ActiveRunID = activeCupId;
		DAO.ActiveRunData = activeCupData;
		DAO.CompletedRunsData = completedRunsData;
		DAO.WinnedCups = winnedCups;
		DAO.PurchasedPlayers = purchasedPlayers;
		DAO.OpenedRegions = openedRegions;

		for(int i=0; i<players.Count; i++){
			DAO.Instance.LOCAL.playerData [i] = players [i].data;
		}

		DAO.BestPracticeScore = highScore;
		DAO.TotalPracticeTime = trainingTime;
		DAO.TotalGoldMedals = totalGoldMedals;
		DAO.TotalSilverMedals = totalSilverMedals;
		DAO.TotalFailedRuns = totalFailedRuns;
		DAO.RankScore = rankScore;

		if (!string.IsNullOrEmpty(userType)) {
			DAO.UserType = (UserData.UserType)Enum.Parse (typeof(UserData.UserType), userType);
			UserData.UpdateUserType (DAO.UserType);
		} else {
			DAO.UserType = UserData.UserType.NEW_USER;
			UserData.UpdateUserType (DAO.UserType);
		}

		DAO.Email = email;
		DAO.Gender = gender;
		DAO.FirstName = firstName;
		DAO.LastName = lastName;
        DAO.Lang = lang;
		DAO.Country = country;
		DAO.CanSendEmail = (canSendEmail == 1);
		DAO.Ftd = Ftd;
		if (iapPoints > 0 && DAO.whaleID != whaleID && (!string.IsNullOrEmpty(whaleID)))
			DAO.whaleID = whaleID;
//		switch (userType) {
//		//case "
//		}

//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;

		DAO.TutorialState = 5;
		DAO.BILevelUp = bILevelUp;
		DAO.Instance.LOCAL.SaveAll (false);



		CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

		GameManager.Instance.ReadRegions ();
		GameManager.Instance.ReadWinnedCups ();
	}

	public int NumOfWinnedCups{
		get{
			if (winnedCups == null){
				return 0;
			}else {
				return winnedCups.Split (new char[1]{ '%' }, System.StringSplitOptions.RemoveEmptyEntries).Length;
			}

		}
	}

	public int NumOfPurchasedPlayers{
		get{
			if (purchasedPlayers == null){
				return 0;
			}else {
				return purchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries).Length;
			}
		}
	}

	public void PrintMe(){
		string p = "============== User Data Structure ====================" + "\n";
		p += "User ID: " + user_id + "\n";
		p += "User Name: " + name + "\n";
		p += "IAP Points: " + iapPoints + "\n";
		p += "Play Time In Minutes: " + playTimeInMinutes + "\n";
		p += "Total Coins: " + totalCoins + "\n";
		p += "Total Magnets: " + totalMagnets + "\n";
		p += "Total Energy Drinks: " + totalEnergyDrinks + "\n";
		p += "Total Medic Kits: " + totalMedikKits + "\n";
		p += "Total Gift Cards: " + totalGiftCards + "\n";
		p += "Active Cup ID: " + activeCupId + "\n";
		p += "Active Cup DATA: " + activeCupData + "\n";
		p += "Active Cup DATA: " + completedRunsData + "\n";
		p += "Active Cup Expiration: " + activeCupExpiration + "\n";
		p += "Winned Cups: " + winnedCups + "\n";
		p += "Purchased Players: " + purchasedPlayers + "\n";
		p += "Opened Regions: " + openedRegions + "\n";
		p += "User Email: " + email + "\n";

		for (int i = 0; i < players.Count; i++) {
			p += "Player " + players [i].name + " : " + players [i].data + "\n";
		}

		p += "Foundation Day: " + foundationDate + "\n";
		p += "Practice High Score: " + highScore + "\n";
		p += "Training Time: " + trainingTime + "\n";
		p += "Gold Medals: " + totalGoldMedals + "\n";
		p += "Silver Medals: " + totalSilverMedals + "\n";
		p += "Failed Runs: " + totalFailedRuns + "\n";
		p += "Rank Score: " + rankScore + "\n";
		p += "BI Level Up: " + bILevelUp + "\n";

		p += "==========================================================";

		DDB.Log (p);
	}
}
	

public class PlayerItem{
	[DynamoDBProperty]
	public string name;
	[DynamoDBProperty]
	public string data;

	public void Set(int index){
		this.data = DAO.Instance.LOCAL.playerData[index];
		name = PrefabManager.instanse.PlayerStructures [index].PlayerName;
	}
}