using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

[System.Serializable]
public class PlayerData {

	public static string DEFAULT_PLAYER_DATA = "30|0|0|DEFAULT|1|0|0|NONE|1|NONE|0|NONE|0|0";

	// Global variables:
	public int PlayerID;
	public HealthState curHealthState = PlayerData.HealthState.ENERGETIC;

	public float fitnessPresent;

	public int maxFitness;
	public int level = 1;
	public int levelAdditionalFitness = 0;
	public int metersPassedToNextLevel;
	public enum HealthState{ENERGETIC, IDEAL, TIERED, WOUNDED};
	public PlayerLevel NextLevelSetup;
	public bool PerksDisabled = false;
	public int totalDistance;

	public System.DateTime LastFitnessUpdate;
	public System.DateTime UnfreezeFitnessTime;
	public bool fitnessFrozen = false;

	public Perk Perk_1;
	public Perk Perk_2;
	public Perk Perk_3;
	public Perk TMPPerk;

	public Player player;
	public int MaxLevel = 10;
	
	string ed;
	string[] dd;
	public Dictionary<string, JSONNode> Perks_Effect;
	public float Perk_FitnessBonusFactor = 1f;

	public enum PlayerRole{GK, DEF, MID, FR};


	public PlayerData(int playerId, Player player){
		PlayerID = playerId;
		this.player = player;

		mDecode ();

		maxFitness = DAO.Settings.DefaultMaxFitness + levelAdditionalFitness;
//		level = 1;
//		metersPassedToNextLevel = 0;

		NextLevelSetup = new PlayerLevel ();
		InitNextLevel ();

		Perks_Effect = new Dictionary<string, JSONNode> ();
		for (int i=0; i<DAO.Perks.Count; i++) {
			Perks_Effect.Add( DAO.Perks[i]["id"].Value,  DAO.Perks[i]["effect"]);
		}

		TMPPerk = GetPerkIfPurchased (Perk.PerkType.EXELLENT_FITNESS);
		if (TMPPerk.type == Perk.PerkType.EXELLENT_FITNESS) {
			Perk_FitnessBonusFactor = Perks_Effect[ Perk.GetPerkIDByPerkType(Perk.PerkType.EXELLENT_FITNESS) ][TMPPerk.level - 1].AsFloat;
		}
			
		UpdateHealthCondition ();
	}

	public int _fitness = 30;
	public int fitness{
		get{return _fitness;}
		set{
			_fitness = value;
			if (fitness <= 1) {
				_fitness = 1;
				//FreezeFitness();
			}
		}
	}

	// -----------------------------------

	public Perk GetPerkIfPurchased( Perk.PerkType perkType ){
		if (Perk_1.type == perkType) return Perk_1;
		if (Perk_2.type == perkType) return Perk_2;
		if (Perk_3.type == perkType) return Perk_3;

		return new Perk ("none", "0");
	}

	public int GetNumOfPurchasedPerks(){
		int res = 1;

		if (Perk_2.type != Perk.PerkType.NONE)
			res ++;

		if (Perk_3.type != Perk.PerkType.NONE)
			res++;

		return res;
	}

	public void LevelUpPerk(Perk.PerkType perkType){

		if (Perk_1.type == perkType) {
			Perk_1.level++;
			UnitedAnalytics.LogEvent ("Perk Upgraded or Added", "upgraded", Perk_1.type.ToString (), Perk_1.level);
			//if(GameManager.Instance.isPlayscapeLoaded)
			//BI._.Inventory_Upgrade (1, Perk_1.type.ToString (), "Skills", player.playerName);
			//GameAnalyticsWrapper.BuyNewUpgrade (Perk_1.type.ToString ());
		}
		if (Perk_2.type == perkType) {
			Perk_2.level++;
			UnitedAnalytics.LogEvent ("Perk Upgraded or Added", "upgraded", Perk_2.type.ToString (), Perk_2.level);
			//if(GameManager.Instance.isPlayscapeLoaded)	
			//BI._.Inventory_Upgrade (1, Perk_2.type.ToString (), "Skills", player.playerName);
			//GameAnalyticsWrapper.BuyNewUpgrade (Perk_2.type.ToString ());
		}
		if (Perk_3.type == perkType) {
			Perk_3.level++;
			UnitedAnalytics.LogEvent ("Perk Upgraded or Added", "upgraded", Perk_3.type.ToString (), Perk_3.level);
			//if(GameManager.Instance.isPlayscapeLoaded)	
			//BI._.Inventory_Upgrade (1, Perk_3.type.ToString (), "Skills", player.playerName);
			//GameAnalyticsWrapper.BuyNewUpgrade (Perk_3.type.ToString ());
		}



		NativeSocial._.ReportAchievment (SocialConstants.achievement_working_out);

		if (Perk_1.level == 3 && Perk_2.level == 3 && Perk_3.level == 3) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_superman);
		}
			

		TMPPerk = GetPerkIfPurchased (Perk.PerkType.EXELLENT_FITNESS);
		if (TMPPerk.type == Perk.PerkType.EXELLENT_FITNESS) {
			Perk_FitnessBonusFactor = Perks_Effect[ Perk.GetPerkIDByPerkType(Perk.PerkType.EXELLENT_FITNESS) ][TMPPerk.level - 1].AsFloat;
		}
	}

	public void LevelUp(bool level10){
//		if (level >= MaxLevel) {
//			if (maxFitness == DAO.Settings.DefaultMaxFitness) {
//				maxFitness = 65;
//			}
//			return;
//		}

		if (level < MaxLevel && !level10) {
			level++;
		}
//		if (GameManager.Instance.isPlayscapeLoaded) {
//			BI._.Player_GainNewLevel (level);
//			//BI._.Inventory_Upgrade (1, "Player Level", "Train Run", player.playerName);
//		}
		metersPassedToNextLevel = 0;
		levelAdditionalFitness = NextLevelSetup.AdditionalFitness;
		//if (!level10) {
		maxFitness = DAO.Settings.DefaultMaxFitness + levelAdditionalFitness;
		//}
		InitNextLevel ();
		PracticeRunController.Instance.LevelUp ();
		UIUpdater_PracticeRun.Instance.SetProgressBar ();
		//UIUpdater_PracticeRun.Instance.SetPauseScreen ();
		UIUpdater_PracticeRun.Instance.SetLooseScreen (true);

		AudioManager.Instance.OnLevelUp ();

		GameManager.Instance.needToShowRateUsPopup = true;

		UnitedAnalytics.LogEvent ("Player Level Up" , "Bench player number " + player.placeInBench , UserData.Instance.userType.ToString (), maxFitness);

		if (level == 5) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_in_the_gym);
		}else if (level == 10) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_trainnig_hard);
		}else if (level == 15) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_ultimate_teammate);
		}

		//Debug.Log ("alon__________ Level increased! - level = " + level);
	}

	public void InitNextLevel(){
		//Debug.Log ("alon__________ InitNextLevel() - start");

		if (level < MaxLevel) {
			JSONNode lvl = DAO.Settings.PlayerLevelsSettings [(level - 1)];

			NextLevelSetup.AdditionalFitness = lvl ["f"].AsInt;
			NextLevelSetup.MetersToThisLevel = lvl ["d"].AsInt;
			//Debug.Log ("alon__________ InitNextLevel() - end");
		}
		else {
			JSONNode lvl = DAO.Settings.PlayerLevelsSettings [(MaxLevel - 2)];
			NextLevelSetup.AdditionalFitness = lvl ["f"].AsInt;
			//NextLevelSetup.AdditionalFitness = DAO.Settings.PlayerLevelsSettings [(MaxLevel - 1)];
			NextLevelSetup.MetersToThisLevel = 0;
		}
	}

	bool level10Reached = false;
	public void UpdateMetersToNextLevel(int meters){
//		if (level >= 10) {
//			return;
//		}
		metersPassedToNextLevel += meters;

		if (level < MaxLevel) {
			UIUpdater_PracticeRun.Instance.SetProgressBar ();
			if (metersPassedToNextLevel >= NextLevelSetup.MetersToThisLevel) {
				LevelUp (false);
				//Debug.Log ("alon__________ Level sould increased! (under 10)  -  level: " + level);
				//Debug.Log ("alon__________ metersPassedToNextLevel: " + metersPassedToNextLevel + "  NextLevelSetup.MetersToThisLevel: " + NextLevelSetup.MetersToThisLevel);
			}
		} else {
		//	UIUpdater_PracticeRun.Instance.updateDistanceCounter ();
			if (!level10Reached) {
				level10Reached = true;
				UIUpdater_PracticeRun.Instance.SetProgressBarToMax ();
				//LevelUp (true);
				//Debug.Log ("alon__________ Level is 10 !");
			}
		}

		DAO.BIDistance += meters;

		if (DAO.BIDistance >= 1000) {
			DAO.BIDistance = 0;
			DAO.BILevelUp++;
			//if(GameManager.Instance.isPlayscapeLoaded)	 BI._.PlayerLevelUp_1000M (DAO.BILevelUp);
			GameAnalyticsWrapper.ProgressionEvent (GameAnalyticsWrapper.progerssionTypes.Complete, DAO.BILevelUp.ToString (), "Level", null, 0);

			//Debug.Log ("alon________ Playerdata - BI Level Up - BILevelUp = " + DAO.BILevelUp);
		}

	}


	
	[System.Serializable]
	public class PlayerLevel{
		public int MetersToThisLevel;
		public int AdditionalFitness;
	}



	public string mEncode(){
		/*
		 *	Dont forget Update  DEFAULT_PLAYER_DATA when changing this func.
		 */

		ed = fitness.ToString() + "|";
		ed += levelAdditionalFitness.ToString() + "|";
		ed += metersPassedToNextLevel.ToString() + "|";
		ed += curHealthState.ToString() + "|";
		ed += level.ToString () + "|";
		ed += LastFitnessUpdate +"|";
		ed += UnfreezeFitnessTime + "|";

		ed += Perk_1.type + "|";
		ed += Perk_1.level + "|";
		ed += Perk_2.type + "|";
		ed += Perk_2.level + "|";
		ed += Perk_3.type + "|";
		ed += Perk_3.level + "|";
		ed += totalDistance;

		return ed;
	}

	public void mDecode(){
		dd = DAO.GetPlayerData (PlayerID).Split ('|');

		fitness = int.Parse(dd [0]);
		levelAdditionalFitness = int.Parse(dd [1]);
		metersPassedToNextLevel = int.Parse(dd [2]);

		switch (dd [3]) {
		case "ENERGETIC" : { curHealthState = HealthState.ENERGETIC;break; }
		case "IDEAL" : { curHealthState = HealthState.IDEAL;break; }
		case "TIERED" : { curHealthState = HealthState.TIERED;break; }
		case "WOUNDED" : { curHealthState = HealthState.WOUNDED;break; }
		}

		level = int.Parse (dd[4]);

		if (dd [5] == "0")
			LastFitnessUpdate = System.DateTime.Now;
		else
			LastFitnessUpdate = System.DateTime.Parse (dd [5]);


		if (dd [6] != "0" ) {
			UnfreezeFitnessTime = System.DateTime.Parse (dd [6]);
			if(minutesRemainToUnfreezeFitness() > 0){
				fitnessFrozen = true;
			}
		}

		Perk_1 = new Perk( player.DefaultPerk.ToString(), dd[8] );
		Perk_2 = new Perk( dd [9], dd[10] );
		Perk_3 = new Perk( dd [11], dd[12] );

		totalDistance = int.Parse (dd [13]);
	}

	public void AddFitnes(int amount, bool timeHeal = false){
		LastFitnessUpdate = System.DateTime.Now;

		if (fitnessFrozen) {
			if(minutesRemainToUnfreezeFitness() > 0){
				return;
			}else{
				UnfreezeFitness();
			}
		}

		int oldFitnessAmount = fitness;
		if (timeHeal) {
			fitness += (int)(amount * Perk_FitnessBonusFactor);
		} else {
			fitness += amount;
		}

		if (fitness > maxFitness) fitness = maxFitness; 

		if (fitness - oldFitnessAmount >= 1f) {
			//if(GameManager.Instance.isPlayscapeLoaded)	
			//BI._.Inventory_Upgrade ((int)(fitness - oldFitnessAmount), "Player Energy", timeHeal ? "Time Pass" : "Energy Pack", player.playerName);
			GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_ENERGY,(float)(fitness - oldFitnessAmount),GameAnalyticsWrapper.REWARD,"Time");
		}

		UpdateHealthCondition ();

	}

	public int minutesRemainToUnfreezeFitness(){
		return (int)(UnfreezeFitnessTime - System.DateTime.Now).TotalMinutes;
	}

	public string GetTimeRemainToUnfreezeFitnessAsString(){
		return Utils.GetTimeUnitAsFormatedString( (UnfreezeFitnessTime - System.DateTime.Now).Hours )  +":"+ Utils.GetTimeUnitAsFormatedString( (UnfreezeFitnessTime - System.DateTime.Now).Minutes);
	}

	public void FreezeFitness(){
		_fitness = 0;
		fitnessFrozen = true;
		UnfreezeFitnessTime = System.DateTime.Now.AddMinutes(30);
		UpdateHealthCondition ();
		player.Save ();

	}

	public void UnfreezeFitness(){
		fitnessFrozen = false;
		UnfreezeFitnessTime = System.DateTime.MinValue;
		UpdateHealthCondition ();
		player.Save ();
	}


	public void UpdateHealthCondition(){
		fitnessPresent = ((float)fitness / (float)maxFitness) * 100f;
		PerksDisabled = false;

		if (fitnessPresent == 100) {
			curHealthState = HealthState.ENERGETIC;
		} else if (fitnessPresent <= 99 && fitnessPresent >= 30) {
			curHealthState = HealthState.IDEAL;
		} else if (fitnessPresent <= 29 && fitnessPresent >= 2) {
			curHealthState = HealthState.TIERED;
			PerksDisabled = true;
		} else if (fitness <= 1) {
			curHealthState = HealthState.WOUNDED;
			PerksDisabled = true;
		}

	}




}
