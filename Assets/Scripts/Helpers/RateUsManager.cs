﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using System;
//using System.Net;
//using System.Net.Mail;
//using System.Net.Security;
//using System.Security.Cryptography.X509Certificates;

public class RateUsManager : MonoBehaviour {


	public GameObject[] stars;

	//public Animator lowPopapAnim;
	public Animator tutorialAnim;

	void Start () {
		foreach (GameObject star in stars) {
			star.SetActive (false);
		}
	}
	

	public void FillStars (int starIndex) {
		for (int i = 0; i < starIndex; i++) {
			stars [i].SetActive (true);
		}
	}


	public void OnRateUsHighClicked(){
		tutorialAnim.enabled = false;
		DAO.RateUsPopup = 1;
		#if UNITY_ANDROID || UNITY_EDITOR
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.gamehour.arsenalfc");
		#elif UNITY_IOS
        Application.OpenURL("https://itunes.apple.com/us/app/arsenal-fc-endless-football/id1158656031?ls=1&mt=8");
		#endif

		MainScreenUiManager.instance.OnRateUsClose(true);
		//DAO.TotalCoinsCollected += 1000;
		CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (10 , 1000);
		//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (1000, "Rate Us popup", "FCB Coins", "", "", "");
		GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,1000, GameAnalyticsWrapper.REWARD,"RateUs");
		UnitedAnalytics.LogEvent ("User Rated" , "High Rate", UserData.Instance.userType.ToString());

		if(GameManager.Instance.isPlayscapeLoaded)	BI._.ReporRateUs (BI.RateUsActions.YES);
		DAO.RateUsPopup = 1;
	}


	public void OnRateUsLowClicked(){
		tutorialAnim.enabled = false;
		DAO.RateUsPopup = 1;

		MainScreenUiManager.instance.OnRateUsClose(true);
		//DAO.TotalCoinsCollected += 1000;
		CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (10 , 1000);
		if (GameManager.Instance.isPlayscapeLoaded) {
			//BI._.Wallet_Deposit (1000, "Rate Us popup", "FCB Coins", "", "", "");
			BI._.ReporRateUs (BI.RateUsActions.YES);
		}
		GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,1000, GameAnalyticsWrapper.REWARD,"RateUs");
		UnitedAnalytics.LogEvent("User Rated" , "Low Rate" , UserData.Instance.userType.ToString());
		MainScreenUiManager.instance.RateUsThanksToggle (true);
	}


	public void SendEmail ()
	{
		string email = "Games@Gamehour.com";
		string subject = MyEscapeURL("");
		string body = MyEscapeURL("");
		Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}

	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}
}
