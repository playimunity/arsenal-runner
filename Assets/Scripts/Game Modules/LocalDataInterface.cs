using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Amazon;

public class LocalDataInterface : MonoBehaviour {
	public int LocalDataVersion;
	public int dataVersion;
	public int dataRunsVersion;
	public string data;
	public string dataRuns;
	public UserData.UserType userType;
	public int playTimeInMinutes;
	public int playTimeInDays;
	public int totalCoinsCollected;
	public int totalMagnetsCollected;
	public int totalSpeedBoostsCollected;
	public int totalSheildsCollected;
	public int totalX2CoinsCollected;
	public int totalMedikKitsCollected;
	public float totalGiftCardsCollected;
	public float MapScrollY;

	public int starterPackBought;
	public int FCBMegaDealBought;

	public int totalGoldMedals;
	public int totalSilverMedals;
	public int totalFailedRuns;
	public int totalPracticeTime;

	public int rankScore;

	public int iapPoints;
	public int tutorialState;
	public int isMusicEnabled;
	public int isSFXEnabled;
	public int isNotificationsEnabled;

	public string[] playerData;
	public int activeRunId;
	public string activeRunData;
	public string completedRunsData;
	public string winnedCups;
	public string purchasedPlayers;
	public string openedRegions;
	public string perkBonusTime;
	public string installDate;
	public string appLanguage;
	public string countryCode;

	//tutorial popups:
	public int popupHeal;
	public int popupBuySecondPlayer1;
	public int popupBuySecondPlayer2;
	public string recruitSecondPlayerPopup2Date;
	public int popupBuyThirdPlayer;
	public int popupPlayPractice;
	public int popupPlayCup;
	public string playCupPopupDate;
	public int popupOffer;
	public string offerPopupDate;
	public int swipingHand;
	public int cardsHandTut;
	public int cardsPowerUpsTut;
	public int secretAreaBall;
	public int isLegendPurchased;

	public int facebookPopup;
	public int rateUsPopup;
	public string rateUsPopupDate;
	public int rateUsTimeFactor;

	// Stats For Achievemnts and events
	public int totalGiftCardsSent;
	public int totalGiftCardsRecieved;
	public int fbConnected;
	public int woundedHealed;
	public int magnetsUsed;
	public int sheildsUsed;
	public int speedBoostsUsed;
	public int totalBallShooted;
	public int totalFriendsInvited;

    public int isRestoredPurchases;

	public int bestPracticeScore;

	public int appLaunchCount;
	public string timeToClaimGift;
	public int iOSUserTypeForSaveMeByVideo;
	public int numberOfAdsForSaveMeCup;
	public int numberOfAdsForSaveMeEndless;

	public string isDebugMode;

	public int bILevelUp;
	public int bIDistance;
	public DateTime dailyGiftsStart;
	public DateTime dailyGiftsLast;
	public int isFreePlayerClaimed;

	public string email;
	public string gender;
	public string firstName;
	public string lastName;
	public string lang;
	public string country;
	public int canSendEmail;
	public string Ftd;
	public string whaleID;
	public int maxRunID;
    /// <summary>
    /// Dictionary explaination: int=> questIndex, KeyValuePair=> counts medals for each color.
    /// KeyValuePair explaination: first int(KEY)=> GoldCount, second int(VALUE)=> SilverCount.
    /// </summary>
    //public Dictionary<int,KeyValuePair<int,int>> medalsPerQuest;

	// ==================================================================================================
	private static LocalDataInterface instance;
	public static LocalDataInterface Instance{
		get{
			return instance;
		}
	}



	public void BuildDefaultData(){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalDataInterface: BuildDefaultData start ");
		dataVersion = 0;
		PlayerPrefs.SetInt("data_version", dataVersion);

		dataRunsVersion = 0;
		PlayerPrefs.SetInt("dataRuns_version", dataRunsVersion);

		appLaunchCount = 0;
		PlayerPrefs.SetInt("app_launch_count", appLaunchCount);

		data = "[]";
		PlayerPrefs.SetString("data", data);

		PlayerPrefs.SetString("dataRuns", data);



		userType = UserData.UserType.NEW_USER;
		PlayerPrefs.SetInt("user_type", (int)userType);

		playTimeInMinutes = 0;
		PlayerPrefs.SetInt("play_time_in_minutes", playTimeInMinutes);
		playTimeInDays = 0;
		PlayerPrefs.SetInt("play_time_in_days", playTimeInDays);

		totalCoinsCollected = 0;
		//totalCoinsCollected = 2000000;
		PlayerPrefs.SetInt("total_coins_collected", totalCoinsCollected);

		totalMagnetsCollected = 0;
		PlayerPrefs.SetInt("total_magnets_collected", totalMagnetsCollected);

		totalSpeedBoostsCollected = 0;
		PlayerPrefs.SetInt("total_speed_boosts_collected", totalSpeedBoostsCollected);

		totalSheildsCollected = 0;
		PlayerPrefs.SetInt("total_sheilds_collected", totalSheildsCollected);

		totalX2CoinsCollected = 0;
		PlayerPrefs.SetInt("total_x2_collected", totalX2CoinsCollected);

		totalGiftCardsCollected = 3;
		PlayerPrefs.SetFloat("total_gift_cards_collected", totalGiftCardsCollected);

		totalMedikKitsCollected = 0;
		PlayerPrefs.SetInt("total_medic_kits_collected", totalMedikKitsCollected);

		starterPackBought = 0;
		PlayerPrefs.SetInt("starter_pack_bought", starterPackBought);

		FCBMegaDealBought = 0;
		PlayerPrefs.SetInt("fcb_mega_deal_bought", FCBMegaDealBought);

		isMusicEnabled = 1;
		PlayerPrefs.SetInt("music_enabled", isMusicEnabled);
		isSFXEnabled = 1;
		PlayerPrefs.SetInt("sfx_enabled", isSFXEnabled);
		isNotificationsEnabled = 1;
		PlayerPrefs.SetInt("notifications_enabled", isNotificationsEnabled);



//		installDate = DateTime.Now.ToString ("mm/dd/yyyy");
		installDate = DateTime.Now.ToString();
		PlayerPrefs.SetString("install_date", installDate);




		iapPoints = 0;
		PlayerPrefs.SetInt("iap_points", iapPoints);

        for (int i=0; i<20; i++) {
			playerData[i] = PlayerData.DEFAULT_PLAYER_DATA;
			PlayerPrefs.SetString("player_data_"+i, playerData[i]);
		}

		activeRunId = -1;
		PlayerPrefs.SetInt ("ar_id", activeRunId);


		activeRunData = "";
		PlayerPrefs.SetString ("ar_data", activeRunData);

		completedRunsData = "*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0";
		PlayerPrefs.SetString ("cr_data", completedRunsData);

		winnedCups = "";
		PlayerPrefs.SetString ("winned_cups", winnedCups);

		purchasedPlayers = ""; //"0|1|5|7|8|11|";

		PlayerPrefs.SetString ("purchased_players", purchasedPlayers);

		openedRegions = "";
		PlayerPrefs.SetString ("opened_regions", openedRegions);

		perkBonusTime = "0";
		PlayerPrefs.SetString ("perk_bonus_time", perkBonusTime);

		appLanguage = Application.systemLanguage.ToString();
		PlayerPrefs.SetString ("app_lang", appLanguage);

		tutorialState = 1;
		PlayerPrefs.SetInt ("tutorial_state", tutorialState);

		countryCode = "WW";
		PlayerPrefs.SetString ("country-code", countryCode);

		//tutorial popups:
		popupHeal = 0;
		PlayerPrefs.SetInt ("popup_heal", popupHeal);

		popupBuySecondPlayer1 = 0;
		PlayerPrefs.SetInt ("popup_buy_second_player_1", popupBuySecondPlayer1);

		popupBuySecondPlayer2 = 0;
		PlayerPrefs.SetInt ("popup_buy_second_player_2", popupBuySecondPlayer2);

		popupBuyThirdPlayer = 0;
		PlayerPrefs.SetInt ("popup_buy_third_player", popupBuyThirdPlayer);

		recruitSecondPlayerPopup2Date = DateTime.Now.ToString ();
		PlayerPrefs.SetString("recruit_second_player_popup2_date", recruitSecondPlayerPopup2Date);

		popupPlayPractice = 0;
		PlayerPrefs.SetInt ("popup_play_practice", popupPlayPractice);

		popupPlayCup = 0;
		PlayerPrefs.SetInt ("popup_play_cup", popupPlayCup);

		playCupPopupDate = DateTime.Now.ToString ();
		PlayerPrefs.SetString("play_cup_popup_date", playCupPopupDate);

		popupOffer = 0;
		PlayerPrefs.SetInt ("popup_offer", popupOffer);

		offerPopupDate = DateTime.Now.ToString ();
		PlayerPrefs.SetString("offer_popup_date", offerPopupDate);

		swipingHand = 0;
		PlayerPrefs.SetInt ("swiping_hand", swipingHand);

		cardsHandTut = 0;
		PlayerPrefs.SetInt ("cards_hand_tut", cardsHandTut);

		cardsPowerUpsTut = 0;
		PlayerPrefs.SetInt ("cards_powerups_tut", cardsPowerUpsTut);

		secretAreaBall = 0;
		PlayerPrefs.SetInt ("secret_area_ball", secretAreaBall);



		facebookPopup = 0;
		PlayerPrefs.SetInt ("facebook_popup", facebookPopup);

		rateUsPopup = 0;
		PlayerPrefs.SetInt ("rate_us_popup", rateUsPopup);

		rateUsPopupDate = DateTime.Now.ToString ();
		PlayerPrefs.SetString("rate_us_popup_date", rateUsPopupDate);

		rateUsTimeFactor = 10;
		PlayerPrefs.SetInt ("rate_us_time", rateUsTimeFactor);

		isLegendPurchased = 0;
		PlayerPrefs.SetInt ("is_legend_purchased", isLegendPurchased);

		totalGiftCardsSent = 0;
		PlayerPrefs.SetInt("total_gifts_sent", totalGiftCardsSent);

		totalGiftCardsRecieved = 0;
		PlayerPrefs.SetInt("total_gifts_recieved", totalGiftCardsRecieved);

		fbConnected = 0;
		PlayerPrefs.SetInt("fb_connect", fbConnected);

		woundedHealed = 0;
		PlayerPrefs.SetInt("wounded_healed", woundedHealed);

		magnetsUsed = 0;
		PlayerPrefs.SetInt("magnets_used", magnetsUsed);

		sheildsUsed = 0;
		PlayerPrefs.SetInt("sheilds_used", sheildsUsed);

		speedBoostsUsed = 0;
		PlayerPrefs.SetInt("speed_boosts_used", speedBoostsUsed);

		totalBallShooted = 0;
		PlayerPrefs.SetInt("total_ball_shooted", totalBallShooted);


		totalFriendsInvited = 0;
		PlayerPrefs.SetInt("total_friends_invited", totalFriendsInvited);

		MapScrollY = -10f;
		PlayerPrefs.SetFloat ("map_scroll_y", MapScrollY);

		bestPracticeScore = 0;
		PlayerPrefs.SetInt("best_practice_score", bestPracticeScore);

		isDebugMode = "NO";
		PlayerPrefs.SetString("is_debug_mode", isDebugMode);

		totalGoldMedals = 0;
		PlayerPrefs.SetInt("total_gold_medals", totalGoldMedals);
		totalSilverMedals = 0;
		PlayerPrefs.SetInt("total_silver_medals", totalSilverMedals);
		totalFailedRuns = 0;
		PlayerPrefs.SetInt("total_failed_runs", totalFailedRuns);
		totalPracticeTime = 0;
		PlayerPrefs.SetInt("total_practice_time", totalPracticeTime);

		rankScore = 0;
		PlayerPrefs.SetInt("rank_score", rankScore);

        isRestoredPurchases = 0;
        PlayerPrefs.SetInt("is_restored", isRestoredPurchases);
        timeToClaimGift = "04:00:00";
		//timeToClaimGift = "00:00:01";
		PlayerPrefs.SetString ("time_to_claim_free_gift", timeToClaimGift);
		iOSUserTypeForSaveMeByVideo = 4;
		PlayerPrefs.SetInt ("ios_user_type_for_saveme_by_video", iOSUserTypeForSaveMeByVideo);
		numberOfAdsForSaveMeCup = 1;
		PlayerPrefs.SetInt ("number_of_ads_for_saveme_cup", numberOfAdsForSaveMeCup);
		numberOfAdsForSaveMeEndless = 1;
		PlayerPrefs.SetInt ("number_of_ads_for_saveme_endless", numberOfAdsForSaveMeEndless);
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalDataInterface: BuildDefaultData end");

//        medalsPerQuest = new Dictionary<int, KeyValuePair<string, int>>();
       // SetMedalsPerQuestToPlayerPrefs();

		bILevelUp = 0;
		PlayerPrefs.SetInt("bi_level_up", bILevelUp);

		bIDistance = 0;
		PlayerPrefs.SetInt("bi_distance", bIDistance);
		dailyGiftsStart = new DateTime();
		PlayerPrefs.SetString("daily_gifts_start", dailyGiftsStart.ToString());
		dailyGiftsLast = new DateTime();
		PlayerPrefs.SetString("daily_gifts_last", dailyGiftsLast.ToString());
		isFreePlayerClaimed=0;
		PlayerPrefs.SetInt("is_free_player_claimed", isFreePlayerClaimed);


		email="";
		PlayerPrefs.SetString("user_email", email);
	    gender="";
		PlayerPrefs.SetString("user_gender", gender);
		firstName="";
		PlayerPrefs.SetString("user_first_name", firstName);
		lastName="";
		PlayerPrefs.SetString("user_last_name", lastName);
		lang="";
		PlayerPrefs.SetString("user_lang", lang);
		country="";
		PlayerPrefs.SetString("user_country", country);
		canSendEmail=0;
		PlayerPrefs.SetInt("user_canSendEmail", canSendEmail);
		Ftd = "";
		PlayerPrefs.SetString("Ftd", Ftd);
		whaleID = "";
		PlayerPrefs.SetString("whale_id", whaleID);
		maxRunID = 0;
		PlayerPrefs.SetInt("max_run_id", maxRunID);
	}


//    private void SetMedalsPerQuestToPlayerPrefs()
//    {
//        string str = string.Empty;
//
//        foreach (var item in medalsPerQuest)
//        {
//            str+=item.Key+"|"+item.Value.Key+"|"+item.Value.Value;
//            //* seperates between items
//            str+="*";
//        }
//
//        PlayerPrefs.SetString("medals_per_quest", str);
//    }
//    public void GetMedalsPerQuestFromPLayerPrefs()
//    {
//        string str = PlayerPrefs.GetString("medals_per_quest");
//        foreach (var item in str.Split('*'))
//        {
//            var quest = item.Split('|');
//            if (!medalsPerQuest.ContainsKey(item[0]))
//            {
//                medalsPerQuest.Add(int.Parse(item[0]),new KeyValuePair<int, int>({item[0],item[1]});
//            }
//        }
//    }

	private void ReadData(){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalDataInterface: ReadData start ");
		if (!PlayerPrefs.HasKey ("storage_not_empty")) {
			DAO.Log("Local data is empty, creating default data...");
			PlayerPrefs.SetInt("local_data_version", LocalDataVersion);
			BuildDefaultData();
			return;
		}

		if (!PlayerPrefs.HasKey ("local_data_version") || PlayerPrefs.GetInt("local_data_version") != LocalDataVersion) {
			DAO.Log("Local structure is changed, creating default data...");
			PlayerPrefs.SetInt("local_data_version", LocalDataVersion);
			BuildDefaultData();
			return;
		}

		// ------------------

		dataVersion = PlayerPrefs.GetInt("data_version");
		dataRunsVersion = PlayerPrefs.GetInt("dataRuns_version");
		data = PlayerPrefs.GetString ("data");
		dataRuns = PlayerPrefs.GetString ("dataRuns");
		userType = (UserData.UserType)PlayerPrefs.GetInt ("user_type");
		playTimeInMinutes = PlayerPrefs.GetInt ("play_time_in_minutes");
		playTimeInDays = PlayerPrefs.GetInt ("play_time_in_days");
		totalCoinsCollected = PlayerPrefs.GetInt ("total_coins_collected");
		totalMagnetsCollected = PlayerPrefs.GetInt ("total_magnets_collected");
		totalSpeedBoostsCollected = PlayerPrefs.GetInt ("total_speed_boosts_collected");
		totalSheildsCollected = PlayerPrefs.GetInt ("total_sheilds_collected");
		totalX2CoinsCollected = PlayerPrefs.GetInt ("total_x2_collected");
		totalGiftCardsCollected = PlayerPrefs.GetFloat ("total_gift_cards_collected");
		totalMedikKitsCollected = PlayerPrefs.GetInt("total_medic_kits_collected");
		installDate = PlayerPrefs.GetString("install_date");
		iapPoints = PlayerPrefs.GetInt("iap_points");

		isMusicEnabled = PlayerPrefs.GetInt("music_enabled");
		isSFXEnabled = PlayerPrefs.GetInt("sfx_enabled");
		isNotificationsEnabled = PlayerPrefs.GetInt("notifications_enabled");
        //GetMedalsPerQuestFromPLayerPrefs();

        for (int i=0; i<20; i++) {
			playerData[i] = PlayerPrefs.GetString("player_data_"+i);
		}

		isLegendPurchased = PlayerPrefs.GetInt ("is_legend_purchased");

		activeRunId = PlayerPrefs.GetInt ("ar_id");
		activeRunData = PlayerPrefs.GetString ("ar_data");
		completedRunsData = PlayerPrefs.GetString ("cr_data");

		winnedCups = PlayerPrefs.GetString ("winned_cups");
		purchasedPlayers = PlayerPrefs.GetString ("purchased_players");
		openedRegions = PlayerPrefs.GetString ("opened_regions");
		perkBonusTime = PlayerPrefs.GetString ("perk_bonus_time");

		appLanguage = PlayerPrefs.GetString ("app_lang");

		countryCode = PlayerPrefs.GetString ("country-code");

		tutorialState = PlayerPrefs.GetInt ("tutorial_state");

		//tutorial popups:
		popupHeal = PlayerPrefs.GetInt ("popup_heal");
		popupBuySecondPlayer1 = PlayerPrefs.GetInt ("popup_buy_second_player_1");
		popupBuySecondPlayer2 = PlayerPrefs.GetInt ("popup_buy_second_player_2");
		popupBuyThirdPlayer = PlayerPrefs.GetInt ("popup_buy_third_player");
		recruitSecondPlayerPopup2Date = PlayerPrefs.GetString("recruit_second_player_popup2_date");
		popupPlayPractice = PlayerPrefs.GetInt ("popup_play_practice");
		popupPlayCup = PlayerPrefs.GetInt ("popup_play_cup");
		playCupPopupDate = PlayerPrefs.GetString("play_cup_popup_date");
		popupOffer = PlayerPrefs.GetInt ("popup_offer");
		offerPopupDate = PlayerPrefs.GetString("offer_popup_date");
		swipingHand = PlayerPrefs.GetInt ("swiping_hand");
		cardsHandTut = PlayerPrefs.GetInt ("cards_hand_tut");
		cardsPowerUpsTut = PlayerPrefs.GetInt ("cards_powerups_tut");
		secretAreaBall = PlayerPrefs.GetInt ("secret_area_ball");

		MapScrollY = PlayerPrefs.GetFloat ("map_scroll_y");

		starterPackBought = PlayerPrefs.GetInt("starter_pack_bought");
		FCBMegaDealBought = PlayerPrefs.GetInt("fcb_mega_deal_bought");

		facebookPopup = PlayerPrefs.GetInt ("facebook_popup");
		rateUsPopup = PlayerPrefs.GetInt ("rate_us_popup");
		rateUsPopupDate = PlayerPrefs.GetString("rate_us_popup_date");
		rateUsTimeFactor = PlayerPrefs.GetInt ("rate_us_time");



		totalGiftCardsSent = PlayerPrefs.GetInt ("total_gifts_sent");
		totalGiftCardsRecieved = PlayerPrefs.GetInt("total_gifts_recieved");
		fbConnected = PlayerPrefs.GetInt("fb_connect");
		woundedHealed = PlayerPrefs.GetInt("wounded_healed");
		magnetsUsed = PlayerPrefs.GetInt("magnets_used");
		sheildsUsed = PlayerPrefs.GetInt("sheilds_used");
		speedBoostsUsed = PlayerPrefs.GetInt("speed_boosts_used");
		totalBallShooted = PlayerPrefs.GetInt("total_ball_shooted");
		totalFriendsInvited = PlayerPrefs.GetInt("total_friends_invited");

		bestPracticeScore = PlayerPrefs.GetInt("best_practice_score");

		appLaunchCount = PlayerPrefs.GetInt("app_launch_count");

		isDebugMode = PlayerPrefs.GetString("is_debug_mode");

		totalGoldMedals = PlayerPrefs.GetInt("total_gold_medals");
		totalSilverMedals = PlayerPrefs.GetInt("total_silver_medals");
		totalFailedRuns = PlayerPrefs.GetInt("total_failed_runs");
		totalPracticeTime = PlayerPrefs.GetInt("total_practice_time");

		rankScore = PlayerPrefs.GetInt("rank_score");

        isRestoredPurchases = PlayerPrefs.GetInt("is_restored");
		timeToClaimGift = PlayerPrefs.GetString ("time_to_claim_free_gift");
		iOSUserTypeForSaveMeByVideo = PlayerPrefs.GetInt ("ios_user_type_for_saveme_by_video");
		numberOfAdsForSaveMeCup = PlayerPrefs.GetInt ("number_of_ads_for_saveme_cup");
		numberOfAdsForSaveMeEndless = PlayerPrefs.GetInt ("number_of_ads_for_saveme_endless");
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalDataInterface: ReadData end ");

		bILevelUp = PlayerPrefs.GetInt("bi_level_up");
		bIDistance = PlayerPrefs.GetInt("bi_distance");


		if (!DateTime.TryParse(PlayerPrefs.GetString("daily_gifts_start"),out dailyGiftsStart))
		{
			dailyGiftsStart = new DateTime();
		}
		if(!DateTime.TryParse(PlayerPrefs.GetString("daily_gifts_last"),out dailyGiftsLast))
		{
			dailyGiftsLast = new DateTime();
		}
		isFreePlayerClaimed = PlayerPrefs.GetInt("is_free_player_claimed");

		email = PlayerPrefs.GetString ("user_email");
		gender = PlayerPrefs.GetString ("user_gender");
		firstName = PlayerPrefs.GetString ("user_first_name");
		lastName = PlayerPrefs.GetString ("user_last_name");
		lang= PlayerPrefs.GetString ("user_lang");
		country= PlayerPrefs.GetString ("user_country");
		canSendEmail = PlayerPrefs.GetInt("user_canSendEmail");
		Ftd = PlayerPrefs.GetString("Ftd");
		whaleID = PlayerPrefs.GetString ("whale_id");
		maxRunID = PlayerPrefs.GetInt("max_run_id");
}


	public void SaveAll(bool saveDDB = true){
		//try{
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalDataInterface: SaveAll start ");
		PlayerPrefs.SetInt ("data_version", dataVersion);
		PlayerPrefs.SetInt ("dataRuns_version", dataRunsVersion);
		//PlayerPrefs.SetString ("data", data);
		PlayerPrefs.SetInt ("user_type", (int)userType);
		PlayerPrefs.SetInt("play_time_in_minutes", playTimeInMinutes);
		PlayerPrefs.SetInt("play_time_in_days", playTimeInMinutes);
		PlayerPrefs.SetInt("total_coins_collected", totalCoinsCollected);
		PlayerPrefs.SetInt("total_magnets_collected", totalMagnetsCollected);
		PlayerPrefs.SetInt("total_speed_boosts_collected", totalSpeedBoostsCollected);
		PlayerPrefs.SetInt("total_sheilds_collected", totalSheildsCollected);
		PlayerPrefs.SetInt("total_x2_collected", totalX2CoinsCollected);
		PlayerPrefs.SetFloat("total_gift_cards_collected", totalGiftCardsCollected);
		PlayerPrefs.SetInt("total_medic_kits_collected", totalMedikKitsCollected);
		PlayerPrefs.SetInt ("tutorial_state", tutorialState);
		PlayerPrefs.SetInt("music_enabled", isMusicEnabled);
		PlayerPrefs.SetInt("sfx_enabled", isSFXEnabled);
		PlayerPrefs.SetInt("notifications_enabled", isNotificationsEnabled);
		PlayerPrefs.SetString("install_date", installDate);

		for (int i=0; i<20; i++) {
			PlayerPrefs.SetString("player_data_"+i, playerData[i]);
		}

		//tutorial popups:
		PlayerPrefs.SetInt ("popup_heal", popupHeal);
		PlayerPrefs.SetInt ("popup_buy_second_player_1", popupBuySecondPlayer1);
		PlayerPrefs.SetInt ("popup_buy_second_player_2", popupBuySecondPlayer2);
		PlayerPrefs.SetInt ("popup_buy_third_player", popupBuyThirdPlayer);
		PlayerPrefs.SetString("recruit_second_player_popup2_date", recruitSecondPlayerPopup2Date);
		PlayerPrefs.SetInt ("popup_play_practice", popupPlayPractice);
		PlayerPrefs.SetInt ("popup_play_cup", popupPlayCup);
		PlayerPrefs.SetString("play_cup_popup_date", playCupPopupDate);
		PlayerPrefs.SetInt ("popup_offer", popupOffer);
		PlayerPrefs.SetString("offer_popup_date", offerPopupDate);
		PlayerPrefs.SetInt ("swiping_hand", swipingHand);
		PlayerPrefs.SetInt ("cards_hand_tut", cardsHandTut);
		PlayerPrefs.SetInt ("cards_powerups_tut" , cardsPowerUpsTut);
		PlayerPrefs.SetFloat ("map_scroll_y", MapScrollY);
		PlayerPrefs.SetInt ("secret_area_ball", secretAreaBall);
		PlayerPrefs.SetInt ("is_legend_purchased", isLegendPurchased);
		PlayerPrefs.SetInt("starter_pack_bought", starterPackBought);
		PlayerPrefs.SetInt("fcb_mega_deal_bought", FCBMegaDealBought);

		PlayerPrefs.SetInt ("facebook_popup", facebookPopup);
		PlayerPrefs.SetInt ("rate_us_popup", rateUsPopup);
		PlayerPrefs.SetString("rate_us_popup_date", rateUsPopupDate);
		PlayerPrefs.SetInt ("rate_us_time", rateUsTimeFactor);

		PlayerPrefs.SetInt ("ar_id", activeRunId);
		PlayerPrefs.SetString ("ar_data", activeRunData);
		PlayerPrefs.SetString ("cr_data", completedRunsData);
		PlayerPrefs.SetString ("winned_cups", winnedCups);
		PlayerPrefs.SetString ("purchased_players", purchasedPlayers);
		PlayerPrefs.SetString ("opened_regions", openedRegions);
		PlayerPrefs.SetString ("perk_bonus_time", perkBonusTime);
		PlayerPrefs.SetString ("app_lang", appLanguage);
		PlayerPrefs.SetString ("country-code", countryCode);

		PlayerPrefs.SetInt ("total_gifts_sent", totalGiftCardsSent);
		PlayerPrefs.SetInt("total_gifts_recieved", totalGiftCardsSent);
		PlayerPrefs.SetInt("fb_connect", fbConnected);
		PlayerPrefs.SetInt("wounded_healed", woundedHealed);
		PlayerPrefs.SetInt("magnets_used", magnetsUsed);
		PlayerPrefs.SetInt("sheilds_used", sheildsUsed);
		PlayerPrefs.SetInt("speed_boosts_used", speedBoostsUsed);
		PlayerPrefs.SetInt("total_ball_shooted", totalBallShooted);
		PlayerPrefs.SetInt("total_friends_invited", totalFriendsInvited);

		PlayerPrefs.SetInt("best_practice_score", bestPracticeScore);
		PlayerPrefs.SetInt("app_launch_count", appLaunchCount);
		PlayerPrefs.SetString("is_debug_mode", isDebugMode);
		PlayerPrefs.SetInt("total_gold_medals", totalGoldMedals);
		PlayerPrefs.SetInt("total_silver_medals", totalSilverMedals);
		PlayerPrefs.SetInt("total_failed_runs", totalFailedRuns);
		PlayerPrefs.SetInt("total_practice_time", totalPracticeTime);
		PlayerPrefs.SetInt("rank_score", rankScore);
        PlayerPrefs.SetInt("is_restored", isRestoredPurchases);
		PlayerPrefs.SetString ("time_to_claim_free_gift", timeToClaimGift);
		PlayerPrefs.SetInt ("ios_user_type_for_saveme_by_video", iOSUserTypeForSaveMeByVideo);
		PlayerPrefs.SetInt ("number_of_ads_for_saveme_cup", numberOfAdsForSaveMeCup);
		PlayerPrefs.SetInt ("number_of_ads_for_saveme_endless", numberOfAdsForSaveMeEndless);

		PlayerPrefs.SetInt ("storage_not_empty", 1);

		PlayerPrefs.SetInt("bi_level_up", bILevelUp);
		PlayerPrefs.SetInt("bi_distance", bIDistance);
		PlayerPrefs.SetString("daily_gifts_start", dailyGiftsStart.ToString());
		PlayerPrefs.SetString("daily_gifts_last", dailyGiftsLast.ToString());
		PlayerPrefs.SetInt("is_free_player_claimed", isFreePlayerClaimed);

		PlayerPrefs.SetString("user_email", email);
		PlayerPrefs.SetString("user_gender", gender);
		PlayerPrefs.SetString("user_first_name", firstName);
		PlayerPrefs.SetString("user_last_name", lastName);
		PlayerPrefs.SetString("user_lang", lang);
		PlayerPrefs.SetString("user_country", country);
		PlayerPrefs.SetInt("user_canSendEmail", canSendEmail);
		PlayerPrefs.SetString("Ftd", Ftd);
		PlayerPrefs.SetString("whale_id", whaleID);
		PlayerPrefs.SetInt("iap_points", iapPoints);
		PlayerPrefs.SetInt("max_run_id", maxRunID);
		PlayerPrefs.Save();
			
		DAO.Log ("Local data saved.");

		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("LocalDataInterface: SaveAll end ");
//        Debug.Log("SAVING TO DDB%^&*^&#%^%$^$^@5@5@#$#$");
//        DDB._.SaveAll ();		
		try {
			if ((saveDDB) && (DDB._ != null) && (DAO.Instance != null) && (DAO.Instance.InitFacebookWithDDB)) DDB._.SaveAll ();
		} 
		catch (Exception ex) {
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("LocalDataInterface: saveDDB failed" + ex.Message);
			Debug.Log ("LocaldataInterface DDB.SaveAll" +ex.Message);
		}


	}
	

	// ==================================================================================================


	public void Init () {
		instance = this;
		playerData = new string[20];

		ReadData ();

	}


	public void TempResetTutPopups(){
		popupHeal = 0;
		PlayerPrefs.SetInt ("popup_heal", popupHeal);

		popupBuySecondPlayer1 = 0;
		PlayerPrefs.SetInt ("popup_buy_second_player_1", popupBuySecondPlayer1);

		popupBuySecondPlayer2 = 0;
		PlayerPrefs.SetInt ("popup_buy_second_player_2", popupBuySecondPlayer2);

		recruitSecondPlayerPopup2Date = DateTime.Now.ToString ();
		PlayerPrefs.SetString("recruit_second_player_popup2_date", recruitSecondPlayerPopup2Date);

		popupBuyThirdPlayer = 0;
		PlayerPrefs.SetInt ("popup_buy_third_player", popupBuyThirdPlayer);

		popupPlayPractice = 0;
		PlayerPrefs.SetInt ("popup_play_practice", popupPlayPractice);

		popupPlayCup = 0;
		PlayerPrefs.SetInt ("popup_play_cup", popupPlayCup);

		playCupPopupDate = DateTime.Now.ToString ();
		PlayerPrefs.SetString("play_cup_popup_date", playCupPopupDate);

		popupOffer = 0;
		PlayerPrefs.SetInt ("popup_offer", popupOffer);

		offerPopupDate = DateTime.Now.ToString ();
		PlayerPrefs.SetString("offer_popup_date", offerPopupDate);

		swipingHand = 0;
		PlayerPrefs.SetInt ("swiping_hand", swipingHand);

	}


	public void ResetGiftCards(){
		totalGiftCardsCollected = 3;
		PlayerPrefs.SetFloat("total_gift_cards_collected", totalGiftCardsCollected);

		cardsHandTut = 0;
		PlayerPrefs.SetInt ("cards_hand_tut", cardsHandTut);

		cardsPowerUpsTut = 0;
		PlayerPrefs.SetInt ("cards_powerups_tut", cardsPowerUpsTut);
	}
}