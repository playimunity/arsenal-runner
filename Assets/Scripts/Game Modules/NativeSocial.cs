﻿using UnityEngine;
using System.Collections;
#if !UNITY_IOS
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using UnityEngine.SocialPlatforms;
using System;
using Amazon;

// Signing certificate fingerprint 
// CF:AF:13:86:D6:B7:80:6B:E8:59:2A:01:2E:70:BF:0B:B4:22:75:57

public class NativeSocial : MonoBehaviour {

	public static NativeSocial _;

	public static bool IsLoggedIn = false;

	void Awake(){
		_ = this;
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("NativeSocial awake start");

	#if UNITY_ANDROID
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
		PlayGamesPlatform.InitializeInstance(config);

		//PlayGamesPlatform.DebugLogEnabled = true;

		PlayGamesPlatform.Activate();

		Loggin ();
	#elif UNITY_IOS
		Loggin ();
	#endif
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("NativeSocial awake end");

	}

	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log ("Native Social | " + msg);
	}


	public void Loggin(){

		if (!Social.localUser.authenticated) {

			Social.localUser.Authenticate ((bool success) => {
				if (success) {
					Log("Successfully Loged in");
                    
					//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.GPG_SUCCESS );
					IsLoggedIn = true;
				}else{
					Log("Login Failed!");
					//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.GPG_FAILED );
				}
			});
		}

	}

	public void ReportAchievment( string achievment ){

        #if !UNITY_EDITOR
		if (Social.localUser.authenticated) {

			Social.ReportProgress (achievment, 100.0f, (bool success) => {
				if (success) {
					// TODO: Success achievment
				}
			});

		}
        #endif

	}

	public void PostScore(int score){

		if(Social.localUser.authenticated){
			Social.ReportScore(score, SocialConstants.leaderboard_barca_high_score, (bool success) => {
				if(success){
					// TODO: Score sent
				}
			});
		}

	}


	public void ShowAchievmentsUI(){
		if(Social.localUser.authenticated){
			Social.ShowAchievementsUI();
		}

	}

	public void ShowLeaderboardUI(){

	if(!Social.localUser.authenticated) Loggin ();

	#if UNITY_ANDROID	

		PlayGamesPlatform.Instance.ShowLeaderboardUI(SocialConstants.leaderboard_barca_high_score);

	#elif UNITY_IOS

		Social.ShowLeaderboardUI();

	#endif

	}


//	public void GetUserDataCollection(){
//		GooglePlayGames.BasicApi. GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//		GoogleSignInAccount acct = result.getSignInAccount();
//		String personName = acct.getDisplayName();
//		String personEmail = acct.getEmail();
//		String personId = acct.getId();
//		Uri personPhoto = acct.getPhotoUrl();
//	}







}
