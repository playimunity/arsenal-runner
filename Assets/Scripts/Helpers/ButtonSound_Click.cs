﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSound_Click : MonoBehaviour {

	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => {
			OnClicked();
		});
	}


	public void OnClicked(){
		AudioManager.Instance.OnBTNClick ();
	}
}
