﻿using UnityEngine;
using System.Collections;

public class UpdateFixer : MonoBehaviour
{

	public string lastAppVersionPPKey = "last_app_version";
	// Use this for initialization
	void Start ()
	{
		if (PlayerPrefs.HasKey (lastAppVersionPPKey)) {
			if (PlayerPrefs.GetString (lastAppVersionPPKey) != Application.version) {
				DoFixes ();
			}
		} else {
			PlayerPrefs.SetString (lastAppVersionPPKey, Application.version);
			DoFixes ();
		}
	}

	public void DoFixes ()
	{
		print ("*************************doing fixes*********************");
		//Fixes For 1.06
		FixFor1_06 ();
	}

	public void FixFor1_06()
	{
		if (PlayerPrefs.GetInt ("user_type") == 4) {
			DAO.UserType = UserData.UserType.PAYING_USER;
		}
		if (DAO.CompletedRunsData == "")
		{
			DAO.CompletedRunsData = "*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0";
		}
	}
}
