﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class InfinityMapBuilder : MonoBehaviour {

	public enum Regions { GENERIC, METRO, GOTH, GUELL, RAMB }
	int NUM_OF_SECTIONS_ON_SCENE = 6;
	public float SECTION_REMOVE_DELAY_IN_SECONDS = 1f;
	public float SECTION_WIDTH = 30f;
	public float SECTION_HEIGHT_LEVEL = 5f;

	public static InfinityMapBuilder instance;
	public List<PracticeRun> AvailbleRuns;
	public PracticeRun currentRun;
	PracticeRun RunForDeleter;

	static bool FreezeBuilder = false;
	static WorldSection.RegionType curRegion = WorldSection.RegionType.HR;
	public int CurrentNumOfSectionsOnScene = 0;

	int currentRunIndex;

	WaitForSeconds removeDelay;
	int currentRunHeight = 0;
	int nextSection = 0;
	int previosSectionToDelete = -1;
	Vector3 nextSectionPosition = Vector3.zero;
	Vector3 nextSectionPositionOffset;
	Vector3 nextSectionRotation = Vector3.zero;
	Vector3 vectorZona;
	WorldSectionAbstract section;
	bool instantiateDoubleSection = false;

	static bool needToTurnLeft = false;
	static bool needToTurnRight = false;
	static bool needNewSection = false;
	static bool needToRemoveSection = false;
	static bool waitingToRemoveSection = false;
	//bool isFirstSection = true;
	int sectionIndexToDelete = 0;

	// -------------------------------------------------

	void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log ("Practice Map Builder | " + msg);
	}

	void Start(){
		removeDelay = new WaitForSeconds (SECTION_REMOVE_DELAY_IN_SECONDS);
		instance = this;
		FreezeBuilder = false;

		needToTurnLeft = false;
		needToTurnRight = false;
		needNewSection = false;
		needToRemoveSection = false;

		//Utils.SetFogColor (curRegion);
		PrepareAvailableRuns ();
		BuildFirstSections ();

		//MainScreenUiManager.instance.FreeGiftBtnAnimToggle (false);
	}

	void Update(){

		
		if(needToTurnLeft){
			nextSectionPosition -= nextSectionPositionOffset;
			TurnBuilderLeft();
			nextSectionPosition += nextSectionPositionOffset;
			nextSectionPosition += nextSectionPositionOffset;
			
			needToTurnLeft = false;
//			needNewSection = true;
			needToRemoveSection = true;
		}
		
		if(needToTurnRight){
			nextSectionPosition -= nextSectionPositionOffset;
			TurnBuilderRight();
			nextSectionPosition += nextSectionPositionOffset;
			nextSectionPosition += nextSectionPositionOffset;
			
			needToTurnRight = false;
//			needNewSection = true;
			needToRemoveSection = true;
		}
		
//		if (needToRemoveSection) {
//			if (PlayerController.instance.isDead) return;
//			needToRemoveSection = false;
//			StartCoroutine( RemoveUsedSections() );
//		}
//
		if (CurrentNumOfSectionsOnScene < NUM_OF_SECTIONS_ON_SCENE && !FreezeBuilder) {
			AddNewSection ();
		}
		if (needToRemoveSection) {
			needToRemoveSection = false;
			if (PlayerController.instance.isDead) {
				//print ("____________****NeedToRemoveSection and Previous Section****");
				previosSectionToDelete = sectionIndexToDelete;
			} else {
				//print ("_________*******NEED to remove section - Player not dead");
				waitingToRemoveSection = true;
				StartCoroutine ("RemoveUsedSections");
			}
		}
		
	}

	// =======================================================================

	bool IsRegionOpened(Regions region){
		int index = 0;
		if (region == Regions.METRO) index = 2;
		if (region == Regions.GOTH) index = 3;
		if (region == Regions.GUELL) index = 4;
		if (region == Regions.RAMB) index = 5;

		foreach (int r in GameManager.OpenedRegions) {
			if(r == index) return true;
		}

		return false;
	}

	void PrepareAvailableRuns(){
		// REGIONS INDEX:
		// (0) GENERIC
		// (1) Metro
		// (2) Goth
		// (3) Guell
		// (4) Ramb
 
		bool DAO_metro_section_achieved = true;
		bool DAO_goth_section_achieved = false;
		bool DAO_guell_section_achieved = false;
		bool DAO_ramb_section_achieved = false;

		// ----


		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(0) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(1) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(2) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(3) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(4) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(5) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(6) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(7) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(8) ) );
		AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(9) ) );

		Log ("Preparing Runs according to opened regions...");

//		if( IsRegionOpened(Regions.METRO) ){
//			Log ("Adding 'Metro' Runs...");
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(14) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(15) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(16) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(17) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(18) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(19) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(20) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(21) ) );
//		}
//
//		if( IsRegionOpened(Regions.GOTH) ){
//			Log ("Adding 'Goth' Runs...");
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(22) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(23) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(24) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(25) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(26) ) );
//			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(27) ) );
//		}

		if( IsRegionOpened(Regions.GUELL) ){
			Log ("Adding 'Guell' Runs...");
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(28) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(29) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(30) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(31) ) );

			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(10) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(11) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(14) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(15) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(16) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(17) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(18) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(19) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(20) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(21) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(22) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(23) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(24) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(25) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(26) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(27) ) );

			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(36) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(37) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(38) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(39) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(40) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(41) ) );
		}

		if( IsRegionOpened(Regions.RAMB) ){
			Log ("Adding 'Ramb' Runs...");
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(32) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(33) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(34) ) );
			AvailbleRuns.Add ( new PracticeRun( DAO.getPracticeRunByIndex(35) ) );
		}

		Log ("Done.");

	}

	void UpdateCurrentRun(){
		nextSection = 0;

		currentRunIndex = GetRandom(0, AvailbleRuns.Count, currentRunIndex);
		currentRun = AvailbleRuns[ currentRunIndex ];

	}

	int GetRandom(int from, int to, int excepts){
		int res = Random.Range (from, to);

		if(res == excepts) return GetRandom(from, to, excepts);
		else return res;
	}


	void BuildFirstSections (){
		currentRunIndex = Random.Range(0 , (AvailbleRuns.Count-1)/2);
		currentRun = AvailbleRuns[ currentRunIndex*2 ];
		RunForDeleter = currentRun;
//		Debug.Log ("alon___________ first currentRun = " + currentRunIndex);
		nextSectionPositionOffset = new Vector3 (0f, 0f, SECTION_WIDTH);
		
		if (NUM_OF_SECTIONS_ON_SCENE > currentRun.sections.Count) NUM_OF_SECTIONS_ON_SCENE = currentRun.sections.Count;
		
		for (int i=0; i<NUM_OF_SECTIONS_ON_SCENE; i++) {
			AddNewSection();
		}
		
		currentRun.sections[0].ObjectOnScene.GetComponentsInChildren<WorldSectionTrigger>()[0].isEnabled = false;


	}

	public static void OnSectionAchieved(int direction, bool Is_T_Section, WorldSection.RegionType region){
		if (direction < 0) { // turn left
			needToTurnLeft = true;
		} else if (direction > 0) { // turn right
			needToTurnRight = true;
		} else {
			needToRemoveSection = true;
		}

		if( Is_T_Section && FreezeBuilder){
			FreezeBuilder = false;
		} 

		if (curRegion != region) {
			curRegion = region;
//			Utils.SetFogColor (curRegion);
		}
		
		
	}

	void AddNewSection(){

		StartCoroutine(AddSection());
	}
	
	IEnumerator AddSection(){
		if (nextSection >= currentRun.sections.Count) UpdateCurrentRun();

		section = currentRun.sections [nextSection];
		
		if (instantiateDoubleSection) {
			instantiateDoubleSection = false;
			
			currentRun.sections [nextSection].ObjectOnScene = (GameObject)Instantiate (PrefabManager.instanse.GetPrefabBySectionID(section.id), nextSectionPosition, Quaternion.Euler( nextSectionRotation) );
			if(section.collectables.Count > 0 || section.obstacles.Count > 0) section.ObjectOnScene.GetComponent<WorldSection>().PlaceObjects( section.collectables, section.obstacles );
			
			TurnBuilderRight();
			TurnBuilderRight();
			nextSectionPosition += nextSectionPositionOffset;
			nextSectionPosition += nextSectionPositionOffset;
			section.ObjectOnSceneCopy = (GameObject)Instantiate ( PrefabManager.instanse.GetPrefabBySectionID(section.id), nextSectionPosition, Quaternion.Euler( nextSectionRotation) );
			if(section.collectables.Count > 0 || section.obstacles.Count > 0) section.ObjectOnSceneCopy.GetComponent<WorldSection>().PlaceObjects( section.collectables, section.obstacles );
			
			nextSectionPosition -= nextSectionPositionOffset;
			TurnBuilderLeft();
			FreezeBuilder = true;

		} else {
			section.ObjectOnScene = (GameObject)Instantiate ( PrefabManager.instanse.GetPrefabBySectionID(section.id), nextSectionPosition, Quaternion.Euler( nextSectionRotation) );
			if(section.collectables.Count > 0 || section.obstacles.Count > 0) section.ObjectOnScene.GetComponent<WorldSection>().PlaceObjects( section.collectables, section.obstacles );
			
		}
		
		
        currentRun.sections[nextSection].dontSetActiveFalse = currentRun.sections[nextSection].ObjectOnScene.GetComponent<WorldSection>().dontSetActiveFalse;

		
		if ( Utils.IsTurnLeftSection(section.id) ) TurnBuilderLeft ();
		if ( Utils.IsTurnRightSection(section.id) ) TurnBuilderRight ();
		
		if (Utils.IsTurnUpSection(section.id)) TurnBuilderUp ();
		if (Utils.IsTurnDownSection(section.id)) {
			TurnBuilderDown ();
			vectorZona = section.ObjectOnScene.transform.position;
			vectorZona.y -= SECTION_HEIGHT_LEVEL;
			section.ObjectOnScene.transform.position = vectorZona;
			
			if(section.ObjectOnSceneCopy != null){
				vectorZona = section.ObjectOnSceneCopy.transform.position;
				vectorZona.y -= SECTION_HEIGHT_LEVEL;
				section.ObjectOnSceneCopy.transform.position = vectorZona;
			}
			
		}
		
		if (Utils.IsTSection(section.id)) {
			instantiateDoubleSection = true;
			TurnBuilderLeft();
		}
		
		
		nextSectionPosition += nextSectionPositionOffset;
		
		
		nextSection++;
		CurrentNumOfSectionsOnScene++;
		
		yield return null;
	}

	void TurnBuilderLeft(){
		nextSectionRotation.y -= 90f;
		
		if (nextSectionPositionOffset.z != 0f) {
			nextSectionPositionOffset.x = -nextSectionPositionOffset.z;
			nextSectionPositionOffset.z = 0f;
		} else {
			nextSectionPositionOffset.z = nextSectionPositionOffset.x;
			nextSectionPositionOffset.x = 0f;
		}
		
	}
	
	void TurnBuilderRight(){
		nextSectionRotation.y += 90f;
		
		if (nextSectionPositionOffset.z != 0f) {
			nextSectionPositionOffset.x = nextSectionPositionOffset.z;
			nextSectionPositionOffset.z = 0f;
		} else {
			nextSectionPositionOffset.z = -nextSectionPositionOffset.x;
			nextSectionPositionOffset.x = 0f;
		}
		
	}
	
	void TurnBuilderUp(){
		nextSectionPosition.y += SECTION_HEIGHT_LEVEL;
	}
	
	void TurnBuilderDown(){
		nextSectionPosition.y -= SECTION_HEIGHT_LEVEL;
	}

	IEnumerator RemoveUsedSections()
    {
        yield return removeDelay;
        waitingToRemoveSection = false;

        if (!PlayerController.instance.isDead)
        {
//			if (isFirstSection) {
//				isFirstSection = false;
//				yield break;
//			}
            if (sectionIndexToDelete >= RunForDeleter.sections.Count)
            {
                RunForDeleter = currentRun;
                sectionIndexToDelete = 0;
            }
            if (previosSectionToDelete > -1)
            {
                RemovePreviosSection();
            }

            //print ("***RemoveUsedSection*****");
            if (!RunForDeleter.sections[sectionIndexToDelete].dontSetActiveFalse)
            {
                RunForDeleter.sections[sectionIndexToDelete].ObjectOnScene.SetActive(false);//Destroy (RunForDeleter.sections [sectionIndexToDelete].ObjectOnScene);
                RunForDeleter.sections[sectionIndexToDelete].ObjectOnScene = null;
            }
		
			if (RunForDeleter.sections [sectionIndexToDelete].ObjectOnSceneCopy != null) {
				RunForDeleter.sections [sectionIndexToDelete].ObjectOnSceneCopy.SetActive (false);//Destroy (RunForDeleter.sections [sectionIndexToDelete].ObjectOnSceneCopy);
				RunForDeleter.sections [sectionIndexToDelete].ObjectOnSceneCopy = null;
			}
		
			sectionIndexToDelete++;
			CurrentNumOfSectionsOnScene--;

		}
		else {
			previosSectionToDelete = sectionIndexToDelete;
		}
	}
	void RemovePreviosSection(){
		//print ("***RemovePreviousSection *****");
		RunForDeleter.sections [previosSectionToDelete].ObjectOnScene.SetActive (false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnScene);
		RunForDeleter.sections [previosSectionToDelete].ObjectOnScene = null;

		if (RunForDeleter.sections [previosSectionToDelete].ObjectOnSceneCopy != null) {
			RunForDeleter.sections [previosSectionToDelete].ObjectOnSceneCopy.SetActive (false);//Destroy (run.sections [sectionIndexToDelete].ObjectOnSceneCopy);
			RunForDeleter.sections [previosSectionToDelete].ObjectOnSceneCopy = null;
		}

		sectionIndexToDelete++;
		CurrentNumOfSectionsOnScene--;
		previosSectionToDelete = -1;
	}


	public void DecreaseWaitInterval(){
		SECTION_REMOVE_DELAY_IN_SECONDS -= 0.05f;
		if (SECTION_REMOVE_DELAY_IN_SECONDS < 0.07f) {
			SECTION_REMOVE_DELAY_IN_SECONDS = 0.07f;
		}
		removeDelay = new WaitForSeconds (SECTION_REMOVE_DELAY_IN_SECONDS);

		//Debug.Log ("alon_____________ SECTION_REMOVE_DELAY_IN_SECONDS: " + SECTION_REMOVE_DELAY_IN_SECONDS);
	}


	
//	GameObject GetPrefabBySectionID(string id){
//		
//		if( id.Equals("g3") ) 		return PrefabManager.instanse.generic_section_prefabs[0];
//		if( id.Equals("g3b") ) 		return PrefabManager.instanse.generic_section_prefabs[1];
//		if( id.Equals("g3d") ) 		return PrefabManager.instanse.generic_section_prefabs[2];
//		if( id.Equals("g3t") ) 		return PrefabManager.instanse.generic_section_prefabs[3];
//		if( id.Equals("g3t5") ) 	return PrefabManager.instanse.generic_section_prefabs[4];
//		if( id.Equals("g3tl") ) 	return PrefabManager.instanse.generic_section_prefabs[5];
//		if( id.Equals("g3tr") ) 	return PrefabManager.instanse.generic_section_prefabs[6];
//		if( id.Equals("g3u") ) 		return PrefabManager.instanse.generic_section_prefabs[7];
//		if( id.Equals("g5") ) 		return PrefabManager.instanse.generic_section_prefabs[8];
//		
//		if( id.Equals("g5b") ) 		return PrefabManager.instanse.generic_section_prefabs[9];
//		
//		if( id.Equals("g5d") ) 		return PrefabManager.instanse.generic_section_prefabs[10];
//		if( id.Equals("g5t") ) 		return PrefabManager.instanse.generic_section_prefabs[11];
//		if( id.Equals("g5t3") ) 	return PrefabManager.instanse.generic_section_prefabs[12];
//		if( id.Equals("g5tl") ) 	return PrefabManager.instanse.generic_section_prefabs[13];
//		if( id.Equals("g5tr") ) 	return PrefabManager.instanse.generic_section_prefabs[14];
//		if( id.Equals("g5u") ) 		return PrefabManager.instanse.generic_section_prefabs[15];
//		
//		return PrefabManager.instanse.generic_section_prefabs [0];
//	}









}















	
