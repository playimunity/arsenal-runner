using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Amazon;

public class UIUpdater_TutorialRun : MonoBehaviour {

	int FRAMERATE = 10;
	int CUR_FRAME = 0;

	public Text totalMagnetsTXT;
	public Text totalEnergyDrinksTXT;

	public Text RunGoalDescriptionTXT;
	public Text PlayerNumberAndNameTXT;
    public Text FailPlayerNameTXT;

	Vector2 AncPos;




	void FixedUpdate () {
		if (CUR_FRAME < 0) CUR_FRAME = FRAMERATE;
		if (CUR_FRAME == 0) UpdateUI();
		CUR_FRAME--;
	}
	
	void UpdateUI(){
//		totalMagnetsTXT.text = DAO.TotalMagnetsCollected.ToString();
//		totalEnergyDrinksTXT.text = DAO.TotalSpeedBoostsCollected.ToString();

	}

	public void RestartScene(){

		GameManager.SwitchState(GameManager.GameState.TEST_RUN);
	}


	void Start(){
		StartCoroutine (StartUIUpdate());
	}

	IEnumerator StartUIUpdate(){
		yield return new WaitForSeconds (0.3f);
		RunGoalDescriptionTXT.text = Run.Instance.runGoal.goalDescription.goal_A;
		PlayerNumberAndNameTXT.text = PlayerController.instance.data.player.playerName;
        FailPlayerNameTXT.text = PlayerController.instance.data.player.playerName;

		//PlayerNumberAndNameTXT.text = PlayerController.instance.data.player.playerNumber + " " + PlayerController.instance.data.player.playerName;
		if (UnityInitializer.Instance != null)
			UnityInitializer.Instance.StopTracking ();
		// Intro Video
//		if (!Tutorial.Instance.tutRetryed) {
//			Handheld.PlayFullScreenMovie ("intro.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);
//		}
	}
}
