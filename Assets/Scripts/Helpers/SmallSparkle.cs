﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SmallSparkle : MonoBehaviour {

	Color sColor;
	public Color startColor;

	Vector2 sPos;
	Vector2 sRot;
	Vector2 sScale;

	float startTime;
	float forceUp;
	float forceSide;
	float sScaleX;

	RectTransform sRectTransform;

	Image sImage;

	Rigidbody2D sRigidB;

	WaitForFixedUpdate _waitFixedUpdate = new WaitForFixedUpdate();



	void Awake () {
		sRectTransform = GetComponent<RectTransform> ();
		sImage = GetComponent<Image> ();
		sRigidB = GetComponent<Rigidbody2D> ();

		//startTime = Time.time;
		//sImage.color = sColor;
//		sScaleX = Random.Range(25f,60f);
//		sScale.x = sScale.y = sScaleX;
//		sRectTransform.sizeDelta = sScale;
//		sRigidB.AddForce (Vector2.up * Random.Range (2000f, 4000f));
//		sRigidB.AddForce (Vector2.left * Random.Range (-2000f, 2000));
		//sRigidB.MoveRotation (Random.Range(-90f,90f));
//		sRigidB.rotation = 30f;
//		StartCoroutine (SparkleFade());
	}

	void OnEnable(){
		sImage.color = sColor = startColor;
		startTime = Time.time;
		sScaleX = Random.Range(25f,60f);
		sScale.x = sScale.y = sScaleX;
		sRectTransform.sizeDelta = sScale;
		sRigidB.AddForce (Vector2.up * Random.Range (2000f, 4000f));
		sRigidB.AddForce (Vector2.left * Random.Range (-2000f, 2000));
		sRigidB.rotation = 30f;
		StartCoroutine (SparkleFade());
	}

	IEnumerator SparkleFade(){
		while (sColor.a > 0f) {
			sColor.a -= 0.015f;
			sImage.color = sColor;
			yield return _waitFixedUpdate;
		}
		//Destroy (gameObject);
		gameObject.SetActive(false);
		RunManager.instance.sparklesList.Add (transform);
	}
	

}
