using UnityEngine;
using System.Collections;
using System;

public class Tutorial : MonoBehaviour {

	public static Tutorial Instance;

	public bool tutorialPointOn = false;
	public bool tutRetryed = false;
	bool finishedPoint = false;
	bool finishedState1 = false;
	bool finishedState2 = false;
	bool finishedState3 = false;
	bool finishedState4 = false;

	public int tutorialState = 0;
    public int tutorialEndState = 5;
	WaitForFixedUpdate waitFixedUpdate = new WaitForFixedUpdate();
	WaitForSeconds waitForSec_0_15 = new WaitForSeconds(0.15f);




	/*		TUTORIAL STATES
	 * 
	 * 		0 - New 					(need to load menu in tutorial substate)
	 * 		1 - Recriut Player			(need to show recruit player tutorial)
	 * 		2 - Let's run				(show generic message with call to run)
	 * 		3 - Let's run 2				(show generic message with call to run, with skip button)
	 * 		4 - Firs time tutorial complete
	 * 
	 */



	void Awake () {
		Instance = this;
	}


	public void SwipeRight(){
		if (GameManager.TutorialPaused && tutorialPointOn && tutorialState == 0) {
			
			ExitSlowDown ();
			PlayerController.instance.TurnRight ();
			tutorialState++;
			tutorialPointOn = false;

			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.SWIPE_R );
		}

		if (tutorialState >= tutorialEndState) {
			PlayerController.instance.TurnRight ();
		}
	}


	public void SwipeLeft(){
		if (GameManager.TutorialPaused && tutorialPointOn && tutorialState == 1) {

			ExitSlowDown ();
			PlayerController.instance.TurnLeft ();
			tutorialState++;
			tutorialPointOn = false;

			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.SWIPE_L );
		}

        if (tutorialState >= tutorialEndState) {
			PlayerController.instance.TurnLeft ();
		}
	}
		

	public void Jump(){
		if (GameManager.TutorialPaused && tutorialPointOn && tutorialState == 2) {
			
			ExitSlowDown ();
			PlayerController.instance.Jump ();
			tutorialState++;
			tutorialPointOn = false;

			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.JUMP );
		}

        if (tutorialState >= tutorialEndState) {
			PlayerController.instance.Jump ();
		}
	}


	public void Slide(){
		if (GameManager.TutorialPaused && tutorialPointOn && (tutorialState == 3 || tutorialState == 5)) {
			
			ExitSlowDown ();
			PlayerController.instance.Slide ();
			tutorialState++;
			tutorialPointOn = false;
			if (GameManager.Instance.isPlayscapeLoaded) {
				if (tutorialState == 3)
					BI._.Flow_FTUE (BI.FLOW_STEP.SLIDE);
				else
					BI._.Flow_FTUE (BI.FLOW_STEP.SLIDE_2);
			}
		}

        if (tutorialState >= tutorialEndState) {
			PlayerController.instance.Slide ();
		}
	}


	public void LaunchBall(GameObject target){
		if (GameManager.TutorialPaused && tutorialPointOn && (tutorialState == 4 || tutorialState == 6)) {
			
			ExitSlowDown ();
			PlayerController.instance.LaunchBall (target);
			tutorialState++;
			tutorialPointOn = false;

			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.KICK );
		}

        if (tutorialState >= tutorialEndState) {
			PlayerController.instance.LaunchBall (target);
		}
	}



	void ExitSlowDown(){
		//GameManager.RequestUnpause (true);
		GameManager.Instance.TutorialPointUnpause();
		//CupRunController.Instance.TutSwipePanelUp (false);
		CupRunController.Instance.TutHideAnim ();
	}


	public void PlayTutAnim(){
		//CupRunController.Instance.TutSwipePanelUp (true);

		switch (tutorialState) {
		case 0:
			{
				//CupRunController.Instance.tutorialHeaderTxt.text = DAO.Language.GetString ("swipe-right");
				CupRunController.Instance.TutSwipeRight ();
				break;
			}
		case 1:
			{
				//CupRunController.Instance.tutorialHeaderTxt.text = DAO.Language.GetString ("swipe-left");
				CupRunController.Instance.TutSwipeLeft ();
				break;
			}
		case 2:
			{
				//CupRunController.Instance.tutorialHeaderTxt.text = DAO.Language.GetString ("jump");
				CupRunController.Instance.TutSwipeUp ();
				break;
			}
		case 3:
			{
				//CupRunController.Instance.tutorialHeaderTxt.text = DAO.Language.GetString ("slide");
				CupRunController.Instance.TutSwipeDown ();
				break;
			}
		case 4:
			{
				//CupRunController.Instance.tutorialHeaderTxt.text = DAO.Language.GetString ("hit-target");
				CupRunController.Instance.TutHit ();
				break;
			}
		case 5:
			{
				//CupRunController.Instance.tutorialHeaderTxt.text = DAO.Language.GetString ("tackle");
				CupRunController.Instance.TutTackle ();
				break;
			}
		case 6:
			{
				//CupRunController.Instance.tutorialHeaderTxt.text = DAO.Language.GetString ("shoot");
				CupRunController.Instance.TutHitGlass ();
				break;
			}
		}

		StartCoroutine (ActivateTutorialPoint());
	}

	IEnumerator ActivateTutorialPoint(){
		yield return waitForSec_0_15;
		Tutorial.Instance.tutorialPointOn = true;
	}



	public void FinishMassage1(){
		CupRunController.Instance.YouAreReady1Up (true);
		GameAnalyticsWrapper.DesignEvent (GameAnalyticsWrapper.FLOW_STEP.TUTORIAL_COMPLETED);
	}




	public static bool IsNeedToShowMenuTutorial{   //tells the DAO.TutorialState if the tutorial state is 0 or not.
		get{ 
			return (DAO.TutorialState == 0);
		}
	}


	public static bool IsNeedToShowTutorialRun{  
		get{ 
			return (DAO.TutorialState == 1);
		}
	}


	public static bool IsNeedToShowRunLockerRoomTutorial{   
		get{ 
			return (DAO.TutorialState == 2);
		}
	}

	public static bool IsNeedToShowRunLockerRoomSkipTutorial{   
		get{ 
			return (DAO.TutorialState == 3);
		}
	}

	public static bool IsNeedToBounceCupButton{
		get{ 
			return (DAO.TutorialState == 4);
		}
	}



	public static bool Is7DaysPassedFromInstall(){

		return ( (DateTime.Now - DAO.InstallDate).TotalDays >= 7);
	
	}


	public static bool IsNeedToShowHealPopup{   
		get{ 
			return (DAO.HealPopoup == 0);
		}
	}


	public static bool IsNeedToShowBuySecondPlayer1Popup{   
		get{ 
			return (DAO.BuySecondPlayerPopup1 == 0);
		}
	}


	public static bool IsNeedToShowBuySecondPlayer2Popup{   
		get{ 
			return (DAO.BuySecondPlayerPopup1 == 1 && DAO.BuySecondPlayerPopup2 == 0 && (DateTime.Now - DAO.RecruitSecondPlayerPopup2Date).TotalHours >= 24);
		}
	}


	public static bool IsNeedToShowBuyThirdPlayerPopup{   
		get{ 
			return (DAO.BuyThirdPlayerPopup == 0);
		}
	}


	public static bool IsNeedToShowPlayPracticePopup{   
		get{ 
			return (DAO.PlayPracticePopup == 0 && DAO.PlayTimeInMinutes >= 120);
		}
	}


	public static bool IsNeedToShowPlayCupPopup{   
		get{ 
			return (DAO.PlayCupPopup == 0 && DAO.PlayTimeInMinutes >= 120 && (DateTime.Now - DAO.PlayCupPopupDate).TotalHours >= 24);
		}
	}


	public static bool IsNeedToShowSpecialOfferPopup{   
		get{ 
			return (DAO.OfferPopup == 0 && DAO.PlayTimeInMinutes >= 10 && (DateTime.Now - DAO.InstallDate).TotalDays <= 7  && (DateTime.Now - DAO.SpecialOfferPopupDate).TotalHours >= 24);
		}
	}


	public static bool IsNeedToShowSwipingHand{   
		get{ 
			return (DAO.SwipingHand == 0 || DAO.SwipingHand == 1 || DAO.SwipingHand == 2);
		}
	}


	public static bool IsNeedToShowCardsHandTut{   
		get{ 
			return (DAO.CardsHandTut == 0);
		}
	}


	public static bool IsNeedToShowCardsPowerUpsTut{   
		get{ 
			return (DAO.CardsPowerUpsTut == 0);
		}
	}




}
