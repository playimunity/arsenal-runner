﻿using UnityEngine;
using System.Collections;
using System;
using Amazon;
using OnePF;
using System.Collections.Generic;


public class AppsFlyerManager : MonoBehaviour {

	public static AppsFlyerManager _;
    public bool isSandbox = true;
    public bool isAppsFlyer;
	void Awake(){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("AppsFlyerManager start");
		_ = this;
	}

	public static string publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlnOneJ9Lm/fFP+b4Tv5ZF5GtRzvBmd3lXu62/J6hG0eZTHq6Np5rLVPr6q/vzlMrIXuNXlbvnQMOGb7hH8Rp0dBjrT9GXOSLYWGfx2Y4U3NLqUe/6Nen22RNFrJKxxKWpA7ssb1n5It17J+iYonyGVXRonxk2fvDPOLcdUfE08OePHNa/LDipJ9PhOIMCc1et4D4XhtPliXBcWW39OhiTGGXr6BAPHv1Rjv0myrAkjUiWemnyy751u0l4hS7e48tSjoxkpqcUL/1Q1dHgML4RraSdNtBCT3Ks+XKAt2DC1JrHEtbHSPzBQjETNs114BuKXPhUS/RTMqOhH+gb06oswIDAQAB";
	void Start () {
        if (!isAppsFlyer)
            return;
		#if !UNITY_EDITOR
		AppsFlyer.setAppsFlyerKey ("G5qxJYGmGX392JuXyDYNnK");
		#endif

		#if UNITY_IOS 

        AppsFlyer.setAppID ("1158656031");

		// For detailed logging
		//AppsFlyer.setIsDebug (true); 

		// For getting the conversion data will be triggered on AppsFlyerTrackerCallbacks.cs file
		//AppsFlyer.getConversionData (); 

		// For testing validate in app purchase (test against Apple's sandbox environment
		//AppsFlyer.setIsSandbox(true);         

		AppsFlyer.trackAppLaunch ();

		#elif UNITY_ANDROID

		// All Initialization occur in the override activity defined in the mainfest.xml, 
		// including the track app launch
		// For your convinence (if your manifest is occupied) you can define AppsFlyer library
		// here, use this commented out code.

//		AppsFlyer.setAppID ("com.gamehour.afc"); 
//		AppsFlyer.setIsDebug (true);
		//AppsFlyer.createValidateInAppListener ("AppsFlyerTrackerCallbacks", "onInAppBillingSuccess", "onInAppBillingFailure");
		//AppsFlyer.loadConversionData("AppsFlyerTrackerCallbacks","didReceiveConversionData", "didReceiveConversionDataWithError");
//		AppsFlyer.trackAppLaunch ();
		#endif
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("AppsFlyerManager end");
		//UnityEngine.Debug.Log("RON____________xAppsFlyerManager awake end Time:"+ DateTime.Now.ToLongTimeString());
	}

	public  void ValidatePurchase(Purchase p, IAPProduct iapp)
	{
        if (!isAppsFlyer)
            return;
		//GameAnalyticsWrapper.ResourceEvent(true, "Coins",1000,"Coins","Store");
		string purchaseData = p.OriginalJson;
        string currency = "";
        #if UNITY_IOS
                currency= iapp.SKUDetails.CurrencyCode;
        #elif UNITY_ANDROID
        currency= iapp.SKUDetails.CurrencyString;
        #endif
		Debug.Log ("publicKey:" + publicKey);
		Debug.Log ("purchaseData:" + purchaseData);
        Debug.Log ("Receipt:" + p.Receipt);
		Debug.Log ("price:" + iapp.PriceValue);
		Debug.Log ("currency:" + currency);
        Debug.Log("JSON: " + iapp.JSONString);
        Debug.Log("JSONSPECIAL: " + iapp.SKUDetails.JSONSPECIAL.fields.Count);
        Debug.Log("OrderID: " + p.OrderId);


        Dictionary<string,string> details = new Dictionary<string, string>(){
        {
            "Item",p.Sku
        },
        {
            "Price", iapp.PriceValue
        },
        {
            "Currency", currency
        }
        };
        iappGlobal = iapp;
		try{
        #if UNITY_ANDROID
			AppsFlyer.validateReceipt(publicKey,purchaseData,p.Signature,"0",iapp.CurrencyString, new Dictionary<string,string>());
            
			AppsFlyer.createValidateInAppListener ("AppsFlyerManager", "onInAppBillingSuccess", "onInAppBillingFailure");
            #endif
            #if UNITY_IOS
            //AppsFlyer.createValidateInAppListener()
            AppsFlyerTrackerCallbacks.instance.currency = iapp.SKUDetails.CurrencyCode;
            AppsFlyerTrackerCallbacks.instance.priceValue = iapp.PriceValue;
            AppsFlyerTrackerCallbacks.instance.SKU = p.Sku;
            AppsFlyer.validateReceipt(p.Sku,iapp.PriceValue,currency,p.Receipt, details);
			ReportIAP( iapp );
            #endif
		}
		catch(Exception e)
		{
			Debug.Log(e.Message);
			Debug.Log(e.StackTrace);
		}
	}
	public static IAPProduct iappGlobal;
	public void onInAppBillingSuccess()
	{
        if (!isAppsFlyer)
            return;
		string currency = "";
        #if UNITY_IOS
		currency= iappGlobal.SKUDetails.CurrencyCode;
        #elif UNITY_ANDROID
		currency= iappGlobal.SKUDetails.CurrencyString;
        #endif
		AppsFlyer.trackRichEvent ("IAP Validated - "+iappGlobal.Sku, new System.Collections.Generic.Dictionary<string, string> (){
			
			{"af_revenue", iappGlobal.PriceValue},
			{"af_currency", iappGlobal.CurrencyString}
		});
		Debug.Log ("LALALALALA ===> - A REAL Purchase");
	}

	public void onInAppBillingFailure()
	{
        if (!isAppsFlyer)
            return;
		Debug.Log ("LALALALALA ===> - NOT A REAL Purchase");
		string currency = "";
        #if UNITY_IOS
		currency= iappGlobal.SKUDetails.CurrencyCode;
        #elif UNITY_ANDROID
		currency= iappGlobal.SKUDetails.CurrencyString;
        #endif
		AppsFlyer.trackRichEvent ("IAP NOT Validated - "+iappGlobal.Sku, new System.Collections.Generic.Dictionary<string, string> (){
			
			{"af_revenue", "0"},
			{"af_currency", iappGlobal.CurrencyString}
		});
	}

	#if !UNITY_EDITOR
	public void ReportIAP( IAPProduct product ){
    if (!isAppsFlyer)
            return;
		//AppsFlyer.trackEvent ("IAP - " + product.Sku, "1");
    string currency = "";
    #if UNITY_IOS
        currency= product.SKUDetails.CurrencyCode;
        #elif UNITY_ANDROID
        currency= product.SKUDetails.CurrencyString;
        #endif
		AppsFlyer.trackRichEvent ("IAP - "+product.Sku, new System.Collections.Generic.Dictionary<string, string> (){
			
			{"af_revenue", product.PriceValue},
            {"af_currency", currency}
		});

	}

	public void ReportVirtualEconomy(string item_type, string item_name, string item_cost, string amount){
    if (!isAppsFlyer)
            return;
		AppsFlyer.trackRichEvent ("Virtual Economy - "+item_type, new System.Collections.Generic.Dictionary<string, string> (){
			{"amount", amount},
			{"name", item_name},
			{"cost", item_cost}
		});

	}

	public void ReportGiftCards(string action, string amount){
    if (!isAppsFlyer)
            return;
		AppsFlyer.trackRichEvent ("Gift Cards - "+action, new System.Collections.Generic.Dictionary<string, string> (){
			{"amount", amount}
		});

	}

	public void ReportFBLogin(){
    if (!isAppsFlyer)
            return;
		AppsFlyer.trackRichEvent ("Connect with Facebook", new System.Collections.Generic.Dictionary<string, string> (){
			{"fb connected", "1"}
		});
	}

	public void ReportUserTypeChange( UserData.UserType new_type ){
    if (!isAppsFlyer)
            return;
		AppsFlyer.trackRichEvent ("UserType Upgraded to "+new_type.ToString(), new System.Collections.Generic.Dictionary<string, string> (){
			{"User Become "+new_type.ToString(), "1"}
		});

	}

	#else

	public void ReportIAP( IAPProduct product ){	}

	public void ReportVirtualEconomy(string item_type, string item_name, string item_cost, string amount){}

	public void ReportGiftCards(string action, string amount){}

	public void ReportFBLogin(){}

	public void ReportUserTypeChange( UserData.UserType new_type ){}

	#endif
}
