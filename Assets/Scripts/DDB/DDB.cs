﻿using UnityEngine;
using System.Collections;
using Amazon.DynamoDBv2;
using Amazon.Runtime;
using Amazon.CognitoIdentity;
using Amazon;
using Amazon.DynamoDBv2.DataModel;
using System.Threading;
using System.Collections.Generic;
using Facebook.Unity;
using System;
using Amazon.DynamoDBv2.Model;
using Amazon.DynamoDBv2.DocumentModel;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

using Amazon.Util;

//using System.IO;

public class DDB : MonoBehaviour {

	string IDENTITY_POOL_ID = "us-east-1:49203a56-67a9-40e0-99ec-ee7bab9d9b3f";
	int PLAY_TIME_DIFFERENCE_TO_PREFER_LOCAL_DATA = 30;
	public static DDB _;
	public bool DataReady = false;

	List<string> SuccessfullyRemovedRequests;
	public static event Action OnRecievedGiftCardsChanged;
	public static event Action OnGiftCardsClaimed;

	private static IAmazonDynamoDB _ddbClient;
	private CognitoAWSCredentials _credentials;
	private DynamoDBContext _context;

	GiftCardsStructure gc;
	RequestGiftCardsStructure gcr;
	public List<string> RecievedGiftCards;
	public List<string> RecievedGiftCardsRequests;
	private bool bIsDDBReady;
	private string sErorr;
	private string id;
	private StreamWriter outStream;
	public UserDataStructure UserDataToCompareFromDB;
	private int readDataIndex = 0;
	private int whaleTrackerIndex = 0;


	Thread FriendLoadThread;

	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;

		Debug.Log ("Dynamo DB | " + msg);
	}

	public void Awake(){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("DDB Start");
		_ = this;
		bIsDDBReady = false;
		RecievedGiftCards = new List<string> ();
		RecievedGiftCardsRequests = new List<string> ();
		UserDataToCompareFromDB = null;



	}
		

	private CognitoAWSCredentials Credentials{
		get{
			if (_credentials == null){
				Log("creating credentials");
				if (_credentials == null) _credentials = new CognitoAWSCredentials(IDENTITY_POOL_ID, RegionEndpoint.USEast1);
				Log("credential created");

			}
			return _credentials;
		}
	}

	protected IAmazonDynamoDB Client{
		get{
			if (_ddbClient == null){
				Log("creating client");
				_ddbClient = new AmazonDynamoDBClient(Credentials,RegionEndpoint.USWest2);
				Log("client created");
			}
			return _ddbClient;
		}
	}

	private DynamoDBContext Context{
		get
		{
			if (_context == null){
				Log("creating context");
				_context = new DynamoDBContext(Client);
				Log("context created");
			}
			return _context;
		}
	}


	// =============================================================================================================================================

	public void SaveAll(){
		if (!FB.IsLoggedIn) return;


		SaveData ();
	}

	public void SaveData(){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("DDB SaveData Start");
		try {
			//Debug.Log("RON_______SaveData start Time:"+ DateTime.Now.ToLongTimeString());
		UserDataStructure uds = new UserDataStructure ();
		Context.SaveAsync(uds, (result) => {
			if (result.Exception == null){
				Log(" UserData Saved ");
			}else{
				
				Log(" Error Saving UserData: " + result.Exception.Message + " | --- | " + result.Exception.StackTrace);             
			}
		});	

		}catch (Exception ex) {
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("DDB SaveData failed:"+ex.Message);
	}
	




		//uds.PrintMe ();
		/*
		Preloader.Instance.PrintStatus ("Before Context");
		try {
			Preloader.Instance.PrintStatus ("Context is null: " +(Context==null).ToString());
			Context.SaveAsync(uds, (result) => {
				if (result==null)
				{
					Preloader.Instance.PrintStatus ("Result is null: " +(result==null).ToString());
				}
				if (result.Exception == null){
					Log(" UserData Saved ");
				}else{
					Preloader.Instance.PrintStatus ("Error Saving UserData: " + result.Exception.Message + " | --- | " + result.Exception.StackTrace);
					Log(" Error Saving UserData: " + result.Exception.Message + " | --- | " + result.Exception.StackTrace); 

				}
			});	
		} catch (Exception ex) {
			Preloader.Instance.PrintStatus ("DDBSAveData Exception: "+ex.StackTrace, false);
		}
*/

	}

	public void SaveOldProgressData(){
		//Debug.Log("RON_______SaveOldProgressData start Time:"+ DateTime.Now.ToLongTimeString());
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("DDB SaveData Start");
		try {
			UserDataOldProgressStructure uds = new UserDataOldProgressStructure ();
			Context.SaveAsync(uds, (result) => {
				if (result.Exception == null){
					Log(" UserData Saved ");
				}else{

					Log(" Error Saving UserData: " + result.Exception.Message + " | --- | " + result.Exception.StackTrace); 

				}
			});	

		}catch (Exception ex) {
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("DDB SaveData failed:"+ex.Message);
		}
	}


	public delegate void FriendQuestProgressCallback(int quest_id);    
	public void GetFriendsProgress(string friend_id, FriendQuestProgressCallback callback ){
		try {
			//Debug.Log("RON_______GetFriendsProgress start Time:"+ DateTime.Now.ToLongTimeString());
		Context.LoadAsync<FriendQuestProgressStructure> (friend_id, (result) => {

			if (result.Result == null) {
				callback.Invoke( -1 );
				return;
			}

			callback.Invoke( result.Result.current_quest );

		}, null);

		}catch (Exception ex) {
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("DDB GetFriendsProgress failed:"+ex.Message);
		}
	}

	public void ReportQuestProgress(){
		try {
			//Debug.Log("RON_______ReportQuestProgress start Time:"+ DateTime.Now.ToLongTimeString());
		Context.SaveAsync(new FriendQuestProgressStructure(), (send_result) => {
			if (send_result.Exception == null){
				Log("Current Quest Status Successfully Updated");
			}else{
				Log(" Error Updating Current Quest Status | " + send_result.Exception.Message);
			}
		});
		}catch (Exception ex) {
			if (UnityInitializer.Instance != null)
				UnityInitializer.Instance.addToMonitorLog ("DDB ReportQuestProgress failed:" + ex.Message);
		}
	}

	/*
	public IEnumerator isDDBReady()
	{
		
		Debug.Log("RON_______DDB isDDBReady start Time:"+ DateTime.Now.ToLongTimeString() + " isDDBReady "+ bIsDDBReady);
		if (bIsDDBReady)
			//return true;
			Context.LoadAsync<UserDataStructure> ("10154003624013463", (result) => {
				if (result.Result == null) {
					bIsDDBReady = false;
				} else {
					bIsDDBReady = true;
				}
			}, null);

		yield return new WaitUntil (() => bIsDDBReady);
	}
*/

	int iFetchNum=0;
	public void ReadWhaleData()
	{
		string FileName = Application.dataPath + "/FCB_Whale" + DateTime.Now.ToString("dd_MM_yy") + ".csv";
		UnityEngine.Debug.Log("RON______________________"+ FileName);
		outStream = System.IO.File.CreateText(FileName);
		outStream.WriteLine("whale_id, revenue, currency, numberOfIAP, lastLoginDate, ftd, coinsAmount, playersAmount, maxRunID, totalMeters, firstName, lastName, email, storeType, isFBConnected, user_id,");
		ReadWhaleData(null);
		//outStream.Close();
	}

	private void ReadWhaleData(Dictionary<string,AttributeValue> lastKeyEvaluated)
	{
		iFetchNum++;
		UnityEngine.Debug.Log("RON______________________Fetch"+ iFetchNum.ToString());
		var request = new ScanRequest
		{
			TableName = "ars_bd_whale_tracker",
			Limit = 100,
			ExclusiveStartKey = lastKeyEvaluated,
//			ExpressionAttributeValues = new Dictionary<string,AttributeValue> {
//				{":val", new AttributeValue { N = "1" }}
//			},
//			FilterExpression = "canSendEmail = :val",

			ProjectionExpression = "whale_id, revenue, currency, numberOfIAP, lastLoginDate, ftd, coinsAmount, playersAmount, maxRunID, totalMeters, firstName, lastName, email, storeType, isFBConnected, user_id"
		};

		Client.ScanAsync(request,(result)=>{
			UnityEngine.Debug.Log("RON______________________");
			foreach (Dictionary<string, AttributeValue> item
				in result.Response.Items)
			{
				UnityEngine.Debug.Log("RON______________________item");
				PrintWhaleItem(item);
			}
			lastKeyEvaluated = result.Response.LastEvaluatedKey;
			if(lastKeyEvaluated != null && lastKeyEvaluated.Count != 0)
			{
				ReadFcbData(lastKeyEvaluated);
			}
			else 
			{
				UnityEngine.Debug.Log("RON______________________end");
				outStream.Close();
			}
		});
	}

	private void PrintWhaleItem(Dictionary<string, AttributeValue> attributeList)
	{

		string text="";
		AttributeValue value;
		//UnityEngine.Debug.Log ("RON________________SACN RESULT:");
		if (attributeList.TryGetValue ("whale_id", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("revenue", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("currency", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("numberOfIAP", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("lastLoginDate", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("ftd", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("coinsAmount", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("playersAmount", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("maxRunID", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("totalMeters", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("firstName", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("lastName", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("email", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("storeType", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("isFBConnected", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("user_id", out value)) {
			text += value.S+",";
		} else
			text += ",";

		outStream.WriteLine(text);
		UnityEngine.Debug.Log("RON________________SACN RESULT:" +text);
	}
		
	public void ReadFcbData()
	{
		string FileName = Application.dataPath + "/FCB_data" + DateTime.Now.ToString("dd_MM_yy") + ".csv";
		UnityEngine.Debug.Log("RON______________________"+ FileName);
		outStream = System.IO.File.CreateText(FileName);
		outStream.WriteLine("First Name, Last Name, Email, Gender, Country, Lang,");
		ReadFcbData(null);
		//outStream.Close();
	}



	private void ReadFcbData(Dictionary<string,AttributeValue> lastKeyEvaluated)
	{
		iFetchNum++;
		UnityEngine.Debug.Log("RON______________________Fetch"+ iFetchNum.ToString());
		var request = new ScanRequest
		{
			TableName = "ars_bd_users",
			Limit = 100,
			ExclusiveStartKey = lastKeyEvaluated,
			ExpressionAttributeValues = new Dictionary<string,AttributeValue> {
				{":val", new AttributeValue { N = "1" }}
			},
			FilterExpression = "canSendEmail = :val",

			ProjectionExpression = "firstName, lastName, email, gender, country, lang "
		};

		Client.ScanAsync(request,(result)=>{
			UnityEngine.Debug.Log("RON______________________");
			foreach (Dictionary<string, AttributeValue> item
				in result.Response.Items)
			{
				UnityEngine.Debug.Log("RON______________________item");
				PrintItem(item);
			}
			lastKeyEvaluated = result.Response.LastEvaluatedKey;
			if(lastKeyEvaluated != null && lastKeyEvaluated.Count != 0)
			{
				ReadFcbData(lastKeyEvaluated);
			}
			else 
			{
				UnityEngine.Debug.Log("RON______________________end");
				outStream.Close();
			}
		});
	}
		
	private void PrintItem(Dictionary<string, AttributeValue> attributeList)
	{
		string text="";
		AttributeValue value;
		//UnityEngine.Debug.Log ("RON________________SACN RESULT:");
		if (attributeList.TryGetValue ("firstName", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("lastName", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("email", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("gender", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("country", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("lang", out value)) {
			text += value.S+",";
		} else
			text += ",";
	
		outStream.WriteLine(text);
		UnityEngine.Debug.Log("RON________________SACN RESULT:" +text);
	}


	public void ReadData(){
		//Debug.Log("RON_______ReadData start Time:"+ DateTime.Now.ToLongTimeString());
		if (UnityInitializer.Instance != null)
			UnityInitializer.Instance.addToMonitorLog ("DDB ReadData start"+readDataIndex.ToString());
		if (DAO.Instance.NO_INTERNET) {
			DataReady = true;
			return;
		}
		if (FBModule.Instance.FBButonClicked)
		{
			CrossSceneUIHandler.Instance.ShowPreloader ();
		}
		if (UnityInitializer.Instance != null)
			UnityInitializer.Instance.addToMonitorLog ("DDB ReadData userID: "+readDataIndex.ToString());
		
		Context.LoadAsync<UserDataStructure>(UserData.Instance.userID.Trim(), (result) => {
			if(result.Result == null){
				if(readDataIndex < 3){ //temp for debugging:
					readDataIndex ++;
					Debug.Log( "alon_____________ DDB - if(readDataIndex < 3) - index: " + readDataIndex);
					StartCoroutine(ReadDataTryAgain());
					return;
				}
				DataReady = true;
				DAO.Instance.DDBDataReady();
				if (DAO.Instance.IsAllDataReady)
				{
					CrossSceneUIHandler.Instance.HidePreloader ();
				}
				return;
			}
			UserDataStructure item;
			if (result.Result != null)
			    item = result.Result;
			else
				item = new UserDataStructure();

			if (!FBModule.Instance.FBButonClicked)
			{
				//item.PrintMe();
				if (UnityInitializer.Instance != null)
					UnityInitializer.Instance.addToMonitorLog ("DDB ReadData suceess iapPoints:ddb"+item.iapPoints.ToString()+"local:"+DAO.IAPPoints +" PlayTimeInMinutes ddb:"+item.playTimeInMinutes
						+"local:"+ DAO.PlayTimeInMinutes+"end");
				
				if( item.iapPoints > DAO.IAPPoints ){
					item.ReplaceLocalData();
				}else if(item.iapPoints < DAO.IAPPoints){
					
					SaveData();
				}else{
					if(item.playTimeInMinutes > DAO.PlayTimeInMinutes){
						item.ReplaceLocalData();
					}else if(item.playTimeInMinutes < DAO.PlayTimeInMinutes){

						SaveData();
					}else{
						//Log("DDB Play Time is == Local Play Time, No need to replace data");
					}
				}
				DataReady = true;
				DAO.Instance.DDBDataReady();
				if (UnityInitializer.Instance != null)
					UnityInitializer.Instance.addToMonitorLog ("DDB ReadData end"+readDataIndex.ToString());
			}
			else
			{
				if (result.Result == null)
				{
					SaveData();
					CrossSceneUIHandler.Instance.HidePreloader ();
				}
				else
				{
					UserDataToCompareFromDB = item;
					// Should open the popup
					CrossSceneUIHandler.Instance.HidePreloader ();
					MainScreenUiManager.instance.ShowFacebookProgression();
				}
				FBModule.Instance.FBButonClicked = false;
			}

			if (FBModule.Instance.FBButonClicked) 
			{
				CrossSceneUIHandler.Instance.HidePreloader ();
			}
			FBModule.Instance.FBButonClicked = false;
		}, null);

	}

	IEnumerator ReadDataTryAgain(){  
		yield return new WaitForSeconds (0.5f);
		ReadData ();
	}


	public void LoadFriendData(string friend_id){
		//Debug.Log("RON_______LoadFriendData start Time:"+ DateTime.Now.ToLongTimeString());
		Context.LoadAsync<UserDataStructure>(friend_id, (result) => {

			if(result.Result == null){
				// TODO: NO User Data Found

				CrossSceneUIHandler.Instance.HidePreloader();

				GenericUIMessage gm = new GenericUIMessage(DAO.Language.GetString("fnf-error-title"), DAO.Language.GetString("fnf-error-boy"));
				gm.PositiveBTNAction = ()=>{
					CrossSceneUIHandler.Instance.hideGenericMessage();	
				};
				gm.PositiveButtonText = DAO.Language.GetString("back");

				CrossSceneUIHandler.Instance.showGennericMessage(gm);

				return;
			}

			UserDataStructure item = result.Result;

			//item.PrintMe();
			//Debug.Log("alon_______loading friends data!");
			PlayerStatsStructure fpss = new PlayerStatsStructure();
			fpss.SetToFriend(item);
			StatsController.Instance.UpdateStats(fpss);
			CrossSceneUIHandler.Instance.HidePreloader();

		}, null);

	}

	public void CheckGiftCards(){
		//Debug.Log("RON_______CheckGiftCards start Time:"+ DateTime.Now.ToLongTimeString());
		if (DAO.Instance.NO_INTERNET || !FB.IsLoggedIn) return;

		Context.LoadAsync<GiftCardsStructure> (UserData.Instance.userID, (result) => {
			if (result.Result == null) {
				Log("GiftCards Result Recieved: NO GIFT CARDS FOUND");
				return;
			}

			Log("GiftCards Result Recieved: GIFT CARDS FOUND!");

			RecievedGiftCards = result.Result.SendersIds;
			if(OnRecievedGiftCardsChanged != null) OnRecievedGiftCardsChanged();
		});
	}

	public void CheckGiftCardsRequests(){
		//Debug.Log("RON_______CheckGiftCardsRequests start Time:"+ DateTime.Now.ToLongTimeString());
		if (DAO.Instance.NO_INTERNET || !FB.IsLoggedIn) return;

		Context.LoadAsync<RequestGiftCardsStructure> (UserData.Instance.userID, (result) => {
			if (result.Result == null) {
				Log("Requested GiftCards Result Recieved: NO GIFT CARDS REQUESTS FOUND");
				return;
			}

			Log("GiftCards Result Recieved: GIFT CARDS REQUESTS FOUND!");

			RecievedGiftCardsRequests = result.Result.RequestersIds;
			if(OnRecievedGiftCardsChanged != null) OnRecievedGiftCardsChanged();
		});
	}

	public void SendGiftCards(List<string> selectedFriends){
		//Debug.Log("RON_______SendGiftCards start Time:"+ DateTime.Now.ToLongTimeString());
		foreach(string friendId in selectedFriends){
			string friend_id = friendId;

			// Loading friend giftcards data
			Context.LoadAsync<GiftCardsStructure> (friend_id, (result) => {

				if (result.Result == null) { 
					// Friend has no gift cards records, creating new
					Log("Friend: " + friend_id + ", have no giftcards records, creating new...");

					// Sending...
					gc = new GiftCardsStructure();
					gc.RecipientId = friend_id;
					gc.SendersIds.Add(UserData.Instance.userID);

					Context.SaveAsync(gc, (send_result) => {
						if (send_result.Exception == null){
							Log("GiftCard Successfully sent to: " + friend_id);
							if(friend_id == selectedFriends[selectedFriends.Count-1]){
								CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("gift-cards-sent"), 3f );
							}
						}else{
							Log(" Error Sending GiftCard to: " +friend_id+ " | " + send_result.Exception.Message);
						}
					});
					return;
				}

				Log("Friend: " + friend_id + ", have giftcards records, checking if there is cards from you...");
				GiftCardsStructure item = result.Result;

				if( !item.SendersIds.Contains(UserData.Instance.userID) ){
					Log("Friend: " + friend_id + ", have no giftcards from you! sending him a one...");
					item.SendersIds.Add(UserData.Instance.userID);

					// Sending...
					Context.SaveAsync(item, (send_result) => {
						if (send_result.Exception == null){
							Log("GiftCard Successfully sent to: " + friend_id);
							if(friend_id == selectedFriends[selectedFriends.Count-1]){
								CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("gift-cards-sent"), 3f );
								AppsFlyerManager._.ReportGiftCards("Send", selectedFriends.Count.ToString());
							}
						}else{
							Log(" Error Sending GiftCard to: " +friend_id+ " | " + send_result.Exception.Message);
						}
					});
				}else{
					Log("Friend: " + friend_id + ", already have giftcard from you! skiping...");
					if(friend_id == selectedFriends[selectedFriends.Count-1]){
						CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("gift-cards-sent"), 3f );
					}
				}


			});

		}

	}

	public void ClaimGiftCard(string friend_id){
		RecievedGiftCards.Remove (friend_id);
		//Debug.Log("RON_______ClaimGiftCard start Time:"+ DateTime.Now.ToLongTimeString());
		GiftCardsStructure gc = new GiftCardsStructure ();
		gc.RecipientId = UserData.Instance.userID;
		gc.SendersIds = RecievedGiftCards;

		if (RecievedGiftCards.Count > 1) {
			Context.SaveAsync (gc, (send_result) => {
				if (send_result.Exception == null) {
					DAO.TotalGiftCardsCollected++;
					DAO.TotalGiftCardsRecieved ++;
					CardsScrn_Actions.instance.UpdateGiftCardsCount();
					Log ("GiftCard from:  " + friend_id + " successfully claimed");
					AppsFlyerManager._.ReportGiftCards("Claim", "1");
					CrossSceneUIHandler.Instance.ShowNotification(DAO.Language.GetString("gift-card-claimed"), 3f);
					if(OnRecievedGiftCardsChanged != null) OnRecievedGiftCardsChanged();
					if(OnGiftCardsClaimed != null) OnGiftCardsClaimed();
				} else {
					Log (" Error claiming GiftCard from: " + friend_id + " | " + send_result.Exception.Message);
				}
			});
		} else {
			Context.DeleteAsync (gc, (send_result) => {
				if (send_result.Exception == null) {
					DAO.TotalGiftCardsCollected++;
					DAO.TotalGiftCardsRecieved ++;
					CardsScrn_Actions.instance.UpdateGiftCardsCount();
					Log ("GiftCard from:  " + friend_id + " successfully claimed");
					CrossSceneUIHandler.Instance.ShowNotification(DAO.Language.GetString("gift-card-claimed"), 3f);
					if(OnRecievedGiftCardsChanged != null) OnRecievedGiftCardsChanged();
					if(OnGiftCardsClaimed != null) OnGiftCardsClaimed();
				} else {
					Log (" Error claiming GiftCard from: " + friend_id + " | " + send_result.Exception.Message);
				}
			});
		}


	}


	public void RemoveGiftCardRequest(string friend_id){
		RecievedGiftCardsRequests.Remove (friend_id);
		//Debug.Log("RON_______RemoveGiftCardRequest start Time:"+ DateTime.Now.ToLongTimeString());
		RequestGiftCardsStructure gcr = new RequestGiftCardsStructure ();
		gcr.RecieverId = UserData.Instance.userID;
		gcr.RequestersIds = RecievedGiftCardsRequests;

		if (RecievedGiftCardsRequests.Count > 1) {
			Context.SaveAsync (gcr, (send_result) => {
				if (send_result.Exception == null) {
					Log ("GiftCard Request from:  " + friend_id + " successfully removed");
					if(OnRecievedGiftCardsChanged != null) OnRecievedGiftCardsChanged();
				} else {
					Log (" Error removing GiftCard request: " + friend_id + " | " + send_result.Exception.Message);
				}
			});
		} else {
			Context.DeleteAsync (gcr, (send_result) => {
				if (send_result.Exception == null) {
					Log ("GiftCard request from:  " + friend_id + " successfully removed");
					if(OnRecievedGiftCardsChanged != null) OnRecievedGiftCardsChanged();
				} else {
					Log (" Error removing GiftCard request from: " + friend_id + " | " + send_result.Exception.Message);
				}
			});
		}


	}



	public void ClaimAllGiftCards(){
		//Debug.Log("RON_______ClaimAllGiftCards start Time:"+ DateTime.Now.ToLongTimeString());
		GiftCardsStructure gc = new GiftCardsStructure ();
		gc.RecipientId = UserData.Instance.userID;

		Context.DeleteAsync (gc, (send_result) => {
			if (send_result.Exception == null) {
				DAO.TotalGiftCardsCollected += RecievedGiftCards.Count;
				DAO.TotalGiftCardsRecieved += RecievedGiftCards.Count;
				CardsScrn_Actions.instance.UpdateGiftCardsCount();
				RecievedGiftCards.Clear();
				Log ("All Gift Cards successfully claimed");
				//AppsFlyerManager._.ReportGiftCards("Claim", RecievedGiftCards.Count.ToString());
				CrossSceneUIHandler.Instance.ShowNotification(DAO.Language.GetString("all-gift-cards-claimed"), 3f);
				if(OnRecievedGiftCardsChanged != null) OnRecievedGiftCardsChanged();
				if(OnGiftCardsClaimed != null) OnGiftCardsClaimed();
			} else {
				Log (" Error claiming GiftCards | " + send_result.Exception.Message);
			}
		});
	}

	public void RequestGiftCards(List<string> selectedFriends){
		Debug.Log("RequestGiftCards start Time:"+ DateTime.Now.ToLongTimeString());
		foreach(string friendId in selectedFriends){
			string friend_id = friendId;

			// Loading friend giftcards data
			Context.LoadAsync<RequestGiftCardsStructure> (friend_id, (result) => {

				if (result.Result == null) { 
					// Friend has no gift cards records, creating new
					Log("Friend: " + friend_id + ", have no requests records, creating new...");

					// Sending...
					gcr = new RequestGiftCardsStructure();
					gcr.RecieverId = friend_id;
					gcr.RequestersIds.Add(UserData.Instance.userID);

					Context.SaveAsync(gcr, (send_result) => {
						if (send_result.Exception == null){
							Log("GiftCard Successfully Requested from: " + friend_id);
							if(friend_id == selectedFriends[selectedFriends.Count-1]){
								CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("gift-cards-requested"), 3f );
								AppsFlyerManager._.ReportGiftCards("Ask for", selectedFriends.Count.ToString());
							}
						}else{
							Log(" Error Requesting GiftCard from: " +friend_id+ " | " + send_result.Exception.Message);
						}
					});
					return;
				}

				Log("Friend: " + friend_id + ", have giftcards request records, checking if there is request from you...");
				RequestGiftCardsStructure item = result.Result;

				if( !item.RequestersIds.Contains(UserData.Instance.userID) ){
					Log("Friend: " + friend_id + ", have no giftcards request from you! sending him a one...");
					item.RequestersIds.Add(UserData.Instance.userID);

					// Sending...
					Context.SaveAsync(item, (send_result) => {
						if (send_result.Exception == null){
							Log("GiftCard Successfully requested from: " + friend_id);
							if(friend_id == selectedFriends[selectedFriends.Count-1]){
								CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("gift-cards-requested"), 3f );
								AppsFlyerManager._.ReportGiftCards("Ask for", selectedFriends.Count.ToString());
							}
						}else{
							Log(" Error Requesting GiftCard from: " +friend_id+ " | " + send_result.Exception.Message);
						}
					});
				}else{
					Log("Friend: " + friend_id + ", already have giftcard request from you! skiping...");
					if(friend_id == selectedFriends[selectedFriends.Count-1]){
						CrossSceneUIHandler.Instance.ShowNotification( DAO.Language.GetString("gift-cards-requested"), 3f );
					}
				}

			});
		}

	}

	public void SaveIapTransaction(IapTransactionStructure Iap){
		//Debug.Log("RON_______SaveOldProgressData start Time:"+ DateTime.Now.ToLongTimeString());
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("DDB SaveIapTransaction Start");
		try {
			
			Context.SaveAsync(Iap, (result) => {
				if (result.Exception == null){
					Debug.Log("RON___________________SaveIapTransaction");
					//Log(" UserData Saved ");
				}else{
					Debug.Log("RON___________________Error SaveIapTransaction" + result.Exception.Message + " | --- | " + result.Exception.StackTrace  );
					//Log(" Error Saving UserData: " + result.Exception.Message + " | --- | " + result.Exception.StackTrace); 

				}
			});	

		}catch (Exception ex) {
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("DDB Saving IapTransaction failed:"+ex.Message);
		}
	}

	public void SaveWhaleTracker(WhaleTrackerStructure whaleTracker){
		Debug.Log("RON_______SaveWhaleTracker start Time:"+ DateTime.Now.ToLongTimeString());
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("DDB SaveWhaleTracker Start");
		try {

			Context.SaveAsync(whaleTracker, (result) => {
				if (result.Exception == null){
					Debug.Log("RON___________________SaveWhaleTracker");
					//Log(" UserData Saved ");
				}else{
					Debug.Log("RON___________________Error SaveWhaleTracker" + result.Exception.Message + " | --- | " + result.Exception.StackTrace  );
					//Log(" Error Saving UserData: " + result.Exception.Message + " | --- | " + result.Exception.StackTrace); 

				}
			});	

		}catch (Exception ex) {
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("DDB Saving SaveWhaleTracker failed:"+ex.Message);
		}
	}

	public void QueryIapTransaction(Dictionary<string,AttributeValue> lastKeyEvaluated)
	{


		QueryRequest queryRequest = new QueryRequest
		{
			TableName = "ars_bd_iap_transaction",
			IndexName = "sDate-sDateTime-index",
			ScanIndexForward = true,
			Limit = 100
		};


		String keyConditionExpression;
		Dictionary<string, AttributeValue> expressionAttributeValues = new Dictionary<string, AttributeValue>();
		//keyConditionExpression = "CreateDate = :v_date and ";
		keyConditionExpression = "sDate = :v_date and sDateTime BETWEEN :v_fromDate and :v_todate";
		//keyConditionExpression = "sDate = :v_date";
		expressionAttributeValues.Add(":v_date", new AttributeValue { S = "20160803" });
		expressionAttributeValues.Add(":v_fromDate", new AttributeValue { S = "20160803130000" });
		expressionAttributeValues.Add(":v_todate", new AttributeValue { S = "20160803140000" });
		queryRequest.Select = "ALL_PROJECTED_ATTRIBUTES";

		queryRequest.KeyConditionExpression = keyConditionExpression;
		queryRequest.ExpressionAttributeValues = expressionAttributeValues;
		Client.QueryAsync(queryRequest,(result)=>{
			foreach (Dictionary<string, AttributeValue> item
				in result.Response.Items)
			{
				//Debug.Log("Ron_________________item found");
				PrintIapTransaction(item);
				//PrintItem(item);
			}
			lastKeyEvaluated = result.Response.LastEvaluatedKey;
			if(lastKeyEvaluated != null && lastKeyEvaluated.Count != 0)
			{
				QueryIapTransaction(lastKeyEvaluated);
			}

		});
	}

	private void PrintIapTransaction(Dictionary<string, AttributeValue> attributeList)
	{
		string text="";
		AttributeValue value;
		//UnityEngine.Debug.Log ("RON________________SACN RESULT:");
		if (attributeList.TryGetValue ("user_id", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("date", out value)) {
			text += value.N+",";
		} else
		text += ",";
		if (attributeList.TryGetValue ("sDate", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("sDateTime", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("price", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("sku", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("storeType", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("isFtd", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("ftd", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("coinsAmount", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("playersAmount", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("totalMeters", out value)) {
			text += value.N+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("isValid", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("email", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("firstName", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("lastName", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("currency", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("receiptJson", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("receipt", out value)) {
			text += value.S+",";
		} else
			text += ",";
		if (attributeList.TryGetValue ("numberOfIAP", out value)) {
			text += value.S+",";
		}
		else
			text += ",";
		Debug.Log ("RON__________________________transaction item:" + text);
		//outStream.WriteLine(text);
		//UnityEngine.Debug.Log("RON________________SACN RESULT:" +text);
	}
		
	public void UpdateWhaleTracker(float addAmount){
		//Debug.Log("RON_______ReadData start Time:"+ DateTime.Now.ToLongTimeString());
		Debug.Log("RON_______UpdateWhaleTracker start Time:"+ addAmount.ToString());
		if (DAO.Instance.NO_INTERNET || string.IsNullOrEmpty(DAO.Ftd) || string.IsNullOrEmpty(DAO.whaleID)) {
			return;
		}

		Context.LoadAsync<WhaleTrackerStructure>(DAO.whaleID, (result) => {
			if(result.Result == null){
				if(DAO.IAPPoints > 1 && whaleTrackerIndex < 3){ //temp for debugging:
					Debug.Log("RON_______UpdateWhaleTracker empty item:"+DateTime.Now.ToLongTimeString());
					whaleTrackerIndex ++;
					Debug.Log( "RON_____________ DDB - if(whaleTrackerIndex < 3) - index: " + whaleTrackerIndex);
					StartCoroutine(WhaleTrackerTryAgain(addAmount));
					return;
				}
				else
				{
					whaleTrackerIndex = 0;
				}
			}
			WhaleTrackerStructure item = null;
			if (result.Result != null)
				item = result.Result;

			if (item != null)
			{
				Debug.Log("RON_______UpdateWhaleTracker full item: revenue"+item.revenue.ToString());
				item.lastLoginDate = DateTime.Now.ToString("yyyyMMdd");
				item.coinsAmount = DAO.TotalCoinsCollected;
				item.playersAmount = DAO.NumOfPurchasedPlayers.ToString ();
				if ((!string.IsNullOrEmpty(DAO.Ftd)) &&  string.Compare(DAO.Ftd,item.ftd) < 0)
					item.ftd = DAO.Ftd;
				if (DAO.maxRunID > item.maxRunID)
					item.maxRunID = DAO.maxRunID;
				item.totalMeters = (DAO.BILevelUp * 1000) + DAO.BIDistance;
				if (!string.IsNullOrEmpty(DAO.Email))
					item.email = DAO.Email;
				if (!string.IsNullOrEmpty(DAO.FirstName))
					item.firstName = DAO.FirstName;
				if (!string.IsNullOrEmpty(DAO.LastName))
					item.lastName = DAO.LastName;
				item.revenue = item.revenue + addAmount;
				//item.numberOfIAP = DAO.IAPPoints;
				if (!String.IsNullOrEmpty(DAO.currency))
				{
					if (!String.IsNullOrEmpty(item.currency) && item.currency != DAO.currency)
						item.currency = item.currency + "|" + DAO.currency;
					else
						item.currency = DAO.currency;
				}
				if (FB.IsLoggedIn) {
					item.isFBConnected = 1;
					item.user_id = UserData.Instance.userID;
				} 
				SaveWhaleTracker(item);
			}
			else
			{
				Debug.Log("RON_______UpdateWhaleTracker empty item:");
				WhaleTrackerStructure whaleTracker = new WhaleTrackerStructure ();
				whaleTracker.revenue = whaleTracker.revenue + addAmount;
				whaleTracker.numberOfIAP =  0;
				if (!String.IsNullOrEmpty(DAO.currency))
					whaleTracker.currency = DAO.currency;
				DDB._.SaveWhaleTracker (whaleTracker);
			}


		}, null);

	}

	IEnumerator WhaleTrackerTryAgain(float addAmount){  
		yield return new WaitForSeconds (0.5f);
		UpdateWhaleTracker (addAmount);
	}

	/*
	public void ReportErrorLog(){
		try {
			Debug.Log("RON_______ReportErrorLog start Time:"+ DateTime.Now.ToLongTimeString());
			ErrorLog errorLog= new ErrorLog();
			if (UnityInitializer.Instance != null)
				errorLog.AddError(UnityInitializer.Instance.getMonitorLog());
				//File.WriteAllText(Application.persistentDataPath+"/Test.txt",UnityInitializer.Instance.getMonitorLog());
			Context.SaveAsync(errorLog, (send_result) => {
				Debug.Log("RON_______ReportErrorLog save Time:"+ DateTime.Now.ToLongTimeString());
				if (send_result.Exception == null){
					Log("ReportErrorLog Successfully Updated");
					UnityInitializer.Instance.CleanMonitorLog();
				}else{
					Debug.Log("RON_______ReportErrorLog Error!:"+ DateTime.Now.ToLongTimeString());
					Log(" Error Updating ReportErrorLog Status | " + send_result.Exception.Message);
					UnityInitializer.Instance.CleanMonitorLog();
				}
			});
		}catch (Exception ex) {
			Debug.Log("RON_______ReportErrorLog Error!:"+ DateTime.Now.ToLongTimeString() + ex.Message);
			if (UnityInitializer.Instance != null)
				UnityInitializer.Instance.addToMonitorLog ("DDB ReportErrorLog failed:" + ex.Message);
		}
	}
	*/




}
