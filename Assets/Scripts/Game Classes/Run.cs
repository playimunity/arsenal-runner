using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class Run : MonoBehaviour {

	public static Run Instance;
	public string runName = "Test Run";

	public RunGoal runGoal;
	public List<WorldSectionAbstract> sections;
	public bool running = false;

	WaitForSeconds pause_second;
	
	void Awake(){
		running = false;
		runGoal = new RunGoal ();
		Instance = this;
		ParseRun ();
	}

	void Start(){
		pause_second = new WaitForSeconds (1f);
		StartCoroutine ( updateRunTime () );
	}


	string[] sectionsTypes = new string[]{"str" , "aly" , "crs"};
	//int randomSectionType;
	void ParseRun(){
		JSONNode run = DAO.TutorialRun;

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {

			if (GameManager.CurrentRun.NumOfFails >= 2) {
				run = DAO.getEasyCupRunByIndex (GameManager.CurrentRun.id);
				//Debug.Log ("alon__________ParseRun() - NumOfFails >= 2 - run = DAO.getEasyCupRunByIndex");
			} else {
				run = DAO.getCupRunByIndex (GameManager.CurrentRun.id);
				//Debug.Log ("alon__________ParseRun() - NumOfFails < 2 - run = DAO.getCupRunByIndex");
			}

			runGoal.goal = RunGoal.GetGoalTypeByGoalString (run ["goal"].Value);
			runGoal.goalDescription = RunGoal.GetGoalDescriptionByRunGoalType (runGoal.goal);

			// To Match Fails

		}else if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			run = DAO.TutorialRun;
			runGoal.goal = RunGoal.GetGoalTypeByGoalString (run ["goal"].Value);
			runGoal.goalDescription = RunGoal.GetGoalDescriptionByRunGoalType (runGoal.goal);
		} else if (GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
			run = DAO.TestRun;
			runGoal.goal = RunGoal.GetGoalTypeByGoalString (run ["goal"].Value);
			runGoal.goalDescription = RunGoal.GetGoalDescriptionByRunGoalType (runGoal.goal);
		}





		sections = new List<WorldSectionAbstract>();
		for (int i=0; i < run["run"].Count; i++) {
			sections.Add( new WorldSectionAbstract( run["run"][i] ) );
		}

        //TODO: Add Logic - retrieve region and choose preend and end sections individually.
        var lastSection = sections[sections.Count - 1];
        var prefix = lastSection.id.Substring(0, 2);
		//string areaType = lastSection.id.Substring(0, 2);
		sections.Add( new WorldSectionAbstract( prefix+"preend" ) );

		switch (prefix) {
		case "os":
			{
				sections.Add( new WorldSectionAbstract( prefix+"ex" ) );
				sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
				sections.Add( new WorldSectionAbstract( prefix+"end" ) );
				Debug.Log ("alon_________ create end sc sections - hr type");
				break;
			}
		case "hr":
		case "sp":
			{
				sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
				sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
				sections.Add( new WorldSectionAbstract( prefix+"end" ) );
				Debug.Log ("alon_________ create end sc sections - hr type");
				break;
			}
		case "ct":
		case "ms":
			{
				sections.Add( new WorldSectionAbstract( prefix+"ex2" ) );
				sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
				sections.Add( new WorldSectionAbstract( prefix+"end" ) );
				Debug.Log ("alon_________ create end sc sections - ct type");
				break;
			}
		case "tb":
			{
				sections.Add( new WorldSectionAbstract( prefix+"ex3" ) );
				sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
				sections.Add( new WorldSectionAbstract( prefix+"end" ) );
				Debug.Log ("alon_________ create end sc sections - tb type");
				break;
			}
		default:
			{
//				for (int i = 0; i < 2; i++)
//				{
//					randomSectionType = Random.Range(0,sectionsTypes.Length);
//					sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
//				}
				sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
				sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
				sections.Add( new WorldSectionAbstract( prefix+"end" ) );
				Debug.Log ("alon_________ create end sc sections - other type");
				break;
			}
		}
			

        for (int i = 0; i < 7; i++)
        {
			//sections.Add( new WorldSectionAbstract( sections[Random.Range(sections.Count-5,sections.Count-2)].id ) );
			//randomSectionType = Random.Range(0,sectionsTypes.Length);
			sections.Add( new WorldSectionAbstract( prefix+sectionsTypes[0] ) );
        }
        
	}

	IEnumerator updateRunTime(){
		
		yield return pause_second;

		if(running) runGoal.playTime++;

		StartCoroutine (updateRunTime());

	}


	public GameObject secterArea;
	Vector3 firstSectionDirection;
	//Vector3 lastSectionDirection;

	public void MoveSectionsForwards(int numberOfSections){


//		for (int i = 0; i < sections.Count; i++) {
//			if (sections [i].ObjectOnScene != null) {
//				secterArea.transform.position = sections [i].ObjectOnScene.transform.position;
//				secterArea.transform.rotation = sections [i].ObjectOnScene.transform.rotation;
//				secterArea.SetActive (true);
//				break;
//			}
//		}

		firstSectionDirection = sections [RunManager.LastSectionNumber - 1].ObjectOnScene.transform.forward;
		//lastSectionDirection = sections [RunManager.LastSectionNumber - 1].ObjectOnScene.transform.forward;

		secterArea.transform.position = sections [RunManager.LastSectionNumber-1].ObjectOnScene.transform.position;
		secterArea.transform.rotation = sections [RunManager.LastSectionNumber-1].ObjectOnScene.transform.rotation;
		secterArea.SetActive (true);

//		for (int i = 0; i < sections.Count; i++) {
//
//		}

		foreach (WorldSectionAbstract section in sections) {
			if (section.ObjectOnScene != null) {
				section.ObjectOnScene.transform.position += firstSectionDirection * 30 * numberOfSections;
			}

			if (section.ObjectOnSceneCopy != null && section.ObjectOnSceneCopy != section.ObjectOnScene) {
				section.ObjectOnSceneCopy.transform.position += firstSectionDirection * 30 * numberOfSections;
			}
		}

		//CupMapBuilder.secretAreaPosOffset = true;
		//CupMapBuilder.Instance.SecretAreaNewSectionsPosition (numberOfSections);
	}



}
