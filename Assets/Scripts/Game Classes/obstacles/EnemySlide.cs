﻿using UnityEngine;
using System.Collections;

public class EnemySlide : MonoBehaviour {

	Obstacle.ObstacleTypes type = Obstacle.ObstacleTypes.Enemy_Slide;

	public static EnemySlide instanse;

	public Animator animator;

	bool moveTriggered = false;
	bool colided = false;
	bool triggered = false;

	public float moveSpeed;
	public float maxMoveSpeed;

	public GameObject ball;

	Quaternion enemyRotation = Quaternion.Euler(0,0,0);
	Quaternion enemyRotationFliped = Quaternion.Euler(0,180,0);


	void Awake () {
		instanse = this;
	}


	void Start(){
		if (transform.localPosition.x > 15f) {
			transform.localRotation = enemyRotationFliped;
		} else {
			transform.localRotation = enemyRotation;
		}
	}
	

	void Update () {
		if (moveTriggered) {
			transform.Translate( Vector3.right * moveSpeed * Time.deltaTime );
		}
	}

	
	void OnTriggerEnter(Collider col){
		if (triggered)
			return;

		if (col.gameObject.tag == "Player") {
			triggered = true;
			StartSlide ();
		}
	}


	public void StartSlide () {

		moveTriggered = true;
		StartCoroutine (SpeedUp());
		animator.SetTrigger ("run");
	}


	void OnCollisionEnter(Collision col) {
		if (colided)
			return;

		if (col.gameObject.tag == "Player" && !colided) {
			colided = true;
			GetComponent<Collider> ().enabled = false;
			GetComponent<Rigidbody> ().isKinematic = true;
			ball.SetActive(true);
			//PlayerController.instance.hardDamage = true;
			PlayerController.instance.Damage(1);
		}
	}


	public void StopSlide () {
		StartCoroutine (SlowDown());
	}

	static WaitForEndOfFrame _weof;
	static WaitForEndOfFrame weof{
		get{ 
			if (_weof == null)
				_weof = new WaitForEndOfFrame ();

			return _weof;
		}
	}

	IEnumerator SpeedUp(){
		while (moveSpeed<maxMoveSpeed) {
			moveSpeed = moveSpeed+12*Time.deltaTime;
			yield return weof;
		}
		moveSpeed = maxMoveSpeed;
	}


	IEnumerator SlowDown(){
		while (moveSpeed>0) {
			moveSpeed = moveSpeed-12*Time.deltaTime;
			yield return weof;
		}
		moveSpeed = 0;
		moveTriggered = false;
	}




}
