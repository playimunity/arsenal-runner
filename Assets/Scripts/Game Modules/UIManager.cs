﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Amazon;

public class UIManager : MonoBehaviour {
	private static Dictionary<GameManager.GameState, int> SCENE_ORDER = new Dictionary<GameManager.GameState, int>()
	{
		{GameManager.GameState.PRELOADER, 0},
		//{GameManager.GameState.MAIN_MENU, 1},
		{GameManager.GameState.LOCKER_ROOM, 1},
		{GameManager.GameState.CUP_RUN, 2},
		{GameManager.GameState.INFINITY_RUN, 3},
		{GameManager.GameState.TUTORIAL_RUN, 4},
		{GameManager.GameState.TEST_RUN, 4}
	}; 
	public static AsyncOperation asyncLeveLoad;
	public static event Action OnChangeScene;
	public static bool isManagersLoaded;
	public static bool isGAListenersAttached;
	public static bool isHeyzapInitialized;
	public WaitForSeconds wait_0_5_s = new WaitForSeconds (0.5f);
	public static void SwitchStae(GameManager.GameState state){
        
		switch(state){

			case GameManager.GameState.CUP_RUN : {
				if(SceneManager.GetActiveScene().buildIndex != SCENE_ORDER[GameManager.GameState.PRELOADER] ) SceneTransitionContainer.Instance.showTransition();
				if(OnChangeScene != null) OnChangeScene();
				asyncLeveLoad = SceneManager.LoadSceneAsync(SCENE_ORDER[state]);
				break;
			}

		case GameManager.GameState.INFINITY_RUN : {
				SceneTransitionContainer.Instance.showTransition();
			if(OnChangeScene != null) OnChangeScene();
				asyncLeveLoad = SceneManager.LoadSceneAsync(SCENE_ORDER[state]);
				break;
			}

		case GameManager.GameState.TUTORIAL_RUN : {
				GameAnalyticsWrapper.DesignEvent (GameAnalyticsWrapper.FLOW_STEP.TUTORIAL_STARTED);
				if (UnityInitializer.Instance != null)
					UnityInitializer.Instance.StopTracking ();
				if (Tutorial.Instance.tutRetryed) {	
					SceneTransitionContainer.Instance.showTransition ();
					if (OnChangeScene != null)
						OnChangeScene ();
					asyncLeveLoad = Application.LoadLevelAsync (SCENE_ORDER [state]);
				} else {
					GameManager.Instance.StartCoroutine (LoadManagersAndInitPlugins (state));
				}
			break;
		}

		case GameManager.GameState.TEST_RUN : {
				SceneTransitionContainer.Instance.showTransition();
			if(OnChangeScene != null) OnChangeScene();
				asyncLeveLoad = SceneManager.LoadSceneAsync(SCENE_ORDER[state]);
			break;
		}

//		case GameManager.GameState.MAIN_MENU : {
//				if (UnityInitializer.Instance != null)
//					UnityInitializer.Instance.StopTracking ();
//				GameManager.Instance.StartCoroutine (LoadManagersAndInitPlugins (state));
//				if(Application.loadedLevel != SCENE_ORDER[GameManager.GameState.PRELOADER] ) SceneTransitionContainer.Instance.showTransition();
//			if(OnChangeScene != null) OnChangeScene();
//			asyncLeveLoad = Application.LoadLevelAsync(SCENE_ORDER[state]);
//				break;
//			}

		case GameManager.GameState.LOCKER_ROOM:
			{
				//GameManager.Instance.StartCoroutine (LoadManagersAndInitPlugins (state));
				if (SceneManager.GetActiveScene ().buildIndex != SCENE_ORDER [GameManager.GameState.PRELOADER])
					SceneTransitionContainer.Instance.showTransition ();
				if (OnChangeScene != null)
					OnChangeScene ();
				//asyncLeveLoad = SceneManager.LoadSceneAsync (SCENE_ORDER [state]);
				GameManager.Instance.StartCoroutine (LoadManagersAndInitPlugins (state));
				break;
			}

		case GameManager.GameState.GAME_OVER : {
			SceneUIScreens.Instance.GameoverScreen.SetActive(true);
			break;
		}

		case GameManager.GameState.PRELOADER : {
				//SceneTransitionContainer.Instance.showTransition();
				if(OnChangeScene != null) OnChangeScene();
				asyncLeveLoad = SceneManager.LoadSceneAsync(SCENE_ORDER[state]);
			break;
		}


		}
		if (asyncLeveLoad != null)
			asyncLeveLoad.allowSceneActivation = false;
		else {
			//Debug.Log ("****ASYNC_LOAD_IS_NULL!!!!!**** State:" + state);

		}
	}

	void Start(){
//		SCENE_ORDER.Add(GameManager.GameState.PRELOADER, 0);
//		SCENE_ORDER.Add(GameManager.GameState.MAIN_MENU, 1);
//		SCENE_ORDER.Add(GameManager.GameState.LOCKER_ROOM, 2);
//		SCENE_ORDER.Add(GameManager.GameState.CUP_RUN, 3);
//		SCENE_ORDER.Add(GameManager.GameState.INFINITY_RUN, 4);
//		SCENE_ORDER.Add(GameManager.GameState.TUTORIAL_RUN, 5);
//		SCENE_ORDER.Add(GameManager.GameState.TEST_RUN, 5);

	}
	public static IEnumerator LoadManagersAndInitPlugins (GameManager.GameState gState)
	{
		GameObject subManagers = new GameObject ();
		if (!isHeyzapInitialized) {
			print ("************!!!!!!!**********!!!!!!!!**********");
			print ("before heyzap init");
			if (DevicesManager.AreAdsAllowed ()) {
				HeyzapWrapper.Init ();
				isHeyzapInitialized = true;
				print ("************!!!!!!!**********!!!!!!!!**********");
				print ("after heyzap init");
			}
		}
		switch (gState)
		{
		case GameManager.GameState.LOCKER_ROOM:
			//HeyzapWrapper.Init ();
			if (SceneManager.GetActiveScene().buildIndex != SCENE_ORDER[GameManager.GameState.PRELOADER])
			{
				SceneTransitionContainer.Instance.showTransition();
			}
			if (!isManagersLoaded)
			{
				yield return new WaitUntil(() => subManagers = Instantiate(Resources.Load("SubManagers")) as GameObject);
				Preloader.Instance.FillLoadingBar (9,"Submanagers");
				subManagers.transform.SetParent (GameObject.Find ("Managers").transform);
				isManagersLoaded = true;
			}
			//Ads._.OnMainMenu ();
			if(OnChangeScene != null) OnChangeScene();
			asyncLeveLoad = SceneManager.LoadSceneAsync(SCENE_ORDER[gState]);
			break;
		case GameManager.GameState.TUTORIAL_RUN:
			if (SceneManager.GetActiveScene().buildIndex != SCENE_ORDER [GameManager.GameState.PRELOADER]) {
//				if (!isManagersLoaded)
//					SceneTransitionContainer.Instance.transitionObject.gameObject.SetActive(true);
//				else
				SceneTransitionContainer.Instance.showTransition ();

			}

			if(OnChangeScene != null) OnChangeScene();
			asyncLeveLoad = SceneManager.LoadSceneAsync(SCENE_ORDER[gState]);
			if (!isManagersLoaded) {
//				yield return new WaitForSeconds (0.2f);

				yield return new WaitUntil(() =>subManagers = Instantiate(Resources.Load("SubManagers")) as GameObject);
				Preloader.Instance.FillLoadingBar (9,"Submanagers");
				isManagersLoaded = true;
				subManagers.transform.SetParent (GameObject.Find ("Managers").transform);
			}
			break;
		default:
			break;
		}
		if (!isGAListenersAttached) {
			print ("Attaching listener to X Button");
			CrossSceneUIHandler.Instance.GAXBTN.onClick.AddListener (delegate {

				Ads._.OnAdClosed ();	
			});
			print ("Attaching listener to Negative Button");
			CrossSceneUIHandler.Instance.GANegativeBTN.onClick.AddListener (delegate {
			
				Ads._.OnAdClosed ();	
			});
			print ("Attaching listener to Positive Button");
			CrossSceneUIHandler.Instance.GAPositiveBTN.onClick.AddListener (delegate {
				Ads._.OnAdClicked ();	
			});
			isGAListenersAttached = true;
		}
	}



	void OnLevelWasLoaded(int level) {

		switch ( GameManager.getGameStateBySceneIndex(level) ) {
			case GameManager.GameState.LOCKER_ROOM :{
				StartCoroutine(HandleLockerRoomSubStates());
				break;
			} 
//			case GameManager.GameState.MAIN_MENU:{
//				HandleMainMenuSubStates ();
//				//CrossSceneUIHandler.Instance.showCoinsCounter ();
//				break;
//			}
			default :{
				//CrossSceneUIHandler.Instance.showCoinsCounter ();
				break;		
			}

		}
        HeaderManager.instance.State = GameManager.curentGameState;
	}

	IEnumerator HandleLockerRoomSubStates ()
	{
		yield return wait_0_5_s;
		if (GameManager.curentSubGameState != GameManager.GameSubState.CUPS_SCREEN && GameManager.curentSubGameState != GameManager.GameSubState.RUNS_SCREEN) {
			//CrossSceneUIHandler.Instance.showCoinsCounter ();
		}

		if (GameManager.curentSubGameState == GameManager.GameSubState.CUPS_SCREEN) {
			MainScreenUiManager.instance.CupScrnAnimToggle (true, true);
			PlayerChooseManager.instance.tutPopupsDisabled = true;
//			CrossSceneUIHandler.Instance.hideCoinsCounter ();
		}

		if (GameManager.curentSubGameState == GameManager.GameSubState.LOW_ENERGY) {
			MainScreenUiManager.instance.LowEnergyPopup (true);
			PlayerChooseManager.instance.tutPopupsDisabled = true;
		}

		if (GameManager.curentSubGameState == GameManager.GameSubState.RUNS_SCREEN) {
			if (GameManager.ActiveCupCompleted) {
				CupsManager.Instance.SetCupScreenRuns (GameManager.ActiveCompletedCup);
			} else {
				CupsManager.Instance.SetCupScreenRuns (GameManager.ActiveCup);
			}

			MainScreenUiManager.instance.CupScrnAnimToggle (true,true);
			MainScreenUiManager.instance.RunsScrnAnimToggle (true,true);
			PlayerChooseManager.instance.tutPopupsDisabled = true;
//			Debug.Log ("alon_______________ UIManager - HandleLockerRoomSubStates() - GameSubState.RUNS_SCREEN");
//			CrossSceneUIHandler.Instance.hideCoinsCounter ();
		}
		if (GameManager.curentSubGameState == GameManager.GameSubState.PLAYER_PREVIEW) {
			PlayerChooseManager.instance.isFreePlayer = true;
		}
//		if (GameManager.curentSubGameState == GameManager.GameSubState.TUTORIAL_CONNECTED) {
//			// on locker room first time tutorial connected to facebook events:
//			LockerRoom_Actions.instance.TutorialConnectedAnim(true);
//			//LocalDataInterface.Instance.tutorialState = 1;
//		}

//		if (GameManager.curentSubGameState == GameManager.GameSubState.TUTORIAL_NOT_CONNECTED) {
//			// on locker room first time tutorial not connected to facebook events:
//			LockerRoom_Actions.instance.TutorialNotConnectedAnim(true);
//			//LocalDataInterface.Instance.tutorialState = 1;
//		}

//		if (GameManager.curentSubGameState == GameManager.GameSubState.TUTORIAL_FIRST_RUN) {
//			// on locker room first time tutorial RUN:
//			//LocalDataInterface.Instance.tutorialState = 1;
//		}

//		if (GameManager.curentSubGameState == GameManager.GameSubState.TUTORIAL_FIRST_RUN_SKIP) {
//			// on locker room first time tutorial RUN or SKIP:
//			//LocalDataInterface.Instance.tutorialState = 1;
//		}


		GameManager.restetGameSubState();
	}

	IEnumerator HideCoins(){
		yield return new WaitForFixedUpdate ();
		CrossSceneUIHandler.Instance.hideCoinsCounter ();

	}


	void HandleMainMenuSubStates(){

		if (GameManager.curentSubGameState == GameManager.GameSubState.MENU_TUTORIAL) {
			MainMenu_Actions.instance.MainMenuTutorialAnimIn ();
		}else{
			MainMenu_Actions.instance.MainMenuAnimIn ();
		}


		GameManager.restetGameSubState();
	}


}
