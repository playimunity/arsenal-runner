﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CupLooseScreen_Actions : MonoBehaviour {

	public static CupLooseScreen_Actions Instance;
	
	public Text CupName;
	public Text CupPrize;
	
	void Awake(){
		Instance = this;
	}
	
	public void Show(){
		CupPrize.text = GameManager.ActiveCup.prize.ToString();
		CupName.text = GameManager.ActiveCup.name;

		CrossSceneUIHandler.Instance.showCupLooseScreen ();
		
	}
	
	public void Hide(){
		CrossSceneUIHandler.Instance.hideCupLooseScreen ();
	}
	

}
