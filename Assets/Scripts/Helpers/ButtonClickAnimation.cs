﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonClickAnimation : MonoBehaviour {

	public bool isAutoListener = true;
	public bool bounce;
	bool bounceStarted = false;

	Vector3 min;
	Vector3 max;
	Vector3 nor;
	Vector3 tar;
	//Vector3 bounceScale;

	WaitForSeconds waitForFPS;
	WaitForSeconds waitForSec;

	void Awake () {
		min = max = nor = transform.localScale;
		min *= 0.7f;
		max *= 1.1f;

		waitForFPS = new WaitForSeconds (0.02f);
		waitForSec = new WaitForSeconds (1.5f);
		if (isAutoListener)
		GetComponent<Button> ().onClick.AddListener (() => {
			OnClicked();
		});
	}

	void Start(){
		if (bounce) {
			BounceToggle (true);
		}
	}

	public void BounceToggle(bool on){
		if (on && !bounceStarted) {
			bounceStarted = true;
			bounce = true;
			StartCoroutine ("Bounce");
			//Debug.Log ("alon___ button sould bounce");
		} else if (!on && bounceStarted) {
			bounceStarted = false;
			bounce = false;
			StopCoroutine ("Bounce");
			//Debug.Log ("alon___ button sould stop bouncing!");
		}
	}

	public void OnClicked(){
		//GetComponent<Button> ().interactable = false;
		StopAllCoroutines ();
		StartCoroutine( Animate() );
		bounce = false;
	}

	IEnumerator Animate(){

		// ------- START

		float t = 0f;
//		while (t <= 1f) {
//			t += Time.deltaTime/0.3f;
//			transform.localScale = Vector3.Lerp(transform.localScale, min, Mathf.SmoothStep(0f, 1f, t) );
//			yield return waitForFrame;
//		}

		while(transform.localScale.x > min.x + 0.01f){
			transform.localScale = Vector3.Lerp( transform.localScale, min, 15f * Time.deltaTime);
			yield return null;
		}


		// ------- PHASE 2

		t = 0f;
		transform.localScale = min; 


//		while (t <= 1f) {
//			t += Time.deltaTime/0.3f;
//			transform.localScale = Vector3.Lerp(transform.localScale, max, Mathf.SmoothStep(0f, 1f, t) );
//			yield return waitForFrame;
//		}

		while(transform.localScale.x < max.x - 0.01f ){
			transform.localScale = Vector3.Lerp( transform.localScale, max, 15f * Time.deltaTime);
			yield return null;
		}	


		// ------- PHASE 3

		t = 0f;
		transform.localScale = max; 

//		
//		while (t <= 1f) {
//			t += Time.deltaTime/0.2f;
//			transform.localScale = Vector3.Lerp(transform.localScale, nor, Mathf.SmoothStep(0f, 1f, t) );
//			yield return waitForFrame;
//		}

		while(transform.localScale.x > nor.x + 0.01f){
			transform.localScale = Vector3.Lerp( transform.localScale, nor, 15f * Time.deltaTime);
			yield return null;
		}


		transform.localScale = nor; 

		// ------- END


	}


	float bounceFactor;
	float originalBounceFactor;

	IEnumerator Bounce(){
		bounceFactor = 1.03f;
		originalBounceFactor = 1.03f;
		while (bounceFactor >= 1) {
			//bounceScale.x *= bounceFactor;
			//bounceScale.y *= bounceFactor;
			//transform.localScale = bounceScale;

			//transform.localScale *= bounceFactor * Time.deltaTime;
			transform.localScale *= bounceFactor;
			bounceFactor /= 1.003f;
			//bounceFactor /= (1.01f * Time.deltaTime);
			yield return waitForFPS;
			//yield return null;
		}
		bounceFactor = 1f;

		while (bounceFactor <= originalBounceFactor) {
			transform.localScale /= bounceFactor;
			bounceFactor *= 1.003f;
			yield return waitForFPS;
		}
		bounceFactor = originalBounceFactor;
		transform.localScale = nor;

		bounceFactor = 1.01f;
		originalBounceFactor = 1.01f;

		while (bounceFactor >= 1) {
			transform.localScale *= bounceFactor;
			bounceFactor /= 1.0015f;
			yield return waitForFPS;
		}
		bounceFactor = 1f;

		while (bounceFactor <= originalBounceFactor) {
			transform.localScale /= bounceFactor;
			bounceFactor *= 1.0015f;
			yield return waitForFPS;
		}
		bounceFactor = originalBounceFactor;
		transform.localScale = nor;


		yield return waitForSec;

		if (bounce) {
			StartCoroutine ("Bounce");
		}

	}

}
