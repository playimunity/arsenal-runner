﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollViewChildHider : MonoBehaviour {

	ScrollRect sv;
	float halfElementHeight;
	bool isElementAboveTheView;
	bool isElementBelowTheView;
	float PrevY = 0f;

	RectTransform _contentTransform;
	RectTransform ContentTransform{
		get{ 
			if (_contentTransform == null) {
				_contentTransform = sv.content;
			}

			return _contentTransform;
		}
	}
	float ViewportHeight;


	void Start () {
		sv = GetComponent<ScrollRect> ();
		sv.onValueChanged.AddListener (OnScrolling);
		ViewportHeight = sv.viewport.rect.height;


		//SetVisibilityOfTheChildrens();
	}
	
	void OnScrolling(Vector2 pos){
		if (Mathf.Abs (pos.y - PrevY) > 0.004f) {
			PrevY = pos.y;
			//SetVisibilityOfTheChildrens ();
		}
	}

//	public void SetVisibilityOfTheChildrens()
//    {
//		
//        foreach (RectTransform child in sv.content)
//        {
//            if (!child.name.Contains("OneField"))
//            {
//                halfElementHeight = child.sizeDelta.y / 2f;
//                isElementAboveTheView = Mathf.Abs(child.localPosition.y) + halfElementHeight < ContentTransform.localPosition.y;
//                isElementBelowTheView = Mathf.Abs(child.localPosition.y) - halfElementHeight > ContentTransform.localPosition.y + ViewportHeight;
//
//                child.gameObject.SetActive(!isElementAboveTheView && !isElementBelowTheView);
//            }
//		}
//
//	}


}