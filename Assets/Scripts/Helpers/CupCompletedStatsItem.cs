﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CupCompletedStatsItem : MonoBehaviour {

	//public Text cupName;
	public Text cupStar1;
	public Text cupStar2;
	public Text cupStar3;

	public Image cupBG;

	Color colGrey;
	Color colBlack;


	void Start(){

		if (cupBG != null) {
			colBlack = cupBG.color;
			colGrey = cupBG.color;
			colGrey.a = 0.3f;
		}
	}


	public void SetCupStatus(int status){
		switch (status) {
		case 0:
			{
				cupBG.color = colGrey;
				cupStar1.text = "8";
				cupStar2.text = "8";
				cupStar3.text = "8";
				break;
			}
		case 1:
			{
				cupBG.color = colBlack;
				cupStar1.text = "7";
				cupStar2.text = "8";
				cupStar3.text = "8";
				break;
			}
		case 2:
			{
				cupBG.color = colBlack;
				cupStar1.text = "7";
				cupStar2.text = "7";
				cupStar3.text = "8";
				break;
			}
		case 3:
			{
				cupBG.color = colBlack;
				cupStar1.text = "7";
				cupStar2.text = "7";
				cupStar3.text = "7";
				break;
			}
		}
	}

}
