﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuestUIItem : MonoBehaviour {

//	public GameObject Grid;
//	public Image PuzzleMaskImage;
//	public Mask PuzzleMaks;
//
//	public List<Sprite> Masks;

	public GameObject UILocked;
	public GameObject UIAvailable;
	public GameObject UICompleted;
	public GameObject UICurrent;

	public GameObject UIAvailablePlay;
	public GameObject UIAvailableLock;

    public Text questIndex_Available;
    public Text questIndex_Current;
    public Text goldMedals;
    public Text silverMedals;
    public GameObject goldMedal;
    public GameObject silverMedal;
	public Text UILockedPlayersAmount;

	public Text UICurrentRunsPassed;
	public Text UICurrentRunsTotal;


	public Text UIAvailable_NumOfRuns;
	public Text UIAvailable_PassedRuns;

	public Text UICompleted_NumOfRuns;
	public Text UICompleted_PassedRuns;
    public KeyValuePair<bool,int> mostMedalsInQuest;

	Button btn;

	public QuestFriendsHandler qfHandler;

	void Start(){
		btn = GetComponent<Button> ();
		btn.onClick.AddListener (() => {
			HandleClick();
		});


	}

	// ----------- 

	public Cup cup;


	public void Set(Cup cup)
    {

        this.cup = cup;

        qfHandler.Set(cup.id);

        // ---
      
        UILocked.SetActive(false);
        UIAvailable.SetActive(false);
        UICompleted.SetActive(false);
        UICurrent.SetActive(false);
        questIndex_Available.text = (cup.id+1).ToString();
        questIndex_Current.text = (cup.id+1).ToString();

//		Grid.SetActive (true);
//		PuzzleMaks.enabled = true;


        switch (cup.status)
        {

            case Cup.CupStatus.NOT_AVAILABLE:
                {
                    //Debug.Log ("alon___________________ QuestUIItem - Set Cup UI - CupStatus.NOT_AVAILABLE " + " cup id " + cup.id);
                    UILocked.SetActive(true);
                    UILockedPlayersAmount.text = cup.requeredPlayers.ToString();
//				PuzzleMaskImage.sprite = Masks[0];
//				Grid.SetActive (false);

                    break;
                }

            case Cup.CupStatus.NOT_STARTED:
                {
                    //Debug.Log ("alon______________________ QuestUIItem - Set Cup UI - CupStatus.NOT_STARTED " + " cup id " + cup.id);
                    UIAvailable.SetActive(true);

                    UIAvailable_NumOfRuns.text = cup.numOfRuns.ToString();
                    UIAvailable_PassedRuns.text = "0";

                    // Set Play or Lock Image
                    if (!DAO.WinnedCups.Contains((cup.id - 1) + "|") && cup.id != 0)
                    {
                        UIAvailablePlay.SetActive(false);
                        UIAvailableLock.SetActive(true);
                    }
                    else
                    {
                        UIAvailablePlay.SetActive(true);
                        UIAvailableLock.SetActive(false);
                    }

//				PuzzleMaskImage.sprite = Masks[0];
//				Grid.SetActive (false);

                    break;
                }

            case Cup.CupStatus.IN_PROGRESS:
                {

                    //Debug.Log ("alon______________________ QuestUIItem - Set Cup UI - CupStatus.IN_PROGRESS " + " cup id " + cup.id);
                    UICurrent.SetActive(true);

                    UICurrentRunsPassed.text = cup.NumOfCompletedRuns.ToString();
                    UICurrentRunsTotal.text = cup.numOfRuns.ToString();

                    //HandlePuzzlePieces ();

                    break;
                }

            case Cup.CupStatus.COMPLETED:
                {
                    //Debug.Log ("alon______________________ QuestUIItem - Set Cup UI - CupStatus.COMPLETED " + " cup id " + cup.id);
                    UICompleted.SetActive(true);

                    UICompleted_NumOfRuns.text = cup.numOfRuns.ToString();
                    UICompleted_PassedRuns.text = cup.numOfRuns.ToString();
                    if (!mostMedalsInQuest.Equals(default(KeyValuePair<bool,int>)))
                    {
                        goldMedal.SetActive(mostMedalsInQuest.Key);           
                        silverMedal.SetActive(!mostMedalsInQuest.Key);

                        if (mostMedalsInQuest.Key)
                        { 
                            goldMedals.text = mostMedalsInQuest.Value.ToString();
                        }
                        else
                        {
                            silverMedals.text = mostMedalsInQuest.Value.ToString();
                        }
                    }
//                    var runs = cup.GetCompletedRunsPerCup();
//                    foreach (var item in runs)
//                    {
//                        if (item.GoalAchieved == 1)
//                            gold++;
//                        if (item.GoalAchieved == 0)
//                            silver++;
//                    }
//
//                    silverMedals.text = silver.ToString();
//                    goldMedals.text = gold.ToString();
//
//                    if (gold >= silver)
//                    {
//                        silverMedal.SetActive(false);
//                        goldMedal.SetActive(true);
//                    }
//                    else
//                    {
//                        silverMedal.SetActive(true);
//                        goldMedal.SetActive(false);
//                    }

//				Grid.SetActive (false);
//				PuzzleMaks.enabled = false;

				//cup.GetMadalPerQuest ();

				break;
			}

		}

	}

//	void HandlePuzzlePieces(){
//
////		if (cup.GetPrevCompleteRank() > -1) {
////			Grid.SetActive (false);
////			PuzzleMaks.enabled = false;
////			return;
////		}
//
//		int PuzzlesUnLocked = Mathf.CeilToInt(Masks.Count * cup.NumOfCompletedRuns / cup.numOfRuns);
//
//		if (PuzzlesUnLocked < 0) PuzzlesUnLocked = 0;
//
//		if (PuzzlesUnLocked >= Masks.Count) {
//			Grid.SetActive (false);
//			PuzzleMaks.enabled = false;
//		} else {
//			PuzzleMaskImage.sprite = Masks [PuzzlesUnLocked];
//		}
//
//	}

//	void HandlePuzzlePiecesTest(int completed_runs, int total_runs){
//
//
//
//	}

	void HandleClick(){
		// Check if previous cup completed
		if (!DAO.WinnedCups.Contains ((cup.id - 1) + "|") && cup.id != 0) return;

		if (cup.status == Cup.CupStatus.NOT_AVAILABLE) {

			AudioManager.Instance.OnBTNClick();
			GenericUIMessage msg = new GenericUIMessage();
			msg.Title = DAO.Language.GetString("cup-locked");
			msg.Message = DAO.Language.GetString("recruit-more-players-msg");

			msg.NegativeBTNAction = ()=>{
				CrossSceneUIHandler.Instance.hideGenericMessage();
			};

			msg.PositiveBTNAction = ()=>{
				CrossSceneUIHandler.Instance.hideGenericMessage();
				MainScreenUiManager.instance.CupScrnAnimToggle(false);
				//LockerRoom_Actions.instance.PlayerChooseScrnAnim(true);
				PlayerChooseManager.instance.SwitchToPreviewMode();
			};

			msg.PositiveButtonText = DAO.Language.GetString("reqruit-player");
			msg.NegativeButtonText = DAO.Language.GetString("not-now");

			CrossSceneUIHandler.Instance.showGennericMessage(msg);

		}else {
			CupsManager.Instance.OnCupClicked(cup);
			AudioManager.Instance.OnBTNClick();
		}
	}

}
