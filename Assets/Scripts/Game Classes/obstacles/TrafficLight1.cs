﻿using UnityEngine;
using System.Collections;

public class TrafficLight1 : Obstacle {

	//public bool flickering = false;
	bool triggered = false;

	public GameObject redLight;

	int flickerIndex = 0;




	void OnTriggerEnter(Collider col){
		if (!triggered && col.gameObject.tag == "Player") {
			triggered = true;
			StartCoroutine (RedLight ());
			if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
				CupRunController.Instance.RedFrameToggle ();
			} else if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
				PracticeRunController.Instance.RedFrameToggle ();
			}
		}
	}


	IEnumerator RedLight(){
		redLight.SetActive (true);
		yield return new WaitForSeconds (0.15f);
		redLight.SetActive (false);
		yield return new WaitForSeconds (0.15f);
		if (flickerIndex <= 15) {
			flickerIndex++;
			StartCoroutine (RedLight ());
		}
	}
}
