﻿using UnityEditor;
using UnityEngine;
public class MenuItems : MonoBehaviour {

    // Add a menu item named "Do Something" to MyMenu in the menu bar.
    [MenuItem ("Gamehour/Clear Data #d")]
    static void DeleteAllData () {
        Debug.Log ("Doing Something...");
        PlayerPrefs.DeleteAll();
        print ("All local data deleted!");
    }
}
