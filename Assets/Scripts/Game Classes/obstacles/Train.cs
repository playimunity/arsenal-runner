﻿using UnityEngine;
using System.Collections;

public class Train : Obstacle {


	bool localMovingTriggered = false;
	WaitForSeconds waitDeathCheck = new WaitForSeconds(0.5f);
	

	void Update () {
		if (move == Obstacle.MoveTypes.MOVE && moveTriggered) {
			transform.Translate( Vector3.back * moveSpeed * Time.deltaTime );
		}
	}



	void OnTriggerEnter(Collider col){

		if (!localMovingTriggered && col.gameObject.tag == "Player") {
			localMovingTriggered = true;
			moveTriggered = true;
			StartCoroutine (CheckPlayerDeath());
		}
	}

	IEnumerator CheckPlayerDeath(){
		if (localMovingTriggered && PlayerController.instance.isDead) {
			moveTriggered = false;
			yield break;
		}
		yield return waitDeathCheck;
		StartCoroutine (CheckPlayerDeath());
	}

	void OnTriggerExit(Collider col){

		if (col.gameObject.tag == "Player" && move == Obstacle.MoveTypes.MOVE) {

			AudioManager.Instance.OnTrainPass ();

		}


	}


}
