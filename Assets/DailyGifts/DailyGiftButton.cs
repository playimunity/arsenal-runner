﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;

public class DailyGiftButton : MonoBehaviour
{

	public Image image;
	public Text text;
	public Text amount;
	public Prize prize;
	public bool isSpecial;
	public Animator animator;
	public Image presentImage;
	// Use this for initialization
//	void Start ()
//	{
//		animator = GetComponent<Animator> ();
//	}
	
	// Update is called once per frame
//	void Update ()
//	{
//	
//	}

	public void Collect ()
	{
		if (isSpecial) {
			StartCoroutine (RevealSpecial ());
			return;
		}
		DailyGifts.instance.particlesObj.SetActive (true);
		GetComponent<ButtonClickAnimation> ().OnClicked ();
		MarkAsClaimed ();
		CollectPrize ();
		print (text.text);
		StartCoroutine (DailyGifts.instance.Close ());
	}

	public IEnumerator RevealSpecial ()
	{
		//image.sprite = prize.sprite;
		//amount.gameObject.SetActive (true);
		if (presentImage != null) {
			animator.Play ("mainmenu_claimPresentIcon", 0, 0f);
			yield return new WaitForSeconds (0.2f);
			image.gameObject.SetActive (true);
			yield return new WaitForSeconds (0.3f);
			presentImage.gameObject.SetActive (false);
			yield return new WaitForSeconds (0.5f);
		}

		isSpecial = false;
		Collect ();
	}


	public void MarkAsClaimed ()
	{
//		if (amount)
//			amount.gameObject.SetActive (false);
		//image.gameObject.SetActive (false);
		animator.Play("mainmenu_claimDailyGift",0,0f);
		//GetComponent<Image> ().sprite = DailyGifts.instance.tick;
	}

	public void MarkAsClaimedOnStart ()
	{
//		if (amount)
//			amount.gameObject.SetActive (false);
		image.gameObject.SetActive (false);
		GetComponent<Image> ().sprite = DailyGifts.instance.tick;
	}

	public void ChangeToSpecial ()
	{
		isSpecial = true;

		//image.sprite = DailyGifts.instance.giftsSprites [7];
		//amount.gameObject.SetActive (false);
		image.gameObject.SetActive(false);
		presentImage.gameObject.SetActive (true);
	}
	public void MarkCurrentDay ()
	{
		//GetComponent<Image> ().color = Color.red;
		//GetComponent<Image> ().sprite = DailyGifts.instance.currentDaySprite;
	}

	public void SetText (string s)
	{
		text.text = s;
	}



	public enum PrizeTypes
	{
		ADD_X_COINS,
		ADD_X_GIFTCARDS,
		ADD_X_X2,
		ADD_X_MAGNETS,
		ADD_X_GODMODE,
		ADD_X_ENERGYDRINKS,
		ADD_X_PLAYERS
	}

	public struct Prize
	{
		public PrizeTypes type;
		public int amount;
		public Sprite sprite;
	}

	public void CollectPrize ()
	{

		switch (prize.type) {
		case PrizeTypes.ADD_X_COINS:
			{
				//DAO.TotalCoinsCollected += prize.amount;
				CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (10 , prize.amount);
				//CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
				//if (GameManager.Instance.isPlayscapeLoaded)
					//BI._.Wallet_Deposit (prize.amount, "Ads Module Prize", "FCB Coins", "", "", "");
					GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,prize.amount, GameAnalyticsWrapper.REWARD,"CoinsReward");
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 

		case PrizeTypes.ADD_X_ENERGYDRINKS:
			{
				DAO.TotalMedikKitsCollected += prize.amount;
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
		case PrizeTypes.ADD_X_GIFTCARDS:
			{
				DAO.TotalGiftCardsCollected += prize.amount;

				if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {
					CardsScrn_Actions.instance.UpdateGiftCardsCount ();
				}
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
		case PrizeTypes.ADD_X_GODMODE:
			{
				DAO.TotalSpeedBoostsCollected += prize.amount;
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
		case PrizeTypes.ADD_X_MAGNETS:
			{
				DAO.TotalMagnetsCollected += prize.amount;
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
		case PrizeTypes.ADD_X_X2:
			{
				DAO.TotalX2CoinsCollected += prize.amount;
				DAO.Instance.LOCAL.SaveAll ();
				break;
			} 
		case PrizeTypes.ADD_X_PLAYERS:
			{
				DAO.IsFreePlayerClaimed = true;
				DailyGifts.instance.OpenSelectPlayerPanel ();
				break;
			} 
		}

	}

	public void SetPrize (JSONNode cta)
	{

		Prize p = new Prize ();
		switch (cta [0].Value) {
		case "AXC":
			{
				p.type	= PrizeTypes.ADD_X_COINS;
				p.amount = cta [1].AsInt;
				p.sprite = DailyGifts.instance.giftsSprites [0];
				//p.sprite = DailyGifts.instance.
				break;	
			}
		case "AXG":
			{
				p.type = PrizeTypes.ADD_X_GIFTCARDS;
				p.amount = cta [1].AsInt;
				p.sprite = DailyGifts.instance.giftsSprites [1];
				break;	
			}
		case "AXX2":
			{
				p.type = PrizeTypes.ADD_X_X2;
				p.amount = cta [1].AsInt;
				p.sprite = DailyGifts.instance.giftsSprites [2];
				break;	
			}
		case "AXM":
			{
				p.type = PrizeTypes.ADD_X_MAGNETS;
				p.amount = cta [1].AsInt;
				p.sprite = DailyGifts.instance.giftsSprites [3];
				break;	
			}
		case "AXGM":
			{
				p.type = PrizeTypes.ADD_X_GODMODE;
				p.amount = cta [1].AsInt;
				p.sprite = DailyGifts.instance.giftsSprites [4];
				break;	
			}
		case "AXED":
			{
				p.type = PrizeTypes.ADD_X_ENERGYDRINKS;
				p.amount = cta [1].AsInt;
				p.sprite = DailyGifts.instance.giftsSprites [5];
				break;	
			}
		case "AXP":
			{
				p.type = PrizeTypes.ADD_X_PLAYERS;
				p.amount = cta [1].AsInt;
				p.sprite = DailyGifts.instance.giftsSprites [6];
				break;	
			}
		default:
			{
				p.type = PrizeTypes.ADD_X_COINS;
				p.amount = 1;
				p.sprite = DailyGifts.instance.giftsSprites [0];
				break;
			}
		}
		prize = p;
		if (!isSpecial)
			image.sprite = prize.sprite;
		if (amount)
			amount.text = prize.amount.ToString ();
	}
}
