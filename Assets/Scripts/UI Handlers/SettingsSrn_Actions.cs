﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Facebook.Unity;

public class SettingsSrn_Actions : MonoBehaviour {

	public static SettingsSrn_Actions Instance;

//	public RectTransform musicHandle;
//	public RectTransform sFXHandle;
//	public RectTransform notificationsHandle;
	public GameObject musicOn;
	public GameObject musicOff;
	public GameObject sfxOn;
	public GameObject sfxOff;
	public GameObject notificationsOn;
	public GameObject notificationsOff;


//	public Image musicSliderDisabled;
//	public Image musicSliderEnabled;
//	public Image SFXSliderDisabled;
//	public Image SFXSliderEnabled;
//	public Image notificationSliderDisabled;
//	public Image notificationSliderEnabled;

	public Animator uiAnimator;
	
//	Vector2 targetHandlePosition;
//	Vector2 currentHandlePosition;

	//bool notificationsIsOn = true;

	public GameObject FBBtn;
	//public GameObject FBText;
	public GameObject InviteBtn;
	//public GameObject InviteText;

	void Start(){
		Instance = this;

		LoadSettingsState ();

		ResetFBState ();
	}


	void LoadSettingsState(){
		if (!DAO.IsMusicEnabled) {
			musicOn.SetActive(false);
			musicOff.SetActive(true);
		}

		if (!DAO.IsSFXEnabled) {
			sfxOn.SetActive(false);
			sfxOff.SetActive(true);
		}

		if (!DAO.IsNotificationsEnabled) {
			notificationsOn.SetActive(false);
			notificationsOff.SetActive(true);
		}
	}


	public void MusicToggle(){

		//StartCoroutine(HandleAnim (musicHandle , musicSliderEnabled, musicSliderDisabled, DAO.IsMusicEnabled));
		AudioManager.Instance.OnBTNClick ();
		if (DAO.IsMusicEnabled) {
			AudioManager.Instance.MuteMusic ();
			DAO.IsMusicEnabled = false;
			musicOn.SetActive(false);
			musicOff.SetActive(true);
		} else {
			DAO.IsMusicEnabled = true;
			AudioManager.Instance.switchMusicOnLevelLoad( GameManager.curentGameState );
			musicOn.SetActive(true);
			musicOff.SetActive(false);
		}
	}


	public void SFXToggle(){
		
		//StartCoroutine(HandleAnim (sFXHandle , SFXSliderEnabled, SFXSliderDisabled , DAO.IsSFXEnabled));

		if (DAO.IsSFXEnabled) {
			AudioManager.Instance.OnBTNClick ();
			DAO.IsSFXEnabled = false;
			sfxOn.SetActive(false);
			sfxOff.SetActive(true);
		} else {
			DAO.IsSFXEnabled = true;
			AudioManager.Instance.OnBTNClick ();
			sfxOn.SetActive(true);
			sfxOff.SetActive(false);
		}
	}


	public void NotificationsToggle(){
		
		//StartCoroutine(HandleAnim (notificationsHandle , notificationSliderEnabled, notificationSliderDisabled , DAO.IsNotificationsEnabled));
		AudioManager.Instance.OnBTNClick ();
		if (DAO.IsNotificationsEnabled) {
			DAO.IsNotificationsEnabled = false;
			notificationsOn.SetActive(false);
			notificationsOff.SetActive(true);
		} else {
			DAO.IsNotificationsEnabled = true;
			notificationsOn.SetActive(true);
			notificationsOff.SetActive(false);
		}
	}

	WaitForEndOfFrame _weof;
	WaitForEndOfFrame weof{
		get{
			if (_weof == null)
				_weof = new WaitForEndOfFrame ();

			return _weof;
		}
	}

//	IEnumerator HandleAnim(RectTransform obj , Image enabled, Image disbled , bool state){
//		currentHandlePosition = obj.anchoredPosition;
//		targetHandlePosition = obj.anchoredPosition;
//
//		if (!state) {
//			targetHandlePosition.x += 75;
//		} else {
//			targetHandlePosition.x -= 75;
//		}
//
//		for (float i=0f; i <= 1f; i += (3f * Time.deltaTime)) {
//			obj.anchoredPosition = Vector2.Lerp (obj.anchoredPosition, targetHandlePosition, i);
//
//			if (!state) {
//				//enabled.color = Color.Lerp(enabled.color , Color.white , i);
//				disbled.color = Color.Lerp(enabled.color , Color.clear , i);
//			} else {
//				//enabled.color = Color.Lerp(enabled.color , Color.clear , i);
//				disbled.color = Color.Lerp(enabled.color , Color.white , i);
//			}
//
//
//			yield return weof;
//		}
//	}


//	bool settingsScrnOn = false;
//
//	public void SettingsScrnToggle(bool on){
//		if (on && !settingsScrnOn) {
//			uiAnimator.Play ("settings_scrn_in");
//			settingsScrnOn = true;
//			GM_Input._.Settings = true;
//		} else if (!on && settingsScrnOn) {
//			uiAnimator.Play ("settings_scrn_out");
//			settingsScrnOn = false;
//			GM_Input._.Settings = false;
//			DAO.Instance.LOCAL.SaveAll ();
//		}
//	}



	public void ResetFBState(){
		if (FB.IsLoggedIn) {
			FBBtn.SetActive (false);
			//FBText.SetActive (false);
			InviteBtn.SetActive (true);
			//InviteText.SetActive (true);
		} else {
			FBBtn.SetActive (true);
			//FBText.SetActive (true);
			InviteBtn.SetActive (false);
			//InviteText.SetActive (false);
		}
	}


}
