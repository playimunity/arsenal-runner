﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;
using SimpleJSON;

public class FriendChooseScreen : MonoBehaviour {

	public static FriendChooseScreen _;
	public enum ScreenStates {SEND_GIFTCARDS, CLAIM_GIFTCARDS, ASK_FOR_GIFTCARDS, GIFTCARDS_REQUESTS, INVITE_FRIENDS, CHOOSE_FRIEND };
	public ScreenStates CurrentState;
	public RectTransform FIContainer;
	public float FriendItemHeight;


	public GameObject Footer_Gifts;
	public Text Title;
	public Text SubTitleOnlyTxt;
	public GameObject titleOnlyTxtParent;
	public GameObject titleTxtBtnParent;
	public Text SubTitleBtnTxt;
	public Text SendGiftBtn;

	public GameObject Footer_ClaimGifts;
	public Text ClaimAllBtn;

	public GameObject Footer_RequestGiftCards;
	public Text RequestBtn;

	public GameObject Footer_GiftCardsRequests;
	public Text ResponseRequestBtn;

	public GameObject Footer_InviteFriends;
	public Text InviteAllBtn;

	public GameObject SelectAllV;
	public Text AllTxt;

	public GameObject SelectDeselectAllBtn;


	public RectTransform scrollViewArea;

	Vector2 SCROLL_AREA_ORIGINAL_MAX;
	Vector2 SCROLL_AREA_ORIGINAL_MIN;

	Vector2 SCROLL_AREA_EXPANDED_MAX;
	Vector2 SCROLL_AREA_EXPANDED_MIN;

	Vector2 scrollContentSize;
	GameObject FI;
	FBFriendItem FIScript;

	bool AllSelected = true;

	public void Awake(){
		_ = this;
		scrollContentSize = FIContainer.sizeDelta;

		SCROLL_AREA_ORIGINAL_MAX = new Vector2 (scrollViewArea.offsetMax.x, -336f);
		SCROLL_AREA_ORIGINAL_MIN = new Vector2 (scrollViewArea.offsetMin.x, 256f);

		SCROLL_AREA_EXPANDED_MAX = new Vector2 (scrollViewArea.offsetMax.x, -169f);
		SCROLL_AREA_EXPANDED_MIN = new Vector2 (scrollViewArea.offsetMin.x, 45f);
	}

	void OnEnable(){
		AllSelected = true;
	}

	void LoadFriendsWithApp(){
		
		foreach(FBFriend friend in FBModule.Instance.FriendsWithGame){

			// Instantiate & Set FriendItem
			FI = (GameObject)Instantiate (PrefabManager.instanse.FriendItemElement , transform.position , transform.rotation);
			FI.transform.SetParent (FIContainer.transform , false);
			FI.GetComponent<FBFriendItem> ().SetFriend (friend);

			// Change Container Height
			scrollContentSize.y += FriendItemHeight;
			FIContainer.sizeDelta = scrollContentSize;
		}

	}

	void LoadInvitableFriends (){
	
		foreach(FBFriend friend in FBModule.Instance.InvitableFriends){

			// Instantiate & Set FriendItem
			FI = (GameObject)Instantiate (PrefabManager.instanse.FriendItemElement , transform.position , transform.rotation);
			FI.transform.SetParent (FIContainer.transform , false);
			FI.GetComponent<FBFriendItem> ().SetFriend (friend);

			// Change Container Height
			scrollContentSize.y += FriendItemHeight;
			FIContainer.sizeDelta = scrollContentSize;
		}
	
	}

	void LoadRecievedGiftCards(){
		foreach (string sender_id in DDB._.RecievedGiftCards) {
			foreach (FBFriend friend in FBModule.Instance.FriendsWithGame) {
				if(sender_id == friend.id){

					// Instantiate & Set FriendItem
					FI = (GameObject)Instantiate (PrefabManager.instanse.FriendItemElement , transform.position , transform.rotation);
					FI.transform.SetParent (FIContainer.transform , false);
					FI.GetComponent<FBFriendItem> ().SetFriend (friend);

					// Change Container Height
					scrollContentSize.y += FriendItemHeight;
					FIContainer.sizeDelta = scrollContentSize;

				}
			}
		}
	}

	void LoadGiftCardsRequesters(){
		foreach (string sender_id in DDB._.RecievedGiftCardsRequests) {
			foreach (FBFriend friend in FBModule.Instance.FriendsWithGame) {
				if(sender_id == friend.id){

					// Instantiate & Set FriendItem
					FI = (GameObject)Instantiate (PrefabManager.instanse.FriendItemElement , transform.position , transform.rotation);
					FI.transform.SetParent (FIContainer.transform , false);
					FI.GetComponent<FBFriendItem> ().SetFriend (friend);

					// Change Container Height
					scrollContentSize.y += FriendItemHeight;
					FIContainer.sizeDelta = scrollContentSize;

				}
			}
		}
	}

	// --------------------------------------------------------------------------------------------------


	public void SetState( ScreenStates state ){
		CurrentState = state;
		Footer_Gifts.SetActive (false);
		Footer_ClaimGifts.SetActive (false);
		Footer_RequestGiftCards.SetActive (false);
		Footer_GiftCardsRequests.SetActive (false);
		Footer_InviteFriends.SetActive (false);
		SelectDeselectAllBtn.SetActive (false);
		titleOnlyTxtParent.SetActive (false);
		titleTxtBtnParent.SetActive (false);

		AllTxt.text = DAO.Language.GetString("all"); 

		scrollViewArea.offsetMax = SCROLL_AREA_ORIGINAL_MAX;
		scrollViewArea.offsetMin = SCROLL_AREA_ORIGINAL_MIN;

		// SEND GIFTCARDS
		if (state == ScreenStates.SEND_GIFTCARDS) {
			
			Title.text = DAO.Language.GetString("send-gift-title");
			//SubTitleOnlyTxt.text = DAO.Language.GetString("send-gift-subtitle");
			//SelectDeselectAllBtn.SetActive (true);
			titleTxtBtnParent.SetActive(true);
			SubTitleBtnTxt.text = DAO.Language.GetString("send-gift-subtitle");
			Footer_Gifts.SetActive (true);
			SendGiftBtn.text = DAO.Language.GetString("send");

			LoadFriendsWithApp ();
			//LoadInvitableFriends ();
		}

		// CLAIM GIFTCARDS
		if(state == ScreenStates.CLAIM_GIFTCARDS){
			Title.text = DAO.Language.GetString("claim-gift-title");
			titleOnlyTxtParent.SetActive (true);
			SubTitleOnlyTxt.text = DAO.Language.GetString("claim-gift-subtitle");
			Footer_ClaimGifts.SetActive (true);
			ClaimAllBtn.text = DAO.Language.GetString("claim-all");

			LoadRecievedGiftCards ();
		}

		// REQUEST GIFTCARDS
		if(state == ScreenStates.ASK_FOR_GIFTCARDS){
			Title.text = DAO.Language.GetString("request-gift-title");
			//SubTitleOnlyTxt.text = DAO.Language.GetString("request-gift-subtitle");
			titleTxtBtnParent.SetActive(true);
			SubTitleBtnTxt.text = DAO.Language.GetString("request-gift-subtitle");
			Footer_RequestGiftCards.SetActive (true);
			RequestBtn.text = DAO.Language.GetString("request");
			//SelectDeselectAllBtn.SetActive (true);

			LoadFriendsWithApp ();
			//LoadInvitableFriends ();
		}

		// GIFTCARDS REQUESTS
		if (state == ScreenStates.GIFTCARDS_REQUESTS) {
			
			Title.text = DAO.Language.GetString("gift-requests-title");
			//SubTitleOnlyTxt.text = DAO.Language.GetString("gift-requests-subtitle");
			titleTxtBtnParent.SetActive(true);
			SubTitleBtnTxt.text = DAO.Language.GetString("gift-requests-subtitle");
			Footer_GiftCardsRequests.SetActive (true);
			ResponseRequestBtn.text = DAO.Language.GetString("send");
			//SelectDeselectAllBtn.SetActive (true);

			LoadGiftCardsRequesters ();
		}

		// INVITE FRIENDS
		if (state == ScreenStates.INVITE_FRIENDS) {
			Title.text = DAO.Language.GetString("invite-friends-title");
			//SubTitleOnlyTxt.text = DAO.Language.GetString("invite-friends-subtitle");
			titleTxtBtnParent.SetActive(true);
			SubTitleBtnTxt.text = DAO.Language.GetString("invite-friends-subtitle");
			Footer_InviteFriends.SetActive (true);
			InviteAllBtn.text = DAO.Language.GetString("invite");
			//SelectDeselectAllBtn.SetActive (true);

			LoadInvitableFriends ();
		}

		// CHOOSE FRIEND
		if (state == ScreenStates.CHOOSE_FRIEND) {
			Title.text = DAO.Language.GetString("choose-friend");
			SubTitleOnlyTxt.text = "";

			scrollViewArea.offsetMax = SCROLL_AREA_EXPANDED_MAX;
			scrollViewArea.offsetMin = SCROLL_AREA_EXPANDED_MIN;

			LoadFriendsWithApp ();
		}



	}

	public void DestroyAll(){
		foreach (Transform child in FIContainer.transform) {
			Destroy (child.gameObject);
		}

		scrollContentSize.y = 0f;
		FIContainer.sizeDelta = scrollContentSize;
	}


	public void SelectDeselectAll(){ 
		if (AllSelected) {
			DeselectAll ();
		} else {
			SelectAll ();
		}

		AllSelected = !AllSelected;
		SelectAllV.SetActive (AllSelected);
	}

	public void SelectAll(){
		foreach (Transform child in FIContainer.transform) {
			FIScript = child.gameObject.GetComponent<FBFriendItem> ();
			FIScript.Selected = true;

			FIScript.resetSelectedStatus ();
		}
	}

	public void DeselectAll(){
		foreach (Transform child in FIContainer.transform) {
			FIScript = child.gameObject.GetComponent<FBFriendItem> ();
			FIScript.Selected = false;
			FIScript.resetSelectedStatus ();
		}
	}

	public void OnSendGifCardsClicked(){
		List<string> selectedFriends = new List<string> ();
		foreach (Transform child in FIContainer.transform) {
			FIScript = child.gameObject.GetComponent<FBFriendItem> ();
			if (FIScript.Selected) {
				selectedFriends.Add (FIScript.FriendId);
			}
		}

		FBModule.Instance.SendGiftCards (selectedFriends);

		CrossSceneUIHandler.Instance.HideFriendChooseScreen ();
		DAO.TotalGiftCardsSent += selectedFriends.Count;

		UnitedAnalytics.LogEvent ("sent gift cards button", "completed", UserData.Instance.userType.ToString (), DAO.TotalGiftCardsSent);
	}

	public void OnSendRequestedGifCardsClicked(){
		List<string> selectedFriends = new List<string> ();
		foreach (Transform child in FIContainer.transform) {
			FIScript = child.gameObject.GetComponent<FBFriendItem> ();
			if (FIScript.Selected) {
				selectedFriends.Add (FIScript.FriendId);
				DDB._.RemoveGiftCardRequest (FIScript.FriendId);
			}
		}

		FBModule.Instance.SendGiftCards (selectedFriends);

		CrossSceneUIHandler.Instance.HideFriendChooseScreen ();
		DAO.TotalGiftCardsSent += selectedFriends.Count;
	}


	public void OnClaimAllClicked(){
		DDB._.ClaimAllGiftCards ();
		CrossSceneUIHandler.Instance.HideFriendChooseScreen ();
	}

	public void OnRequestGiftCardsClicked(){
		List<string> selectedFriends = new List<string> ();

		foreach (Transform child in FIContainer.transform) {
			FIScript = child.gameObject.GetComponent<FBFriendItem> ();
			if (FIScript.Selected) {
				selectedFriends.Add (FIScript.FriendId);
			}
		}

		FBModule.Instance.RequestGiftCards (selectedFriends);

		CrossSceneUIHandler.Instance.HideFriendChooseScreen ();

		UnitedAnalytics.LogEvent ("request gift", "completed", UserData.Instance.userType.ToString (), (int)Mathf.Floor(DAO.TotalGiftCardsCollected));
	}

	public void OnInviteAllClicked(){
		List<string> selectedFriends = new List<string> ();

		foreach (Transform child in FIContainer.transform) {
			FIScript = child.gameObject.GetComponent<FBFriendItem> ();
			if (FIScript.Selected) {
				selectedFriends.Add (FIScript.FriendId);
			}
		}

		FBModule.Instance.InviteFriends( selectedFriends );
		CrossSceneUIHandler.Instance.HideFriendChooseScreen ();
	}



}
