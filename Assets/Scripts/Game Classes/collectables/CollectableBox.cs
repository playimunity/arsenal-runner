﻿using UnityEngine;
using System.Collections;

public class CollectableBox : MonoBehaviour {

	public GameObject collectable;
	public GameObject target;
	public GameObject genericCargo;
	public GameObject ramblaCargo;

	public Renderer[] genericParts;
	public Renderer[] ramblaParts;

	bool collided = false;

	protected float destroyOnHeight;

	protected Vector3 tempPos;

	public Animator genericCargoAnim;
	public Animator ramblaCargoAnim;
	Animator anim;

	BoxCollider[] targetColliders;

	public Material genericMat;
	public Material gothicMat;
	public Material guellMat;
	public Material subwayMat;
	public Material ramblaMat;
	public Material whiteMat;

	string sectionType;



	void Awake () {
		//StartCoroutine(BeKinematic());

	}

	void Start(){
		targetColliders = target.GetComponentsInChildren<BoxCollider> ();

		sectionType = transform.parent.name;

		switch (sectionType) {
		case "objectsContainer_generic":
			{
				genericCargo.SetActive (true);
				foreach (Renderer rend in genericParts) {
					rend.material = genericMat;
				}
				anim = genericCargoAnim;
				break;
			}

		case "objectsContainer_gothic":
			{
				genericCargo.SetActive (true);
				foreach (Renderer rend in genericParts) {
					rend.material = gothicMat;
				}
				anim = genericCargoAnim;
				break;
			}

		case "objectsContainer_guell":
			{
				genericCargo.SetActive (true);
				foreach (Renderer rend in genericParts) {
					rend.material = guellMat;
				}
				anim = genericCargoAnim;
				break;
			}

		case "objectsContainer_subway":
			{
				genericCargo.SetActive (true);
				foreach (Renderer rend in genericParts) {
					rend.material = subwayMat;
				}
				anim = genericCargoAnim;
				break;
			}

		case "objectsContainer_rambla":
			{
				ramblaCargo.SetActive (true);
				foreach (Renderer rend in ramblaParts) {
					rend.material = ramblaMat;
				}
				anim = ramblaCargoAnim;
				break;
			}

		default:
			{
				genericCargo.SetActive (true);
				foreach (Renderer rend in genericParts) {
					rend.material = whiteMat;
				}
				anim = genericCargoAnim;
				break;
			}
		}

		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			CupRunController.Instance.AddBoxTransform (gameObject);
		}

	}



	void OnCollisionEnter(Collision col) {

		if (col.gameObject.tag == "Player" && !collided) {
			collided = true;

			if (!PlayerController.instance.sheildOn) {
				foreach (BoxCollider bCol in targetColliders) {
					bCol.enabled = false;
				}
				PlayerController.instance.Damage (1);
				AudioManager.Instance.OnSoftHit ();
				ParticleCloudeGenerator.instance.ParticleWhistle (transform.position, transform.eulerAngles);
				gameObject.SetActive(false);//Destroy (gameObject);
			} else {
				ParticleCloudeGenerator.instance.ParticleBoom(transform.position, transform.eulerAngles);
				//PlayerController.instance.GodSphereFX ();
				//RunManager.instance.StopSheild ();
				PlayerController.instance.sheildModeDamage = true;
				AudioManager.Instance.OnHitGod ();
				gameObject.SetActive(false);//Destroy (gameObject);
			}

		}
//		else if (col.gameObject.tag == "Ball" && !collided && PlayerController.instance.ballShooted) {
//			collided = true;
//			DestroyTarget();
//			PlayerController.instance.ballHitTheTarget = true;
//			foreach (BoxCollider bCol in targetColliders) {
//				bCol.enabled = false;
//			}
//		}

	}


	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Ball" && !collided) {
			collided = true;
			DestroyTarget();
			PlayerController.instance.ballHitTheTarget = true;
			foreach (BoxCollider bCol in targetColliders) {
				bCol.enabled = false;
			}
		}
	}



	void DestroyTarget(){

		AudioManager.Instance.OnBallSmash ();
		ParticleCloudeGenerator.instance.ParticleCloud (transform.position);
//		PlayerController.instance.ballShooted = false;
		RunManager.OnTargetShotByBall ();
		collectable.SetActive (true);
		anim.Play ("cargo_explode");
		StartCoroutine (DeActivateCargo());
	}

	static WaitForSeconds _wfs;
	static WaitForSeconds wfs{
		get{
			if (_wfs == null) _wfs = new WaitForSeconds (1.5f);
			return _wfs;
		}
	}

	IEnumerator BeKinematic(){
		yield return wfs;
		GetComponent<Rigidbody> ().isKinematic = true;
	}


	IEnumerator DeActivateCargo(){
		yield return wfs;
		target.SetActive (false);
	}





}
