using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerDataUIScreen : MonoBehaviour {
	public static PlayerDataUIScreen Instance;
	public Text PlayerNumber;
	public Text PlayerName;
	public Text Perk_1;
	public Text Perk_2;
	public Text Perk_3;
	public Text LevelTXT;
	public Text FitnessProgressTXT;
	public RectTransform ProgressBar;
	public Text PB_Text;
	public Text PB_inside_Text;

	public Sprite PB_ColorA;
	public Sprite PB_ColorB;
	public Image PB_Fill_Image;

	public ButtonClickAnimation hospitalIcon;


	float PB_OR_WIDTH;


	Vector2 V2;

	public GameObject FrozenTimer;
	public Text FT_Text;

	Player pl;

	public Color PerkIconColor;
	public Color PerkIconColorDisabled;
	public int energyToAdd;
	public TimerManager energyTimer;

	void Awake(){
		Instance = this;

		PB_OR_WIDTH = ProgressBar.sizeDelta.x;
	}

	void OnDisable(){
		if( pl != null) pl.OnFitnessUpdate -= UpdateProgressBar;
	}

	public void SetViewWithCurrentPlayer(){

		//CancelInvoke ("UpdateProgressBar");

		pl = PlayerChooseManager.instance.currentPlayerScript;
		energyToAdd = energyTimer.OnChangePlayerToDisplay (pl.PlayerID);
		//if (LockerRoom_Actions.instance.energyTimer.callback != null)
		energyTimer.callback = PlayerChooseManager.instance.EnergyCallback;
		pl.UpdateFitness ();

		PlayerName.text = pl.playerName;
		PlayerNumber.text = (pl.playerNumber == 0) ? "M" : pl.playerNumber.ToString();
		LevelTXT.text = DAO.Language.GetString("level")+" " + pl.data.level;

		Perk_1.text = Perk.GetPerkIconByPerkType (pl.data.Perk_1.type);
		Perk_2.text = Perk.GetPerkIconByPerkType(pl.data.Perk_2.type);
		Perk_3.text = Perk.GetPerkIconByPerkType(pl.data.Perk_3.type);

		UpdateProgressBar ();

		pl.OnFitnessUpdate += UpdateProgressBar;

		PlayerChooseManager.instance.CheckFitnessOfSelectedPlayer ();
		//InvokeRepeating("UpdateProgressBar", 0f , 1f);
	}

	public void UpdateProgressBar(){

		//if (pl.GetTimeToNextFitnessUpdate () < 1) pl.ForceFitnessUpdate ();

//		if (pl.data.curHealthState == PlayerData.HealthState.WOUNDED) {
//			FrozenTimer.SetActive (true);
//			PB_Text.text = 	Utils.ConvertSecondsToMMSSString(pl.GetTimeToNextFitnessUpdate());
//			PB_Text.gameObject.SetActive (false);
//			PB_inside_Text.gameObject.SetActive (false);
//			PB_Fill_Image.sprite = PB_ColorB;
//			hospitalIcon.BounceToggle(true);

		if(pl.data.curHealthState == PlayerData.HealthState.TIERED || pl.data.curHealthState == PlayerData.HealthState.WOUNDED){
			FrozenTimer.SetActive (false);
			energyTimer.gameObject.SetActive (true);
			PB_Text.gameObject.SetActive (true);
			PB_Text.text = DAO.Language.GetString ("refill-in");//+""+ Utils.ConvertSecondsToMMSSString(pl.GetTimeToNextFitnessUpdate()) + " m ";
			PB_inside_Text.gameObject.SetActive (true);
			PB_inside_Text.text = DAO.Language.GetString ("tiered");
			PB_Fill_Image.sprite = PB_ColorB;
			hospitalIcon.BounceToggle (true);
		}else if(pl.data.curHealthState == PlayerData.HealthState.IDEAL){
			FrozenTimer.SetActive (false);
			energyTimer.gameObject.SetActive (true);
			PB_Text.gameObject.SetActive (true);
			PB_Text.text = DAO.Language.GetString ("refill-in");//+" "+  Utils.ConvertSecondsToMMSSString (pl.GetTimeToNextFitnessUpdate ()) + " m";
			PB_inside_Text.gameObject.SetActive (false);
			PB_Fill_Image.sprite = PB_ColorA;
			hospitalIcon.BounceToggle (false);
		}else{
			FrozenTimer.SetActive (false);
			energyTimer.FullEnergy (pl.PlayerID);
			energyTimer.gameObject.SetActive (false);
			PB_Text.text = DAO.Language.GetString("fit-to-run");
			PB_Fill_Image.sprite = PB_ColorA;
			PB_Text.gameObject.SetActive (true);
			PB_inside_Text.gameObject.SetActive (false);
			hospitalIcon.BounceToggle (false);
		}

		FitnessProgressTXT.text = pl.data.fitness + " / " + pl.data.maxFitness;


		V2 = ProgressBar.sizeDelta;
		V2.x = (float)pl.data.fitness * (PB_OR_WIDTH / (float)pl.data.maxFitness);
		if (V2.x < 123f) V2.x = 123f;
		ProgressBar.sizeDelta = V2;

		SetPerksIconsOpacity ();

	}


	public void SetPerksIconsOpacity(){


		if (pl.data.PerksDisabled) {
			Perk_1.color = PerkIconColorDisabled;
			Perk_2.color = PerkIconColorDisabled;
			Perk_3.color = PerkIconColorDisabled;
		} else {
			
			Perk_1.color = PerkIconColor;
			Perk_2.color = PerkIconColor;
			Perk_3.color = PerkIconColor;

			if (Perk_2.text == "D") Perk_2.color = PerkIconColorDisabled;
			if (Perk_3.text == "D") Perk_3.color = PerkIconColorDisabled;
		
		}
	}
}
