using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using SimpleJSON;

public class DailyGifts : MonoBehaviour
{
	public int currentDay;
	public List<DailyGiftButton> dailyGiftButtons;
	public Sprite tick;
	private DateTime referenceDate;
	public bool isDebugging;
	public int bigPrizeDay;
	public Button collectButton;
	public Button collectBigButton;
	public JSONNode prizes;
	public static DailyGifts instance;
	public List<Sprite> giftsSprites;
	public Sprite currentDaySprite;
	public GameObject particlesObj;
	// Use this for initialization
	void Awake()
	{
		prizes = DAO.Settings.DailyGiftsTable1;
		print ("On ENABLE ===> isDebugging = " + isDebugging);
		currentDay = GetDaysInARow ();
		if (currentDay>=bigPrizeDay)
		{
			if (isDebugging)
				Reset ();
			else
				gameObject.SetActive (false);
		}
	}
	void Start ()
	{
		print ("******DAAYAYSYSA");
		instance = this;
		dailyGiftButtons = new List<DailyGiftButton> (GetComponentsInChildren<DailyGiftButton> ());
		dailyGiftButtons [currentDay].MarkCurrentDay ();
		for (int i = 0; i < dailyGiftButtons.Count; i++) {
            dailyGiftButtons [i].SetText (DAO.Language.GetString("day") + (i + 1).ToString ());
			if (i < currentDay) {
				dailyGiftButtons [i].MarkAsClaimedOnStart ();
			}
			//special prize
			else {
				if ((i + 1) % 7 == 0 && dailyGiftButtons [i].GetType () != typeof(BigPrize)) {
					dailyGiftButtons [i].ChangeToSpecial ();
				}
				dailyGiftButtons [i].SetPrize (prizes[i]);
			}
		}
	}

	public int GetDaysInARow ()
	{
		if(DAO.DailyGiftsStart == new DateTime())
			DAO.DailyGiftsStart= DateTime.Now;
		DateTime startDate = DAO.DailyGiftsStart;
		DateTime lastDate = DAO.DailyGiftsLast;
		referenceDate = isDebugging?lastDate.AddDays(1):DateTime.Now;
		DateTime yesterdayMidnight = referenceDate.AddHours (-24);
        yesterdayMidnight = GetMidnightOfDate(yesterdayMidnight);
		if (lastDate > yesterdayMidnight) {
            var referenceMidnight = GetMidnightOfDate(referenceDate);
            var startMidnight = GetMidnightOfDate(startDate);
			var days = (referenceMidnight - startMidnight).Days;
			//days = days == 0 ? 1 : days;
			return days;
		}
		Reset ();
		return 0;
	}

    public DateTime GetMidnightOfDate(DateTime date)
    {
        return new DateTime (date.Year, date.Month, date.Day, 0, 0, 0);
    }

	public void CollectClicked()
	{
		particlesObj.transform.position = dailyGiftButtons [currentDay].transform.position;
		//particlesObj.SetActive (true);
		collectButton.interactable = false;
		collectBigButton.interactable = false;
		DAO.DailyGiftsLast = referenceDate;
		dailyGiftButtons [currentDay].Collect ();
		if (isDebugging)
		{
			DAO.DailyGiftsLast = referenceDate;
			referenceDate = referenceDate.AddDays(1);
			currentDay++;
		}
//		StartCoroutine (Close ());
	}
	public IEnumerator Close ()
	{
		yield return new WaitForSeconds(1.7f);
		MainMenu_Actions.instance.CloseDailyGfts ();
		yield return new WaitForSeconds(1.5f);
		particlesObj.SetActive (false);
		gameObject.SetActive (false);
	}
	public void Reset ()
	{
		currentDay = 0;
		DAO.DailyGiftsStart = DateTime.Now;
		DAO.DailyGiftsLast 	= new DateTime();
	}

	public void OpenSelectPlayerPanel ()
	{
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.PLAYER_PREVIEW);
	}


}
