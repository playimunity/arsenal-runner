﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StringProvider_LockerRoom : MonoBehaviour {

	// Cups Screen
	//public Text CS_Title;

	// Runs Screen
//	public Text RS_Title;
//	public Text RS_Prize;

	//Medic Kits
//	public Text MK_Title;
//	public Text MK_UseBtn;
//	public Text MK_GetBtn;


	//Player Choose screen
//	public Text PCS_Title;

	//Player to buy info
//	public Text PTBIS_CancelBtn;
//	public Text PTBIS_RecruitBtn;

	//Cup Won
//	public Text CW_Title;
//	public Text CW_GreatWork;
//	public Text CW_YouFinished;
//	public Text CW_Announcment;
//	public Text CW_SendGiftBTn;
//	public Text CW_ConnectToFBBtn;
//	public Text CW_AlreadyCompleted;
//	public Text CW_CloseBtn;

	//Buttons / Title
//	public Text OngiongTXT;
//	public Text RecruitPlayerTXT;
//	public Text PickPlayerTXT;
//	public Text PracticeBTN;
//	public Text CupBTN;
//	public Text GoBTN;

	// Tutorial
	//public Text HelloCoach;
	//public Text Free;
	//public Text ScratchCardsAdded;
	//public Text Great;
	//public Text LetsStartConnected;
	//public Text LetsStartNotConnected;
//	public Text SwipeLeftOrRight;
	//public Text Skip;
	//public Text FirstPlayerRecruited;
	//public Text YoullHaveMore;
	//public Text LetsTakeHimFor;

	//Tutorial popups
//	public Text NotNow1;
//	public Text Heal;
//	public Text HealBtn;
//	public Text YourPlayerIsTired;

//	public Text NotNow2;
//	public Text RecruitAnother;
//	public Text RecruitPlayer;
//	public Text YouHaveEnoughFBC;

//	public Text NotNow3;
//	public Text RecruitPlayer2;
//	public Text Get;
//	public Text AnotherPlayerIsJust;

//	public Text NotNow4;
//	public Text Practice;
//	public Text PracticeBtn;
//	public Text PracticeToRaiseYour;

//	public Text NotNow5;
//	public Text StartACup;
//	public Text StartCup;
//	public Text PlayCupsToGet;

//	public Text NotNow6;
//	public Text WelcomeOfferTitle;
//	public Text GetIt;
//	public Text WelcomeOfferOfer;

//	public Text LowEnergy_Title;
//	public Text LowEnergy_Body;
//	public Text LowEnergy_HealBtn;
//	public Text LowEnergy_ChoosePlayerBtn;

	//perks
//	public Text perks_UpgradeTitle;
//	public Text perks_GetMoreSkills;

	//rate us popup
//	public Text rs_title;
//	public Text rs_loveOurGame;
//	public Text rs_rateUsAndGet;
//	public Text rs_rateUsBtn;
//	public Text rs_notNowBtn;

//	public Text rs_thankYou;
//	public Text rs_feelFree;
//	public Text rs_closeBtn;

//	public Text questMapHeader;

//	public Text skillsHeader;


	//public Text freeGifts;

	void Awake () {





		// Cups Screen
		//CS_Title.text = DAO.Language.GetString("cups-title");

		// Runs Screen
//		RS_Title.text = DAO.Language.GetString("runs-title");
//		RS_Prize.text = DAO.Language.GetString("prize");

		//Medic Kits
//		MK_Title.text = DAO.Language.GetString("medikits-title");
//		MK_UseBtn.text = DAO.Language.GetString("use");
//		MK_GetBtn.text = DAO.Language.GetString("get");

		//Player Choose screen
//		PCS_Title.text = DAO.Language.GetString("player-choose-title");

		//Player to buy info
//		PTBIS_CancelBtn.text = DAO.Language.GetString("cancel");
//		PTBIS_RecruitBtn.text = DAO.Language.GetString("recruit");

		//Cup Won
//		CW_Title.text = DAO.Language.GetString("cup-won-title");
//		CW_GreatWork.text = DAO.Language.GetString("great-work");
//		CW_YouFinished.text = DAO.Language.GetString("you-finished");
//		CW_Announcment.text = DAO.Language.GetString("send-free-gift");
//		CW_SendGiftBTn.text = DAO.Language.GetString("send-gift-btn");
//		CW_ConnectToFBBtn.text = DAO.Language.GetString("connect-to-fb");
//		CW_AlreadyCompleted.text = DAO.Language.GetString("already-completed");
//		CW_CloseBtn.text = DAO.Language.GetString("close");

		//Buttons - Title
//		OngiongTXT.text = DAO.Language.GetString("ongoing");
//		RecruitPlayerTXT.text = DAO.Language.GetString("reqruit-player");
	//	PickPlayerTXT.text = DAO.Language.GetString("pick-player");
//		PracticeBTN.text = DAO.Language.GetString("endless");
//		CupBTN.text = DAO.Language.GetString("cup");
//		GoBTN.text = DAO.Language.GetString("go");

		//Tutorial:
//		HelloCoach.text = DAO.Language.GetString ("hello-coach");
//		Free.text = DAO.Language.GetString ("free");
//		ScratchCardsAdded.text = DAO.Language.GetString ("scratch-cards-added");
//		Great.text = DAO.Language.GetString ("great");
//		LetsStartConnected.text = DAO.Language.GetString ("lets-start");
//		LetsStartNotConnected.text = DAO.Language.GetString ("lets-start");
//		SwipeLeftOrRight.text = DAO.Language.GetString ("swipe-left-or-right");
		//Skip.text = DAO.Language.GetString ("skip");
//		FirstPlayerRecruited.text = DAO.Language.GetString ("first-player-recruited");
//		YoullHaveMore.text = DAO.Language.GetString ("youll-have-more");
//		LetsTakeHimFor.text = DAO.Language.GetString ("lets-take-him");

//		NotNow1.text = DAO.Language.GetString ("not-now");
//		Heal.text = DAO.Language.GetString ("heal");
//		HealBtn.text = DAO.Language.GetString ("heal");
//		YourPlayerIsTired.text = DAO.Language.GetString ("player-is-tired");

//		NotNow2.text = DAO.Language.GetString ("not-now");
//		RecruitAnother.text = DAO.Language.GetString ("recruit-another");
//		RecruitPlayer.text = DAO.Language.GetString ("recruit-player");
//		YouHaveEnoughFBC.text = DAO.Language.GetString ("you-have-enough");

//		NotNow3.text = DAO.Language.GetString ("not-now");
//		RecruitPlayer2.text = DAO.Language.GetString ("recruit-player");
//		Get.text = DAO.Language.GetString ("get");
//		AnotherPlayerIsJust.text = DAO.Language.GetString ("another-player-is-just");

//		NotNow4.text = DAO.Language.GetString ("not-now");
//		Practice.text = DAO.Language.GetString ("practice");
//		PracticeBtn.text = DAO.Language.GetString ("practice");
//		PracticeToRaiseYour.text = DAO.Language.GetString ("practice-to-raise");

//		NotNow5.text = DAO.Language.GetString ("not-now");
//		StartACup.text = DAO.Language.GetString ("start-a-cup");
//		StartCup.text = DAO.Language.GetString ("start-cup");
//		PlayCupsToGet.text = DAO.Language.GetString ("play-cups-to-get");

//		NotNow6.text = DAO.Language.GetString ("not-now");
//		WelcomeOfferTitle.text = DAO.Language.GetString ("starter_pack_title");
//		GetIt.text = DAO.Language.GetString ("get-it");
//		WelcomeOfferOfer.text = DAO.Language.GetString ("starter_pack_text");


//		LowEnergy_Title.text = DAO.Language.GetString ("low-energy-title");
//		LowEnergy_Body.text = DAO.Language.GetString ("low-energy-body");
//		LowEnergy_HealBtn.text = DAO.Language.GetString ("heal");
//		LowEnergy_ChoosePlayerBtn.text = DAO.Language.GetString ("low-energy-choose-player");

//		perks_UpgradeTitle.text = DAO.Language.GetString ("upgrades");
//		perks_GetMoreSkills.text = DAO.Language.GetString ("get-more-skills");


		//rate us:
//		rs_title.text = DAO.Language.GetString ("rate-us");
//		rs_loveOurGame.text = DAO.Language.GetString ("love-our-game");
//		rs_rateUsAndGet.text = DAO.Language.GetString ("rate-us-text");
//		rs_rateUsBtn.text = DAO.Language.GetString ("rate-us");
//		rs_notNowBtn.text = DAO.Language.GetString ("not-now");

//		rs_thankYou.text = DAO.Language.GetString ("thank-you");
//		rs_feelFree.text = DAO.Language.GetString ("rate-pop-up");
//		rs_closeBtn.text = DAO.Language.GetString ("back");


//		questMapHeader.text = DAO.Language.GetString ("choose-quest");
//		skillsHeader.text = DAO.Language.GetString ("perks-str");

	}




}
