﻿using UnityEngine;
using System.Collections;

public class levelHeight : MonoBehaviour {

	public static levelHeight Instance;

	public Transform player;
	public Transform Puppet;
	Vector3 pos;
	PlayerController pc;


	public bool followPlayerZ = false;

	void Start(){
		Instance = this;

		pc = player.parent.gameObject.GetComponent<PlayerController> ();

		Puppet = GameObject.Find ("Bip001 Pelvis").transform;


	}

	// Update is called once per frame
	void FixedUpdate () {
		pos = transform.localPosition;
		pos.x = player.localPosition.x;


		if (followPlayerZ) {
			pos.z = Puppet.localPosition.z - 1.5f;
		}

		transform.localPosition = pos;
	}
}
