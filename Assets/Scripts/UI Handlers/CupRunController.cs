using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;



public class CupRunController : MonoBehaviour {

	#region SAVEME UI THINGS

	public GameObject SaveMeCoinsContainer;
	public GameObject SaveMeAdsContainer;
	public Text SaveMeAdCoinsBtnTxt;

	#endregion
	public Animator an;
	//public Animator tutAninmator;
	public static CupRunController Instance;

	//public static bool tutRetryed = false;


	public Text WS_Title;
	public GameObject Header_Gold;
	public GameObject Header_Silver;
	public GameObject Header_Fail;
	//public Transform medalsParent;
	//public GameObject goldBonus;
	//public GameObject silverBonus;
	//public GameObject failBonus;
	//public Text totalCoinsAchived;
	bool medalWon = false;
	//public Color gold;
	//public Color silver;
	public Image GoalIcon;
	public Text Goal_Achieved;
	public GameObject coinsGoalParent;
	public GameObject goalParent;
	public Text coinsGoalAchieved;
	//public Text Goal_goal;
	public Text CoinsCollectedTXT;
	//public Text CoinsBonusTXT;
	//public GameObject CoinsBonus;
	public Text goalDiscription;
	public GameObject NextRunBTN;
	public GameObject RetryBTN;
	public Text playerName;
	PlayerData playerData;

	public Text RetryPrice;
	public Sprite GoalImage_Adrenaline;
	public Sprite GoalImage_Coins;
	public Sprite GoalImage_DGH;
	public Sprite GoalImage_DGH_And_DUA;
	public Sprite GoalImage_Tackle;
	public Sprite GoalImage_Smash;
	public Sprite GoalImage_Time;




	public Text goalA;
	public Text goalB;
	//public Text runNumber;

	public GameObject countDownUI;
	public GameObject countDownParent;
	public GameObject RestartBTN;

	//public Vector2 tapToShotPos;
	//public Vector3 boxPos;

	int saveMeCounterIndex = 0;
	int adSaveMeCounterIndex = 0;

	bool SaveMeClicked = false;
	bool countingDown = false;

	public Button saveMeBtn;
	public Button saveMeBtn2;

	public GameObject debugParent;

	// SaveMe PB
	WaitForSeconds SMPBI;
	float SMPB_TIME = 5f;
	float SMPB_MIN = 111f;
	float SMPB_MAX = 880f;
	float SMPB_STTEP = 0f;
	Vector2 SMTMPV2;
	public RectTransform SaveMeBar;

	void Awake(){
		Instance = this;
		GM_Input._.CupRunPause = false;
	}

	void Start(){
		SMPBI = new WaitForSeconds (0.5f);
		saveMeCounterIndex = 0;
		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			goalA.text = Run.Instance.runGoal.goalDescription.goal_A;
			goalB.text = Run.Instance.runGoal.goalDescription.goal_B;
			//barRankParticlse.Stop ();
			if (DAO.IsInDebugMode) {
				debugParent.SetActive (true);
			}
		}else if(GameManager.curentGameState == GameManager.GameState.TEST_RUN){
			RestartBTN.SetActive(true);
			PlayerController.instance.HitsRemain = 10;
		}
		playerData = PlayerController.instance.data;

		//MainScreenUiManager.instance.FreeGiftBtnAnimToggle (false);
	}

	#if UNITY_EDITOR
	void Update(){
		if (Input.GetKeyUp (KeyCode.R)) {
			BarRankFX ();
		}
	}
	#endif

	public void OnRestartRunClicked(){
		GameManager.SwitchState( GameManager.GameState.TEST_RUN );
	}

//	public void StartRun(){
//		PlayerController.instance.StartRun ();
//	}

//	IEnumerator StartAnimationsProcces(){
//
//	}

	public void RunUiAnimToggle(bool isOn){
		if (isOn) {
			an.Play ("CupRunUI_in");
		} else {
			an.Play ("CupRunUI_out");
		}
	}

	public void RunBtnAnimToggle(bool isOn){
		if (isOn) {
			an.Play ("Run_Go_btn_in");
		} else {
			an.Play ("Run_Go_btn_out");
		}
	}

	public void RunGoalsAnimToggle(bool isOn){
		if (isOn) {
			an.Play ("Run_Goals_in");
		} else {
			an.Play ("Run_Goals_out");
		}
	}

//	public void PauseCountDown(){
//		//countDownUI.SetActive(true);
//		an.Play ("pause_count_down");
//	}

	public void TutorialCountDown(){
	//	countDownUI.SetActive(true);
		an.Play ("tut_count_down");
	}

	public void TutorialStartRun(){
		PlayerController.instance.StartTutorialRun ();
	}


	public void Pause(){
		if (GameManager.Instance.runPaused)		return;

		//GameManager.Instance.runPaused = true;
		GM_Input._.TutorialPause = true;
		GM_Input._.CupRunPause = true;
		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			if (Tutorial.Instance.tutorialPointOn) return;
		}

//		if (RunManager.instance.fiveSectionsToEnd) {
//			AudioManager.Instance.PreperToEndRunCrowd (false);
//		}

		GameManager.RequestPause ();
		an.Play ("CupRun_pause", 1);

	}

	public void UnpauseCountDown(){
		if (countingDown) {
			return;
		}
		countingDown = true;
//		GM_Input._.TutorialPause = false;
//		GM_Input._.CupRunPause = false;
		//GameManager.RequestUnpause ();
		//PauseCountDown();
		countDownParent.SetActive(true);
		an.Play ("pause_count_down" , 9 , 0f);
		an.Play ("CupRun_unpause", 1);

//		if (RunManager.instance.fiveSectionsToEnd) {
//			AudioManager.Instance.PreperToEndRunCrowd (true);
//		}
			
	}

	public void DeactivateCountDown(){
		//countDownParent.SetActive(false);
		an.Play("stop count down");

	}


	public void UnPause(){
		
		if (SaveMeClicked) {
			PlayerController.instance.StartRunAfterDie ();
			Run.Instance.running = true;
			countingDown = false;
			//RunManager.instance.ActivateGodMode (false);
		} else {
			an.Play ("CupRun_unpause", 1);
			GameManager.RequestUnpause ();
		}
		GM_Input._.TutorialPause = false;
		GM_Input._.CupRunPause = false;
		SaveMeClicked = false;
		countingDown = false;
	}


	public void FailedScrn(bool on){
		if (on) {
			GM_Input._.TutorialFail = true;
			an.Play ("tut_run_fail_in");
		} else {
			GM_Input._.TutorialFail = false;
			an.Play ("tut_run_fail_out");
		}
	}


	public void RetryTut(){
		Tutorial.Instance.tutRetryed = true;
        Tutorial.Instance.tutorialState = 5;
		GameManager.SwitchState (GameManager.GameState.TUTORIAL_RUN);
	}


	public void SkipTutorial(){
		LocalDataInterface.Instance.tutorialState = 4;
		GM_Input._.TutorialPause = false;
		GM_Input._.CupRunPause = false;

		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
	}
		

	public void ShowWonScreen(){
//		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
//
//		}
		an.Play ("CupRun_Won_show", 2);
		//StartCoroutine (WonScrnAnimations());
	}



//	IEnumerator WonScrnAnimations(){
//		yield return new WaitForSeconds (0.75f);
//		if (medalWon) {
//			an.Play ("CupRun_Medal_won" , 6 , 0f);
//			yield return new WaitForSeconds (0.1f);
//			if (goldBonus.activeSelf == true) {
//				CrossSceneUIHandler.Instance.SparklesFX (3f, medalsParent.position, gold , 10 , 5, 5, false);
//			} else if (silverBonus.activeSelf == true) {
//				CrossSceneUIHandler.Instance.SparklesFX (3f, medalsParent.position, silver , 10 , 5, 5, false);
//			}
//			AudioManager.Instance.RunCompleted ();
//			yield return new WaitForSeconds (1f);
//			CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (10 , 0);
//			//CrossSceneUIHandler.Instance.SparklesFX ();
//		} else {
//			an.Play ("CupRun_Medal_won" , 6 , 0f);
//			//an.Play ("CupRun_Medal_fail");
//		}
//	}

	public void HideWonScreen(){
		an.Play ("CupRun_Won_hide", 2);
	}

	bool SaveMeCounterActive = false;
	public void ShowSaveMeScreen(){
		int saveMePrice = DAO.getSaveMePricesByIndex(saveMeCounterIndex,DAO.SaveMePriceTag.CUP);
		RetryPrice.text = saveMePrice.ToString ();
		// If the user can still have a save me chance.
		if (saveMePrice != -1) {
			//if the number of times the user watched an ad to save itself is between the allowed range.
			if (adSaveMeCounterIndex <= LocalDataInterface.Instance.numberOfAdsForSaveMeCup) {
				//if the usertype is allowed to watch an ad.
				if (HeyzapWrapper.AreAdsAllowedForUser (HeyzapWrapper.AdType.SaveMe)) {
					SaveMeAdCoinsBtnTxt.text = DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.CUP).ToString ();
					SaveMeAdsContainer.SetActive (HeyzapWrapper.IsAvailable);
					SaveMeCoinsContainer.SetActive (!HeyzapWrapper.IsAvailable);
				}
			//if the usertype is not allowed to watch an ad.
			 else {
					SaveMeAdsContainer.SetActive (false);
					SaveMeCoinsContainer.SetActive (true);
				}
			}
			//if the number of times the user watched an ad to save itself is over the allowed range.
			else {
				SaveMeAdsContainer.SetActive (false);
				SaveMeCoinsContainer.SetActive (true);
			}
		}  else {
			RunManager.instance.OnSaveMeDeclined ();
			return;
		}
			//EndRun run 
		GM_Input._.SaveMe = true;
		saveMeBtn.enabled = true;
		saveMeBtn2.enabled = true;
		an.Play ("CupRun_SaveMe_Show", 8);
		SetAndStartSaveMeTimer ();
	}

	public void HideSaveMeScreen(bool declined = true){
		AudioManager.Instance.StopWatch (false);
		GM_Input._.SaveMe = false;
		an.Play ("CupRun_SaveMe_Hide", 8);
		SaveMeCounterActive = false;

		if (declined && !SaveMeClicked) {
			RunManager.instance.OnSaveMeDeclined ();
		}
	}

	public bool saveMeTimerIsOn = false;
	public void SetAndStartSaveMeTimer(){
		if (saveMeTimerIsOn)		return;

		saveMeTimerIsOn = true;
		SMPB_STTEP = (float)((SMPB_MAX - SMPB_MIN) / (SMPB_TIME / 0.5));

		SMTMPV2 = SaveMeBar.sizeDelta;
		SMTMPV2.x = SMPB_MAX;
		SaveMeBar.sizeDelta = SMTMPV2;

		SaveMeCounterActive = true;

		StartCoroutine (PlaySaveMeSound());

		StartCoroutine( SaveMePBTimer() );
	}

//	public RectTransform SaveMeBar;

	IEnumerator SaveMePBTimer(){
//		yield return SMPBI;
//		SMTMPV2 = SaveMeBar.sizeDelta;
//		SMTMPV2.x -= SMPB_STTEP;
//		if (SMTMPV2.x < SMPB_MIN && !SaveMeClicked) {
//			SMTMPV2.x = SMPB_MIN;
//			SaveMeCounterActive = false;
//			saveMeTimerIsOn = false;
//			saveMeBtn.enabled = false;
//			saveMeBtn2.enabled = false;
//			HideSaveMeScreen ();
//		}
//		SaveMeBar.sizeDelta = SMTMPV2;
//		if(SaveMeCounterActive && !SaveMeClicked) StartCoroutine( SaveMePBTimer() );


		SMTMPV2 = SaveMeBar.sizeDelta;
		while (SaveMeCounterActive && !SaveMeClicked) {
			yield return SMPBI;
			SMTMPV2.x -= SMPB_STTEP;
			if (SMTMPV2.x < SMPB_MIN && !SaveMeClicked) {
				SMTMPV2.x = SMPB_MIN;
				SaveMeCounterActive = false;
				saveMeTimerIsOn = false;
				saveMeBtn.enabled = false;
				saveMeBtn2.enabled = false;
				HideSaveMeScreen ();
			}
			SaveMeBar.sizeDelta = SMTMPV2;
		}
	}

	IEnumerator PlaySaveMeSound(){
		yield return new WaitForSeconds (0.15f);
		AudioManager.Instance.StopWatch (true);
	}



	public void SetWonScreen(int points){
		//medalsWonNum = points;
		saveMeCounterIndex = 0;
		GM_Input._.CupRunLooseWon = true;
		Header_Fail.SetActive (false);
		Header_Silver.SetActive (false);
		Header_Gold.SetActive (false);
		NextRunBTN.SetActive (false);
		RetryBTN.SetActive (false);
		coinsGoalParent.SetActive (false);
		goalParent.SetActive (true);
//		silverBonus.SetActive (false);
//		failBonus.SetActive (false);

		switch (points) {
			case -1:{																				//	----- Crash

				AudioManager.Instance.OnGameLost (); //the lost sound

				//WS_Title.text = DAO.Language.GetString("run-failed");  //title text

				playerName.text = playerData.player.playerName;
				Header_Fail.SetActive (true);  
				RetryBTN.SetActive (true);

				CoinsCollectedTXT.text = Run.Instance.runGoal.coinsCollected.ToString ();
				//CoinsBonus.SetActive (false);
//				if (DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.CUP) > 0) {
//					RetryPrice.text = DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.CUP).ToString ();
//				} else {
//					RetryPrice.text = "Play AD";
//				}

				if (PlayerController.instance.hardDamage) {
					UnitedAnalytics.LogEvent ("failed run", "Hard", GameManager.CurrentRun.id.ToString(), RunManager.LastSectionNumber);
				} else {
					UnitedAnalytics.LogEvent ("failed run", "Soft", GameManager.CurrentRun.id.ToString(), RunManager.LastSectionNumber);
				}

				//failBonus.SetActive (true);
				//totalCoinsAchived.text = (Run.Instance.runGoal.coinsCollected + 0).ToString ();

				medalWon = false;

				break;
			}

		case 0:{		//	----- Failed
				GameManager.CurrentRun.NumOfFails++;
				AudioManager.Instance.OnGameLost ();

				//WS_Title.text = DAO.Language.GetString ("run-failed");
				playerName.text = playerData.player.playerName;
				Header_Fail.SetActive (true);
				RetryBTN.SetActive (true);

				CoinsCollectedTXT.text = Run.Instance.runGoal.coinsCollected.ToString ();
				//CoinsBonus.SetActive (false);
				//RetryPrice.text = "x" + DAO.Settings.RetryRunCost;

//				if (DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.CUP) > 0) {
//					RetryPrice.text =  DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.CUP).ToString ();
//				} else {
//					RetryPrice.text = "Play AD";
//				}
				UnitedAnalytics.LogEvent ("failed run", "didnt reach goal", GameManager.CurrentRun.id.ToString(), RunManager.LastSectionNumber);

//				failBonus.SetActive (true);
//				totalCoinsAchived.text = (Run.Instance.runGoal.coinsCollected + 0).ToString ();

				medalWon = false;

				break;
			}

			case 1:{																			//	----- Rank B

				//WS_Title.text = DAO.Language.GetString ("run-completed");

				Header_Silver.SetActive (true);

				if(ThereIsNextRun()) NextRunBTN.SetActive (true);
//				if (GameManager.ActiveCupCompleted) {
//					RetryBTN.SetActive (true);
//				}

				//CoinsCollectedTXT.text = (Run.Instance.runGoal.coinsCollected + 50).ToString ();
				CoinsCollectedTXT.text = Run.Instance.runGoal.coinsCollected.ToString ();

//				if (PlayerController.instance.Perk_MoneyBonusIfRunCompleted > 0) {
//					CoinsBonusTXT.text = "X" + PlayerController.instance.Perk_MoneyBonusIfRunCompleted + " " + DAO.Language.GetString ("bonus-chips-txt");
//					CoinsBonus.SetActive (true);
//				}
//				else {
//					CoinsBonus.SetActive (false);
//				}

				UnitedAnalytics.LogEvent ("finish run", "Rank B", GameManager.CurrentRun.id.ToString(), Run.Instance.runGoal.coinsCollected);

				//goldBonus.SetActive (false);
//				silverBonus.SetActive (true);
//				totalCoinsAchived.text = (Run.Instance.runGoal.coinsCollected + 50).ToString ();
				medalWon = true;
				GameManager.Instance.needToShowRateUsPopup = true;
				break;
			}
			case 2:{																			//	----- Rank A

				//WS_Title.text = DAO.Language.GetString ("run-completed");

				Header_Gold.SetActive (true);

				if(ThereIsNextRun()) NextRunBTN.SetActive (true);

				//CoinsCollectedTXT.text = (Run.Instance.runGoal.coinsCollected + 100).ToString ();
				CoinsCollectedTXT.text = Run.Instance.runGoal.coinsCollected.ToString ();

//				if (PlayerController.instance.Perk_MoneyBonusIfRunCompleted > 0) {
//					CoinsBonusTXT.text = "X" + PlayerController.instance.Perk_MoneyBonusIfRunCompleted + " " + DAO.Language.GetString ("bonus-chips-txt");
//					CoinsBonus.SetActive (true);
//				} else {
//					CoinsBonus.SetActive (false);
//				}

				UnitedAnalytics.LogEvent ("finish run", "Rank A", GameManager.CurrentRun.id.ToString(), Run.Instance.runGoal.coinsCollected);

				//goldBonus.SetActive (true);
				//silverBonus.SetActive (false);
				//totalCoinsAchived.text = (Run.Instance.runGoal.coinsCollected + 100).ToString ();
				medalWon = true;
				GameManager.Instance.needToShowRateUsPopup = true;
				break;
			}
		}

		// Set Goal Status
		//GoalImage2.SetActive (false);
		//GoalNumsContainer.SetActive (true);
		//Goal_goal.gameObject.SetActive (true);
		//coinsGoalAmount.gameObject.SetActive (false);

		if (Run.Instance.runGoal.goal == RunGoal.GoalType.COLLECT_COINS) {
			goalDiscription.text = DAO.Language.RunGoal_CollectCoins.Rank_A_Goal;
			GoalIcon.sprite = GoalImage_Coins;
//			if(points <= 0) Goal_Achieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_FOR_1_MEDALS.ToString();
//			else Goal_Achieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_FOR_2_MEDALS.ToString();
			goalParent.SetActive (false);
			coinsGoalParent.SetActive (true);
			if(points == -1) coinsGoalAchieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_FOR_2_MEDALS.ToString();
			else if(points == 0) coinsGoalAchieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_FOR_1_MEDALS.ToString();
			else coinsGoalAchieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_FOR_2_MEDALS.ToString();


		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.COLLECT_COINS_HARD) {
			goalDiscription.text = DAO.Language.RunGoal_CollectCoinsHard.Rank_A_Goal;
			GoalIcon.sprite = GoalImage_Coins;
			//Goal_Achieved.text = Run.Instance.runGoal.coinsCollected.ToString ();
			if(points == -1) Goal_Achieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_HARD_FOR_2_MEDALS.ToString();
			else if(points == 0) Goal_Achieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_HARD_FOR_1_MEDALS.ToString();
			else Goal_Achieved.text = Run.Instance.runGoal.coinsCollected + "/" + RunGoal.COINS_COLECT_HARD_FOR_2_MEDALS.ToString();

		
//		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.COMPLETE_RUN) {
//			goalDiscription.text = DAO.Language.RunGoal_CompleteRun.Rank_A_Goal;
//			GoalIcon.sprite = GoalImage_DGH;
//			Goal_Achieved.text = Run.Instance.runGoal.hits.ToString ();
//			Goal_goal.gameObject.SetActive (false);
//			if(points <= 0) Goal_goal.text = "/" + RunGoal.COMPLETE_RUN_HITS_TO_1_MEDALS.ToString();
//			else if(points == 1) Goal_goal.text = "/" + RunGoal.COMPLETE_RUN_HITS_TO_1_MEDALS.ToString();
//			else Goal_goal.text = "/" + RunGoal.COMPLETE_RUN_HITS_TO_2_MEDALS.ToString();


		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS) {
			goalDiscription.text = DAO.Language.RunGoal_CompleteRunInLessXSec.Rank_A_Goal;
			//goalDiscription.text = DAO.Language.GetString ("btc");
			GoalIcon.sprite = GoalImage_Time;
			//Goal_Achieved.text = Run.Instance.runGoal.playTime.ToString ();
			if(points == -1) Goal_Achieved.text = Run.Instance.runGoal.playTime + "/" + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL;
			else if(points == 0) Goal_Achieved.text = Run.Instance.runGoal.playTime + "/" + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL;
			else  Goal_Achieved.text = Run.Instance.runGoal.playTime + "/" + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL;
//			else if(points == 1) Goal_goal.text = "/" + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL.ToString();



		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD) {
			goalDiscription.text = DAO.Language.RunGoal_CompleteRunInLessXSecHard.Rank_A_Goal;
			//goalDiscription.text = DAO.Language.GetString ("btc");
			GoalIcon.sprite = GoalImage_Time;
			if(points == -1) Goal_Achieved.text = Run.Instance.runGoal.playTime + "/" + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL;
			else if(points == 0) Goal_Achieved.text = Run.Instance.runGoal.playTime + "/" + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_1_MEDAL;
			else  Goal_Achieved.text = Run.Instance.runGoal.playTime + "/" + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL;




		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.DONT_GET_HIT) {
			goalDiscription.text = DAO.Language.RunGoal_DontGetHit.Rank_A_Goal;
			GoalIcon.sprite = GoalImage_DGH_And_DUA;
//			GoalImage2.SetActive (true);
//			GoalNumsContainer.SetActive (false);
			if(PlayerController.instance.HitsRemain < 0)
				Goal_Achieved.text = "0";
			else
				Goal_Achieved.text = PlayerController.instance.HitsRemain.ToString();


		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.KICK_BALL_AT_SPACESHIP) {
			goalDiscription.text = DAO.Language.RunGoal_KickBallAtSpaceShip.Rank_A_Goal;
			//goalDiscription.text = DAO.Language.GetString ("ht");
			GoalIcon.sprite = GoalImage_Smash;
			if(points == -1) Goal_Achieved.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL.ToString();
			else if(points == 0) Goal_Achieved.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_FOR_1_MEDAL.ToString();
			else Goal_Achieved.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL.ToString();
//			Debug.Log ("alon__________ Points at the end: " + points);
//			Debug.Log ("alon__________ spaceShips down at the end: " + Run.Instance.runGoal.spaceShipsDown);



		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.KICK_BALL_AT_SPACESHIP_HARD) {
			goalDiscription.text = DAO.Language.RunGoal_KickBallAtSpaceShip.Rank_A_Goal;
			//goalDiscription.text = DAO.Language.GetString ("ht");
			GoalIcon.sprite = GoalImage_Smash;
			if(points == -1) Goal_Achieved.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_HARD_FOR_2_MEDAL.ToString();
			else if(points == 0) Goal_Achieved.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_HARD_FOR_1_MEDAL.ToString();
			else Goal_Achieved.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_HARD_FOR_2_MEDAL.ToString();


//		}else if (Run.Instance.runGoal.goal == RunGoal.GoalType.TACKLE_ENEMIES) {
//			goalDiscription.text = DAO.Language.RunGoal_TackleEnemies.Rank_A_Goal;
//			GoalIcon.sprite = GoalImage_Tackle;
//			Goal_Achieved.text = Run.Instance.runGoal.enemiesTackled.ToString ();
//			if(points <=0 ) Goal_goal.text = "/" + RunGoal.TACKLE_ENEMIES_FOR_1_MEDAL.ToString();
//			else if(points == 1) Goal_goal.text = "/" + RunGoal.TACKLE_ENEMIES_FOR_2_MEDAL.ToString();
//			else Goal_goal.text = "/" + RunGoal.TACKLE_ENEMIES_FOR_2_MEDAL.ToString();
		}
	}

	public void ShowSilverAchieved(){
		an.Play ("CupRun_SilverAchieved", 0);
		//BarRankFX ();
	}

	public void ShowGoldAchieved(){
		an.Play ("CupRun_GoldAchieved", 0);
		//BarRankFX ();
	}

//	public GameObject barRankFX;
	//public ParticleSystem barRankParticlse;

	public void BarRankFX(){
//		barRankFX.SetActive (true);
		//barRankParticlse.Play ();
		//CrossSceneUIHandler.Instance.SparklesFX (2f, barRankFX.transform.position, new Color (255f, 252f, 119f), 1.5f , 8 , true);
//		StartCoroutine (BarRankFXCoroutine());
	}

	IEnumerator BarRankFXCoroutine(){
		yield return new WaitForSeconds (1.2f);
//		barRankFX.SetActive (false);
		//barRankParticlse.Stop ();
		//yield return new WaitForSeconds (5f);
	}


	public void StopRun(){
		Cup activeCup;
		if (GameManager.ActiveCupCompleted) {
			activeCup = GameManager.ActiveCompletedCup;
		} else {
			activeCup = GameManager.ActiveCup;
		}
        
		//		GameManager.RequestUnpause ();
		an.Play ("CupRun_unpause", 1);
		GM_Input._.CupRunLooseWon = false;
		if (GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
		} else {
			//Debug.Log ("alon____________ StopRun() - GoalAchieved: " + GameManager.CurrentRun.GoalAchieved);
			if (GameManager.CurrentRun.GoalAchieved == 0) {
				GameManager.CurrentRun.status = RunAbstract.RunStatus.FAILED;
				GameManager.CurrentRun.NumOfFails++;
				//Debug.Log ("alon____________ StopRun() - NumOfFails: " + GameManager.CurrentRun.NumOfFails);
			} else {
				GameManager.CurrentRun.status = RunAbstract.RunStatus.COMPLETED;
			}
			// Reduce player Fitness according to run cost
			//playerData.fitness -= activeCup.cost;
			//if(GameManager.Instance.isPlayscapeLoaded)	
			//BI._.Inventory_Downgrade (activeCup.cost, PlayerController.instance.data.player.playerName);
			GameAnalyticsWrapper.ResourceEvent(false,GameAnalyticsWrapper.CURR_ENERGY,activeCup.cost,GameAnalyticsWrapper.PAY_ENERGY,"StoppedRun");

			GM_Input._.TutorialPause = false;
			GM_Input._.CupRunPause = false;
//			AudioManager.Instance.PreperToEndRunCrowd (false);

			activeCup.SaveProgress ();
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.RUNS_SCREEN);
		}
	}


	public void SaveMeByAd()
	{	
        SaveMeCounterActive = false;
		
		#if UNITY_EDITOR
        HeyzapWrapper.CallBackAdditive();
		SaveMe(true);   
		adSaveMeCounterIndex++;   
		#endif
		HeyzapWrapper.listener = delegate(string adState, string adTag) {
			if (adState.Equals ("incentivized_result_complete")) {
				SaveMe(true);
				HeyzapWrapper.CallBackAdditive();
				adSaveMeCounterIndex++;
			}
			if (adState.Equals ("incentivized_result_incomplete")) {
				// The user did not watch the entire video and should not be given a   reward.
			}
		};
		HeyzapWrapper.SetListener ();
        AudioManager.Instance.StopWatch(false);
		HeyzapWrapper.Show ();
	}
	public void SaveMeBtnClicked()
	{
		SaveMe ();
	}
	public void SaveMe (bool isByAd = false)
	{
		if (!isByAd) {
		int saveMePrice = DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.CUP);
		if (saveMePrice > 0) {
			RetryPrice.text = saveMePrice.ToString ();
			if (DAO.TotalCoinsCollected < saveMePrice) {
				CrossSceneUIHandler.Instance.showStore (CrossSceneUIHandler.ShopStates.COINS);
				SaveMeCounterActive = false;
				return;
			}
		
				DAO.TotalCoinsCollected -= saveMePrice;

				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Withdraw (saveMePrice, "Retry Used", "Retry Used", "", "", "");
				GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, saveMePrice, GameAnalyticsWrapper.SAVE_ME, "SaveMe");
				saveMeCounterIndex++; 
				CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
			}
			if (!GameManager.ActiveCupCompleted) {
				UnitedAnalytics.LogEvent ("Save Me used", UserData.Instance.userType.ToString (), GameManager.ActiveCup.name, GameManager.CurrentRun.id);
			}
		}
			
		if (!GameManager.ActiveCupCompleted) {
			UnitedAnalytics.LogEvent ("Save Me used", UserData.Instance.userType.ToString (), GameManager.ActiveCup.name, GameManager.CurrentRun.id);
		} else {
			UnitedAnalytics.LogEvent ("Save Me used", UserData.Instance.userType.ToString (), GameManager.ActiveCompletedCup.name, GameManager.CurrentRun.id);
		}

		//Run.Instance.runGoal.ResetAll();
		//GameManager.SwitchState (GameManager.GameState.CUP_RUN);

		// ----

		HideSaveMeScreen (false);
		Run.Instance.runGoal.playerDie = false;
		Run.Instance.runGoal.hits = 0;
		//UIUpdater_cupRun.instance.SetProgressBar ();
		SaveMeClicked = true;
		countDownParent.SetActive(true);
		PlayerController.instance.Respown ();
		an.Play ("pause_count_down" , 9 , 0f);
		countingDown = true;
		SMTMPV2.x = SMPB_MIN;
		SaveMeCounterActive = false;
		saveMeTimerIsOn = false;
		saveMeBtn.enabled = false;
		saveMeBtn2.enabled = false;
		UIUpdater_cupRun.instance.ResetPBOnSaveMe ();
	}

	public void Retry(){
		Cup activeCup;
		if (GameManager.ActiveCupCompleted) {
			activeCup = GameManager.ActiveCompletedCup;
		} else {
			activeCup = GameManager.ActiveCup;
		}
		// Reduce player Fitness according to run cost
		//playerData.fitness -= activeCup.cost;
		//if(GameManager.Instance.isPlayscapeLoaded)	
		//BI._.Inventory_Downgrade (activeCup.cost, PlayerController.instance.data.player.playerName);
		GameAnalyticsWrapper.ResourceEvent(false,GameAnalyticsWrapper.CURR_ENERGY,activeCup.cost,GameAnalyticsWrapper.PAY_ENERGY,"Retry");
		CrossSceneUIHandler.Instance.sparklesOn = false;
		Run.Instance.runGoal.ResetAll();
		GameManager.SwitchState (GameManager.GameState.CUP_RUN);
		saveMeCounterIndex = 0;

	}

	void OnIAPMadeAfterRestartClicked(string sku){
		IAP.AdditionalIAPCallback -= OnIAPMadeAfterRestartClicked;
		UnitedAnalytics.LogEvent ( "IAP from retry", sku, UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers );
	}

	public void EndRun(){
		if (countingDown) {
			return;
		}
		HideWonScreen ();
		GM_Input._.CupRunLooseWon = false;
		CrossSceneUIHandler.Instance.sparklesOn = false;
		saveMeCounterIndex = 0;
		if (GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
		} else if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			GM_Input._.TutorialYouReady2 = false;
			LocalDataInterface.Instance.tutorialState = 4;
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
		} else {
			Cup activeCup;
			if (GameManager.ActiveCupCompleted) {
				activeCup = GameManager.ActiveCompletedCup;
			} else {
				activeCup = GameManager.ActiveCup;
			}
			//if(activeCup != null)	Debug.Log ("alon____________ EndRun() - cup status: " + activeCup.status.ToString ());
			// Reduce player Fitness according to run cost
			//playerData.fitness -= activeCup.cost;
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Inventory_Downgrade (activeCup.cost, PlayerController.instance.data.player.playerName);
			GameAnalyticsWrapper.ResourceEvent(false,GameAnalyticsWrapper.CURR_ENERGY,activeCup.cost,GameAnalyticsWrapper.PAY_ENERGY,"StoppedRun");

			//Debug.Log ("alon__________ EndRun() - medalsWon: " + Run.Instance.runGoal.medalsWon ());
			if (Run.Instance.runGoal.medalsWon () == 0 && GameManager.CurrentRun.status != RunAbstract.RunStatus.COMPLETED) {
				GameManager.CurrentRun.status = RunAbstract.RunStatus.FAILED;
				GameManager.CurrentRun.NumOfFails++;
				//Debug.Log ("alon__________ EndRun() - NumOfFails: " + GameManager.CurrentRun.NumOfFails);
			}

			// Update Run progress if it > than previous
			if (GameManager.CurrentRun.GoalAchieved < Run.Instance.runGoal.medalsWon ()) {
				GameManager.CurrentRun.GoalAchieved = Run.Instance.runGoal.medalsWon ();
                ///implement - add medals colors per quest
                //AddMedalsPerQuest(GameManager.CurrentRun.GoalAchieved,GameManager.CurrentRun.status == RunAbstract.RunStatus.COMPLETED);
				GameManager.CurrentRun.status = RunAbstract.RunStatus.COMPLETED;
			}

			activeCup.SaveProgress ();
			// GOTO Bench
			if (ThereIsNextRun ()) {
				GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.RUNS_SCREEN);
			} else {
				GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.CUPS_SCREEN);
			}
		}
	}

//    public void AddMedalsPerQuest(int medalColor, bool isImprovement)
//    {
//        KeyValuePair<int, KeyValuePair<string,int>> s = new KeyValuePair<int, KeyValuePair<string, int>>();
//        s.Value = new KeyValuePair<string, int>();
//
//        s.Key = GameManager.ActiveCupCompleted ? GameManager.ActiveCompletedCup.id : GameManager.ActiveCup.id;
//        //GameManager.CurrentRun.
//        if (medalColor == 2)
//        {
//            s.Value.Key = "GOLD";
//            s.Value.Insert(GameManager.CurrentRun.IndexInQuest, "GOLD");
//        }
//        else
//        {
//            s.Value.Key = "GOLD";
//            s.Value.Insert(GameManager.CurrentRun.IndexInQuest, "SILVER");
//        }
//
//    }

	public bool ThereIsNextRun(){
		if (!GameManager.ActiveCupCompleted && GameManager.CurrentRun.IndexInQuest + 1 < GameManager.ActiveCup.numOfRuns && GameManager.ActiveCup.status != Cup.CupStatus.COMPLETED) {
			//Debug.Log ("alon___________ ThereIsNextRun() - true - CurrentRun.IndexInQuest + 1 = " + (GameManager.CurrentRun.IndexInQuest + 1) + " , ActiveCup.numOfRuns = " + GameManager.ActiveCup.numOfRuns);
			GameManager.Log(" This is a  " + (GameManager.CurrentRun.IndexInQuest + 1) + " run of "+GameManager.ActiveCup.numOfRuns+" quest runs");
			return true;
		} 
		//Debug.Log ("alon___________ ThereIsNextRun() - false");
		return false;
	}

	public void NextRun(){
		Cup activeCup;
		if (GameManager.ActiveCupCompleted) {
			activeCup = GameManager.ActiveCompletedCup;
			//Debug.Log ("alon___________ NextRun() - ActiveCupCompleted= " + GameManager.ActiveCupCompleted + " - activeCup = GameManager.ActiveCompletedCup");
		} else {
			activeCup = GameManager.ActiveCup;
			//Debug.Log ("alon___________ NextRun() - ActiveCupCompleted= " + GameManager.ActiveCupCompleted + " - activeCup = GameManager.ActiveCup");
		}

		HideWonScreen ();
		GM_Input._.CupRunLooseWon = false;
		CrossSceneUIHandler.Instance.sparklesOn = false;

		// Reduce player Fitness according to run cost
		//		PlayerController.instance.data.fitness -= activeCup.cost;
		//		BI._.Inventory_Downgrade (activeCup.cost, PlayerController.instance.data.player.playerName);

		// Update Run progress if it > than previous
		if (GameManager.CurrentRun.GoalAchieved < Run.Instance.runGoal.medalsWon ()) {
			GameManager.CurrentRun.GoalAchieved = Run.Instance.runGoal.medalsWon ();

			GameManager.CurrentRun.status = RunAbstract.RunStatus.COMPLETED;
		}

		activeCup.SaveProgress ();

		//PlayerController.instance.data.fitness -= activeCup.cost;
		if (playerData.fitness < 1) {
			playerData.fitness = 1;
		}
		// Check Energy, if enougth go to next run, if no go to locker room
		if( playerData.fitness >= activeCup.cost ){
			//GameManager.CurrentRun = activeCup.selectedRuns[ GameManager.CurrentRun.IndexInQuest+1 ];
			//Debug.Log("RON______________________GameManager.CurrentRun.IndexInQuest+1"+(GameManager.CurrentRun.IndexInQuest+1).ToString() + "GameManager.ActiveCup.numOfRuns"+GameManager.ActiveCup.numOfRuns);

			if (GameManager.CurrentRun.IndexInQuest + 1 < GameManager.ActiveCup.numOfRuns) {
				int indexInQuest = GameManager.CurrentRun.IndexInQuest;
				int questID = GameManager.CurrentRun.id;
//				for (int i = 0; i < activeCup.selectedRuns.Count; i++) {
//					Debug.Log ("RON__________________selectedRuns " + i + " goal" + activeCup.selectedRuns [i].goal.ToString () + "IndexInQuest" + activeCup.selectedRuns [i].IndexInQuest.ToString ());
//				}

				GameManager.CurrentRun = activeCup.selectedRuns [GameManager.CurrentRun.IndexInQuest + 1];
				if (GameManager.CurrentRun.IndexInQuest == 0) {
					JSONNode tmpRunJSON;                     // temporary fix to a bug in activeCup.selectedRuns
					tmpRunJSON = DAO.getCupRunByIndex (GameManager.CurrentRun.id + 1);
					GameManager.CurrentRun.goal = RunGoal.GetGoalTypeByGoalString (tmpRunJSON ["goal"]);
					GameManager.CurrentRun.IndexInQuest = indexInQuest + 1;
				}
			}
			Retry ();
		}
		else{
			//PlayerController.instance.data.fitness -= activeCup.cost;
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.LOW_ENERGY);
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Inventory_Downgrade (activeCup.cost, PlayerController.instance.data.player.playerName);
			GameAnalyticsWrapper.ResourceEvent(false,GameAnalyticsWrapper.CURR_ENERGY,activeCup.cost,GameAnalyticsWrapper.PAY_ENERGY,"NextRun");
		}

	}


	// Tutorial methods:

	//int index;

	//GameObject newCoin;
	//public GameObject coinPrefab;

	//public RectTransform coinStartPos;
	//public RectTransform bankPos;
	RectTransform newCoinRectTransform;

	//public Text tutorialHeaderTxt;

	//public Transform coinParent;

	WaitForEndOfFrame waitToEndFrame = new WaitForEndOfFrame ();


	public void TutSwipePanelUp(bool on){
		if (on) {
			an.Play ("swipe_pannel_up");
		} else {
			an.Play ("swipe_pannel_down");
		}
	}

	public void TutSwipeRight(){
		an.Play ("swipe_right");
	}

	public void TutSwipeLeft(){
		an.Play ("swipe_left");
	}

	public void TutSwipeUp(){
		an.Play ("swipe_up");
	}

	public void TutSwipeDown(){
		an.Play ("swipe_down");
	}

	public void AddBoxTransform(GameObject box){
		boxTransforms.Add (box);
	}

	public void TutHit(){
		an.Play ("hit");
		MoveTapUiToBox ();
	}

	public RectTransform tapToShotTransform;
	public List<GameObject> boxTransforms = new List<GameObject>();
	int boxIndex = 0;
	public Camera tutorialRunCamera;
	Vector3 boxPos;
	Vector3 tapToShotPos;

	public void MoveTapUiToBox(){
		boxPos = boxTransforms [boxIndex].transform.position;
		tapToShotPos = tutorialRunCamera.WorldToScreenPoint (boxPos);
		//tapToShotPos.x = 0f;
		tapToShotPos.z = 0f;
		tapToShotTransform.position = tapToShotPos;
		//Debug.Log ("alon____________ boxPos: " + boxPos);
		//Debug.Log ("alon____________ tapToShotPos: " + tapToShotPos);
		//Debug.Log ("alon____________ boxTransform.name: " + boxTransforms[boxIndex].name);
		boxIndex++;

		//tapToShotTransform.position = new Vector2 (30f, 30f);
		//boxPos = boxTransform.position;
		//tapToShotPos.y = boxPos.y;
		//tapToShotTransform.anchoredPosition = tapToShotPos;
	}

	public void TutHitGlass(){
		an.Play ("hit glass");
	}

	public void TutTackle(){
		an.Play ("tackle");
	}

	public void TutHideAnim(){
		an.Play ("hide_anim");
	}

	public void YouAreReady1Up(bool on){
		if (on) {
			GM_Input._.TutorialYouReady = true;
			an.Play ("you_are_ready1_up");
		} else {
			GM_Input._.TutorialYouReady = false;
			//GM_Input._.TutorialYouReady2 = true;
			an.Play ("you_are_ready1_down");
		}
	}

	public Button collectBtn;
	public void CoinsMoveToBankBtn(){
		collectBtn.enabled = false;
		LocalDataInterface.Instance.tutorialState = 4;
		//DAO.TotalCoinsCollected += DAO.Settings.TutorialBonus;
		CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (15 , DAO.Settings.TutorialBonus , collectBtn.gameObject.transform.position);
		AudioManager.Instance.OnCoinPick();
		StartCoroutine(CoinsMomeToBankCoroutine ());
		//if (GameManager.Instance.isPlayscapeLoaded) {
			//BI._.Flow_FTUE (BI.FLOW_STEP.COLLECTED_COINS);
			//BI._.Wallet_Deposit (DAO.Settings.TutorialBonus, "Tutorial Bonus", "FCB Coins", "FTUE", "15", "");
		GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,DAO.Settings.TutorialBonus, GameAnalyticsWrapper.REWARD,"TutorialBonus");
		//}
	}

//	public void CoinsMoveToBank(int coinAmount){
//		StartCoroutine(CoinsMomeToBankCoroutine (coinAmount));
//	}

	IEnumerator CoinsMomeToBankCoroutine(){
		
//		while (coinAmount > 0) {
//			coinAmount -= 100;
//			CoinMoveToBankCoroutine();
//			yield return new WaitForSeconds (0.1f);
//		}
		yield return new WaitForSeconds (0.75f);
		YouAreReady1Up (false);
		GM_Input._.TutorialYouReady = false;
		yield return new WaitForSeconds (1.25f);

		UnitedAnalytics.LogEvent ("Tutorial Completed", "Completed", PlayerController.instance.playerScript.PlayerID.ToString (), DAO.Settings.TutorialBonus); 
		EndRun ();
	}


	public void PwrUpBtnsIn(){
			an.Play ("tutorialRun_powerUpBtnsIn");
	}

	public void PwrUpBtnsToggle(bool on){
		if (on) {
			an.Play ("tutorialRun_powerUpBtnsIn");
		} else {
			an.Play ("tutorialRun_powerUpBtnsOut");
		}
	}



	void OnApplicationFocus(bool focusStatus) {
		#if !UNITY_EDITOR
		if (!GameManager.Paused && !PlayerController.instance.isDead) {
			Pause ();
		}
		#endif


	}


	public void SecretPanelToggle(bool on){
		if (on) {
			an.Play ("CupRun_secret_panel_in");
		} else {
			an.Play ("CupRun_secret_panel_out");
		}
	}


	public void RedFrameToggle(){
		an.Play ("red_frame" , 8 , 0f);
	}


	public void BottomGoalDiscriptionToggle(bool on){
		if (on) {
			an.Play ("CupRun_goal_discription in");
		} else {
			an.Play ("CupRun_goal_discription out");
		}
	}


}
