using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Facebook.Unity;
using Amazon;
using OnePF;

public class MainMenu_Actions : MonoBehaviour {

	public static MainMenu_Actions instance;

	public Animator an;
	public RawImage userPhoto;
	public Text userNameTXT;

	public GameObject AC_BTN;
	public Text AC_Name;
	public Text AC_Time;

	public GameObject GiftNotification;
    public Button restorePurchasesBtn;
    public DailyGifts dailyGifts;
    public Text dailyGifts_header;
    public Text dailyGifts_claim;
    public Text dailyGifts_freePlayer;

	WaitForSeconds ACUpdateInterval;

	public static event Action OnMenuClosed;
	public bool FBProgressionToggleOn;


	void Awake(){
		instance = this;

	}

	void Start ()
	{
		userNameTXT.text = UserData.Instance.UserName;
		//GameManager.BenchInPreselectedRunMode = false;
		ACUpdateInterval = new WaitForSeconds (60f);
		restorePurchasesBtn.interactable = !DAO.isRestoredPurchases;
		if (!DAO.Instance.InitFacebookWithDDB && DAO.FBConnected && FB.IsLoggedIn) {
			DAO.Instance.InitFacebookWithDDB = true;
			DDB._.ReadData ();
			DDB._.CheckGiftCards ();     
			DDB._.CheckGiftCardsRequests ();
		}
		FBProgressionToggleOn = false;
		if (!DAO.IsFreePlayerClaimed) {
			if (DAO.DailyGiftsLast.Day != DateTime.Now.Day) {
				dailyGifts.gameObject.SetActive (true);
			}
		}

        dailyGifts_claim.text = DAO.Language.GetString("claim");
        dailyGifts_freePlayer.text = DAO.Language.GetString("free");
        dailyGifts_header.text = DAO.Language.GetString("daily-gifts");
	//	Ads._.OnMainMenu ();
	}

	void OnEnable(){
		if (FB.IsLoggedIn) {
			OnFacebookLogginComplete ();
		}

		FBModule.OnFacebookProfileRecieved += OnFacebookLogginComplete;
		DDB.OnRecievedGiftCardsChanged += OnRecievedGiftCardsChanged;
		DDB.OnGiftCardsClaimed += OnGiftCardsAdded;

		InvokeRepeating ("CheckRecievedCardsAndRequests", 5f, 15f);
	}

	void OnDisable(){
		FBModule.OnFacebookProfileRecieved -= OnFacebookLogginComplete;
		DDB.OnRecievedGiftCardsChanged -= OnRecievedGiftCardsChanged;
		DDB.OnGiftCardsClaimed -= OnGiftCardsAdded;
		OnMenuClosed = null;

		CancelInvoke ("CheckRecievedCardsAndRequests");
	}

	void CheckRecievedCardsAndRequests(){
		if (FB.IsLoggedIn) {
			DDB._.CheckGiftCards ();
			DDB._.CheckGiftCardsRequests ();
		}
	}


	string pass = "";
	public void OnHBTNPress(string num){
		pass += num;

		StopCoroutine("resetPass");

		if (pass == "3434"){
			CrossSceneUIHandler.Instance.ConsoleShow (false);
		}

		if (pass == "4443"){
			CrossSceneUIHandler.Instance.ConsoleShow ();
		}

		StartCoroutine("resetPass");
	}

	IEnumerator resetPass(){
		yield return new WaitForSeconds (1f);
		pass = "";
	}



	public void GotoCupsScreen(){
		an.Play ("main_menu_close", 2);
		OnMenuClosed += SwitchTo_CupsScreen;
	}

	public void Start_InfintyRun(){
		an.Play ("main_menu_close", 2);
		OnMenuClosed += SwitchTo_PracticeScreen;
	}

	public void Start_TestRun(){
		an.Play ("main_menu_close", 2);
		OnMenuClosed += SwitchTo_TestRunScreen;
	}
	public void OnLogoClicked(){

		if (DAO.IsInDebugMode) {
			Start_TestRun ();
		} else {
			Start_ManageTeam ();
		}
	}

	public void Start_ManageTeam(){
		an.Play ("main_menu_close", 2);
		OnMenuClosed += SwitchTo_ManageScreen;	

	}
		


	public void OnSettingClicked(){
		AudioManager.Instance.OnSettingsBTN ();
		//an.Play ("mainmenu_settings-btn", 3);
		MainScreenUiManager.instance.SettingsScrnToggle (true);
		//OpenIAB.queryInventory ();
	}

	public void OnGiftsClicked(){
		//an.Play ("mainmenu_gift-btn", 3);

		CardsScrn_Actions.instance.CreateCards ();
		MainScreenUiManager.instance.CardScrnToggle (true);
	}

//	public void ShowCardsHandTut(){
//		if (!Tutorial.IsNeedToShowCardsHandTut) {
//			
//			an.Play ("Tutorial_anim_in");
//		}
//	}

//	public void HideCardsHandTut(){
//		an.Play ("Tutorial_anim_out");
//		//LocalDataInterface.Instance.cardsHandTut = 1;
//	}

	public void OnMyStatsClicked(){
		//an.Play ("mainmenu_stats-btn", 3);

		PlayerStatsStructure lpss = new PlayerStatsStructure ();
		lpss.SetToLocalPlayer ();

		StatsController.Instance.UpdateStats (lpss);
		StatsController.Instance.StatsScrnAnimTogge (true);

	}

	public void OnFriendsStatsClicked(){
		an.Play ("mainmenu_friendstats-btn", 3);

		// TODO: open friends stats clicked
	}

	public void MainMenuAnimIn(){
		an.Play ("main_menu_main" , 2);
	}

	public void MainMenuTutorialAnimIn(){
		an.Play ("main_menu_Tutorial_main" , 2);
	}

	public void MainMenuTutorialAnimOut(){
		an.Play ("main_menu_Tutorial_close" , 2);
	}

	public void FacebookPopupToggle(bool on){
		if (on) {
			an.Play ("main_menu_facebook_popup_in");
			GM_Input._.Facebook = true;
		} else {
			an.Play ("main_menu_facebook_popup_out");
			GM_Input._.Facebook = false;
		}
	}

	public void ShowFacebookProgression(){
		an.Play ("main_menu_facebook_progression_in", 4, 0f);
		//CrossSceneUIHandler.Instance.hideCoinsCounter ();
		//CrossSceneUIHandler.Instance.FreeGiftBtnAnimToggle (false);
	}

	public void HideFacebookProgression(){
		an.Play ("main_menu_facebook_progression_out",4, 0f);
		CrossSceneUIHandler.Instance.showCoinsCounter ();
		//CrossSceneUIHandler.Instance.FreeGiftBtnAnimToggle (true);
	}


	public void TutorialConnectedToFacebook(){
		
		//LogginToFacebook ();
		//GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.TUTORIAL_CONNECTED);
	}

	public void TutorialNotNowBTN(){
		MainMenuTutorialAnimOut ();
		//LocalDataInterface.Instance.tutorialState = 1;
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.TUTORIAL_NOT_CONNECTED);
	}

	public void TutorialNotComplete(){
		
		if (FB.IsLoggedIn) {
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.TUTORIAL_CONNECTED);
		} else {
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.TUTORIAL_NOT_CONNECTED);
		}
	}

	public static void OnMenuCloseAnimationComplete(){

		if (OnMenuClosed != null) OnMenuClosed ();

	}

	public void LogginToFacebookFromSettings(){
		FBModule.Instance.FBButonClicked = true;
		FBModule.OnFacebookProfileRecieved += OnFBLoggin_Settings;
		FBModule.Instance.Login();
	}

	public void LogginToFacebookFromTutorial(){
		FBModule.Instance.FBButonClicked = true;
		FBModule.OnFacebookProfileRecieved += OnFBLoggin_Tutorial;
		FBModule.Instance.Login();
	}

	public void LogginToFacebookFromPopup(){
		FBModule.Instance.FBButonClicked = true;
		FBModule.OnFacebookProfileRecieved += OnFBLoggin_Popup;
		FBModule.Instance.Login();
	}


	public void ContinueActiveCup(){
		an.Play ("main_menu_close", 2);
		OnMenuClosed += SwitchTo_RunsScreen;
	}

	void SwitchTo_RunsScreen(){
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.RUNS_SCREEN);
	}

	void SwitchTo_CupsScreen(){
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.CUPS_SCREEN);
	}

	void SwitchTo_PracticeScreen(){
		GameManager.BenchInPreselectedRunMode = true;
		GameManager.PreselectedGameState = GameManager.GameState.INFINITY_RUN;
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
	}

	void SwitchTo_TestRunScreen(){
		PrefabManager.instanse.ChoosePlayer (0);
		GameManager.SwitchState (GameManager.GameState.TEST_RUN);
	}

	void SwitchTo_ManageScreen(){
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
	}

	// -------------------

	void OnFacebookLogginComplete(){
		userNameTXT.text = UserData.Instance.UserName;
		userPhoto.texture = UserData.Instance.UserImageTexture;

		DDB._.CheckGiftCards ();
		DDB._.CheckGiftCardsRequests ();
	}

	void OnRecievedGiftCardsChanged(){

		if (DDB._.RecievedGiftCards.Count > 0 || DDB._.RecievedGiftCardsRequests.Count > 0) {
			GiftNotification.SetActive (true);
		} else {
			GiftNotification.SetActive (false);
		}
	}

	void OnGiftCardsAdded(){
		CardsScrn_Actions.instance.CreateCards ();
	}

	public  void OnInviteFriendClicked(){
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.INVITE_FRIENDS);
	}

	void OnFBLoggin_Tutorial(){
		FBModule.OnFacebookProfileRecieved -= OnFBLoggin_Tutorial;
		MainMenuTutorialAnimOut ();
		//an.Play ("main_menu_Tutorial_close", 2);
		//LocalDataInterface.Instance.tutorialState = 1;
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.TUTORIAL_CONNECTED);
	}

	void OnFBLoggin_Settings(){
		FBModule.OnFacebookProfileRecieved -= OnFBLoggin_Settings;

		SettingsSrn_Actions.Instance.ResetFBState ();

	}

	void OnFBLoggin_Popup(){
		FBModule.OnFacebookProfileRecieved -= OnFBLoggin_Popup;
		FacebookPopupToggle (false);
	}
		
	public void OnLeaderboardClicked(){
		//Debug.Log ("##### zlksdjghdlsjfgh ######");
		NativeSocial._.ShowLeaderboardUI ();
	}

	public void OnAchievementsClicked(){
		NativeSocial._.ShowAchievmentsUI ();
	}

	public void StartTutorialRun(){
		GameManager.SwitchState (GameManager.GameState.TUTORIAL_RUN);
	}


	public void LogoPressedAnalyticEvent(){
		UnitedAnalytics.LogEvent ("logo pressed", "pressed", UserData.Instance.userType.ToString () , DAO.PlayTimeInMinutes);
	}



	public void OnRateUsClicked(){
		#if UNITY_ANDROID || UNITY_EDITOR
		//Application.OpenURL("https://play.google.com/store/apps/details?id=com.gamehour.afc");
		Application.OpenURL(DAO.Instance.stroeAppLink);
		#elif UNITY_IOS
		//Application.OpenURL("https://itunes.apple.com/app/id1087788295");
		Application.OpenURL(DAO.Instance.stroeAppLink);
		#endif
	}

	public Text debugText;

	public void DebugLog(string log){
		debugText.text += "\n" + log;
	}


	public void SecretBtn(){
		NativeSocial._.ReportAchievment (SocialConstants.achievement_found_the_secert_button);
	}

	public void OnRestorePurchasesClicked(){
		IAP.Instance.OnRestorePurchasesClicked();
	}

	public void OnUserProfileImageClicked(){
		if (!FB.IsLoggedIn) {
			FBModule.Instance.FBButonClicked = true;
			FBModule.Instance.Login ();
		}
	}


//	public void PlayComics(){
//		Handheld.PlayFullScreenMovie ("intro.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);
//	}


	public void Test_ChooseFriend(){
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.CHOOSE_FRIEND);

		//StatsController.Instance.OnFriendForStatsClicked ("");

		//CrossSceneUIHandler.Instance.ShowPreloader ();
	}


	public void OpenPrivecyPolicyLink(){
		Application.OpenURL("http://ars.gamehour.com/fcbur-policy.html");
	}


	public void CloseDailyGfts(){
		an.Play("mainmenu_dailyGifts_out" , 7 , 0f);
	}
		

}