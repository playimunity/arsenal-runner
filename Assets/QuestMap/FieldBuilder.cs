﻿using UnityEngine;
using System.Collections;

public class FieldBuilder : MonoBehaviour
{

    public RectTransform content;
    public RectTransform top;
    public RectTransform middle;
    public RectTransform bottom;
    public RectTransform leftBanners;
    public RectTransform rightBanners;
    // Use this for initialization
    void Start()
    {
        float initHeight = middle.rect.height;
        content.sizeDelta = new Vector2(1080, CupsManager.Instance.QuestUIItems[CupsManager.Instance.QuestUIItems.Count-1].GetComponent<RectTransform>().anchoredPosition.y+100+ top.rect.height + bottom.rect.height);
        //content.sizeDelta = new Vector2(1080, CupsManager.Instance.QuestUIItems.Count * middle.rect.height + top.rect.height + bottom.rect.height);
        float middleHeightModulu = content.rect.height / initHeight - (int)(content.rect.height / initHeight);
        if (middleHeightModulu != 0)
        {
            print(middleHeightModulu);
            if (middleHeightModulu > 0.5f)
            {
                content.sizeDelta = new Vector2(content.sizeDelta.x, content.sizeDelta.y + initHeight * (1 - middleHeightModulu));
            }
            else
            {
                content.sizeDelta = new Vector2(content.sizeDelta.x, content.sizeDelta.y - initHeight * middleHeightModulu);
            }
            leftBanners.sizeDelta = rightBanners.sizeDelta = new Vector2(leftBanners.sizeDelta.x, content.sizeDelta.y);
        }
        QuestMap.instance.DefineScrollPos();
//        bottom.anchoredPosition = new Vector2(0, 0);
//        top.anchoredPosition = new Vector2(0, 0);
//        middle.sizeDelta = new Vector2(1080, GetComponent<RectTransform>().rect.height - top.rect.height * 2);
//        middle.anchoredPosition = new Vector2(0, 0);
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }
}
