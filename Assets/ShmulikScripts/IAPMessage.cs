﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IAPMessage : MonoBehaviour {

    public Text title;
    public Text message;
    public Button btn;
    public bool isConnecting;
    public float timer;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        //enable exiting the iap message window, in case the user cant connect to iap servers.
        if (isConnecting)
        {
            if (timer < 20)
            {
                timer += Time.deltaTime;
            }
            else
            {
                isConnecting = false;
                btn.interactable = true;
            }
        }
	}
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
        title.text = DAO.Language.GetString("iap-status");
        message.text = DAO.Language.GetString("connecting");
        btn.interactable = false;
        isConnecting = true;
        timer = 0;
    }
    public void ShowResult(string result)
    {
        message.text = DAO.Language.GetString(result);
        btn.interactable = true;
    }
}
