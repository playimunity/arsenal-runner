﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SecterAreaHandler : MonoBehaviour {

	public static SecterAreaHandler instance;

	public GameObject whitePlane;
	public GameObject[] ballUIElements;
	public GameObject tutUI;

	public static int numOfSectionsToMove = 15; 
	public int didHeaderIndex = 0;
	public int amountOfCoinForHeader = 10;
	int currentBall = -1;

	public bool tutorialOn = false;

	public Text coinsText; 
	WaitForSeconds wfs_0_4;
	WaitForSeconds wfs_0_2;
	void Awake(){
		instance = this;
		wfs_0_4 = new WaitForSeconds(0.4f);
		wfs_0_2 = new WaitForSeconds(0.2f);
	}

	#if UNITY_EDITOR
	void Update(){
		if (Input.GetKeyUp (KeyCode.P)) {
			OnPortalEntered ();
		}
	}
	#endif

	public void OnPortalEntered () {
		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			CupMapBuilder.secretAreaPosOffset = true;
		} else if (GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
			TutorialMapBuilder.secretAreaPosOffset = true;
		}
		//CupMapBuilder.Instance.FreezeBuildSections (true);
		//CupMapBuilder.FreezeBuilder = true;
		whitePlane.SetActive (true);
		//PlayerController.instance.sectionOf3 = true;
		PlayerController.instance.secretArea = true;
		RunManager.instance.wasSecretArea = true;
		AudioManager.Instance.SecretAreaEnter();
		CameraLookAt.instance.SetCameraPosition (4f, -4.5f, 18.45f);
		CameraFollow.instance.downUp = false;
		CameraFollow.instance.CameraBackToPlace ();
	}


	public void CreateStadium(){
		Run.Instance.MoveSectionsForwards (numOfSectionsToMove);
		StartCoroutine (BiuldStadiumSections ());
	}
		

	IEnumerator BiuldStadiumSections () {
		yield return wfs_0_4;
		//CupMapBuilder.Instance.FreezeBuildSections (false);
		//CupMapBuilder.FreezeBuilder = false;
		whitePlane.SetActive (false);
		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			CupRunController.Instance.SecretPanelToggle (true);
			CupRunController.Instance.PwrUpBtnsToggle (false);
		}
	}


//	public void LastHeaderSwitcher(bool on){
//		if (on) {
//			didLastHeader = true;
//		} else {
//			didLastHeader = false;
//		}
//	}

	public void HeaderGetCoins(){
		DAO.TotalCoinsCollected += amountOfCoinForHeader * didHeaderIndex;
		StartCoroutine (HeaderFeedback());
		currentBall+=1;

	}

	IEnumerator HeaderFeedback(){
		yield return wfs_0_2;
		CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (10 , 0);
		coinsText.text = "+"+(amountOfCoinForHeader  * didHeaderIndex);
		coinsText.gameObject.SetActive (true);
		ballUIElements [currentBall].transform.GetChild (0).gameObject.SetActive (true);
	}

	public void LosePoint(){
		currentBall+=1;
		ballUIElements [currentBall].transform.GetChild (1).gameObject.SetActive (true);
	}


	public void ActivateTutorial(){
		PlayerController.instance.ForceJumpEnd ();
		GameManager.Instance.TutorialPointPause();
		tutorialOn = true;
		tutUI.SetActive (true);
	}


	public void FinishTutorial(){
		if (!tutorialOn)	return;

		tutUI.SetActive (false);
		GameManager.Instance.TutorialPointUnpause();
		PlayerController.instance.secretAreaTutPoint = false;
		DAO.FirstSecterAreaBall = 1;
		tutorialOn = false;
	}



}
