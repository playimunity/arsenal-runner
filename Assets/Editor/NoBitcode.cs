﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

public class NoBitcode : MonoBehaviour {

	#if UNITY_IOS
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {

        if (buildTarget == BuildTarget.iOS)
        {
            Debug.Log("INSIDE NOBITCODE1");
            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject proj = new PBXProject();
            Debug.Log("BEFORE READ FROM STRING");
            proj.ReadFromString(File.ReadAllText(projPath));
            Debug.Log("AFTER READ FROM STRING");
            string target = proj.TargetGuidByName("Unity-iPhone");
            Debug.Log("START SET BUILD PROPERTY");
            proj.SetBuildProperty(target, "ENABLE_BITCODE", "false");
            Debug.Log("AFTER SET BUILD PROPERTY");
            File.WriteAllText(projPath, proj.WriteToString());
        }
    }
	#endif
}
