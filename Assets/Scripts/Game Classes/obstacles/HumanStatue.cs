﻿using UnityEngine;
using System.Collections;

public class HumanStatue : MonoBehaviour {
	

	public Obstacle.ObstacleTypes type = Obstacle.ObstacleTypes.HuemanStatue1;

	public Animator animator;

	bool colided = false;


	void Start () {
		StartCoroutine(BeKinematic());
	
	}


	void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "Player" && !colided) {
			colided = true;
			Tackle();
			AudioManager.Instance.OnHitMale ();
		}
	}


	public void Tackle(){
		animator.Play ("Statue_Fall");

		GetComponent<Collider> ().enabled = false;
		GetComponent<Rigidbody> ().isKinematic = true;

		PlayerController.instance.hardDamage = true;
		PlayerController.instance.upperBodyDamage = false;
		PlayerController.instance.Damage(10);

	}


	IEnumerator BeKinematic(){
		yield return new WaitForSeconds (1.5f);
		GetComponent<Rigidbody> ().isKinematic = true;
	}

	


}
