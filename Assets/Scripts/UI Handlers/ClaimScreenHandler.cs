﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ClaimScreenHandler : MonoBehaviour {

	public static ClaimScreenHandler _;


	bool powerUpsWon = false;

	public GiftCard.Prizes Prize;

	public Text youWonTxt;
	//public GameObject PrizesContainer_1prize;
	//public GameObject PrizesContainer_2prize;
	//public GameObject PrizesContainer_3prize;
	public GameObject cardsClaimTitle;
	public GameObject freeGiftsClaimTitle;

	public GameObject SpeedParent;
    public GameObject ShieldParent;
	public GameObject MagnetParent;
	public GameObject CoinsParent;
	public GameObject X2Parent;
	public GameObject EnergyDrinkParent;
	public GameObject PlayerParent;
	public GameObject JackpotParent;

//	public Text _1pc_p1_icon;
//	public Text _1pc_p1_text;
//	public Text _2pc_p1_icon;
//	public Text _2pc_p1_text;
//	public Text _2pc_p2_icon;
//	public Text _2pc_p2_text;
//	public Text _3pc_p1_icon;
//	public Text _3pc_p1_text;
//	public Text _3pc_p2_icon;
//	public Text _3pc_p2_text;
//	public Text _3pc_p3_icon;
//	public Text _3pc_p3_text;

	public Text SpeedValue;
    public Text ShieldValue;
	public Text MagnetValue;
	public Text coinsValue;
	public Text X2Value;
	public Text EnergyDrinkValue;
	public Text PlayerValue;
	//public Text MagnetAndAdrenalineValue;
	public Text JackpotValue;
	//public Text JackpotAdrenalineValue;
	//public Text JackpotCoinsValue;

	public Text prizeDescription;
	//public Text prizeDescription2;
	//public Text prizeDescription3;

	public Text ClaimBtnTxt;



	int CoinsPrize=0;
	int MagnetsPrize=0;
	int SpeedPrize=0;
    int ShieldPrize=0;
	int PlayerPrize=0;
	int EnergyDrinkPrize=0;
	int X2Prize=0;

	string MAGNET_ICON = "d";
	string COIN_ICON = "p";
	string ADRENALINE_ICON = "r";
	string PLAYER_ICON = "9";




	void Awake(){
		_ = this;
	}


	void OnEnable(){
		updateTitle ();
	}


	void updateTitle(){
		if (isGiftCard) {
			cardsClaimTitle.SetActive (true);
			freeGiftsClaimTitle.SetActive (false);
		} else {
			cardsClaimTitle.SetActive (false);
			freeGiftsClaimTitle.SetActive (true);
		}
	}



	public void SetView(GiftCard.Prizes Prize){
		updateTitle ();

		this.Prize = Prize;
		CoinsPrize = 0;
		MagnetsPrize = 0;
		SpeedPrize = 0;
        ShieldPrize = 0;
		PlayerPrize = 0;
		X2Prize = 0;

		SpeedParent.SetActive(false);
        ShieldParent.SetActive(false);
		MagnetParent.SetActive(false);
		CoinsParent.SetActive(false);
		X2Parent.SetActive(false);
		EnergyDrinkParent.SetActive(false);
		PlayerParent.SetActive(false);      
		JackpotParent.SetActive(false);

		//youWonTxt.text = DAO.Language.GetString("you-won");

		switch (Prize) {
			case GiftCard.Prizes.COINS_S:{
				CoinsPrize = 200;
				CoinsParent.SetActive(true);
				coinsValue.text = "200";
				prizeDescription.text = "x200 " + DAO.Language.GetString("coins-prize");
				break;
			}
			case GiftCard.Prizes.COINS_M:{
				CoinsPrize = 400;
				CoinsParent.SetActive(true);
				coinsValue.text = "400";
				prizeDescription.text = "x400 " + DAO.Language.GetString("coins-prize");
				break;
			}
			case GiftCard.Prizes.COINS_L:{
				CoinsPrize = 800;
				CoinsParent.SetActive(true);
				coinsValue.text = "800";
				prizeDescription.text = "x800 " + DAO.Language.GetString("coins-prize");
				break;
			}
			case GiftCard.Prizes.COINS_XL:{
				CoinsPrize = 3000;
				CoinsParent.SetActive(true);
				coinsValue.text = "3000";
				prizeDescription.text = "x3000 " + DAO.Language.GetString("coins-prize");
				break;
			}
			case GiftCard.Prizes.MAGNET_S:{
				MagnetsPrize = 1;
				MagnetValue.text = "1";
				MagnetParent.SetActive(true);
				coinsValue.text = "1";
				prizeDescription.text = "x1 " + DAO.Language.GetString("magnet-prize");
				powerUpsWon = true;
				break;
			}
			case GiftCard.Prizes.MAGNET_M:{
				MagnetsPrize = 2;
				MagnetValue.text = "2";
				MagnetParent.SetActive(true);
				coinsValue.text = "2";
				prizeDescription.text = "x2 " + DAO.Language.GetString("magnet-prize");
				powerUpsWon = true;
				break;
			}
			case GiftCard.Prizes.ENERGY_DRINK:{
				EnergyDrinkPrize = 1;
				EnergyDrinkValue.text = "1";
				EnergyDrinkParent.SetActive(true);
				coinsValue.text = "1";
				prizeDescription.text = "x1 " + DAO.Language.GetString("energy-drink-prize");

				break;
			}
			case GiftCard.Prizes.SPEED_S:{
				SpeedPrize = 1;
				SpeedValue.text = "1";
				SpeedParent.SetActive(true);
				coinsValue.text = "1";
				prizeDescription.text = "x1 " + DAO.Language.GetString("speed-prize");
				powerUpsWon = true;
				break;
			}
			case GiftCard.Prizes.X2_S:{
				X2Prize = 1;
				X2Parent.SetActive(true);
				X2Value.text = "1";
				prizeDescription.text = "x1 " + DAO.Language.GetString("x2-prize");
				powerUpsWon = true;
				break;
			}
			case GiftCard.Prizes.SPEED_M:{
				SpeedPrize = 3;
				SpeedValue.text = "3";
				SpeedParent.SetActive(true);
				coinsValue.text = "3";
				prizeDescription.text = "x3 " + DAO.Language.GetString("speed-prize");
				powerUpsWon = true;
				break;
			}
			case GiftCard.Prizes.SHIELD_S:{

				ShieldPrize= 1;            
				ShieldParent.SetActive(true);
				//MagnetAndAdrenalineValue.text = "1";
				ShieldValue.text = "1";

				prizeDescription.text = "x1 " + DAO.Language.GetString("shield-prize");
				powerUpsWon = true;
				break;
			}
            case GiftCard.Prizes.SHIELD_M:{

                ShieldPrize= 3;
                ShieldParent.SetActive(true);
                //MagnetAndAdrenalineValue.text = "1";
                ShieldValue.text = "3";

                prizeDescription.text = "x3 " + DAO.Language.GetString("shield-prize");
                powerUpsWon = true;
                break;
            }
			case GiftCard.Prizes.JACKPOT:{
			
			    ShieldPrize = 2;
                SpeedPrize = 2;
				CoinsPrize = 400;

				JackpotParent.SetActive(true);

				//JackpotAdrenalineValue.text = "2";
				//JackpotValue.text = "2";
				//JackpotCoinsValue.text = "400";

				prizeDescription.text = "x2 " + DAO.Language.GetString("speed-prize") + "\n" + "x2 " + DAO.Language.GetString("shield-prize") + "\n" + "x400 " + DAO.Language.GetString("coins-prize");
				powerUpsWon = true;
				break;
			}

			case GiftCard.Prizes.PLAYER:{
				PlayerPrize = 1;
				PlayerParent.SetActive(true);
				PlayerValue.text = "1";
				prizeDescription.text = "x1 " + DAO.Language.GetString("player-prize");
				break;
			}

		}

	}



	public bool isGiftCard = true;
	public void OnClaimClicked ()
	{

		if (CoinsPrize != 0) {
			CrossSceneUIHandler.Instance.UpdateMultiCoinsAmount (15 , CoinsPrize);
//			DAO.TotalCoinsCollected += CoinsPrize;

			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (CoinsPrize, "Scratch Card Reward", "FCB Coins", "", "", "");
			GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,CoinsPrize, GameAnalyticsWrapper.GIFTCARD,"GiftCard");
		}

		//DAO.TotalSpeedBoostsCollected += AdrenalinePrize;
        DAO.TotalSheildsCollected += ShieldPrize;
        DAO.TotalSpeedBoostsCollected += SpeedPrize;
		DAO.TotalMagnetsCollected += MagnetsPrize;
		DAO.TotalMedikKitsCollected += EnergyDrinkPrize;
		DAO.TotalX2CoinsCollected += X2Prize;

		if (isGiftCard) {
			CardsScrn_Actions.instance.TutPopupClaimPlay (powerUpsWon);

			CardsScrn_Actions.instance.HideClaimScreen ();
		}
		else
		{
			//CrossSceneUIHandler.Instance.mainAnimator.Play("free_gift_popup_anim out",5,0f);
			MainScreenUiManager.instance.uiAnimator.Play("ClaimScreen_Hide");
		}


	}




}
