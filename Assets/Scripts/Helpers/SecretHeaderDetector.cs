﻿using UnityEngine;
using System.Collections;

public class SecretHeaderDetector : MonoBehaviour { // the ball collider:

	public Animator secretBallAnimator;

	public SecretAreaBall ballParentScript;

	int randomNum;

	bool triggered = false;
	//bool countDownOn = false;

	//public GameObject ballParticleTrail;
	//public GameObject ball;

	//public Transform ballmesh;
	//public Transform playerTransform;
	//public Transform ballParent;

	//Vector3 ballScale;

	//Vector2 countDownPos;

	//public RectTransform countDownUI;





	void OnTriggerEnter(Collider col){
		if (!triggered && col.transform.gameObject.tag == "Player") {
			triggered = true;
			ballParentScript.triggered = true;
			if (PlayerController.instance.jumping) {
				secretBallAnimator.enabled = true;
				randomNum = Random.Range (0, 2);
				if (randomNum == 0) {
					secretBallAnimator.Play ("SecretAreaBallAnimRight");
				} else {
					secretBallAnimator.Play ("SecretAreaBallAnimLeft");
				}
				//ballParticleTrail.SetActive (true);
				AudioManager.Instance.OnKick ();
				SecterAreaHandler.instance.didHeaderIndex++;
				SecterAreaHandler.instance.HeaderGetCoins ();
			} else {
				//SecterAreaHandler.instance.LastHeaderSwitcher (false);
				SecterAreaHandler.instance.didHeaderIndex = 0;
				SecterAreaHandler.instance.LosePoint ();
			}
		}
	}
}
