﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinMoveToBank : MonoBehaviour {


	//public RectTransform bankPos;

	RectTransform rectTransform;

	Image coinImage;

	//float ops = 1f;

	Color coinColor;

	public Vector2 bankPos;



	void Start () {
		rectTransform = GetComponent<RectTransform> ();
		coinImage = GetComponent<Image> ();
		coinColor = coinImage.color;
	}
	

	void Update () {
		rectTransform.anchoredPosition = Vector2.MoveTowards (rectTransform.anchoredPosition, bankPos, 1250f * Time.deltaTime);
		coinColor.a -= 1f * Time.deltaTime;
		coinImage.color = coinColor;
		if (Vector2.Distance (rectTransform.anchoredPosition, bankPos) < 30) {
			Destroy (gameObject);
		}
	}




}
