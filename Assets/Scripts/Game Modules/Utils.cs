using UnityEngine;
using System.Collections;
using Facebook.Unity;
using SimpleJSON;
using System.Collections.Generic;
using System.Linq;
using RTLService;
using System;

public class Utils : MonoBehaviour {
	public static Utils Instance;
	public delegate void LoadPictureCallback (Texture texture);

	public enum DayInterval { MORNING, AFTERNOON, NIGTH };

	public Color FogNight;
	public Color FogMorning;
	public Color FogAfternoon;


	void Awake(){
		Instance = this;
	}

	public static string GetTimeUnitAsFormatedString(int unit){
		return (unit < 10) ?  "0" + unit : "" + unit;
	}

	public static string ConvertSecondsToMMSSString(int seconds){
		int sec = seconds % 60;
		int min = (int)Mathf.Floor (seconds / 60);
		return GetTimeUnitAsFormatedString (min) + ":" + GetTimeUnitAsFormatedString (sec);
	}

	public static string GetFBPictureURL(string facebookID, int? width = null, int? height = null, string type = null)
	{
		string url = string.Format("/{0}/picture", facebookID);
		string query = width != null ? "&width=" + width.ToString() : "";
		query += height != null ? "&height=" + height.ToString() : "";
		query += type != null ? "&type=" + type : "";
		query += "&redirect=false";
		if (query != "") url += ("?g" + query);
		return url;
	}

	IEnumerator LoadPictureEnumerator(string url, LoadPictureCallback callback){
		WWW www = new WWW(url);
		yield return www;
		callback(www.texture);
	}

	public void LoadPictureFromURL (string url, LoadPictureCallback callback){
		StartCoroutine(LoadPictureEnumerator(url,callback));
		
	}

	public void LoadPictureFromFBAPI (string url, LoadPictureCallback callback){

		FB.API(url,HttpMethod.GET,result =>{
			if (result.Error != null){
				return;
			}

			StartCoroutine(LoadPictureEnumerator( JSON.Parse( result.RawResult )["data"]["url"].Value ,callback));
		});
	}




	public static bool IsTurnLeftSection(string id){
        return id.Equals ("hrtl") 
            || id.Equals ("ostl")
            ||	id.Equals("tbex1")
            || id.Equals("cttl");
	}

	public static bool IsTurnRightSection(string id){
        return id.Equals ("hrtr") 
            || id.Equals ("ostr")
            || id.Equals("cttr")
            || id.Equals("mspreend")
            || id.Equals("msex1");
	}

	public static bool IsTurnUpSection(string id){
		return id.Equals ("g3u") 
			|| id.Equals ("g5u")
			|| id.Equals ("s3ex")
			|| id.Equals ("p3en");
	}

	public static bool IsTurnDownSection(string id){
		return id.Equals ("g3d") 
			|| id.Equals ("g5d")
			||	id.Equals("s3en");
	}

	public static bool IsTSection(string id){
		return id.Equals ("g3t") 
			|| id.Equals ("g5t")
			|| id.Equals ("gt3t")
			|| id.Equals ("gt3ts");
	}



	public static DayInterval GetDayInerval(){
		if (System.DateTime.Now.Hour >= 21 || System.DateTime.Now.Hour < 6)
			return DayInterval.NIGTH;
		else if (System.DateTime.Now.Hour >= 6 && System.DateTime.Now.Hour < 15)
			return DayInterval.MORNING;

		else if(System.DateTime.Now.Hour >= 15 && System.DateTime.Now.Hour < 21)
			return DayInterval.AFTERNOON;

		//---

		return DayInterval.MORNING;
	}

//	public static void SetFogColor(WorldSection.RegionType region){
//		if (region == WorldSection.RegionType.) {
//			SetMetroFogColor ();
//		} else {
//			SetGenericFogColor ();
//		}
//	}

	static void SetGenericFogColor(){

		RenderSettings.fogDensity = 0.009f;

		switch (GetDayInerval()) {
		case DayInterval.AFTERNOON:{
				//RenderSettings.fogColor = Utils.Instance.FogAfternoon;
				Utils.Instance.StartCoroutine( Utils.Instance.ChangeFogColorGradually(RenderSettings.fogColor, Utils.Instance.FogAfternoon) );
				break;
			}
		case DayInterval.MORNING:{
				//RenderSettings.fogColor = Utils.Instance.FogMorning;
				Utils.Instance.StartCoroutine( Utils.Instance.ChangeFogColorGradually(RenderSettings.fogColor, Utils.Instance.FogMorning) );
				break;
			}
		case DayInterval.NIGTH:{
				//RenderSettings.fogColor = Utils.Instance.FogNight;
				Utils.Instance.StartCoroutine( Utils.Instance.ChangeFogColorGradually(RenderSettings.fogColor, Utils.Instance.FogNight) );
				break;
			}
		}

	}

	static void SetMetroFogColor(){
		RenderSettings.fogDensity = 0.02f;
		//RenderSettings.fogColor = Color.black;
		Utils.Instance.StartCoroutine( Utils.Instance.ChangeFogColorGradually(RenderSettings.fogColor, Color.black) );
	}

	IEnumerator ChangeFogColorGradually(Color start, Color end){
		for (float i = 0; i < 1f; i += 2f * Time.deltaTime) {
			RenderSettings.fogColor = Color.Lerp (start, end, i);
			yield return null;
		}

		RenderSettings.fogColor = end;
	}



	public static string FixRTLString(string str){

		if (DAO.AppLanguage == "Arabic" || DAO.AppLanguage == "Hebrew") {
			return RTL.Convert (str);
		} else {
			return str;
		}

	}

	public static int CalculateRankScore(){

		if (DAO.NumOfPurchasedPlayers < 1) return 0;

		int totalMaxFitness = 0;
		int AvrgMaxFitness = 0;
		int totalMeters = 0; 
		int PuzzlePieces = DAO.TotalGoldMedals + DAO.TotalSilverMedals;

		string[] plData;
		foreach (string pid in DAO.PurchasedPlayers.Split (DAO.char_separator, StringSplitOptions.RemoveEmptyEntries)) {
			plData = DAO.GetPlayerData (int.Parse (pid)).Split (DAO.char_separator, StringSplitOptions.RemoveEmptyEntries);

			totalMeters += int.Parse(plData [13]);
			totalMaxFitness += DAO.Settings.DefaultMaxFitness + int.Parse (plData [1]);
		}

		if (totalMaxFitness < 1) totalMaxFitness = 1;
		if (totalMeters < 1) totalMeters = 1;
		//if (PuzzlePieces < 1) PuzzlePieces = 1;

		AvrgMaxFitness = (int)(totalMaxFitness / DAO.NumOfPurchasedPlayers);

		return (DAO.NumOfPurchasedPlayers * AvrgMaxFitness) + (int)(totalMeters / 700) + PuzzlePieces * 20;
	}


}

public static class IEnumerableExtensions
{

	public static IEnumerable<t> Randomize<t>(this IEnumerable<t> target)
	{
		System.Random r = new System.Random();

		return target.OrderBy(x=>(r.Next()));
	}        
}