﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CupScrn_Actions : MonoBehaviour {


	public static CupScrn_Actions Instance;
	public RectTransform scrollContent;
	public ScrollRect scroll;
	public GameObject cupUiElementPrefab;
	public GameObject firstCupUiElement;
	GameObject tempCupUiElement;

	float cupUiElementHeight = 376;

	Vector3 cupUiElementCurrentPos;

	Vector2 scrollContentSize;

	Vector2 scrollOffset;

	void Awake(){
		Instance = this;
		cupUiElementCurrentPos = new Vector3 (0f, -189f, 0);
		scrollContentSize.y = 0f;
		scrollOffset = new Vector2 (0f,200f);
	}



	public void Reset(){

		foreach (Transform child in scrollContent) {
			GameObject.Destroy(child.gameObject);
		}
		scrollContentSize.y = 0f;
		cupUiElementCurrentPos = new Vector3 (0f, -189f, 0);
	}

	public void AddCupUiEllement(Cup cup){
		scrollContentSize.y += cupUiElementHeight;
		scrollContent.sizeDelta = scrollContentSize;
		tempCupUiElement = (GameObject)Instantiate (PrefabManager.instanse.cupItemElement , transform.position , transform.rotation);
		tempCupUiElement.transform.SetParent (scrollContent.transform , false);
		tempCupUiElement.GetComponent<RectTransform>().anchoredPosition = cupUiElementCurrentPos;
		cupUiElementCurrentPos.y -= cupUiElementHeight;

		tempCupUiElement.GetComponent<CupUIElement> ().SetView (cup);
	}

	public void ScrollToAciveCup(){

		if (GameManager.ActiveCup == null || !GameManager.ActiveCup.exist) return;

		foreach (Transform child in scrollContent.transform) {
			if (child.GetComponent<CupUIElement> ().cup.id == GameManager.ActiveCup.id) {
				Canvas.ForceUpdateCanvases();
				scrollContent.anchoredPosition = (Vector2)scroll.transform.InverseTransformPoint(scrollContent.position) - (Vector2)scroll.transform.InverseTransformPoint(child.position) - scrollOffset;
				
			}
		}




	}
}
