using UnityEngine;
using SimpleJSON;
using System.Collections;

[System.Serializable]
public class RunAbstract{
	//public enum RunStatus {NOT_STARTED, FAILED, COMPLETED , DONE};
	public enum RunStatus {NOT_STARTED, FAILED, COMPLETED};
	public int id;
	public int IndexInQuest = 0;
	public RunStatus status;
	public RunGoal.GoalType goal;
	public int GoalAchieved = 0;
	public int NumOfFails = 0;

	public RunAbstract(int _id){
		id = _id;
		status = RunStatus.NOT_STARTED;
	}

	public RunAbstract(string runDecoded){
		mDecode (runDecoded);
	}

	public string mEncode(){
		string s = "";
		s += id + "|";

		if(status == RunStatus.NOT_STARTED) s += "0|";
		if(status == RunStatus.FAILED) s += "1|"; 
		if(status == RunStatus.COMPLETED) s += "2|"; 
		//if(status == RunStatus.DONE) s += "3|"; 

		s += GoalAchieved + "|";
		s += NumOfFails;

		return s;
	}

	public void mDecode(string r){
		string[] d = r.Split('|');

		id = int.Parse(d[0]);

		if(d[1] == "0") status = RunStatus.NOT_STARTED;
		if(d[1] == "1") status = RunStatus.FAILED;
		if(d[1] == "2") status = RunStatus.COMPLETED;
		//if(d[1] == "3") status = RunStatus.DONE;

		GoalAchieved = int.Parse ( d[2] );

		if (d.Length < 4) NumOfFails = 0;
		else NumOfFails = int.Parse ( d[3] );
	}


	//build completed runs:

	public RunAbstract(int runId , bool openCup){
		SetOpenCup(runId);
	}

	public void SetOpenCup(int runId){
		//Debug.Log ("alon_______________ SetOpenCup()");
		id = runId;

		status = RunStatus.COMPLETED;

		GoalAchieved = -1;

		NumOfFails = 0;
	}

	public RunAbstract(string runDecoded , bool openCup){
		mDecodeCompleted (runDecoded);
	}

	public void mDecodeCompleted(string r){
		string[] d = r.Split('|');

		id = int.Parse(d[0]);

		status = RunStatus.COMPLETED;

		GoalAchieved = int.Parse ( d[2] );

		if (d.Length < 4) NumOfFails = 0;
		else NumOfFails = int.Parse ( d[3] );
	}

}
