﻿using UnityEngine;
using System.Collections;

public class FXCamera : MonoBehaviour {

	public GameObject sFXCamera;

	public Transform cameraStartPoint;
	public Transform cameraEndPoint;
	public Transform cameraOriginalPoint;
	public Transform cameraWinPoint;
	public Transform cameraLoosePoint;

	WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
	WaitForSeconds waitForSecond = new WaitForSeconds(0.3f);

	public static FXCamera instance;

	float lerpFraction = 0;



	void Awake(){
		instance = this;
	}



	void Start(){
		StartCoroutine("StartCameraAnimation");
		StartCoroutine("InstantiatePlayer");
	}



	IEnumerator StartCameraAnimation(){
		yield return new WaitForSeconds (0.7f);
		while((Vector3.Distance(sFXCamera.transform.position , cameraEndPoint.position)) > 0.01f){
			sFXCamera.transform.position = Vector3.Lerp (sFXCamera.transform.position, cameraEndPoint.position, 1.5f * Time.deltaTime);
			sFXCamera.transform.eulerAngles = Vector3.Lerp (sFXCamera.transform.eulerAngles, cameraEndPoint.eulerAngles, 1.5f * Time.deltaTime);
			yield return waitFrame;
		}
		sFXCamera.transform.position = cameraEndPoint.position;
		sFXCamera.transform.eulerAngles = cameraEndPoint.eulerAngles;
	}


	IEnumerator InstantiatePlayer(){
		yield return new WaitForSeconds (0.7f);
		PlayerController.instance.PlayerStartAnimation ();

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN || GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
			yield return new WaitForSeconds (1.5f);
			CupRunController.Instance.RunGoalsAnimToggle (true);
//			yield return new WaitForSeconds (0.25f);
//			CupRunController.Instance.RunBtnAnimToggle (true);
		} else if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			yield return new WaitForSeconds (2.75f);
			CupRunController.Instance.TutorialCountDown();
		} else if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
			yield return new WaitForSeconds (1.75f);
			PracticeRunController.Instance.RunBtnAnimToggle (true);
		}
	}


	public void CameraMoveToOriginal(){
		StopCoroutine ("StartCameraAnimation");
		sFXCamera.transform.position = cameraEndPoint.position;
		sFXCamera.transform.eulerAngles = cameraEndPoint.eulerAngles;
		StartCoroutine ("CameraMoveToOriginalCoroutine");
	}

	IEnumerator CameraMoveToOriginalCoroutine(){
		while((Vector3.Distance(sFXCamera.transform.position , cameraOriginalPoint.position)) > 0.01f){
			sFXCamera.transform.position = Vector3.Lerp (sFXCamera.transform.position, cameraOriginalPoint.position, 7f * Time.deltaTime);
			sFXCamera.transform.eulerAngles = Vector3.Lerp (sFXCamera.transform.eulerAngles, cameraOriginalPoint.eulerAngles, 7f * Time.deltaTime);
			yield return waitFrame;
		}
		sFXCamera.transform.position = cameraOriginalPoint.position;
		sFXCamera.transform.eulerAngles = cameraOriginalPoint.eulerAngles;
		CameraManager.instance.switchCamera (CameraManager.Cameras.PLAYER_FOLLOW_CAMERA);
	}



	public void EndCameraAnimation(){
		StartCoroutine (EndCameraAnimationWinCoroutine());
	}

	public IEnumerator EndCameraAnimationWinCoroutine(){
		sFXCamera.transform.position = cameraOriginalPoint.position;
		sFXCamera.transform.eulerAngles = cameraOriginalPoint.eulerAngles;
		CameraManager.instance.switchCamera (CameraManager.Cameras.SPECIAL_EFFECTS_CAMERA);

		//while((Vector3.Distance(sFXCamera.transform.position , cameraWinPoint.position)) > 0.01f){
		while(lerpFraction < 1f){
			lerpFraction += 5f * Time.deltaTime;
//			Debug.Log ("alon_________ lerp fractions = " + lerpFraction);
			sFXCamera.transform.position = Vector3.Lerp (sFXCamera.transform.position, cameraWinPoint.position, lerpFraction);
			sFXCamera.transform.eulerAngles = Vector3.Lerp (sFXCamera.transform.eulerAngles, cameraWinPoint.eulerAngles, lerpFraction);
//			Debug.Log ("alon___________ EndCameraAnimationWinCoroutine() - while(lerpFraction < 1f) , lerpFraction = " + lerpFraction);
			yield return waitFrame;
		}
		sFXCamera.transform.position = cameraWinPoint.position;
		sFXCamera.transform.eulerAngles = cameraWinPoint.eulerAngles;
		PlayerController.instance.currentSpeed = 0f;
	}



	public IEnumerator EndCameraAnimationLooseCoroutine(){
		CameraManager.instance.switchCamera (CameraManager.Cameras.SPECIAL_EFFECTS_CAMERA);

		//while((Vector3.Distance(sFXCamera.transform.position , cameraLoosePoint.position)) > 0.01f){
		while(lerpFraction < 1f){	
			lerpFraction += 0.3f * Time.deltaTime;
			sFXCamera.transform.position = Vector3.Lerp (sFXCamera.transform.position, cameraLoosePoint.position, lerpFraction);
			sFXCamera.transform.eulerAngles = Vector3.Lerp (sFXCamera.transform.eulerAngles, cameraLoosePoint.eulerAngles, lerpFraction);
			yield return waitFrame;
		}
		sFXCamera.transform.position = cameraLoosePoint.position;
		sFXCamera.transform.eulerAngles = cameraLoosePoint.eulerAngles;
		PlayerController.instance.currentSpeed = 0f;
	}



}
