﻿using UnityEngine;
using System.Collections;

public class SkyMove : MonoBehaviour {

	MeshRenderer rend;
	Vector2 of;

	void Start () {
		rend = GetComponent<MeshRenderer> ();
		of = rend.material.mainTextureOffset;
	}
	

	void FixedUpdate () {
		of.x  += 0.0001f;
		rend.material.mainTextureOffset = of;
	}
}
