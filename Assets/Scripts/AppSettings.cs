﻿using UnityEngine;
using System.Collections;

public class AppSettings : MonoBehaviour {

	public static AppSettings _;

	void Awake(){
		_ = this;
	}



	public bool IS_TEST_BUILD;
	public bool DEBUG;
	public bool LOAD_DATA_FROM_LOCAL_SERVER;
	public bool ALWAYS_LOAD_DATA_FROM_SERVER;
	public int APP_VERSION;



}
