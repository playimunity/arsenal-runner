﻿



public static class SocialConstants{

	#if UNITY_ANDROID

	public const string achievement_complete_10 = "CgkIwb_k7twcEAIQOA"; // <GPGSID>
	public const string achievement_mid_table_kings = "CgkIwb_k7twcEAIQKQ"; // <GPGSID>
	public const string achievement_complete_7 = "CgkIwb_k7twcEAIQNQ"; // <GPGSID>
	public const string achievement_rising_star = "CgkIwb_k7twcEAIQIw"; // <GPGSID>
	public const string achievement_money_in_the_bank = "CgkIwb_k7twcEAIQFw"; // <GPGSID>
	public const string achievement_donator = "CgkIwb_k7twcEAIQIg"; // <GPGSID>
	public const string achievement_complete_14 = "CgkIwb_k7twcEAIQPA"; // <GPGSID>
	public const string achievement_full_squad = "CgkIwb_k7twcEAIQMw"; // <GPGSID>
	public const string achievement_world_record = "CgkIwb_k7twcEAIQHg"; // <GPGSID>
	public const string achievement_scratch_my_back = "CgkIwb_k7twcEAIQIA"; // <GPGSID>
	public const string achievement_ultimate_teammate = "CgkIwb_k7twcEAIQBg"; // <GPGSID>
	public const string achievement_rollin_in_the_deep = "CgkIwb_k7twcEAIQGg"; // <GPGSID>
	public const string achievement_need_bigger_bag = "CgkIwb_k7twcEAIQFg"; // <GPGSID>
	public const string achievement_small_money = "CgkIwb_k7twcEAIQDg"; // <GPGSID>
	public const string achievement_double_trouble = "CgkIwb_k7twcEAIQCw"; // <GPGSID>
	public const string achievement_game_changer = "CgkIwb_k7twcEAIQHA"; // <GPGSID>
	public const string achievement_make_it_rain = "CgkIwb_k7twcEAIQEg"; // <GPGSID>
	public const string achievement_big_boss = "CgkIvvjuq60bEAIQRA"; // <GPGSID>        // no
	public const string achievement_r_for_rich = "CgkIwb_k7twcEAIQGA"; // <GPGSID>
	public const string achievement_social_smart = "CgkIwb_k7twcEAIQAQ"; // <GPGSID>
	public const string achievement_friendly_player = "CgkIwb_k7twcEAIQIQ"; // <GPGSID>
	public const string achievement_complete_17 = "CgkIvvjuq60bEAIQLw"; // <GPGSID>    // no
	public const string achievement_gothic_close_encounter = "CgkIvvjuq60bEAIQCw"; // <GPGSID>    // no
	public const string achievement_run_forest_run = "CgkIwb_k7twcEAIQFQ"; // <GPGSID>
	public const string achievement_found_the_secert_button = "CgkIvvjuq60bEAIQLQ"; // <GPGSID>      // no
	public const string achievement_missed_the_train = "CgkIwb_k7twcEAIQCA"; // <GPGSID>
	public const string achievement_people_just_loves_me = "CgkIwb_k7twcEAIQJQ"; // <GPGSID>
	public const string achievement_complete_2 = "CgkIwb_k7twcEAIQLA"; // <GPGSID>
	public const string achievement_missed_the_bus = "CgkIwb_k7twcEAIQBw"; // <GPGSID>
	public const string achievement_complete_3 = "CgkIwb_k7twcEAIQLQ"; // <GPGSID>
	public const string achievement_money_magnet = "CgkIwb_k7twcEAIQEw"; // <GPGSID>
	public const string achievement_complete_8 = "CgkIwb_k7twcEAIQNg"; // <GPGSID>
	public const string achievement_complete_13 = "CgkIwb_k7twcEAIQOw"; // <GPGSID>
	public const string achievement_walking_on_water = "CgkIvvjuq60bEAIQFw"; // <GPGSID>   // no
	public const string achievement_come_to_papa = "CgkIwb_k7twcEAIQEQ"; // <GPGSID>
	public const string achievement_goaler = "CgkIwb_k7twcEAIQGw"; // <GPGSID>
	public const string achievement_fresh_out_the_acadmy = "CgkIwb_k7twcEAIQDQ"; // <GPGSID>
	public const string achievement_2nd_is_the_new_1st = "CgkIwb_k7twcEAIQJw"; // <GPGSID>
	public const string achievement_true_gunner = "CgkIwb_k7twcEAIQHw"; // <GPGSID>
	public const string achievement_complete_1 = "CgkIwb_k7twcEAIQKw"; // <GPGSID>
	public const string achievement_complete_16 = "CgkIvvjuq60bEAIQPg"; // <GPGSID> // no
	public const string achievement_complete_15 = "CgkIwb_k7twcEAIQPQ"; // <GPGSID>
	public const string achievement_top_ten = "CgkIwb_k7twcEAIQEA"; // <GPGSID>
	public const string achievement_doctor_you = "CgkIwb_k7twcEAIQAw"; // <GPGSID>
	public const string achievement_title_hunter = "CgkIwb_k7twcEAIQKA"; // <GPGSID>
	public const string achievement_trainnig_hard = "CgkIwb_k7twcEAIQBQ"; // <GPGSID>
	public const string achievement_complete_5 = "CgkIwb_k7twcEAIQLw"; // <GPGSID>
	public const string achievement_complete_9 = "CgkIwb_k7twcEAIQNw"; // <GPGSID>
	public const string achievement_in_the_gym = "CgkIwb_k7twcEAIQBA"; // <GPGSID>
	public const string achievement_we_have_a_club = "CgkIwb_k7twcEAIQDw"; // <GPGSID>
	public const string achievement_kaboom = "CgkIwb_k7twcEAIQCQ"; // <GPGSID>
	public const string achievement_complete_6 = "CgkIwb_k7twcEAIQNA"; // <GPGSID>
	public const string achievement_fcbillion = "CgkIvvjuq60bEAIQHA"; // <GPGSID>   // no
	public const string achievement_invested_manager = "CgkIwb_k7twcEAIQAA"; // <GPGSID>
	public const string achievement_complete_12 = "CgkIwb_k7twcEAIQOg"; // <GPGSID>
	public const string achievement_godmode = "CgkIwb_k7twcEAIQDA"; // <GPGSID>
	public const string achievement_old_school_style = "CgkIvvjuq60bEAIQQw"; // <GPGSID>  // no
	public const string achievement_pump_it_up = "CgkIwb_k7twcEAIQFA"; // <GPGSID>
	public const string achievement_nice_and_easy = "CgkIwb_k7twcEAIQJg"; // <GPGSID>
	public const string achievement_working_out = "CgkIwb_k7twcEAIQMA"; // <GPGSID>
	public const string achievement_doctor_who = "CgkIwb_k7twcEAIQAg"; // <GPGSID>
	public const string achievement_striker = "CgkIwb_k7twcEAIQHQ"; // <GPGSID>
	public const string achievement_complete_4 = "CgkIwb_k7twcEAIQLg"; // <GPGSID>
	public const string achievement_superman = "CgkIwb_k7twcEAIQMg"; // <GPGSID>
	public const string achievement_bff = "CgkIwb_k7twcEAIQJA"; // <GPGSID>
	public const string achievement_complete_11 = "CgkIwb_k7twcEAIQOQ"; // <GPGSID>
	public const string achievement_in_shape = "CgkIwb_k7twcEAIQMQ"; // <GPGSID>
	public const string achievement_m_for_millioner = "CgkIwb_k7twcEAIQGQ"; // <GPGSID>

	public const string leaderboard_barca_team_ranking = "CgkIvvjuq60bEAIQAA"; // <GPGSID>
	public const string leaderboard_barca_high_score = "CgkIwb_k7twcEAIQPg"; // <GPGSID>


	#elif UNITY_IOS

	public const string achievement_complete_10 = "CgkIwb_k7twcEAIQOA"; // <GPGSID>
	public const string achievement_mid_table_kings = "CgkIwb_k7twcEAIQKQ"; // <GPGSID>
	public const string achievement_complete_7 = "CgkIwb_k7twcEAIQNQ"; // <GPGSID>
	public const string achievement_rising_star = "CgkIwb_k7twcEAIQIw"; // <GPGSID>
	public const string achievement_money_in_the_bank = "CgkIwb_k7twcEAIQFw"; // <GPGSID>
	public const string achievement_donator = "CgkIwb_k7twcEAIQIg"; // <GPGSID>
	public const string achievement_complete_14 = "CgkIwb_k7twcEAIQPA"; // <GPGSID>
	public const string achievement_full_squad = "CgkIwb_k7twcEAIQMw"; // <GPGSID>
	public const string achievement_world_record = "CgkIwb_k7twcEAIQHg"; // <GPGSID>
	public const string achievement_scratch_my_back = "CgkIwb_k7twcEAIQIA"; // <GPGSID>
	public const string achievement_ultimate_teammate = "CgkIwb_k7twcEAIQBg"; // <GPGSID>
	public const string achievement_rollin_in_the_deep = "CgkIwb_k7twcEAIQGg"; // <GPGSID>
	public const string achievement_need_bigger_bag = "CgkIwb_k7twcEAIQFg"; // <GPGSID>
	public const string achievement_small_money = "CgkIwb_k7twcEAIQDg"; // <GPGSID>
	public const string achievement_double_trouble = "CgkIwb_k7twcEAIQCw"; // <GPGSID>
	public const string achievement_game_changer = "CgkIwb_k7twcEAIQHA"; // <GPGSID>
	public const string achievement_make_it_rain = "CgkIwb_k7twcEAIQEg"; // <GPGSID>
	public const string achievement_big_boss = "CgkIvvjuq60bEAIQRA"; // <GPGSID>        // no
	public const string achievement_r_for_rich = "CgkIwb_k7twcEAIQGA"; // <GPGSID>
	public const string achievement_social_smart = "CgkIwb_k7twcEAIQAQ"; // <GPGSID>
	public const string achievement_friendly_player = "CgkIwb_k7twcEAIQIQ"; // <GPGSID>
	public const string achievement_complete_17 = "CgkIvvjuq60bEAIQLw"; // <GPGSID>    // no
	public const string achievement_gothic_close_encounter = "CgkIvvjuq60bEAIQCw"; // <GPGSID>    // no
	public const string achievement_run_forest_run = "CgkIwb_k7twcEAIQFQ"; // <GPGSID>
	public const string achievement_found_the_secert_button = "CgkIvvjuq60bEAIQLQ"; // <GPGSID>      // no
	public const string achievement_missed_the_train = "CgkIwb_k7twcEAIQCA"; // <GPGSID>
	public const string achievement_people_just_loves_me = "CgkIwb_k7twcEAIQJQ"; // <GPGSID>
	public const string achievement_complete_2 = "CgkIwb_k7twcEAIQLA"; // <GPGSID>
	public const string achievement_missed_the_bus = "CgkIwb_k7twcEAIQBw"; // <GPGSID>
	public const string achievement_complete_3 = "CgkIwb_k7twcEAIQLQ"; // <GPGSID>
	public const string achievement_money_magnet = "CgkIwb_k7twcEAIQEw"; // <GPGSID>
	public const string achievement_complete_8 = "CgkIwb_k7twcEAIQNg"; // <GPGSID>
	public const string achievement_complete_13 = "CgkIwb_k7twcEAIQOw"; // <GPGSID>
	public const string achievement_walking_on_water = "CgkIvvjuq60bEAIQFw"; // <GPGSID>   // no
	public const string achievement_come_to_papa = "CgkIwb_k7twcEAIQEQ"; // <GPGSID>
	public const string achievement_goaler = "CgkIwb_k7twcEAIQGw"; // <GPGSID>
	public const string achievement_fresh_out_the_acadmy = "CgkIwb_k7twcEAIQDQ"; // <GPGSID>
	public const string achievement_2nd_is_the_new_1st = "CgkIwb_k7twcEAIQJw"; // <GPGSID>
	public const string achievement_true_gunner = "CgkIwb_k7twcEAIQHw"; // <GPGSID>
	public const string achievement_complete_1 = "CgkIwb_k7twcEAIQKw"; // <GPGSID>
	public const string achievement_complete_16 = "CgkIvvjuq60bEAIQPg"; // <GPGSID> // no
	public const string achievement_complete_15 = "CgkIwb_k7twcEAIQPQ"; // <GPGSID>
	public const string achievement_top_ten = "CgkIwb_k7twcEAIQEA"; // <GPGSID>
	public const string achievement_doctor_you = "CgkIwb_k7twcEAIQAw"; // <GPGSID>
	public const string achievement_title_hunter = "CgkIwb_k7twcEAIQKA"; // <GPGSID>
	public const string achievement_trainnig_hard = "CgkIwb_k7twcEAIQBQ"; // <GPGSID>
	public const string achievement_complete_5 = "CgkIwb_k7twcEAIQLw"; // <GPGSID>
	public const string achievement_complete_9 = "CgkIwb_k7twcEAIQNw"; // <GPGSID>
	public const string achievement_in_the_gym = "CgkIwb_k7twcEAIQBA"; // <GPGSID>
	public const string achievement_we_have_a_club = "CgkIwb_k7twcEAIQDw"; // <GPGSID>
	public const string achievement_kaboom = "CgkIwb_k7twcEAIQCQ"; // <GPGSID>
	public const string achievement_complete_6 = "CgkIwb_k7twcEAIQNA"; // <GPGSID>
	public const string achievement_fcbillion = "CgkIvvjuq60bEAIQHA"; // <GPGSID>   // no
	public const string achievement_invested_manager = "CgkIwb_k7twcEAIQAA"; // <GPGSID>
	public const string achievement_complete_12 = "CgkIwb_k7twcEAIQOg"; // <GPGSID>
	public const string achievement_godmode = "CgkIwb_k7twcEAIQDA"; // <GPGSID>
	public const string achievement_old_school_style = "CgkIvvjuq60bEAIQQw"; // <GPGSID>  // no
	public const string achievement_pump_it_up = "CgkIwb_k7twcEAIQFA"; // <GPGSID>
	public const string achievement_nice_and_easy = "CgkIwb_k7twcEAIQJg"; // <GPGSID>
	public const string achievement_working_out = "CgkIwb_k7twcEAIQMA"; // <GPGSID>
	public const string achievement_doctor_who = "CgkIwb_k7twcEAIQAg"; // <GPGSID>
	public const string achievement_striker = "CgkIwb_k7twcEAIQHQ"; // <GPGSID>
	public const string achievement_complete_4 = "CgkIwb_k7twcEAIQLg"; // <GPGSID>
	public const string achievement_superman = "CgkIwb_k7twcEAIQMg"; // <GPGSID>
	public const string achievement_bff = "CgkIwb_k7twcEAIQJA"; // <GPGSID>
	public const string achievement_complete_11 = "CgkIwb_k7twcEAIQOQ"; // <GPGSID>
	public const string achievement_in_shape = "CgkIwb_k7twcEAIQMQ"; // <GPGSID>
	public const string achievement_m_for_millioner = "CgkIwb_k7twcEAIQGQ"; // <GPGSID>

	public const string leaderboard_barca_team_ranking = "CgkIvvjuq60bEAIQAA"; // <GPGSID>
	public const string leaderboard_barca_high_score = "CgkIwb_k7twcEAIQPg"; // <GPGSID>


	#endif



}

