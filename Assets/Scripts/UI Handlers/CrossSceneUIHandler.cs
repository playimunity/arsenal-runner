using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class CrossSceneUIHandler : MonoBehaviour {

//    public Text AdsStatusTxt;
//    public Text UserTypeTxt;
	public StringProvider_CrossScene cssp;


	public Animator coinsCounterAnimator;
	public Animator storeAnimator;
	public Animator genericMessageAnimator;
	public Animator genericAdAnimator;
	public Animator mainAnimator;

	public GameObject coinsCounterObject;

	public GameObject genericMessageObject;
	public GameObject genericAdObject;
	public GameObject friendChooseScreen;
	public GameObject fullPlayers;
	public Text coinsCounterAmount;

	public Text GMTitle;
	public Text GMMessage;
	public Text GMPositiveBtnTXT;
	public Text GMNegativeBtnTXT;
	public Button GMPositiveBTN;
	public Button GMNegativeBTN;
	public RectTransform GMBody;
	Vector2 GMBody_OriginalHeight;

	public Text GATitle;
	public Text GAMessage;
	public Text GAPrice;
	public Text GACoinsPrice;
	public Text GAPositiveBtnTXT;
	public Text GANegativeBtnTXT;
	public RawImage GAImage;
	public Button GAPositiveBTN;
	public Button GANegativeBTN;
	public Button GAXBTN;
	//public RectTransform GABody;
	//Vector2 GABody_OriginalHeight;

	// ------------------------------------------	Store START

	public ActivateBtns coinsBtn;
	public ActivateBtns energyBtn;
	public ActivateBtns dealsBtn;
	public GameObject storeObject;
	public GameObject StoreTabCoins;
	public GameObject StoreTabEnergy;
	public GameObject StoreTabDeals;

    public Text pack1;
    public Text pack2;
    public Text pack3;
    public Text pack4;
    public Text pack5;
    public Text pack6;

	public Text StorePack_1_Value;
	public Text StorePack_2_Value;
	public Text StorePack_3_Value;
	public Text StorePack_4_Value;
	public Text StorePack_5_Value;
	public Text StorePack_6_Value;
    public Text Deal_Starter_OldValue;
    public Text Deal_Starter_Value;

	public Text StorePack_1_Price;
	public Text StorePack_2_Price;
	public Text StorePack_3_Price;
	public Text StorePack_4_Price;
	public Text StorePack_5_Price;
	public Text StorePack_6_Price;
    public Text Deal_Starter_Price;

	public GameObject NoDealsLabel;
	public GameObject SpecialDeal1;
	public GameObject SpecialDeal2;
	// ------------------------------------------	Store END

    public TimerManager energyTimer;
	public GameObject NotificationObj;
	public Text NotificationTxt;

	//public GameObject SocialFlowTestScreen;

	public GameObject DebugConsole;
	public GameObject StagingIndicator;
    //IAP Message Instance
    public IAPMessage iapMessage;

	int GM_LINE_LENGTH = 43;
	int GM_LINE_HEIGHT = 80;

    float fillAmount;
    public Image lvlFillImage;
    public Image lvlValue;
    public Text energyValue;

	public Text godModeFeedbackTxt;


	WaitForSeconds waitSecond;

	bool ccShown = false;

	//Free Gift:
//	public Button claimFreeGiftBTN;
//	public ClaimScreenHandler prizeScreen;

	public Text Privacy;

	public TimerManager playerEnergyTimer;


	//debugging:
	public GameObject debugParent;


	// ----------------------------------------------------------

	static CrossSceneUIHandler _instance;
	public static CrossSceneUIHandler Instance{
		get{ return _instance; }
	}

	// ====================================================================================

	void Start () {
		_instance = this;
        lvlValue = HeaderManager.instance.levelValueIMG;
		GMBody_OriginalHeight = GMBody.sizeDelta;
		//storeObject.SetActive (false);
		coinsCounterObject.SetActive (false);
		genericMessageObject.SetActive (false);

		waitSecond = new WaitForSeconds(0.5f);

		ResetStagingMarker ();

//		claimFreeGiftBTN.GetComponent<TimerManager> ().callback += FreeGiftsTimerEnd;
//		claimFreeGiftBTN.gameObject.SetActive (HeyzapWrapper.AreAdsAllowedForUser (HeyzapWrapper.AdType.FreeGift));
		if (PlayerChooseManager.instance != null) {
			PlayerChooseManager.instance.StopPerkGlowFX ();
		}
		sparclesParticleSystem.Stop();
		particleBox = sparclesParticleSystem.shape;
		sparclesParticleSystemObj.SetActive (false);

		if (DAO.IsInDebugMode) {
			debugParent.SetActive (true);
		}
			
		if (DAO.TotalCoinsCollected == 0) {
			coinsCounterAmount.text = "0";
		} else {
			coinsCounterAmount.text = DAO.TotalCoinsCollected.ToString("##,#");
		}

	}


	void OnLevelWasLoaded(int level) {
		if (level != 0) {
//			StartCoroutine(hideTransitionAfterSecond());
			//if(GameManager.curentSubGameState == GameManager.GameSubState.NO)  showCoinsCounter ();

			GameManager.RequestUnpause ();
		}
	}

	#if UNITY_EDITOR
	void Update(){
		if (Input.GetKeyUp (KeyCode.A)) {
			TestAddGiftCards ();
			//Debug.Log ("alon__________ should add gift card");
		}
	}
	#endif

	// ====================================================================================

	public void ResetStagingMarker(){
		StagingIndicator.SetActive (DAO.IsInDebugMode);
	}

	public void SetStrings(){
		cssp.SetStrings ();
        pack1.text = DAO.Language.GetString ("pack1-btn");
        pack2.text = DAO.Language.GetString ("pack2-btn");
        pack3.text = DAO.Language.GetString ("pack3-btn");
        pack4.text = DAO.Language.GetString ("pack4-btn");
        pack5.text = DAO.Language.GetString ("pack5-btn");
        pack6.text = DAO.Language.GetString ("pack6-btn");
		StorePack_1_Value.text = "" + IAP.Pack_1_Value.ToString ("##,#");
		StorePack_2_Value.text = "" + IAP.Pack_2_Value.ToString ("##,#");
		StorePack_3_Value.text = "" + IAP.Pack_3_Value.ToString ("##,#");
		StorePack_4_Value.text = "" + IAP.Pack_4_Value.ToString ("##,#");
		StorePack_5_Value.text = "" + IAP.Pack_5_Value.ToString ("##,#");
		StorePack_6_Value.text = "" + IAP.Pack_6_Value.ToString ("##,#");
        Deal_Starter_OldValue.text = "" + IAP.Pack_starter_OldValue.ToString("##,#");
        Deal_Starter_Value.text = "" + IAP.Pack_starter_Value.ToString("##,#");
	}

	public void ConsoleShow(bool isDebug = true){
		cr_handler.IsDebug = isDebug;
		DebugConsole.SetActive (true);
	}

	public void ConsoleHide(){
		DebugConsole.SetActive (false);
	}
		

	public void showCupLooseScreen(){
		mainAnimator.Play ("CrossScene_CupLooseShow");
		GM_Input._.CupLoose = true;
	}
		
	public void hideCupLooseScreen(){
		GM_Input._.CupLoose = false;
		mainAnimator.Play ("CrossScene_CupLooseHide");
		//QuestTimeOverAnalyticsEvent ();
	}

	public void ShowPreloader(){
		mainAnimator.Play ("CrossSceneUI_Preloader_Show", 4);
	}

	public void HidePreloader(){
		mainAnimator.Play ("CrossSceneUI_Preloader_Hide", 4);
	}


//	void QuestTimeOverAnalyticsEvent(){
//		UnitedAnalytics.LogEvent ("Quest time over", "closed", UserData.Instance.userType.ToString (), DAO.ActiveRunID);
//	}


	public void showCoinsCounter(){
		ccShown = true;
		coinsCounterObject.SetActive (true);
		//coinsCounterAnimator.Play ("coins_counter_show");
	}

	public void hideCoinsCounter(bool force = false){
		if (!ccShown && !force)return; 
		ccShown = false;
		//coinsCounterAnimator.Play ("coins_counter_hide");
	}

    public void PlayerInfoUpdate(){ //driven also by animations event:

        Player pl = PlayerChooseManager.instance.currentPlayerScript;

        fillAmount = (float)pl.data.metersPassedToNextLevel / (float)pl.data.NextLevelSetup.MetersToThisLevel;
        lvlFillImage.fillAmount = fillAmount;
        lvlValue.sprite = HeaderManager.instance.levelImages[pl.data.level]; 
        energyValue.text = "" + pl.data.fitness + "/" + pl.data.maxFitness;
    }

	public void ShowStoreClicked(){
		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN || GameManager.curentGameState == GameManager.GameState.INFINITY_RUN || GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			return;
		}
		//MainScreenUiManager.instance.PerksScrnAnim (false);
		//MainScreenUiManager.instance.RunsScrnAnimToggle (false);
		//MainScreenUiManager.instance.CupScrnAnimToggle (false);
		//MainScreenUiManager.instance.CardScrnToggle (false);
		if (PlayerChooseManager.instance != null) {
			PlayerChooseManager.instance.StopPerkGlowFX ();
		}
		sparclesParticleSystem.Stop();
		sparclesParticleSystemObj.SetActive (false);
		showStore (ShopStates.COINS);
	}

	public void ShowStoreEnergyClicked(){
		showStore (ShopStates.ENERGY);
	}

	public void ShowStoreDealsClicked(){
		showStore (ShopStates.DEALS);
	}

	public void OnBuyMedicKitClicked(int id){

		int cost = DAO.Settings.MedikKits [id] ["p"].AsInt;
		int heal = DAO.Settings.MedikKits [id] ["h"].AsInt;

		if (DAO.TotalCoinsCollected >= cost) {
			DAO.TotalMedikKitsCollected += heal;
			DAO.TotalCoinsCollected -= cost;
			UpdateCoinsAmount ();
			hideStore ();
			AppsFlyerManager._.ReportVirtualEconomy ("Energy Shop", "Medic Kit", cost.ToString(), heal.ToString());
			if (id == 2 && GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM && PlayerChooseManager.IsNeedToShowRateUsPopup) {
				MainScreenUiManager.instance.ShowRateUsPopup ();
			}
		} else {
			showStore (ShopStates.COINS);
		}

	}

	public enum ShopStates {CLOSED, COINS, ENERGY, DEALS};
	public static ShopStates CurrentShopState = ShopStates.CLOSED;
	public void showStore(ShopStates state){
        Debug.Log("ON Show Store");
		if (!IAP.IsReady) return;
        Debug.Log("Store is ready!");
		if (state == CurrentShopState) return;

		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			if (Tutorial.Instance.tutorialPointOn) return;
		}

		// ----
        print("QUICK FIX PRICE: "+IAP.Instance.Product_QUICK_FIX.GetPriceLabel());
		StorePack_1_Price.text = IAP.Instance.Product_QUICK_FIX.GetPriceLabel();
		StorePack_2_Price.text = IAP.Instance.Product_LOCAL_CLUB.GetPriceLabel();
		StorePack_3_Price.text = IAP.Instance.Product_NATIONAL_FUND.GetPriceLabel();
		StorePack_4_Price.text = IAP.Instance.Product_WORLD_DOMINATOR.GetPriceLabel();
		StorePack_5_Price.text = IAP.Instance.Product_FCB_PRESIDENT.GetPriceLabel();
		StorePack_6_Price.text = IAP.Instance.Product_CHAMPIONS_PACK.GetPriceLabel();
        Deal_Starter_Price.text = IAP.Instance.Product_STARTER_DEAL.GetPriceLabel();

		// ---

		StoreTabCoins.SetActive(false);
		StoreTabEnergy.SetActive(false);
		StoreTabDeals.SetActive(false);
		NoDealsLabel.SetActive(true);
		SpecialDeal1.SetActive(false);
		SpecialDeal2.SetActive(false);

		coinsBtn.Activate ();
		energyBtn.Deactivate ();
		dealsBtn.Deactivate ();

//		StoreYellowCoin.SetActive(false);
//		StoreWhiteCoin.SetActive(true);
//		StoreBtnTxtCoins.color = Color.white;
//		StoreBtnTxtEnergy.color = Color.white;
//		StoreBtnTxtDeals.color = Color.white;
//		StoreIconEnergy.color = Color.white;
//		StoreIconDeals.color = Color.white;
//		StoreLineCoins.SetActive(false);
//		StoreLineEnergy.SetActive(false);
//		StoreLineDeals.SetActive(false);

		switch (state) {
			case ShopStates.COINS : {
				coinsBtn.Activate ();
				energyBtn.Deactivate ();
				dealsBtn.Deactivate ();
//				StoreYellowCoin.SetActive(true);
//				StoreWhiteCoin.SetActive(false);
//				StoreBtnTxtCoins.color = StoreColorYellow;
//				StoreLineCoins.SetActive(true);
				StoreTabCoins.SetActive(true);

				break;
			}

			case ShopStates.ENERGY : {
				coinsBtn.Deactivate ();
				energyBtn.Activate ();
				dealsBtn.Deactivate ();
				StoreTabEnergy.SetActive(true);
//				StoreBtnTxtEnergy.color = StoreColorYellow;
//				StoreIconEnergy.color = StoreColorYellow;
//				StoreLineEnergy.SetActive(true);
				break;
			}

			case ShopStates.DEALS : {
				coinsBtn.Deactivate ();
				energyBtn.Deactivate ();
				dealsBtn.Activate ();
				StoreTabDeals.SetActive(true);
//				StoreBtnTxtDeals.color = StoreColorYellow;
//				StoreIconDeals.color = StoreColorYellow;
//				StoreLineDeals.SetActive(true);

				// Check witch deals need show
				bool First7Days = ((int)(DateTime.Now - DAO.InstallDate).TotalDays <= 7);

				if (!DAO.StarterPackBought && First7Days) {	// Starter Pack Deal
					NoDealsLabel.SetActive(false);
					SpecialDeal1.SetActive(true);
				}

				if (DAO.IAPPoints > 0 && !DAO.FCBMegaDealBought) {	// FCB Mega Deal
					NoDealsLabel.SetActive(false);
					SpecialDeal2.SetActive(true);
				}

				break;
			}
		}

		if (CurrentShopState == ShopStates.CLOSED) {
			GameManager.RequestPause ();
			storeObject.SetActive (true);
			storeAnimator.Play ("store_show");
		}

		// ----

		CurrentShopState = state;
			
		GM_Input._.Store = true;
	}



	public void hideStore(){
		GM_Input._.Store = false;
		IAP.Instance.ClearAdditionalIAPCallback ();
		CurrentShopState = ShopStates.CLOSED;
		storeAnimator.Play ("store_hide");
	}










	public void UpdateCoinsAmount(){
		coinsCounterAnimator.Play ("coins_conter_in");
		coinsCounterAmount.text = DAO.TotalCoinsCollected.ToString("##,#");
//		if (DAO.TotalCoinsCollected == 0) {
//			coinsCounterAmount.text = "0";
//		} else {
//			coinsCounterAmount.text = DAO.TotalCoinsCollected.ToString("##,#");
//		}
	}

	public GameObject coinPrefab;
	public List<GameObject> coinsList = new List<GameObject>();
	GameObject tempCoin;
	WaitForEndOfFrame waitForEndFrame = new WaitForEndOfFrame();
	public Transform coinsParent;
	Vector2 scrnMiddle = new Vector2 (Screen.width / 2, Screen.height / 2);
	public RectTransform bankPos;
	int[] positiveNegative = new int[] {-1,1};
	int currentCoinsAmount;
	bool counterOn = false;
	public RectTransform coinBig;
	public RectTransform coinSmall;

//	public void UpdateMultiCoinsAmount(int fxAmount){
//		UpdateMultiCoinsAmount (fxAmount, (Vector3)scrnMiddle);
//	}

	public void UpdateMultiCoinsAmount(int fxAmount , int coinsAmount){
		UpdateMultiCoinsAmount (fxAmount, coinsAmount , (Vector3)scrnMiddle);
	}

	public void UpdateMultiCoinsAmount(int fxAmount , int coinsAmount , Vector3 fxPosition){
		for (int i = 0; i < fxAmount ; i++) {
			if (coinsList.Count > 0) {
				tempCoin = coinsList [0];
				coinsList.RemoveAt (0);
				tempCoin.transform.position = fxPosition;
				tempCoin.SetActive (true);
			}else{
				tempCoin = (GameObject)Instantiate (coinPrefab , fxPosition , transform.rotation);
				tempCoin.transform.SetParent (coinsParent);
				tempCoin.transform.localScale = Vector3.one;
			}

			StartCoroutine (addMultiCoins(tempCoin.GetComponent<RectTransform>() , fxPosition));
		}
		//AudioManager.Instance.OnCoinPick();
		//currentCoinsAmount = DAO.TotalCoinsCollected;
		SparklesFX (1.0f, fxPosition, Color.yellow , 10 , 50, 50 , false);
		currentCoinsAmount = DAO.TotalCoinsCollected;
		DAO.TotalCoinsCollected += coinsAmount;
		//StartCoroutine ("CoinsCounterAnim");
	}

	IEnumerator addMultiCoins(RectTransform coin , Vector3 fxPosition){
		Vector2 pos = new Vector2 (fxPosition.x + UnityEngine.Random.Range(-Screen.width/3 , Screen.width/3) , fxPosition.y + UnityEngine.Random.Range(-Screen.width/3 , Screen.width/3));

		//RectTransform coinRectTransform = coin.GetComponent<RectTransform> ();
		coin.sizeDelta = coinSmall.sizeDelta;
		while (Vector2.Distance (coin.position , pos) > 1f) {
			coin.position = Vector2.Lerp (coin.position, pos, 8f * Time.deltaTime);
			coin.sizeDelta = Vector2.Lerp (coin.sizeDelta, coinBig.sizeDelta, 8f * Time.deltaTime);
			yield return waitForEndFrame;
		}
		while (Vector2.Distance (coin.position, bankPos.position) > 25f) {
			coin.position = Vector2.Lerp (coin.position, bankPos.position, 8f * Time.deltaTime);
			if (!counterOn && Vector2.Distance (coin.position, bankPos.position) < 150f) {
				counterOn = true;
				StartCoroutine ("CoinsCounterAnim");
			}
			yield return waitForEndFrame;
		}
			
		//Destroy (coin);
		coin.gameObject.SetActive(false);
		coinsList.Add (coin.gameObject);
	}

	IEnumerator CoinsCounterAnim(){
		//coins_counter_addcoin
		AudioManager.Instance.CoinCounter (true);
		while (currentCoinsAmount < DAO.TotalCoinsCollected - 9) {
			currentCoinsAmount += 10;
			coinsCounterAmount.text = currentCoinsAmount.ToString ("##,#");
			coinsCounterAnimator.Play ("coins_counter_addcoin_loop");
			yield return waitForEndFrame;
		}
		coinsCounterAmount.text = DAO.TotalCoinsCollected.ToString("##,#");
		coinsCounterAnimator.Play ("coins_counter_addcoin");
		AudioManager.Instance.CoinCounter (false);
		counterOn = false;
	}

	public bool sparklesOn = false;
	public GameObject sparklePrefab;
	//public List<GameObject> sparklesList = new List<GameObject> ();
	public List<GameObject> sparklesList;
	GameObject tempSparkle;
	float sparkleStartTime;
	float sparkSize;
	public ParticleSystem sparclesParticleSystem;
	public GameObject sparclesParticleSystemObj;
	public RectTransform sparclesRectTransform;
	ParticleSystem.EmissionModule sparksParticleEmission;
	ParticleSystem.MinMaxCurve sparksParticleRate;
	Vector3 particlesSize;
	ParticleSystem.ShapeModule particleBox;

	public void SparklesFX(){
		sparklesOn = true;
		SparklesFX (1.5f, scrnMiddle , Color.yellow , 10 , 4 , 4 , false);
	}

	public void SparklesFX(float fxTime , Vector3 fxPosition , Color col , int amount , float width , float height , bool down){
		sparklesOn = true;
		sparkleStartTime = Time.time;
		StartCoroutine (ParticleSparklesGenerator(fxTime , fxPosition , col , amount , width , height , down));
	}

	IEnumerator ParticleSparklesGenerator(float fxTime , Vector3 fxPosition , Color col , int amount , float width , float height , bool down){
		sparclesParticleSystemObj.SetActive (true);
		sparclesRectTransform.position = fxPosition;
		sparclesParticleSystem.startColor = col;
		//set the rate of the particles:
		sparksParticleEmission = sparclesParticleSystem.emission;
		sparksParticleRate = sparksParticleEmission.rate;
		sparksParticleRate.constantMax = (float)amount;
		sparksParticleRate.constantMin = (float)amount;
		sparksParticleEmission.rate = sparksParticleRate;
		particlesSize.x = width;
		particlesSize.y = height;
		particleBox.box = particlesSize;
		//sparclesParticleSystem.shape = particleBox.box;

		sparclesParticleSystem.Play();
		yield return new WaitForSeconds (fxTime);
		sparclesParticleSystem.Stop();
		//sparclesParticleSystemObj.SetActive (false);
	}


	public void SparklesFxUi(){
		sparklesOn = true;
		SparklesFxUi (1.5f, scrnMiddle , Color.yellow , 4 , 4 , false);
	}

	public void SparklesFxUi(float fxTime , Vector3 fxPosition , Color col , float width , float height , bool down){
		sparklesOn = true;
		sparkleStartTime = Time.time;
		StartCoroutine (SparklesGenerator(fxTime , fxPosition , col , width , height , down));
	}
		
	IEnumerator SparklesGenerator(float fxTime , Vector3 fxPosition , Color col , float width , float height , bool down){
		while (sparklesOn && (Time.time - sparkleStartTime) < fxTime) {
			Vector2 pos = new Vector2 (fxPosition.x + UnityEngine.Random.Range(-Screen.width/width , Screen.width/width) , fxPosition.y + UnityEngine.Random.Range(-Screen.width/height , Screen.width/height));
			sparkSize = UnityEngine.Random.Range(100f,200f);
			Vector2 sparkScale = new Vector2 (sparkSize, sparkSize);
			Vector3 rot = Vector3.zero;
			rot.z = UnityEngine.Random.Range(0f,360f);
			if (sparklesList.Count > 0) {
				tempSparkle = sparklesList [0];
				sparklesList.RemoveAt (0);
				tempSparkle.transform.position = pos;
				tempSparkle.SetActive (true);
			} else {
				tempSparkle = (GameObject)Instantiate (sparklePrefab, pos, transform.rotation);
				tempSparkle.transform.SetParent (coinsParent);
				tempSparkle.transform.localScale = Vector3.one;
			}
			RectTransform sparkleRectTransform = tempSparkle.GetComponent<RectTransform> ();

			Image[] images = tempSparkle.GetComponentsInChildren<Image> ();
			foreach (Image img in images) {
				img.color = col;
			}

			sparkleRectTransform.sizeDelta = sparkScale;
			sparkleRectTransform.eulerAngles = rot;

			StartCoroutine (SparklesMover(tempSparkle.transform , down));

			yield return new WaitForSeconds (UnityEngine.Random.Range (0.02f, 0.12f));
		}
	}
		

	IEnumerator SparklesMover(Transform sparkle , bool down){
		float movingSpeed;
		if (down) {
			movingSpeed = UnityEngine.Random.Range (-70f, -100f);
		} else {
			movingSpeed = UnityEngine.Random.Range (60f, 90f);
		}
		Vector2 pos = sparkle.position;
		float moveStartTime = Time.time;
		while (sparklesOn && (Time.time - moveStartTime) < 1f) {
			pos.y += movingSpeed * Time.deltaTime;
			sparkle.position = pos;
			yield return waitForEndFrame;
		}
		//Destroy (sparkle);
		sparkle.gameObject.SetActive(false);
		sparklesList.Add (sparkle.gameObject);
	}




	//  ----------- temp TESTS ------------:
	Vector3 testCoinPos = new Vector3(100f,100f,0f);
	public void TestGetCoinsTest(){
		//DAO.TotalCoinsCollected += 25000;
		UpdateMultiCoinsAmount (15, 25000 , testCoinPos);
	}

	public void TestGenerateSparkles(){
		StartCoroutine (GenerateSparklesCo());
	}
	GameObject newTempSparkle;
	public GameObject newSparklePrefab;
	IEnumerator GenerateSparklesCo(){
		for (int i = 0; i < 10; i++) {
			newTempSparkle = (GameObject)Instantiate (newSparklePrefab, scrnMiddle, transform.rotation);
			newTempSparkle.transform.SetParent (coinsParent);
			yield return waitSecond;
		}
	}

	public void TestGetPowerUps(){
		DAO.TotalSpeedBoostsCollected += 3;
		DAO.TotalMagnetsCollected += 3;
		DAO.TotalX2CoinsCollected += 3;
		DAO.TotalSheildsCollected += 3;
	}

	public void TestAddGiftCards(){
		DAO.TotalGiftCardsCollected += 3;
	}

	public void TestResetGiftBox(){
		LocalDataInterface.Instance.ResetGiftCards ();
	}

	int index = 0;
	public void ChangeUserType(){
		index++;
		if (index > 5) {
			index = 0;
		}
		switch(index){
		case 0:
			DAO.UserType = UserData.UserType.NEW_USER;
			break;
		case 1:
			DAO.UserType = UserData.UserType.ENGAGED_A;
			break;
		case 2:
			DAO.UserType = UserData.UserType.ENGAGED_B;
			break;
		case 3:
			DAO.UserType = UserData.UserType.ENGAGED_C;
			break;
		case 4:
			DAO.UserType = UserData.UserType.ENGAGED_D;
			break;
		case 5:
			DAO.UserType = UserData.UserType.PAYING_USER;
			break;
		}
	}

	public void TestMethodZona(){
		//Debug.Log ("DAO.RateUsPopupDate: " + DAO.RateUsPopupDate);
		//Debug.Log ("(DateTime.Now - DAO.RateUsPopupDate).TotalSeconds: " + (DateTime.Now - DAO.RateUsPopupDate).TotalSeconds);
	}

	public void GetPlayersList(){
		for (int i = 0; i < PrefabManager.instanse.PlayerStructures.Count; i++) {
			//Debug.Log ("id: " + PrefabManager.instanse.PlayerStructures [i].PlayerID + ", Player Name: " + PrefabManager.instanse.PlayerStructures [i].PlayerName + ", Player Number: " + PrefabManager.instanse.PlayerStructures [i].PlayerNumber);
		}
	}

	public void CheckString(){
		string first_player_name = PrefabManager.instanse.PlayerStructures[ int.Parse(DAO.PurchasedPlayers.Split('|')[0]) ].PlayerName;
		//Debug.Log ("first_player_name: " + first_player_name);
	}

	public void DebugRefreshRuns(){
		ServerDataInterface.Instance.GetDataRuns ();
	}


	//---------------------------------------

	//Vector2 sz = GMBody.sizeDelta;
	public void showGennericMessage(GenericUIMessage msg){
		//GameManager.RequestPause ();
		genericMessageObject.SetActive (true);
		GMTitle.text = msg.Title;
		GMMessage.text = msg.Message;

		GMPositiveBtnTXT.text = msg.PositiveButtonText;
		GMNegativeBtnTXT.text = msg.NegativeButtonText;


		if (msg.PositiveBTNAction != null) {
			GMPositiveBTN.onClick.AddListener(msg.PositiveBTNAction);
			GMPositiveBTN.gameObject.SetActive(true);
		}

		if (msg.NegativeBTNAction != null) {
			GMNegativeBTN.onClick.AddListener(msg.NegativeBTNAction);
			GMNegativeBTN.gameObject.SetActive(true);
		}

		AudioManager.Instance.OnGenericPopup();

		Vector2 sz = GMBody_OriginalHeight;
		sz.y += GMMessage.preferredHeight;
		GMBody.sizeDelta = sz;

		genericMessageAnimator.Play ("generic_message_show");

		GM_Input._.GenericMessage = true;
	}

	public void hideGenericMessage(){
		GMNegativeBTN.onClick.RemoveAllListeners ();
		GMPositiveBTN.onClick.RemoveAllListeners ();
		GM_Input._.GenericMessage = false;
		genericMessageAnimator.Play ("generic_message_hide");
	}

	public void ShowFriendChooseScreen(FriendChooseScreen.ScreenStates state){
		friendChooseScreen.SetActive (true);
		mainAnimator.Play ("CrossScene_FriendChoose_Show", 1);
		FriendChooseScreen._.SetState (state);
		GM_Input._.ChooseFriend = true;
	}

	public void HideFriendChooseScreen(){
		GM_Input._.ChooseFriend = false;

		if (FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.SEND_GIFTCARDS) {
			UnitedAnalytics.LogEvent ("sent gift cards button", "canceled", UserData.Instance.userType.ToString (), DAO.TotalGiftCardsSent);
		}else if(FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.ASK_FOR_GIFTCARDS){
			UnitedAnalytics.LogEvent ("request gift", "canceled", UserData.Instance.userType.ToString (), (int)Mathf.Floor(DAO.TotalGiftCardsCollected));
		}

		FriendChooseScreen._.DestroyAll ();
		mainAnimator.Play ("CrossScene_FriendChoose_Hide", 1);
	}

	bool NotificationHidden = true;
	public void ShowNotification(string msg, float duration){
		NotificationHidden = false;
		NotificationTxt.text = msg;
		NotificationObj.SetActive (true);

		mainAnimator.Play ("notification_show", 2);
		StartCoroutine (HideNotificationDelayed(duration));
	}

	public void HideNotification(){
		NotificationHidden = true;
		mainAnimator.Play ("notification_hide", 2);
	}

	IEnumerator HideNotificationDelayed(float sec){
		yield return new WaitForSeconds (sec);
		if(!NotificationHidden) HideNotification ();
	}


	public void GodModeFeedback(int index){
		if (PlayerController.instance.sheildModeDamage) {
			return;
		}
			
		if (index == 1) {
			//godModeFeedbackTxt.text = "YEAH!";
			godModeFeedbackTxt.text = DAO.Language.GetString ("amaizing");
			AudioManager.Instance.PlayerPositive2 ();
		} else if (index == 2) {
			//godModeFeedbackTxt.text = "AMAZING!";
			godModeFeedbackTxt.text = DAO.Language.GetString ("yeah");
			AudioManager.Instance.PlayerWoohoo ();
		}

		mainAnimator.Play ("GodMode_feedback");
	}


	public void showGennericAd(Ad ad){
		genericAdObject.SetActive (true);
		GATitle.text = ad.Title;
		GAMessage.text = ad.PromoTXT;
		GAImage.texture = ad.ImageTexture;
		GAPositiveBtnTXT.text = ad.CATTXT;
		string price;
		if (ad.CancelTXT == "") {
			GANegativeBtnTXT.transform.parent.gameObject.SetActive (false);
		} else {
			GANegativeBtnTXT.transform.parent.gameObject.SetActive (true);
			GANegativeBtnTXT.text = ad.CancelTXT;
		}
		//Debug.Log ("alon_______________ showGennericAd(Ad ad) - ad.IAPPrice: " + ad.IAPPrice);
		if (ad.IAPPrice == null || ad.IAPPrice == "") {
			GAPrice.transform.gameObject.SetActive (false);
		} else {
			GAPrice.transform.gameObject.SetActive (true);
			GAPrice.text = ad.IAPPrice;
		}


		for (int index = 0; index < ad.CTAs.Count; index++) {
			if (ad.CTAs [index].CTAType == AdCTA.Types.REMOVE_X_COINS) {
				//Debug.Log ("alon_______________ showGennericAd(Ad ad) - ad.CTAs [index].Param: " + ad.CTAs [index].Param);
				price = ad.CTAs [index].Param;
				GACoinsPrice.transform.gameObject.SetActive (true);
				GACoinsPrice.text = price;
				//GACoinsPrice.text = price + " " + DAO.Language.GetString("chips");
			} else {
				GACoinsPrice.transform.gameObject.SetActive (false);
			}
		}

		AudioManager.Instance.OnGenericPopup();
		if (ad.ShowPrivacy)
			Privacy.enabled = true;
		else
			Privacy.enabled = false;
			

		genericAdAnimator.Play ("generic_ad_show");
	}

	public void hideGenericAd(){
		genericAdAnimator.Play ("generic_ad_hide");
	}


//	public void OnGMNegativeClicked()
//	{
//		print ("Attaching listener to Negative Button");
//
//			Ads._.OnAdClosed();	
//
//	}
//	public void OnGMPositive

//	bool freeGiftBtnOn = false;
//	public void FreeGiftBtnAnimToggle(bool on){
//		if (on && !freeGiftBtnOn) {
//			mainAnimator.Play ("free_gift_btn_anim in", 0, 0f);
//			freeGiftBtnOn = true;
//			//Debug.Log ("alon_________ FreeGiftBtnAnimToggle SHOW");
//		} else if (!on && freeGiftBtnOn) {
//			mainAnimator.Play ("free_gift_btn_anim out", 0, 0f);
//			freeGiftBtnOn = false;
//			//Debug.Log ("alon_________ FreeGiftBtnAnimToggle HIDE");
//		}
//	}


//	public void ClaimFreeGiftClicked ()
//	{
//		prizeScreen.SetView (GiftCard.GetRandomPrize ());
//		#if UNITY_EDITOR
//		mainAnimator.Play("free_gift_popup_anim in",5,0f);
//		if (PlayerPrefs.GetInt("is_free_gifts_claimed") == 1)
//			claimFreeGiftBTN.GetComponent<TimerManager>().Restart();
//			else
//			{
//				claimFreeGiftBTN.GetComponent<TimerManager>().StartTimer();
//				PlayerPrefs.SetInt("is_free_gifts_claimed", 1) ;
//			}
//		HeyzapWrapper.CallBackAdditive();
//		#endif
//		HeyzapWrapper.listener = delegate(string adState, string adTag) {
//			if (adState.Equals ("incentivized_result_complete")) {
//			mainAnimator.Play ("free_gift_popup_anim in", 5, 0f);
//			UnitedAnalytics.LogEvent ("Free Gifts Clicked", UserData.Instance.userType.ToString ());
//			HeyzapWrapper.CallBackAdditive();
//			if (PlayerPrefs.GetInt("is_free_gifts_claimed") == 1)
//			claimFreeGiftBTN.GetComponent<TimerManager>().Restart();
//			else
//			{
//				claimFreeGiftBTN.GetComponent<TimerManager>().StartTimer();
//				PlayerPrefs.SetInt("is_free_gifts_claimed", 1) ;
//			}
//				// The user has watched the entire video and should be given a reward.
//			}
//			if (adState.Equals ("incentivized_result_incomplete")) {
//				// The user did not watch the entire video and should not be given a   reward.
//			}
//		};
//		HeyzapWrapper.SetListener ();
//		HeyzapWrapper.Show ();
//	}

//	public void FreeGiftsTimerEnd()
//	{
//		claimFreeGiftBTN.GetComponent<TimerManager>().timeDisplay.text = DAO.Language.GetString("claim");
//	}
		

	public void OpenPrivecyPolicyLink(){
		Application.OpenURL ("http://ars.gamehour.com/fcbur-policy.html");
	}


}


public class GenericUIMessage{

	 public GenericUIMessage(){
		
	 }

	public GenericUIMessage(string title, string message){
		Title = title;
		Message = message;

	}

	public string Title;
	public string Message;
	public string PositiveButtonText;
	public string NegativeButtonText;
	public UnityEngine.Events.UnityAction PositiveBTNAction;
	public UnityEngine.Events.UnityAction NegativeBTNAction;

}



public class GenericUIAd{

	public string Title = "";
	public string Message = "";
	public float Price;
	public string PositiveButtonText = "";
	public string NegativeButtonText = "";

	public Texture pic = null;

	public UnityEngine.Events.UnityAction PositiveBTNAction;
	public UnityEngine.Events.UnityAction NegativeBTNAction;

	public GenericUIAd(){

	}
		
		
}







