﻿using UnityEngine;
using System.Collections;

public class GroundDetector : MonoBehaviour {

	public static GroundDetector instance;

	public bool isJumping = false;


	void Awake(){
		instance = this;
	}

	void OnTriggerEnter(Collider other) {
		if (isJumping && other.tag == "Section") {
			PlayerController.instance.JumpEndEvent ();
			isJumping = false;
		}
	}

}
