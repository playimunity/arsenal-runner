﻿using UnityEngine;
using System.Collections;

public class ParticleCollectGenerator : MonoBehaviour {

	public static ParticleCollectGenerator instance;

	public ParticleSystem particleCollect;
	public ParticleSystem particleBallCollect;

	bool active = false;


	void Awake(){
		instance = this;
	}


	void Update(){
		if (active) {
			transform.Translate(Vector3.forward * 3f * Time.deltaTime);
		}
	}


	public void PlayParticle(Vector3 pos , Vector3 rot){
		transform.position = pos;
		transform.eulerAngles = rot;
		active = true;
		particleCollect.gameObject.SetActive (true);
		particleCollect.Play ();
		StartCoroutine (WaitForDeactivate());
	}

	public void PlayBallParticle(Vector3 pos , Vector3 rot){
		transform.position = pos;
		transform.eulerAngles = rot;
		active = true;
		particleBallCollect.gameObject.SetActive (true);
		//particleBallCollect.Play ();
		StartCoroutine (WaitForDeactivate());
	}

	static WaitForSeconds _wfs;
	static WaitForSeconds wfs{
		get{
			if (_wfs == null)
				_wfs = new WaitForSeconds (1f);

			return _wfs;
		}
	}

	IEnumerator WaitForDeactivate(){
		yield return wfs;
		//particleBallCollect.Stop ();
		particleBallCollect.gameObject.SetActive (false);
		particleCollect.Stop ();
		particleCollect.gameObject.SetActive (false);

		active = false;
	}
	

}
