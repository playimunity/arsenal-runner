﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StringProvider_CupRun : MonoBehaviour {


	public Text GoalsTxt;
	public Text RunIsReady;
	public Text GoBtn;

	public Text PauseTitle;
	public Text pauseRunStatus;
	public Text KeepRunningBtn;
	public Text StopBtn;
	//public Text BackToLockerTxt;

	public Text NextRunBTN;
	public Text LockerRoomBTN;

	public Text RetryBTN;
	public Text CollectTxt;
	public Text TotalTxt;
	public Text Gold2TXT;
	public Text Silver2TXT;
	public Text Failed2TXT;

	public Text ChipsTxt;

	public Text SilverTXT;
	public Text GoldTXT;

	public Text GoldFeedbackTXT;
	public Text SilverFeedbackTXT;
	public Text FailFeedbackTXT;

	public Text medalTxtGold;
	public Text medalTxtSilver;

	public Text SaveMeHeader;
	public Text SaveMeBTN;
	public Text SaveMeCostTxt;

	public Text SaveMeAdCostTxt;
	public Text SaveMeAdBTN;


//	void Awake () {
//
//		GoalsTxt.text = DAO.Language.GetString ("goals");
//		RunIsReady.text = DAO.Language.GetString ("run-ready");
//		GoBtn.text = DAO.Language.GetString ("go");
//
//		PauseTitle.text = DAO.Language.GetString ("cr-pause-title");
//		pauseRunStatus.text = DAO.Language.GetString ("run-status");
//		KeepRunningBtn.text = DAO.Language.GetString ("keep-running");
//		StopBtn.text = DAO.Language.GetString ("stop-run");
//		//BackToLockerTxt.text = DAO.Language.GetString ("goto-locker-room");
//
//		NextRunBTN.text = DAO.Language.GetString ("next-run");
//
//		LockerRoomBTN.text = DAO.Language.GetString ("locker-room");
//		RetryBTN.text = DAO.Language.GetString ("retry");
//		CollectTxt.text = DAO.Language.GetString ("gc-nc-collect-btn");
//		TotalTxt.text = DAO.Language.GetString ("total");
//
//		ChipsTxt.text = DAO.Language.GetString ("chips");
//
//		SilverTXT.text = DAO.Language.GetString ("silver");
//		GoldTXT.text = DAO.Language.GetString ("gold");
//		Gold2TXT.text = DAO.Language.GetString ("gold");
//		Silver2TXT.text = DAO.Language.GetString ("silver");
//		Failed2TXT.text = DAO.Language.GetString ("failed");
//
//		GoldFeedbackTXT.text = DAO.Language.GetString ("gold");
//		SilverFeedbackTXT.text = DAO.Language.GetString ("silver");
//		FailFeedbackTXT.text = DAO.Language.GetString ("failed");
//		medalTxtGold.text = DAO.Language.GetString ("medal");
//		medalTxtSilver.text = DAO.Language.GetString ("medal");
//
//		//SaveMeCostTxt.text = DAO.Language.GetString ("retry_run_cost");
//		SaveMeHeader.text = DAO.Language.GetString ("save-me-header");
//		SaveMeBTN.text = DAO.Language.GetString ("save-me");
//		SaveMeCostTxt.text = DAO.getSaveMePricesByIndex (0, DAO.SaveMePriceTag.CUP).ToString ();
//
//
//		SaveMeAdBTN.text = DAO.Language.GetString ("free");
//		SaveMeAdCostTxt.text = DAO.getSaveMePricesByIndex (0, DAO.SaveMePriceTag.CUP).ToString ();
//	}

}
