﻿using UnityEngine;
using System.Collections;

public class DeactivateObjectEvent : MonoBehaviour {

	public void DeactivateObject(){
		gameObject.SetActive (false);
	}
}
