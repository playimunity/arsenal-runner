﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour
{
	#region Intervals

	public int hours;
	public int minutes;
	public int seconds;

	#endregion

	/// <summary>
	/// The Player Prefs key to remember the destination date.
	/// </summary>
	private string PPKey;
	/// <summary>
	/// The destination date to stop counting.
	/// </summary>
	public DateTime destination;
	/// <summary>
	/// the text that represent the countdown.
	/// </summary>
	public Text timeDisplay;
	/// <summary>
	/// should the time restart itself on each interval.
	/// </summary>
	public Image timeFillAmount;
	/// <summary>
	/// should the time restart itself on each interval.
	/// </summary>
	public bool isAutoRestart;
	/// <summary>
	/// The time format you want to display in text.
	/// </summary>
	public DisplayFormat displayFormat;
	/// <summary>
	/// The callback.
	/// </summary>
	public Action callback;
	/// <summary>
	/// Attach a button if there is a button related to the timer.
	/// </summary>
	public Button btn;
	/// <summary>
	/// The DAO field for the time interval for this timer.
	/// </summary>
	public string timeDAOField;
	/// <summary>
	/// The DAO field for the text for this timer's local notification.
	/// </summary>
	public string textDAOField;
	/// <summary>
	/// Indicates whether to send a local notification at the end of the timer.
	/// </summary>
	public bool isLocalNotification;
	/// <summary>
	/// If on debugging mode - wait time is only 3 seconds
	/// </summary>
	public bool isDebugging;
	/// <summary>
	/// Indicates whether tha callback was invoked or not.
	/// </summary>
	private bool isCallbackInvoked;
	/// <summary>
	/// indicates if the timer relates to player energy refill.
	/// </summary>
	public bool isPlayerEnergyTimer;
	/// <summary>
	/// indicates if the button should start as enabled and after first click, it starts counting.
	/// </summary>
	public bool isStartEnabled;
	/// <summary>
	/// The is initialized.
	/// </summary>
	private bool isInitialized;
	/// <summary>
	/// The interval.
	/// </summary>
	private TimeSpan interval;



	public bool deleteData;

	public static TimerManager instance;


	void Awake(){
		instance = this;
	}


	void Start ()
	{
        if (PlayerPrefs.GetInt("is_free_gifts_claimed")!=1) {
			if (isStartEnabled) {

				///	Hard coded solution - temporary.
				timeDisplay.text = DAO.Language.GetString ("claim");
                
				btn.interactable = true;
				return;
			}
		}
		StartTimer ();
	}
	public void StartTimer()
	{
		interval = GetIntervalFromServer ();
		if (isPlayerEnergyTimer)
			return;
		PPKey = timeDAOField + "_destination";
		destination = GetDateFromPP ();
		if (isLocalNotification) {
			UpdateLocalNotificationDate ();
		}
		isInitialized = true;
	}

	public void UpdateLocalNotificationDate ()
	{
		if (LocalNotifications.timers.ContainsKey (timeDAOField)) {
			LocalNotifications.timers [timeDAOField] = destination;
		} else {
			LocalNotifications.timers.Add (timeDAOField, destination);
		}
	}

	public DateTime Restart ()
	{
		DateTime _destination = DateTime.Now;

		if (hours > 0)
			_destination = _destination.AddHours (hours);
		if (minutes > 0)
			_destination = _destination.AddMinutes (minutes);
		if (seconds > 0)
			_destination = _destination.AddSeconds (seconds);

		destination = _destination;
		if (isLocalNotification) {
			UpdateLocalNotificationDate ();
		}
		PlayerPrefs.SetString (PPKey, destination.ToString ());
		isCallbackInvoked = false;
		if (btn != null) {
			btn.interactable = false;
		}

		return _destination;
	}
	// Update is called once per frame
	void Update ()
	{
		if (isInitialized) {
			if (!isCallbackInvoked) {
				if (destination > DateTime.Now) {
					var span = (destination - DateTime.Now);
					switch (displayFormat) {
					case DisplayFormat.HHmmSS:
						timeDisplay.text = (string.Format ("{0}:{1}:{2}", span.Hours.ToString ("00"), span.Minutes.ToString ("00"), span.Seconds.ToString ("00")));
						break;
					case DisplayFormat.MMss:
						timeDisplay.text = (string.Format ("{0}:{1}", span.Minutes.ToString ("00"), span.Seconds.ToString ("00")));
						break;
					case DisplayFormat.FillImage:
						//timeDisplay.text = (string.Format ("{0}:{1}", span.Minutes.ToString ("00"), span.Seconds.ToString ("00")));
						float spanFloat = 1f - ((float)span.TotalSeconds / (minutes * 60f));
						timeFillAmount.fillAmount = spanFloat;
						break;
					default:
						break;
					}
				} else {
					if (callback != null)
						callback ();
					if (btn != null)
						btn.interactable = true;
					isCallbackInvoked = true;
					if (isAutoRestart) {
						Restart ();
					}
				}
			}
		}

	}

	public DateTime GetDateFromPP ()
	{
		if (deleteData)
			return Restart ();
		if (!PlayerPrefs.HasKey (PPKey))
			return Restart ();
		return(DateTime.Parse (PlayerPrefs.GetString (PPKey)));
	}

	public TimeSpan GetIntervalFromServer ()
	{
		if (isDebugging)
			return	new TimeSpan (hours, minutes, seconds);

		var tempString = isPlayerEnergyTimer ? DAO.Settings.EnergyInterval : PlayerPrefs.GetString (timeDAOField);
		var tempArray = tempString.Split (':');
		if (tempArray.Length == 3) {
			if (!int.TryParse (tempArray [0], out hours))
				hours = 4;
			if (!int.TryParse (tempArray [1], out minutes))
				minutes = 0;
			if (!int.TryParse (tempArray [2], out seconds))
				seconds = 0;
		} else {
			hours = 4;
			minutes = 0;
			seconds = 0;
		}
		return	new TimeSpan (hours, minutes, seconds);
	}

	public int OnChangePlayerToDisplay (int playerID)
	{
		int numberofCycles = 0;
		isInitialized = false;
		PPKey = "player_energy_" + playerID.ToString () + "_destination";
		destination = GetDateFromPP ();
		var span = destination - DateTime.Now;
		if (span.Milliseconds < 0) {
			TimeSpan timeToRemoveFromInterval = new TimeSpan (0, (int)span.TotalMinutes % 4, span.Seconds);
			//var timeToRemove =span.TotalSeconds / (60 * 4);
			numberofCycles = ((Math.Abs ((int)span.TotalMinutes / 4)) + 1);
			destination = DateTime.Now.Add (interval).Add (timeToRemoveFromInterval);
			PlayerPrefs.SetString (PPKey, destination.ToString ());
		}
		isInitialized = true;
		return numberofCycles;
	}

	public void FullEnergy (int playerID)
	{
		PPKey = "player_energy_" + playerID.ToString () + "_destination";
		PlayerPrefs.DeleteKey (PPKey);
	}


	public enum DisplayFormat
	{
		HHmmSS,
		MMss,
		FillImage
	}

}
