using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;
using Facebook.Unity;

[DynamoDBTable("ars_bd_giftcards_requests")]
public class RequestGiftCardsStructure{

	[DynamoDBHashKey] 
	public string RecieverId;

	[DynamoDBProperty("requesters")]
	public List<string> RequestersIds;

	public RequestGiftCardsStructure(){
		RequestersIds = new List<string> ();
	}

}

