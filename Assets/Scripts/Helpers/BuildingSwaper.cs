﻿using UnityEngine;
using System.Collections;

public class BuildingSwaper : MonoBehaviour {

	public GameObject[] buildings;
	int randomBuilding;


	void Start () {
		randomBuilding = Random.Range (0, buildings.Length);

		foreach (GameObject b in buildings) {
			if (b == buildings[randomBuilding])
				b.SetActive(true);

			else
				b.SetActive(false);
		}
	}
	

}
