﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using RTLService;
using System;
using System.Linq;

public class PlayerId : MonoBehaviour {

	public int PlayerPrefabIndex;
	GameObject playerPrefab;
	public Image Photo;
	public Sprite[] perksIcons;
	public Text PlayerNameText;
	public Image PerkIcon;
	public Text PerkName;
	//public Text playerRole;
	public GameObject roleIconGK;
	public GameObject roleIconMID;
	public GameObject roleIconDEF;
	public GameObject roleIconFR;
	//public Text PerkDescription;
	//public Text PriceText;

	//public GameObject PlayerIAPPrice;
	//public Text PlayerIAPPriceTXT;

//	public Button BuyBTN;
//	public Button InfoBTN;

//	public Text BuyBtnTXT;

	[Serializable]
	public struct PlayersImages {
		public int id;
		public Sprite image;
	}

	public PlayersImages[] playersImages;

	public PlayerStructure pl;

	
	public void SetPlayerCard (int PlayerIndex) {
		
//		playerPrefab = PrefabManager.instanse.PlayerPrefabs [PlayerPrefabIndex];
//		pl = playerPrefab.GetComponent<Player>();
		pl = PrefabManager.instanse.PlayerStructures[PlayerIndex];

		PlayerNameText.text = pl.PlayerName;
		//Photo.sprite = pl.PlayerPhoto;
		foreach (PlayersImages playerImg in playersImages) {
			if (pl.PlayerID == playerImg.id) {
				Photo.sprite = playerImg.image;
			}
		}


//		BuyBTN.onClick.AddListener (()=>{
//			Bench.Log("Player Buy Clicked");
//			Bench.instance.OnPlayerBuyClicked(PlayerPrefabIndex);
//		});
//
//		InfoBTN.onClick.AddListener (()=>{
//			Bench.Log("Player Info Clicked");
//			Bench.instance.OnPlayerInfoClicked(PlayerPrefabIndex);
//		});

		//SetPrice(PlayerChooseManager.instance.CurrentPlayerPrice);

		//PerkIcon.sprite = Perk.ins.GetIconByStringType( pl.DefaultPerk );
		foreach (Sprite icon in perksIcons) {
			if (icon.name == pl.DefaultPerk.ToString ()) {
				PerkIcon.sprite = icon;
			}
		}
		PerkName.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + Perk.GetPerkIDByPerkType( pl.DefaultPerk )]["name"]);
		//PerkDescription.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + Perk.GetPerkIDByPerkType( pl.DefaultPerk )]["desc_1"]);

//		BuyBtnTXT.text = DAO.Language.GetString ("recruit");
		//PlayerPrefabIndex = PlayerIndex;
		//playerRole.text = pl.playerRole.ToString();

		if (pl.playerRole == PlayerData.PlayerRole.DEF)
			roleIconDEF.transform.parent = transform;
		else if (pl.playerRole == PlayerData.PlayerRole.FR)
			roleIconFR.transform.parent = transform;
		else if (pl.playerRole == PlayerData.PlayerRole.GK)
			roleIconGK.transform.parent = transform;
		else if (pl.playerRole == PlayerData.PlayerRole.MID)
			roleIconMID.transform.parent = transform;
	}

//	public void SetPrice(int price){
//		
//		if(price == 0) PriceText.text = "Free";
//		else PriceText.text = price.ToString ();
//
//	}



	public void BackToBench(){
		MainScreenUiManager.instance.PlayerChooseScrnAnim (false);
	}



	public void PlayerToBuyInfoScrn(int playerNum){
		
		BackToBench();
	}



	public void OnInfoClick(){
		AudioManager.Instance.OnBTNClick();
		PlayerChooseManager.instance.BringSpecificPlayerToPreview (PlayerPrefabIndex);
	}

}
