﻿using UnityEngine;
using System.Collections;

public class UpperBodyDamage : MonoBehaviour {

	bool colided = false;

	WaitForSeconds waitForDisable = new WaitForSeconds (0.7f);

	PlayerController playerControllerScript;



	void Awake () {
		playerControllerScript = GetComponentInParent<PlayerController> ();
	}


	void OnTriggerEnter(Collider other) {
		
		if (!colided && (other.tag == "Obstacle" || other.tag == "Moving Obstacle")) {
			
			colided = true;
			playerControllerScript.upperBodyDamage = true;
			StartCoroutine (DisableUpperBodyDamage ());
		}
	}


	IEnumerator DisableUpperBodyDamage(){
		yield return waitForDisable;
		playerControllerScript.upperBodyDamage = false;
		colided = false;
	}
}
