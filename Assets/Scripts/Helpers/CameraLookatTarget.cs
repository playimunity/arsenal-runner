﻿using UnityEngine;
using System.Collections;

public class CameraLookatTarget : MonoBehaviour {

	//public static CameraLookatTarget Instance;

	public Transform playerCollider;

	Vector3 pos;

	public bool downUp = false;


	void Start(){
		StartCoroutine ("ResetPositions");
	}


	void Update () {
		pos = transform.localPosition;
		pos.x = playerCollider.localPosition.x;

		if (downUp) {
			pos.y = playerCollider.localPosition.y;
		}

		transform.localPosition = pos;
	}


	IEnumerator ResetPositions(){
		yield return new WaitForSeconds (1f);
		transform.position = playerCollider.position;
	}




	float staticYPos;

	public void TargetBackToPlace(float yPos){
		staticYPos = yPos;
		pos.y = staticYPos;

	}


}
