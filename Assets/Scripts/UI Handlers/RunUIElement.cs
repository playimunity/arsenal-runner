﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RunUIElement : MonoBehaviour {

	public RunAbstract run;

	public static int Index = 1;

	public Text RunTitle;
	public Text GoalDescription;
	//public Text GoalTitleTXT;
	public Text EnergyCost;

	public GameObject GoalTXTHolder;
	public GameObject SilverMedal;
	public GameObject GoldMedal;
	public GameObject bG;
	public GameObject flatBG;
	//public GameObject blackCover;
	public GameObject buttonParent;

	public Text BTN_Text;

	public Button CTAButton;

	public ButtonClickAnimation BCAnimation;

	public Image GoalIcon;

	public Sprite GoalImage_Adrenaline;
	public Sprite GoalImage_Coins;
	public Sprite GoalImage_DGH;
	public Sprite GoalImage_DGH_And_DUA;
	public Sprite GoalImage_Tackle;
	public Sprite GoalImage_Smash;
	public Sprite GoalImage_Time;

	public void SetView(RunAbstract runa){
		run = runa;
		runa.IndexInQuest = RunUIElement.Index-1;

		// set button & status text by statu
		if (run.status == RunAbstract.RunStatus.NOT_STARTED) {
			SetView_NotStarted ();
		} else if (run.status == RunAbstract.RunStatus.COMPLETED) {
			SetView_Completed ();
		} else if (run.status == RunAbstract.RunStatus.FAILED) {
			SetView_Failed();
		}

		// fill texts
		GoalDescription.text = RunGoal.GetGoalDescriptionByRunGoalType ( runa.goal ).goal_A;
		//GoalTitleTXT.text = DAO.Language.GetString ("goal");
		RunTitle.text = DAO.Language.GetString("run") + " " + RunUIElement.Index;
		if (GameManager.ActiveCupCompleted) {
			//Debug.Log ("alon___________ RunUIElement - Cup SetView() - GameManager.ActiveCupCompleted: " + GameManager.ActiveCupCompleted);
			EnergyCost.text = "-" + GameManager.ActiveCompletedCup.cost;
		} else {
			//Debug.Log ("alon___________ RunUIElement - Cup SetView() - GameManager.ActiveCupCompleted: " + GameManager.ActiveCupCompleted);
			EnergyCost.text = "-" + GameManager.ActiveCup.cost;
		}

		// Button Action
		CTAButton.onClick.RemoveAllListeners();
		CTAButton.onClick.AddListener (() => {
			BCAnimation.OnClicked();
			AudioManager.Instance.OnBTNClick();
			CupsManager.Instance.StartRun (runa);
		});

			
		CTAButton.interactable = true;
		if (!GameManager.ActiveCupCompleted && runa.IndexInQuest > 0) {
			if (GameManager.ActiveCup.selectedRuns [runa.IndexInQuest - 1].status != RunAbstract.RunStatus.COMPLETED) {
				CTAButton.interactable = false;
				//blackCover.SetActive (true);
				buttonParent.SetActive (false);
				flatBG.SetActive (true);
				bG.SetActive (false);
			}
		}


		if(runa.goal == RunGoal.GoalType.COLLECT_COINS){
			GoalIcon.sprite = GoalImage_Coins;
		}else if(runa.goal == RunGoal.GoalType.COLLECT_COINS_HARD){
			GoalIcon.sprite = GoalImage_Coins;
		}else if(runa.goal == RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD){
			GoalIcon.sprite = GoalImage_Time;
		}else if(runa.goal == RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS){
			GoalIcon.sprite = GoalImage_Time;
		}else if(runa.goal == RunGoal.GoalType.DONT_GET_HIT){
			GoalIcon.sprite = GoalImage_DGH;
		}else if(runa.goal == RunGoal.GoalType.KICK_BALL_AT_SPACESHIP){
			GoalIcon.sprite = GoalImage_Smash;
		}else if(runa.goal == RunGoal.GoalType.KICK_BALL_AT_SPACESHIP_HARD){
			GoalIcon.sprite = GoalImage_Smash;
		} 


	}


	void SetView_NotStarted(){
		BTN_Text.text = DAO.Language.GetString("start");

		GoalTXTHolder.SetActive (true);
		GoldMedal.SetActive (false);
		SilverMedal.SetActive (false);
	}


	void SetView_Completed(){
		BTN_Text.text = DAO.Language.GetString("run-again");

		GoalTXTHolder.SetActive (false);

		if (run.GoalAchieved == 2) {
			GoldMedal.SetActive (true);
			SilverMedal.SetActive (false);
		} else if (run.GoalAchieved == -1) {
			GoldMedal.SetActive (false);
			SilverMedal.SetActive (false);
			GoalTXTHolder.SetActive (true);
		} else {
			GoldMedal.SetActive (false);
			SilverMedal.SetActive (true);
		}

	}

	void SetView_Failed(){
		BTN_Text.text = DAO.Language.GetString("retry");

		GoalTXTHolder.SetActive (true);
		GoldMedal.SetActive (false);
		SilverMedal.SetActive (false);

	}

	void SetView_Done(){
		BTN_Text.text = DAO.Language.GetString("run-again");

		GoalTXTHolder.SetActive (true);

		if (run.GoalAchieved == 2) {
			GoldMedal.SetActive (true);
			SilverMedal.SetActive (false);
		} else if (run.GoalAchieved == -1) {
			GoldMedal.SetActive (false);
			SilverMedal.SetActive (false);
		} else {
			GoldMedal.SetActive (false);
			SilverMedal.SetActive (true);
		}

	}

}
