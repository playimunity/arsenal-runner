﻿using UnityEngine;
using System.Collections;

public class SpaceShipSmash : MonoBehaviour {



	bool collided = false;

	protected float destroyOnHeight;

	protected Vector3 tempPos;
	protected Vector3 rot;

	public ParticleSystem particles;

	public GameObject spaceShipObj;
	public GameObject obstacle;
//	public Obstacle.ObstacleTypes obstacleType;
	public Transform spaceShipTransform;

	public Animator spaceShipAnim;

	static WaitForSeconds wfs = new WaitForSeconds (1.5f);




	void Awake () {
		//StartCoroutine(BeKinematic());

	}

	void Start(){
		
		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			CupRunController.Instance.AddBoxTransform (gameObject);
		}

		rot.y = Random.Range (-90f, 90f);
		spaceShipTransform.localEulerAngles = rot;

	}



//	void OnCollisionEnter(Collision col) {
//
//		if (col.gameObject.tag == "Player" && !collided) {
//			collided = true;
//
//			if (!PlayerController.instance.god) {
//				
//				PlayerController.instance.Damage (1);
//				AudioManager.Instance.OnSoftHit ();
//				ParticleCloudeGenerator.instance.ParticleWhistle (transform.position, transform.eulerAngles);
//				gameObject.SetActive(false);//Destroy (gameObject);
//			} else {
//				ParticleCloudeGenerator.instance.ParticleBoom(transform.position, transform.eulerAngles);
//				PlayerController.instance.GodSphereFX ();
//				AudioManager.Instance.OnHitGod ();
//				gameObject.SetActive(false);//Destroy (gameObject);
//			}
//
//		}
//
//	}


	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Ball" && !collided) {
			collided = true;
			DestroyTarget();
			PlayerController.instance.ballHitTheTarget = true;
		}
	}



	void DestroyTarget(){

		AudioManager.Instance.OnBallSmash ();
		ParticleCloudeGenerator.instance.ParticleCloud (transform.position);
		RunManager.OnTargetShotByBall ();
		//particles.Play (true);
		spaceShipAnim.Play("space_ship_blow");
		obstacle.SetActive (false);
	}


	IEnumerator DeactivateObj(){
		yield return wfs;
		spaceShipObj.SetActive (false);
	}

//	static WaitForSeconds _wfs;
//	static WaitForSeconds wfs{
//		get{
//			if (_wfs == null) _wfs = new WaitForSeconds (1.5f);
//			return _wfs;
//		}
//	}

	IEnumerator BeKinematic(){
		yield return wfs;
		GetComponent<Rigidbody> ().isKinematic = true;
	}







}
