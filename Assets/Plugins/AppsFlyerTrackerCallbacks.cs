﻿using UnityEngine;
using System.Collections;


public class AppsFlyerTrackerCallbacks : MonoBehaviour {
	
	// Use this for initialization
    public string SKU = "";
    public string priceValue = "0";
    public string currency = "USD";

    public static AppsFlyerTrackerCallbacks instance;

	void Start () {
        DontDestroyOnLoad(gameObject);
		print ("AppsFlyerTrackerCallbacks on Start");
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void didReceiveConversionData(string conversionData) {
		print ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
	}
	
	public void didReceiveConversionDataWithError(string error) {
		print ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
	}
	
	public void didFinishValidateReceipt(string validateResult) {
		print ("AppsFlyerTrackerCallbacks:: got didFinishValidateReceipt  = " + validateResult);
		
	}
	
	public void didFinishValidateReceiptWithError (string error) {
		AppsFlyer.trackRichEvent ("error=" + error, new System.Collections.Generic.Dictionary<string, string>());
		
	}
	
	public void onAppOpenAttribution(string validateResult) {
		print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
		
	}
	
	public void onAppOpenAttributionFailure (string error) {
		print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
		
	}
	
    public void onInAppBillingSuccess()
    {

        AppsFlyer.trackRichEvent ("IAP Validated - "+SKU, new System.Collections.Generic.Dictionary<string, string> (){
            
            {"af_revenue", priceValue},
            {"af_currency", currency}
        });
        Debug.Log ("LALALALALA ===> - A REAL Purchase");
    }

    public void onInAppBillingFailure()
    {
        Debug.Log ("LALALALALA ===> - NOT A REAL Purchase");
        AppsFlyer.trackRichEvent ("IAP NOT Validated - "+SKU, new System.Collections.Generic.Dictionary<string, string> (){
            
            {"af_revenue", "0"},
            {"af_currency", currency}
        });
    }
}
