﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StringProvider_CrossScene : MonoBehaviour {

	// Cup Loose
//	public Text CupLoose;
//	public Text OhSnap;
//	public Text TimeIsUp;
//	public Text YouFailedToFinish;
//	public Text back;

	// Store Main
//	public Text Store_CoinsBTN;
//	public Text Store_EnergyBTN;
//	public Text Store_DealsBTN;

	// Store coins
//	public Text GetChips;
//	public Text pack1;
//	public Text pack2;
//	public Text pack3;
//	public Text pack4;
//	public Text pack5;
//	public Text pack6;
//	public Text mostPopular;
//	public Text bestvalue;

//	public Text ccTxt1;
//	public Text ccTxt2;
//	public Text ccTxt3;
//	public Text ccTxt4;
//	public Text ccTxt5;
//	public Text ccTxt6;

	// Store MedikKits
//	public Text MK_1_Name;
//	public Text MK_1_NrgTxt;
//	public Text MK_1_EnergyAmount;
//	public Text MK_1_Price;
//	public Text MK_2_Name;
//	public Text MK_2_NrgTxt;
//	public Text MK_2_EnergyAmount;
//	public Text MK_2_Price;
//	public Text MK_2_XCounter;
//	public Text MK_3_Name;
//	public Text MK_3_NrgTxt;
//	public Text MK_3_EnergyAmount;
//	public Text MK_3_Price;
//	public Text MK_3_XCounter;

	// Store SpecailOffers
//	public Text NoOffers;
//	public Text SO_1_Name;
//	public Text SO_1_Ribon;
//	public Text SO_1_Desc;
//	public Text SO_1_OldValue;
//	public Text SO_1_NewValue;
//	public Text SO_1_Price;
//	public Text SO_1_ConsTXT;

//	public Text SO_2_Name;
//	public Text SO_2_Ribon;
//	public Text SO_2_Desc;
//	public Text SO_2_OldValue;
//	public Text SO_2_NewValue;
//	public Text SO_2_Price;
//	public Text SO_2_ConsTXT;

//	public Text freeGifts;
//	public Text privacy_policy;

	// Preloader


	public void SetStrings(){
		
//		CupLoose.text = DAO.Language.GetString ("cup-loose-title");
//		OhSnap.text = DAO.Language.GetString ("oh-snap");
//		TimeIsUp.text = DAO.Language.GetString ("time-is-up");
//		YouFailedToFinish.text = DAO.Language.GetString ("you-failed-finish");
//		back.text = DAO.Language.GetString ("back");


		// Set Store - Main
//		Store_CoinsBTN.text = DAO.Language.GetString ("only-coins");
//		Store_EnergyBTN.text = DAO.Language.GetString ("fitness");
//		Store_DealsBTN.text = DAO.Language.GetString ("deals");

		// Set Store - Coins
//		GetChips.text = DAO.Language.GetString ("shop");
//		pack1.text = DAO.Language.GetString ("pack1-btn");
//		pack2.text = DAO.Language.GetString ("pack2-btn");
//		pack3.text = DAO.Language.GetString ("pack3-btn");
//		pack4.text = DAO.Language.GetString ("pack4-btn");
//		pack5.text = DAO.Language.GetString ("pack5-btn");
//		pack6.text = DAO.Language.GetString ("pack6-btn");

//		ccTxt1.text = DAO.Language.GetString ("only-coins");
//		ccTxt2.text = DAO.Language.GetString ("only-coins");
//		ccTxt3.text = DAO.Language.GetString ("only-coins");
//		ccTxt4.text = DAO.Language.GetString ("only-coins");
//		ccTxt5.text = DAO.Language.GetString ("only-coins");
//		ccTxt6.text = DAO.Language.GetString ("only-coins");

//		mostPopular.text = DAO.Language.GetString ("most-popular");
//		bestvalue.text = DAO.Language.GetString ("best-value");


		// Set Store - Medic Kits
//		MK_1_Name.text = DAO.Language.GetString ("energy-pack");
//		MK_1_NrgTxt.text = DAO.Language.GetString ("fitness");
//		MK_1_EnergyAmount.text = "+" + DAO.Settings.MedikKits[0]["h"].AsInt * 30;
//		MK_1_Price.text = DAO.Settings.MedikKits[0]["p"].Value;

//		MK_2_Name.text = DAO.Language.GetString ("medikits-pack2");
//		MK_2_NrgTxt.text = DAO.Language.GetString ("energy-pack");
//		MK_2_EnergyAmount.text = "+" + DAO.Settings.MedikKits[1]["h"].AsInt * 30;
//		MK_2_Price.text = DAO.Settings.MedikKits[1]["p"].Value;
//		MK_2_XCounter.text = DAO.Settings.MedikKits [1]["h"].Value;

//		MK_3_Name.text = DAO.Language.GetString ("medikits-pack3");
//		MK_3_NrgTxt.text = DAO.Language.GetString ("energy-pack");
//		MK_3_EnergyAmount.text = "+" + DAO.Settings.MedikKits[2]["h"].AsInt * 30;
//		MK_3_Price.text = DAO.Settings.MedikKits[2]["p"].Value;
//		MK_3_XCounter.text = DAO.Settings.MedikKits [2]["h"].Value;

		// Set Store - Special Deals
//		NoOffers.text = DAO.Language.GetString ("no-offers");
//
//		SO_1_Name.text = DAO.Language.GetString ("starter-pack-name");
//		SO_1_Ribon.text = DAO.Language.GetString ("off50");
//		SO_1_Desc.text = DAO.Language.GetString ("starter-pack-desc");
//		SO_1_OldValue.text = DAO.Settings.SpecialOffers ["starter-pack"] ["old-value"].Value;
//		SO_1_NewValue.text = IAP.Pack_starter_Value.ToString ();
		//ShmulikAlert
//		SO_1_Price.text = IAP.Instance.Product_STARTER_DEAL.GetPriceLabel();
//		SO_1_ConsTXT.text = DAO.Language.GetString ("only-coins");

//		SO_2_Name.text = DAO.Language.GetString ("fcb-megadeal-name");
//		SO_2_Ribon.text = DAO.Language.GetString ("one-time-offer");
//		SO_2_Desc.text = DAO.Language.GetString ("fcb-megadeal-desc");
//		SO_2_OldValue.text = DAO.Settings.SpecialOffers ["fcb-mega-deal"] ["old-value"].Value;
//		SO_2_NewValue.text = IAP.Pack_fcbmega_Value.ToString ();
		//ShmulikAlert
//		SO_2_Price.text = IAP.Instance.Product_FCB_MEGA_PACK.GetPriceLabel();
//		SO_2_ConsTXT.text = DAO.Language.GetString ("only-coins");

//		freeGifts.text = DAO.Language.GetString ("free-gifts");
		//privacy_policy.text = "Privacy Policy";


	}

}
