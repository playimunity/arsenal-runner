using UnityEngine;
using System.Collections;

[System.Serializable]
public class RunGoal{

	public enum GoalType {
		COLLECT_COINS,
		COLLECT_COINS_HARD,
		//COMPLETE_RUN,
		KICK_BALL_AT_SPACESHIP,
		KICK_BALL_AT_SPACESHIP_HARD,
		//COLLECT_ENERGY_DRINKS,
		DONT_GET_HIT,
		//TACKLE_ENEMIES,
		COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS,
		COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD,
		TUTORIAL,
		TEST
	}
	public GoalType goal;
	public GoalDescription goalDescription;

	public int coinsCollected = 0;
	public int hits = 0;
	public int spaceShipsDown = 0;
	//public int energyDrinksCollected = 0;
	public int playTime = 0;
	//public int enemiesTackled = 0;
	//public bool SheildUsed = false;
	//public bool SpeedBoostUsed = false;

	public bool playerDie = false;

	public static float COINS_COLECT_FOR_1_MEDALS = 100f;
	public static float COINS_COLECT_FOR_2_MEDALS = 150f;

	public static float COINS_COLECT_HARD_FOR_1_MEDALS = 250f;
	public static float COINS_COLECT_HARD_FOR_2_MEDALS = 300f;

	public static float KICK_BALL_AT_SPACESHIP_FOR_1_MEDAL = 5f;
	public static float KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL = 7f;

	public static float KICK_BALL_AT_SPACESHIP_HARD_FOR_1_MEDAL = 8f;
	public static float KICK_BALL_AT_SPACESHIP_HARD_FOR_2_MEDAL = 10f;

//	public static float COLLECT_ENERGY_DRINKS_FOR_1_MEDAL = 3f;
//	public static float COLLECT_ENERGY_DRINKS_FOR_2_MEDAL = 5f;
//	public static float TACKLE_ENEMIES_FOR_1_MEDAL = 3f;
//	public static float TACKLE_ENEMIES_FOR_2_MEDAL = 5f;

	public static float COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL = 90f;
	public static float COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL = 70f;

	public static float COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_1_MEDAL = 90f;
	public static float COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL = 70f;

	public static float COMPLETE_RUN_HITS_TO_2_MEDALS = 0f;
	public static float COMPLETE_RUN_HITS_TO_1_MEDALS = 1f;



	public static GoalType GetGoalTypeByGoalString(string goal){
		if (goal == "collect_coins") return GoalType.COLLECT_COINS;
		if (goal == "collect_coins_hard") return GoalType.COLLECT_COINS_HARD;
		//if( goal == "complete_run" )return GoalType.COMPLETE_RUN;
		if( goal == "kick_ball" )return GoalType.KICK_BALL_AT_SPACESHIP;
		if( goal == "kick_ball" )return GoalType.KICK_BALL_AT_SPACESHIP_HARD;
		//if( goal == "collect_energy" )return GoalType.COLLECT_ENERGY_DRINKS;
		if( goal == "dont_get_hit" )return GoalType.DONT_GET_HIT;
		//if( goal == "tackle" )return GoalType.TACKLE_ENEMIES;
		if( goal == "less_than_x_sec" )return GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS;
		if( goal == "less_than_x_sec" )return GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD;
		if( goal == "tutorial" ) return GoalType.TUTORIAL;
		if( goal == "test" ) return GoalType.TEST;

		// Default
		return GoalType.COLLECT_COINS;
	}

	public static GoalDescription GetGoalDescriptionByRunGoalType(GoalType type){
		if (type == GoalType.COLLECT_COINS) return DAO.Language.RunGoal_CollectCoins;
		if (type == GoalType.COLLECT_COINS_HARD) return DAO.Language.RunGoal_CollectCoins;
		//if (type == GoalType.COLLECT_ENERGY_DRINKS) return DAO.Language.RunGoal_CollectEnergyDrinks;
		if (type == GoalType.KICK_BALL_AT_SPACESHIP) return DAO.Language.RunGoal_KickBallAtSpaceShip;
		if (type == GoalType.KICK_BALL_AT_SPACESHIP_HARD) return DAO.Language.RunGoal_KickBallAtSpaceShip;
		//if (type == GoalType.TACKLE_ENEMIES) return DAO.Language.RunGoal_TackleEnemies;
		//if (type == GoalType.COMPLETE_RUN) return DAO.Language.RunGoal_CompleteRun;
		if (type == GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS) return DAO.Language.RunGoal_CompleteRunInLessXSec;
		if (type == GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD) return DAO.Language.RunGoal_CompleteRunInLessXSec;
		if (type == GoalType.DONT_GET_HIT) return DAO.Language.RunGoal_DontGetHit;
		if (type == GoalType.TUTORIAL) return DAO.Language.RunGoal_Tutorial;
		if (type == GoalType.TEST) return DAO.Language.RunGoal_Test;

		// Default
		return DAO.Language.RunGoal_CollectCoins;
	}

	public int medalsWon(){		// if 0 -> goal not reached
		int medals = 0;

		if (playerDie) return 0;

		switch(goal){
			case GoalType.COLLECT_COINS : {
			if(coinsCollected >=  COINS_COLECT_FOR_1_MEDALS) medals = 1;
			if(coinsCollected >=  COINS_COLECT_FOR_2_MEDALS) medals = 2;
				break;
			}

		case GoalType.COLLECT_COINS_HARD : {
				if(coinsCollected >=  COINS_COLECT_HARD_FOR_1_MEDALS) medals = 1;
				if(coinsCollected >=  COINS_COLECT_HARD_FOR_2_MEDALS) medals = 2;
				break;
			}

//			case GoalType.COMPLETE_RUN : {
//				if( hits == 0 ) medals = 2;
//				else medals = 1;
//				break;
//			}

			case GoalType.KICK_BALL_AT_SPACESHIP : {
				if(spaceShipsDown >= KICK_BALL_AT_SPACESHIP_FOR_1_MEDAL) medals = 1;
				if(spaceShipsDown >=  KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL) medals = 2;
				break;
			}

		case GoalType.KICK_BALL_AT_SPACESHIP_HARD : {
				if(spaceShipsDown >= KICK_BALL_AT_SPACESHIP_HARD_FOR_1_MEDAL) medals = 1;
				if(spaceShipsDown >=  KICK_BALL_AT_SPACESHIP_HARD_FOR_2_MEDAL) medals = 2;
				break;
			}

//			case GoalType.COLLECT_ENERGY_DRINKS : {
//				if(energyDrinksCollected >=  COLLECT_ENERGY_DRINKS_FOR_1_MEDAL) medals = 1;
//				if(energyDrinksCollected >=  COLLECT_ENERGY_DRINKS_FOR_2_MEDAL) medals = 2;
//				break;
//			}

			case GoalType.DONT_GET_HIT : {
				if( hits == 0 ) medals = 2;
				else medals = 1;
				break;
			}

//			case GoalType.TACKLE_ENEMIES : {
//				if(enemiesTackled >=  TACKLE_ENEMIES_FOR_1_MEDAL) medals = 1;
//				if(enemiesTackled >=  TACKLE_ENEMIES_FOR_2_MEDAL) medals = 2;
//				break;
//			}

			case GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS : {
				if(playTime <= COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL && playTime > COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL) medals = 1;
				else if(playTime <=  COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL) medals = 2;

//				if(playTime <= 90 && playTime > 70) medals = 1;
//				else if(playTime <=  70) medals = 2;

				break;
			}

		case GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD : {
				if(playTime <= COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_1_MEDAL && playTime > COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL) medals = 1;
				else if(playTime <=  COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL) medals = 2;

				break;
			}

			case GoalType.TUTORIAL : 
			case GoalType.TEST : {
				medals = 2;
				break;
			}

		}

		return medals;
	}

	public void ResetAll(){
		coinsCollected = 0;
		hits = 0;
		spaceShipsDown = 0;
		//energyDrinksCollected = 0;
		playTime = 0;
		//enemiesTackled = 0;
		//SheildUsed = false;
		playerDie = false;
	}

}
