﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIUpdater_cupRun : MonoBehaviour {

	public static UIUpdater_cupRun instance;

	enum MedalTypes { NO, SILVER, GOLD };
	MedalTypes CurMedalType = MedalTypes.NO;

	int FRAMERATE = 10;
	int CUR_FRAME = 0;
    float PB_WIDTH = 610f;

	//public Text Whistles;
	//public Text totalMagnetsTXT;
	//public Text totalEnergyDrinksTXT;

	public GameObject BP_Parent;
	public RectTransform PB_Bar_Silver;
	public RectTransform PB_Bar_Gold;   
	public RectTransform PB_Seperator;
    public RectTransform PB_SilverLine;
   

	public RectTransform PB_GoldenLine;
	//public Text PB_Text;
	public Text AnimatedText;

	public Text Count_RunGoal_A;
	public Text Count_RunGoal_B;

	public Text RankA_TXT;
	public Text RankB_TXT;
	public Text RankA_Desc;
	public Text RankB_Desc;

	public Text PlayerNameTXT;
	public Text PlayerNumberTXT;
	public Text bottomGoalDiscritionTxt;

	//public GameObject ReplayVideoBTN;
	public GameObject StopRecordingAndWatchVideoBTN;
	//public Text saveMeFreeTXT;
	bool ThereISRecord = false;

	event Action OnProgressBarUpdate;

	float SeperatorPosition;
	float GoldenLinePosition;

	Vector2 AncPos;
	Vector2 TMP_VECTOR;
	Vector2 PBGoldStartSize;
	Vector2 PBSilverStartSize;
	Vector2 PBGoldLineStartPos;
	Vector2 PBSilverLineStartPos;

	int curWhistlesAmount = 0;
	float TMP_FLOAT = 0f;
	string PB_Text_prefix;
	float PB_updateStep;

	bool SilverAchieved = false;
	bool GoldAchieved = false;

	float MARKER_LIMIT_MAX = 877f;
	float MARKER_LIMIT_MIN = 68f;
	float PB_MIN_WIDTH = 0f;

	//public Text pBCounterValue;


	void Awake(){
		instance = this;
        PB_Bar_Gold = HeaderManager.instance.goldFill;
        PB_WIDTH = HeaderManager.instance.fullBarWidth.sizeDelta.x;
        PB_Bar_Silver = HeaderManager.instance.silverFill;
        PB_Seperator = HeaderManager.instance.seperator;
        PB_GoldenLine = HeaderManager.instance.goldLine;
        PB_SilverLine = HeaderManager.instance.silverLine;
	}
		

	void FixedUpdate () {
		if (CUR_FRAME < 0) CUR_FRAME = FRAMERATE;
		if (CUR_FRAME == 0) UpdateUI();
		CUR_FRAME--;
	}

	void UpdateUI(){
		//totalMagnetsTXT.text = DAO.TotalMagnetsCollected.ToString();
//		totalEnergyDrinksTXT.text = DAO.TotalSpeedBoostsCollected.ToString();

		UpdateWhistles ();
		if (OnProgressBarUpdate != null) OnProgressBarUpdate ();
	}




	public void RestartScene(){
		GameManager.SwitchState(GameManager.GameState.CUP_RUN);
		CrossSceneUIHandler.Instance.sparklesOn = false;
	}

	void Start(){
		SetProgressBar ();

		RankA_TXT.text = DAO.Language.GetString ("rank_a");
		RankB_TXT.text = DAO.Language.GetString ("rank_b");
		RankA_Desc.text = Run.Instance.runGoal.goalDescription.Rank_A_Goal;
		RankB_Desc.text = Run.Instance.runGoal.goalDescription.Rank_B_Goal;


		Count_RunGoal_A.text = Run.Instance.runGoal.goalDescription.Rank_A_Full;
		Count_RunGoal_B.text = Run.Instance.runGoal.goalDescription.Rank_B_Full;

		PlayerNameTXT.text = PlayerController.instance.data.player.playerName;
		PlayerNumberTXT.text = PlayerController.instance.data.player.playerNumber.ToString();

		PBGoldStartSize = PB_Bar_Gold.sizeDelta;
		PBGoldLineStartPos = PB_GoldenLine.anchoredPosition;
			
		PBSilverStartSize = PB_Bar_Silver.sizeDelta;
		PBSilverLineStartPos = PB_Seperator.anchoredPosition;
		//saveMeFreeTXT.text = DAO.Language.GetString ("free");
	}

	public void SetProgressBar(){
		AncPos = PB_Seperator.anchoredPosition;
        print("******SETTING PROGRESS BAR*******");
		switch (Run.Instance.runGoal.goal) {

			case RunGoal.GoalType.COLLECT_COINS :{

				SeperatorPosition = PB_WIDTH * (RunGoal.COINS_COLECT_FOR_1_MEDALS / RunGoal.COINS_COLECT_FOR_2_MEDALS)+HeaderManager.instance.Seperator_Gap;
				if (SeperatorPosition > MARKER_LIMIT_MAX) SeperatorPosition = MARKER_LIMIT_MAX;
				if (SeperatorPosition < MARKER_LIMIT_MIN) SeperatorPosition = MARKER_LIMIT_MIN;

				AncPos.x = SeperatorPosition;
				PB_Seperator.anchoredPosition = AncPos;
                PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x- HeaderManager.instance.SilverLine_Gap, PB_Seperator.anchoredPosition.y);
				PB_Text_prefix = DAO.Language.GetString ("chips-collected") + " ";
				//PB_Text_prefix + "0";

				PB_updateStep = PB_WIDTH / RunGoal.COINS_COLECT_FOR_2_MEDALS;

				TMP_VECTOR = PB_Bar_Silver.sizeDelta;
				TMP_VECTOR.x = PB_MIN_WIDTH;
				TMP_FLOAT = PB_MIN_WIDTH;
				PB_Bar_Silver.sizeDelta = PB_Bar_Gold.sizeDelta = TMP_VECTOR;

//				pBCounterValue.text = Run.Instance.runGoal.coinsCollected + "/" + (int)RunGoal.COINS_COLECT_FOR_2_MEDALS;
				bottomGoalDiscritionTxt.text = DAO.Language.RunGoal_CollectCoins.Rank_A_Goal;

				OnProgressBarUpdate += updatePB_onCoinsCollect;

				break;
			}

		case RunGoal.GoalType.COLLECT_COINS_HARD :{
				SeperatorPosition = PB_WIDTH * (RunGoal.COINS_COLECT_HARD_FOR_1_MEDALS / RunGoal.COINS_COLECT_HARD_FOR_2_MEDALS)+HeaderManager.instance.Seperator_Gap;
				if (SeperatorPosition > MARKER_LIMIT_MAX) SeperatorPosition = MARKER_LIMIT_MAX;
				if (SeperatorPosition < MARKER_LIMIT_MIN) SeperatorPosition = MARKER_LIMIT_MIN;

				AncPos.x = SeperatorPosition;
				PB_Seperator.anchoredPosition = AncPos;
				PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x- HeaderManager.instance.SilverLine_Gap, PB_Seperator.anchoredPosition.y);
				PB_Text_prefix = DAO.Language.GetString ("chips-collected") + " ";
				//PB_Text_prefix + "0";

				PB_updateStep = PB_WIDTH / RunGoal.COINS_COLECT_HARD_FOR_2_MEDALS;

				TMP_VECTOR = PB_Bar_Silver.sizeDelta;
				TMP_VECTOR.x = PB_MIN_WIDTH;
				TMP_FLOAT = PB_MIN_WIDTH;
				PB_Bar_Silver.sizeDelta = PB_Bar_Gold.sizeDelta = TMP_VECTOR;

//				pBCounterValue.text = Run.Instance.runGoal.coinsCollected + "/" + (int)RunGoal.COINS_COLECT_HARD_FOR_2_MEDALS;
				bottomGoalDiscritionTxt.text = DAO.Language.RunGoal_CollectCoinsHard.Rank_A_Goal;

				OnProgressBarUpdate += updatePB_onCoinsCollect;
				break;
			}

		case RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD :{

				//GoldenLinePosition = PB_WIDTH - (PB_WIDTH * (RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL / RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_1_MEDAL));
                SeperatorPosition = PB_WIDTH - (PB_WIDTH * (RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL / RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL));
				if (GoldenLinePosition > MARKER_LIMIT_MAX) GoldenLinePosition = MARKER_LIMIT_MAX;
				if (GoldenLinePosition < MARKER_LIMIT_MIN) GoldenLinePosition = MARKER_LIMIT_MIN;


				AncPos = PB_GoldenLine.anchoredPosition;
				AncPos.x = SeperatorPosition;
				PB_Seperator.anchoredPosition = AncPos;
                //PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x, PB_Seperator.anchoredPosition.y);
                PB_SilverLine.gameObject.SetActive(false);
				PB_Text_prefix = DAO.Language.GetString ("seconds-remain")+" ";
//				PB_Text_prefix + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL;

				TMP_VECTOR = PB_Bar_Silver.sizeDelta;
				TMP_VECTOR.x = PB_WIDTH;
				TMP_FLOAT =PB_WIDTH;

				PB_Bar_Gold.sizeDelta = TMP_VECTOR;
                TMP_VECTOR.x = SeperatorPosition;
				PB_Bar_Silver.sizeDelta = TMP_VECTOR;

				CurMedalType = MedalTypes.GOLD;

				PB_updateStep = PB_WIDTH / RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_1_MEDAL;

//				pBCounterValue.text = Run.Instance.runGoal.playTime + "/" + (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL;
				bottomGoalDiscritionTxt.text = DAO.Language.RunGoal_CompleteRunInLessXSecHard.Rank_A_Goal;

				OnProgressBarUpdate += updatePB_onCompleteRunLessXSec;

				break;
			}

			case RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS :{
				
				//GoldenLinePosition = PB_WIDTH - (PB_WIDTH * (RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL / RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL));
                SeperatorPosition = PB_WIDTH - (PB_WIDTH * (RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL / RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL));
				if (GoldenLinePosition > MARKER_LIMIT_MAX) GoldenLinePosition = MARKER_LIMIT_MAX;
				if (GoldenLinePosition < MARKER_LIMIT_MIN) GoldenLinePosition = MARKER_LIMIT_MIN;


				AncPos = PB_GoldenLine.anchoredPosition;
				AncPos.x = SeperatorPosition;
                    PB_Seperator.anchoredPosition = AncPos;
                    PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x, PB_Seperator.anchoredPosition.y);
				PB_Text_prefix = DAO.Language.GetString ("seconds-remain")+" ";
//				PB_Text_prefix + RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL;

				TMP_VECTOR = PB_Bar_Silver.sizeDelta;
				TMP_VECTOR.x = PB_WIDTH;
				TMP_FLOAT =PB_WIDTH;

				PB_Bar_Gold.sizeDelta = TMP_VECTOR;
                    TMP_VECTOR.x = SeperatorPosition;
				PB_Bar_Silver.sizeDelta = TMP_VECTOR;

				CurMedalType = MedalTypes.GOLD;

				PB_updateStep = PB_WIDTH / RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL;

//				pBCounterValue.text = Run.Instance.runGoal.playTime + "/" + (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL;
				bottomGoalDiscritionTxt.text = DAO.Language.RunGoal_CompleteRunInLessXSec.Rank_A_Goal;

				OnProgressBarUpdate += updatePB_onCompleteRunLessXSec;

				break;
			}

			case RunGoal.GoalType.KICK_BALL_AT_SPACESHIP :{
                    SeperatorPosition = PB_WIDTH * (RunGoal.KICK_BALL_AT_SPACESHIP_FOR_1_MEDAL / RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL)+HeaderManager.instance.Seperator_Gap;
				if (SeperatorPosition > MARKER_LIMIT_MAX) SeperatorPosition = MARKER_LIMIT_MAX;
				if (SeperatorPosition < MARKER_LIMIT_MIN) SeperatorPosition = MARKER_LIMIT_MIN;


				AncPos.x = SeperatorPosition;
				PB_Seperator.anchoredPosition = AncPos;
                    PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x- HeaderManager.instance.SilverLine_Gap, PB_Seperator.anchoredPosition.y);
				PB_Text_prefix = DAO.Language.GetString ("targets-down") + " ";
//				PB_Text_prefix + "0";
				
				TMP_VECTOR = PB_Bar_Silver.sizeDelta;
				TMP_VECTOR.x = PB_MIN_WIDTH;
				TMP_FLOAT = PB_MIN_WIDTH;
				PB_Bar_Silver.sizeDelta = PB_Bar_Gold.sizeDelta = TMP_VECTOR;

				PB_updateStep = PB_WIDTH / RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL;

//				pBCounterValue.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL;
				bottomGoalDiscritionTxt.text = DAO.Language.RunGoal_KickBallAtSpaceShip.Rank_A_Goal;

				OnProgressBarUpdate += updatePB_onKickTargets;

				break;
			}

			case RunGoal.GoalType.DONT_GET_HIT :{ // silver if you use adrenalin. fail if you get hit. 
               
				SeperatorPosition = PB_WIDTH / 2f+HeaderManager.instance.Seperator_Gap;
				if (SeperatorPosition > MARKER_LIMIT_MAX) SeperatorPosition = MARKER_LIMIT_MAX;
				if (SeperatorPosition < MARKER_LIMIT_MIN) SeperatorPosition = MARKER_LIMIT_MIN;


				AncPos = PB_Seperator.anchoredPosition;
				AncPos.x = SeperatorPosition;
				PB_Seperator.anchoredPosition = AncPos;
				PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x- HeaderManager.instance.SilverLine_Gap, PB_Seperator.anchoredPosition.y);
				PB_Text_prefix = "Hits: ";
//				PB_Text_prefix + "0";

				TMP_VECTOR = PB_Bar_Silver.sizeDelta;
				TMP_VECTOR.x = PB_WIDTH;
				TMP_FLOAT =PB_WIDTH;
				PB_Bar_Gold.sizeDelta = TMP_VECTOR;
				TMP_VECTOR.x = PB_WIDTH / 2f;
				PB_Bar_Silver.sizeDelta = TMP_VECTOR;

				CurMedalType = MedalTypes.GOLD;

				PB_updateStep = PB_WIDTH / 2f;

				//pBCounterValue.text = Run.Instance.runGoal.hits + "/" + (int)RunGoal.COMPLETE_RUN_HITS_TO_2_MEDALS;
//				pBCounterValue.text = "";
				bottomGoalDiscritionTxt.text = DAO.Language.RunGoal_DontGetHit.Rank_A_Goal;

				//OnProgressBarUpdate += updatePB_onCompleteRun;
                    OnProgressBarUpdate += GotHitEvent;
				break;
			}


		case RunGoal.GoalType.KICK_BALL_AT_SPACESHIP_HARD :{
				SeperatorPosition = PB_WIDTH * (RunGoal.KICK_BALL_AT_SPACESHIP_HARD_FOR_1_MEDAL / RunGoal.KICK_BALL_AT_SPACESHIP_HARD_FOR_2_MEDAL)+HeaderManager.instance.Seperator_Gap;
				if (SeperatorPosition > MARKER_LIMIT_MAX) SeperatorPosition = MARKER_LIMIT_MAX;
				if (SeperatorPosition < MARKER_LIMIT_MIN) SeperatorPosition = MARKER_LIMIT_MIN;


				AncPos.x = SeperatorPosition;
				PB_Seperator.anchoredPosition = AncPos;
				PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x- HeaderManager.instance.SilverLine_Gap, PB_Seperator.anchoredPosition.y);
				PB_Text_prefix = DAO.Language.GetString ("targets-down") + " ";
//				PB_Text_prefix + "0";

				TMP_VECTOR = PB_Bar_Silver.sizeDelta;
				TMP_VECTOR.x = PB_MIN_WIDTH;
				TMP_FLOAT = PB_MIN_WIDTH;
				PB_Bar_Silver.sizeDelta = PB_Bar_Gold.sizeDelta = TMP_VECTOR;

				PB_updateStep = PB_WIDTH / RunGoal.KICK_BALL_AT_SPACESHIP_HARD_FOR_2_MEDAL;

//				pBCounterValue.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_HARD_FOR_2_MEDAL;
				bottomGoalDiscritionTxt.text = DAO.Language.RunGoal_KickBallAtSpaceShipHard.Rank_A_Goal;

				OnProgressBarUpdate += updatePB_onKickTargets;

				break;
			}
            default:
                print(Run.Instance.runGoal.goal);
                break;
		}

	}


	IEnumerator UpdateSilverPBFill(float to, float speed){
		TMP_VECTOR = PB_Bar_Silver.sizeDelta;

		while (TMP_VECTOR.x < to) {
			TMP_VECTOR.x = Mathf.Lerp (TMP_VECTOR.x, to, speed * Time.deltaTime);
			if (TMP_VECTOR.x < PB_MIN_WIDTH) TMP_VECTOR.x = PB_MIN_WIDTH;

			if(!SilverAchieved) PB_Bar_Silver.sizeDelta = TMP_VECTOR;
			yield return null;
		}


	}

	IEnumerator UpdateSilverPBFillBackwards(float to, float speed){
		TMP_VECTOR = PB_Bar_Silver.sizeDelta;
		
		while (TMP_VECTOR.x > to + 10f) {
			TMP_VECTOR.x = Mathf.Lerp (TMP_VECTOR.x, to, speed * Time.deltaTime);
//			Debug.Log ("alon________ UpdateSilverPBFillBackwards - TMP_VECTOR.x = " + TMP_VECTOR.x + " to = " + to);
			if (TMP_VECTOR.x < PB_MIN_WIDTH) {
				TMP_VECTOR.x = PB_MIN_WIDTH;
				yield break;
			}

			PB_Bar_Silver.sizeDelta = TMP_VECTOR;


			yield return null;
		}
		
		
	}

	IEnumerator UpdateGoldPBFill(float to, float speed){
		TMP_VECTOR = PB_Bar_Gold.sizeDelta;

		while (TMP_VECTOR.x < to) {
			TMP_VECTOR.x = Mathf.Lerp(TMP_VECTOR.x, to, speed * Time.deltaTime);
			if (TMP_VECTOR.x < PB_MIN_WIDTH) TMP_VECTOR.x = PB_MIN_WIDTH;

			if(!GoldAchieved) PB_Bar_Gold.sizeDelta = TMP_VECTOR;
			yield return null;
		}

	}

	IEnumerator UpdateGoldPBFillBackwards(float to, float speed){
		TMP_VECTOR = PB_Bar_Gold.sizeDelta;

		while (TMP_VECTOR.x > to + 10f) {
			TMP_VECTOR.x = Mathf.Lerp(TMP_VECTOR.x, to, speed * Time.deltaTime);
			//Debug.Log ("alon________ UpdateGoldPBFillBackwards");
			if (TMP_VECTOR.x < PB_MIN_WIDTH) {
				TMP_VECTOR.x = PB_MIN_WIDTH;
				yield break;
			}

			if(!GoldAchieved) PB_Bar_Gold.sizeDelta = TMP_VECTOR;


			yield return null;
		}
		
	}

	public void ResetPBOnSaveMe(){
//    if (GameManager.CurrentRun.goal == RunGoal.GoalType.DONT_GET_HIT)
//    {
//            StopCoroutine("UpdateSilverPBFillBackwards");
//            SetProgressBar();
//            return;
//    }
		if (secondHitOn) {

			PB_Bar_Gold.sizeDelta = PBGoldStartSize;
			PB_GoldenLine.anchoredPosition = PBGoldLineStartPos;

			PB_Bar_Silver.sizeDelta = PBSilverStartSize;
			PB_Seperator.anchoredPosition = PBSilverLineStartPos;
            PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x- HeaderManager.instance.SilverLine_Gap, PB_Seperator.anchoredPosition.y);


			SeperatorPosition = PB_WIDTH / 2f;
			if (SeperatorPosition > MARKER_LIMIT_MAX)
				SeperatorPosition = MARKER_LIMIT_MAX;
			if (SeperatorPosition < MARKER_LIMIT_MIN)
				SeperatorPosition = MARKER_LIMIT_MIN;

			AncPos = PB_Seperator.anchoredPosition;
			AncPos.x = SeperatorPosition;
			PB_Seperator.anchoredPosition = AncPos;
            PB_SilverLine.anchoredPosition = new Vector2(PB_Seperator.anchoredPosition.x- HeaderManager.instance.SilverLine_Gap, PB_Seperator.anchoredPosition.y);
			PB_Text_prefix = "Hits: ";
//			PB_Text_prefix + "0";

			TMP_VECTOR = PB_Bar_Silver.sizeDelta;
			TMP_VECTOR.x = PB_WIDTH;
			TMP_FLOAT = PB_WIDTH;
			PB_Bar_Gold.sizeDelta = TMP_VECTOR;
			TMP_VECTOR.x = PB_WIDTH / 2f;
			PB_Bar_Silver.sizeDelta = TMP_VECTOR;

			CurMedalType = MedalTypes.GOLD;

			PB_updateStep = PB_WIDTH / 2f;

			firstHitOn = false;
			secondHitOn = false;
			//adrenalineTook = false;
			//gotHit = false;

		}
	}


	void updatePB_onCoinsCollect(){
		
//		pBCounterValue.text = Run.Instance.runGoal.coinsCollected + "/" + (int)RunGoal.COINS_COLECT_FOR_2_MEDALS;

		if (Run.Instance.runGoal.coinsCollected < RunGoal.COINS_COLECT_FOR_1_MEDALS) {

			StartCoroutine ( UpdateSilverPBFill(PB_updateStep * Run.Instance.runGoal.coinsCollected, 4f) );

		} else if (Run.Instance.runGoal.coinsCollected < RunGoal.COINS_COLECT_FOR_2_MEDALS) {



			if (!SilverAchieved) HandleSilverAchieved ();

			StartCoroutine ( UpdateGoldPBFill(PB_updateStep * Run.Instance.runGoal.coinsCollected, 4f) );

		} else {
			if(!GoldAchieved) HandleGoldAchieved();
		}

//		PB_Text_prefix + Run.Instance.runGoal.coinsCollected;

	}

//	void updatePB_onEnergyCollect(){
//
//		//pBCounterValue.text = Run.Instance.runGoal.energyDrinksCollected + "/" + (int)RunGoal.COLLECT_ENERGY_DRINKS_FOR_2_MEDAL;
//
//		if (Run.Instance.runGoal.energyDrinksCollected < RunGoal.COLLECT_ENERGY_DRINKS_FOR_1_MEDAL) {
//			
//			StartCoroutine ( UpdateSilverPBFill(PB_updateStep * Run.Instance.runGoal.energyDrinksCollected, 4f) );
//			
//		} else if (Run.Instance.runGoal.energyDrinksCollected < RunGoal.COLLECT_ENERGY_DRINKS_FOR_2_MEDAL) {
//			
//			
//			
//			if (!SilverAchieved) HandleSilverAchieved ();
//			
//			StartCoroutine ( UpdateGoldPBFill(PB_updateStep * Run.Instance.runGoal.energyDrinksCollected, 4f) );
//			
//		} else {
//			if(!GoldAchieved) HandleGoldAchieved();
//		}
//
//		PB_Text.text = PB_Text_prefix + Run.Instance.runGoal.energyDrinksCollected;
//
//	}

	bool firstHitOn = false;
	bool secondHitOn = false;
    void GotHitEvent()
    {
//        pBCounterValue.text ="";

        if (!firstHitOn && Run.Instance.runGoal.hits == RunGoal.COMPLETE_RUN_HITS_TO_1_MEDALS) {
            firstHitOn = true;

            if (PlayerController.instance.isDead) Run.Instance.runGoal.hits = 2;
            else if (!SilverAchieved) HandleDecreasedSilverAchieved ();
            
            StartCoroutine ( UpdateGoldPBFillBackwards(0f, 4f) );
			//PlayerController.instance.dontGetHitFirstHit = true;
            
        //} else if (!secondHitOn && Run.Instance.runGoal.hits > RunGoal.COMPLETE_RUN_HITS_TO_1_MEDALS){
		} else if (!secondHitOn && PlayerController.instance.HitsRemain < RunGoal.COMPLETE_RUN_HITS_TO_1_MEDALS){
            secondHitOn = true;
            
            StartCoroutine ( UpdateSilverPBFillBackwards(0f, 4f) );


        }
            
//       PB_Text_prefix + Run.Instance.runGoal.hits;
    }
	void updatePB_onCompleteRun(){

		//pBCounterValue.text = Run.Instance.runGoal.hits + "/" + (int)RunGoal.COMPLETE_RUN_HITS_TO_2_MEDALS;
//		pBCounterValue.text ="";

		if (!firstHitOn && Run.Instance.runGoal.hits == RunGoal.COMPLETE_RUN_HITS_TO_1_MEDALS) {
			firstHitOn = true;

			if (PlayerController.instance.isDead) Run.Instance.runGoal.hits = 2;
			else if (!SilverAchieved) HandleDecreasedSilverAchieved ();
			
			StartCoroutine ( UpdateGoldPBFillBackwards(0f, 4f) );

			
		} else if (!secondHitOn && Run.Instance.runGoal.hits > RunGoal.COMPLETE_RUN_HITS_TO_1_MEDALS){
			secondHitOn = true;
			
			StartCoroutine ( UpdateSilverPBFillBackwards(0f, 4f) );


		}
			
//		PB_Text_prefix + Run.Instance.runGoal.hits;

	}



	//bool adrenalineTook = false;
	//bool gotHit = false;

//	void updatePB_onDontGetHit(){
//
//		//pBCounterValue.text = Run.Instance.runGoal.playTime + "/" + (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL;
//		//pBCounterValue.text = Run.Instance.runGoal.hits + "";
//		pBCounterValue.text = "";
//
//		if (!adrenalineTook && Run.Instance.runGoal.SheildUsed == true) {
//			adrenalineTook = true;
//			HandleDecreasedSilverAchieved ();
//			StartCoroutine ( UpdateGoldPBFillBackwards(0f, 4f) );
//		} else if (!gotHit && Run.Instance.runGoal.hits > 0){
//			gotHit = true;
//			StartCoroutine ( UpdateSilverPBFillBackwards(0f, 4f) );
//			StartCoroutine ( UpdateGoldPBFillBackwards(0f, 4f) );
//			PlayerController.instance.Damage (10);
//		}
//
//		//gotHit = true;
//		StartCoroutine ( UpdateSilverPBFillBackwards(0f, 4f) );
//		StartCoroutine ( UpdateGoldPBFillBackwards(0f, 4f) );
//		//PlayerController.instance.Damage (10);
//
//		PB_Text.text = PB_Text_prefix + Run.Instance.runGoal.hits;
//
//	}



	void updatePB_onCompleteRunLessXSec(){

//		pBCounterValue.text = Run.Instance.runGoal.playTime + "/" + (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL;

		if (Run.Instance.runGoal.playTime < RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL) {

			StartCoroutine ( UpdateGoldPBFillBackwards(PB_WIDTH - PB_updateStep * Run.Instance.runGoal.playTime, 4f) );
			
		}else if(Run.Instance.runGoal.playTime < RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL){
			if (!SilverAchieved) HandleDecreasedSilverAchieved ();
			StartCoroutine ( UpdateGoldPBFillBackwards(PB_WIDTH - PB_updateStep * Run.Instance.runGoal.playTime, 4f) );
		}


//		PB_Text_prefix + (RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL - Run.Instance.runGoal.playTime);

	}


	void updatePB_onKickTargets(){

//		pBCounterValue.text = Run.Instance.runGoal.spaceShipsDown + "/" + RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL;

		if (Run.Instance.runGoal.spaceShipsDown < RunGoal.KICK_BALL_AT_SPACESHIP_FOR_1_MEDAL) {
			
			StartCoroutine ( UpdateSilverPBFill(PB_updateStep * Run.Instance.runGoal.spaceShipsDown, 4f) );
			
		} else if (Run.Instance.runGoal.spaceShipsDown < RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL) {

			if (!SilverAchieved) HandleSilverAchieved ();
			
			StartCoroutine ( UpdateGoldPBFill(PB_updateStep * Run.Instance.runGoal.spaceShipsDown, 4f) );
			
		} else {
			if(!GoldAchieved) HandleGoldAchieved();
		}
		
//		PB_Text_prefix + Run.Instance.runGoal.spaceShipsDown;



	}

//	void updatePB_onTackleEnemies(){
//
//		pBCounterValue.text = Run.Instance.runGoal.enemiesTackled + "/" + (int)RunGoal.TACKLE_ENEMIES_FOR_2_MEDAL;
//
//		if (Run.Instance.runGoal.enemiesTackled < RunGoal.TACKLE_ENEMIES_FOR_1_MEDAL) {
//			
//			StartCoroutine ( UpdateSilverPBFill(PB_updateStep * Run.Instance.runGoal.enemiesTackled, 4f) );
//			
//		} else if (Run.Instance.runGoal.enemiesTackled < RunGoal.TACKLE_ENEMIES_FOR_2_MEDAL) {
//			
//			
//			
//			if (!SilverAchieved) HandleSilverAchieved ();
//			
//			StartCoroutine ( UpdateGoldPBFill(PB_updateStep * Run.Instance.runGoal.enemiesTackled, 4f) );
//			
//		} else {
//			if(!GoldAchieved) HandleGoldAchieved();
//		}
//
//		PB_Text.text = PB_Text_prefix + Run.Instance.runGoal.enemiesTackled;




//		if(PB_Bar.fillAmount != PB_updateStep * Run.Instance.runGoal.enemiesTackled) StartCoroutine ( UpdatePBFill(PB_updateStep * Run.Instance.runGoal.enemiesTackled, 2f) );
//
//
//
//		if (Run.Instance.runGoal.enemiesTackled >= RunGoal.TACKLE_ENEMIES_FOR_2_MEDAL && CurMedalType == MedalTypes.SILVER) {
//			StartCoroutine (ChangeBPColor (PB_Color_goldAchieved));
//			CurMedalType = MedalTypes.GOLD;
//		} else if (Run.Instance.runGoal.enemiesTackled >= RunGoal.TACKLE_ENEMIES_FOR_1_MEDAL && CurMedalType == MedalTypes.NO) {
//			StartCoroutine( ChangeBPColor(PB_Color_goalAchieved) );
//			CurMedalType = MedalTypes.SILVER;
//		}
//	}



	void HandleSilverAchieved(){
		SilverAchieved = true;

		AudioManager.Instance.OnRankB_Achieved ();
		TMP_VECTOR = PB_Bar_Silver.sizeDelta;
		TMP_VECTOR.x = SeperatorPosition;
		PB_Bar_Silver.sizeDelta = TMP_VECTOR;

		AnimatedText.text = DAO.Language.GetString ("rank-b-achieved");
		CupRunController.Instance.ShowSilverAchieved();
		CupRunController.Instance.BarRankFX ();
	}


	void HandleDecreasedSilverAchieved(){
		SilverAchieved = true;
		
		TMP_VECTOR = PB_Bar_Silver.sizeDelta;
		TMP_VECTOR.x = SeperatorPosition;
		PB_Bar_Silver.sizeDelta = TMP_VECTOR;
		
		AnimatedText.text = DAO.Language.GetString ("rank-b-raised");
		CupRunController.Instance.ShowGoldAchieved();
	}

	void HandleGoldAchieved(){
		GoldAchieved = true;

		AudioManager.Instance.OnRankA_Achieved ();
		TMP_VECTOR = PB_Bar_Gold.sizeDelta;
		TMP_VECTOR.x = PB_WIDTH;
		PB_Bar_Gold.sizeDelta = TMP_VECTOR;

		AnimatedText.text = DAO.Language.GetString ("rank-a-achieved");
		CupRunController.Instance.ShowGoldAchieved();
		CupRunController.Instance.BarRankFX ();
	}




	public GameObject[] whistlesArray;
	public void UpdateWhistles ()
	{
		if (curWhistlesAmount == PlayerController.instance.HitsRemain)
			return;

		curWhistlesAmount = PlayerController.instance.HitsRemain;

		for (int i = 0; i < whistlesArray.Length; i++) {
			if (i < curWhistlesAmount) {
				whistlesArray [i].SetActive (true);
			} else {
				whistlesArray [i].SetActive (false);
			}
		}

		//Whistles.text = "";
		//for (int i = 0; i < curWhistlesAmount; i++) {
		//Whistles.text += "E ";

		//}

	}




}
