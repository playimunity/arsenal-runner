using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System;
using UnityEngine.SceneManagement;
using Amazon;

public class GameManager : MonoBehaviour {
	public static string BUNDLE_ID = "com.playim.soccerRun";
	public static GameManager Instance;

	public enum GameState {PRELOADER, LOCKER_ROOM, CUP_RUN, INFINITY_RUN, TUTORIAL_RUN, TEST_RUN, GAME_OVER, PAUSE};
	public enum GameSubState {NO, LOW_ENERGY, CUPS_SCREEN, RUNS_SCREEN, MENU_TUTORIAL , TUTORIAL_CONNECTED , TUTORIAL_NOT_CONNECTED , TUTORIAL_FIRST_RUN , TUTORIAL_FIRST_RUN_SKIP, PLAYER_PREVIEW};

	public static bool Initialized = false;
	public string[] winnedCups;
	public static bool DEBUG = true;

	public static event Action OnPause;
	public static event Action<bool> OnUnpause;
	public static event Action OnCupTimeUpdate;

	public static bool BenchInPreselectedRunMode = false;
	public static GameState PreselectedGameState;
	WaitForSeconds waitMinute;
	WaitForSeconds slowMoTime;
	public float FTS;

	public static bool Paused = false;
	public bool runPaused = false;

	bool inSlowMo = false;
	//bool connectedToFacebook = false;
	public bool needToShowRateUsPopup = false;
	public bool isPlayscapeLoaded = false;

	WaitForEndOfFrame waitForEndOfFrame;

	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log ("Game Manager | " + msg);
	}


	void Start () {
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("GameManager start");
		Instance = this;
		Initialized = true;
		waitMinute = new WaitForSeconds (60f);
		slowMoTime = new WaitForSeconds (0.75f);
		waitForEndOfFrame = new WaitForEndOfFrame ();
		OnPause += Pause;
		OnUnpause += Unpause;
		//Application.targetFrameRate = 60;
		ReadRegions ();
		ReadWinnedCups ();

		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		FTS = Time.fixedDeltaTime;
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("GameManager end");
	}

	void Update(){

		if (Input.GetKey (KeyCode.D) && Input.GetKeyDown (KeyCode.LeftShift) ) {
			PlayerPrefs.DeleteAll();
			DAO.Instance.LOCAL.BuildDefaultData();
			DAO.Log ("All local data deleted!");
		}
	}

	// ------------------------------------------------------------------------------------------------------------ Public Variables

	public static RunAbstract CurrentRun;
	public static Cup ActiveCup;
	public Cup activeCupInstance;
	public static Cup ActiveCompletedCup;
	public Cup activeCompletedCupInstance;
	public static bool ActiveCupCompleted;
	public string[] completedRunsData;
	public string completedRunData;

	private static GameState _gameState = GameManager.GameState.PRELOADER;
	public static GameState curentGameState{
		get{
			return _gameState;
		}
	}

	private static GameSubState _gameSubState = GameManager.GameSubState.NO;
	public static GameSubState curentSubGameState{
		get{
			return _gameSubState;
		}
	}

	private static HashSet<int> openedRegions;
	public static HashSet<int> OpenedRegions{
		get{return openedRegions;}
	}

	public static void AddRegonToOpened(int[] regions){

		for (int i=0; i<regions.Length; i++) {
			openedRegions.Add ( regions[i] );
		}

		DAO.OpenedRegions = "";
		foreach(int r in openedRegions ){
			DAO.OpenedRegions += r + "|";
		}
	}

	public void ReadRegions(){
		//Debug.Log ("alon__________ GameManager - ReadRegions()");
		string[] reg = DAO.OpenedRegions.Split (DAO.char_separator, StringSplitOptions.RemoveEmptyEntries);
		openedRegions = new HashSet<int> ();

		for (int i=0; i<reg.Length; i++) {
			openedRegions.Add ( int.Parse(reg[i]) );
		}
	}

	// ------------------------------------------------------------------------------------------------------------ Public Methods

	public static GameState getGameStateBySceneIndex(int index){
		if (index == 0) return GameState.PRELOADER;
		//if (index == 1) return GameState.MAIN_MENU;
		if (index == 1) return GameState.LOCKER_ROOM;
		if (index == 2) return GameState.CUP_RUN;
		if (index == 3) return GameState.INFINITY_RUN;
		if (index == 4) return GameState.TUTORIAL_RUN;

		// default
		return GameState.PRELOADER;
	}

	public void ReadWinnedCups(){
		//Debug.Log ("alon__________ GameManager - ReadWinnedCups()");
		winnedCups = DAO.WinnedCups.Split(new char[] {'%'}, System.StringSplitOptions.RemoveEmptyEntries);
		CreateCompletedRunsData ();
	}

	public void WriteWinnedCups(){
		DAO.WinnedCups = "";
		for (int i=0; i < winnedCups.Length; i++) {
			DAO.WinnedCups += winnedCups[i] +"%";
		}
	}

	public void CreateCompletedRunsData(){
		if (DAO.CompletedRunsData == "")
		{
			DAO.CompletedRunsData = "*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0*0";
		}
		//Debug.Log ("alon____________ GameManager - CreateCompletedRunsData() - DAO.CompletedRunsData: " + DAO.CompletedRunsData);
		completedRunsData = DAO.CompletedRunsData.Split (new char[] { '*' }, System.StringSplitOptions.RemoveEmptyEntries); // create an arrey of all competed runs data
		//Debug.Log ("alon___________ GameManager - CreateCompletedRunsData() - completedRunsData count: " + completedRunsData.Length);
	}

	public void UpdateCupRunsData(int id , string runData){
		//Debug.Log ("alon___________ GameManager - UpdateCupRunsData() - DAO.CompletedRunsData before adding: " + DAO.CompletedRunsData);
		completedRunsData[id] = runData;
		DAO.CompletedRunsData = "";
		for (int i = 0 ; i < completedRunsData.Length ; i++) {
			DAO.CompletedRunsData += "*" + completedRunsData [i];
		}
		//Debug.Log ("alon___________ GameManager - UpdateCupRunsData() - DAO.CompletedRunsData after adding: " + DAO.CompletedRunsData);

	//	GameManager.Instance.completedRunData = "";

		DAO.Instance.LOCAL.SaveAll();
	}
	public int myIndex = 0;
	public static void SwitchState(GameState state){
		if (state == GameState.LOCKER_ROOM) {
			GameManager.Instance.myIndex++;
		//	Debug.Log ("--------------------------------  alon______ SwitchState to LOCKER_ROOM - TRRRRRRUUUEEEE!!!!" + GameManager.Instance.myIndex);
		}
		_SwitchState (state);
	}

	public static void SwitchState(GameState state, GameSubState substate){
		_SwitchSubState (substate);
		_SwitchState (state);
	}

	public static void restetGameSubState(){
		_gameSubState = GameManager.GameSubState.NO;
	}


	static void _SwitchState(GameState state){
	
		switch (state) {
			case GameState.CUP_RUN : {
				UIManager.SwitchStae(state);
				break;
			}
			case GameState.GAME_OVER : {
				UIManager.SwitchStae(state);
				break;
			}
			case GameState.INFINITY_RUN : {
				UIManager.SwitchStae(state);
				break;
			}

			case GameState.TUTORIAL_RUN : {
				UIManager.SwitchStae(state);
				break;
			}

			case GameState.TEST_RUN : {

				UIManager.SwitchStae(state);
				break;
			}
				
//			case GameState.MAIN_MENU : {
//				UIManager.SwitchStae(state);
//				break;
//			}
				
			case GameState.LOCKER_ROOM : {
				UIManager.SwitchStae(state);
				break;
			}

			case GameState.PRELOADER : {
				UIManager.SwitchStae(state);
				break;
			}
		}
		
		_gameState = state;
	}

	static void _SwitchSubState(GameSubState sub){
		_gameSubState = sub;
	}

	public IEnumerator LoadUIPT2()
	{
		//var uiPt2 = Instantiate(Resources.Load("CrossSceneUI_pt2"));
		SceneTransitionContainer.Instance.loading.text = DAO.Language.GetString ("loading");
//		yield return new WaitUntil(()=>Instantiate(Resources.Load("CrossScene UI")));
//		Preloader.Instance.FillLoadingBar (6,"CrossSceneUI");
		CrossSceneUIHandler.Instance.SetStrings ();

		GameObject prefabsManager = new GameObject();
		yield return new WaitUntil(()=>prefabsManager = Instantiate(Resources.Load("Prefabs Manager")) as GameObject);
		Preloader.Instance.FillLoadingBar (7,"PrefabsManager");
		prefabsManager.transform.SetParent (GameObject.Find ("Managers").transform);
		//Debug.Log("After UI");


//		try {
//		StartCoroutine(LoadPlayscape());
//			
//		} catch (Exception ex) {
//			Debug.Log("*!*!*!**!*!*!**!*!*!*!*!* PLAYSCAPE FAILED !*!*!**!*!***!*!**!**!**!*!**!*!*!*!*");
//			UnitedAnalytics.LogEvent("PLAYSCAPE","Failed to load playscape - "+ex.Message);
//			isPlayscapeLoaded = false;
//			if (UnityInitializer.Instance!=null)
//				UnityInitializer.Instance.addToMonitorLog("Failed to Load Playscape:");
//		}
//		GameObject BI = new GameObject();
//		yield return new WaitUntil(()=>BI = Instantiate(Resources.Load("BI")) as GameObject);
//		Preloader.Instance.FillLoadingBar (7,"BI");
//		BI.transform.SetParent (GameObject.Find ("Managers").transform);
//		isPlayscapeLoaded = true;

		CompleteOnallDataLoaded();

	}


	public IEnumerator LoadPlayscape()
	{
		GameObject Playscape = new GameObject();
			yield return new WaitUntil(()=>Playscape = Instantiate(Resources.Load("PlayScape")) as GameObject);
			//Playscape = Instantiate(Resources.Load("PlayScape")) as GameObject;
			Preloader.Instance.FillLoadingBar (8,"PlayScape");
			Playscape.transform.SetParent (GameObject.Find ("Managers").transform);
			isPlayscapeLoaded = true;
	}


	public void CompleteOnallDataLoaded()
	{
		CrossSceneUIHandler.Instance.ResetStagingMarker ();
		CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

		if (Tutorial.IsNeedToShowTutorialRun) {
			PrefabManager.instanse.ChoosePlayer (0);
			if (DAO.TotalCoinsCollected == 0) {
				CrossSceneUIHandler.Instance.coinsCounterAmount.text = "0";
			} else {
				CrossSceneUIHandler.Instance.coinsCounterAmount.text = DAO.TotalCoinsCollected.ToString("##,#");
			}
			GameManager.SwitchState (GameManager.GameState.TUTORIAL_RUN);
		}
		else{

			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
		}


		if(UIManager.asyncLeveLoad != null) UIManager.asyncLeveLoad.allowSceneActivation = true;
		CrossSceneUIHandler.Instance.SetStrings ();

		//GetFirstPlayerName ();
	}


//	public static void OnAllDataLoaded(){

//		CrossSceneUIHandler.Instance.ResetStagingMarker ();
//
//		CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
//
////		if (Tutorial.IsNeedToShowMenuTutorial) {// Check if there is a tutorial
////			GameManager.SwitchState (GameManager.GameState.MAIN_MENU, GameSubState.MENU_TUTORIAL);
////		} else if (Tutorial.IsNeedToShowTutorialRun) {
////			if (FBModule.Instance.UserProfileLoaded) {
////				GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameSubState.TUTORIAL_CONNECTED);
////			} else {
////				GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameSubState.TUTORIAL_NOT_CONNECTED);
////			}
////		} else if (Tutorial.IsNeedToShowRunLockerRoomTutorial) {
////			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameSubState.TUTORIAL_FIRST_RUN);
////		} else if (Tutorial.IsNeedToShowRunLockerRoomSkipTutorial){
////			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameSubState.TUTORIAL_FIRST_RUN_SKIP);
////		}
//		if (Tutorial.IsNeedToShowTutorialRun) {
//			PrefabManager.instanse.ChoosePlayer (23);
//			GameManager.SwitchState (GameManager.GameState.TUTORIAL_RUN);
//		}
//		else{
//			GameManager.SwitchState (GameManager.GameState.MAIN_MENU);
//		}
//
//		if(UIManager.asyncLeveLoad != null) UIManager.asyncLeveLoad.allowSceneActivation = true;


//	}
		
	public static void RequestPause(){
		if (OnPause != null) {
			//GameManager.Instance.runPaused = true;
			OnPause ();
		}
	}

	public static void RequestUnpause(bool imidiatelly = false){
		if (OnUnpause != null) OnUnpause (imidiatelly);
	}
		

	void Pause(){
		//runPaused = true;
		AudioManager.Instance.SFX_Run.mute = true;
		AudioManager.Instance.SFX_RunGodMode.mute = true;

		StartCoroutine (ScaleTime(1f, 0f, 0.5f));
	}

	void Unpause(bool imidiatelly){
		AudioManager.Instance.SFX_Run.mute = false;
		AudioManager.Instance.SFX_RunGodMode.mute = false;

		if (!imidiatelly) {
			StartCoroutine (ScaleTime (0f, 1f, 1f));
		}else {
			Time.timeScale = 1f;
		}
	}

	float originalDeltaTime = 0.02f;
	float fixedDeltaTime;
	float originalTimeScale = 1.0f;
	float targetTimeScale = 0.02f;


	public void SlowMo_Start(bool fast = false){
		fixedDeltaTime = originalDeltaTime * targetTimeScale;
		inSlowMo = true;
		Time.fixedDeltaTime = fixedDeltaTime; // 0.02;
		if (fast) Time.timeScale = 0.15f;
		else StartCoroutine (ScaleTime(Time.timeScale, targetTimeScale, 0.03f));	
		StartCoroutine ( StopSlowMo() );
	}

	public void SlowMo_End(){
		if (!inSlowMo) return;

		inSlowMo = false;
		Time.fixedDeltaTime = FTS;
		StartCoroutine (ScaleTime(Time.timeScale, 1f, 1f));	
	}

	IEnumerator StopSlowMo(){
		yield return slowMoTime;
		SlowMo_End ();
	}

	IEnumerator ScaleTime(float start, float end, float time)
	{
		float lastTime = Time.realtimeSinceStartup;
		float timer = 0f;

		if( end > 0f ) GameManager.Paused = false;

		while (timer < time)
		{
			Time.timeScale = Mathf.Lerp (start, end, timer / time);
			timer += (Time.realtimeSinceStartup - lastTime);
			lastTime = Time.realtimeSinceStartup;
			yield return null;
		}
		
		Time.timeScale = end;
		if (end <= 0f) {
			GameManager.Paused = true;
			runPaused = true;
		} else {
			runPaused = false;
		}
			
	}
	//public bool isFreeGifts = false;
	void OnLevelWasLoaded(int level) {
		//System.GC.Collect();
        if (!HeyzapWrapper.IsAvailable)
        {
            HeyzapWrapper.Fetch();
        }

		if (curentGameState == GameState.LOCKER_ROOM) {
			CheckActiveCup();

//			isFreeGifts = HeyzapWrapper.AreAdsAllowedForUser (HeyzapWrapper.AdType.FreeGift);
//			MainScreenUiManager.instance.claimFreeGiftBTN.gameObject.SetActive (isFreeGifts);
//			MainScreenUiManager.instance.FreeGiftBtnAnimToggle (isFreeGifts);
		}
	}

	public void CheckActiveCup(){
//		CreateCompletedRunsData ();

//		if (ActiveCup != null && ActiveCup.exist) {
//		}
		if (DAO.ActiveRunID != -1) {
			//Debug.Log ("alon___________ GameManager - CheckActiveCup() - DAO.ActiveRunID != -1");
			ActiveCup = new Cup (DAO.getCupByIndex (DAO.ActiveRunID), DAO.ActiveRunID);
			activeCupInstance = ActiveCup;
		}

	}

	public void NewCupStarted(){
		
		if(OnCupTimeUpdate != null) OnCupTimeUpdate();
	}



//	public void HandleCupLoose(){
//
//		UnitedAnalytics.LogEvent ("Quest time over", "popup shown", UserData.Instance.userType.ToString(), DAO.ActiveRunData.Split (new char[1]{ '%' }, System.StringSplitOptions.RemoveEmptyEntries).Length);
//
//		ActiveCup.Loose ();
//		CrossSceneUIHandler.Instance.showCupLooseScreen ();
//
//		ActiveCup.exist = false;
//		DAO.ActiveRunData = "";
//		DAO.ActiveRunID = -1;
//		DAO.Instance.LOCAL.SaveAll();
//
//		if(curentGameState == GameState.LOCKER_ROOM){
//			CupsManager.Instance.ResetCupsUIElements ();
//		}
//
//		ActiveCup = null;
//	}



	public static bool TutorialPaused = false;
	public static bool TutorialPrePaused = false;

	public void TutorialPointPause(){
		TutorialPrePaused = true;
		PlayerController.instance.Pause ();
	}

	public void TutorialPointUnpause(){
		TutorialPrePaused = false;
		PlayerController.instance.Unpause ();
	}

	public void HandleRankScoreChange(){
		int newScore = Utils.CalculateRankScore ();

		if (DAO.RankScore < newScore) {
			DAO.RankScore = newScore;
			//NativeSocial._.PostScore (DAO.RankScore);

			Log ("New Score Achieved: " + DAO.RankScore);
		}
	}

	public Canvas crossScCanvas;
	public void PromtAppNoInternet(){
		Log ("PromtAppNoInternet init");
		//Debug.Log("alon___________ PromtAppNoInternet() - start");

		crossScCanvas.sortingOrder = 110;

		GenericUIMessage gm;
		//Debug.Log("alon___________ GameManager - PromtAppNoInternet() - GenericUIMessage gm");
		if (GameManager.curentGameState == GameState.PRELOADER) {
			
			 gm = new GenericUIMessage("No internet connection", "Unable to download content, Please check your internet connection and try again.");
			//Debug.Log("alon___________ GameManager - PromtAppNoInternet() - if (GameManager.curentGameState == GameState.PRELOADER)");
		} else {
			
			gm = new GenericUIMessage(DAO.Language.GetString("no-internet"), DAO.Language.GetString("no-download"));
			gm.PositiveBTNAction = () => {
				CrossSceneUIHandler.Instance.hideGenericMessage();	

			};
			gm.PositiveButtonText = DAO.Language.GetString ("back");
		}
		CrossSceneUIHandler.Instance.showGennericMessage(gm);
	}
		


	public string firstPlayerName;

	public void GetFirstPlayerName(){
		//Debug.Log ("alon___________ GameManager - first player name: " + firstPlayerName);
		firstPlayerName = PrefabManager.instanse.PlayerStructures[ int.Parse(DAO.PurchasedPlayers.Split('|')[0]) ].PlayerName;
		//Debug.Log ("alon___________ GameManager - first player name: " + firstPlayerName);
	}
		


}
