﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StringProvider_InfinityRun : MonoBehaviour {

	public Text GoBtn;
	public Text LevelTxt;
	public Text LevelUpTxt;
	public Text PauseTitle;
	public Text PracticePause;
	public Text YouHaveAchieved;
	public Text YouMaxFitnessIs;
	public Text KeepRunning;
	public Text QuitRun;
	public Text PSGoToLockerRoom;
	public Text GOSGoToLockerRoom;

	public Text TrainOver;
	//public Text LoosePracticeStatus;
	//public Text LooseYouHaveReached;
	//public Text LooseYourMaxFitnessIs;
	public Text LockerRoomBtn;
	public Text RetryBtn;
	public Text ShareBtn;
	public Text highScore;

	public Text BigHighScoreTxt;
	public Text BigLevelUpTxt;

	public Text saveMeHeaderTxt;

	public Text SaveMeCostTxt;
	public Text saveMeBtnTxt;

	public Text SaveMeAdCostTxt;
	public Text saveMeAdBtnTxt;

//	void Awake () {
//
//		GoBtn.text = DAO.Language.GetString ("go");
//		LevelTxt.text = DAO.Language.GetString ("level");
//		LevelUpTxt.text = DAO.Language.GetString ("level-up");
//		PauseTitle.text = DAO.Language.GetString ("ir-pause-title");
//		PracticePause.text = DAO.Language.GetString ("train-paused");
//		YouHaveAchieved.text = DAO.Language.GetString ("you-have-reached");
//		YouMaxFitnessIs.text = DAO.Language.GetString ("you-max-fitness-now");
//		KeepRunning.text = DAO.Language.GetString ("keep-running");
//		QuitRun.text = DAO.Language.GetString ("quit-run");
//		PSGoToLockerRoom.text = DAO.Language.GetString ("stop-run");
//		GOSGoToLockerRoom.text = DAO.Language.GetString ("stop-run");
//
//		TrainOver.text = DAO.Language.GetString ("train-over");
////		LoosePracticeStatus.text = DAO.Language.GetString ("practice-status");
////		LooseYouHaveReached.text = DAO.Language.GetString ("you-have-reached");
////		LooseYourMaxFitnessIs.text = DAO.Language.GetString ("you-max-fitness-now");
//		//LockerRoomBtn.text = DAO.Language.GetString ("locker-room");
//		RetryBtn.text = DAO.Language.GetString ("retry");
//		ShareBtn.text = DAO.Language.GetString ("share");
//		highScore.text = DAO.Language.GetString ("total-distance");
//
//		BigHighScoreTxt.text = DAO.Language.GetString ("new-highscore");
//		BigLevelUpTxt.text = DAO.Language.GetString ("level-up");
//
//		saveMeHeaderTxt.text = DAO.Language.GetString ("save-me-header");
//
//		SaveMeCostTxt.text = DAO.getSaveMePricesByIndex (0, DAO.SaveMePriceTag.ENDLESS).ToString ();
//		saveMeBtnTxt.text = DAO.Language.GetString ("save-me");
//
//		SaveMeAdCostTxt.text = DAO.getSaveMePricesByIndex (0, DAO.SaveMePriceTag.ENDLESS).ToString ();
//		saveMeAdBtnTxt.text = DAO.Language.GetString ("free");
//	}



}
