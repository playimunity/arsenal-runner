﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Facebook.Unity;

public class QuestFriendsHandler : MonoBehaviour {

	List<FBFriend> friends;
	int cup_id;

	public GameObject BG_1_Friend;
	public GameObject BG_2_Friends;
    public GameObject BG_3_Friends;
	//public GameObject BG_MoreThan2;


	public RawImage friend1_image;
	public RawImage friend2_image;
	public RawImage friend3_image;

	public void OnDisable(){
		CancelInvoke ("ReSet");
	}

	// ---

	public void InitFriends(){

		friends = new List<FBFriend> ();

		foreach( FBFriend ff in FBModule.Instance.FriendsWithGame ){
			if (ff.current_quest == cup_id) friends.Add (ff);
		}

	}


	public void Set(int cupid){
		cup_id = cupid;

		BG_1_Friend.SetActive(false);
		BG_2_Friends.SetActive(false);
        BG_3_Friends.SetActive(false);
		//BG_MoreThan2.SetActive(false);

		if (!FB.IsLoggedIn){
			return;
		}
			

		InitFriends ();


		if (friends.Count == 1) Set1Friend ();
		if (friends.Count == 2) Set2Friends ();
        if (friends.Count >= 3) Set3Friends ();
		//if (friends.Count > 2) SetMoreThan2Friends();

		//InvokeRepeating ("ReSet", 3f, 3f);

	}

	public void ReSet(){
		Set (cup_id);
	}

	void Set1Friend(){
		BG_1_Friend.SetActive (true);


		friend1_image.texture = friends [0].photo;
	}

	void Set2Friends(){
        BG_1_Friend.SetActive (true);
		BG_2_Friends.SetActive (true);


		friend1_image.texture = friends [0].photo;
		friend2_image.texture = friends [1].photo;

	}
    void Set3Friends(){
        BG_1_Friend.SetActive (true);
        BG_2_Friends.SetActive (true);
        BG_3_Friends.SetActive (true);


        friend1_image.texture = friends [0].photo;
        friend2_image.texture = friends [1].photo;
        friend3_image.texture = friends [2].photo;
    }

	public void OnClicked(){

		MainScreenUiManager.instance.ShowQuestFriendsList (friends);

	}
}
