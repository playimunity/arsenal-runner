﻿using UnityEngine;
using System.Collections;

public class SpaceShipObstacle : Obstacle {

	bool colided2 = false;
	public GameObject externalModel;
	public GameObject targetCollider;
	
	void OnCollisionEnter(Collision col) {

		if (col.gameObject.tag == "Player" && !colided2) {
			colided2 = true;

			if (!PlayerController.instance.speedBoostOn) {
				GetComponentInChildren<BoxCollider> ().enabled = false;
				PlayerController.instance.Damage (1);
				AudioManager.Instance.OnSoftHit ();
				ParticleCloudeGenerator.instance.ParticleWhistle (transform.position, transform.eulerAngles);
				//Debug.Log ("alon__________ obstacle name: " + gameObject.name);
				externalModel.SetActive (false);//Destroy (gameObject);
				targetCollider.SetActive (false);
				gameObject.SetActive (false);
			} else {
				ParticleCloudeGenerator.instance.ParticleBoom(transform.position, transform.eulerAngles);
				//PlayerController.instance.GodSphereFX ();
				//RunManager.instance.StopSheild ();
				PlayerController.instance.sheildModeDamage = true;
				AudioManager.Instance.OnHitGod ();
				externalModel.SetActive (false);//Destroy (gameObject);
				targetCollider.SetActive (false);
				gameObject.SetActive (false);
			}
	
}
}
}