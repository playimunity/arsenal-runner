﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HeaderManager : MonoBehaviour
{
    //quest run properties
    public RectTransform silverFill;
    public RectTransform goldFill;
    public RectTransform seperator;
    public RectTransform silverLine;
    public RectTransform goldLine;
    public RectTransform fullBarWidth;
    public float SilverLine_Gap;
    public float Seperator_Gap;
    //end
    public Image levelValueIMG;
    public Image levelFill;

    public List<Sprite> levelImages;
    private GameManager.GameState state;

    public GameManager.GameState State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;
            ArrangeHeader();
        }
    }

    public GameObject layout;
    public GameObject element;
    //private List<Transform> layouts;
    private Dictionary<string,Transform> elements;
    public static HeaderManager instance;

    void Start()
    {
        elements = new Dictionary<string,Transform>();
        foreach (Transform child in element.transform)
        {
            elements.Add(child.name, child);
        }
        instance = this;
    }
	
   

    public void ArrangeHeader()
    {
        Transform currLayout = layout.transform.FindChild(state.ToString());
        if (currLayout)
        {
            foreach (Transform child in currLayout)
            {
                if (elements.ContainsKey(child.name))
                {
                    elements[child.name].GetComponent<RectTransform>().position = child.GetComponent<RectTransform>().position;
                    elements[child.name].gameObject.SetActive(child.gameObject.activeSelf);
                }
            }
        }
        else
        {
            print("ERROR| NO LAYOUT FOUND!  " + state.ToString());
        }
    }


	//public Image energyTimerBar; 
//	public Text energyValueTxt;
//	public void UpdateEnergy(int energy, int maxEnergy){
//		//energyTimerBar.fillAmount = timePast;
//		energyValueTxt.text = "" + energy + "/" + maxEnergy;
//	}


    #region Events

    public void PauseButtonClicked()
    {
        switch (GameManager.curentGameState)
        {
            case GameManager.GameState.CUP_RUN:
                CupRunController.Instance.Pause();
                break;
            case GameManager.GameState.INFINITY_RUN:
                PracticeRunController.Instance.Pause();
                break;
            case GameManager.GameState.TUTORIAL_RUN:
                CupRunController.Instance.Pause();
                break;
            default:
                break;
        }
    }
    public void SettingsButtonClicked()
    {
		MainScreenUiManager.instance.PerksScrnAnim (false);
		MainScreenUiManager.instance.RunsScrnAnimToggle (false);
		MainScreenUiManager.instance.CupScrnAnimToggle (false);
		MainScreenUiManager.instance.CardScrnToggle (false);
		MainScreenUiManager.instance.MidekKitsScreenToggle (false);
        MainScreenUiManager.instance.OnSettingClicked();
    }
    public void RecruitPlayerClicked()
    {
		MainScreenUiManager.instance.PerksScrnAnim (false);
		MainScreenUiManager.instance.RunsScrnAnimToggle (false);
		MainScreenUiManager.instance.CupScrnAnimToggle (false);
		MainScreenUiManager.instance.CardScrnToggle (false);
		MainScreenUiManager.instance.MidekKitsScreenToggle (false);
        PlayerChooseManager.instance.SwitchToPreviewMode();
    }
	public void EnergyButtonClicked()
	{
		MainScreenUiManager.instance.PerksScrnAnim (false);
		MainScreenUiManager.instance.RunsScrnAnimToggle (false);
		MainScreenUiManager.instance.CupScrnAnimToggle (false);
		MainScreenUiManager.instance.CardScrnToggle (false);
		MainScreenUiManager.instance.MidekKitsScreenToggle (true);
	}
    #endregion


}
