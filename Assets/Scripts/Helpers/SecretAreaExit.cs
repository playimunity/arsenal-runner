﻿using UnityEngine;
using System.Collections;

public class SecretAreaExit : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if (col.transform.gameObject.tag == "Player") {
			//PlayerController.instance.sectionOf3 = false;
			PlayerController.instance.secretArea = false;
			StartCoroutine (DeactivateSecretArea());
			if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
				CupRunController.Instance.SecretPanelToggle (false);
				CupRunController.Instance.PwrUpBtnsToggle (true);
			}
		}
	}

	IEnumerator DeactivateSecretArea(){
		yield return new WaitForSeconds (1.5f);
		Run.Instance.secterArea.SetActive (false);
	}
}
