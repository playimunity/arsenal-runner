﻿using UnityEngine;
using System.Collections;

public class GiftCardPrizeIcon : MonoBehaviour {

	public GiftCard.Prizes PrizeType;
	public bool Winner;


	RectTransform parentTr;
	Vector2 cardCenter;
	RectTransform MyTr;
	WaitForEndOfFrame WFEOF;


	void Start(){
		WFEOF = new WaitForEndOfFrame ();
		MyTr = ((RectTransform)transform);
		cardCenter = new Vector2 ( 376f, -340f );
		//cardCenter = new Vector2 ( Screen.width/2, Screen.height/1.5f );
	}

	public void Animate(){
		
		StartCoroutine (Anim ());
	}


	IEnumerator Anim(){


		Vector2 max, min;
		max = min = MyTr.localScale;
		max = max * 1.5f;


		//--

		while(MyTr.localScale.x < max.x - 0.05f){
			MyTr.localScale = Vector2.Lerp( MyTr.localScale, max, 7.5f * Time.deltaTime);
			yield return null;
		}

		//--

		while(MyTr.localScale.x > min.x + 0.01f ){
			MyTr.localScale = Vector3.Lerp( MyTr.localScale, min, 7.5f * Time.deltaTime);
			yield return null;
		}	

		//--

		while( Vector2.Distance( MyTr.anchoredPosition, cardCenter ) > 0.1f ){

			MyTr.anchoredPosition = Vector2.Lerp (MyTr.anchoredPosition, cardCenter, 3f * Time.deltaTime);

			yield return WFEOF;
		}

		// ---


	}


}
