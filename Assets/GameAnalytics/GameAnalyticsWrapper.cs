﻿using UnityEngine;
using System.Collections;
using GameAnalyticsSDK;
using OnePF;
using System;

public class GameAnalyticsWrapper
{
	//Resources Item Types
	public const string IAP = "IAP";
	public const string REWARD = "Reward";
	public const string BUY_PLAYER = "BuyPlayer";
	public const string BUY_UPGREADE = "BuyUpgrade";
	public const string BUY_ENERGY = "BuyEnergy";
	public const string PAY_ENERGY = "PayEnergy";
	public const string REWARDED_VIDEO = "RewardedVideo";
	public const string GIFTCARD = "Giftcard";
	public const string SAVE_ME = "SaveMe";
	public const string FINISHED_RUN = "FinishedRun";

	//Resources Currencies
	public const string CURR_COINS = "Coins";
	public const string CURR_ENERGY = "Energy";
	public const string CURR_PLAYERS = "Players";


	public  enum progerssionTypes
	{
		Complete,
		Fail,
		Start
	}


	public static void BusinessEvent (Purchase p, IAPProduct iapp, string itemType, string cartType)
	{
        string currency = "";
        #if UNITY_IOS
                currency= iapp.SKUDetails.CurrencyCode;
        #elif UNITY_ANDROID
        currency= iapp.SKUDetails.CurrencyString;
        #endif
		int priceInCents = (int)(float.Parse (iapp.PriceValue) * 100);
		#if UNITY_ANDROID
        GameAnalytics.NewBusinessEventGooglePlay (currency, priceInCents, itemType, p.Sku, cartType, p.OriginalJson, p.Signature);
		#elif UNITY_IOS
		GameAnalytics.NewBusinessEventIOS (currency, priceInCents, itemType, p.Sku, cartType, p.Receipt);
		#endif
	}

	public static void ResourceEvent (bool isAdd, string inGameCurrency, float amount, string itemType, string itemID)
	{
		GAResourceFlowType flowType = isAdd ? GAResourceFlowType.Source : GAResourceFlowType.Sink;
		GameAnalytics.NewResourceEvent (flowType, inGameCurrency, amount, itemType, itemID);
	}

	public static void ProgressionEvent (progerssionTypes progressionStatus, string progression01, string progression02 = null, string progression03 = null, int score = 0)
	{
		GAProgressionStatus progStat = (GAProgressionStatus)Enum.Parse (typeof(GAProgressionStatus), progressionStatus.ToString());
		GameAnalytics.NewProgressionEvent (progStat, progression01, progression02, progression03, score);
	}

	public static void DesignEvent (FLOW_STEP eventArgs)
	{
		GameAnalytics.NewDesignEvent ("FTUE:"+eventArgs.ToString()+":complete");
	}

	public static void BuyNewPlayer(string playerName, int price)
	{
		GameAnalyticsWrapper.ResourceEvent (true, GameAnalyticsWrapper.CURR_PLAYERS, 1, GameAnalyticsWrapper.BUY_PLAYER, playerName);
		GameAnalyticsWrapper.ResourceEvent (true, GameAnalyticsWrapper.CURR_ENERGY, 30, GameAnalyticsWrapper.BUY_ENERGY, "NewPlayer");
		GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, price, GameAnalyticsWrapper.BUY_PLAYER, playerName);
	}
	public static void BuyNewPlayer(string playerName)
	{
		GameAnalyticsWrapper.ResourceEvent (true, GameAnalyticsWrapper.CURR_PLAYERS, 1, GameAnalyticsWrapper.BUY_PLAYER, playerName);
		GameAnalyticsWrapper.ResourceEvent (true, GameAnalyticsWrapper.CURR_ENERGY, 30, GameAnalyticsWrapper.BUY_ENERGY, "NewPlayer");
		//GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, price, GameAnalyticsWrapper.BUY_PLAYER, playerName);
	}
	public static void PowerUpCollectedOnTutorial()
	{
		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN)
		{
			DesignEvent (FLOW_STEP.TUTORIAL_QUIT);
		}
	}
	public enum FLOW_STEP { 
		LOADING,
		GPG_SUCCESS,
		TUTORIAL_STARTED,
		TUTORIAL_QUIT,
		TUTORIAL_COMPLETED,
		POWERUP_PICKED_IN_TUTORIAL,
		COLLECTED_COINS,
		PRESS_BUY_PLAYER,
		HOME_BUTTON_CLICKED,
		QUEST_RUN_START,
		QUIT_RUN_FROM_PAUSE,
		FIRST_QUEST_RUN_WON,
		FIRST_QUEST_RUN_LOST
	};
	
}
