﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using RTLService;

public class PlayerToBuyInfoUIScreen : MonoBehaviour {
	public static PlayerToBuyInfoUIScreen Instance;

	public Image Photo;
	public Text PlayerName;
	public Text PerkIcon;
	public Text PerkName;
	public Text PerkDescription;
	public Text Prize;
	public PlayerStructure player;


	public GameObject IAPPrice;
	public Text IAPPriceTXT;

	void Awake(){
		Instance = this;
	}

	public void SetViewFromChoosenPlayer(){
		player = PrefabManager.instanse.chosenPlayer;

		Photo.sprite  = player.PlayerPhoto;
		PlayerName.text = player.PlayerName;

//
//		if (player.isLedgend) {
//			IAPPrice.SetActive (true);
//			if (player.PlayerID == 12) {		// Puyol
//				IAPPriceTXT.text = IAP.Instance.Product_LEGEND_PUYOL.GetPriceLabel();
//			}else if(player.PlayerID == 19){	// Ronaldinho
//				IAPPriceTXT.text = IAP.Instance.Product_LEGEND_RONALDINHO.GetPriceLabel();
//			}else if (player.PlayerID == 15) {	// Stoichkov
//				IAPPriceTXT.text = IAP.Instance.Product_LEGEND_STOICHKOV.GetPriceLabel ();
//			}else if (player.PlayerID == 20) {	// Rivaldo
//				IAPPriceTXT.text = IAP.Instance.Product_LEGEND_RIVALDO.GetPriceLabel ();
//			}else if (player.PlayerID == 17) {	// Koeman
//				IAPPriceTXT.text = IAP.Instance.Product_LEGEND_KOEMAN.GetPriceLabel ();
//			}else if (player.PlayerID == 14) {	// Pep
//				IAPPriceTXT.text = IAP.Instance.Product_LEGEND_PEP.GetPriceLabel ();
//			}
//
//		} else {
		IAPPrice.SetActive (false);
		if (player.PlayerNumber == 0)Prize.text = DAO.GetCoachPrice ().ToString ();
		else if (PlayerChooseManager.instance.CurrentPlayerPrice == 0)Prize.text = DAO.Language.GetString ("free");
		else Prize.text = "X " + PlayerChooseManager.instance.CurrentPlayerPrice;

//		}


		PerkIcon.text = Perk.GetPerkIconByPerkType( player.DefaultPerk );
		PerkName.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + Perk.GetPerkIDByPerkType( player.DefaultPerk )]["name"]);
		PerkDescription.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + Perk.GetPerkIDByPerkType( player.DefaultPerk )]["desc_1"]);
		
	}

}
