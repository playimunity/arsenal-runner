﻿using UnityEngine;
using System.Collections;

public class TurnDriver : MonoBehaviour {

	//GameObject player;
	GameObject playerParent;
	Animator driverAnim;
	//float playerSpeed;
	//Rigidbody playerRigidbody;
	public bool canTurn = true;
	Vector3 playerRotation;
	float playerRotationY;
	public bool turnLeft;
	//Vector3 forward;
	//int playerParentOffset;
	//Vector3 playerParentPosition;
	float runningSpeed = DAO.Settings.PlayerRunSpeed;
	float godmodeAdditionalSpeed = DAO.Settings.PlayerGodModeAdditionalSpeed;
	float driverSpeed;
	float driverSpeedGodMode;


	void Start () {
		playerParent = GameObject.FindGameObjectWithTag ("PlayerParent");
		//player = GameObject.FindGameObjectWithTag ("Player");
		//playerRigidbody = player.GetComponent<Rigidbody> ();
		driverAnim = GetComponent<Animator> ();
		runningSpeed = PlayerController.instance.runningSpeed;
		driverSpeed  = runningSpeed / 10;
		driverSpeedGodMode = runningSpeed * godmodeAdditionalSpeed / 10;
		driverAnim.speed = driverSpeed;

		//godmodeAdditionalSpeed = DAO.Settings.PlayerGodModeAdditionalSpeed;
		//godmodeAdditionalSpeed = 2;

	}

	float tempAnimSpeed;
	



	void OnTriggerEnter(Collider other) {
		if (canTurn && !PlayerController.instance.isDead) {
			if (other.gameObject.tag == "Player") {
				canTurn = false;

				playerRotation = playerParent.transform.eulerAngles;

				//CameraLookAt.instance.turning = true;
				PlayerController.instance.turning = true;

				playerParent.transform.parent = transform;

				if (turnLeft){
					playerRotation.y = playerRotation.y - 90;
					//CameraFollow.instance.CrossTurn(1);
					//CameraLookAt.instance.turnLeft = true;
				}
				else{
					playerRotation.y = playerRotation.y + 90;
					//CameraFollow.instance.CrossTurn(-1);
					//CameraLookAt.instance.turnLeft = false;
				}
					
				if(PlayerController.instance.speedBoostOn){
					driverAnim.speed = driverSpeedGodMode ;
				}else{
					driverAnim.speed = driverSpeed ;
				}

				//driverAnim.SetTrigger ("DriverTrigger");
				driverAnim.Play ("TurnDriver");

				StartCoroutine(DriverReset());
			}
		}
	}

	public void OnTurnEnd(){

		playerParent.transform.parent = null;
		playerParent.transform.eulerAngles = playerRotation;
		PlayerController.instance.FixedPlayerXPosition ();
		PlayerController.instance.turning = false;
		//CameraLookAt.instance.turning = false;
		//CameraFollow.instance.CrossTurnEnd ();

	}

	static WaitForSeconds _wfs;
	static WaitForSeconds wfs{
		get{
			if (_wfs == null)
				_wfs = new WaitForSeconds (4f);

			return _wfs;
		}
	}

	IEnumerator DriverReset(){

		//playerParent.transform.right = Mathf.Round (playerParent.transform.right);
		yield return wfs;
		canTurn = true;
	}
}
