﻿using UnityEngine;
using System.Collections;

public class Sedan : Obstacle {



	//public GameObject carLight1;
	//public GameObject carLight2;
	bool flickering = false;

	Transform PlayerTransform;

	void Start(){
		PlayerTransform = player.transform;
	}

	void Update () {
		if (move == Obstacle.MoveTypes.MOVE && moveTriggered) {
			transform.Translate (Vector3.back * moveSpeed * Time.deltaTime);
			if (!flickering && Vector3.Distance (transform.position, PlayerTransform.position) <= distanseToHorn) {
				flickering = true;
				StartCoroutine (Flicker ());
				PlayHornSFX ();
			}
		} else {
			flickering = false;
		}
	}

	void OnTriggerEnter(Collider col){

		if (col.gameObject.tag == "Player") {
			moveTriggered = true;
		}
			
	}



	IEnumerator Flicker(){
		carLight1.SetActive (true);
		carLight2.SetActive (true);
		yield return new WaitForSeconds (0.2f);
		carLight1.SetActive (false);
		carLight2.SetActive (false);
		yield return new WaitForSeconds (0.2f);
		if (flickering) {
			StartCoroutine (Flicker());
		}
	}


}
