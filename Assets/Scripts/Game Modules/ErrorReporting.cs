﻿using UnityEngine;
using System.Collections;

public class ErrorReporting : MonoBehaviour {

	string logURL = "";
	public static ErrorReporting Instance;

	void Start () {
		Instance = this;

		#if !UNITY_EDITOR
		Application.logMessageReceived += OnLog;
		#endif
	}

	void OnLog(string msg, string stackTrace, LogType type){

		if (type == LogType.Error || type == LogType.Exception) {
			UnitedAnalytics.LogEvent ("Exceptions", msg, stackTrace, DAO.PlayTimeInMinutes); 
		}

	}



}
