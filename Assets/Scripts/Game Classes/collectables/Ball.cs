﻿using UnityEngine;
using System.Collections;

public class Ball : Collectable {


	public GameObject MeshObject;

	Transform meshTransform;

	void Start () {
		type = CollectableTypes.BALL_STACK;
		meshTransform = MeshObject.transform;
	}

	void FixedUpdate () {
		meshTransform.Rotate( 0f,5f, 0f );
	}

	void OnTriggerEnter(Collider col){
		if (col.transform.gameObject.tag == "Player") {
			ParticleCollectGenerator.instance.PlayBallParticle(transform.position , transform.eulerAngles);
			PlayerController.instance.CollectBall ();
			AudioManager.Instance.OnPowerUpPick ();
			gameObject.SetActive (false);//Destroy(gameObject);

		}
			
	}
}
