﻿using UnityEngine;
using System.Collections;
//using Playscape.Analytics;
using System.Collections.Generic;

public class BI : MonoBehaviour {

	public static BI _;

	public static Dictionary <string, int> dick;

	void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log ("BI | " + msg);
	}

//	FlowInstance flow;
	public enum FLOW_STEP { 
		LOADING,
		GPG_FAILED,
		GPG_SUCCESS,
		PRESS_BUY_PLAYER,
		BOUGHT_FREE_PLAYER,
		SWIPE_R,
		SWIPE_L,
		JUMP,
		SLIDE,
		KICK,
		SLIDE_2,
		POWERUP_PICKED,
		TUTORIAL_FAILED,
		TUTORIAL_COMPLETED,
		COLLECTED_COINS,
		LOCKER_ROOM_ENTERED,
		STARTED_PRACTICE,
		STARTED_QUEST,
		ENTERED_PRACTICE_GAMEPLAY,
		ENTERED_QUEST_RUN,
		ELIGIBLE_FOR_2ND_PLAYER,
		ACQUIRED_2ND_PLAYER
	};

	void Awake(){
		_ = this;

		if (PlayerPrefs.GetInt("app_launch_count") <= 1) {
			Log ("Initializing flow FTUE...");
//			Report.Instance.RegisterFlow ("FTUE", new Dictionary<string, int> () {
//				{ FLOW_STEP.LOADING.ToString (), 1 },
//				{ FLOW_STEP.GPG_FAILED.ToString (), 2 },
//				{ FLOW_STEP.GPG_SUCCESS.ToString (), 3 },
//				{ FLOW_STEP.PRESS_BUY_PLAYER.ToString (), 4 },
//				{ FLOW_STEP.BOUGHT_FREE_PLAYER.ToString (), 5 },
//				{ FLOW_STEP.SWIPE_R.ToString (), 6 },
//				{ FLOW_STEP.SWIPE_L.ToString (), 7 },
//				{ FLOW_STEP.JUMP.ToString (), 8 },
//				{ FLOW_STEP.SLIDE.ToString (), 9 },
//				{ FLOW_STEP.KICK.ToString (), 10 },
//				{ FLOW_STEP.SLIDE_2.ToString (), 11 },
//				{ FLOW_STEP.POWERUP_PICKED.ToString (), 12 },
//				{ FLOW_STEP.TUTORIAL_FAILED.ToString (), 13 },
//				{ FLOW_STEP.TUTORIAL_COMPLETED.ToString (), 14 },
//				{ FLOW_STEP.COLLECTED_COINS.ToString (), 15 },
//				{ FLOW_STEP.LOCKER_ROOM_ENTERED.ToString (), 16 },
//				{ FLOW_STEP.STARTED_PRACTICE.ToString (), 17 },
//				{ FLOW_STEP.STARTED_QUEST.ToString (), 18 },
//				{ FLOW_STEP.ENTERED_PRACTICE_GAMEPLAY.ToString (), 19 },
//				{ FLOW_STEP.ENTERED_QUEST_RUN.ToString (), 20 },
//				{ FLOW_STEP.ELIGIBLE_FOR_2ND_PLAYER.ToString (), 21 },
//				{ FLOW_STEP.ACQUIRED_2ND_PLAYER.ToString (), 22 }
//			});

			//flow = Report.Instance.StartNewFlow ("FTUE");
//			Flow_FTUE (FLOW_STEP.LOADING);
			//Debug.Log("alon________________Flow_FTUE (FLOW_STEP.LOADING)");
		}
	}

	// ===============================================================


	/*
	 * 
	 * 		FLOWS
	 * 
	 * 
	 */

	public void Flow_FTUE(FLOW_STEP step){
		if (DAO.AppLaunchCount > 1) return;

		// Report Custom Flow Step
		//Report.Instance.ReportFlowStep(flow, step.ToString(), "achieved", new Dictionary<string, double>());

		//Log ("Reporting Flow Step: "+step.ToString());
		//Debug.Log("alon________________Flow_FTUE(FLOW_STEP step)");

	}



	/*
	 * 
	 * 		PROGRESSION
	 * 
	 * 
	 */

	public void Player_GainNewLevel(int level){

		// Report Custom Event
		//Report.Instance.ReportEvent("LevelUp", new Dictionary<string, string>(){{"LevelNumber", level.ToString()}});

		Log ("Reporting 'Player Gain New Level' event");
	}

	public void Quest_Completed(string quest_name){

		// Report Custom Event
		//Report.Instance.ReportEvent("AchievementReached", new Dictionary<string, string>(){{"Achievement", "Quest '"+quest_name+"' Completed"}});

		Log ("Reporting 'Quest Completed' event, quest name: "+quest_name);
	}

	public void QuestRun_Completed(int run_id, int coins, string gold_silver){

//		// Report Custom Event
//		Report.Instance.ReportEvent("MiniGame", new Dictionary<string, string>(){
//			{"Type", "Quest Run"},
//			{"Result", "Completed " + gold_silver},
//			{"Outcome Type", "FCB Coins"},
//			{"Outcome Amount", coins.ToString()},
//			{"Source", "Quests Menu"},
//			{"TypeConfigID", run_id.ToString()}
//		});

		Log ("Reporting 'MiniGame' event, Quest run completed");
	}
	public void AdWatched()
	{
//		Report.Instance.ReportEvent ("DisplayAd", new Dictionary<string, string> () {
//			{ "Network","UnityAds" }
//		});
	}

	public void QuestRun_Failed(int run_id, int coins, string reason, int section_num){

		// Report Custom Event
//		Report.Instance.ReportEvent("MiniGame", new Dictionary<string, string>(){
//			{"Type", "Quest Run"},
//			{"Result", "Failed"},
//			{"Outcome Type", "FCB Coins"},
//			{"Outcome Amount", coins.ToString()},
//			{"Source", "Quests Menu"},
//			{"TypeConfigID", run_id.ToString()},
//			{"Failure Type", reason},
//			{"Section of failure", section_num.ToString()}
//		});

		Log ("Reporting 'MiniGame' event, Quest run failed");
	}

	public void PracticeRun_Completed(int meters, int coins){

		// Report Custom Event
//		Report.Instance.ReportEvent("MiniGame", new Dictionary<string, string>(){
//			{"Type", "Tain Run"},
//			{"Result", "Completed"},
//			{"Outcome Type", "FCB Coins"},
//			{"Outcome Amount", coins.ToString()},
//			{"Source", "Locker Room Menu"},
//			{"Distance", meters.ToString()}
//		});

		Log ("Reporting 'MiniGame' event, Practice Run Completed");
	}


	public void PlayerLevelUp_1000M(int value){

		// Report Custom Event
		string valueStrng = value.ToString();

		Dictionary<string, string> eventAttrs = new Dictionary<string, string>();
		eventAttrs["Game Event Description"] = "User reached new level";
		eventAttrs["Playscape API Call"] = "Custom Event"; 
		eventAttrs["Event Type"] = "LevelUp"; 
		eventAttrs["Event Attributes - Key"] = "LevelNumber"; 
		eventAttrs["Event Attributes - Value"] = valueStrng;

//		Report.Instance.ReportEvent("ReachedNewLevel", eventAttrs);
	}



	/*
	 * 
	 * Inventory 
	 * 
	 */

	public void Inventory_Increase(string source, string itemId, string sku){

		// Report Custom Event
//		Report.Instance.ReportEvent("Inventory", new Dictionary<string, string>(){
//			{"Operation", "Increase"},
//			{"Amount", "1"},
//			{"Item", "Player"},
//			{"Source", source},
//			{"ItemID", itemId},
//			{"TransactionID", sku}
//		});

		Log ("Reporting 'Inventory Increase' event | Source: " + source + " , item id: " + itemId);
	}

	public void Inventory_Upgrade(int amount, string item, string source, string itemId){

		// Report Custom Event
//		Report.Instance.ReportEvent("Inventory", new Dictionary<string, string>(){
//			{"Operation", "Upgrade"},
//			{"Amount", amount.ToString()},
//			{"Item", item},
//			{"Source", source},
//			{"ItemID", itemId}
//		});

		Log ("Reporting 'Inventory Upgrade' event | Source: " + source + " , item: " + item);
	}

	public void Inventory_Downgrade(int amount, string itemId){

		// Report Custom Event
//		Report.Instance.ReportEvent("Inventory", new Dictionary<string, string>(){
//			{"Operation", "Downgrade"},
//			{"Amount", amount.ToString()},
//			{"Item", "Player Energy"},
//			{"Source", "Run Start"},
//			{"ItemID", itemId}
//		});

		Log ("Reporting 'Inventory Downgrade' event | Amount: " + amount + " , item id: " + itemId);

	}



	/*
	 * 
	 * Wallet 
	 * 
	 */


	public void Wallet_Deposit(int amount, string source, string item, string flow, string step, string sku ){

//		Report.Instance.ReportWalletOperation (
//			Report.WalletOperation.Deposit,
//			"",
//			sku,
//			amount,
//			"FCB Coins",
//			source,
//			flow,
//			step,
//			item,
//			Report.WalletResult.Success,
//			""
//		);

		Log ("Reporting 'Wallet Deposit' event | Amount: " + amount + " , item: " + item + " , source: " + source);
	}

	public void Wallet_Withdraw(int amount, string source, string item, string flow, string step, string sku ){

//		Report.Instance.ReportWalletOperation (
//			Report.WalletOperation.Withdraw,
//			"",
//			sku,
//			amount,
//			"FCB Coins",
//			source,
//			flow,
//			step,
//			item,
//			Report.WalletResult.Success,
//			""
//		);

		Log ("Reporting 'Wallet Withdraw' event | Amount: " + amount + " , item: " + item + " , source: " + source);
	}

	/*
	 * 
	 * Rate Us 
	 * 
	 */

	public enum RateUsActions {SHOWN, YES, NO};

	public void ReporRateUs(RateUsActions _action){

//		if (_action == RateUsActions.SHOWN) Report.Instance.ReportRatingDialogShow ();
//		else if(_action == RateUsActions.YES) Report.Instance.ReportRatingDialogYes ();
//		else if(_action == RateUsActions.NO) Report.Instance.ReportRatingDialogNo ();
	}


}
