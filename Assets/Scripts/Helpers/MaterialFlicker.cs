﻿using UnityEngine;
using System.Collections;

public class MaterialFlicker : MonoBehaviour {

	public SkinnedMeshRenderer mat;

	// Use this for initialization
	void Start () {
		foreach (Material myMat in mat.materials) {
			myMat.color = Color.blue;
		}
	}

}
