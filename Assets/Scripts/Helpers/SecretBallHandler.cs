﻿using UnityEngine;
using System.Collections;

public class SecretBallHandler : MonoBehaviour { //the ball itself:

	public Transform playerTransform;

	//public SecretHeaderDetector sHeaderDetector;

	public bool didHeader = false;
	bool countDownOn = false;

	Vector3 ballScale;

	MeshRenderer mRenderer;

	public RectTransform countDownUI;

	public Transform ballParent;

	Vector2 countDownPos;






	void Start(){
		mRenderer = GetComponent<MeshRenderer> ();
		mRenderer.enabled = false;
		ballScale = transform.localScale;
		countDownPos = countDownUI.position;
	}


	void Update(){
		if (!didHeader && Vector3.Distance (transform.position, playerTransform.position) < 80) {

			transform.RotateAround (transform.up, 5f * Time.deltaTime);

			ballScale.x = ballScale.y = ballScale.z += 0.25f * Time.deltaTime;

			transform.localScale = ballScale;

			if (!countDownOn) {
				countDownOn = true;
				StartCoroutine (BallCountDown());
			}
		}
	}


	IEnumerator BallCountDown(){
		if (ballParent.transform.localPosition.x > 0f) {
			countDownPos.x += Screen.width / 4;
			countDownUI.position = countDownPos;
		} else if (ballParent.transform.localPosition.x < 0f) {
			countDownPos.x -= Screen.width / 4;
			countDownUI.position = countDownPos;
		} else {
			countDownPos.x = Screen.width / 2;
			countDownUI.position = countDownPos;
		}

		countDownUI.gameObject.SetActive (true);

		yield return new WaitForSeconds (2);
		mRenderer.enabled = true;
		countDownUI.gameObject.SetActive (false);

	}
}
