using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Facebook.Unity;
using System.Collections.Generic;
using System;

public class LockerRoom_ActionsOld : MonoBehaviour
{

	public static LockerRoom_ActionsOld instance;

	public Animator uiAnimator;

	public GameObject PerksScreen;
	public GameObject RunsScreen;
	public GameObject PlayerChooseScreen;

	//public bool infoOn = false;
	bool chooseScreenOn = false;
	bool playerInfoAnimOn = false;
	bool mainMenuBtnAnimOn = false;
	bool runBtnAnimOn = false;
	bool startRrunBtnAnimOn = false;
	bool playerToBuyInfoOn = false;
	bool medikKitsOn = false;
	public bool runScrnOn = false;
	public bool cupScrnOn = false;
	public bool tutPopupsDisabled = false;
	
	//public GameObject playerChooseScreen;
	public GameObject runBtn;
	public GameObject playerInfo;
	//public GameObject goToPlusBtn;
	public GameObject leftArrow;
	public GameObject rightArrow;
	public GameObject swipeSign;

	public Text StarterOffer_OldPrice;
	public Text StarterOffer_NewPrice;
	public GameObject ScreebBlocker;
	public Text infoBtnTet;


	public GameObject FriendsListScreen;
	public GameObject MedikKitsScreen;
	GameManager.GameState RunToStartAfterPlayerAnimationDone;

	public ButtonClickAnimation recruitBtnAnim;

	public GameObject debugParent;


	void Start ()
	{
		//uiAnimator = GetComponent<Animator> ();
		//PlayerChooseScreenToggle ();
		StartCoroutine (PlayStartAnim ());
		PerksScreen.SetActive (false);
//		CupsScreen.SetActive (false);
//		RunsScreen.SetActive (false);	

		if (DAO.IsInDebugMode) {
			debugParent.SetActive (true);
		}
	}

	public void StartPreselectedRun ()
	{
		if (PlayerChooseManager.instance.onAction == true)
			return;
		if (!DAO.Instance.IsRunsDataReady) {
			GameManager.Instance.PromtAppNoInternet ();
			return;
		}
		if (!PlayerChooseManager.instance.IsSelectedPlayerCanRunCup () && GameManager.PreselectedGameState == GameManager.GameState.CUP_RUN) {
			MidekKitsScreenToggle (true);
			return;
		}
		PlayerChooseManager.instance.onAction = true;
		if (PrefabManager.instanse.chosenPlayer.PlayerName == "") {
			PrefabManager.instanse.ChoosePlayer (PlayerChooseManager.instance.currentPlayer.GetComponent<Player> ().PlayerID);
		}
		//Bench.instance.currentBenchPlayer.GetComponent<Player>().WalkOut (false);

		StartRunBtnAnim (false);
		PlayerInfoAnim (false);
		RunBtnAnim (false);
		RunToStartAfterPlayerAnimationDone = GameManager.PreselectedGameState;
		GameManager.BenchInPreselectedRunMode = false;
		StartCoroutine (WaitForPlayerToWalkOut (true));
		ScreebBlocker.SetActive (true);
	}

	public void Start_PracticeRun ()
	{
		if (PlayerChooseManager.instance.onAction == true)
			return;
		if (!DAO.Instance.IsRunsDataReady) {
			GameManager.Instance.PromtAppNoInternet ();
			return;
		}
		PlayerChooseManager.instance.onAction = true;

		PlayerChooseManager.instance.currentPlayerScript.WalkOut (false);

		PlayerInfoAnim (false);
		RunBtnAnim (false);
		RunToStartAfterPlayerAnimationDone = GameManager.GameState.INFINITY_RUN;
		StartCoroutine (WaitForPlayerToWalkOut (false));

		UnitedAnalytics.LogEvent ("Practice Started", "From Bench", UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers);
		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE (BI.FLOW_STEP.STARTED_PRACTICE);

		if (PrefabManager.instanse.chosenPlayer.PlayerName == "") {
			PrefabManager.instanse.ChoosePlayer (PlayerChooseManager.instance.currentPlayerScript.PlayerID);
		}
		ScreebBlocker.SetActive (true);
	}

	public void reportQuestStartCicked ()
	{
		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE (BI.FLOW_STEP.STARTED_QUEST);
	}

	public void Start_TutorialRun ()
	{
		PlayerChooseManager.instance.onAction = true;

		RunToStartAfterPlayerAnimationDone = GameManager.GameState.TUTORIAL_RUN;

		StartCoroutine (WaitForPlayerToWalkOut (true));
	}

	IEnumerator WaitForPlayerToWalkOut (bool feedbackOb)
	{
		if (feedbackOb) {
			yield return new WaitForSeconds (4f);
		} else {
			yield return new WaitForSeconds (2.5f);
		}
		GameManager.SwitchState (RunToStartAfterPlayerAnimationDone);
	}



	public void GoTo_MainMenu ()
	{
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
	}


	IEnumerator PlayStartAnim ()
	{
		yield return new WaitForSeconds (1.5f);
		//PlayerInfoAnim ();
		MainMenuBtnAnim ();
		//RunBtnAnim ();
	}


	public void MainMenuBtnAnim ()
	{
		mainMenuBtnAnimOn = !mainMenuBtnAnimOn;
		if (mainMenuBtnAnimOn)
			uiAnimator.Play ("main menu btn in", 1, 0);
		else
			uiAnimator.Play ("main menu btn out", 1, 0);
	}


	public void PlayerInfoAnim (bool on)
	{
		//playerInfoAnimOn = !playerInfoAnimOn;
		if (on && !playerInfoAnimOn) {
			uiAnimator.Play ("player info in", 0, 0);
			playerInfoAnimOn = true;
		} else if (!on && playerInfoAnimOn) {
			uiAnimator.Play ("player info out", 0, 0);
			playerInfoAnimOn = false;
		}
	}

	bool playerSwapAnimOn = false;
	public void PlayerInfoSwapAnim(bool on){
		if (on && !playerSwapAnimOn) {
			playerSwapAnimOn = true;
			PlayerDataUIScreen.Instance.SetViewWithCurrentPlayer ();
			uiAnimator.Play ("player info swap in", 3, 0);
		} else if (!on && playerSwapAnimOn) {
			playerSwapAnimOn = false;
			uiAnimator.Play ("player info swap out", 3, 0);
		}
	}



	public void RunBtnAnim (bool on)
	{

		if (on && !runBtnAnimOn) {
			uiAnimator.Play ("run btn in", 2, 0);
			runBtnAnimOn = true;

			//if(Tutorial.IsNeedToBounceCupButton) uiAnimator.Play ("cup_btn_bounce_on");

		} else if (!on && runBtnAnimOn) {
			uiAnimator.Play ("run btn out", 2, 0);
			runBtnAnimOn = false;

			//if(Tutorial.IsNeedToBounceCupButton) uiAnimator.Play ("cup_btn_bounce_off");
		}
	}

	public void StartRunBtnAnim (bool on)
	{
		if (on && !startRrunBtnAnimOn) {
			uiAnimator.Play ("startrun btn in", 2, 0);
			startRrunBtnAnimOn = true;
		} else if (!on && startRrunBtnAnimOn) {
			uiAnimator.Play ("startrun btn out", 2, 0);
			startRrunBtnAnimOn = false;
		}
	}




	public void TutorialRunBtnAnim (bool on)
	{
		if (on) {
			uiAnimator.Play ("start_tutorial_run_btn_in", 2, 0);
		} else {
			uiAnimator.Play ("start_tutorial_run_btn_out", 2, 0);
		}
	}


	public void FirstPlayerRecruted (bool skipOn)
	{
//		GenericUIMessage gm = new GenericUIMessage();
//		gm.Title = DAO.Language.GetString ("first-player-recruited");
//		gm.Message = DAO.Language.GetString ("youll-have-more");
//		CrossSceneUIHandler.Instance.showGennericMessage (gm);

//		uiAnimator.Play ("go_btn_bounce");
		if (skipOn) {
			//LocalDataInterface.Instance.tutorialState = 1;
			//uiAnimator.Play ("skip_tutorial_in");
			SkipTutorialAnimToggle (true);
		} else {
			//LocalDataInterface.Instance.tutorialState = 1;
		}

	}


	public void PlayerChooseScrnAnim (bool on)
	{
		if (on && !chooseScreenOn) {
			chooseScreenOn = true;
			PlayerChooseScreen.SetActive (true);
			uiAnimator.Play ("player choose scrn in", 4, 0);
			GM_Input._.PlayersList = true;
		} else if (!on && chooseScreenOn) {
			chooseScreenOn = false;
			uiAnimator.Play ("player choose scrn out", 4, 0);
			GM_Input._.PlayersList = false;
		}
	}





	public void PlayerToBuyCardAnimToggle (bool on)
	{
		//playerToBuyInfoOn = !playerToBuyInfoOn;
		if (on && !playerToBuyInfoOn) {
			playerToBuyInfoOn = true;
			uiAnimator.Play ("player to buy card up", 0, 0);
		} else if (!on && playerToBuyInfoOn) {
			playerToBuyInfoOn = false;
			uiAnimator.Play ("player to buy card down", 0, 0);
		}
	}


	public void CupScrnAnimToggle (bool on)
	{
		if (!DAO.Instance.IsRunsDataReady) {
			GameManager.Instance.PromtAppNoInternet ();
			return;
		}
		uiAnimator.speed = 1f;

		if (Tutorial.IsNeedToBounceCupButton) DAO.TutorialState++;
	//	Debug.Log ("alon_ CupScrnAnimToggle()");
		if (on && !cupScrnOn) {
			cupScrnOn = true;
			//uiAnimator.Play ("cup scrn in", 5 , 0);
			CupsManager.Instance.InitializeQuestUIElements ();
			CrossSceneUIHandler.Instance.hideCoinsCounter ();
			uiAnimator.Play ("QuestsMap_Show", 5, 0);
			PlayerChooseManager.instance.onAction = true;
			GM_Input._.QuestMap = true;
		//	Debug.Log ("alon_ CupScrnAnimToggle()  on");
		} else if (!on && cupScrnOn) {
			cupScrnOn = false;
			GM_Input._.QuestMap = false;
			//uiAnimator.Play ("cup scrn out", 5 , 0);
			//CrossSceneUIHandler.Instance.showCoinsCounter();
			uiAnimator.Play ("QuestsMap_Hide", 5, 0);
			PlayerChooseManager.instance.onAction = false;
			//Debug.Log ("alon_ CupScrnAnimToggle()  off");
		}
	}

	public void CupScrnAnimToggle (bool on, bool ShowImideately)
	{
		if (ShowImideately)
			uiAnimator.speed = 100f;


		if (on && !cupScrnOn) {
			cupScrnOn = true;

			CrossSceneUIHandler.Instance.hideCoinsCounter ();
			uiAnimator.Play ("QuestsMap_Show", 5, 0);
			PlayerChooseManager.instance.onAction = true;
			GM_Input._.QuestMap = true;
		} else if (!on && cupScrnOn) {
			GM_Input._.QuestMap = false;
			//CrossSceneUIHandler.Instance.showCoinsCounter();
			uiAnimator.Play ("QuestsMap_Hide", 5, 0);
			PlayerChooseManager.instance.onAction = false;
			cupScrnOn = false;
		}
	}


	public void RunsScrnAnimToggle (bool on)
	{
		uiAnimator.speed = 1f;
		if (on) {
			uiAnimator.Play ("runs screen in", 4, 0);
			runScrnOn = true;
			GM_Input._.RunsScreen = true;
			//Debug.Log ("alon________ LockerRoomUIActions - RunsScrnAnimToggle(true)");
		} else {
			uiAnimator.Play ("runs screen out", 4, 0);
			runScrnOn = false;
			GM_Input._.RunsScreen = false;
			//Debug.Log ("alon________ LockerRoomUIActions - RunsScrnAnimToggle(false)");
		}
	}

	public void RunsScrnAnimToggle (bool on, bool ShowImideately)
	{
		if (ShowImideately)
			uiAnimator.speed = 100f;


		if (on) {
			uiAnimator.Play ("runs screen in", 4, 0);
			runScrnOn = true;
			GM_Input._.RunsScreen = true;

		} else {
			uiAnimator.Play ("runs screen out", 4, 0);
			runScrnOn = false;
			GM_Input._.RunsScreen = false;
		}
	}


	public void CloseCupScrn (bool on)
	{
//		
//			if(GameManager.BenchInPreselectedRunMode){
//				StartRunBtnAnim(true);
//			}else{
//				RunBtnAnim(true);
//			}
		RunBtnAnim (true);
		CupScrnAnimToggle (false);

	}


	//	public void CloseRunsScreen(){
	//		if (runScrnOn) {
	//			RunsScrnAnimToggle (false);
	//			//runScrnOn = false;
	//		}
	//	}

	public void CloseCupsAndRuns ()
	{
		uiAnimator.Play ("close_cup_and_run", 5, 0);

		GM_Input._.RunsScreen = false;
		runScrnOn = false;

		GM_Input._.QuestMap = false;
		cupScrnOn = false;
		//CrossSceneUIHandler.Instance.showCoinsCounter();
		PlayerChooseManager.instance.onAction = false;
		//Debug.Log ("alon_ CloseCupsAndRuns()");
	}

	public void ShowCoinsCounter ()
	{
		CrossSceneUIHandler.Instance.showCoinsCounter ();
	}

	public void MidekKitsScreenToggle (bool show)
	{
		if (!medikKitsOn && show) {
			MedikKitsScreen.SetActive (true);
			//MedikKits_Actions.Instance.OnOpen();
			uiAnimator.Play ("medik kit show", 4, 0);
			medikKitsOn = true;
			GM_Input._.MedikKits = true;
		} else if (medikKitsOn && !show) {
			//LocalDataInterface.Instance.popupHeal = 1;
			DAO.HealPopoup = 1;
			medikKitsOn = false;
			uiAnimator.Play ("medik kit hide", 4, 0);
			GM_Input._.MedikKits = false;
		}
	}

	bool lowEnergyPopupOn = false;

	public void LowEnergyPopup (bool on)
	{
		if (on && !lowEnergyPopupOn) {
			GM_Input._.LowEnergy = true;
			uiAnimator.Play ("LowEnergy_Show", 10, 0);
			PlayerChooseManager.instance.onAction = true;
			lowEnergyPopupOn = true;
		} else if (!on && lowEnergyPopupOn) {
			GM_Input._.LowEnergy = false;
			uiAnimator.Play ("LowEnergy_Hide", 10, 0);
			PlayerChooseManager.instance.onAction = false;
			lowEnergyPopupOn = false;
		}
	}

	public bool healPopupOn = false;

	public void HealPopup (bool on)
	{
		if (on && !healPopupOn) {
			uiAnimator.Play ("tut_heal_popup_in");
			healPopupOn = true;
			GM_Input._.Heal = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && healPopupOn) {
			uiAnimator.Play ("tut_heal_popup_out");
			healPopupOn = false;
			GM_Input._.Heal = false;
			PlayerChooseManager.instance.onAction = false;
			DAO.HealPopoup = 1;
		}
	}

	public void HealPopupAnalyticsEvent ()
	{
		UnitedAnalytics.LogEvent ("health tutroail", "Closed", UserData.Instance.userType.ToString ());
	}



	public bool winScrnOn = false;

	public void WinScrnToggle ()
	{
		winScrnOn = !winScrnOn;

		if (winScrnOn) {
			GM_Input._.CupWon = true;
			uiAnimator.Play ("winning screen in", 14, 0);
		} else {
			GM_Input._.CupWon = false;
			uiAnimator.Play ("winning screen out", 14, 0);
			GameManager.ActiveCup = null;
			GameManager.Instance.activeCupInstance = null;
		}
	}


	bool looseScrnOn = false;

	public void LoseScrnToggle ()
	{
		looseScrnOn = !looseScrnOn;
		if (looseScrnOn) {
			uiAnimator.Play ("loosing screen in", 4, 0);
		} else {
			uiAnimator.Play ("loosing screen out", 4, 0);
		}
	}


	bool perksScrnOn = false;

	public void PerksScrnAnim (bool on)
	{
		if (on && !perksScrnOn) {
			PerksScreen.SetActive (true);
			PerksScrn_Actions.Instance.OnBeforeShow ();
			uiAnimator.Play ("perks screen in", 4);
			PlayerChooseManager.instance.onAction = true;
			perksScrnOn = true;
			GM_Input._.Perks = true;
		} else if (!on && perksScrnOn) {
			GM_Input._.Perks = false;
			uiAnimator.Play ("perks screen out", 4);
			UnitedAnalytics.LogEvent ("Perk Screen closed", "closed", UserData.Instance.userType.ToString (), DAO.PlayTimeInMinutes);
			//Bench.instance.swiping = false;
			perksScrnOn = false;
			PlayerChooseManager.instance.PerkGlowFX ();
		}
	}

	bool addBtnOn = false;

	public void AddBtnAnimation (bool on)
	{
		if (on && !addBtnOn) {
			uiAnimator.Play ("add_btn_in", 6);
			addBtnOn = true;
		} else if (!on && addBtnOn) {
			uiAnimator.Play ("add_btn_out", 6);
			addBtnOn = false;
		}
	}

	bool goToPlusOn = false;

	public void GoToPlusToggle (bool on)
	{
		
		if (on && !goToPlusOn) {
			//goToPlusBtn.SetActive (true);
			uiAnimator.Play ("go_to_plus_btn_in", 7);
			goToPlusOn = true;
		} else if (!on && goToPlusOn) {
			//goToPlusBtn.SetActive (false);
			uiAnimator.Play ("go_to_plus_btn_out", 7);
			goToPlusOn = false;
		}
	}


	public void ContractAnim ()
	{
		uiAnimator.Play ("contract", 12, 0f);
	}


	bool leftArrowOn = false;

	public void LeftArrow (bool on)
	{
		if (on && !leftArrowOn) {
			uiAnimator.Play ("arrow_left_in", 9);
			leftArrowOn = true;
		} else if (!on && leftArrowOn) {
			uiAnimator.Play ("arrow_left_out", 9);
			leftArrowOn = false;
		}
	}

	bool rightArrowOn = false;

	public void RightArrow (bool on)
	{
		if (on && !rightArrowOn) {
			uiAnimator.Play ("arrow_right_in", 8);
			rightArrowOn = true;
		} else if (!on && rightArrowOn) {
			uiAnimator.Play ("arrow_right_out", 8);
			rightArrowOn = false;
		}
	}

	Vector2 swipeSignPos = new Vector2 (0, 0);
	bool swipeSignOn = false;

	//	public void SwipeSign(bool on){
	//		if (on && !swipeSignOn) {
	//			//uiAnimator.Play ("arrow_right_in", 8);
	//			swipeSign.SetActive(true);
	//			if (Bench.instance.benchState == Bench.BenchState.BENCH)
	//				swipeSignPos.y = 815f;
	//			else
	//				swipeSignPos.y = -450f;
	//			swipeSign.GetComponent<RectTransform> ().anchoredPosition = swipeSignPos;
	//			swipeSignOn = true;
	//		}else if(!on && swipeSignOn){
	//			//uiAnimator.Play ("arrow_right_out", 8);
	//			swipeSign.SetActive(false);
	//			swipeSignOn = false;
	//		}
	//	}


	private enum ArrowsConditions
	{
		LEFT_ARROW,
		RIGHT_ARROW,
		BOTH_ARROWS,
		NO_ARROWS}

	;

	ArrowsConditions arrowsConditions = ArrowsConditions.BOTH_ARROWS;

	public void ArrowsToggle (int side)
	{
		switch (side) {
		case -1:
			if (arrowsConditions == ArrowsConditions.LEFT_ARROW)
				return;
			uiAnimator.Play ("arrow_left_in", 10);
			arrowsConditions = ArrowsConditions.LEFT_ARROW;
			break;
		case 1:
			if (arrowsConditions == ArrowsConditions.RIGHT_ARROW)
				return;
			uiAnimator.Play ("arrow_left_in", 10);
			arrowsConditions = ArrowsConditions.RIGHT_ARROW;
			break;
		case 2:
			if (arrowsConditions == ArrowsConditions.BOTH_ARROWS)
				return;
			uiAnimator.Play ("arrow_left_in", 10);
			arrowsConditions = ArrowsConditions.BOTH_ARROWS;
			break;
		case 0:
			if (arrowsConditions == ArrowsConditions.NO_ARROWS)
				return;
			uiAnimator.Play ("arrow_left_out", 10);
			arrowsConditions = ArrowsConditions.NO_ARROWS;
			break;
		}
	}



	Vector3 objScale;
	Vector3 targetObjScale;

	public void ObjectPressAnim (GameObject obj)
	{
		objScale = obj.transform.localScale;
		targetObjScale = new Vector3 (objScale.x / 1.2f, objScale.y / 1.2f, objScale.z / 1.2f);
		StartCoroutine (ObjectPressAnimEnumatator (obj));
	}

	WaitForEndOfFrame _weof;

	WaitForEndOfFrame weof {
		get { 
			if (_weof == null)
				_weof = new WaitForEndOfFrame ();

			return weof;
		}
	}

	Transform trnsf;

	IEnumerator ObjectPressAnimEnumatator (GameObject obj)
	{
		trnsf = obj.transform;
		for (float i = 0f; i <= 1f; i += (6f * Time.deltaTime)) {
			trnsf.localScale = Vector3.Lerp (trnsf.localScale, targetObjScale, i);
			if (i >= 0.4f)
				i = 1f;
			yield return weof;
		}
		for (float i = 0f; i <= 1f; i += (4f * Time.deltaTime)) {
			trnsf.localScale = Vector3.Lerp (trnsf.localScale, objScale, i);
			yield return weof;
		}
	}

	//bool swipingHandOn = false;
	public void SwipingHand (bool on)
	{
		if (on) {
			StartCoroutine (SwipingHandCoroutine ());
		} else {
			uiAnimator.Play ("swiping_hand_out");
		}

//		if (on && !swipingHandOn) {
//			StartCoroutine (SwipingHandCoroutine());
//			swipingHandOn = true;
//		} else if (!on && swipingHandOn) {
//			uiAnimator.Play ("swiping_hand_out");
//			swipingHandOn = false;
//		}
	}

	IEnumerator SwipingHandCoroutine ()
	{
		yield return new WaitForSeconds (2);
		//uiAnimator.Play ("swiping_hand");
	}


	public bool connectedToFacebook = false;

	public void TutorialConnectedAnim (bool on)
	{
		if (on) {
			uiAnimator.Play ("tutorial_user_connected");
			uiAnimator.Play ("tutorial_pointer_in");
		} else {
			uiAnimator.Play ("tutorial_user_connected_out");
			uiAnimator.Play ("tutorial_pointer_out");
		}
		connectedToFacebook = true;
	}


	public void TutorialNotConnectedAnim (bool on)
	{
		if (on) {
			uiAnimator.Play ("tutorial_not_connected_in");
			uiAnimator.Play ("tutorial_pointer_in");
		} else {
			uiAnimator.Play ("tutorial_not_connected_out");
			uiAnimator.Play ("tutorial_pointer_out");
		}
		connectedToFacebook = false;
	}

	bool tutPointerOn = false;

	public void TutorialPointerToggle (bool on)
	{
		if (on && !tutPointerOn) {
			uiAnimator.Play ("tutorial_pointer_in");
			tutPointerOn = true;
		} else if (!on && tutPointerOn) {
			uiAnimator.Play ("tutorial_pointer_out");
			tutPointerOn = false;
		}
	}

	bool recruitBounce = false;

	public void TutorialRecruitBounce (bool on)
	{
		if (on && !recruitBounce) {
			recruitBounce = true;
			recruitBtnAnim.BounceToggle (true);
			StartCoroutine ("TutorialChoosePlayerCoroutine");
		} else if (!on && recruitBounce) {
			recruitBounce = false;
			recruitBtnAnim.BounceToggle (false);
			StopCoroutine ("TutorialChoosePlayerCoroutine");
		}
	}

	IEnumerator TutorialChoosePlayerCoroutine ()
	{
		yield return new WaitForSeconds (3f);
		uiAnimator.Play ("tutorial_recruit_bounce", 6, 0f);
		yield return new WaitForSeconds (1f);
		StartCoroutine ("TutorialChoosePlayerCoroutine");
	}


	public void SkipTutorial ()
	{
		StartCoroutine (SkipTutorialCoroutine ());
	}

	IEnumerator SkipTutorialCoroutine ()
	{
		yield return new WaitForSeconds (0.05f);
		TutorialRunBtnAnim (false);
		//uiAnimator.Play ("skip_tutorial_out");
		SkipTutorialAnimToggle (false);
		yield return new WaitForSeconds (0.75f);
		GoToPlusToggle (true);
		RunBtnAnim (true);
		LocalDataInterface.Instance.tutorialState = 4;
		GameAnalyticsWrapper.DesignEvent (GameAnalyticsWrapper.FLOW_STEP.TUTORIAL_QUIT);
	}

	bool skipOn = false;

	public void SkipTutorialAnimToggle (bool on)
	{
		if (on && !skipOn) {
			uiAnimator.Play ("skip_tutorial_in");
			skipOn = true;
		} else if (!on && skipOn) {
			uiAnimator.Play ("skip_tutorial_out");
			skipOn = false;
		}
	}



	public bool recruitSecondPopup = false;

	public void RecruitSecondPopup (bool on)
	{
		if (on && !recruitSecondPopup) {
			uiAnimator.Play ("tut_recruit_popup_in");
			recruitSecondPopup = true;
			GM_Input._.Recruit = true;
			//Bench.instance.swiping = true;
		} else if (!on && recruitSecondPopup) {
			GM_Input._.Recruit = false;
			uiAnimator.Play ("tut_recruit_popup_out");
			recruitSecondPopup = false;
			//Bench.instance.swiping = false;
			if (Tutorial.IsNeedToShowBuySecondPlayer1Popup) {
				LocalDataInterface.Instance.recruitSecondPlayerPopup2Date = DateTime.Now.ToString ();
				DAO.BuySecondPlayerPopup1 = 1;
			} else if (Tutorial.IsNeedToShowBuySecondPlayer2Popup) {
				LocalDataInterface.Instance.recruitSecondPlayerPopup2Date = DateTime.Now.ToString ();
			}
		}
	}

	public void RecruitSecondAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("Player Available pop up", "used", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("Player Available pop up", "closed", UserData.Instance.userType.ToString ());
		}
	}

	public Text coinsToThirdPlayerTxt;
	public bool recruitThirdPopup = false;

	public void RecruitThirdPopup (bool on, int coinsToThirdPlayer)
	{
		if (on && !recruitThirdPopup) {
			coinsToThirdPlayerTxt.text = coinsToThirdPlayer.ToString ();
			uiAnimator.Play ("tut_recruit2_popup_in");
			recruitThirdPopup = true;
			GM_Input._.RecruitAnother = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && recruitThirdPopup) {
			GM_Input._.RecruitAnother = false;
			uiAnimator.Play ("tut_recruit2_popup_out");
			recruitThirdPopup = false;
			PlayerChooseManager.instance.onAction = false;
		}
	}

	public void RecruitThirdPopupClose ()
	{
		uiAnimator.Play ("tut_recruit2_popup_out");
		recruitThirdPopup = false;
		DAO.BuyThirdPlayerPopup = 1;
		PlayerChooseManager.instance.onAction = false;
	}

	public void OnRequruitThirdPopupGetClicked ()
	{
		RecruitThirdPopupClose ();
		CrossSceneUIHandler.Instance.showStore (CrossSceneUIHandler.ShopStates.COINS);

		IAP.AdditionalIAPCallback += OnPackPurchasedAfterRecruitThirdPopup;
	}

	void OnPackPurchasedAfterRecruitThirdPopup (string sku)
	{
		IAP.AdditionalIAPCallback -= OnPackPurchasedAfterRecruitThirdPopup;

		UnitedAnalytics.LogEvent ("almost enough for next player pop up", "IAP MAde", UserData.Instance.userType.ToString (), DAO.TotalCoinsCollected);
	}

	public void RecruitThirdAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("almost enough for next player pop up", "clicked", UserData.Instance.userType.ToString (), DAO.TotalCoinsCollected);
		} else {
			UnitedAnalytics.LogEvent ("almost enough for next player pop up", "Closed", UserData.Instance.userType.ToString (), DAO.TotalCoinsCollected);
		}
	}


	public bool practicePopup = false;

	public void PracticePopup (bool on)
	{
		if (on && !practicePopup) {
			uiAnimator.Play ("tut_practice_popup_in");
			practicePopup = true;
			GM_Input._.PracticePopup = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && practicePopup) {
			GM_Input._.PracticePopup = false;
			uiAnimator.Play ("tut_practice_popup_out");
			practicePopup = false;
			DAO.PlayPracticePopup = 1;
			PlayerChooseManager.instance.onAction = false;
		}
	}

	public void PracticePopupAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("play practice pop up", "used", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("play practice pop up", "Closed", UserData.Instance.userType.ToString ());
		}
	}


	bool cupPopup = false;

	public void CupPopup (bool on)
	{
		if (on && !cupPopup) {
			uiAnimator.Play ("tut_cup_popup_in");
			cupPopup = true;
			GM_Input._.CupsPopup = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && cupPopup) {
			GM_Input._.CupsPopup = false;
			uiAnimator.Play ("tut_cup_popup_out");
			cupPopup = false;
			PlayerChooseManager.instance.onAction = false;
			LocalDataInterface.Instance.playCupPopupDate = DateTime.Now.ToString ();
		}
	}

	public void CupPopupAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("play quests pop up", "used", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("play quests pop up", "Closed", UserData.Instance.userType.ToString ());
		}
	}


	bool offerPopup = false;

	public void OfferPopup (bool on)
	{
		if (on && !offerPopup) {
			StarterOffer_OldPrice.text = IAP.Instance.Product_LOCAL_CLUB.GetPriceLabel ();
			StarterOffer_NewPrice.text = IAP.Instance.Product_STARTER_DEAL.GetPriceLabel ();

			uiAnimator.Play ("tut_offer_popup_in");
			offerPopup = true;
			GM_Input._.WellcomeOffer = true;
		} else if (!on && offerPopup) {
			GM_Input._.WellcomeOffer = false;
			uiAnimator.Play ("tut_offer_popup_out");
			offerPopup = false;
			LocalDataInterface.Instance.offerPopupDate = DateTime.Now.ToString ();
		}
	}

	public void OfferPopupAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("Buy starter pop up", "clicked", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("Buy starter pop up", "Closed", UserData.Instance.userType.ToString ());
		}
	}




	public void OnSendGiftCardsClicked ()
	{
		if (FB.IsLoggedIn) {
			CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.SEND_GIFTCARDS);
		} else {
			FBModule.OnFacebookProfileRecieved += OnFBConnectedSendGiftCards;
			FBModule.Instance.FBButonClicked = true;
			FBModule.Instance.Login ();
		}
	}

	public void OnFBConnectedSendGiftCards ()
	{
		FBModule.OnFacebookProfileRecieved -= OnFBConnectedSendGiftCards;
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.SEND_GIFTCARDS);
	}

	public void OnBuySpecialDealClicked ()
	{
		IAP.Instance.OnStarterPackClicked ();
		OfferPopup (false);
	}

	public void ShowQuestFriendsList (List<FBFriend> friends)
	{
		FriendsListScreen.SetActive (true);
		FriendsListScreen.GetComponent<QuestFriendsList> ().SetFriends (friends);
		uiAnimator.Play ("FriendsListShow", 13);
	}

	public void HideQuestFriendsList ()
	{
		uiAnimator.Play ("FriendsListHide", 13);
	}

	public void ShowRateUsPopup ()
	{
		if (DAO.Instance.settings.showRateUs == 1) {
			uiAnimator.Play ("rate_us_popup_in");
			if (DAO.RateUsTimeFactor == 10) {
				DAO.RateUsTimeFactor = 720;
				//Debug.Log ("alon______________ RateUsTimeFactor was 10, now its " + DAO.RateUsTimeFactor);
			}
			UnitedAnalytics.LogEvent ("Rate Us Opened", "Popup");
			if (GameManager.Instance.isPlayscapeLoaded)
				BI._.ReporRateUs (BI.RateUsActions.SHOWN);
			GM_Input._.RateUsPopup = true;
			//CrossSceneUIHandler.Instance.FreeGiftBtnAnimToggle (false);
		} 

	}



	public void OnRateUsClose (bool rated)
	{
		if (!rated) {
			DAO.RateUsPopupDate = DateTime.Now;
			uiAnimator.Play ("rate_us_popup_out");
			if(GameManager.Instance.isPlayscapeLoaded)	BI._.ReporRateUs (BI.RateUsActions.NO);
			GM_Input._.RateUsPopup = false;
		} else {
			uiAnimator.Play ("rate_us_popup_out");
		}
		//CrossSceneUIHandler.Instance.FreeGiftBtnAnimToggle (GameManager.Instance.isFreeGifts);
	}

	public void RateUsThanksToggle (bool on)
	{
		if (on) {
			//Debug.Log ("alon________ RateUsThanksToggle(true)");
			GM_Input._.RateUsThanksPopup = true;
			uiAnimator.Play ("rate_us_thanks_popup in");
		} else {
			//Debug.Log ("alon________ RateUsThanksToggle(false)");
			GM_Input._.RateUsPopup = false;
			GM_Input._.RateUsThanksPopup = false;
			uiAnimator.Play ("rate_us_thanks_popup out");
		}
	}
		

}

