﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;
using Amazon;

public class ServerDataInterface : MonoBehaviour {
	private string DATA_URL_BASE;
	private string VERSION_URL_BASE;

//	private const string LIVE_DATA_URL_BASE = "http://unity.play.im/data/get.php?data=setup";
//	private const string LIVE_VERSION_URL_BASE = "http://unity.play.im/data/get.php?data=version";

	private const string LIVE_DATA_URL_BASE = "https://ars.gamehour.com/data/get.php?data=dataInit";
	private const string LIVE_DATA_URL_BASE_RUNS = "https://ars.gamehour.com/data/get.php?data=dataRuns";
	private const string LIVE_VERSION_URL_BASE = "https://ars.gamehour.com/data/get.php?data=version";

	private const string STAGING_DATA_URL_BASE = "https://arsstage.gamehour.com/data/get.php?data=dataInit";
	private const string STAGING_DATA_URL_BASE_RUNS = "https://arsstage.gamehour.com/data/get.php?data=dataRuns";
	private const string STAGING_VERSION_URL_BASE = "https://arsstage.gamehour.com/data/get.php?data=version";

	private const string LOCAL_DATA_URL_BASE = "https://arsstage.gamehour.com/data/get.php?data=dataInit";
	private const string LOCAL_DATA_URL_BASE_RUNS = "https://arsstage.gamehour.com/data/get.php?data=dataRuns";
	private const string LOCAL_VERSION_URL_BASE = "https://arsstage.gamehour.com/data/get.php?data=version";


	private string DATA_URL;
	private string DATA_URL_RUNS;
	private string VERSION_URL;

	private int tryToGetVersionCount =0;
	private int tryToGetDataInitCount =0;
	private int tryToGetDataRuns = 0;

	public event Action<string> onVersionRecieved;
	public event Action<string> onDataRecieved;
	public event Action<string> onDataRunsRecieved;
	public event Action<string> onError;

	private WWW ajax;
	public JSONNode DATA;

	private static ServerDataInterface instance;
	public static ServerDataInterface Instance{
		get{
			return instance;
		}
	}

	void Awake(){
		instance = this;


		 
	}  

	void SetHosts(){
		if (!AppSettings._.LOAD_DATA_FROM_LOCAL_SERVER) {
            if (DAO.IsInDebugMode) {
//			if (true) {
				DATA_URL_BASE = STAGING_DATA_URL_BASE + "&appversion=" + AppSettings._.APP_VERSION;
				DATA_URL_RUNS = STAGING_DATA_URL_BASE_RUNS;
				VERSION_URL_BASE = STAGING_VERSION_URL_BASE;
				Debug.Log ("alon_____________________ ServerDataInterface SetHosts - IsInDebugMode - STAGING");
			} else {
				// change later to live
				DATA_URL_BASE = LIVE_DATA_URL_BASE + "&appversion=" + AppSettings._.APP_VERSION;
				DATA_URL_RUNS = LIVE_DATA_URL_BASE_RUNS;
				VERSION_URL_BASE = LIVE_VERSION_URL_BASE;
				Debug.Log ("alon_____________________ ServerDataInterface SetHosts - LIVE");
			}
		} else {
			DATA_URL_BASE = LOCAL_DATA_URL_BASE + "&appversion="+AppSettings._.APP_VERSION;
			VERSION_URL_BASE = LOCAL_DATA_URL_BASE_RUNS;
			DATA_URL_RUNS = LIVE_DATA_URL_BASE_RUNS;
			Debug.Log ("alon_____________________ ServerDataInterface SetHosts - LOAD_DATA_FROM_LOCAL_SERVER");
		}
	}


	private IEnumerator WaitForData(){
		//Preloader.Instance.PrintStatus ("Waiting for data");
		ajax = new WWW (DATA_URL);
		//yield return ajax;

		while (ajax.isDone == false) {
			DAO.Log ("bytes downloaded: " + ajax.bytesDownloaded.ToString ());
			yield return null;
		}
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("WaitForData data recived");
		if (ajax.error == null) {
			prepareData (ajax.text);
			if (onDataRecieved != null)
				onDataRecieved (ajax.text);
		} else {
			if (tryToGetDataInitCount < 2 ||
			    (tryToGetDataInitCount < 5 && ((DAO.Instance.LOCAL == null) || (DAO.Instance.LOCAL.dataVersion == 0) ||
			    (string.IsNullOrEmpty (DAO.Instance.LOCAL.data)) || (DAO.Instance.LOCAL.data.Trim () == "[]")))) {
				tryToGetDataInitCount++;
				StartCoroutine (WaitForData ());	
			} else {
				if ((DAO.Instance.LOCAL == null) || (DAO.Instance.LOCAL.dataVersion == 0) ||
				    (string.IsNullOrEmpty (DAO.Instance.LOCAL.data)) || (DAO.Instance.LOCAL.data.Trim () == "[]"))
					GameManager.Instance.PromtAppNoInternet ();
				else
					DAO.Instance.LOCAL.dataVersion = DAO.Instance.LOCAL.dataVersion - 1;
				DAO.Log ("Ajax Error: " + ajax.error);
				//Debug.Log ("WaitForData() ---- end");
				if (onError != null)
					onError (ajax.error);
			}

		}
	}





	private IEnumerator WaitForVersion(){
		ajax = new WWW (VERSION_URL);
		while (ajax.isDone == false) {
			DAO.Log ("bytes downloaded: " + ajax.bytesDownloaded.ToString () + " Error: " + ajax.error);
			yield return null;
		}

		//yield return ajax;
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("GetVersion end");
		if (ajax.error == null) {
			DAO.Log ("Version recieved: " + ajax.text);
			if (onVersionRecieved != null)
				onVersionRecieved (ajax.text);
		} else {
			if (tryToGetVersionCount < 3) {
				tryToGetVersionCount++;
				if (UnityInitializer.Instance!=null)
					UnityInitializer.Instance.addToMonitorLog("GetVersion try number"+ tryToGetVersionCount.ToString());
				StartCoroutine (WaitForVersion ());
			} else {
				DAO.Log ("NO_INTERNET!!! ");
				DAO.Instance.NO_INTERNET = true;
				if ((DAO.Instance == null) || (DAO.Instance.LOCAL == null) || (DAO.Instance.LOCAL.dataVersion == 0) || (string.IsNullOrEmpty (DAO.Instance.LOCAL.data)) || (DAO.Instance.LOCAL.data.Trim () == "[]")) {
					DAO.Log ("NO_INTERNET on First Load!!! Plaese give Error message");
					GameManager.Instance.PromtAppNoInternet ();
				}
				/*
			if ((DAO.Instance == null) || (DAO.Instance.LOCAL == null) || (string.IsNullOrEmpty(DAO.Instance.LOCAL.data)) || (DAO.Instance.LOCAL.data.Trim() == "[]"))
		    {
				DAO.Log ("NO_INTERNET on First Load!!! Plaese give Error message");
				GameManager.Instance.PromtAppNoInternet ();
			}
			*/
				//Debug.Log ("WaitForVersion() ---- end");
				if (onError != null)
					onError (ajax.error);
				//DAO.Log ("Ajax Error: " + ajax.error);
			}
		}
	}

	private IEnumerator WaitForDataRuns(){
		ajax = new WWW(DATA_URL_RUNS);
		//yield return ajax;

		while(ajax.isDone == false){
			DAO.Log("bytes downloaded: "+ajax.bytesDownloaded.ToString() );
			yield return null;
		}
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("WaitForDataRuns end"+ tryToGetVersionCount.ToString());

		if (ajax.error == null) {
			DAO.Log ("Setup recieved from server: " + ajax.text);
			prepareDataRuns( ajax.text );
			//prepareDataRuns( LocalDataInterface.Instance.dataRuns );   // for debuging
			if (onDataRunsRecieved != null) onDataRunsRecieved (ajax.text);

		} else {
			if (tryToGetDataRuns < 2) {
				tryToGetDataRuns++;
				StartCoroutine (WaitForDataRuns ());
			} else {
				DAO.Log ("Ajax Error: " + ajax.error);
				Debug.Log ("WaitForDataRuns() ---- end");
				if (onError != null)
					onError (ajax.error);
			}
		}
	}




	public void GetVersion(){
		SetHosts ();
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("GetVersion start");
		VERSION_URL = VERSION_URL_BASE + "&bundle="+GameManager.BUNDLE_ID + "&lang="+DAO.AppLanguage;
		DAO.Log ("Sending Ajax for Version...");
		DAO.Log ("Version URL: " + VERSION_URL);
		StartCoroutine (WaitForVersion ());
	}

	public void GetData(){
		Debug.Log ("RON_____________________ServerDataInterface GetData");
		//Preloader.Instance.PrintStatus ("Getting Data");
		SetHosts ();
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("GetData start");
		DATA_URL = DATA_URL_BASE + "&bundle="+GameManager.BUNDLE_ID + "&lang="+DAO.AppLanguage;
		DAO.Log ("Sending Ajax for setup...");
		DAO.Log ("Data URL: " + DATA_URL);
		DAO.Log ("strat to download data" + DateTime.Now.ToString());
		StartCoroutine (WaitForData ());


	}

	public void GetDataRuns(){
		Debug.Log ("RON_____________________ServerDataInterface GetDataRuns");
		SetHosts ();
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("GetDataRuns start");
		//DATA_URL_RUNS = DATA_URL_RUNS;
		DAO.Log ("Sending Ajax for setup...");
		DAO.Log ("Data URL: " + DATA_URL_RUNS);
		DAO.Log ("strat to download RUNS data" + DateTime.Now.ToString());
		StartCoroutine (WaitForDataRuns ());


	}

	// =========================================================================

	public JSONNode strings;
	public JSONNode settings;
	public JSONNode cup_runs;
	public JSONNode cup_runs_easy;
	public JSONNode practice_runs;
	public JSONNode cups;
	public JSONNode perks;
	public JSONNode tutorial_run;
	public JSONNode test_run;

	public void prepareData(string data){
		DAO.Log("Parsing setup...");
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("Parsing setup start");
		if (!String.IsNullOrEmpty (data) && (data.Trim() !="[]")) {
			DATA = JSON.Parse (data);
			//----------------------------

			DAO.Log ("Parsing strings...");
			strings = DATA ["strings"];

			DAO.Log ("Parsing strings...");
			settings = DATA ["setings"];

			//cup_runs = DATA ["cup_runs"];

			//cup_runs_easy = DATA ["cup_runs_easy"];

			//practice_runs = DATA ["practice_runs"]; 
			//test_run = DATA ["test_run"];
			DAO.Log ("Parsing cups...");
			cups = DATA ["cups"];

			DAO.Log ("Parsing perks...");
			perks = DATA ["perks"];

			DAO.Log ("Parsing tutorial_run...");
			tutorial_run = DATA ["tutorial_run"];

			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("Parsing setup end");
		}

	}

	public void prepareDataRuns(string data){
		DAO.Log("Parsing setup runs..." );
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("Parsing prepareDataRuns start");
		if (!String.IsNullOrEmpty (data) && (data.Trim() !="[]")) {
			DATA = JSON.Parse (data);
			//----------------------------
			DAO.Log ("Parsing cup_runs...");
			cup_runs = DATA ["cup_runs"];
			DAO.Log ("Parsing cup_runs_easy...");
			cup_runs_easy = DATA ["cup_runs_easy"];
			DAO.Log ("Parsing practice_runs...");
			practice_runs = DATA ["practice_runs"]; 
			DAO.Log ("Parsing test_run...");
			test_run = DATA ["test_run"];
			DAO.Log ("Parsing finished runs...");
			if (UnityInitializer.Instance!=null)
				UnityInitializer.Instance.addToMonitorLog("Parsing prepareDataRuns end");
		}
	}





}
