﻿using UnityEngine;
using System.Collections;

public class BuildingCollider : MonoBehaviour {

	bool colided = false;

	public bool isCatedral = false;

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "Player" && !colided) {
			colided = true;
			PlayerController.instance.DeadlyCrush();

			if (isCatedral) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_gothic_close_encounter);
			}
		}
	}

}
