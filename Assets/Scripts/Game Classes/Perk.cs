using UnityEngine;
using System.Collections;

[System.Serializable]
public class Perk{

	public enum PerkType {
		NONE,
		JUMPER,
		MAGNETIC,
		MAN_OF_STEEL,
		GAMBLER,
		DODGER,
		EXELLENT_FITNESS,
		ENERGY_FIEND,
		SUPER_MAGNET,
		FANS_FAVORITE,
		MANAGER
	}

	public  Sprite jumperIcon;
	public  Sprite magneticIcon;
	public  Sprite manOfSteelIcon;
	public  Sprite gamblerIcon;
	public  Sprite dodgerIcon;
	public  Sprite exellentFitnessIcon;
	public  Sprite fansFavoriteIcon;
	public  Sprite managerIcon;

	public PerkType type;
	public int level;

	public Perk(string type, string level){

		switch (type) {
			case "NONE" 				: { this.type = PerkType.NONE; break; }
			case "JUMPER" 				: { this.type = PerkType.JUMPER; break; }
			case "MAGNETIC" 			: { this.type = PerkType.MAGNETIC; break; }
			case "MAN_OF_STEEL" 		: { this.type = PerkType.MAN_OF_STEEL; break; }
			case "GAMBLER" 				: { this.type = PerkType.GAMBLER; break; }
			case "DODGER" 				: { this.type = PerkType.DODGER; break; }
			case "EXELLENT_FITNESS" 	: { this.type = PerkType.EXELLENT_FITNESS; break; }
			case "ENERGY_FIEND" 		: { this.type = PerkType.ENERGY_FIEND; break; }
			case "SUPER_MAGNET" 		: { this.type = PerkType.SUPER_MAGNET; break; }
			case "FANS_FAVORITE" 		: { this.type = PerkType.FANS_FAVORITE; break; }
			case "MANAGER" 				: { this.type = PerkType.MANAGER; break; }
		}

		this.level = int.Parse(level);
	}

	// ------------------------------

	public string GetIcon(){
		return GetPerkIconByPerkType (type);
	}

	public Sprite GetIconByStringType(string type_str){
		switch (type_str) {
		case 	"JUMPER" 			: return jumperIcon;
		case 	"MAN_OF_STEEL" 		: return manOfSteelIcon;
		case 	"MAGNETIC" 			: return magneticIcon;
		case 	"EXELLENT_FITNESS" 	: return exellentFitnessIcon;
		case 	"FANS_FAVORITE" 	: return fansFavoriteIcon;
		case 	"MANAGER" 			: return managerIcon;
		case	"DODGER"			: return dodgerIcon;
		case 	"GAMBLER"			: return gamblerIcon;
			
//		case 	"ENERGY_FIEND" 		: return "R";
//		case 	"SUPER_MAGNET" 		: return "Y";
//		case 	"NONE" 				: return "";
		}

		return jumperIcon;
	}

	public static string GetPerkIconByPerkType(PerkType type){
		switch (type) {
			case PerkType.DODGER 			: return "Q";
			case PerkType.ENERGY_FIEND 		: return "R";
			case PerkType.EXELLENT_FITNESS 	: return "V";
			case PerkType.FANS_FAVORITE 	: return "U";
			case PerkType.GAMBLER 			: return "W";
			case PerkType.JUMPER 			: return "X";
			case PerkType.MAGNETIC 			: return "Z";
			case PerkType.MAN_OF_STEEL 		: return "1";
			case PerkType.MANAGER 			: return "0";
			case PerkType.SUPER_MAGNET 		: return "Y";
			case PerkType.NONE 				: return "D";
		}

		return "0";
	}

	public static string GetPerkIDByPerkType(PerkType type){
		switch (type) {
			case PerkType.DODGER 			: return "dodgr";
			case PerkType.ENERGY_FIEND 		: return "nrg";
			case PerkType.EXELLENT_FITNESS 	: return "ftns";
			case PerkType.FANS_FAVORITE 	: return "fansfav";
			case PerkType.GAMBLER 			: return "gamblr";
			case PerkType.JUMPER 			: return "jumper";
			case PerkType.MAGNETIC 			: return "magnetic";
			case PerkType.MAN_OF_STEEL 		: return "mos";
			case PerkType.MANAGER 			: return "mngr";
			case PerkType.SUPER_MAGNET 		: return "smagnet";
		}
		
		return "none";
	}

	public static PerkType GetPerkTypeByPerkID(string id){
		switch (id) {
			case "dodgr" 		: return PerkType.DODGER;
			case "nrg" 			: return PerkType.ENERGY_FIEND;
			case "ftns" 		: return PerkType.EXELLENT_FITNESS;
			case "fansfav" 		: return PerkType.FANS_FAVORITE;
			case "gamblr" 		: return PerkType.GAMBLER;
			case "jumper" 		: return PerkType.JUMPER;
			case "magnetic" 	: return PerkType.MAGNETIC;
			case "mos" 			: return PerkType.MAN_OF_STEEL;
			case "mngr" 		: return PerkType.MANAGER;
			case "smagnet" 		: return PerkType.SUPER_MAGNET;
		}

		return PerkType.NONE;
	}



}
