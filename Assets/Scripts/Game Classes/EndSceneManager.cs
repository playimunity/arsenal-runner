﻿using UnityEngine;
using System.Collections;
using System;

public class EndSceneManager : MonoBehaviour {


	public static EndSceneManager instance;

	public GameObject loosingScene;
	public Animator loosingAliansAn;

	public GameObject cannonScene;
	public Animator cannonAliansAn;
	public Animator cannonCannonAn;
	public Transform cannonPlayerLocation;

	public GameObject shootongScene;
	public Animator shootingAliansAn;
	public Animator shootingBallAn;
	public Transform shootingPlayerLocation;

	public event Action chosenWinEvent;

	int randomWinEvent;   // when entering the end section

	WaitForSeconds waitLoosingScene = new WaitForSeconds(4.5f);
	WaitForSeconds waitCannonScene = new WaitForSeconds(3.25f);
	WaitForSeconds waitShootingScene = new WaitForSeconds(3.25f);



	void Awake () {
		instance = this;
	}


	public void ActivateWinningEvent(){    // when entering the pre end sections in case of win
//		ActivateCannonScene ();
//		return;
		randomWinEvent = UnityEngine.Random.Range (0, 2);
		if (randomWinEvent == 0) {
			ActivateCannonScene ();
		} else {
			ActivateShootingScene ();
		}
	}

	public void ActivateCannonScene(){
		cannonScene.SetActive (true);
		loosingScene.SetActive (false);
		chosenWinEvent = PlayCannonScene;
	}

	public void PlayCannonScene(){
		cannonAliansAn.enabled = true;
		cannonCannonAn.enabled = true;
		PlayerController.instance.EndSceneCannonEvent (cannonPlayerLocation);
		StartCoroutine (WaitAndShowEndResults (waitCannonScene));
		PlayerController.instance.currentSpeed = 0f;
	}

	public void ActivateShootingScene(){
		shootongScene.SetActive (true);
		loosingScene.SetActive (false);
		chosenWinEvent = PlayShootingScene;
	}

	public void PlayShootingScene(){
		shootingAliansAn.enabled = true;
		shootingBallAn.enabled = true;
		PlayerController.instance.EndSceneShootingEvent (shootingPlayerLocation);
		StartCoroutine (WaitAndShowEndResults (waitShootingScene));
		PlayerController.instance.currentSpeed = 0f;
	}

	public void ActivateLoosingScene(){     // when entering the pre end sections in case of loose
		loosingScene.SetActive (true);
		chosenWinEvent = PlayLoosingScene;
	}

	public void PlayLoosingScene(){
		loosingAliansAn.enabled = true;
		loosingAliansAn.Play ("Loosing_Scene_Alian_Anim");
//		Debug.Log ("alon___________ should play loosing scene anim!!!");
		//PlayerController.instance.EndSceneShootingEvent (cannonPlayerLocation);
		StartCoroutine (WaitAndShowEndResults (waitLoosingScene));
		PlayerController.instance.currentSpeed = 0f;
	}


	IEnumerator WaitAndShowEndResults(WaitForSeconds waitTime){
		yield return waitTime;
		StartCoroutine (RunManager.instance.showEndRunResults());
	}


	public void ChosenWinningEvent(){
		chosenWinEvent ();
//		Debug.Log ("alon___________ should play Win scene anim!!!");
	}


	public void SetPosition(Transform pos){
		//gameObject.SetActive (true);
		transform.parent = pos;
		transform.position = Vector3.zero;
		transform.eulerAngles = Vector3.zero;
		//transform.position = pos.position;
		//transform.rotation = pos.rotation;
	}
}
