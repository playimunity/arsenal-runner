﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class languageDropDown : MonoBehaviour {
	
	string[] savedLanguages = new string[] {"SP", "CA", "FR", "DE", "RU", "PT", "KO", "ZH", "JP", "AR", "TR"};


	int currentLang = 0;

	public Dropdown dD;
	
	public List<Dropdown.OptionData> languages;

	void Start () {
		InitLanguages ();
	}


	void InitLanguages(){
		for (int i = 0; i < DAO.Settings.AvailableLanguages.Count; i++) {

			AddElement( ConvertLangNameToLangNick( DAO.Settings.AvailableLanguages[i] ) );
		}

		currentLang = GetLangIdByLangName (DAO.AppLanguage);
		dD.value = currentLang;

	}

	public void AddElement(string language){

		foreach (Dropdown.OptionData lang in languages) {
			if(lang.image.name == language) dD.options.Add (lang);
		}
	}


	public void OnLanguageChange(){
		if(currentLang == dD.value) return;

		SceneTransitionContainer.Instance.showTransition ();

		currentLang = dD.value;
		DAO.AppLanguage = ConvertLangNickToLangName ( dD.options[currentLang].image.name );

		AnimationEvents.OnSceneTransitionShownEvent += OnSceneTransitionShown;

		//DAO.Instance.SERVER.GetData ();

	}

	void OnSceneTransitionShown(){
		AnimationEvents.OnSceneTransitionShownEvent -= OnSceneTransitionShown;
		DAO.Instance.SERVER.GetData ();
	}

	string ConvertLangNickToLangName(string nick){
		switch (nick) {
			case "English":{return "English";}
			case "Spanish":{return "Spanish";}
			case "Catalonic":{return "Catalan";}
			case "French":{return "French";}
			case "German":{return "German";}
			case "Russian":{return "Russian";}
			case "Portuguesse":{return "Portuguese";}
			case "Korean":{return "Korean";}
			case "Chinese":{return "Chinese";}
			case "Japanese":{return "Japanese";}
			case "Arabic":{return "Arabic";}
			case "Turkish":{return "Turkish";}
			default : { return "English"; }
		}
	}

	string ConvertLangNameToLangNick(string name){
		switch (name) {
			case "English": { return "English"; }
			case "Spanish": { return "Spanish"; }
			case "Catalan": { return "Catalonic"; }
			case "French": { return "French"; }
			case "German": { return "German"; }
			case "Russian": { return "Russian"; }
			case "Portuguese": { return "Portuguesse"; }
			case "Korean": { return "Korean"; }
			case "Chinese": { return "Chinese"; }
			case "Japanese": { return "Japanese"; }
			case "Arabic": { return "Arabic"; }
			case "Turkish": { return "Turkish"; }

			default : {return "English";}
		}
	}

	int GetLangIdByLangName(string name){
		
		for (int i = 0; i < dD.options.Count; i++) {
			if (dD.options [i].image.name == ConvertLangNameToLangNick (name)) {
				return i;
			}
		}

		return 0;
	}

}
