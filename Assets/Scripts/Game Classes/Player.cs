using UnityEngine;
using System.Collections;
using System;
//using RootMotion.Dynamics;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public static Player instance;

	public PlayerData data;
	public event Action OnFitnessUpdate;

	public Animator playerAnimator;
    //public PuppetMaster puppet; 

	public GameObject PlayerHeadContainer;
	public Renderer PlayerRenderer;
	public List<Material> playerMaterials = new List<Material>();

	public int PlayerID;
	public Sprite Photo;
	public string playerName;
	public int playerNumber;
	public int placeInBench;

	GameObject MyHead;

	public PlayerStructure playerStructure;

	public bool selectedInBench = false;
	bool sitIdle = false;

	public Perk.PerkType DefaultPerk;

	string[] sitAnimations = {"sit_moving_soulders", "Player_Sit_fixHair_01", "Player_Sit_fixHair_02", "Player_Sit_NeckRelax_01", "Player_sit_NeckScratch"};

	public int standType;

	System.TimeSpan TimeSinceLastFitnessUpdate;
	WaitForSeconds waitASecond;

	public GameObject glove;

	int randomStandAnim;
	int randomSitAnim;
	int randomYesAnim;

	float randomFrame;
	float randomStandTime;
	float randomSitTime;

	public Transform SholderL;
	public Transform SholderR;
	public Transform neck;

	Vector3 vz;



	void Awake(){
		instance = this;
		waitASecond = new WaitForSeconds(DAO.Settings.SecondsFor1Fitness);

	}
	
	void OnDisable(){
		Save ();
	}

	public void Save(){
		DAO.SetPlayerData (data.PlayerID, data.mEncode ());
	}



	public void WalkIn(bool mirror){

		standType = 2;
		if (mirror)
			playerAnimator.SetBool ("walk in mirror", true);
		else
			playerAnimator.SetBool ("walk in mirror", false);
		
		//playerAnimator.SetTrigger ("walk in");
		playerAnimator.Play("walk in");
	}



//	public void StandUp(){
//		StopAllCoroutines();
//		//playerAnimator.SetTrigger ("stand up");
//		playerAnimator.CrossFade ("stand up" , 0.1f);
//	}

	public void Stand(){

		StopAllCoroutines();
		switch (standType) {
		case 1: // bench tap stand:
			{
				StopCoroutine ("StandRandomFeedback");
				playerAnimator.CrossFade("stand_idle" , 0.15f); 
				StartCoroutine ("StandRandomFeedback");
				PlayerChooseManager.instance.onAction = false;
				break;
			}
		case 2: // preview idle stand:
			{
				PlayerChooseManager.instance.OnPlayerStand ();
				Yesss ();
				StopCoroutine ("StandRandomFeedback");
				playerAnimator.SetTrigger("stand idle"); 
				StartCoroutine ("StandRandomFeedback");
				break;
			}
		case 3: // walk out stand:
			WalkOut(false);
			break;
		case 4: // Yess!:
			playerAnimator.CrossFade("player_stand_power_up" , 0.1f);
			playerAnimator.SetTrigger ("stand idle");
			break;
		case 5: // yess and walk out:
			Yesss();
			playerAnimator.SetTrigger ("walk out");
			break;
		case 6: // pre run stand idle:
			playerAnimator.CrossFade("stand_idle" , 0.08f); 
			StartCoroutine ("StandRandomFeedback");
			break;
		}
		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {
			PlayerChooseManager.instance.onAction = false;
		}
	}

	IEnumerator StandRandomFeedback(){

		randomStandTime = UnityEngine.Random.Range (7f, 17f);
		randomStandAnim = UnityEngine.Random.Range (0, 4);
		yield return new WaitForSeconds (randomStandTime);
		if (randomStandAnim == 0)	playerAnimator.SetFloat ("random stand" , 0);
		else if (randomStandAnim == 1)	playerAnimator.SetFloat ("random stand" , 1);
		else if (randomStandAnim == 2)	playerAnimator.SetFloat ("random stand" , 2);
		else if (randomStandAnim == 3)	playerAnimator.SetFloat ("random stand" , 3);
		playerAnimator.CrossFade ("Stand Idles" , 0.15f);
		StartCoroutine ("StandRandomFeedback");
	}

	public void Yesss(){

		StopCoroutine ("StandRandomFeedback");

		if (playerName == "Luis Suarez") {
			playerAnimator.SetFloat ("positive feedback", 2f);
		}
		else if (playerName == "Lionel Messi") {
			playerAnimator.SetFloat ("positive feedback", 1f);
		}
		else if (playerName == "Neymar Junior") {
			playerAnimator.SetFloat ("positive feedback", 0f);
		}
		else {
			randomYesAnim = UnityEngine.Random.Range (1, 7);

			switch (randomYesAnim) {
			case 1:
				{
					playerAnimator.SetFloat ("positive feedback", 3f);
					break;
				}
			case 2:
				{
					playerAnimator.SetFloat ("positive feedback", 4f);
					break;
				}
			case 3:
				{
					playerAnimator.SetFloat ("positive feedback", 5f);
					break;
				}
			case 4:
				{
					playerAnimator.SetFloat ("positive feedback", 6f);
					break;
				}
			case 5:
				{
					playerAnimator.SetFloat ("positive feedback", 7f);
					break;
				}
			case 6:
				{
					playerAnimator.SetFloat ("positive feedback", 8f);
					break;
				}
			default :
				{
					playerAnimator.SetFloat ("positive feedback", 7f);
					break;
				}
			}
		}
		playerAnimator.Play ("positive feedback");
	}


//	public void Sit(bool instance){
//
//		if (instance)
//			playerAnimator.Play ("sit down", 0 , 0.8f);
//		else
//			playerAnimator.SetTrigger ("sit down");
//
//		//UpdateFitness (true);
//	}

//	public void SitIdle(){
//
//		StopCoroutine ("StandRandomFeedback");
//		randomSitAnim = UnityEngine.Random.Range (1, 3);
//		randomFrame = UnityEngine.Random.Range (0f, 1.0f);
//
//		switch (data.curHealthState) {
//		case PlayerData.HealthState.ENERGETIC:
//		case PlayerData.HealthState.IDEAL:
//			{
//				StopCoroutine ("SitIdlesAnimations");
//				playerAnimator.CrossFade ("sit_idle1", 0.2f, 0, randomFrame);
//				StartCoroutine ("SitIdlesAnimations");
//				break;
//			}
//		
//		case PlayerData.HealthState.TIERED:
//			{
//				if (randomSitAnim == 1)
//					playerAnimator.CrossFade ("sit_hurt1", 0.2f, 0, randomFrame);
//				else
//					playerAnimator.CrossFade ("sit_hurt2", 0.2f, 0, randomFrame);
//				break;
//			}
//
//		case PlayerData.HealthState.WOUNDED:
//			{
//				playerAnimator.CrossFade ("sit_exhausted", 0.2f, 0, randomFrame);
//
//				break;
//			}
//		}
//	}



//	IEnumerator SitIdlesAnimations(){
//
//		randomSitTime = UnityEngine.Random.Range (7.0f, 17.0f);
//		randomSitAnim = UnityEngine.Random.Range (1, sitAnimations.Length);
//
//		yield return new WaitForSeconds (randomSitTime);
//
//		playerAnimator.CrossFade (sitAnimations[randomSitAnim], 0.1f, 0);
//
//		StartCoroutine ("SitIdlesAnimations");
//	}



	public void WalkOut(bool mirror){
		StopAllCoroutines();

		if (mirror)
			playerAnimator.SetBool ("walk out mirror", true);
		else
			playerAnimator.SetBool ("walk out mirror", false);

		playerAnimator.CrossFade ("walk out" , 0.1f);
	}

//	public void StandAndSit(bool stand){
//
//		if (stand) {
//			playerAnimator.SetTrigger ("stand up");
//		}else{
//			playerAnimator.SetTrigger ("sit down");
//		}
//	}

	public void ForceFitnessUpdate(){
		UpdateFitness ();
	}

	public void UpdateFitness(){

		int fitnessToAdd = MainScreenUiManager.instance.energyToAdd;

		if (fitnessToAdd > 0) {
			data.AddFitnes (fitnessToAdd, true);

			Save ();

			if (OnFitnessUpdate != null) OnFitnessUpdate ();

		}
	}


	public int GetTimeToNextFitnessUpdate(){

		return DAO.Settings.SecondsFor1Fitness - (int)(DateTime.Now - data.LastFitnessUpdate).TotalSeconds;

	}

	public void InvokeFitnessUpdate(){
		if (OnFitnessUpdate != null) OnFitnessUpdate();
		Save ();
	}
	

	public void ShowGlove(){
		glove.SetActive (true);
	}

	public void HideGlove(){
		glove.SetActive (false);
	}



	public void SetPlayerStructure(int playerId){
		
		playerStructure = PrefabManager.instanse.PlayerStructures [playerId];

		// Set Texsture
		if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
			PlayerRenderer.materials [0].mainTexture = playerStructure.PlayerTrainingTexture;
			PlayerRenderer.materials [1].mainTexture = playerStructure.ClothTrainingTexture;
		} else {
			PlayerRenderer.materials [0].mainTexture = playerStructure.PlayerTexture;
			PlayerRenderer.materials [1].mainTexture = playerStructure.ClothTexture;
		}

		// Set Head
		MyHead = (GameObject)Instantiate (playerStructure.PlayerHead, transform.position, transform.rotation);
		MyHead.transform.parent = PlayerHeadContainer.transform;

		// Set Other Data

		PlayerID = playerStructure.PlayerID;
		Photo = playerStructure.PlayerPhoto;
		playerName = playerStructure.PlayerName;
		playerNumber = playerStructure.PlayerNumber;
		DefaultPerk = playerStructure.DefaultPerk;

		data = new PlayerData(PlayerID, this);

	}



	public void StopAllCoroutins(){
		StopAllCoroutins ();
	}


	Color emissionColor;
	float emissionColorFloat;

	public void PlayerGlow(float time){
		StartCoroutine (PlayerGlowCoroutine (time));
	}

	static WaitForEndOfFrame _weof;
	static WaitForEndOfFrame weof{
		get{
			if (_weof == null)
				_weof = new WaitForEndOfFrame ();

			return _weof;
		}
	}

	IEnumerator PlayerGlowCoroutine(float time){
		playerMaterials [1].EnableKeyword("_EmissionColor");

		yield return new WaitForSeconds (time);

		emissionColor = playerMaterials[1].GetColor("_EmissionColor");
		emissionColorFloat = emissionColor.b;
		while(emissionColorFloat < 1f){
			emissionColorFloat += 0.05f;
			emissionColor.b = emissionColorFloat;

			playerMaterials [1].SetColor ("_EmissionColor", emissionColor);
			yield return weof;
		}

		yield return new WaitForSeconds (time);
	}

//	bool fastWAlkSpeed = false;
//	public void ChangeWalkSpeed(bool on){
//		if (on && !fastWAlkSpeed) {
//			playerAnimator.SetFloat ("walk in speed", 1.3f);
//			fastWAlkSpeed = true;
//		} else if (!on && fastWAlkSpeed) {
//			playerAnimator.SetFloat ("walk in speed", 1f);
//			fastWAlkSpeed = false;
//		}
//	}

	public void ChangeWalkSpeed(bool on){
		if (on) {
			playerAnimator.SetFloat ("walk in speed", 1.3f);
		} else {
			playerAnimator.SetFloat ("walk in speed", 1f);
		}
	}




}
