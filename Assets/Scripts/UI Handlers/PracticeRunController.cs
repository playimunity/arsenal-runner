using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using System.IO;
using Facebook.Unity;
using System.Collections.Generic;

public class PracticeRunController : MonoBehaviour
{

	#region SAVEME UI THINGS

	public GameObject SaveMeCoinsContainer;
	public GameObject SaveMeAdsContainer;
	public Text SaveMeAdCoinsBtnTxt;

	#endregion


	public static PracticeRunController Instance;
	public Animator an;
	public int CoinsCollected;

	//public GameObject countDownUI;
	public Text RetryPrice;
	public float StartTime;
	int saveMeCounterIndex = 0;
	int adSaveMeCounterIndex = 0;

	public Camera followCam;

	public GameObject debugParent;

	bool countingDown = false;


	void Awake ()
	{
		Instance = this;
		CoinsCollected = 0;
		saveMeCounterIndex = 0;
		GM_Input._.PractisePause = false;

		if (DAO.IsInDebugMode) {
			debugParent.SetActive (true);
		}
	}

	public void Pause ()
	{
		if (GameManager.Instance.runPaused)
			return;

		//UIUpdater_PracticeRun.Instance.SetPauseScreen ();
		UIUpdater_PracticeRun.Instance.SetLooseScreen (true);
		GameManager.RequestPause ();
		an.Play ("PracticeRun_Loose_Puase_in", 1);

		GM_Input._.PractisePause = true;
	}

	public void UnpauseCountDown ()
	{
		if (countingDown) {
			return;
		}
		countingDown = true;
		//GameManager.RequestUnpause ();
		//countDownUI.SetActive(true);
		//GM_Input._.PractisePause = false;
		an.Play ("PracticeRun_Loose_Puase_out", 1);
		an.Play ("pause_count_down", 2, 0f);
	}

	public void UnPause ()
	{
		countingDown = false;
		GameManager.RequestUnpause ();

		GM_Input._.PractisePause = false;
	}

	public void StopRun ()
	{
		an.Play ("PracticeRun_Loose_Puase_out", 1);

		OnPracticeEnded ();
		GM_Input._.PractisePause = false;
		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
	}

	public void GoToLockerRoom ()
	{
		if (countingDown) {
			return;
		}
		GM_Input._.PracticeLoose = false;
		an.Play ("PracticeRun_Loose_Puase_out", 1);

		OnPracticeEnded ();

		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
	}

	public void ShowLooseScreen ()
	{
		GM_Input._.PracticeLoose = true;

		UIUpdater_PracticeRun.Instance.SetLooseScreen ();
		AudioManager.Instance.OnGameLost ();
		an.Play ("PracticeRun_Loose_Puase_in", 1);
		saveMeCounterIndex = 0;

	}

	public void HideLooseScreen ()
	{
		GM_Input._.PracticeLoose = false;
		//UIUpdater_PracticeRun.Instance.SetLooseScreen ();
		an.Play ("PracticeRun_Loose_Puase_out", 1);
	}


	public void RestartRun ()
	{
		saveMeCounterIndex = 0;
		GM_Input._.PracticeLoose = false;
		UnitedAnalytics.LogEvent ("Practice Started", "From Retry", UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers);
		GameManager.SwitchState (GameManager.GameState.INFINITY_RUN);
		DAO.PlayPracticePopup = 1;
	}


	public void RunUiAnimToggle (bool isOn)
	{
		if (isOn) {
			an.Play ("PrecticeRunUi_in");
		} else {
			an.Play ("PrecticeRunUi_out");
		}
	}

	public void RunBtnAnimToggle (bool isOn)
	{
		if (isOn) {
			an.Play ("PrecticeRunBtn_in");
		} else {
			an.Play ("PrecticeRunBtn_out");
		}
	}


	void OnApplicationFocus (bool focusStatus)
	{
		#if !UNITY_EDITOR
		if (!GameManager.Paused && !PlayerController.instance.isDead) {
		Pause ();
		}
		#endif
	}


	public void OnPracticeStarted ()
	{
		StartTime = Time.realtimeSinceStartup;
		saveMeCounterIndex = 0;

	}

	public void OnPracticeEnded ()
	{
		//if (GameManager.Instance.isPlayscapeLoaded) {
			//BI._.PracticeRun_Completed (RunManager.totalMeters, PracticeRunController.Instance.CoinsCollected);
		//	BI._.Wallet_Deposit (PracticeRunController.Instance.CoinsCollected, "Practice Run Ended", "FCB Coins", "", "", "");
		GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,PracticeRunController.Instance.CoinsCollected, GameAnalyticsWrapper.REWARD,"RunCompleted");
		//}
		DAO.TotalPracticeTime += (int)(Time.realtimeSinceStartup - StartTime);
		GameManager.Instance.HandleRankScoreChange ();
		DAO.PlayPracticePopup = 1;
	}




	bool newHighScore = false;
	public GameObject newHighScoreTxt;

	public void LevelUp ()
	{
		an.Play ("PracticeRun_LevelUp", 0 , 0f);
		playerGlow.SetActive (true);
		AudioManager.Instance.PlayerWoohoo ();
	}

	public void NewHighScore ()
	{
		playerGlow.SetActive (true);
		newHighScore = true;
	}

	public GameObject playerGlow;
	public RectTransform levelUpFXParent;
	public RectTransform levelUpIcon1;
	public RectTransform levelUpIcon2;
	public RectTransform levelUpIcon3;
	public GameObject levelUpGlow;
	public Transform playerTransform;
	public RectTransform iconEndPosition1;
	public RectTransform iconEndPosition2;
	public RectTransform iconEndPosition3;
	WaitForEndOfFrame _waitEndFrame = new WaitForEndOfFrame ();
	Vector2 iconStartPos;

	public void LevelUpIconsAnim ()
	{
		if (newHighScore) {
			StartCoroutine (HighScoreFxCoroutine ());
		} else {
			StartCoroutine (LevelUpIconsCoroutine ());
		}
	}

	IEnumerator LevelUpIconsCoroutine ()
	{
		//levelUpFXParent.position = followCam.WorldToScreenPoint (playerTransform.position);
		levelUpGlow.SetActive (true);
//		levelUpIcon1.gameObject.SetActive (true);
//		levelUpIcon2.gameObject.SetActive (true);
//		levelUpIcon3.gameObject.SetActive (true);
//		iconStartPos = levelUpIcon1.position;

		levelUpFXParent.position = followCam.WorldToScreenPoint (playerTransform.position);

//		while (Vector2.Distance (levelUpIcon1.position, iconEndPosition1.position) > 2f) {
//			levelUpIcon1.position = Vector2.Lerp (levelUpIcon1.position, iconEndPosition1.position, 4f * Time.deltaTime);
//			levelUpIcon2.position = Vector2.Lerp (levelUpIcon2.position, iconEndPosition2.position, 5f * Time.deltaTime);
//			levelUpIcon3.position = Vector2.Lerp (levelUpIcon3.position, iconEndPosition3.position, 6f * Time.deltaTime);
//			yield return _waitEndFrame;
//		}
//		levelUpIcon1.gameObject.SetActive (false);
//		levelUpIcon2.gameObject.SetActive (false);
//		levelUpIcon3.gameObject.SetActive (false);
//		levelUpIcon1.position = iconStartPos;
//		levelUpIcon2.position = iconStartPos;
//		levelUpIcon3.position = iconStartPos;
		yield return new WaitForSeconds (1f);
		levelUpGlow.SetActive (false);
		playerGlow.SetActive (false);
		//yield return null;
	}

	public Color sparksColor;

	IEnumerator HighScoreFxCoroutine ()
	{
		levelUpFXParent.position = Camera.current.WorldToScreenPoint (playerTransform.position);
		levelUpGlow.SetActive (true);
		newHighScoreTxt.SetActive (true);
		//CrossSceneUIHandler.Instance.SparklesFX (1f, newHighScoreTxt.transform.position, sparksColor , 30 , 250, 250, false);
		yield return new WaitForSeconds (2);

		levelUpGlow.SetActive (false);
		playerGlow.SetActive (false);
		newHighScoreTxt.SetActive (false);
		newHighScore = false;
	}


	public void RedFrameToggle ()
	{
		an.Play ("red_frame", 3, 0f);
	}



	bool grab = true;

	Texture2D shareTexture;

	//public RawImage tempRawImage;

	byte[] savedImage;

	string title = "FCB-Ultimate Rush Endless Mode!";
	string subject = "FCB-Ultimate Rush Endless Mode!";
	string text = "http://www.gamehour.com/";

	string destination;

	WaitForEndOfFrame wairForEndFrame = new WaitForEndOfFrame ();

	public Diffusion diffusion;

	public void Login ()
	{
		FB.LogInWithPublishPermissions (new List<string> () { "publish_actions" }, LoginCallback);
		FB.ActivateApp ();
	}

	public void LoginCallback (ILoginResult result)
	{
		if (FB.IsLoggedIn) {
			FBModule.Instance.LoginCallback (result);
			StartCoroutine (ShareCoroutine ());
		}
	}

	public IEnumerator ShareOnFacebook ()
	{
		string captionToPost = "Look at my stats on FC Barcelona Ultimate Rush! " + DAO.Instance.stroeAppLink;
		yield return new WaitForEndOfFrame ();
		var wwwForm = new WWWForm ();
		wwwForm.AddBinaryData ("image", savedImage, "fcb-team.jpg");
		wwwForm.AddField ("caption", captionToPost);
		FB.API ("me/photos", HttpMethod.POST, SharedCallback, wwwForm);
//		File.WriteAllBytes (Path.Combine (Application.persistentDataPath, "Share.png"),snapshot);
//		print (Application.persistentDataPath);
		//FBModule.Instance.ShareLink ("http://www.gamehour.com/", "test", "testSUB", "file://"+Path.Combine (Application.persistentDataPath, "Share.png"), "");
		yield return new WaitForEndOfFrame ();
	}

	private void SharedCallback (IGraphResult result)
	{
		print (result.ToString ());
		CrossSceneUIHandler.Instance.ShowNotification( "Shared On Facebook!", 3f );
	}

	public void Share ()
	{
		//Login ();
		StartCoroutine(ShareCoroutine ());
	}

	IEnumerator ShareCoroutine ()
	{
		yield return wairForEndFrame;

		if (grab) {
			shareTexture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
			shareTexture.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
			shareTexture.Apply ();
			savedImage = shareTexture.EncodeToJPG ();
			//tempRawImage.texture = shareTexture;
			grab = false;
			//StartCoroutine (ShareOnFacebook ());
		}

		destination = Path.Combine (Application.persistentDataPath, "fcb-gameover-scrn.jpg");
		File.WriteAllBytes (destination, savedImage);


		#if UNITY_ANDROID && !UNITY_EDITOR

		// block to open the file and share it ------------START
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent"); // yes
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");  // yes
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));  // yes
		//intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND_MULTIPLE"));  // test

		// transfer text :
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), title);  // yes
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text);  // yes
		intentObject.Call<AndroidJavaObject> ("setType", "text/plain");  // yes

		// transfer data :
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse","file://" + destination);

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

		intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");  // yes
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");  // yes
		currentActivity.Call("startActivity", intentObject);  // yes

		#elif  UNITY_IPHONE && !UNITY_EDITOR

		diffusion.Share (text, destination);

		#endif

	}

	#region SaveMe

	public GameObject countDownParent;
	public Button saveMeBtn;
	public Button saveMeBtn2;
	public bool running = false;
	WaitForSeconds SMPBI = new WaitForSeconds (0.5f);
	float SMPB_TIME = 5f;
	float SMPB_MIN = 111f;
	float SMPB_MAX = 880f;
	float SMPB_STTEP = 0f;
	Vector2 SMTMPV2;
	public RectTransform SaveMeBar;
	bool SaveMeClicked = false;
	bool SaveMeCounterActive = false;

	public void ShowSaveMeScreen(){
		GM_Input._.SaveMePractice = true;
	
		int saveMePrice = DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.ENDLESS);
		RetryPrice.text = saveMePrice.ToString ();
		// If the user can still have a save me chance.
		if (saveMePrice != -1) {
			//if the number of times the user watched an ad to save itself is between the allowed range.
			if (adSaveMeCounterIndex <= LocalDataInterface.Instance.numberOfAdsForSaveMeEndless) {
				//if the usertype is allowed to watch an ad.
				if (HeyzapWrapper.AreAdsAllowedForUser (HeyzapWrapper.AdType.SaveMe)) {
					SaveMeAdCoinsBtnTxt.text = DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.ENDLESS).ToString ();
					SaveMeAdsContainer.SetActive (HeyzapWrapper.IsAvailable);
					SaveMeCoinsContainer.SetActive (!HeyzapWrapper.IsAvailable);
				}
			//if the usertype is not allowed to watch an ad.
			 else {
					SaveMeAdsContainer.SetActive (false);
					SaveMeCoinsContainer.SetActive (true);
				}
			}
			//if the number of times the user watched an ad to save itself is over the allowed range.
			else {
				
				SaveMeAdsContainer.SetActive (false);
				SaveMeCoinsContainer.SetActive (true);
			}
		} else {
			ShowLooseScreen ();
			return;
		}
		saveMeBtn.enabled = true;
		saveMeBtn2.enabled = true;
		an.Play ("PracticeRun_SaveMe_Show");
		SetAndStartSaveMeTimer ();
	}

	public bool saveMeTimerIsOn = false;

	public void SetAndStartSaveMeTimer ()
	{
		if (saveMeTimerIsOn)
			return;

		saveMeTimerIsOn = true;
		SMPB_STTEP = (float)((SMPB_MAX - SMPB_MIN) / (SMPB_TIME / 0.5));

		SMTMPV2 = SaveMeBar.sizeDelta;
		SMTMPV2.x = SMPB_MAX;
		SaveMeBar.sizeDelta = SMTMPV2;

		SaveMeCounterActive = true;
		SaveMeClicked = false;

		StartCoroutine (PlaySaveMeSound ());

		StartCoroutine (SaveMePBTimer ());
	}

	//	public RectTransform SaveMeBar;

	IEnumerator SaveMePBTimer ()
	{
//		yield return SMPBI;
//		SMTMPV2 = SaveMeBar.sizeDelta;
//		SMTMPV2.x -= SMPB_STTEP;
//		if (SMTMPV2.x < SMPB_MIN && !SaveMeClicked) {
//			SMTMPV2.x = SMPB_MIN;
//			SaveMeCounterActive = false;
//			saveMeTimerIsOn = false;
//			saveMeBtn.enabled = false;
//			saveMeBtn2.enabled = false;
//			HideSaveMeScreen ();
//		}
//		SaveMeBar.sizeDelta = SMTMPV2;
//		if (SaveMeCounterActive && !SaveMeClicked)
//			StartCoroutine (SaveMePBTimer ());


		SMTMPV2 = SaveMeBar.sizeDelta;
		while (SaveMeCounterActive && !SaveMeClicked) {
			yield return SMPBI;
			SMTMPV2.x -= SMPB_STTEP;
			if (SMTMPV2.x < SMPB_MIN && !SaveMeClicked) {
				SMTMPV2.x = SMPB_MIN;
				SaveMeCounterActive = false;
				saveMeTimerIsOn = false;
				saveMeBtn.enabled = false;
				saveMeBtn2.enabled = false;
				HideSaveMeScreen ();
			}
			SaveMeBar.sizeDelta = SMTMPV2;
		}
	}

	public void HideSaveMeScreen (bool declined = true)
	{
		AudioManager.Instance.StopWatch (false);
		GM_Input._.SaveMePractice = false;
		an.Play ("PracticeRun_SaveMe_Hide");
		SaveMeCounterActive = false;

		if (declined) {
			//RunManager.instance.OnSaveMeDeclined ();
			ShowLooseScreen ();
		}
	}

	IEnumerator PlaySaveMeSound ()
	{
		yield return new WaitForSeconds (0.15f);
		AudioManager.Instance.StopWatch (true);
	}

	public void SaveMeByAd ()
	{
        SaveMeCounterActive = false;
		
		#if UNITY_EDITOR
		SaveMe (true);
		HeyzapWrapper.CallBackAdditive();
		adSaveMeCounterIndex++;
		#endif
		HeyzapWrapper.listener = delegate(string adState, string adTag) {
			if (adState.Equals ("incentivized_result_complete")) {
			SaveMe (true);
			HeyzapWrapper.CallBackAdditive();
				adSaveMeCounterIndex++;
			}
			if (adState.Equals ("incentivized_result_incomplete")) {
				// The user did not watch the entire video and should not be given a   reward.
			}
		};
		HeyzapWrapper.SetListener ();
        AudioManager.Instance.StopWatch(false);
		HeyzapWrapper.Show ();
	}

	public void SaveMeBtnClicked ()
	{
		SaveMe ();
	}

	public void SaveMe (bool isByAd = false)
	{
		if (!isByAd) {
		int saveMePrice = DAO.getSaveMePricesByIndex (saveMeCounterIndex, DAO.SaveMePriceTag.ENDLESS);
		if (saveMePrice > 0) {
			if (DAO.TotalCoinsCollected < saveMePrice) {
				CrossSceneUIHandler.Instance.showStore (CrossSceneUIHandler.ShopStates.COINS);
				SaveMeCounterActive = false;
				return;
			}

		
				DAO.TotalCoinsCollected -= saveMePrice;

				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Withdraw (DAO.Settings.RetryRunCost, "Retry Used", "Retry Used", "", "", "");
				GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, saveMePrice, GameAnalyticsWrapper.SAVE_ME, "SaveMe");
				saveMeCounterIndex++;
				CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
			}

		}

		//UnitedAnalytics.LogEvent ("Save Me used", UserData.Instance.userType.ToString (), GameManager.ActiveCup.name, GameManager.CurrentRun.id );

		//Run.Instance.runGoal.ResetAll();
		//GameManager.SwitchState (GameManager.GameState.CUP_RUN);

		// ----

		HideSaveMeScreen (false);

		//Run.Instance.runGoal.playerDie = false;
		//Run.Instance.runGoal.hits = 0;
		//UIUpdater_cupRun.instance.ResetPBOnSaveMe ();
		SaveMeClicked = true;
		countDownParent.SetActive (true);
		PlayerController.instance.Respown ();
		an.Play ("count_down_practice_saveme", 2, 0f);
		countingDown = true;
		SMTMPV2.x = SMPB_MIN;
		SaveMeCounterActive = false;
		saveMeTimerIsOn = false;
		saveMeBtn.enabled = false;
		saveMeBtn2.enabled = false;
		//SaveMeClicked = false;
		//saveMeCounterIndex++;
	}

	public void SaveMeCountDownEvent ()
	{
		PlayerController.instance.StartRunAfterDie ();
		countingDown = false;
	}

	#endregion



}