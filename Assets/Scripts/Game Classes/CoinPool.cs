﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CoinPool : MonoBehaviour {

	int INSTANTIATE_COINS = 100;

	public static CoinPool _;
	public List<GameObject> AvailableCoins;



	GameObject goz;

	void Awake () {
		_ = this;
		InstantiateFirstCoins ();
	}

	void InstantiateFirstCoins(){
		AvailableCoins = new List<GameObject> ();

		for (int i = 0; i < INSTANTIATE_COINS; i++) {
			AvailableCoins.Add ( InstantiateCoin () );
		}

	}

	GameObject InstantiateCoin(){
		return (GameObject)Instantiate (PrefabManager.instanse.coin , Vector3.zero, Quaternion.identity );
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------

	public GameObject GetCoin(){

		if (AvailableCoins.Count > 0) {
			goz = AvailableCoins [0];
			goz.SetActive (true);
			AvailableCoins.RemoveAt (0);

			return goz;

		} else {
			goz = InstantiateCoin ();
			goz.SetActive (true);
			return goz;
		}	
	}

	public void ReturnCoin(GameObject coin){
		coin.SetActive (false);
		AvailableCoins.Add (coin);
	}

}
