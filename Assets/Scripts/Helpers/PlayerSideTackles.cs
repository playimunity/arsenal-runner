﻿using UnityEngine;
using System.Collections;

public class PlayerSideTackles : MonoBehaviour {

	public static PlayerSideTackles instance;

	bool colided = false;

	PlayerController playerControllerScript;
	
	void Awake () {
		instance = this;
		playerControllerScript = GetComponentInParent<PlayerController> ();
	}
	
	void OnTriggerEnter(Collider other) {

		if (!colided && (other.tag == "Obstacle" || other.tag == "Moving Obstacle")) {

			colided = true;
			playerControllerScript.SideTackle();
			StartCoroutine (BackToFalse());
		}
	}

	IEnumerator BackToFalse(){
		yield return new WaitForSeconds (0.05f);
		colided = false;
	}
}
