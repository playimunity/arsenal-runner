﻿using UnityEngine;
using System.Collections;

public class ParticleCoinGenerator : MonoBehaviour {

	public static ParticleCoinGenerator instance;

	public ParticleSystem particleCoin;

	bool active = false;


	void Awake(){
		instance = this;
	}


	void Update(){
		if (active) {
			transform.Translate(Vector3.forward * 3f * Time.deltaTime);
		}
	}


	public void PlayParticle(Vector3 pos , Vector3 rot){
		//pos.y += 1f;
		transform.position = pos;
		transform.eulerAngles = rot;
		active = true;
		particleCoin.gameObject.SetActive (true);
		particleCoin.Play ();
		StartCoroutine (WaitForDeactivate());
	}


	static WaitForSeconds _wfs;
	static WaitForSeconds wfs{
		get{
			if (_wfs == null)
				_wfs = new WaitForSeconds (1f);

			return _wfs;
		}
	}


	IEnumerator WaitForDeactivate(){
		yield return wfs;
		particleCoin.Stop ();
		particleCoin.gameObject.SetActive (false);
		active = false;
	}
	

}
