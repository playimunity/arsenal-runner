using UnityEngine;
using System.Collections;
using OnePF;
using System.Collections.Generic;
using System;
using Amazon;
using SimpleJSON;

public class IAP : MonoBehaviour {

	public static IAP Instance;
	public static bool IsReady = false;

	bool LOCKED = false;

	public List<AdCTA> AdModuleCallBacks;
	public static event Action<string> AdditionalIAPCallback;

	public static int Pack_1_Value; 			// Provided by DAO
	public static int Pack_2_Value; 			// Provided by DAO
	public static int Pack_3_Value; 			// Provided by DAO
	public static int Pack_4_Value; 			// Provided by DAO
	public static int Pack_5_Value; 			// Provided by DAO
	public static int Pack_6_Value; 			// Provided by DAO
    public static int Pack_starter_OldValue;    // Provided by DAO
	public static int Pack_starter_Value;		// Provided by DAO
	public static int Pack_fcb_Value;			// Provided by DAO
	public static int Pack_fcbmega_Value;		// Provided by DAO

    public const string GOOGLE_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0Kmh3loGroTxpzZwx0lLZZMq6s0bu4/6ubKsR2SyErrPa6fKJlnMatIn5zSqHFDWELxN/9eVrEDWZIQ+GzFcatQvz+eb9+ewk5mzkjJDW3gQdqVgA+ayi8z0XOk1euWfHEeqoiOPw364PZ1Zr7b82FCH+Qoezs98uvwvPjGFMjluVAaUmCndLEdvf+mNYkuJDcUuoN6ZRbM/aXdLm7Q3FO4dlqE4Aea58yf/hohY+6zNRz8VKo14UvWgnLe5hVRbseweQMHvL9wg8Wro3OIw64SjayKZwp5JLG46rZtGca06owF/uqwZJeOJmvrAtTcF2cm7G//rJm4xNchpa6y5lQIDAQAB";

	// SKUS
	#if UNITY_ANDROID

    public const string SKU_STARTER_DEAL        = "deal1";
	public const string SKU_FCB_MEGA_PACK 		= "deal2";   
	public const string SKU_QUICK_FIX			= "pack1";
	public const string SKU_LOCAL_CLUB			= "pack2";
	public const string SKU_NATIONAL_FUND		= "pack3";
	public const string SKU_WORLD_DOMINATOR		= "pack4";
	public const string SKU_FCB_PRESIDENT		= "pack5";
	public const string SKU_CHAMPIONS_PACK		= "pack6";
//    public const string SKU_FCB_PACK            = "fcbarca_pack";
//
//	public const string SKU_LEGEND_RONALDINHO	= "legend_ronaldinho";
//	public const string SKU_LEGEND_PUYOL		= "legend_puyol";
//	public const string SKU_LEGEND_PEP			= "legend_guardiola";
//	public const string SKU_LEGEND_STOICHKOV	= "legend_stoichkov";
//	public const string SKU_LEGEND_RIVALDO		= "legend_rivaldo";
//	public const string SKU_LEGEND_KOEMAN		= "legend_koeman";
//
//	public const string SKU_STARTER_PACK		= "euro001";
//	public const string SKU_FCB_MEGA_DEAL		= "euro002";
//	public const string SKU_COLLECTOR_PACK		= "euro003";
//	public const string SKU_EURO_PACK			= "euro004";
//	public const string SKU_TEN_GIFT_CARDS		= "scr001";
//
//	public const string SKU_DEAL_1				= "deal1";
//	public const string SKU_DEAL_2				= "deal2";
//	public const string SKU_DEAL_3				= "deal3";
//	public const string SKU_DEAL_4				= "deal4";
//	public const string SKU_DEAL_5				= "deal5";
//	public const string SKU_DEAL_6				= "deal6";
//	public const string SKU_DEAL_7				= "deal7";
//	public const string SKU_DEAL_8				= "deal8";
//	public const string SKU_DEAL_9				= "deal9";
//	public const string SKU_DEAL_10				= "deal10";

	#else

    public const string SKU_STARTER_DEAL        = "1deal";
    public const string SKU_FCB_MEGA_PACK       = "2deal";   
    public const string SKU_QUICK_FIX           = "pack1";
    public const string SKU_LOCAL_CLUB          = "pack2";
    public const string SKU_NATIONAL_FUND       = "pack3";
    public const string SKU_WORLD_DOMINATOR     = "pack4";
    public const string SKU_FCB_PRESIDENT       = "pack5";
    public const string SKU_CHAMPIONS_PACK      = "pack6";

//	public const string SKU_LEGEND_RONALDINHO	= "legend_ronaldinho";
//	public const string SKU_LEGEND_PUYOL		= "legend_puyol";
//	public const string SKU_LEGEND_PEP			= "legend_pep";
//	public const string SKU_LEGEND_STOICHKOV	= "legend_stoichkov";
//	public const string SKU_LEGEND_RIVALDO		= "legend_rivaldo";
//	public const string SKU_LEGEND_KOEMAN		= "legend_koeman";
//
//	public const string SKU_STARTER_PACK		= "euro001";
//	public const string SKU_FCB_MEGA_DEAL		= "euro002";
//	public const string SKU_COLLECTOR_PACK		= "euro003";
//	public const string SKU_EURO_PACK			= "euro004";
//	public const string SKU_TEN_GIFT_CARDS		= "scr001";
//
//	public const string SKU_DEAL_1				= "deal1";
//	public const string SKU_DEAL_2				= "deal2";
//	public const string SKU_DEAL_3				= "deal3";
//	public const string SKU_DEAL_4				= "deal4";
//	public const string SKU_DEAL_5				= "deal5";
//	public const string SKU_DEAL_6				= "deal6";
//	public const string SKU_DEAL_7				= "deal7";
//	public const string SKU_DEAL_8				= "deal8";
//	public const string SKU_DEAL_9				= "deal9";
//	public const string SKU_DEAL_10				= "deal10";
	#endif


	// IAP Products
	public IAPProduct Product_FCB_MEGA_PACK;
	public IAPProduct Product_STARTER_DEAL;
	public IAPProduct Product_QUICK_FIX;
	public IAPProduct Product_LOCAL_CLUB;
	public IAPProduct Product_NATIONAL_FUND;
	public IAPProduct Product_WORLD_DOMINATOR;
	public IAPProduct Product_FCB_PRESIDENT;
	public IAPProduct Product_CHAMPIONS_PACK;
//  public IAPProduct Product_FCB_PACK;
//	public IAPProduct Product_LEGEND_RONALDINHO;
//	public IAPProduct Product_LEGEND_PUYOL;
//	public IAPProduct Product_LEGEND_PEP;
//	public IAPProduct Product_LEGEND_RIVALDO;
//	public IAPProduct Product_LEGEND_STOICHKOV;
//	public IAPProduct Product_LEGEND_KOEMAN;
//
//	public IAPProduct Product_STARTER_PACK;
//	public IAPProduct Product_FCB_MEGA_DEAL;
//	public IAPProduct Product_COLLECTOR_PACK;
//	public IAPProduct Product_EURO_PACK;
//	public IAPProduct Product_TEN_GIFT_CARDS;
//
//	public IAPProduct Product_Deal_1;
//	public IAPProduct Product_Deal_2;
//	public IAPProduct Product_Deal_3;
//	public IAPProduct Product_Deal_4;
//	public IAPProduct Product_Deal_5;
//	public IAPProduct Product_Deal_6;
//	public IAPProduct Product_Deal_7;
//	public IAPProduct Product_Deal_8;
//	public IAPProduct Product_Deal_9;
//	public IAPProduct Product_Deal_10;

	public Purchase currPurchase;


	void Awake(){
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("IAP awake start ");
		Instance = this;
		IsReady = false;
		LOCKED = false;
		AdModuleCallBacks = new List<AdCTA> ();

		#if UNITY_EDITOR
			IsReady = true;

			Product_FCB_MEGA_PACK = new IAPProduct(SKU_FCB_MEGA_PACK, "$", "0.1");
			//Product_FCB_PACK = new IAPProduct(SKU_FCB_PACK, "$", "0.2");
			Product_STARTER_DEAL = new IAPProduct(SKU_STARTER_DEAL, "$", "0.3");
			Product_QUICK_FIX = new IAPProduct(SKU_QUICK_FIX, "$", "1.1");
			Product_LOCAL_CLUB = new IAPProduct(SKU_LOCAL_CLUB, "$", "1.2");
			Product_NATIONAL_FUND = new IAPProduct(SKU_NATIONAL_FUND, "$", "1.3");
			Product_WORLD_DOMINATOR = new IAPProduct(SKU_WORLD_DOMINATOR, "$", "1.4");
			Product_FCB_PRESIDENT = new IAPProduct(SKU_WORLD_DOMINATOR, "$", "1.5");
			Product_CHAMPIONS_PACK = new IAPProduct(SKU_WORLD_DOMINATOR, "$", "1.6");

//			Product_LEGEND_RONALDINHO = new IAPProduct(SKU_LEGEND_RONALDINHO, "$", "2");
//			Product_LEGEND_PUYOL = new IAPProduct(SKU_LEGEND_PUYOL, "$", "2.5");
//			Product_LEGEND_RONALDINHO = new IAPProduct(SKU_LEGEND_RONALDINHO, "$", "2");
//			Product_LEGEND_PUYOL = new IAPProduct(SKU_LEGEND_PUYOL, "$", "2.5");
//			Product_LEGEND_PEP = new IAPProduct(SKU_LEGEND_PUYOL, "$", "3.5");
//			Product_LEGEND_RIVALDO = new IAPProduct(SKU_LEGEND_PUYOL, "$", "4.5");
//			Product_LEGEND_STOICHKOV = new IAPProduct(SKU_LEGEND_PUYOL, "$", "5.5");
//			Product_LEGEND_KOEMAN = new IAPProduct(SKU_LEGEND_PUYOL, "$", "6.5");
//
//		Product_STARTER_PACK = new IAPProduct(SKU_EURO_PACK, "$", "17.9");
//		Product_FCB_MEGA_DEAL = new IAPProduct(SKU_EURO_PACK, "$", "17.9");
//		Product_COLLECTOR_PACK = new IAPProduct(SKU_EURO_PACK, "$", "17.9");
//		Product_EURO_PACK = new IAPProduct(SKU_EURO_PACK, "$", "17.9");
//		Product_TEN_GIFT_CARDS = new IAPProduct(SKU_EURO_PACK, "$", "17.9");
//
//		Product_Deal_1 = new IAPProduct(SKU_DEAL_1, "$", "17.9");
//		Product_Deal_2 = new IAPProduct(SKU_DEAL_2, "$", "17.9");
//		Product_Deal_3 = new IAPProduct(SKU_DEAL_3, "$", "17.9");
//		Product_Deal_4 = new IAPProduct(SKU_DEAL_4, "$", "17.9");
//		Product_Deal_5 = new IAPProduct(SKU_DEAL_5, "$", "17.9");
//		Product_Deal_6 = new IAPProduct(SKU_DEAL_6, "$", "17.9");
//		Product_Deal_7 = new IAPProduct(SKU_DEAL_7, "$", "17.9");
//		Product_Deal_8 = new IAPProduct(SKU_DEAL_8, "$", "17.9");
//		Product_Deal_9 = new IAPProduct(SKU_DEAL_9, "$", "17.9");
//		Product_Deal_10 = new IAPProduct(SKU_DEAL_10, "$", "17.9");


		#endif

	}

	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log("IAP | " + msg);
	}

	void OnEnable(){
		OpenIABEventManager.billingSupportedEvent += OnBillingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent += OnBillingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent+= OnQueryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent += OnQueryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent += OnPurchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent += OnPurchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent += OnConsumeSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent += OnConsumeFailedEvent;
		OpenIABEventManager.restoreSucceededEvent += OnRestoreSuccess;
		OpenIABEventManager.restoreFailedEvent += OnRestoreFailed;
		OpenIABEventManager.transactionRestoredEvent += OnTransactionRestored;
	}

	void OnDisable(){
		OpenIABEventManager.billingSupportedEvent -= OnBillingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent -= OnBillingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent-= OnQueryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent -= OnQueryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent -= OnPurchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent -= OnPurchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent -= OnConsumeSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent -= OnConsumeFailedEvent;
		OpenIABEventManager.restoreSucceededEvent -= OnRestoreSuccess;
		OpenIABEventManager.restoreFailedEvent -= OnRestoreFailed;
		OpenIABEventManager.transactionRestoredEvent -= OnTransactionRestored;
	}

	void Start(){


		#if UNITY_ANDROID
		OpenIAB.mapSku(SKU_FCB_MEGA_PACK, OpenIAB_Android.STORE_GOOGLE, SKU_FCB_MEGA_PACK);
		//OpenIAB.mapSku(SKU_FCB_PACK, OpenIAB_Android.STORE_GOOGLE, SKU_FCB_PACK);
		OpenIAB.mapSku(SKU_STARTER_DEAL, OpenIAB_Android.STORE_GOOGLE, SKU_STARTER_DEAL);
		OpenIAB.mapSku(SKU_QUICK_FIX, OpenIAB_Android.STORE_GOOGLE, SKU_QUICK_FIX);
		OpenIAB.mapSku(SKU_LOCAL_CLUB, OpenIAB_Android.STORE_GOOGLE, SKU_LOCAL_CLUB);
		OpenIAB.mapSku(SKU_NATIONAL_FUND, OpenIAB_Android.STORE_GOOGLE, SKU_NATIONAL_FUND);
		OpenIAB.mapSku(SKU_WORLD_DOMINATOR, OpenIAB_Android.STORE_GOOGLE, SKU_WORLD_DOMINATOR);
		OpenIAB.mapSku(SKU_FCB_PRESIDENT, OpenIAB_Android.STORE_GOOGLE, SKU_FCB_PRESIDENT);
		OpenIAB.mapSku(SKU_CHAMPIONS_PACK, OpenIAB_Android.STORE_GOOGLE, SKU_CHAMPIONS_PACK);

//		OpenIAB.mapSku(SKU_LEGEND_RONALDINHO, OpenIAB_Android.STORE_GOOGLE, SKU_LEGEND_RONALDINHO);
//		OpenIAB.mapSku(SKU_LEGEND_PUYOL, OpenIAB_Android.STORE_GOOGLE, SKU_LEGEND_PUYOL);
//		OpenIAB.mapSku(SKU_LEGEND_PEP, OpenIAB_Android.STORE_GOOGLE, SKU_LEGEND_PEP);
//		OpenIAB.mapSku(SKU_LEGEND_STOICHKOV, OpenIAB_Android.STORE_GOOGLE, SKU_LEGEND_STOICHKOV);
//		OpenIAB.mapSku(SKU_LEGEND_RIVALDO, OpenIAB_Android.STORE_GOOGLE, SKU_LEGEND_RIVALDO);
//		OpenIAB.mapSku(SKU_LEGEND_KOEMAN, OpenIAB_Android.STORE_GOOGLE, SKU_LEGEND_KOEMAN);
//
//		OpenIAB.mapSku(SKU_STARTER_PACK, OpenIAB_Android.STORE_GOOGLE, SKU_STARTER_PACK);
//		OpenIAB.mapSku(SKU_FCB_MEGA_DEAL, OpenIAB_Android.STORE_GOOGLE, SKU_FCB_MEGA_DEAL);
//		OpenIAB.mapSku(SKU_COLLECTOR_PACK, OpenIAB_Android.STORE_GOOGLE, SKU_COLLECTOR_PACK);
//		OpenIAB.mapSku(SKU_EURO_PACK, OpenIAB_Android.STORE_GOOGLE, SKU_EURO_PACK);
//		OpenIAB.mapSku(SKU_TEN_GIFT_CARDS, OpenIAB_Android.STORE_GOOGLE, SKU_TEN_GIFT_CARDS);
//
//		OpenIAB.mapSku(SKU_DEAL_1, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_1);
//		OpenIAB.mapSku(SKU_DEAL_2, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_2);
//		OpenIAB.mapSku(SKU_DEAL_3, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_3);
//		OpenIAB.mapSku(SKU_DEAL_4, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_4);
//		OpenIAB.mapSku(SKU_DEAL_5, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_5);
//		OpenIAB.mapSku(SKU_DEAL_6, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_6);
//		OpenIAB.mapSku(SKU_DEAL_7, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_7);
//		OpenIAB.mapSku(SKU_DEAL_8, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_8);
//		OpenIAB.mapSku(SKU_DEAL_9, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_9);
//		OpenIAB.mapSku(SKU_DEAL_10, OpenIAB_Android.STORE_GOOGLE, SKU_DEAL_10);

		#elif UNITY_IOS
		OpenIAB.mapSku(SKU_FCB_MEGA_PACK, OpenIAB_iOS.STORE, SKU_FCB_MEGA_PACK);
//		OpenIAB.mapSku(SKU_FCB_PACK, OpenIAB_iOS.STORE, SKU_FCB_PACK);
		OpenIAB.mapSku(SKU_STARTER_DEAL, OpenIAB_iOS.STORE, SKU_STARTER_DEAL);
		OpenIAB.mapSku(SKU_QUICK_FIX, OpenIAB_iOS.STORE, SKU_QUICK_FIX);
		OpenIAB.mapSku(SKU_LOCAL_CLUB, OpenIAB_iOS.STORE, SKU_LOCAL_CLUB);
		OpenIAB.mapSku(SKU_NATIONAL_FUND, OpenIAB_iOS.STORE, SKU_NATIONAL_FUND);
		OpenIAB.mapSku(SKU_WORLD_DOMINATOR, OpenIAB_iOS.STORE, SKU_WORLD_DOMINATOR);
		OpenIAB.mapSku(SKU_FCB_PRESIDENT, OpenIAB_iOS.STORE, SKU_FCB_PRESIDENT);
		OpenIAB.mapSku(SKU_CHAMPIONS_PACK, OpenIAB_iOS.STORE, SKU_CHAMPIONS_PACK);

//		OpenIAB.mapSku(SKU_LEGEND_RONALDINHO, OpenIAB_iOS.STORE, SKU_LEGEND_RONALDINHO);
//		OpenIAB.mapSku(SKU_LEGEND_PUYOL, OpenIAB_iOS.STORE, SKU_LEGEND_PUYOL);
//		OpenIAB.mapSku(SKU_LEGEND_PEP, OpenIAB_iOS.STORE, SKU_LEGEND_PEP);
//		OpenIAB.mapSku(SKU_LEGEND_STOICHKOV, OpenIAB_iOS.STORE, SKU_LEGEND_STOICHKOV);
//		OpenIAB.mapSku(SKU_LEGEND_RIVALDO, OpenIAB_iOS.STORE, SKU_LEGEND_RIVALDO);
//		OpenIAB.mapSku(SKU_LEGEND_KOEMAN, OpenIAB_iOS.STORE, SKU_LEGEND_KOEMAN);
//
//		OpenIAB.mapSku(SKU_STARTER_PACK, OpenIAB_iOS.STORE, SKU_STARTER_PACK);
//		OpenIAB.mapSku(SKU_FCB_MEGA_DEAL, OpenIAB_iOS.STORE, SKU_FCB_MEGA_DEAL);
//		OpenIAB.mapSku(SKU_COLLECTOR_PACK, OpenIAB_iOS.STORE, SKU_COLLECTOR_PACK);
//		OpenIAB.mapSku(SKU_EURO_PACK, OpenIAB_iOS.STORE, SKU_EURO_PACK);
//		OpenIAB.mapSku(SKU_TEN_GIFT_CARDS, OpenIAB_iOS.STORE, SKU_TEN_GIFT_CARDS);
//
//		OpenIAB.mapSku(SKU_DEAL_1, OpenIAB_iOS.STORE, SKU_DEAL_1);
//		OpenIAB.mapSku(SKU_DEAL_2, OpenIAB_iOS.STORE, SKU_DEAL_2);
//		OpenIAB.mapSku(SKU_DEAL_3, OpenIAB_iOS.STORE, SKU_DEAL_3);
//		OpenIAB.mapSku(SKU_DEAL_4, OpenIAB_iOS.STORE, SKU_DEAL_4);
//		OpenIAB.mapSku(SKU_DEAL_5, OpenIAB_iOS.STORE, SKU_DEAL_5);
//		OpenIAB.mapSku(SKU_DEAL_6, OpenIAB_iOS.STORE, SKU_DEAL_6);
//		OpenIAB.mapSku(SKU_DEAL_7, OpenIAB_iOS.STORE, SKU_DEAL_7);
//		OpenIAB.mapSku(SKU_DEAL_8, OpenIAB_iOS.STORE, SKU_DEAL_8);
//		OpenIAB.mapSku(SKU_DEAL_9, OpenIAB_iOS.STORE, SKU_DEAL_9);
//		OpenIAB.mapSku(SKU_DEAL_10, OpenIAB_iOS.STORE, SKU_DEAL_10);

		#endif



		var options = new OnePF.Options();
		options.checkInventory = false;
		options.availableStoreNames = new string[] {OpenIAB_Android.STORE_GOOGLE, OpenIAB_iOS.STORE};	
		#if UNITY_ANDROID
			options.availableStoreNames = new string[] {OpenIAB_Android.STORE_GOOGLE};	
			options.prefferedStoreNames = new string[] { OpenIAB_Android.STORE_GOOGLE};
		#elif UNITY_IOS 
			options.availableStoreNames = new string[] {OpenIAB_iOS.STORE};	
			options.prefferedStoreNames = new string[] { OpenIAB_iOS.STORE };
		#endif

		options.storeSearchStrategy = SearchStrategy.INSTALLER_THEN_BEST_FIT;
		options.verifyMode = OptionsVerifyMode.VERIFY_SKIP;
		options.storeKeys.Add(OpenIAB_Android.STORE_GOOGLE, GOOGLE_PUBLIC_KEY);
		//options.storeKeys.Add (OpenIAB_iOS.STORE, "");

		OpenIAB.init(options);
		if (UnityInitializer.Instance!=null)
			UnityInitializer.Instance.addToMonitorLog("IAP end");
	}


	void OnBillingSupportedEvent(){
		Log ("Billing Supported Event Triggered, quering inventory...");
		OpenIAB.queryInventory (new string[] { 
			SKU_FCB_MEGA_PACK,
//			SKU_FCB_PACK,
			SKU_STARTER_DEAL,
			SKU_QUICK_FIX,
			SKU_LOCAL_CLUB,
			SKU_NATIONAL_FUND,
			SKU_WORLD_DOMINATOR,
			SKU_FCB_PRESIDENT,
			SKU_CHAMPIONS_PACK,
//			SKU_LEGEND_RONALDINHO,
//			SKU_LEGEND_PUYOL,
//			SKU_LEGEND_PEP,
//			SKU_LEGEND_STOICHKOV,
//			SKU_LEGEND_RIVALDO,
//			SKU_LEGEND_KOEMAN,
//			SKU_STARTER_PACK,
//			SKU_FCB_MEGA_DEAL,
//			SKU_COLLECTOR_PACK,
//			SKU_EURO_PACK,
//			SKU_TEN_GIFT_CARDS,
//			SKU_DEAL_1,
//			SKU_DEAL_2,
//			SKU_DEAL_3,
//			SKU_DEAL_4,
//			SKU_DEAL_5,
//			SKU_DEAL_6,
//			SKU_DEAL_7,
//			SKU_DEAL_8,
//			SKU_DEAL_9,
//			SKU_DEAL_10
		});					
	}

	void OnBillingNotSupportedEvent(string error){
		Log ("Billing NOT Supported Event Triggered, error: " + error);
	}

	Inventory myInventory;

	void OnQueryInventorySucceededEvent(Inventory i){
//   	Log ("Query Inventory Success!");
//		Debug.Log("alon_______ IAP - OnQueryInventorySucceededEvent()");
//		Debug.Log ("alon________ IAP - OnQueryInventorySucceededEvent() - NativeSocial.IsLoggedIn = " + NativeSocial.IsLoggedIn);
//		Debug.Log ("alon________ IAP - OnQueryInventorySucceededEvent() - i.GetAllAvailableSkus ().Count = " + i.GetAllAvailableSkus ().Count);

//		foreach (SkuDetails sku in i.GetAllAvailableSkus ()) {
//			Debug.Log ("alon________ IAP - sku name = " + sku.Sku);
//		}
        print("all available skus:"+i.GetAllAvailableSkus().Count);
		foreach (SkuDetails sku in i.GetAllAvailableSkus ()) {
            print("skudetails: " + sku.Title);
			if (sku.Sku == SKU_QUICK_FIX)
				Product_QUICK_FIX = new IAPProduct (sku);
			else if (sku.Sku == SKU_LOCAL_CLUB)
				Product_LOCAL_CLUB = new IAPProduct (sku);
			else if (sku.Sku == SKU_NATIONAL_FUND)
				Product_NATIONAL_FUND = new IAPProduct (sku);
			else if (sku.Sku == SKU_WORLD_DOMINATOR)
				Product_WORLD_DOMINATOR = new IAPProduct (sku);
			else if (sku.Sku == SKU_FCB_MEGA_PACK)
				Product_FCB_MEGA_PACK = new IAPProduct (sku);
//			else if (sku.Sku == SKU_FCB_PACK)
//				Product_FCB_PACK = new IAPProduct (sku);
			else if (sku.Sku == SKU_STARTER_DEAL)
				Product_STARTER_DEAL = new IAPProduct (sku);
			else if (sku.Sku == SKU_FCB_PRESIDENT)
				Product_FCB_PRESIDENT = new IAPProduct (sku);
			else if (sku.Sku == SKU_CHAMPIONS_PACK)
				Product_CHAMPIONS_PACK = new IAPProduct (sku);


//			else if (sku.Sku == SKU_LEGEND_RONALDINHO) {
//				Product_LEGEND_RONALDINHO = new IAPProduct (sku);
//			} else if (sku.Sku == SKU_LEGEND_PUYOL) {
//				Product_LEGEND_PUYOL = new IAPProduct (sku);
//			} else if (sku.Sku == SKU_LEGEND_PEP) {
//				Product_LEGEND_PEP = new IAPProduct (sku);
//			} else if (sku.Sku == SKU_LEGEND_STOICHKOV) {
//				Product_LEGEND_STOICHKOV = new IAPProduct (sku);
//			} else if (sku.Sku == SKU_LEGEND_RIVALDO) {
//				Product_LEGEND_RIVALDO = new IAPProduct (sku);
//			} else if (sku.Sku == SKU_LEGEND_KOEMAN) {
//				Product_LEGEND_KOEMAN = new IAPProduct (sku);
//			}
//
//
//			else if(sku.Sku == SKU_STARTER_PACK)		Product_STARTER_PACK = new IAPProduct (sku);
//			else if(sku.Sku == SKU_FCB_MEGA_DEAL)		Product_FCB_MEGA_DEAL = new IAPProduct (sku);
//			else if(sku.Sku == SKU_COLLECTOR_PACK)		Product_COLLECTOR_PACK = new IAPProduct (sku);
//			else if(sku.Sku == SKU_EURO_PACK)			Product_EURO_PACK = new IAPProduct (sku);
//			else if(sku.Sku == SKU_TEN_GIFT_CARDS)		Product_TEN_GIFT_CARDS = new IAPProduct (sku);
//
//			else if(sku.Sku == SKU_DEAL_1)				Product_Deal_1 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_2)				Product_Deal_2 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_3)				Product_Deal_3 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_4)				Product_Deal_4 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_5)				Product_Deal_5 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_6)				Product_Deal_6 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_7)				Product_Deal_7 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_8)				Product_Deal_8 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_9)				Product_Deal_9 = new IAPProduct (sku);
//			else if(sku.Sku == SKU_DEAL_10)				Product_Deal_10 = new IAPProduct (sku);

			Log ("Item Found: " + sku.Sku + " curency code: " + sku.CurrencyCode + " price: " + sku.Price);
		}

		// getting all owned products: 
//		foreach (string sku in i.GetAllOwnedSkus ()) {
//			if (sku == SKU_LEGEND_RONALDINHO) {
//				AddPlayerToPurchased (19);
//			} else if (sku == SKU_LEGEND_PUYOL) {
//				AddPlayerToPurchased (12);
//			} else if (sku == SKU_LEGEND_PEP) {
//				AddPlayerToPurchased (14);
//			} else if (sku == SKU_LEGEND_STOICHKOV) {
//				AddPlayerToPurchased (15);
//			} else if (sku == SKU_LEGEND_RIVALDO) {
//				AddPlayerToPurchased (20);
//			} else if (sku == SKU_LEGEND_KOEMAN) {
//				AddPlayerToPurchased (17);
//			}
//		}

		IsReady = true;
		myInventory = i;
//		Debug.Log ("alon________ IAP - OnQueryInventorySucceededEvent() - myInventory.GetAllAvailableSkus ().Count = " + myInventory.GetAllAvailableSkus ().Count);
//		Debug.Log ("alon________ IAP - OnQueryInventorySucceededEvent() - myInventory.GetAllOwnedSkus ().Count = " + myInventory.GetAllOwnedSkus ().Count);
//		foreach (string sku in myInventory.GetAllOwnedSkus ()) {
//			Debug.Log ("alon________ IAP - owned sku name = " + sku);
//		}
	}

	public void RestoreAndroid()
    {
        // getting all owned products: 
		if (!IsReady) {
			CrossSceneUIHandler.Instance.iapMessage.ShowResult("failed");
			return;
		}

//		Debug.Log ("alon________ IAP - RestoreAndroid() - myInventory.GetAllOwnedSkus().Count = " + myInventory.GetAllOwnedSkus ().Count);
//		foreach (string sku in myInventory.GetAllOwnedSkus ()) {
//			Debug.Log ("alon________ IAP - owned sku name = " + sku);
//		}

//		foreach (string sku in myInventory.GetAllOwnedSkus ()) {
//			if (sku == SKU_LEGEND_RONALDINHO) {
//				AddPlayerToPurchased (19);
//				updateLegendPlayerPerks (19);
//			} else if (sku == SKU_LEGEND_PUYOL) {
//				AddPlayerToPurchased (12);
//				updateLegendPlayerPerks (12);
//			} else if (sku == SKU_LEGEND_PEP) {
//				AddPlayerToPurchased (14);
//				updateLegendPlayerPerks (14);
//			} else if (sku == SKU_LEGEND_STOICHKOV) {
//				AddPlayerToPurchased (15);
//				updateLegendPlayerPerks (15);
//			} else if (sku == SKU_LEGEND_RIVALDO) {
//				AddPlayerToPurchased (20);
//				updateLegendPlayerPerks (20);
//			} else if (sku == SKU_LEGEND_KOEMAN) {
//				AddPlayerToPurchased (17);
//				updateLegendPlayerPerks (17);
//			}
//		}

        foreach (var p in myInventory.GetAllPurchases())
        {
            if (p.Sku == SKU_QUICK_FIX)
            {
                //OnCoinsPurchased (Pack_1_Value, p.Sku);
                OpenIAB.consumeProduct(p);
            }
            else if (p.Sku == SKU_LOCAL_CLUB)
            {
                //OnCoinsPurchased (Pack_2_Value, p.Sku);
                OpenIAB.consumeProduct(p);
            }
            else if (p.Sku == SKU_NATIONAL_FUND)
            {
                //OnCoinsPurchased (Pack_3_Value, p.Sku);
                OpenIAB.consumeProduct(p);
            }
            else if (p.Sku == SKU_WORLD_DOMINATOR)
            {
                //OnCoinsPurchased (Pack_4_Value, p.Sku);
                OpenIAB.consumeProduct(p);
            }
            else if (p.Sku == SKU_FCB_PRESIDENT)
            {
                //OnCoinsPurchased (Pack_5_Value, p.Sku);
                OpenIAB.consumeProduct(p);
            }
            else if (p.Sku == SKU_CHAMPIONS_PACK)
            {
                //OnCoinsPurchased (Pack_6_Value, p.Sku);
                OpenIAB.consumeProduct(p);
            }
            else if (p.Sku == SKU_FCB_MEGA_PACK)
            {
                //OnCoinsPurchased (Pack_fcbmega_Value, p.Sku);
                DAO.FCBMegaDealBought = true;
                OpenIAB.consumeProduct(p);
            }
//			} else if (p.Sku == SKU_FCB_PACK) {
//				//OnCoinsPurchased (Pack_fcb_Value, p.Sku);
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_STARTER_PACK) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_FCB_MEGA_DEAL) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_COLLECTOR_PACK) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_EURO_PACK) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_TEN_GIFT_CARDS) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_1) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_2) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_3) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_4) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_5) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_6) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_7) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_8) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_9) {
//				OpenIAB.consumeProduct (p);
//			} else if (p.Sku == SKU_DEAL_10) {
//				OpenIAB.consumeProduct (p);
//			}
		}
			
        OnRestoreSuccess();
	}


	void OnQueryInventoryFailedEvent(string error){
		Log ("Query Inventory Failed, error: " + error);
	}

	void OnPurchaseSucceededEvent(Purchase p)
    {
        Log("Purchase " + p.Sku + " success!");
        currPurchase = p;
        CrossSceneUIHandler.Instance.iapMessage.ShowResult("success");
        LOCKED = false;


        if (DAO.IAPPoints <= 0)
        {
            UnitedAnalytics.LogEvent("First IAP made in game", p.Sku, UserData.Instance.userType.ToString(), DAO.NumOfPurchasedPlayers);
        }
        else
        {
            UnitedAnalytics.LogEvent("IAP made", p.Sku, "Total Purchases: " + (DAO.IAPPoints + 1), DAO.NumOfPurchasedPlayers);
        }
        AppsFlyerManager._.ValidatePurchase(p, GetIAPProducatBySKU(p.Sku));
        //AppsFlyerManager._.ReportIAP( GetIAPProducatBySKU(p.Sku) );

        if (p.Sku == SKU_QUICK_FIX)
        {
            OnCoinsPurchased(Pack_1_Value, p.Sku);
            OpenIAB.consumeProduct(p);
        }
        else if (p.Sku == SKU_LOCAL_CLUB)
        {
            OnCoinsPurchased(Pack_2_Value, p.Sku);
            OpenIAB.consumeProduct(p);
        }
        else if (p.Sku == SKU_NATIONAL_FUND)
        {
            OnCoinsPurchased(Pack_3_Value, p.Sku);
            OpenIAB.consumeProduct(p);
        }
        else if (p.Sku == SKU_WORLD_DOMINATOR)
        {
            OnCoinsPurchased(Pack_4_Value, p.Sku);
            OpenIAB.consumeProduct(p);
        }
        else if (p.Sku == SKU_FCB_PRESIDENT)
        {
            OnCoinsPurchased(Pack_5_Value, p.Sku);
            OpenIAB.consumeProduct(p);
        }
        else if (p.Sku == SKU_CHAMPIONS_PACK)
        {
            OnCoinsPurchased(Pack_6_Value, p.Sku);
            OpenIAB.consumeProduct(p);
        }
        else if (p.Sku == SKU_FCB_MEGA_PACK)
        {
            OnCoinsPurchased(Pack_fcbmega_Value, p.Sku);
            DAO.FCBMegaDealBought = true;
            OpenIAB.consumeProduct(p);
        } 
//        else if (p.Sku == SKU_FCB_PACK) {
//			OnCoinsPurchased (Pack_fcb_Value, p.Sku);
//			OpenIAB.consumeProduct (p);
//		}
        else if (p.Sku == SKU_STARTER_DEAL)
        {
            NativeSocial._.ReportAchievment(SocialConstants.achievement_invested_manager);
            OnCoinsPurchased(Pack_starter_Value, p.Sku);
            OpenIAB.consumeProduct(p);
            DAO.StarterPackBought = true;
            UnitedAnalytics.LogEvent("Buy starter pop up", "IAP Made", UserData.Instance.userType.ToString());
        }
//		} else if (p.Sku == SKU_STARTER_PACK) { // the new euro packs:
//			OpenIAB.consumeProduct (p);
//		} else if (p.Sku == SKU_FCB_MEGA_DEAL) {
//			OpenIAB.consumeProduct (p);
//		} else if (p.Sku == SKU_COLLECTOR_PACK) {
//			OpenIAB.consumeProduct (p);
//		} else if (p.Sku == SKU_EURO_PACK) {
//			OpenIAB.consumeProduct (p);
//		} else if (p.Sku == SKU_TEN_GIFT_CARDS) {
//			OpenIAB.consumeProduct (p);
//		} 
        else {
		OnLegendPurchased (p.Sku);
	}

		IAPProduct iapp = GetIAPProducatBySKU (p.Sku);
		// -----
//		Debug.Log("RON_________________Purchase structure:" + " AppstoreName" + p.AppstoreName + " DeveloperPayload" + p.DeveloperPayload + " ItemType" + p.ItemType + " OrderId:" + p.OrderId +
//			" PackageName" + p.PackageName + " PurchaseState" + p.PurchaseState.ToString() + "PurchaseTime" + p.PurchaseTime.ToString() + " Receipt" + p.Receipt + " Signature" + p.Signature +
//			"Token" + p.Token + " OriginalJson:" +	p.OriginalJson + "/n" );  
//
//		Debug.Log("RON_________________iapp structure:" + " iapp.CurrencyString:"+ iapp.CurrencyString + " iapp.JSONString:" + iapp.JSONString + " iapp.Price" + iapp.Price 
//			+ " iapp.PriceValue" + iapp.PriceValue + iapp.SKUDetails.ToString());  
		
		if (AdditionalIAPCallback != null) AdditionalIAPCallback (p.Sku);
		UserData.UpdateUserType (UserData.UserType.PAYING_USER);
		DAO.IAPPoints ++;
		ProcessAdModuleCallBacks ();
		IapTransactionStructure transaction = new IapTransactionStructure ();

		transaction.sku = p.Sku;
		string currency = "";
		string receipt = "";
		#if UNITY_IOS
		currency= iapp.SKUDetails.CurrencyCode;
		receipt = p.Receipt;
		#elif UNITY_ANDROID
		currency= iapp.SKUDetails.CurrencyString;
		receipt = p.OrderId;
		try
		{
			
			SimpleJSON.JSONNode json = SimpleJSON.JSON.Parse(p.OriginalJson);
		    System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
			//double sec = json["PurchaseTime]"].AsDouble;

			DateTime dt = epochStart.AddSeconds (json["purchaseTime"].AsDouble);
			transaction.sDate = dt.ToString("yyyyMMdd");
			transaction.sDateTime = dt.ToString("yyyyMMddHHmmss");
		}
		catch (Exception ex)
		{
			Debug.Log("Ron____________________exception"+ex.Message);
		}
		#endif
		transaction.price = float.Parse(iapp.PriceValue);

		transaction.currency = currency;
		DAO.currency = currency;
		transaction.receiptJson = p.OriginalJson;
		transaction.receipt = receipt;

		DDB._.SaveIapTransaction(transaction);
		DDB._.UpdateWhaleTracker(0f);


		//OpenIAB.consumeProduct (p);
//		Debug.Log ("alon________ IAP - OnPurchaseSucceededEvent() - myInventory.GetAllOwnedSkus().Count = " + myInventory.GetAllOwnedSkus ().Count);
//		foreach (string sku in myInventory.GetAllOwnedSkus ()) {
//			Debug.Log ("alon________ IAP - owned sku name = " + sku);
//		}
	}

	void ProcessAdModuleCallBacks(){
		Log("Checking AdModule Callbacks...");
		foreach (AdCTA cta in AdModuleCallBacks) {
			cta.Invoke ();
			Log("Invoking " + cta.CTAType.ToString() + " Param: " + cta.Param);
		}
		AdModuleCallBacks.Clear ();
	}


	void OnPurchaseFailedEvent(int code, string message){
        CrossSceneUIHandler.Instance.iapMessage.ShowResult("failed");
		LOCKED = false;
		Log ("Purchase failed! error code: " + code + " error messgae: " + message);
		if(AdModuleCallBacks != null) AdModuleCallBacks.Clear ();
	}

	void OnConsumeSucceededEvent(Purchase p){
		Log ("Consume "+ p.Sku +" success!");
	}

	void OnConsumeFailedEvent(string error){
		Log ("Consume failed, error: " + error);
	}

	void OnRestoreSuccess(){
        DAO.isRestoredPurchases = true;
        MainMenu_Actions.instance.restorePurchasesBtn.interactable = false;
        CrossSceneUIHandler.Instance.iapMessage.ShowResult("success");
		Log( "Transaction Restore Successfully completed");
	}

	void OnRestoreFailed(string error){
        CrossSceneUIHandler.Instance.iapMessage.ShowResult("failed");
		Log( "Error Restoring Transaction: " + error);
	}

	void OnTransactionRestored(string sku){
		Log ("Transaction Restored: " + sku);
		OnLegendPurchased (sku);
	}

	// ----------

	public IAPProduct GetIAPProducatBySKU(string sku){
		if (sku == SKU_QUICK_FIX)				return Product_QUICK_FIX;
		else if(sku == SKU_LOCAL_CLUB)			return Product_LOCAL_CLUB;
		else if(sku == SKU_NATIONAL_FUND)		return Product_NATIONAL_FUND;
		else if(sku == SKU_WORLD_DOMINATOR) 	return Product_WORLD_DOMINATOR;
		else if(sku == SKU_FCB_MEGA_PACK) 		return Product_FCB_MEGA_PACK;
//		else if(sku == SKU_FCB_PACK)			return Product_FCB_PACK;
		else if(sku == SKU_STARTER_DEAL)		return Product_STARTER_DEAL;
		else if(sku == SKU_FCB_PRESIDENT)		return Product_FCB_PRESIDENT;
		else if(sku == SKU_CHAMPIONS_PACK)		return Product_CHAMPIONS_PACK;

//		else if(sku == SKU_LEGEND_RONALDINHO)	return Product_LEGEND_RONALDINHO;
//		else if(sku == SKU_LEGEND_PUYOL)		return Product_LEGEND_PUYOL;
//		else if(sku == SKU_LEGEND_PEP)			return Product_LEGEND_PEP;
//		else if(sku == SKU_LEGEND_STOICHKOV)	return Product_LEGEND_STOICHKOV;
//		else if(sku == SKU_LEGEND_RIVALDO)		return Product_LEGEND_RIVALDO;
//		else if(sku == SKU_LEGEND_KOEMAN)		return Product_LEGEND_KOEMAN;
//
//		else if(sku == SKU_STARTER_PACK)		return Product_STARTER_PACK;
//		else if(sku == SKU_FCB_MEGA_DEAL)		return Product_FCB_MEGA_DEAL;
//		else if(sku == SKU_COLLECTOR_PACK)		return Product_COLLECTOR_PACK;
//		else if(sku == SKU_EURO_PACK)			return Product_EURO_PACK;
//		else if(sku == SKU_TEN_GIFT_CARDS)		return Product_TEN_GIFT_CARDS;
//
//		else if(sku == SKU_DEAL_1)				return Product_Deal_1;
//		else if(sku == SKU_DEAL_2)				return Product_Deal_2;
//		else if(sku == SKU_DEAL_3)				return Product_Deal_3;
//		else if(sku == SKU_DEAL_4)				return Product_Deal_4;
//		else if(sku == SKU_DEAL_5)				return Product_Deal_5;
//		else if(sku == SKU_DEAL_6)				return Product_Deal_6;
//		else if(sku == SKU_DEAL_7)				return Product_Deal_7;
//		else if(sku == SKU_DEAL_8)				return Product_Deal_8;
//		else if(sku == SKU_DEAL_9)				return Product_Deal_9;
//		else if(sku == SKU_DEAL_10)				return Product_Deal_10;
//
		else return new IAPProduct();
	}

	// =============================================================


	void OnCoinsPurchased(int amount, string sku){
		DAO.TotalCoinsCollected += amount;

		if (sku != "") {
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (amount, sku, "FCB Coins", "", "", sku);
			GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,amount, GameAnalyticsWrapper.IAP,"CoinsPurchased");
			GameAnalyticsWrapper.BusinessEvent(currPurchase,GetIAPProducatBySKU (currPurchase.Sku),"Coins","Store");
		}
		CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
		AudioManager.Instance.OnPurchase ();
		DAO.Instance.LOCAL.SaveAll ();
		CrossSceneUIHandler.Instance.hideStore ();

	}


	void OnLegendPurchased(string sku){
		DAO.IsLegendPurchased = true;
		//if (GameManager.Instance.isPlayscapeLoaded) {
//			BI._.Inventory_Upgrade (30, "Player Energy", "Player Bought", sku);
//			BI._.Inventory_Upgrade (1, "Player Level", "Player Bought", sku);
//			BI._.Inventory_Upgrade (1, "Player ", "IAP", sku);
		
		//}

		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {
			Bench.instance.OnLegendPurchaseSuccess ();
			GameManager.Instance.HandleRankScoreChange ();
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Inventory_Increase ("Locker Room IAP", sku, sku);
			try {
				
				GameAnalyticsWrapper.BuyNewPlayer(Bench.instance.np.playerName);
				GameAnalyticsWrapper.BusinessEvent(currPurchase,GetIAPProducatBySKU (currPurchase.Sku),"Legend","Bench");
			} catch (Exception ex) {
				Debug.Log (ex.Message);
			}

		} else {

			// The Following is for Restore Purchases
			int player_id = -1;

//			switch (sku) {
//			case SKU_LEGEND_KOEMAN:
//				{
//					player_id = 17;
//					break;
//				}
//			case SKU_LEGEND_PEP:
//				{
//					player_id = 14;
//					break;
//				}
//			case SKU_LEGEND_PUYOL:
//				{
//					player_id = 12;
//					break;
//				}
//			case SKU_LEGEND_RIVALDO:
//				{
//					player_id = 20;
//					break;
//				}
//			case SKU_LEGEND_RONALDINHO:
//				{
//					player_id = 19;
//					break;
//				}
//			case SKU_LEGEND_STOICHKOV:
//				{
//					player_id = 15;
//					break;
//				}
//			}
			if (player_id > 0) {
				AddPlayerToPurchased (player_id);
				GameManager.Instance.HandleRankScoreChange ();
				updateLegendPlayerPerks (player_id);
			}
			GameAnalyticsWrapper.BuyNewPlayer (sku);
		}

	}


	public void updateLegendPlayerPerks(int playerID)
	{
		PlayerData pd = new PlayerData (playerID, new Player());

		if (playerID == 12) { // Puyol
			pd.Perk_1 = new Perk (Perk.PerkType.ENERGY_FIEND.ToString(), "3");
			pd.Perk_2 = new Perk (Perk.PerkType.EXELLENT_FITNESS.ToString(), "3");
			pd.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString(), "3");
		} else
		if (playerID == 19) { // Ronaldinho
			pd.Perk_1 = new Perk (Perk.PerkType.GAMBLER.ToString(), "3");
			pd.Perk_2 = new Perk (Perk.PerkType.JUMPER.ToString(), "3");
			pd.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString(), "3");
			} else
		if (playerID == 14) { // Pep Guardiola
			pd.Perk_1 = new Perk (Perk.PerkType.MANAGER.ToString(), "3");
			pd.Perk_2 = new Perk (Perk.PerkType.MAGNETIC.ToString(), "3");
			pd.Perk_3 = new Perk (Perk.PerkType.EXELLENT_FITNESS.ToString(), "3");
		} else
		if (playerID == 17) { // Koeman
			pd.Perk_1 = new Perk (Perk.PerkType.ENERGY_FIEND.ToString(), "3");
			pd.Perk_2 = new Perk (Perk.PerkType.DODGER.ToString(), "3");
			pd.Perk_3 = new Perk (Perk.PerkType.MAGNETIC.ToString(), "3");
		} else
		if (playerID == 20) { // Rivaldo
			pd.Perk_1 = new Perk (Perk.PerkType.MAGNETIC.ToString(), "3");
			pd.Perk_2 = new Perk (Perk.PerkType.GAMBLER.ToString(), "3");
			pd.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString(), "3");
		} else
		if (playerID == 15) { // Stoichkov
			pd.Perk_1 = new Perk (Perk.PerkType.DODGER.ToString(), "3");
			pd.Perk_2 = new Perk (Perk.PerkType.EXELLENT_FITNESS.ToString (), "3");
			pd.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString (), "3");
		}
		DAO.SetPlayerData (playerID, pd.mEncode ());
		DAO.Instance.LOCAL.SaveAll();
	}

	void AddPlayerToPurchased(int id){
		if (!DAO.Instance.IsPlayerAllReadyPurchased(id.ToString())) {
			DAO.PurchasedPlayers += id+"|";
		    DAO.Instance.LOCAL.SaveAll ();
		}

	}


	// =============================================================

	public void OnRestorePurchasesClicked(){
		Log ("Restore Transaction Clicked");
        OpenIAPStatus();
		#if UNITY_IOS
		OpenIAB.restoreTransactions ();
		#elif UNITY_ANDROID
		RestoreAndroid();
		//Debug.Log("alon________ trying to restore Purchases on android"); 
		#endif
	}


	public void ClearAdditionalIAPCallback(){
		AdditionalIAPCallback = null;
	}


	public void OnQuickFixClicked(){
		#if UNITY_EDITOR
			OnCoinsPurchased(Pack_1_Value, "");
		#else
			LOCKED = true;
			OpenIAB.purchaseProduct(SKU_QUICK_FIX);
		#endif
        OpenIAPStatus();
	}

	public void OnLocalClubClicked(){
		#if UNITY_EDITOR
			OnCoinsPurchased(Pack_2_Value, "");
		#else
		LOCKED = true;
			OpenIAB.purchaseProduct(SKU_LOCAL_CLUB);
		#endif
        OpenIAPStatus();
	}

	public void OnNationalFundClicked(){
		#if UNITY_EDITOR
			OnCoinsPurchased(Pack_3_Value, "");
		#else
		LOCKED = true;
			OpenIAB.purchaseProduct(SKU_NATIONAL_FUND);
		#endif
        OpenIAPStatus();
	}

	public void OnWorldDominatorClicked(){
		#if UNITY_EDITOR
			OnCoinsPurchased(Pack_4_Value, "");
		#else
			OpenIAB.purchaseProduct(SKU_WORLD_DOMINATOR);
		#endif
        OpenIAPStatus();
	}

	public void OnFcbPresidentClicked(){
		#if UNITY_EDITOR
		OnCoinsPurchased(Pack_5_Value, "");
		#else
		OpenIAB.purchaseProduct(SKU_FCB_PRESIDENT);
		#endif
        OpenIAPStatus();
	}

	public void OnChampionsPackClicked(){
		#if UNITY_EDITOR
		OnCoinsPurchased(Pack_6_Value, "");
		#else
		OpenIAB.purchaseProduct(SKU_CHAMPIONS_PACK);
		#endif
        OpenIAPStatus();
	}

	public void OnStarterPackClicked(){
		#if UNITY_EDITOR
			OnCoinsPurchased(Pack_starter_Value, "");
		#else
		LOCKED = true;
			OpenIAB.purchaseProduct(SKU_STARTER_DEAL);
		#endif
        OpenIAPStatus();
	}

	public void OnFCBPackClicked(){
		#if UNITY_EDITOR
			OnCoinsPurchased(Pack_fcb_Value, "");
		#else
		LOCKED = true;
			//OpenIAB.purchaseProduct(SKU_FCB_PACK);
		#endif
        OpenIAPStatus();
	}

	public void OnEuroPackClicked(){
		#if UNITY_EDITOR
		OnCoinsPurchased(35000, "");
		#else
		LOCKED = true;
		//OpenIAB.purchaseProduct(SKU_EURO_PACK);
		#endif
        OpenIAPStatus();
	}

	public void OnFCBMegaPackClicked(){
		#if UNITY_EDITOR
			OnCoinsPurchased(Pack_fcbmega_Value, "");
		#else
		LOCKED = true;
		//	OpenIAB.purchaseProduct(SKU_FCB_MEGA_PACK);
		#endif
        OpenIAPStatus();
	}

	public void OnPuyolClciked(){
		#if UNITY_EDITOR
		Bench.instance.OnLegendPurchaseSuccess();
		#else
		LOCKED = true;
		//OpenIAB.purchaseProduct(SKU_LEGEND_PUYOL);
		#endif
        OpenIAPStatus();
	}

	public void OnRonaldinhoClciked(){
		#if UNITY_EDITOR
		Bench.instance.OnLegendPurchaseSuccess();
		#else
		LOCKED = true;
		//OpenIAB.purchaseProduct(SKU_LEGEND_RONALDINHO);
		#endif
        OpenIAPStatus();
	}


	public void OnPepClciked(){
		#if UNITY_EDITOR
		Bench.instance.OnLegendPurchaseSuccess();
		#else
		LOCKED = true;
		//OpenIAB.purchaseProduct(SKU_LEGEND_PEP);
		#endif
        OpenIAPStatus();
	}

	public void OnStoichkovClciked(){
		#if UNITY_EDITOR
		Bench.instance.OnLegendPurchaseSuccess();
		#else
		LOCKED = true;
		//OpenIAB.purchaseProduct(SKU_LEGEND_STOICHKOV);
		#endif
        OpenIAPStatus();
	}

	public void OnRivaldoClciked(){
		#if UNITY_EDITOR
		Bench.instance.OnLegendPurchaseSuccess();
		#else
		LOCKED = true;
		//OpenIAB.purchaseProduct(SKU_LEGEND_RIVALDO);
		#endif
        OpenIAPStatus();
	}

	public void OnKoemanClciked(){
		#if UNITY_EDITOR
		Bench.instance.OnLegendPurchaseSuccess();
		#else
		LOCKED = true;
		//OpenIAB.purchaseProduct(SKU_LEGEND_KOEMAN);
		#endif
        OpenIAPStatus();
	}

    public void OpenIAPStatus()
    {
        CrossSceneUIHandler.Instance.iapMessage.Show();
        #if UNITY_EDITOR
		CrossSceneUIHandler.Instance.iapMessage.ShowResult("success");
		UserData.UpdateUserType (UserData.UserType.PAYING_USER);
        #endif
    }



}


[System.Serializable]
public class IAPProduct{

	public string Sku;
	public string CurrencySymbol;
	public string Price;
	public string PriceValue;
	public string CurrencyString;
	public string JSONString;
	public OnePF.JSON JSONSPECIAL;
    public SkuDetails SKUDetails;

	public IAPProduct(string sku, string currency_symbol, string price){
		Sku = sku;
		CurrencySymbol = currency_symbol;
		Price = price;
		PriceValue = "";
	}

	public IAPProduct(SkuDetails sku){
		Sku = sku.Sku;
		CurrencySymbol = sku.CurrencyCode;
		Price = sku.Price;
		PriceValue = sku.PriceValue;
		CurrencyString = sku.CurrencyString;
		JSONString = sku.Json;
        JSONSPECIAL = sku.JSONSPECIAL;
        SKUDetails = sku;
        Debug.Log("JSON STRING:"+JSONString);
	}

	public IAPProduct(){
		Sku = "";
		CurrencySymbol = "";
		Price = "";
		PriceValue = "";
	}

	public string GetPriceLabel(){
		return Price;
	}

}