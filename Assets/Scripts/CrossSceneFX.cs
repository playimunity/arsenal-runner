﻿using UnityEngine;
using System.Collections;

public class CrossSceneFX : MonoBehaviour {


	public Transform fXParent;
	public Transform bankPos;

	public GameObject coinPrefab;

	GameObject tempCoin;

//	float randomX;
//	float randomY;

	int[] posNeg = new int[] {1,-1};
	int randomPosNeg;

	Vector2 coinNewPos;

	WaitForEndOfFrame _waitEndFrame = new WaitForEndOfFrame();

	float startTime;

	Transform TMPCoinTransform;

	void Start () {
		coinNewPos = new Vector2 (0f, 0f);
	}

	public void MultyCoinsFX(int amount){
		startTime = Time.time;
		for (int i = 0; i < amount; i++) {
			tempCoin = (GameObject)Instantiate (coinPrefab, transform.position, transform.rotation);
			TMPCoinTransform = tempCoin.transform;
			TMPCoinTransform.parent = fXParent;

			randomPosNeg = Random.Range (0, 2);
			float randomX = Random.Range (3, 6) * posNeg [randomPosNeg];
			randomPosNeg = Random.Range (0, 2);
			float randomY = Random.Range (3, 6) * posNeg [randomPosNeg];
			coinNewPos.x = randomX;
			coinNewPos.y = randomY;

			StartCoroutine (MultyCoinsFXCorutine(TMPCoinTransform , coinNewPos));
		}
	}


	IEnumerator MultyCoinsFXCorutine(Transform tempCoin , Vector2 coinNewPos){
		
		while ((Time.time - startTime) < 2f) {
			tempCoin.localPosition = Vector2.Lerp (tempCoin.localPosition, coinNewPos, 5f * Time.deltaTime);
			yield return _waitEndFrame;
		}

		Destroy (tempCoin);
	}
}
