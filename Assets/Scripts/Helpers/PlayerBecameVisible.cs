﻿using UnityEngine;
using System.Collections;

public class PlayerBecameVisible : MonoBehaviour {

	public Player playerScript;




	void OnBecameVisible() {
		playerScript.enabled = true;
	}


	void OnBecameInvisible() {
		playerScript.enabled = false;
	}



}
