using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class RunManager : MonoBehaviour {

	public static RunManager instance;


	const float IMAGE_UPDATE_INTERVAL = 0.1f;

	//public static WorldSection.RegionType currentRegion = WorldSection.RegionType.HR;

	public Image x2CoinImage;
	public Image magnetImage;
	public Image speedBoostImage;
	public Image sheildImage;

	public GameObject speedBoostsBtn;
	public GameObject magnetsBtn;
	public GameObject x2CoinsBtn;
	public GameObject sheildsBtn;

//	public Transform powerUpBtnPos1;
//	public Transform powerUpBtnPos2;
//	public Transform powerUpBtnPos3;

	public Text sheildBtnValueTxt;
	public Text speedBoostBtnValueTxt;
	public Text magnetsBtnValueTxt;
	public Text x2CoinsBtnValueTxt;

	public RectTransform powerUpX2FillImage;
	public RectTransform powerUpSpeedBoostFillImage;
	public RectTransform powerUpSheildFillImage;
	public RectTransform powerUpMagnetFillImage;

	public List<RectTransform> activePowerUpsBars;

	Vector2 powerUpX2FillSize;
	Vector2 powerUpSpeedBoostFillSize;
	Vector2 powerUpSheildFillSize;
	Vector2 powerUpMagnetFillSize;

	public Transform powerUpBarOnParent;
	public Transform powerUpBarOffParent;


	Vector2 powerUpFillBarSize;
	float powerUpFillBarWidth;

//	bool haveGodMode = false;
//	bool haveMagnets = false;
//	bool haveX2 = false;
	public bool saveMeOn = false;
	public bool wasSecretArea = false;

//	Vector3 powerUpMagnetFillRot;
//	Vector3 powerUpGodFillRot;
//	Vector3 powerUpX2FillRot;

//	public Image powerUpX2Icon;
//	public Image powerUpAndrenalineIcon;
//	public Image powerUpMagnetIcon;

//	public Transform fillParent1;
//	public Transform fillParent2;
//	public Transform fillParent3;
//	public Transform sparklesParent;

	public Animator powerUpBarAnimator;

	Transform tempSmallSparkle;

	public Transform godSparklePrefab;
	//public Transform magnetSparklePrefab;
	//public Transform x2SparklePrefab;

//	public List<Transform> godSparklesList;
//	public List<Transform> magnetSparklesList;
//	public List<Transform> x2SparklesList;
	public List<Transform> sparklesList;

//	public Transform godSparklesParent;
//	public Transform magnetSparklesParent;
//	public Transform x2SparklesParent;

//	public Color godSparkleColor;
//	public Color magnetSparkleColor;
//	public Color x2SparkleColor;

	WaitForSeconds magnet_counter_interval;
	WaitForSeconds sheild_counter_interval;
	WaitForSeconds speed_boost_counter_interval;
	WaitForSeconds x2_coins_counter_interval;
	
	float SheildTimeRemain = 0f;
	float SheildTimeStep = 0f;
	float MagnetTimeRemain = 0f;
	float MagnetTimeStep = 0f;
	float SpeedBoostTimeRemain = 0f;
	float SpeedBoostTimeStep = 0f;
	float x2CoinsTimeRemain = 0f;
	float x2CoinsTimeStep = 0f;

//	public int SpeedBoostActiveTime = DAO.Settings.SpeedBoostActiveTime;
	//public int x2CoinsActiveTime = DAO.Settings.X2ActiveTime;
	public int coinsCollectedInThisRun = 0;

	public bool PowerupsAvailable = true;

	public static int DecreaseFitnessToPlayer = 0;
	public static int LastSectionNumber = 1;



	//bool godModeFeedback = false;
	bool startSheildEnd = false;
	bool startSpeedBoostEnd = false;

	private bool magnetIsActive = false;
	public bool MagnetIsActive{
		get{ return magnetIsActive; }
	}
	
	private bool sheildIsActive = false;
	public bool SheildIsActive{
		get{ return sheildIsActive; }
	}

	private bool speedBoostIsActive = false;
	public bool SpeedBoostIsActive{
		get{ return speedBoostIsActive; }
	}

	private bool x2CoinsIsActive = false;
	public bool X2CoinsIsActive{
		get{ return x2CoinsIsActive; }
	}





	void Awake(){
		instance = this;
		LastSectionNumber = 1;
	}

	void Start(){
		LastSectionNumber = 1;
		DecreaseFitnessToPlayer = 0;
		magnet_counter_interval = new WaitForSeconds (IMAGE_UPDATE_INTERVAL);
		sheild_counter_interval = new WaitForSeconds (IMAGE_UPDATE_INTERVAL);
		speed_boost_counter_interval = new WaitForSeconds (IMAGE_UPDATE_INTERVAL);
		x2_coins_counter_interval = new WaitForSeconds (IMAGE_UPDATE_INTERVAL);
		totalMeters = 0;
		//SetPowerUpBtns ();
		wasSecretArea = false;

		if(DAO.TotalX2CoinsCollected >= 1){
			x2CoinsBtn.SetActive (true);
			x2CoinsBtnValueTxt.text = DAO.TotalX2CoinsCollected.ToString();
		}
		if(DAO.TotalMagnetsCollected >= 1){
			magnetsBtn.SetActive (true);
			magnetsBtnValueTxt.text = DAO.TotalMagnetsCollected.ToString();
		}
		if(DAO.TotalSheildsCollected >= 1){
			sheildsBtn.SetActive (true);
			sheildBtnValueTxt.text = DAO.TotalSheildsCollected.ToString();
		}
		if(DAO.TotalSpeedBoostsCollected >= 1){
			speedBoostsBtn.SetActive (true);
			speedBoostBtnValueTxt.text = DAO.TotalSpeedBoostsCollected.ToString();
		}

		powerUpFillBarSize = powerUpX2FillImage.sizeDelta;
		powerUpFillBarWidth = powerUpFillBarSize.x;
	}

	#if UNITY_EDITOR
	void Update(){
		if (Input.GetKeyUp (KeyCode.S)) {
			ActivateSpeedBoostTest ();
		}

		if (Input.GetKeyUp (KeyCode.M)) { // just for testing
			RunManager.instance.ActivateMagnetTest ();
		}

		if (Input.GetKeyUp (KeyCode.G)) { // just for testing
			if(!PlayerController.instance.sheildOn)
				ActivateSheildTest ();
			else
				StopSheild();
		}
		if (Input.GetKeyUp (KeyCode.V)) { // just for testing
			CollectWhistleTest ();
		}

		if (Input.GetKeyUp (KeyCode.X)) {
			ActivateX2Test ();
		}

		if(Input.GetKeyUp (KeyCode.W)){
			
			Run.Instance.runGoal.coinsCollected = 300;
			//Run.Instance.runGoal.enemiesTackled = 7;
			//Run.Instance.runGoal.energyDrinksCollected = 20;
			Run.Instance.runGoal.hits = 0;
			Run.Instance.runGoal.spaceShipsDown = 8;
			//EndSectionAchieved ();
			PlayerController.instance.EndRun();
			StartCoroutine(showEndRunResults ());
		}
	}
	#endif

	    // ---------- TESTS ----------   :
	public void ActivateMagnetTest(){
		ActivateMagnet (false);
	}

	public void ActivateSheildTest(){
		ActivateSheild (false);
	}
    public void CollectWhistleTest()
    {
        PlayerController.instance.IncreaseHits();
    }

	public void ActivateSpeedBoostTest(){
		AudioManager.Instance.OnPowerUpBTN ();
		ActivateSpeedBoost (false);
	}

	public void ActivateX2Test(){
		ActivateX2Coins (false);
	}

	//--------------------------------------------

	public static void OnMagnetCollected(){   // the collectable call:
		//DAO.TotalMagnetsCollected++;

		GameAnalyticsWrapper.PowerUpCollectedOnTutorial ();
		RunManager.instance.ActivateMagnet (false);
		DAO.MagnetsUsed++;
		if (DAO.MagnetsUsed == 10) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_come_to_papa);
		} else if (DAO.MagnetsUsed == 50) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_make_it_rain);
		} else if (DAO.MagnetsUsed == 100) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_money_magnet);
		}

		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.POWERUP_PICKED );
	}

	public void ActivateMagnet(){  // the btn call:
		ActivateMagnet (true);
	}
		
	public void ActivateMagnet(bool reduceAmount = true){
		if ( (DAO.TotalMagnetsCollected < 1 && reduceAmount) || !PowerupsAvailable) return;

		if (magnetIsActive) {
			StopCoroutine ("ProcessMagnet");
		}
			
		AudioManager.Instance.OnMagnetPowerUp ();

		magnetIsActive = true;

		if (reduceAmount) {
			AudioManager.Instance.OnPowerUpBTN ();
			DAO.TotalMagnetsCollected--;
			magnetsBtnValueTxt.text = DAO.TotalMagnetsCollected.ToString();
		}

		PlayerController.instance.ActivateMagnet (true);

		StartCoroutine ("ProcessMagnet");
	}

	IEnumerator ProcessMagnet(){
		MagnetTimeStep = 1f / ((float)DAO.Settings.MagnetActiveTime * PlayerController.instance.Perk_MagnetTimeFactor) * IMAGE_UPDATE_INTERVAL;
		MagnetTimeRemain = (float)DAO.Settings.MagnetActiveTime * PlayerController.instance.Perk_MagnetTimeFactor;

		magnetImage.gameObject.SetActive (true);
		magnetImage.fillAmount = 1f;

		PowerUpFillBarAnimToggle (true);
		powerUpMagnetFillImage.sizeDelta = powerUpFillBarSize;
		powerUpMagnetFillSize = powerUpFillBarSize;
//		powerUpMagnetFillImage.parent = powerUpBarOffParent;
//		powerUpMagnetFillImage.parent = powerUpBarOnParent;
//		activePowerUpsBars.Add (powerUpSheildFillImage);
		AddNewPoewrUpBar(powerUpMagnetFillImage);

		ActivateCoinCounter ();

		while (MagnetTimeRemain > 0) {
			yield return magnet_counter_interval;
			MagnetTimeRemain -= IMAGE_UPDATE_INTERVAL;
			magnetImage.fillAmount -= MagnetTimeStep;
			powerUpMagnetFillSize.x -= MagnetTimeStep * powerUpFillBarWidth;
			powerUpMagnetFillImage.sizeDelta = powerUpMagnetFillSize;
		}

		// deactivate god mode

		magnetIsActive = false;

		PlayerController.instance.ActivateMagnet (false);

		if (!MagnetIsActive && !SheildIsActive && !SpeedBoostIsActive && !X2CoinsIsActive) {
			PowerUpFillBarAnimToggle (false);
		}
//		powerUpMagnetFillImage.parent = powerUpBarOffParent;
		RemoveFinishedPoewrUpBar (powerUpMagnetFillImage);
		magnetImage.gameObject.SetActive(false);
		if(DAO.TotalMagnetsCollected < 1){
			magnetsBtn.SetActive (false);
		}

	}





	public static void OnSheildCollected(){  // the collectable call:
		
		GameAnalyticsWrapper.PowerUpCollectedOnTutorial ();
		RunManager.instance.ActivateSheild (false);
//		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
//			Run.Instance.runGoal.energyDrinksCollected++;
//		}
		DAO.SheildsUsed++;
		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.POWERUP_PICKED );
	}
		

	public void ActivateSheild(){  // the btn call
		ActivateSheild (true);
	}
	
	public void ActivateSheild(bool reduceAmount = true){
		if ((DAO.TotalSheildsCollected < 1 && reduceAmount) || !PowerupsAvailable) return;

		if (SheildIsActive) {
			StopCoroutine ("ProcessSheild");
			saveMeOn = false;
		}
			
		startSheildEnd = false;  
			
		sheildIsActive = true;
		PlayerController.instance.Sheild ();
		AudioManager.Instance.OnPowerUpBTN ();

//		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN && !saveMeOn) {
//			Run.Instance.runGoal.SheildUsed = true;
//		}

		if (reduceAmount) {
			AudioManager.Instance.OnPowerUpBTN ();
			DAO.TotalSheildsCollected--;
			sheildBtnValueTxt.text = DAO.TotalSheildsCollected.ToString();
		}

		StartCoroutine ("ProcessSheild");

	}
	
	IEnumerator ProcessSheild(){
		if (!saveMeOn) {
		SheildTimeStep = 1f / ((float)DAO.Settings.SheildActiveTime * PlayerController.instance.Perk_EnergyTimeFactor) * IMAGE_UPDATE_INTERVAL;
		SheildTimeRemain = (float)DAO.Settings.SheildActiveTime * PlayerController.instance.Perk_EnergyTimeFactor;
//			SheildTimeStep = 1f / (30f * PlayerController.instance.Perk_EnergyTimeFactor) * IMAGE_UPDATE_INTERVAL;
//			SheildTimeRemain = 30f * PlayerController.instance.Perk_EnergyTimeFactor;
		} else {
			SheildTimeStep = 1f / (5f * IMAGE_UPDATE_INTERVAL * 100f);
			SheildTimeRemain = 5f;
		}

		sheildImage.gameObject.SetActive (true);
		sheildImage.fillAmount = 1f;

		PowerUpFillBarAnimToggle (true);
		powerUpSheildFillImage.sizeDelta = powerUpFillBarSize;
		powerUpSheildFillSize = powerUpFillBarSize;
		//powerUpSheildFillImage.parent = powerUpBarOffParent;
		//powerUpSheildFillImage.parent = powerUpBarOnParent;
		//activePowerUpsBars.Add (powerUpSheildFillImage);
		AddNewPoewrUpBar(powerUpSheildFillImage);

		while (SheildTimeRemain > 0) {
			yield return sheild_counter_interval;
			SheildTimeRemain -= IMAGE_UPDATE_INTERVAL;
			sheildImage.fillAmount -= SheildTimeStep;
			//Debug.Log ("alon__________  SheildTimeStep: " + SheildTimeStep + " , powerUpFillBarWidth:" + powerUpFillBarWidth);
			powerUpSheildFillSize.x -= SheildTimeStep * powerUpFillBarWidth;
			//Debug.Log ("alon__________  powerUpSheildFillSize size: " + powerUpSheildFillSize.x + " , " + powerUpSheildFillSize.y);
			powerUpSheildFillImage.sizeDelta = powerUpSheildFillSize;
			//Debug.Log ("alon__________  powerUpSheildFillImage size: " + powerUpSheildFillImage.sizeDelta);
//			if (!startSheildEnd && SheildTimeRemain < 2f) {
//				startSheildEnd = true;
//				PlayerController.instance.StartSheildEnd ();
//			}				
		}

		// deactivate god mode

		PlayerController.instance.UnSheild();
		sheildIsActive = false;
		startSheildEnd = false;
		saveMeOn = false;

		if (!MagnetIsActive && !SheildIsActive && !SpeedBoostIsActive && !X2CoinsIsActive) {
			PowerUpFillBarAnimToggle (false);
		}
		//powerUpSheildFillImage.parent = powerUpBarOffParent;
		RemoveFinishedPoewrUpBar (powerUpSheildFillImage);
		sheildImage.gameObject.SetActive(false);
		if(DAO.TotalSheildsCollected < 1){
			sheildsBtn.SetActive (false);
		}
	}


	public void StopSheild(){
		StopCoroutine ("ProcessSheild");

		PlayerController.instance.StartSheildEnd();
		//PlayerController.instance.UnSheild();
		sheildIsActive = false;
		startSheildEnd = false;
		saveMeOn = false;

		if (!MagnetIsActive && !SheildIsActive && !SpeedBoostIsActive && !X2CoinsIsActive) {
			PowerUpFillBarAnimToggle (false);
		}
		//powerUpSheildFillImage.parent = powerUpBarOffParent;
		RemoveFinishedPoewrUpBar (powerUpSheildFillImage);
		sheildImage.gameObject.SetActive(false);
		if(DAO.TotalSheildsCollected < 1){
			sheildsBtn.SetActive (false);
		}
	}





	public static void OnSpeedBoostCollected(){  // the collectable call:
        RunManager.instance.ActivateSpeedBoost(false);
		GameAnalyticsWrapper.PowerUpCollectedOnTutorial ();
//		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
//			Run.Instance.runGoal.energyDrinksCollected++;
//		}
		RunManager.instance.ActivateSpeedBoost (false);
		DAO.SheildsUsed++;
		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.POWERUP_PICKED );
	}
		
	public void ActivateSpeedBoost(){  // the btn call
		ActivateSpeedBoost (true);
	}

	public void ActivateSpeedBoost(bool reduceAmount = true){
		if ((DAO.TotalSpeedBoostsCollected < 1 && reduceAmount) || !PowerupsAvailable) return;

		if (speedBoostIsActive) {
			StopCoroutine ("ProcessSpeedBoost");
			saveMeOn = false;
		}

		startSpeedBoostEnd = false;  
		speedBoostIsActive = true;
		PlayerController.instance.SpeedBoost ();
		AudioManager.Instance.OnPowerUpBTN ();

//		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN && !saveMeOn) {
//			Run.Instance.runGoal.SpeedBoostUsed = true;
//		}

		if (reduceAmount) {
			AudioManager.Instance.OnPowerUpBTN ();
			DAO.TotalSpeedBoostsCollected--;
			speedBoostBtnValueTxt.text = DAO.TotalSpeedBoostsCollected.ToString();
		}

		StartCoroutine ("ProcessSpeedBoost");

	}

	IEnumerator ProcessSpeedBoost(){
		if (!saveMeOn) {
			SpeedBoostTimeStep = 1f / ((float)DAO.Settings.SpeedBoostActiveTime * PlayerController.instance.Perk_EnergyTimeFactor) * IMAGE_UPDATE_INTERVAL;
			SpeedBoostTimeRemain = (float)DAO.Settings.SpeedBoostActiveTime * PlayerController.instance.Perk_EnergyTimeFactor;
		} else {
			SheildTimeStep = 1f / (3f * IMAGE_UPDATE_INTERVAL * 100f);
			SheildTimeRemain = 3f;
		}

		speedBoostImage.gameObject.SetActive (true);
		speedBoostImage.fillAmount = 1f;

		PowerUpFillBarAnimToggle (true);
		powerUpSpeedBoostFillImage.sizeDelta = powerUpFillBarSize;
		powerUpSpeedBoostFillSize = powerUpFillBarSize;
//		powerUpSpeedBoostFillImage.parent = powerUpBarOffParent;
//		powerUpSpeedBoostFillImage.parent = powerUpBarOnParent;
		AddNewPoewrUpBar(powerUpSpeedBoostFillImage);

		while (SpeedBoostTimeRemain > 0) {
			yield return speed_boost_counter_interval;
			SpeedBoostTimeRemain -= IMAGE_UPDATE_INTERVAL;
			speedBoostImage.fillAmount -= SpeedBoostTimeStep;
			powerUpSpeedBoostFillSize.x -= SpeedBoostTimeStep * powerUpFillBarWidth;
			powerUpSpeedBoostFillImage.sizeDelta = powerUpSpeedBoostFillSize;

			if (!startSpeedBoostEnd && SpeedBoostTimeRemain < 2f) {
				startSpeedBoostEnd = true;
				PlayerController.instance.StartSpeedBoostEnd ();
			}
		}
			
		// deactivate god mode
	
		PlayerController.instance.UnSpeedBoost();
		speedBoostIsActive = false;
		startSpeedBoostEnd = false;
		saveMeOn = false;

		if (!MagnetIsActive && !SheildIsActive && !SpeedBoostIsActive && !X2CoinsIsActive) {
			PowerUpFillBarAnimToggle (false);
		}
		//powerUpSpeedBoostFillImage.parent = powerUpBarOffParent;
		RemoveFinishedPoewrUpBar (powerUpSpeedBoostFillImage);
		speedBoostImage.gameObject.SetActive(false);
		if(DAO.TotalSpeedBoostsCollected < 1){
			speedBoostsBtn.SetActive (false);
		}
	}






	public static void OnX2CoinsCollected(){  // the collectable call:
		GameAnalyticsWrapper.PowerUpCollectedOnTutorial ();
		RunManager.instance.ActivateX2Coins (false);
	}

	public void ActivateX2Coins(){  // the btn call:
		ActivateX2Coins(true);
	}

	public void ActivateX2Coins(bool reduceAmount = true){
		if ( (reduceAmount && DAO.TotalX2CoinsCollected < 1  ) || !PowerupsAvailable) return;

		if (x2CoinsIsActive) {
			StopCoroutine ("ProcessX2Coins");
		}

		x2CoinsIsActive = true;
		Coin.isX2 = true;

		AudioManager.Instance.X2PowerUp ();

		StartCoroutine ("ProcessX2Coins");

		if (reduceAmount) {
			AudioManager.Instance.OnPowerUpBTN ();
			DAO.TotalX2CoinsCollected--;
			x2CoinsBtnValueTxt.text = DAO.TotalX2CoinsCollected.ToString();
		}
	}


	IEnumerator ProcessX2Coins(){

		x2CoinsTimeStep = 1f / (float)DAO.Settings.X2ActiveTime * IMAGE_UPDATE_INTERVAL;
		x2CoinsTimeRemain = (float)DAO.Settings.X2ActiveTime;

		x2CoinImage.gameObject.SetActive (true);
		x2CoinImage.fillAmount = 1f;

		PowerUpFillBarAnimToggle (true);
		powerUpX2FillImage.sizeDelta = powerUpFillBarSize;
		powerUpX2FillSize = powerUpFillBarSize;
		//powerUpX2FillImage.parent = powerUpBarOffParent;
		//powerUpX2FillImage.parent = powerUpBarOnParent;
		//activePowerUpsBars.Add (powerUpX2FillImage);
		AddNewPoewrUpBar (powerUpX2FillImage);

		while (x2CoinsTimeRemain > 0) {
			yield return x2_coins_counter_interval;
			x2CoinsTimeRemain -= IMAGE_UPDATE_INTERVAL;
			x2CoinImage.fillAmount -= x2CoinsTimeStep;
			powerUpX2FillSize.x -= x2CoinsTimeStep * powerUpFillBarWidth;
			powerUpX2FillImage.sizeDelta = powerUpX2FillSize;

		}
			
		if (!MagnetIsActive && !SheildIsActive && !SpeedBoostIsActive && !X2CoinsIsActive) {
			PowerUpFillBarAnimToggle (false);
		}
		//powerUpX2FillImage.parent = powerUpBarOffParent;
		RemoveFinishedPoewrUpBar (powerUpX2FillImage);
		x2CoinImage.gameObject.SetActive(false);
		if(DAO.TotalSpeedBoostsCollected < 1){
			x2CoinsBtn.SetActive (false);
		}	

		Coin.isX2 = false;
		x2CoinsIsActive = false;
	}




	bool powerUpFillBarOn = false;
	public void PowerUpFillBarAnimToggle(bool on){
		if (on && !powerUpFillBarOn) {
			powerUpBarAnimator.Play ("Run_PowerUpBar_in");
			powerUpFillBarOn = true;
		} else if (!on && powerUpFillBarOn) {
			powerUpBarAnimator.Play ("Run_PowerUpBar_out");
			powerUpFillBarOn = false;
		}
	}


	public void AddNewPoewrUpBar(RectTransform bar){
		activePowerUpsBars.Add (bar);
		for (int i = activePowerUpsBars.Count - 1; i >= 0; i--) {
			activePowerUpsBars[i].parent = powerUpBarOffParent;
			activePowerUpsBars[i].parent = powerUpBarOnParent;
		}
	}

	public void RemoveFinishedPoewrUpBar(RectTransform bar){
		for (int i = 0; i < activePowerUpsBars.Count; i++) {
			if (activePowerUpsBars [i] == bar) {
				activePowerUpsBars [i].parent = powerUpBarOffParent;
				activePowerUpsBars.Remove (bar);
			}
		}
	}





	public bool fiveSectionsToEnd = false;

	public static void OnSectionAchieved(int direction, bool Is_T_Section, WorldSection.RegionType region){

		LastSectionNumber++;

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) CupMapBuilder.OnSectionAchieved (direction, Is_T_Section, region);
		else if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN || GameManager.curentGameState == GameManager.GameState.TEST_RUN ) TutorialMapBuilder.OnSectionAchieved (direction, Is_T_Section, region);
		else if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) InfinityMapBuilder.OnSectionAchieved (direction, Is_T_Section, region);

		// Pre End Crowd SFX: 
		if(GameManager.curentGameState == GameManager.GameState.CUP_RUN && !RunManager.instance.fiveSectionsToEnd && LastSectionNumber >= Run.Instance.sections.Count - 10){
			RunManager.instance.fiveSectionsToEnd = true;
//			AudioManager.Instance.PreperToEndRunCrowd (true);
		}

	}


	public static void OnCoinCollected(int coinAmount){
		DAO.TotalCoinsCollected += coinAmount;

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN || GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN)
			Run.Instance.runGoal.coinsCollected += coinAmount;
		else if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
			PracticeRunController.Instance.CoinsCollected += coinAmount;
		}

		RunManager.instance.coinsCollectedInThisRun += coinAmount;

		CrossSceneUIHandler.Instance.UpdateCoinsAmount();
	}

	public int coinsCounter = 0;
	public int maxCoinsCounter = 0;
	WaitForSeconds coinsCounterWait = new WaitForSeconds(2f);
	WaitForEndOfFrame _waitEndFrame = new WaitForEndOfFrame();
	//bool magnetOn = false;
	bool massiveCoinAchived = false;

	public void CoinCounter(){ //every time you take a coin while in a magnet mode:
		StartCoroutine (MassiveCoinCounter());
	}

	IEnumerator MassiveCoinCounter(){
		coinsCounter++;
		yield return coinsCounterWait;
		coinsCounter--;
	}

	public void ActivateCoinCounter(){ // at the moment you take a magnet
		StopCoroutine ("ProcessCoinCounter");
		//magnetOn = true;
		StartCoroutine ("ProcessCoinCounter");
	}



	IEnumerator ProcessCoinCounter(){
		while (magnetIsActive) {
			if (!massiveCoinAchived && coinsCounter >= 15) {
				massiveCoinAchived = true;
				AudioManager.Instance.PlayerWoohoo ();
			} else if (massiveCoinAchived && coinsCounter < 5) {
				massiveCoinAchived = false;
			}
			if (coinsCounter > maxCoinsCounter) {
				maxCoinsCounter = coinsCounter;
			}

			yield return _waitEndFrame;
		}
		massiveCoinAchived = false;
	}



	public static void OnHitPlayer(){
		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) Run.Instance.runGoal.hits++;
	}

	public static void OnTargetShotByBall(){
		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) Run.Instance.runGoal.spaceShipsDown++;
//		Debug.Log ("alon__________ spaceShip down: " + Run.Instance.runGoal.spaceShipsDown);
	}

	static bool highScoreAchived = false;
	public static int totalMeters = 0;
	public static void OnDistancePassed( int meters ){
		if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
			PlayerController.instance.data.UpdateMetersToNextLevel (meters);
			if (!highScoreAchived && totalMeters > DAO.BestPracticeScore && DAO.PlayPracticePopup != 0) {
				highScoreAchived = true;
				GameManager.Instance.needToShowRateUsPopup = true;
				PracticeRunController.Instance.NewHighScore ();
			}
		}
		totalMeters += meters;

	}

	public static void OnTackleEnemy(){
		//if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) Run.Instance.runGoal.enemiesTackled++;
	}

	public void StopProcesses(){
		SheildTimeRemain = 0;
		MagnetTimeRemain = 0;
		x2CoinsTimeRemain = 0;
		SpeedBoostTimeRemain = 0;
	}

	public GameObject endingScene;
    public EndSceneManager endSceneManager;
	public Transform endingSceneTransform;
	//public EndSceneManager endingSceneScript;

	public void SetEndingScenePosition(Transform endSectionTransform){
		endingScene.SetActive (true);      
		endingSceneTransform.parent = endSectionTransform;
		endingSceneTransform.localPosition = Vector3.zero;
		endingSceneTransform.localEulerAngles = Vector3.zero;
		//endingSceneScript.SetPosition (endSectionTransform);
	}

	public enum EndingType{WIN_CANNON , WIN_BALL , LOOSE};
	public EndingType endingType = EndingType.WIN_CANNON;

	public void PreEndSectionAchieved(){
		StopProcesses ();
		PowerupsAvailable = false;

		if (GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
			if (endingType == EndingType.WIN_CANNON) {
				PlayerController.instance.PrepareToEnd ();
				//EndSceneManager.instance.ActivateWinningEvent ();
				EndSceneManager.instance.ActivateCannonScene ();
				AudioManager.Instance.OnEndRunCrowd ();
//				Debug.Log ("alon________WinningEvent ()");
			} else if (endingType == EndingType.WIN_BALL) {
				PlayerController.instance.PrepareToEnd ();
				//EndSceneManager.instance.ActivateWinningEvent ();
				EndSceneManager.instance.ActivateShootingScene();
				AudioManager.Instance.OnEndRunCrowd ();
//				Debug.Log ("alon________WinningEvent ()");
			} else {
				PlayerController.instance.PrepareToEnd ();
				EndSceneManager.instance.ActivateLoosingScene ();
//				Debug.Log ("alon________ ActivateLoosingScene ()");
			}
			return;
		}

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			Run.Instance.running = false;
		}


		if (Run.Instance.runGoal.medalsWon () <= 0) {
			//PlayerController.instance.EndRun (false);
			//StartCoroutine (showEndRunResults ());
			PlayerController.instance.PrepareToEnd ();
			EndSceneManager.instance.ActivateLoosingScene ();
//			Debug.Log ("alon________ ActivateLoosingScene ()");
		} else {
			AudioManager.Instance.OnEndRunCrowd ();
			PlayerController.instance.PrepareToEnd ();
			endSceneManager.ActivateWinningEvent ();
//			Debug.Log ("alon________WinningEvent ()");
		}
	}

	public void EndSectionAchieved(){

		if (GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
			if (endingType == EndingType.WIN_BALL || endingType == EndingType.WIN_CANNON) {
				PlayerController.instance.EndRun();
//				Debug.Log ("alon________ playing end run win");
			} else {
				PlayerController.instance.EndRun (false);
//				Debug.Log ("alon________ playing end run failed");
			}
			EndSceneManager.instance.ChosenWinningEvent ();
			return;
		}

		if (Run.Instance.runGoal.medalsWon () <= 0) {
			PlayerController.instance.EndRun (false);
//			Debug.Log ("alon________ playing end run failed");
		} else {
			PlayerController.instance.EndRun();
//			Debug.Log ("alon________ playing end run win");
		}
		EndSceneManager.instance.ChosenWinningEvent ();
		//AudioManager.Instance.OnEndSection ();

		//StartCoroutine ( showEndRunResults() );
	}



	public void OnPlayerDie(){

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			Run.Instance.running = false;
			Run.Instance.runGoal.playerDie = true;
			//StartCoroutine ( showEndRunResults(true) );
			CupRunController.Instance.ShowSaveMeScreen ();

		} else if(GameManager.curentGameState == GameManager.GameState.INFINITY_RUN){
			PracticeRunController.Instance.running = false;
			PracticeRunController.Instance.ShowSaveMeScreen ();

		} else if(GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN){
			CupRunController.Instance.FailedScrn(true);

			if (PlayerController.instance.hardDamage) {
				UnitedAnalytics.LogEvent ("failed in tutorial", "hard", "", RunManager.LastSectionNumber);
			} else {
				UnitedAnalytics.LogEvent ("failed in tutorial", "soft", "", RunManager.LastSectionNumber);
			}
//			if (GameManager.Instance.isPlayscapeLoaded) {
				//BI._.Flow_FTUE (BI.FLOW_STEP.TUTORIAL_FAILED);
//				BI._.Wallet_Deposit (Run.Instance.runGoal.coinsCollected, "Tutorial Failed", "FCB Coins", "FTUE", "13", "");
//			}
			GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,Run.Instance.runGoal.coinsCollected, GameAnalyticsWrapper.FINISHED_RUN,"TutorialFailed");
		}else {
			CameraFollow.instance.GrayScale ();
			GameManager.SwitchState (GameManager.GameState.GAME_OVER);
		}
	}


	public void OnSaveMeDeclined(){
		StartCoroutine ( showEndRunResults(true) );
	}

	public IEnumerator showEndRunResults(bool crash = false){
		

		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			yield return new WaitForSeconds (1f);
			if (GameManager.Instance.isPlayscapeLoaded) {
				BI._.Flow_FTUE (BI.FLOW_STEP.TUTORIAL_COMPLETED);
				//BI._.Wallet_Deposit (Run.Instance.runGoal.coinsCollected, "Tutorial Completed", "FCB Coins", "FTUE", "14", "");
			}
			GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,Run.Instance.runGoal.coinsCollected, GameAnalyticsWrapper.FINISHED_RUN,"TutorialCompleted");
			GameAnalyticsWrapper.DesignEvent(GameAnalyticsWrapper.FLOW_STEP.COLLECTED_COINS);
			Tutorial.Instance.FinishMassage1 ();

			yield break;
		}
		if (GameManager.curentGameState != GameManager.GameState.CUP_RUN) {
			yield break;
		}
			int runId;
			if (!GameManager.ActiveCupCompleted) {
				runId = (GameManager.ActiveCup.id + 1) * 100 + (GameManager.CurrentRun.IndexInQuest + 1);
			} else {
				runId = (GameManager.ActiveCompletedCup.id + 1) * 100 + (GameManager.CurrentRun.IndexInQuest + 1);
			}
			if (DAO.maxRunID < runId)
				DAO.maxRunID = runId;

		//Debug.Log ("alon________________ RunManager - showEndRunResults() - runId: " + runId); 
		if (crash) {
			yield return new WaitForSeconds (0.1f);
			CupRunController.Instance.SetWonScreen (-1);

			//string runIdString = "0" + GameManager.ActiveCup.id + "0"

			if (PlayerController.instance.hardDamage) {
				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.QuestRun_Failed (runId, Run.Instance.runGoal.coinsCollected, "hard", RunManager.LastSectionNumber);
				GameAnalyticsWrapper.ProgressionEvent (GameAnalyticsWrapper.progerssionTypes.Fail, runId.ToString(), "QuestRun", "HardDamage", 0);

			} else {
				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.QuestRun_Failed (runId, Run.Instance.runGoal.coinsCollected, "soft", RunManager.LastSectionNumber);
				GameAnalyticsWrapper.ProgressionEvent (GameAnalyticsWrapper.progerssionTypes.Fail, runId.ToString(), "QuestRun", "SoftDamage", 0);
			}

		} else {
			yield return new WaitForSeconds (3f);
			CupRunController.Instance.SetWonScreen (Run.Instance.runGoal.medalsWon ());
			//if (GameManager.Instance.isPlayscapeLoaded) {
				if (Run.Instance.runGoal.medalsWon () == 0) {
					GameAnalyticsWrapper.ProgressionEvent (GameAnalyticsWrapper.progerssionTypes.Fail, runId.ToString(), "QuestRun", "ObjectiveFail", 0);
					//BI._.Wallet_Deposit (Run.Instance.runGoal.coinsCollected, "Quest Run Failed", "FCB Coins", "", "", "");
					GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,Run.Instance.runGoal.coinsCollected, GameAnalyticsWrapper.FINISHED_RUN,"QuestFailed");
				} else {
					//BI._.QuestRun_Completed (runId, Run.Instance.runGoal.coinsCollected, (Run.Instance.runGoal.medalsWon () == 1) ? "Silver" : "Gold");
					GameAnalyticsWrapper.ProgressionEvent (GameAnalyticsWrapper.progerssionTypes.Complete, runId.ToString(), "QuestRun", (Run.Instance.runGoal.medalsWon () == 1) ? "Silver" : "Gold", 0);
					//	Debug.Log ("alon__________ IndexInQuest: " + GameManager.CurrentRun.IndexInQuest + "   CurrentRun.id: " + GameManager.CurrentRun.id);
					//	Debug.Log ("alon__________ ActiveCup.id: " + GameManager.ActiveCup.id);
				}
			//}
		}



		CupRunController.Instance.ShowWonScreen ();


		if (PlayerController.instance.Perk_MoneyBonusIfRunCompleted > 0 && Run.Instance.runGoal.medalsWon () > 0) {
			DAO.TotalCoinsCollected += PlayerController.instance.Perk_MoneyBonusIfRunCompleted;
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (PlayerController.instance.Perk_MoneyBonusIfRunCompleted, "Extra Coins For Player Skill", "FCB Coins", "", "", "");
			GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,PlayerController.instance.Perk_MoneyBonusIfRunCompleted, GameAnalyticsWrapper.FINISHED_RUN,"SkillBonus");
		}

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			if (Run.Instance.runGoal.medalsWon () == 1) {
				DAO.TotalCoinsCollected += 50;
				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (Run.Instance.runGoal.coinsCollected + 50, "Quest Run won Silver", "FCB Coins", "", "", "");
				GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,Run.Instance.runGoal.coinsCollected + 50, GameAnalyticsWrapper.FINISHED_RUN,"SilverWon");
			} else if (Run.Instance.runGoal.medalsWon () == 2) {
				DAO.TotalCoinsCollected += 100;
				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit (Run.Instance.runGoal.coinsCollected + 100, "Quest Run won Gold", "FCB Coins", "", "", "");
				GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,Run.Instance.runGoal.coinsCollected + 100, GameAnalyticsWrapper.FINISHED_RUN,"GoldWon");
			}
		}

		//CrossSceneUIHandler.Instance.UpdateCoinsAmount();

		//Debug.Log ("alon________________ GameManager.ActiveCup status: " + GameManager.ActiveCup.status.ToString ());
	}

	public GameObject fakedWonScrnPanel;
	bool fakedWonScrnOn = false;
	public void FakeWonScrnBtnToggle(){
		fakedWonScrnOn = !fakedWonScrnOn;
		if (fakedWonScrnOn){
			fakedWonScrnPanel.SetActive (true);
		}else{
			fakedWonScrnPanel.SetActive (false);
		}
	}

	//public enum FakedGoal {COLLECT_COINS, COLLECT_ENERGY_DRINKS, COMPLETE_RUN, COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS, DONT_GET_HIT, KICK_BALL_AT_TARGET, TACKLE_ENEMIES};
	//public FakedGoal fakedGoal;
	public void ShowFakedWonScreen(int goal){
		switch (goal) {
		case 1: // collect coins gold
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 150;
				EndSectionAchieved ();
				break;
			}
		case 2: // collect coins silver
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 100;
				EndSectionAchieved ();
				break;
			}
		case 3: // collect energy drink gold
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 300;
				EndSectionAchieved ();
				break;
			}
		case 4: // collect energy drink silver
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 150;
				EndSectionAchieved ();
				break;
			}
		case 5: // complete run gold
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 150;
				EndSectionAchieved ();
				break;
			}
		case 6: // complete run silver
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 150;
				EndSectionAchieved ();
				break;
			}
		case 7: // complete run in less gold
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS;
				Run.Instance.runGoal.playTime = (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_2_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 8: // complete run in less silver
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS;
				Run.Instance.runGoal.playTime = (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_FOR_1_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 9: // dont get hit gold - not in use
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD;
				Run.Instance.runGoal.playTime = (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_2_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 10: // dont get hit silver - not in use
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD;
				Run.Instance.runGoal.playTime = (int)RunGoal.COMPLETE_RUN_IN_LESS_THAN_X_SEC_HARD_FOR_1_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 11: // kick ball gold
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.KICK_BALL_AT_SPACESHIP;
				Run.Instance.runGoal.spaceShipsDown = (int)RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 12: // kick ball silver
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.KICK_BALL_AT_SPACESHIP;
				Run.Instance.runGoal.spaceShipsDown = (int)RunGoal.KICK_BALL_AT_SPACESHIP_FOR_1_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 13: // tackle enemys gold
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.KICK_BALL_AT_SPACESHIP;
				Run.Instance.runGoal.spaceShipsDown = (int)RunGoal.KICK_BALL_AT_SPACESHIP_FOR_2_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 14: // tackle enemys silver
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.KICK_BALL_AT_SPACESHIP;
				Run.Instance.runGoal.spaceShipsDown = (int)RunGoal.KICK_BALL_AT_SPACESHIP_FOR_1_MEDAL;
				EndSectionAchieved ();
				break;
			}
		case 15: // collect coins fail
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 0;
				EndSectionAchieved ();
				break;
			}
		case 16: // collect energy fail
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 0;
				EndSectionAchieved ();
				break;
			}
		case 17: // complete run fail
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COLLECT_COINS;
				Run.Instance.runGoal.coinsCollected = 0;
				EndSectionAchieved ();
				break;
			}
		case 18: // complete run in less fail
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS;
				Run.Instance.runGoal.playTime = 120;
				EndSectionAchieved ();
				break;
			}
		case 19: // dont get hit fail
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.COMPLETE_TRACK_IN_LESS_THAN_X_SECONDS_HARD;
				Run.Instance.runGoal.playTime = 200;
				EndSectionAchieved ();
				break;
			}
		case 20: // kick ball fail
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.KICK_BALL_AT_SPACESHIP;
				Run.Instance.runGoal.spaceShipsDown = 0;
				EndSectionAchieved ();
				break;
			}
		case 21: // tackle enemy fail
			{
				Run.Instance.runGoal.goal = RunGoal.GoalType.KICK_BALL_AT_SPACESHIP;
				Run.Instance.runGoal.spaceShipsDown = 0;
				EndSectionAchieved ();
				break;
			}
		}
	}

	public void HideFakedWonScrn(){
		CupRunController.Instance.HideWonScreen ();
		GM_Input._.CupRunLooseWon = false;
		CrossSceneUIHandler.Instance.sparklesOn = false;
	}


//	public static void OnSpeedBoostCollected(){
//		RunManager.instance.ActivateSpeedBoost ();
//		AudioManager.Instance.SpeedBoost ();
//	}

//	public void ActivateSpeedBoost(){
//
//		if (speedBoostIsActive) {
//			StopCoroutine ("ProcessSpeedBoost");
//		}
//
//		speedBoostIsActive = true;
//		PlayerController.instance.SpeedBoost (true);
//
//		StartCoroutine ("ProcessSpeedBoost");
//	}

//	IEnumerator ProcessSpeedBoost(){
//
//		SpeedBoostTimeStep = 1f / ((float)SpeedBoostActiveTime * IMAGE_UPDATE_INTERVAL);
//		SpeedBoostTimeRemain = (float)SpeedBoostActiveTime;
//
//			while (SpeedBoostTimeRemain > 0) {
//			yield return speed_boost_counter_interval;
//			SpeedBoostTimeRemain -= IMAGE_UPDATE_INTERVAL;
//			//energyImage.fillAmount -= GodModeTimeStep;
//
//		}
//
//		PlayerController.instance.SpeedBoost (false);
//		speedBoostIsActive = false;
//	}



//	public void SetPowerUpBtns(){
//
//		if (DAO.TotalSpeedBoostsCollected > 0) {
//			haveGodMode = true;
//			godModeBtn.gameObject.SetActive (true);
//			godModeBtnValueTxt.text = DAO.TotalSpeedBoostsCollected.ToString ();
//		} else {
//			haveGodMode = false;
//			godModeBtn.gameObject.SetActive (false);
//		}
//
//		if (DAO.TotalMagnetsCollected > 0) {
//			haveMagnets = true;
//			magnetsBtn.gameObject.SetActive (true);
//			magnetsBtnValueTxt.text = DAO.TotalMagnetsCollected.ToString ();
//		} else {
//			haveMagnets = false;
//			magnetsBtn.gameObject.SetActive (false);
//		}
//
//		if (DAO.TotalX2CoinsCollected > 0) {
//			haveX2 = true;
//			x2CoinsBtn.gameObject.SetActive (true);
//			x2CoinsBtnValueTxt.text = DAO.TotalX2CoinsCollected.ToString ();
//		} else {
//			haveX2 = false;
//			x2CoinsBtn.gameObject.SetActive (false);
//		}
//
//
//		// the god mode btn posible situations:
//		if (haveGodMode) {
//			godModeBtn.transform.position = powerUpBtnPos1.position;
//			godModeBtn.transform.parent = powerUpBtnPos1;
//		}
//
//
//		// the magnets btn posible situations:
//		if (haveMagnets && !haveGodMode) {
//			magnetsBtn.transform.position = powerUpBtnPos1.position;
//			magnetsBtn.transform.parent = powerUpBtnPos1;
//		} else if (haveMagnets && haveGodMode) {
//			magnetsBtn.transform.position = powerUpBtnPos2.position;
//			magnetsBtn.transform.parent = powerUpBtnPos2;
//		}
//
//
//		// the x2Coins btn posible situations:
//		if (haveX2 && !haveGodMode && !haveMagnets) {
//			x2CoinsBtn.transform.position = powerUpBtnPos1.position;
//			x2CoinsBtn.transform.parent = powerUpBtnPos1;
//		} else if (haveX2 && (haveGodMode && !haveMagnets) || (!haveGodMode && haveMagnets)) {
//			x2CoinsBtn.transform.position = powerUpBtnPos2.position;
//			x2CoinsBtn.transform.parent = powerUpBtnPos2;
//		} else if (haveX2 && haveGodMode && haveMagnets) {
//			x2CoinsBtn.transform.position = powerUpBtnPos3.position;
//			x2CoinsBtn.transform.parent = powerUpBtnPos3;
//		}
//			
//	}


	bool godModeDebugOn = false;
	public void DebugGodMode(){
		godModeDebugOn = !godModeDebugOn;
		if (godModeDebugOn) {
			PlayerController.instance.speedBoostOn = true;
		} else {
			PlayerController.instance.speedBoostOn = false;
		}
	}

	bool runningDebugOn = false;
	public void DebugRunning(){
		runningDebugOn = !runningDebugOn;
		if (runningDebugOn) {
			PlayerController.instance.isRunning = true;
		} else {
			PlayerController.instance.isRunning = false;
		}
	}

	public void DebugAddBall(){
		PlayerController.instance.CollectBall ();
	}


}
