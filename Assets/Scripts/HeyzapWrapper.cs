using UnityEngine;
using System.Collections;
using Heyzap;
using System.Collections.Generic;

public class HeyzapWrapper : MonoBehaviour
{

	public static bool isPlayingAd;
    public AudioListener al;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public static void Init ()
	{
		HeyzapAds.Start ("923346303d69007471945fcef9050dd5", HeyzapAds.FLAG_NO_OPTIONS);
		HZIncentivizedAd.AdDisplayListener listener = delegate(string adState, string adTag) {
			if (adState.Equals ("incentivized_result_complete")) {
				// The user has watched the entire video and should be given a reward.
			}
			if (adState.Equals ("incentivized_result_incomplete")) {
				// The user did not watch the entire video and should not be given a   reward.
			}
		};
		HeyzapAds.NetworkCallbackListener reportNetwork = delegate(string network, string callback) {
			if (callback == HeyzapAds.NetworkCallback.SHOW) {
				UnitedAnalytics.LogEvent ("Ad Show", "By -" + network + "-");
				//BI._.AdWatched();
				#if !UNITY_EDITOR
				AppsFlyer.trackRichEvent("Ad By " +network, new Dictionary<string,string>(){{"network", network}} );
				#endif
			}
		};
		HeyzapAds.SetNetworkCallbackListener (reportNetwork);
 
		HZIncentivizedAd.SetDisplayListener (listener);

		//Fetch ();
	}

	public static void Fetch ()
	{
		HZIncentivizedAd.Fetch ();
        //CrossSceneUIHandler.Instance.AdsStatusTxt.text = "*FETCH*\r\nIs Available: " + HZIncentivizedAd.IsAvailable().ToString() + "\r\n" + "UserType: " + UserData.Instance.userType;
	}

	public static void Show ()
	{
		if (IsAvailable) {
			isPlayingAd = true;
            AudioManager.Instance.OnApplicationPause(true);
			HZIncentivizedAd.Show ();
		}
	}
//	public static string GetProvider()
//	{
//	HeyzapAds.n
//	return "";
//	}

	public static bool IsAvailable {
		get {
            //CrossSceneUIHandler.Instance.AdsStatusTxt.text = "Is Available: " + HZIncentivizedAd.IsAvailable().ToString() + "\r\n" + "UserType: " + UserData.Instance.userType;
			#if !UNITY_EDITOR
			return HZIncentivizedAd.IsAvailable ();
			#else
			return true;
			#endif
		}
	}

	public static bool isLastAdWatchedCompletely;

	public static void SetListener ()
	{
		HZIncentivizedAd.SetDisplayListener (listener);
	}

	#region Callbacks

	public static HZIncentivizedAd.AdDisplayListener listener;

	public static void CallBackAdditive()
	{
		isPlayingAd = false;
        AudioManager.Instance.OnApplicationPause(false);
	}

	#endregion


	#region RulesSystem

	public static bool AreAdsAllowedForUser (AdType adType)
	{
		if (!IsAvailable)
			return false;
//		if (UserData.Instance.userType == UserData.UserType.PAYING_USER)
//			return false;

		switch (adType) {
		case AdType.FreeGift:
		#if UNITY_ANDROID
			return(UserData.Instance.userType >= UserData.UserType.ENGAGED_A);
			//return(UserData.Instance.userType >= UserData.UserType.NEW_USER);   // for debugging - should be changed
		#elif UNITY_IOS
			return(UserData.Instance.userType >= UserData.UserType.ENGAGED_C);
		#elif UNITY_EDITOR
			return(UserData.Instance.userType >= UserData.UserType.NEW_USER);
		#endif
		//break;
		case AdType.DoubleYourCoins:
		#if UNITY_ANDROID
			return(UserData.Instance.userType >= UserData.UserType.NEW_USER);
		#elif UNITY_IOS
			return(UserData.Instance.userType >= UserData.UserType.ENGAGED_C);
		#endif
		//break;
		case AdType.SaveMe:
		#if UNITY_ANDROID
			return(UserData.Instance.userType >= UserData.UserType.NEW_USER);
		#elif UNITY_IOS
                return(UserData.Instance.userType >= (UserData.UserType)DAO.Settings.IOSUserTypeForSaveMeByVideo);
		#endif
		//break;
		default:
			return true;
		//break;
		}

	}

	#endregion

	public enum AdType
	{
		DoubleYourCoins,
		SaveMe,
		FreeGift
	}
}
