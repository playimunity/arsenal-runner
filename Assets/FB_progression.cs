﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Facebook.Unity;
using Amazon;

public class FB_progression : MonoBehaviour {


	public RawImage userPhoto;
	public Text userNameTXT;
	public Text conflictTXT;
	public Text currentProgressTXT;
	public Text serverProgressTXT;
	public Text currentProgressPlayersTXT;
	public Text serverProgressPlayersTXT;
	public Text currentProgressCoinsTXT;
	public Text serverProgressCoinsTXT;
	public Image serverRecommendIMG;
	public Image curRecommendIMG;
	public Text serverRecommendTXT;
	public Text curRecommendTXT;
	public Text saveTXT;
	public Toggle currentProgressToggle;
	public Toggle serverProgressToggle;

	// Use this for initialization
	void Start () {
		/*
		//Debug.Log ("RON_____________________FB_progression start");
		userNameTXT.text = UserData.Instance.UserName;
		userPhoto.texture = UserData.Instance.UserImageTexture;
		//conflictTXT.text = DAO.Language.GetString("two_conflict");
		//currentProgressTXT.text = DAO.Language.GetString("cur_progress");
		//serverProgressTXT.text = DAO.Language.GetString("ser_progress");
		currentProgressPlayersTXT.text = DAO.NumOfPurchasedPlayers + "/" + DAO.Instance.teamMaxSize.ToString() + " Players";
		serverProgressPlayersTXT.text = DAO.Instance.CalcNumOfPurchasedPlayers(DDB._.UserDataToCompareFromDB.purchasedPlayers).ToString() +  "/" + DAO.Instance.teamMaxSize.ToString() +" Players";
		currentProgressCoinsTXT.text = DAO.TotalCoinsCollected.ToString ();
		serverProgressCoinsTXT.text = DDB._.UserDataToCompareFromDB.totalCoins.ToString();
		*/
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	void OnEnable()
	{
		userNameTXT.text = UserData.Instance.UserName;
		userPhoto.texture = UserData.Instance.UserImageTexture;
		conflictTXT.text = DAO.Language.GetString("two_conflict");
		currentProgressTXT.text = DAO.Language.GetString("cur_progress");
		serverProgressTXT.text = DAO.Language.GetString("ser_progress");
		saveTXT.text = DAO.Language.GetString("save");
		serverRecommendTXT.text = DAO.Language.GetString("recommended");
		curRecommendTXT.text = DAO.Language.GetString("recommended");
		currentProgressPlayersTXT.text = DAO.NumOfPurchasedPlayers + "/" + DAO.Instance.teamMaxSize.ToString() + " Players";
		currentProgressCoinsTXT.text = DAO.TotalCoinsCollected.ToString ();
		serverProgressCoinsTXT.text = "0";
		int serNumOfPlayers = 0;
		 // set default to Current Progress
		currentProgressToggle.isOn = true;
		curRecommendTXT.enabled = true;
		curRecommendIMG.enabled = true;
		serverProgressToggle.isOn = false;
		serverRecommendIMG.enabled = false;
		serverRecommendTXT.enabled = false;

		if (DDB._ != null)
		{
			if ((DDB._.UserDataToCompareFromDB != null) && (!string.IsNullOrEmpty(DDB._.UserDataToCompareFromDB.purchasedPlayers)))
				serNumOfPlayers = DAO.Instance.CalcNumOfPurchasedPlayers (DDB._.UserDataToCompareFromDB.purchasedPlayers);
			if (DDB._.UserDataToCompareFromDB != null)
				serverProgressCoinsTXT.text = DDB._.UserDataToCompareFromDB.totalCoins.ToString();
			if (DDB._.UserDataToCompareFromDB != null && DAO.TotalCoinsCollected < DDB._.UserDataToCompareFromDB.totalCoins) {
				serverProgressToggle.isOn = true;
				serverRecommendIMG.enabled = true;
				serverRecommendTXT.enabled = true;
				curRecommendIMG.enabled = false;
				curRecommendTXT.enabled = false;
				currentProgressToggle.isOn = false;

			}
				
		}
		serverProgressPlayersTXT.text = serNumOfPlayers.ToString() +  "/" + DAO.Instance.teamMaxSize.ToString() +" Players";

		if (DAO.NumOfPurchasedPlayers < serNumOfPlayers)
		{
			currentProgressToggle.isOn = false;
			serverProgressToggle.isOn = true;
			serverRecommendIMG.enabled = true;
			serverRecommendTXT.enabled = true;
			curRecommendIMG.enabled = false;
			curRecommendTXT.enabled = false;

		}
	}
		

	public void save() {
		if (currentProgressToggle.isOn) {
			// play close animation
			DDB._.SaveData ();
			UserDataOldProgressStructure oldProgress = new UserDataOldProgressStructure ();
			DDB._.SaveOldProgressData ();

			MainScreenUiManager.instance.HideFacebookProgression ();
		}
		if (serverProgressToggle.isOn) {
			DDB._.UserDataToCompareFromDB.ReplaceLocalData ();
			MainScreenUiManager.instance.HideFacebookProgression ();
			GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM);
			// play close animation
		}
	}

}
