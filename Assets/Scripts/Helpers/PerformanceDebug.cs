﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PerformanceDebug : MonoBehaviour {


	public Text fpsValue;
	public Text dCallsValue;
	public Text usertypeTxt;
	float fps;
	WaitForSeconds waitSecond = new WaitForSeconds(0.5f);


	void OnEnable() {
		InvokeRepeating ("ShowStats", 0.5f, 0.5f);
		//Debug.Log("alon__________ PerformanceDebug - OnEnabled ()");
	}

	void Update () {
		fps = 1.0f / Time.deltaTime;
	}
	

	void ShowStats () {
		//Debug.Log("alon__________ PerformanceDebug - ShowStats ()");
		fpsValue.text = (Mathf.Round(fps)).ToString();
		//fpsValue.text = UnityStats.vboTotal.ToString ();
		#if UNITY_EDITOR
		dCallsValue.text = UnityStats.drawCalls.ToString ();
		#else
		dCallsValue.text = "not available";
		#endif
		//yield return waitSecond;
		usertypeTxt.text = DAO.UserType.ToString();
	}
}
