﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	public GameObject playerFollowCamera;
	public GameObject specialEffectsCamera;

	public enum Cameras { PLAYER_FOLLOW_CAMERA, SPECIAL_EFFECTS_CAMERA }
	public Cameras activeCamera = Cameras.SPECIAL_EFFECTS_CAMERA;

	public static CameraManager instance;



	void Awake(){
		instance = this;
	}


	public void switchCamera(Cameras cam){
		switch (cam) {
			case Cameras.PLAYER_FOLLOW_CAMERA : {
				playerFollowCamera.SetActive(true);
				specialEffectsCamera.SetActive(false);
				activeCamera = Cameras.PLAYER_FOLLOW_CAMERA;
				break;
			}
			case Cameras.SPECIAL_EFFECTS_CAMERA : {
				specialEffectsCamera.SetActive(true);
				playerFollowCamera.SetActive(false);
				activeCamera = Cameras.SPECIAL_EFFECTS_CAMERA;
				break;
			}
		}
	}

}
