﻿using UnityEngine;
using System.Collections;

public class X2Coins : Collectable {

	public GameObject MeshObject;

	Transform MeshTrans;

	void Start(){
		MeshTrans = MeshObject.transform;
	}

	void Update () {
		//MeshTrans.Rotate( 0f,0f, 180f*Time.deltaTime);
	}


	void OnTriggerEnter(Collider col){
		if (col.transform.gameObject.tag == "Player") {
			ParticleCollectGenerator.instance.PlayParticle(transform.position , transform.eulerAngles);
			RunManager.OnX2CoinsCollected ();
			//AudioManager.Instance.X2PowerUp ();
			gameObject.SetActive(false);//Destroy(gameObject);

		}
	}
}
