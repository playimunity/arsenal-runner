using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Facebook.Unity;
using System.Collections.Generic;
using System;

public class MainScreenUiManager : MonoBehaviour
{

	public static MainScreenUiManager instance;

	public Animator uiAnimator;

	public GameObject PerksScreen;
	//public GameObject RunsScreen;
	public GameObject ScreenBlocker;

	public bool runScrnOn = false;
	public bool cupScrnOn = false;
	public bool tutPopupsDisabled = false;

	Player pl;

	GameManager.GameState RunToStartAfterPlayerAnimationDone;

	public GameObject debugParent;

	//UI details:
	public Text playerName;

	public Text playerNumber;
	//public Image shirtImage;

	public Image perkIcon;
	public Text perkName;
	//public Text perkPreviewPannel_Title;
	public Text perkPreviewPannel_Description;

	public bool isFreeGifts = false;

	WaitForSeconds waitOneSec = new WaitForSeconds (1);


	public Button claimFreeGiftBTN;
	public ClaimScreenHandler claimScreen;


	public Sprite[] skillsIconsBrown;
	public Sprite[] skillsIconsBlue;

	[Serializable]
	public struct PlayerIcon{
		public int playerId;
		public Sprite playerSprite;
	}

	public PlayerIcon[] playersIcons;





	void Awake ()
	{
		instance = this;
	}

	#if UNITY_EDITOR
	void Update(){
		if (Input.GetKeyUp (KeyCode.R)) {
			ShowRateUsPopup ();
		}
	}
	#endif

	void Start ()
	{
		if (DAO.IsInDebugMode) {
			debugParent.SetActive (true);
		}

		claimFreeGiftBTN.GetComponent<TimerManager> ().callback += FreeGiftsTimerEnd;
		//claimFreeGiftBTN.gameObject.SetActive (HeyzapWrapper.AreAdsAllowedForUser (HeyzapWrapper.AdType.FreeGift));

		isFreeGifts = HeyzapWrapper.AreAdsAllowedForUser (HeyzapWrapper.AdType.FreeGift);
		claimFreeGiftBTN.gameObject.SetActive (isFreeGifts);
		FreeGiftBtnAnimToggle (isFreeGifts);
        DDB._.CheckGiftCards ();     
        DDB._.CheckGiftCardsRequests ();
//		energyTimer = CrossSceneUIHandler.Instance.playerEnergyTimer;
		GetStringsFromDao();
	}

    void SwitchTo_TestRunScreen(){
        PrefabManager.instanse.ChoosePlayer (0);
        GameManager.SwitchState (GameManager.GameState.TEST_RUN);
    }
    public void Start_TestRun(){
       // an.Play ("main_menu_close", 2);
        SwitchTo_TestRunScreen();
    }


	// handles the player name ui animations:
	public void PlayerNameAnimToggle(){
		if (playerNameOn) {
			uiAnimator.Play ("player_name_swapper" , 5 , 0f);
		}else{
			PlayerNameUpdate ();
			uiAnimator.Play ("player_name_in" , 5 , 0f);
			playerNameOn = true;
		}
	}

	bool playerNameOn = false;
	public void PlayerNameAnimToggle(bool on){
		if (on && !playerNameOn) {
			PlayerNameUpdate ();
			uiAnimator.Play ("player_name_in" , 5 , 0f);
			playerNameOn = true;
		} else if (!on && playerNameOn) {
			uiAnimator.Play ("player_name_out" , 5 , 0f);
			playerNameOn = false;
		}
	}

	public void PlayerNameUpdate(){
		if (plusBtnOn) {
			playerName.text = "BUY NEW PLAYER";
		} else {
			pl = PlayerChooseManager.instance.currentPlayerScript;
			playerName.text = pl.playerName;
		}
	}





	// handles the player top info ui animations:
	public void PlayerUiAnimToggle(){

		CrossSceneUIHandler.Instance.PlayerInfoUpdate();
		SetViewWithCurrentPlayer ();
		//UpdateProgressBar ();
	}




	// handles the player to buy bottom info ui animations:
	public void PreviewPlayerCardSwapAnim(){
		uiAnimator.Play ("PreviewPlayerSwapAnim" , 1 , 0f);
	}
		
	public void PreviewPlayerInfoUpdate(){ // driven by animation event

		pl = PlayerChooseManager.instance.currentPlayerScript;

		playerNumber.text = pl.playerNumber.ToString ();

//		foreach (Sprite icon in skillsIconsBrown) {
//			if (icon.name == pl.DefaultPerk.ToString ()) {
//				perkIcon.sprite = icon;
//			}
//		}
		perkIcon.sprite = GetSkillIconBySkillType (pl.DefaultPerk);
		//perkName.text = pl.DefaultPerk.ToString ();
		perkName.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + Perk.GetPerkIDByPerkType( pl.DefaultPerk )]["name"]);
		perkPreviewPannel_Description.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + Perk.GetPerkIDByPerkType( pl.DefaultPerk )]["desc_1"]);
		//perkPreviewPannel_Description = pl.data.Perk_1.

//		for (int i = 0; i < perkPreviewPannel_levelSquers.Length; i++) {
//			if (i <= pl.data.Perk_1.level) {
//				perkPreviewPannel_levelSquers [i].color = Color.white;
//			} else {
//				perkPreviewPannel_levelSquers [i].color = deepBlue;
//			}
//		}
	}







	bool previewPlayerCardAnimOn = false;

	public void PreviewPlayerCardAnimToggle (bool on)
	{
		if (on && !previewPlayerCardAnimOn) {
			uiAnimator.Play ("PreviewPlayerCardAnim in");
//			Debug.Log ("alon___________ mainScreenUiManager - PreviewPlayerCardAnimToggle() - on");
			previewPlayerCardAnimOn = true;
			ChooseScrnBtnAnimToggle (true);
		} else if (!on && previewPlayerCardAnimOn) {
			uiAnimator.Play ("PreviewPlayerCardAnim out");
//			Debug.Log ("alon___________ mainScreenUiManager - PreviewPlayerCardAnimToggle() - off");
			previewPlayerCardAnimOn = false;
			ChooseScrnBtnAnimToggle (false);
			StartCoroutine (HidePlayerChooseScreen());
		}
	}



	public void ChooseScrnBtnAnimToggle(bool on){
		if (on) {
			playerChooseScreen.SetActive (true);
			uiAnimator.Play ("choose_scrn_btn_in");
			PlayerChooseManager.instance.onAction = true;
		} else {
			uiAnimator.Play ("choose_scrn_btn_out");
			PlayerChooseManager.instance.onAction = false;
			//StartCoroutine (HidePlayerChooseScreen());
		}
	}






	bool plusBtnOn = false;

	public void PlusBtnToggle(bool on){
		if (on && !plusBtnOn) {
			uiAnimator.Play ("plusBtnAnim in");
			plusBtnOn = true;
		} else if (!on && plusBtnOn) {
			uiAnimator.Play ("plusBtnAnim out");
			plusBtnOn = false;
		}
	}



	bool lowerBarOn = true;

	public void LowerBarToggle(bool on){
		if (on && !lowerBarOn) {
			uiAnimator.Play ("lower_bar_anim_in");
			lowerBarOn = true;
		} else if (!on && lowerBarOn) {
			uiAnimator.Play ("lower_bar_anim_out");
			lowerBarOn = false;
		}
	}




	public GameObject playerChooseScreen;
	bool chooseScreenOn = false;

	public void PlayerChooseScrnAnim (bool on)
	{
		if (on && !chooseScreenOn) {
			playerChooseScreen.SetActive (true);
			chooseScreenOn = true;
			uiAnimator.Play ("player choose scrn in", 3, 0f);
			GM_Input._.PlayersList = true;
		} else if (!on && chooseScreenOn) {
			chooseScreenOn = false;
			uiAnimator.Play ("player choose scrn out", 3, 0f);
			GM_Input._.PlayersList = false;
		}
	}

	IEnumerator HidePlayerChooseScreen(){
		yield return waitOneSec;
		playerChooseScreen.SetActive (false);
	}





	public void CupScrnAnimToggle (bool on)
	{
		//Debug.Log("alon_______ MainScreenUI - CupScrnAnimToggle");
		CupScrnAnimToggle (on , false);
	}


	public void CupScrnAnimToggle (bool on, bool ShowImideately)
	{
		//Debug.Log("alon_______ MainScreenUI - CupScrnAnimToggle");

		if (!DAO.Instance.IsRunsDataReady) {
			GameManager.Instance.PromtAppNoInternet ();
//			Debug.Log("alon_______ MainScreenUI - !DAO.Instance.IsRunsDataReady");
			return;
		}

		if (ShowImideately) {
			uiAnimator.speed = 100f;
		} else {
			uiAnimator.speed = 1f;
		}

		if (on && !cupScrnOn) {
			//Debug.Log("alon_______ MainScreenUI - on");
			cupScrnOn = true;
			CupsManager.Instance.InitializeQuestUIElements ();
			//CrossSceneUIHandler.Instance.hideCoinsCounter ();
			uiAnimator.Play ("QuestsMap_Show");
			PlayerChooseManager.instance.onAction = true;
			GM_Input._.QuestMap = true;
//			foreach (var cup in CupsManager.Instance.cups) {
//				if (cup.status == Cup.CupStatus.COMPLETED) {
//					cup.GetMadalPerQuest ();
//				}
//			}
		} else if (!on && cupScrnOn) {
			//Debug.Log("alon_______ MainScreenUI - !on");
			cupScrnOn = false;
			GM_Input._.QuestMap = false;
			uiAnimator.Play ("QuestsMap_Hide");
          //  CrossSceneUIHandler.Instance.showCoinsCounter ();
			PlayerChooseManager.instance.onAction = false;
		}
	}



	public void reportQuestStartCicked ()
	{
		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE (BI.FLOW_STEP.STARTED_QUEST);
	}





	public void RunsScrnAnimToggle (bool on)
	{
		RunsScrnAnimToggle (on, false);
	}

	public void RunsScrnAnimToggle (bool on , bool ShowImideately)
	{
		if (ShowImideately) {
			uiAnimator.speed = 100f;
		}else{
			uiAnimator.speed = 1f;
		}

		if (on && !runScrnOn) {
			uiAnimator.Play ("runs screen in");
			runScrnOn = true;
			GM_Input._.RunsScreen = true;
//			Debug.Log ("alon________ LockerRoomUIActions - RunsScrnAnimToggle(true)");
		} else if(!on && runScrnOn) {
			uiAnimator.Play ("runs screen out");
			runScrnOn = false;
			GM_Input._.RunsScreen = false;
//			Debug.Log ("alon________ LockerRoomUIActions - RunsScrnAnimToggle(false)");
		}
	}



	bool medikKitsOn = false;

	public void MidekKitsScreenToggle (bool show)
	{
		if (!medikKitsOn && show) {
			uiAnimator.Play ("medik kit show");
			medikKitsOn = true;
			GM_Input._.MedikKits = true;
		} else if (medikKitsOn && !show) {
			DAO.HealPopoup = 1;
			medikKitsOn = false;
			uiAnimator.Play ("medik kit hide");
			GM_Input._.MedikKits = false;
		}
	}





	public bool winScrnOn = false;

	public void WinScrnToggle (bool on)
	{
		//winScrnOn = !winScrnOn;

		if (on && !winScrnOn) {
			winScrnOn = true;
			GM_Input._.CupWon = true;
			uiAnimator.Play ("winning screen in");
		} else if (!on && winScrnOn) {
			winScrnOn = false;
			GM_Input._.CupWon = false;
			uiAnimator.Play ("winning screen out");
			GameManager.ActiveCup = null;
			GameManager.Instance.activeCupInstance = null;
			Debug.Log ("alon_________ end of closing winning screen method");
		}
	}




	bool perksScrnOn = false;

	public void PerksScrnAnim (bool on)
	{
		if (on && !perksScrnOn) {
			PerksScreen.SetActive (true);
			PerksScrn_Actions.Instance.OnBeforeShow ();
			uiAnimator.Play ("perks_screen_in");
			PlayerChooseManager.instance.onAction = true;
			perksScrnOn = true;
			GM_Input._.Perks = true;
		} else if (!on && perksScrnOn) {
			GM_Input._.Perks = false;
			uiAnimator.Play ("perks_screen_out");
			UnitedAnalytics.LogEvent ("Perk Screen closed", "closed", UserData.Instance.userType.ToString (), DAO.PlayTimeInMinutes);
			perksScrnOn = false;
			PlayerChooseManager.instance.PerkGlowFX ();
			StartCoroutine (DisablePerksScrn());
		}
	}

	IEnumerator DisablePerksScrn(){
		yield return waitOneSec;
		PerksScreen.SetActive (false);
	}




	public void OnGiftsClicked(){
		//an.Play ("mainmenu_gift-btn", 3);

		CardsScrn_Actions.instance.CreateCards ();
		CardScrnToggle (true);
	}


	bool cardScrnOn = false;

	public void CardScrnToggle(bool on){
		if (on && !cardScrnOn) {
			cardScrnOn = true;
			uiAnimator.Play ("cards_screen_in");
//			Debug.Log ("alon_______ open gift cards !");
			FreeGiftBtnAnimToggle (false);
			PlayerChooseManager.instance.onAction = true;
			GM_Input._.GiftCards = true;
		} else if (!on && cardScrnOn) {
			cardScrnOn = false;
			GM_Input._.GiftCards = false;
			uiAnimator.Play ("cards_screen_out");
//			Debug.Log ("alon_______ close gift cards !");
			FreeGiftBtnAnimToggle (isFreeGifts);
			PlayerChooseManager.instance.onAction = false;

			if (DAO.TotalGiftCardsSent > 10) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_scratch_my_back);
			}
			if (DAO.TotalGiftCardsSent > 50) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_donator);
			}
			if (DAO.TotalGiftCardsSent > 100) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_bff);
			}

			if (DAO.TotalGiftCardsRecieved > 10) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_friendly_player);
			}
			if (DAO.TotalGiftCardsRecieved > 50) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_rising_star);
			}
			if (DAO.TotalGiftCardsRecieved > 100) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_people_just_loves_me);
			}
		}
	}



	public void OnSettingClicked(){
		AudioManager.Instance.OnSettingsBTN ();
		SettingsScrnToggle (true);
	}


	bool settingsScrnOn = false;

	public void SettingsScrnToggle(bool on){
		if (on && !settingsScrnOn) {
			uiAnimator.Play ("settings_scrn_in");
			settingsScrnOn = true;
			GM_Input._.Settings = true;
		} else if (!on && settingsScrnOn) {
			uiAnimator.Play ("settings_scrn_out");
			settingsScrnOn = false;
			GM_Input._.Settings = false;
			DAO.Instance.LOCAL.SaveAll ();
		}
	}



	public GameObject practiceRunBtn;

	public void StartPracticeRunBtnToggle(bool on){
		if (on) {
			practiceRunBtn.SetActive (true);
		} else {
			practiceRunBtn.SetActive (false);
		}
	}




	public enum ArrowsConditions
	{
		LEFT_ARROW,
		RIGHT_ARROW,
		BOTH_ARROWS,
		NO_ARROWS
	};

	bool leftArrowOn = false;
	bool rightArrowOn = false;

	public ArrowsConditions chosenArrowsConditions = ArrowsConditions.NO_ARROWS;

	public void ArrowsToggle (int side)
	{
		switch (side) {
		case -1:
			if (chosenArrowsConditions == ArrowsConditions.LEFT_ARROW)
				return;
			if (!leftArrowOn) {
				uiAnimator.Play ("arrow_left_in");
				leftArrowOn = true;
			}
			if (rightArrowOn) {
				uiAnimator.Play ("arrow_right_out");
				rightArrowOn = false;
			}
			chosenArrowsConditions = ArrowsConditions.LEFT_ARROW;
			break;
		case 1:
			if (chosenArrowsConditions == ArrowsConditions.RIGHT_ARROW)
				return;
			if (leftArrowOn) {
				uiAnimator.Play ("arrow_left_out");
				leftArrowOn = false;
			}
			if (!rightArrowOn) {
				uiAnimator.Play ("arrow_right_in");
				rightArrowOn = true;
			}
			chosenArrowsConditions = ArrowsConditions.RIGHT_ARROW;
			break;
		case 2:
			if (chosenArrowsConditions == ArrowsConditions.BOTH_ARROWS)
				return;
			if (!leftArrowOn) {
				uiAnimator.Play ("arrow_left_in");
				leftArrowOn = true;
			}
			if (!rightArrowOn) {
				uiAnimator.Play ("arrow_right_in");
				rightArrowOn = true;
			}
			chosenArrowsConditions = ArrowsConditions.BOTH_ARROWS;
			break;
		case 0:
			if (chosenArrowsConditions == ArrowsConditions.NO_ARROWS)
				return;
			if (leftArrowOn) {
				uiAnimator.Play ("arrow_left_out");
				leftArrowOn = false;
			}
			if (rightArrowOn) {
				uiAnimator.Play ("arrow_right_out");
				rightArrowOn = false;
			}
			chosenArrowsConditions = ArrowsConditions.NO_ARROWS;
			break;
		}
	}




	bool swipingHandOn = false;
	public void SwipingHand (bool on)
	{
		if (on && !swipingHandOn) {
			StartCoroutine (SwipingHandCoroutine ());
			swipingHandOn = true;
		} else if (!on && swipingHandOn) {
			//uiAnimator.Play ("swiping_hand_out");
			swipingHandOn = false;
		}


	}



	IEnumerator SwipingHandCoroutine ()
	{
		yield return new WaitForSeconds (2);
		//uiAnimator.Play ("swiping_hand");
	}





	bool tutPointerOn = false;

	public void TutorialPointerToggle (bool on)
	{
		if (on && !tutPointerOn) {
			uiAnimator.Play ("tutorial_pointer_in");
			tutPointerOn = true;
		} else if (!on && tutPointerOn) {
			uiAnimator.Play ("tutorial_pointer_out");
			tutPointerOn = false;
		}
	}





	bool lowEnergyPopupOn = false;

	public void LowEnergyPopup (bool on)
	{
		if (on && !lowEnergyPopupOn) {
			GM_Input._.LowEnergy = true;
			uiAnimator.Play ("LowEnergy_Show");
			PlayerChooseManager.instance.onAction = true;
			lowEnergyPopupOn = true;
		} else if (!on && lowEnergyPopupOn) {
			GM_Input._.LowEnergy = false;
			uiAnimator.Play ("LowEnergy_Hide");
			PlayerChooseManager.instance.onAction = false;
			lowEnergyPopupOn = false;
		}
	}




	public bool healPopupOn = false;

	public void HealPopup (bool on)
	{
		if (on && !healPopupOn) {
			uiAnimator.Play ("tut_heal_popup_in");
			healPopupOn = true;
			GM_Input._.Heal = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && healPopupOn) {
			uiAnimator.Play ("tut_heal_popup_out");
			healPopupOn = false;
			GM_Input._.Heal = false;
			PlayerChooseManager.instance.onAction = false;
			DAO.HealPopoup = 1;
		}
	}

	public void HealPopupAnalyticsEvent ()
	{
		UnitedAnalytics.LogEvent ("health tutroail", "Closed", UserData.Instance.userType.ToString ());
	}






	public bool recruitSecondPopup = false;

	public void RecruitSecondPopup (bool on)
	{
		if (on && !recruitSecondPopup) {
			uiAnimator.Play ("tut_recruit_popup_in");
			recruitSecondPopup = true;
			GM_Input._.Recruit = true;
			//Bench.instance.swiping = true;
		} else if (!on && recruitSecondPopup) {
			GM_Input._.Recruit = false;
			uiAnimator.Play ("tut_recruit_popup_out");
			recruitSecondPopup = false;
			//Bench.instance.swiping = false;
			if (Tutorial.IsNeedToShowBuySecondPlayer1Popup) {
				LocalDataInterface.Instance.recruitSecondPlayerPopup2Date = DateTime.Now.ToString ();
				DAO.BuySecondPlayerPopup1 = 1;
			} else if (Tutorial.IsNeedToShowBuySecondPlayer2Popup) {
				LocalDataInterface.Instance.recruitSecondPlayerPopup2Date = DateTime.Now.ToString ();
			}
		}
	}

	public void RecruitSecondAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("Player Available pop up", "used", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("Player Available pop up", "closed", UserData.Instance.userType.ToString ());
		}
	}

	public Text coinsToThirdPlayerTxt;
	public bool recruitThirdPopupOn = false;

	public void RecruitThirdPopup (bool on, int coinsToThirdPlayer)
	{
		if (on && !recruitThirdPopupOn) {
			coinsToThirdPlayerTxt.text = coinsToThirdPlayer.ToString ();
			uiAnimator.Play ("tut_recruit2_popup_in");
			recruitThirdPopupOn = true;
			GM_Input._.RecruitAnother = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && recruitThirdPopupOn) {
			GM_Input._.RecruitAnother = false;
			uiAnimator.Play ("tut_recruit2_popup_out");
			recruitThirdPopupOn = false;
			PlayerChooseManager.instance.onAction = false;
		}
	}

	public void RecruitThirdPopupClose ()
	{
		uiAnimator.Play ("tut_recruit2_popup_out");
		recruitThirdPopupOn = false;
		DAO.BuyThirdPlayerPopup = 1;
		PlayerChooseManager.instance.onAction = false;
	}

	public void OnRequruitThirdPopupGetClicked ()
	{
		RecruitThirdPopupClose ();
		CrossSceneUIHandler.Instance.showStore (CrossSceneUIHandler.ShopStates.COINS);

		IAP.AdditionalIAPCallback += OnPackPurchasedAfterRecruitThirdPopup;
	}

	void OnPackPurchasedAfterRecruitThirdPopup (string sku)
	{
		IAP.AdditionalIAPCallback -= OnPackPurchasedAfterRecruitThirdPopup;

		UnitedAnalytics.LogEvent ("almost enough for next player pop up", "IAP MAde", UserData.Instance.userType.ToString (), DAO.TotalCoinsCollected);
	}

	public void RecruitThirdAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("almost enough for next player pop up", "clicked", UserData.Instance.userType.ToString (), DAO.TotalCoinsCollected);
		} else {
			UnitedAnalytics.LogEvent ("almost enough for next player pop up", "Closed", UserData.Instance.userType.ToString (), DAO.TotalCoinsCollected);
		}
	}


	public bool practicePopup = false;

	public void PracticePopup (bool on)
	{
		if (on && !practicePopup) {
			uiAnimator.Play ("tut_practice_popup_in");
			practicePopup = true;
			GM_Input._.PracticePopup = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && practicePopup) {
			GM_Input._.PracticePopup = false;
			uiAnimator.Play ("tut_practice_popup_out");
			practicePopup = false;
			DAO.PlayPracticePopup = 1;
			PlayerChooseManager.instance.onAction = false;
		}
	}

	public void PracticePopupAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("play practice pop up", "used", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("play practice pop up", "Closed", UserData.Instance.userType.ToString ());
		}
	}


	bool cupPopup = false;

	public void CupPopup (bool on)
	{
		if (on && !cupPopup) {
			uiAnimator.Play ("tut_cup_popup_in");
			cupPopup = true;
			GM_Input._.CupsPopup = true;
			PlayerChooseManager.instance.onAction = true;
		} else if (!on && cupPopup) {
			GM_Input._.CupsPopup = false;
			uiAnimator.Play ("tut_cup_popup_out");
			cupPopup = false;
			PlayerChooseManager.instance.onAction = false;
			LocalDataInterface.Instance.playCupPopupDate = DateTime.Now.ToString ();
		}
	}

	public void CupPopupAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("play quests pop up", "used", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("play quests pop up", "Closed", UserData.Instance.userType.ToString ());
		}
	}




	public Text StarterOffer_OldPrice;
	public Text StarterOffer_NewPrice;
	bool offerPopup = false;

	public void OfferPopup (bool on)
	{
		if (on && !offerPopup) {
			StarterOffer_OldPrice.text = IAP.Instance.Product_LOCAL_CLUB.GetPriceLabel ();
			StarterOffer_NewPrice.text = IAP.Instance.Product_STARTER_DEAL.GetPriceLabel ();

			uiAnimator.Play ("tut_offer_popup_in");
			offerPopup = true;
			GM_Input._.WellcomeOffer = true;
		} else if (!on && offerPopup) {
			GM_Input._.WellcomeOffer = false;
			uiAnimator.Play ("tut_offer_popup_out");
			offerPopup = false;
			LocalDataInterface.Instance.offerPopupDate = DateTime.Now.ToString ();
		}
	}

	public void OfferPopupAnalyticsEvent (bool positive)
	{
		if (positive) {
			UnitedAnalytics.LogEvent ("Buy starter pop up", "clicked", UserData.Instance.userType.ToString ());
		} else {
			UnitedAnalytics.LogEvent ("Buy starter pop up", "Closed", UserData.Instance.userType.ToString ());
		}
	}



	public GameObject FriendsListScreen;

	public void ShowQuestFriendsList (List<FBFriend> friends)
	{
		FriendsListScreen.SetActive (true);
		FriendsListScreen.GetComponent<QuestFriendsList> ().SetFriends (friends);
		uiAnimator.Play ("FriendsListShow", 13);
	}

	public void HideQuestFriendsList ()
	{
		uiAnimator.Play ("FriendsListHide", 13);
	}




	public void ShowRateUsPopup ()
	{
		uiAnimator.Play ("rate_us_popup_in");
		if (DAO.RateUsTimeFactor == 10) {
			DAO.RateUsTimeFactor = 720;
		}
		UnitedAnalytics.LogEvent ("Rate Us Opened", "Popup");
		if (GameManager.Instance.isPlayscapeLoaded)
			BI._.ReporRateUs (BI.RateUsActions.SHOWN);
		GM_Input._.RateUsPopup = true;
		FreeGiftBtnAnimToggle (false); 

	}



	public void OnRateUsClose (bool rated)
	{
		if (!rated) {
			DAO.RateUsPopupDate = DateTime.Now;
			uiAnimator.Play ("rate_us_popup_out");
			if(GameManager.Instance.isPlayscapeLoaded)	BI._.ReporRateUs (BI.RateUsActions.NO);
			GM_Input._.RateUsPopup = false;
		} else {
			uiAnimator.Play ("rate_us_popup_out");
		}
		FreeGiftBtnAnimToggle (MainScreenUiManager.instance.isFreeGifts);
	}

	public void RateUsThanksToggle (bool on)
	{
		if (on) {
			GM_Input._.RateUsThanksPopup = true;
			uiAnimator.Play ("rate_us_thanks_popup_in");
		} else {
			GM_Input._.RateUsPopup = false;
			GM_Input._.RateUsThanksPopup = false;
			uiAnimator.Play ("rate_us_thanks_popup_out");
		}
	}



	public void ShowStore(){
		CrossSceneUIHandler.Instance.ShowStoreClicked ();
	}


    public GameObject imageBlock;
	public void LogginToFacebookFromSettings(){
		FBModule.Instance.FBButonClicked = true;
		FBModule.OnFacebookProfileRecieved += OnFBLoggin_Settings;
        #if UNITY_EDITOR
        imageBlock.SetActive(true);
        #endif
		FBModule.Instance.Login();
	}

	void OnFBLoggin_Settings(){
		FBModule.OnFacebookProfileRecieved -= OnFBLoggin_Settings;
		SettingsSrn_Actions.Instance.ResetFBState ();
        #if UNITY_EDITOR
        imageBlock.SetActive(false);
        #endif
	}

	public  void OnInviteFriendClicked(){
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.INVITE_FRIENDS);
	}

    public void ShowFacebookProgression(){
        //ALON TODO: remove comment signs from the lines below
		uiAnimator.Play ("facebook_progression_in");
    }

    public void HideFacebookProgression(){
        //ALON TODO: remove comment signs from the lines below
		uiAnimator.Play ("facebook_progression_out");
        //CrossSceneUIHandler.Instance.showCoinsCounter ();
    }

	public void OnMyStatsClicked(){
		//an.Play ("mainmenu_stats-btn", 3);

		PlayerStatsStructure lpss = new PlayerStatsStructure ();
		lpss.SetToLocalPlayer ();

		StatsController.Instance.UpdateStats (lpss);
		StatsController.Instance.StatsScrnAnimTogge (true);

	}



	public void OnRateUsClicked(){
		#if UNITY_ANDROID || UNITY_EDITOR
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.gamehour.arsenalfc");
		//Application.OpenURL(DAO.Instance.stroeAppLink);
		#elif UNITY_IOS
        Application.OpenURL("https://itunes.apple.com/us/app/arsenal-fc-endless-football/id1158656031?ls=1&mt=8");
		//Application.OpenURL(DAO.Instance.stroeAppLink);
		#endif
	}



	public void OnLeaderboardClicked(){
		//Debug.Log ("##### zlksdjghdlsjfgh ######");
		NativeSocial._.ShowLeaderboardUI ();
	}



	public void OnAchievementsClicked(){
		NativeSocial._.ShowAchievmentsUI ();
	}



	public void OnRestorePurchasesClicked(){
		IAP.Instance.OnRestorePurchasesClicked();
	}




	string pass = "";
	public void OnHBTNPress(string num){
		pass += num;

		StopCoroutine("resetPass");

		if (pass == "3434"){
			CrossSceneUIHandler.Instance.ConsoleShow (false);
		}

		if (pass == "4443"){
			CrossSceneUIHandler.Instance.ConsoleShow ();
		}

		StartCoroutine("resetPass");
	}

	IEnumerator resetPass(){
		yield return new WaitForSeconds (1.5f);
		pass = "";
	}



	public Sprite GetSkillIconBySkillType(Perk.PerkType type , bool brown = true){
		
//		foreach(Sprite skillSprite in skillsIconsBrown){
//			Debug.Log ("alon________ skillSprite name: " + skillSprite.name);
//			if (skillSprite.name == type.ToString ()) {
//				Debug.Log ("alon________ skillSprite name: " + skillSprite.name + " == type name: " + type.ToString ());
//				return skillSprite;
//			}
//		}

		for (int i = 0; i < skillsIconsBrown.Length ; i++) {
			if (skillsIconsBrown [i].name == type.ToString ()) {
				if (brown) {
					return skillsIconsBrown [i];
				} else {
					return skillsIconsBlue [i];
				}
			}
		}

		//Debug.Log ("alon________ returning null at the end");
		return null;
	}



	public int energyToAdd;
	public TimerManager energyTimer;

	public void SetViewWithCurrentPlayer(){

		energyTimer = CrossSceneUIHandler.Instance.playerEnergyTimer;
		pl = PlayerChooseManager.instance.currentPlayerScript;
		energyToAdd = energyTimer.OnChangePlayerToDisplay (pl.PlayerID);
		//if (LockerRoom_Actions.instance.energyTimer.callback != null)
		energyTimer.callback = PlayerChooseManager.instance.EnergyCallback;
		pl.UpdateFitness ();

		UpdateProgressBar ();

		pl.OnFitnessUpdate += UpdateProgressBar;

		PlayerChooseManager.instance.CheckFitnessOfSelectedPlayer ();
	}



	public void UpdateProgressBar(){

	//HeaderManager.instance.UpdateEnergy (pl.data.fitness, pl.data.maxFitness);

		if(pl.data.curHealthState == PlayerData.HealthState.TIERED || pl.data.curHealthState == PlayerData.HealthState.WOUNDED){
			energyTimer.gameObject.SetActive (true);
		}else if(pl.data.curHealthState == PlayerData.HealthState.IDEAL){
			energyTimer.gameObject.SetActive (true);

		}else{
			energyTimer.FullEnergy (pl.PlayerID);
			energyTimer.gameObject.SetActive (false);
		}
			
		CrossSceneUIHandler.Instance.PlayerInfoUpdate();

	}



	bool freeGiftBtnOn = false;
	public void FreeGiftBtnAnimToggle(bool on){
		if (on && !freeGiftBtnOn) {
			uiAnimator.Play ("free_gift_btn_anim_in");
			freeGiftBtnOn = true;
			//Debug.Log ("alon_________ FreeGiftBtnAnimToggle SHOW");
		} else if (!on && freeGiftBtnOn) {
			uiAnimator.Play ("free_gift_btn_anim_out");
			freeGiftBtnOn = false;
			//Debug.Log ("alon_________ FreeGiftBtnAnimToggle HIDE");
		}
	}



	public void ClaimFreeGiftClicked ()
	{
		claimScreen.SetView (GiftCard.GetRandomPrize ());
		claimScreen.isGiftCard = false;
		#if UNITY_EDITOR
		uiAnimator.Play("ClaimScreen_Show");
//		Debug.Log("alon__________ ClaimFreeGiftClicked () - ClaimScreen_Show");
		if (PlayerPrefs.GetInt("is_free_gifts_claimed") == 1)
			claimFreeGiftBTN.GetComponent<TimerManager>().Restart();
		else
		{
			claimFreeGiftBTN.GetComponent<TimerManager>().StartTimer();
			PlayerPrefs.SetInt("is_free_gifts_claimed", 1) ;
		}
		HeyzapWrapper.CallBackAdditive();
		#endif
		HeyzapWrapper.listener = delegate(string adState, string adTag) {
			if (adState.Equals ("incentivized_result_complete")) {
				uiAnimator.Play ("ClaimScreen_Show");
				UnitedAnalytics.LogEvent ("Free Gifts Clicked", UserData.Instance.userType.ToString ());
				HeyzapWrapper.CallBackAdditive();
				if (PlayerPrefs.GetInt("is_free_gifts_claimed") == 1)
					claimFreeGiftBTN.GetComponent<TimerManager>().Restart();
				else
				{
					claimFreeGiftBTN.GetComponent<TimerManager>().StartTimer();
					PlayerPrefs.SetInt("is_free_gifts_claimed", 1) ;
				}
				// The user has watched the entire video and should be given a reward.
			}
			if (adState.Equals ("incentivized_result_incomplete")) {
				// The user did not watch the entire video and should not be given a   reward.
			}
		};
		HeyzapWrapper.SetListener ();
		HeyzapWrapper.Show ();
	}




	public void FreeGiftsTimerEnd()
	{
		claimFreeGiftBTN.GetComponent<TimerManager>().timeDisplay.text = DAO.Language.GetString("claim");
	}




	// string provider:
	public Text healPopup;
	public Text lowEnergyPopup;
	public Text recruitAnotherPopup;
	public Text recruitThirdPopup;
	public Text endlessPopup;
	public Text questPopup;
	public Text claimScrnCardsTitle;
	public Text claimScrnFreeGiftTitle;
	public Text giftCardsTitle;


	void GetStringsFromDao(){
		endlessPopup.text = DAO.Language.GetString("practice-to-raise");
		questPopup.text = DAO.Language.GetString("play-cups-to-get");
		healPopup.text = DAO.Language.GetString("player-is-tired");
		lowEnergyPopup.text = DAO.Language.GetString("low-energy-body");
		recruitAnotherPopup.text = DAO.Language.GetString("recruit-new-player");
		recruitThirdPopup.text = DAO.Language.GetString("another-player-is-just");

		claimScrnCardsTitle.text = DAO.Language.GetString("gc-title");
		giftCardsTitle.text = DAO.Language.GetString("gc-title");
		claimScrnFreeGiftTitle.text = DAO.Language.GetString("free-gifts");
	}




//	void OnFBLoggin_Popup(){
//		FBModule.OnFacebookProfileRecieved -= OnFBLoggin_Popup;
//		FacebookPopupToggle (false);
//	}
//		
//	public void LogginToFacebookFromPopup(){
//		FBModule.Instance.FBButonClicked = true;
//		FBModule.OnFacebookProfileRecieved += OnFBLoggin_Popup;
//		FBModule.Instance.Login();
//	}



	//	public void LogginToFacebookFromTutorial(){
	//		FBModule.Instance.FBButonClicked = true;
	//		FBModule.OnFacebookProfileRecieved += OnFBLoggin_Tutorial;
	//		FBModule.Instance.Login();
	//	}

	//	void OnFBLoggin_Tutorial(){
	//		FBModule.OnFacebookProfileRecieved -= OnFBLoggin_Tutorial;
	//		MainMenuTutorialAnimOut ();
	//		GameManager.SwitchState (GameManager.GameState.LOCKER_ROOM, GameManager.GameSubState.TUTORIAL_CONNECTED);
	//	}
		

}

