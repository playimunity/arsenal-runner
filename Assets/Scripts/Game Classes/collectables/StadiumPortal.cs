﻿using UnityEngine;
using System.Collections;

public class StadiumPortal : MonoBehaviour {

	//public GameObject MeshObject;
	bool triggered = false;



	void OnTriggerEnter(Collider col){
		if (!triggered && col.transform.gameObject.tag == "Player") {
			triggered = true;
			//ParticleCollectGenerator.instance.PlayParticle(transform.position , transform.eulerAngles);
			SecterAreaHandler.instance.OnPortalEntered();
			//SecterAreaHandler.instance.BiuldStadiumSections();
			//AudioManager.Instance.OnPowerUpBTN ();
			//Destroy(gameObject);
			StartCoroutine(DestroyPortal());
		}
	}


	IEnumerator DestroyPortal(){
		yield return new WaitForSeconds (2);
		gameObject.SetActive(false);//Destroy (gameObject);
	}
}
