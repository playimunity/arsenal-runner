using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using Facebook.Unity;

[System.Serializable]
public class Cup{

	public enum CupStatus { NOT_STARTED, IN_PROGRESS, NOT_AVAILABLE, COMPLETED }

	public int id;
	public string name;
	public string icon;

	public int requeredPlayers;
	public int cost;
	public int prize;
	public float playerStartSpeed;
	public int[] attachedRuns;
	public List<RunAbstract> selectedRuns;
	public List<RunAbstract> completedSelectedRuns;
	public int[] regionOpenedAfterStart;
	public int numOfRuns;
	public CupStatus status = Cup.CupStatus.NOT_STARTED;
	public System.TimeSpan timeRemain;
	public int CompleteRank = 0;
	public bool exist = false;
	//public bool completed;
   
	string[] strArr;
	//string thisRunData = "";
	WaitForFixedUpdate waitFixedUpdate = new WaitForFixedUpdate ();

	public bool isGoldCup = false;
	public bool isSilverCup = false;

	public Cup(JSONNode cup, int _id){
		exist = true;
		id = _id;

		icon = cup["img"].Value;
		name = cup["name"].Value;
		requeredPlayers = cup["min_players"].AsInt;
		cost = cup["cost"].AsInt;
		prize = cup["prize"].AsInt;
		numOfRuns = cup ["runs"].Count;//cup["num_of_runs"].AsInt;
		playerStartSpeed = cup["player_speed"].AsFloat;
		attachedRuns = new int[cup ["runs"].Count];
		for (int i=0; i < cup ["runs"].Count; i++) {
			attachedRuns[i] = cup ["runs"][i].AsInt;
		}

		regionOpenedAfterStart = new int[cup ["regions_to_open"].Count];
		for (int i=0; i < cup ["regions_to_open"].Count; i++) {
			regionOpenedAfterStart[i] = cup ["regions_to_open"][i].AsInt;
		}



		// ----
		CheckAndSetCupStatus ();
		//Debug.Log ("alon_____________ Cup Constractor - CheckAndSetCupStatus ()");
	}

	//=========================================================================


	public void Start(){
		status = CupStatus.IN_PROGRESS;
		GameManager.ActiveCupCompleted = false;
		//Debug.Log ("alon___________  Start() - status: " + status.ToString());

		GameManager.CurrentRun = null;
		DAO.ActiveRunData = "";
		SetRuns();
		GameManager.ActiveCup = this;
		GameManager.Instance.activeCupInstance = GameManager.ActiveCup;

		exist = true;


		//expireTime = System.DateTime.Now.AddMinutes (3);

		GameManager.AddRegonToOpened ( regionOpenedAfterStart );
		DAO.ActiveRunID = id;
		DAO.ActiveRunData = EncodeRuns ();
		DAO.Instance.LOCAL.SaveAll ();



		if(FB.IsLoggedIn) DDB._.ReportQuestProgress ();

		UnitedAnalytics.LogEvent ( "quest start", UserData.Instance.userType.ToString(), this.name );
		//BI._.Inventory_Increase ("Locker Room", np.playerName, "");
//		Debug.Log ("alon_______ Cup - Start() - ActiveCup: " + GameManager.ActiveCup.name);
	}

	public void CheckAndSetCupStatus(){
		//		Debug.Log ("alon____________ CheckAndSetCupStatus() -  status: " + status);
		//		if (status == CupStatus.COMPLETED) {
		//			return;
		//		}
		//Debug.Log ("alon____________ CheckAndSetCupStatus() -  GameManager.ActiveCupCompleted: " + GameManager.ActiveCupCompleted);	
		if (GameManager.ActiveCompletedCup != null && GameManager.ActiveCompletedCup.id == id && GameManager.ActiveCupCompleted) {
			//Debug.Log ("alon____________ Cups - CheckAndSetCupStatus() - ActiveCupCompleted = true - ActiveCompletedCup id " + GameManager.ActiveCompletedCup.id + " Completed");	
			status = CupStatus.COMPLETED;
			return;
		}

		if (id < GameManager.Instance.completedRunsData.Length && GameManager.Instance.completedRunsData [id] != "0") {
			//Debug.Log ("alon____________ Cups - CheckAndSetCupStatus() - ActiveCupCompleted = false - completedRunsData [id] != 0 - completed cup data: " + GameManager.Instance.completedRunsData [id] + " Completed");	
			status = CupStatus.COMPLETED;

//			string[] runsEncoded = GameManager.Instance.completedRunsData [id].Split (new char[] { '%' }, System.StringSplitOptions.RemoveEmptyEntries);
//			for (int i = 0; i < runsEncoded.Length; i++) {
//				completedSelectedRuns.Add (new RunAbstract (runsEncoded [i], true));
//			}
//			int golds = 0;
//			int silvers = 0;
//			foreach (RunAbstract run in completedSelectedRuns) {
//				if (run.GoalAchieved == 1) {
//					silvers++;
//				} else if (run.GoalAchieved == 2) {
//					golds++;
//				}
//			}
//			if (golds > silvers) {
//				isGoldCup = true;
//			} else {
//				isGoldCup = false;
//			}

			return;
		}

		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {

			if (PlayerChooseManager.instance.GetNumOfPurchasedPlayers () < requeredPlayers) {
				//Debug.Log ("alon____________ Cups - CheckAndSetCupStatus() - not enough players for the Cup  - completedRunsData [id] != 0 - completed cup data: " + GameManager.Instance.completedRunsData [id] + " NOT_AVAILABLE");
				status = CupStatus.NOT_AVAILABLE;
			} else {
				status = CupStatus.NOT_STARTED;
				CheckIfItWasWon ();
				CheckIfItInProgress ();
			}
		}
		else {
			CheckIfItInProgress ();
			//Debug.Log ("alon_____________  Cups - CheckAndSetCupStatus() - MENU!!!");
		}

	}

	public void SaveProgress(){
		if (GameManager.ActiveCupCompleted) {
			GameManager.Instance.completedRunData = EncodeCompletedRuns ();
			GameManager.Instance.UpdateCupRunsData (id, GameManager.Instance.completedRunData);
			//Debug.Log ("alon_____________ Cup -  SaveProgress - cup completed - this Run Data: " + GameManager.Instance.completedRunData);
		} else {
			DAO.ActiveRunData = EncodeRuns ();
			//thisRunData = EncodeRuns ();
			//Debug.Log ("alon_____________ Cup -  SaveProgress - cup in progress - this Run Data: " + DAO.ActiveRunData);
		}
	}


	public void End(){
		//Debug.Log ("alon___________ Cup - End()");
		status = CupStatus.NOT_STARTED;
		exist = false;
		GameManager.ActiveCup.status = CupStatus.NOT_STARTED;
		GameManager.ActiveCup.exist = false;
		CheckIfItWasWon ();
	}

	public void CheckIfItWasWon(){
		//Debug.Log ("alon___________ CheckIfItWasWon() - start");
		for (int i=0; i<GameManager.Instance.winnedCups.Length; i++) { //loop all wined cups
			strArr = GameManager.Instance.winnedCups[i].Split(DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries); //GetType the details of current winned cup
			if(int.Parse(strArr[0]) == id){ // if the winned cup id = this cup id it is completed
				//Debug.Log ("alon___________ Cup - CheckIfItWasWon() - true - this cup is Completed");
				status = CupStatus.COMPLETED;
				CompleteRank = int.Parse(strArr[1]);
				return;
			}
		}
	}

	public void CheckIfItInProgress(){
		if (id != DAO.ActiveRunID) return;
		status = Cup.CupStatus.IN_PROGRESS;

		//Debug.Log ("alon___________ Cup - CheckIfItInProgress() - status: IN_PROGRESS");

		selectedRuns = new List<RunAbstract> ();
		string[] runsEncoded = DAO.ActiveRunData.Split(new char[] {'%'}, System.StringSplitOptions.RemoveEmptyEntries);

		for (int i=0; i<runsEncoded.Length; i++) {
			selectedRuns.Add( new RunAbstract( runsEncoded[i] ) );
		//	Debug.Log ("alon_______ adding runs to selected runs");
		}

		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {
			CupsManager.Instance.SetActiveCup (this);
		} else {
			GameManager.ActiveCup = this;
			GameManager.Instance.activeCupInstance = GameManager.ActiveCup;
		}

		//		CupsManager.Instance.SetCupScreenRuns ();
		//		LockerRoom_Actions.instance.RunsScrnAnimToggle(true);
	}

	public void SetCompletedCup(){

		//DAO.ActiveRunID = -1;

		completedSelectedRuns = new List<RunAbstract> ();

		if( id >= GameManager.Instance.completedRunsData.Length){
			//Debug.Log ("alon___________ Cup - SetCompletedCup() - trying to set completed cup but its not in the completedRunsData.Length = " + GameManager.Instance.completedRunsData.Length + " - cup is completed");
			GameManager.Instance.CreateCompletedRunsData ();
			foreach (int i in attachedRuns) {
				completedSelectedRuns.Add ( new RunAbstract( i , true) );
			}
			status = Cup.CupStatus.COMPLETED;
			return;
		}

		if (GameManager.Instance.completedRunsData [id] != "0") {
			string[] runsEncoded = GameManager.Instance.completedRunsData [id].Split (new char[] { '%' }, System.StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < runsEncoded.Length; i++) {
				completedSelectedRuns.Add (new RunAbstract (runsEncoded [i], true));
			}
			//Debug.Log ("alon___________ Cup - SetCompletedCup() - there is data for this completed cup - data: " + GameManager.Instance.completedRunsData [id] + " - cup is completed");

			status = Cup.CupStatus.COMPLETED;
			GameManager.ActiveCompletedCup = this;
			GameManager.Instance.activeCompletedCupInstance = GameManager.ActiveCompletedCup;
			GameManager.ActiveCupCompleted = true;
		} else if (GameManager.Instance.completedRunsData [id] == "0") {
			foreach (int i in attachedRuns) {
				completedSelectedRuns.Add (new RunAbstract (i, true));
			}
			//Debug.Log ("alon___________ Cup - SetCompletedCup() - no data for this completed cup - data: " + GameManager.Instance.completedRunsData [id] + " - cup is completed");

			status = Cup.CupStatus.COMPLETED;
			GameManager.ActiveCompletedCup = this;
			GameManager.Instance.activeCompletedCupInstance = GameManager.ActiveCompletedCup;
			GameManager.ActiveCupCompleted = true;
		} else {
			//Debug.Log ("alon___________ Cup - SetCompletedCup() - trying to set completed cup data but no data - completedRunsData.Length: " + GameManager.Instance.completedRunsData.Length);
		}

		//		status = Cup.CupStatus.COMPLETED;

		//		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {
		//			CupsManager.Instance.SetActiveCup (this);
		//		} else {
		//			GameManager.ActiveCup = this;
		//		}

		//		GameManager.ActiveCompletedCup = this;
		//		GameManager.ActiveCupCompleted = true;
		//Debug.Log ("alon____________ SetCompletedCup() -  GameManager.ActiveCupCompleted: " + GameManager.ActiveCupCompleted);

		//		CupsManager.Instance.SetCupScreenRuns ();
		//		LockerRoom_Actions.instance.RunsScrnAnimToggle(true);
	}


	//	public void SelectRandomRuns(){
	//
	//		HashSet<int> tmpRuns = new HashSet<int> ();
	//		while(tmpRuns.Count < numOfRuns){
	//			tmpRuns.Add( attachedRuns[Random.Range(0, attachedRuns.Length)] );
	//		}
	//	
	//		int i = 0;
	//		selectedRuns = new List<RunAbstract> ();
	//		foreach (int r in tmpRuns) {
	//			selectedRuns.Add ( new RunAbstract( r ) );
	//			i++;
	//		}
	//	}
    public List<RunAbstract> GetCompletedRunsPerCup()
    {
        if (GameManager.Instance.completedRunsData [id] != "0") {
            string[] runsEncoded = GameManager.Instance.completedRunsData [id].Split (new char[] { '%' }, System.StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < runsEncoded.Length; i++) {
                completedSelectedRuns.Add (new RunAbstract (runsEncoded [i], true));
            }
            //Debug.Log ("alon___________ Cup - SetCompletedCup() - there is data for this completed cup - data: " + GameManager.Instance.completedRunsData [id] + " - cup is completed");

        }
        return completedSelectedRuns;
    }
	public void SetRuns(){
		selectedRuns = new List<RunAbstract> ();
		foreach (int i in attachedRuns) {
			selectedRuns.Add ( new RunAbstract( i ) );
//			Debug.Log ("alon_______ adding runs to selected runs");
		}
	}

	public string EncodeRuns(){
		string e = "";

		for (int i = 0; i < selectedRuns.Count; i++) {
			if (GameManager.CurrentRun != null && selectedRuns[i].id == GameManager.CurrentRun.id) selectedRuns[i] = GameManager.CurrentRun;
			e += selectedRuns[i].mEncode() + "%";
		}

		return e;
	}

	public string EncodeCompletedRuns(){
		string e = "";

		for (int i = 0; i < completedSelectedRuns.Count; i++) {
			if (GameManager.CurrentRun != null && completedSelectedRuns[i].id == GameManager.CurrentRun.id) completedSelectedRuns[i] = GameManager.CurrentRun;
			e += completedSelectedRuns[i].mEncode() + "%";
		}

		return e;
	}




	public void AddToWinnedCups(){
		status = CupStatus.COMPLETED;

		GameManager.ActiveCup.exist = false;

		// Calculating Score
		int GoldMedals = 0;
		foreach(RunAbstract run in selectedRuns){
			if(run.GoalAchieved == 2) GoldMedals+=1;
		}

		float success_percent = (float)GoldMedals / (float)numOfRuns;
		if (success_percent >= 1f) CompleteRank = 4;
		else if(success_percent >=  0.66666f) CompleteRank = 3;
		else if(success_percent >=  0.33333f) CompleteRank = 2;
		else CompleteRank = 1;

		UnitedAnalytics.LogEvent ("quest completed ", "Quest: "+(id + 1), UserData.Instance.userType.ToString (), DAO.NumOfCompletedCups);

		// Add to Wins if prev result is less than current
		int prevCompleteRank = GetPrevCompleteRank ();

		if (prevCompleteRank < CompleteRank) {

			if (prevCompleteRank < 0) {
				DAO.WinnedCups += id + "|" + CompleteRank + "%";
				GameManager.Instance.ReadWinnedCups ();
			} else {
				string[] c;
				for (int i=0; i<GameManager.Instance.winnedCups.Length; i++) {
					c = GameManager.Instance.winnedCups [i].Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries);
					if (int.Parse (c [0]) == id)
						GameManager.Instance.winnedCups [i] = id + "|" + CompleteRank;
				}
				GameManager.Instance.WriteWinnedCups ();
			}

		} else {
			CompleteRank = prevCompleteRank;
		}

		if (id == 0) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_1);
		}else if (id == 1) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_2);  // Twice...
		}else if (id == 2) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_3);
		}else if (id == 3) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_4);
		}else if (id == 4) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_5);
		}else if (id == 5) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_2);
		}else if (id == 6) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_6);
		}else if (id == 7) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_7);
		}else if (id == 8) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_8);
		}else if (id == 9) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_9);
		}else if (id == 10) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_10);
		}else if (id == 11) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_11);
		}else if (id == 12) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_12);
		}else if (id == 13) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_13);
		}else if (id == 14) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_14);
		}else if (id == 15) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_15);
		}else if (id == 16) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_16);
		}else if (id == 17) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_complete_17);
		}

		//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Quest_Completed (name);
		//GameAnalyticsWrapper.ProgressionEvent (GameAnalyticsWrapper.progerssionTypes.Complete, id, "QuestRun", "SoftDamage", 0);
		if(FB.IsLoggedIn) DDB._.ReportQuestProgress ();

		//Debug.Log ("alon_____________ Cup -  AddToWinnedCups() - thisRunData is somthing: " + DAO.ActiveRunData);
		GameManager.Instance.UpdateCupRunsData (id, DAO.ActiveRunData);
		//Debug.Log ("alon___________ AddToWinnedCups() - should update cup runs data");

		DAO.ActiveRunData = "";
		DAO.ActiveRunID = -1;

	}

	public int GetPrevCompleteRank(){
		string[] c;
		for (int i=0; i<GameManager.Instance.winnedCups.Length; i++) {
			c = GameManager.Instance.winnedCups[i].Split('|');
			if( int.Parse(c[0]) == id) return int.Parse(c[1]);
		}

		return -1;
	}


	public void Loose(){
		CompleteRank = GetPrevCompleteRank ();

		if (CompleteRank < 0) {
			CompleteRank = 0;
			status = CupStatus.NOT_STARTED;
		} else {
			status = CupStatus.COMPLETED;

		}

		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) {
			CupsManager.Instance.OngoingOBJ.SetActive (false);
		}

		if(FB.IsLoggedIn) DDB._.ReportQuestProgress ();
	}

	public int NumOfCompletedRuns{

		get{
			if (status != CupStatus.IN_PROGRESS)
				return 0;

			int completed = 0;

			foreach (RunAbstract run in selectedRuns) {
				if (run.status == RunAbstract.RunStatus.COMPLETED)
					completed++;
			}


			return completed;
		}
	}
    public KeyValuePair<bool,int> GetMostMedalsPerQuest(){
        string[] runsEncoded = GameManager.Instance.completedRunsData [id].Split (new char[] { '%' }, System.StringSplitOptions.RemoveEmptyEntries);
        int golds = 0;
        int silvers = 0;
        for (int i = 0; i < runsEncoded.Length; i++) {
            string[] d = runsEncoded[i].Split('|');
            int GoalAchieved = int.Parse ( d[2] );
            if (GoalAchieved == 1) {
                silvers++;
            } else if (GoalAchieved == 2) {
                golds++;
            }
        }
            
        if (golds >= silvers) {
            isGoldCup = true;
            isSilverCup = false;
            return new KeyValuePair<bool, int>(true,golds);
        } else {
            isGoldCup = false;
            isSilverCup = true;
            return new KeyValuePair<bool, int>(false,silvers);
        }
    }




	public void GetMadalPerQuest(){
		string[] runsEncoded = GameManager.Instance.completedRunsData [id].Split (new char[] { '%' }, System.StringSplitOptions.RemoveEmptyEntries);
		int golds = 0;
		int silvers = 0;
		for (int i = 0; i < runsEncoded.Length; i++) {
			string[] d = runsEncoded[i].Split('|');
			int GoalAchieved = int.Parse ( d[2] );
			if (GoalAchieved == 1) {
				silvers++;
			} else if (GoalAchieved == 2) {
				golds++;
			}
		}
			
		if (golds > silvers) {
			isGoldCup = true;
			isSilverCup = false;
		} else {
			isGoldCup = false;
			isSilverCup = true;
		}
	}


}
