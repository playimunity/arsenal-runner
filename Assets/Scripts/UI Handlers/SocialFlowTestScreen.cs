﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SocialFlowTestScreen : MonoBehaviour {

	public Button RecievedBtn;
	public Button RequestsBtn;

	public GameObject RecievedGC;
	public GameObject RequestedGC;

	public Text RecievedGCCounter;
	public Text GCRequestsCounter;



	public void OnEnable(){
		if (DDB._.RecievedGiftCards.Count > 0) {
			RecievedBtn.interactable = true;
			RecievedGC.SetActive (true);
			RecievedGCCounter.text = DDB._.RecievedGiftCards.Count.ToString ();
		} else {
			RecievedBtn.interactable = false;
			RecievedGC.SetActive (false);
		}

		if (DDB._.RecievedGiftCardsRequests.Count > 0) {
			RequestsBtn.interactable = true;
			RequestedGC.SetActive (true);
			GCRequestsCounter.text = DDB._.RecievedGiftCardsRequests.Count.ToString ();
		} else {
			RequestsBtn.interactable = false;
			RequestedGC.SetActive (false);
		}
	}

	public void OnDisable(){
		
	}




}
