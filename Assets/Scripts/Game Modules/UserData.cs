using UnityEngine;
using System.Collections;
using System;

public class UserData : MonoBehaviour {
	public enum UserType {NEW_USER=1, PAYING_USER=100, ENGAGED_A=2, ENGAGED_B=3, ENGAGED_C = 5,ENGAGED_D = 6};

	public string userID = "-"; // Advertisment ID or IDFA
	public int playTimeInMinutes = 0;
	public int playTimeInDays = 0;
	public string UserName = "Coach";
	public Texture2D UserImageTexture;
	private bool daoReady = false;

	public UserType userType;
	public static UserData Instance;

	public static void UpdateUserType(UserType new_type){
		//Debug.Log ("alon_____________ UpdateUserType() - start");
		//if (UserData.Instance.userType != null) {
		if (UserData.Instance.userType == null || new_type > UserData.Instance.userType) {
			//Debug.Log ("alon_____________ UpdateUserType() - if (new_type > UserData.Instance.userType) - user type: " + UserData.Instance.userType);
			UnitedAnalytics.LogEvent ("User Changed Type", UserData.Instance.userType.ToString (), new_type.ToString ());
			AppsFlyerManager._.ReportUserTypeChange (new_type);
			UserData.Instance.userType = new_type;
			DAO.UserType = UserData.Instance.userType;
           // CrossSceneUIHandler.Instance.UserTypeTxt.text = DAO.UserType.ToString();

			//Debug.Log ("alon_____________ UpdateUserType() - end");
		}
		



	}


	//---------------------------------------------------------------------------------

	void Start () {
		//Debug.Log("RON____________xuserData start Time:"+ DateTime.Now.ToLongTimeString());
		DAO.onDataReady += fillData;
		Instance = this;
		StartCoroutine ( playTimeCount() );
	}

	IEnumerator playTimeCount(){
		while (true) {
			//Debug.Log ("alon____________ playTimeCount() - start - playTimeInMinutes: " + playTimeInMinutes);
			yield return new WaitForSeconds(60f);
			playTimeInMinutes++;
			playTimeInDays = playTimeInMinutes / 60 / 24;
			if(daoReady) {
				DAO.PlayTimeInMinutes = playTimeInMinutes;
				DAO.PlayTimeInDays = playTimeInDays;
			}
			//Debug.Log ("alon____________ playTimeCount() - after if dao is ready");
			if (playTimeInMinutes >= DAO.Settings.TimeToEngagedUserA) UpdateUserType ( UserType.ENGAGED_A );
			if (playTimeInMinutes >= DAO.Settings.TimeToEngagedUserB) UpdateUserType ( UserType.ENGAGED_B );
			if (playTimeInDays >= DAO.Settings.TimeToEngagedUserC)    UpdateUserType ( UserType.ENGAGED_C );
			if (playTimeInDays >= DAO.Settings.TimeToEngagedUserD)    UpdateUserType ( UserType.ENGAGED_D );
			//Debug.Log ("alon____________ playTimeCount() - end");
		}
	}

	void fillData(){
		daoReady = true;
		UserData.Instance.userType = DAO.UserType;
		playTimeInMinutes = DAO.PlayTimeInMinutes;
		//Debug.Log("RON____________xuserData end Time:"+ DateTime.Now.ToLongTimeString());
	}



}
