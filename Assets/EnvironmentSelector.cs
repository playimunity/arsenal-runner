﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnvironmentSelector : MonoBehaviour {

	// Use this for initialization
    public static int lastIndex;
    public List<GameObject> environments;
    public int index;
	void Start () {
        index = UnityEngine.Random.Range(0, environments.Count);
        while (index == lastIndex)
        {
            index = UnityEngine.Random.Range(0, environments.Count);
        }
        environments[index].SetActive(true);
        lastIndex = index;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
