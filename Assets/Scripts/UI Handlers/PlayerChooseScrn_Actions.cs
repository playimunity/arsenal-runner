﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerChooseScrn_Actions : MonoBehaviour {


	public static PlayerChooseScrn_Actions instance;

	public List<int> playersToBuyIds = new List<int> ();
	public List<PlayerId> playersCards = new List<PlayerId>();

	public Transform gridParentTransform;

	public RectTransform scrollContentRectTransform;
	public RectTransform playerUiItem;
	public RectTransform popupRectTransform;

	float screenRatio;
	float playerBtnHeight = 328;

	Vector2 scrollAreaSize;
	Vector2 popupSize;

//	public RectTransform popupTransform;
//	Vector2 popupSize;
//	Vector2 popupOriginalSize;
//	float scaleRatio;


//	Vector2 btnOutPos;
//	Vector2 btnInPos;
//	Vector3 scrnOpenPos;



	void Awake(){
		instance = this;

		//iphone 7 ratio = 1.77
		//tablet ratio = 1.33
		//full height = 1504
		//min height = 1027

		popupSize = popupRectTransform.sizeDelta;
		screenRatio = (float)Screen.height / (float)Screen.width;
//		Debug.Log ("alon___________ Screen.height = " + Screen.height + " , Screen.width = " + Screen.width + " , screen ratio = " + screenRatio);
		float factor = screenRatio / 1.77f;
//		Debug.Log ("alon___________ factor = " + factor);
		popupSize.y = 1504f * factor;
//		Debug.Log ("alon___________ popupSize.y = " + popupSize.y);
		popupRectTransform.sizeDelta = popupSize;
	}

	void Start(){
		playerBtnHeight = playerUiItem.sizeDelta.y;
//		popupSize = popupOriginalSize = popupTransform.sizeDelta;
//		scaleRatio = Screen.height / Screen.width;

//		btnOutPos = btnInPos = scrnOpenPos = popupTransform.anchoredPosition;
//		btnOutPos.y = 0f;
//		btnInPos.y = Screen.height * 0.1f;
//		scrnOpenPos.y = Screen.height * 0.1f;
	}

//	public void ChangePopupResolution(bool smaller){
//		if (smaller) {
//			popupSize.y = popupOriginalSize.y / (scaleRatio * 1.2f);
//			popupTransform.sizeDelta = popupSize;
//		} else {
//			popupSize.y = popupOriginalSize.y;
//			popupTransform.sizeDelta = popupSize;
//		}
//	}


	void OnEnable(){
		playersToBuyIds = PlayerChooseManager.instance.playersToBuyIds;

		foreach (Transform playerCard in gridParentTransform) {    //creates a list with all the players:
			playersCards.Add (playerCard.GetComponent<PlayerId>());
			playerCard.gameObject.SetActive (false);
		}

		scrollAreaSize.y = playerBtnHeight;
		scrollAreaSize.x = scrollContentRectTransform.sizeDelta.x;

		for (int i = 0; i < playersCards.Count; i++) {
			if (i < playersToBuyIds.Count) {
				scrollAreaSize.y += playerBtnHeight;
				scrollContentRectTransform.sizeDelta = scrollAreaSize;
				playersCards [i].gameObject.SetActive (true);
				playersCards [i].SetPlayerCard (playersToBuyIds [i]);
                playersCards [i].PlayerPrefabIndex = playersToBuyIds [i];
			}
		}
	}


//	WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
//	Vector2 popupPos;
//	float lerpFraction = 0f;

//	public IEnumerator ScrnBtnIn(bool on){
//		Debug.Log ("alon________ ScrnBtnIn()");
//		if (on) {
//			Debug.Log ("alon________ ScrnBtnIn() - on");
//			popupTransform.anchoredPosition = btnOutPos;
//			popupPos = btnOutPos;
//			while (lerpFraction < 1f) {
//				lerpFraction += 1.5f * Time.deltaTime;
//				popupPos.y = Mathf.Lerp (popupPos.y, btnInPos.y, lerpFraction);
//				popupTransform.anchoredPosition = popupPos;
//				yield return waitFrame;
//				Debug.Log ("alon________ popupYPos in: " + popupPos);
//			}
//			popupTransform.anchoredPosition = btnInPos;
//		} else {
//			Debug.Log ("alon________ ScrnBtnIn() - off");
//			popupTransform.anchoredPosition = btnInPos;
//			popupPos = btnInPos;
//			while (lerpFraction < 1f) {
//				lerpFraction += 1.5f * Time.deltaTime;
//				popupPos.y = Mathf.Lerp (popupPos.y, btnOutPos.y, lerpFraction);
//				popupTransform.anchoredPosition = popupPos;
//				yield return waitFrame;
//				Debug.Log ("alon________ popupYPos out: " + popupPos);
//			}
//			popupTransform.anchoredPosition = btnOutPos;
//		}
//	}



	










}
