﻿using UnityEngine;
using System.Collections;
using System;

public class GM_Input : MonoBehaviour {
	
	public static GM_Input _;
	//RaycastHit hit;
	//Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	Touch touch;
	Vector2 prevTouchPos;
	Vector2 currentTouchPos;
	public RaycastHit hit;
	Ray ray;
	bool wasSwipe = false;
	//GameObject hitObject;

	public bool Lock = false;

	void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log ("Input | " + msg);
	}

	// cross scene popups
	public bool Store;
	public bool CupLoose;
	public bool GenericMessage;
	public bool ChooseFriend;

	// Main Menu popups
	public bool GiftCards;
	public bool Settings;
	public bool Stats;
	public bool Facebook;

	// Bench Popups
	public bool CupWon;
	public bool MedikKits;
	public bool Perks;
	public bool QuestMap;
	public bool RunsScreen;
	public bool Heal;
	public bool RecruitAnother;
	public bool Recruit;
	public bool PracticePopup;
	public bool CupsPopup;
	public bool WellcomeOffer;
	public bool PlayersList;
	public bool PlayerPreview;
	public bool LowEnergy;
	public bool RateUsPopup;
	public bool RateUsThanksPopup;

	// Tutorial Run popups
	public bool TutorialPause;
	public bool TutorialFail;
	public bool TutorialYouReady;
	public bool TutorialYouReady2;

	// Cup Run popups
	public bool CupRunPause;
	public bool CupRunLooseWon;
	public bool SaveMe;

	// Practice Run popups
	public bool PractisePause;
	public bool PracticeLoose;
	public bool SaveMePractice;

	// ad module
	public bool AdModulOn;


	void Awake(){
		_ = this;

		Store = false;
		CupLoose = false;
		GenericMessage = false;
		ChooseFriend = false;
		GiftCards = false;
		Settings = false;
		Stats = false;
		Facebook = false;
		CupWon = false;
		MedikKits = false;
		Perks = false;
		QuestMap = false;
		RunsScreen = false;
		Heal = false;
		RecruitAnother = false;
		Recruit = false;
		PracticePopup = false;
		CupsPopup = false;
		WellcomeOffer = false;
		PlayersList = false;
		PlayerPreview = false;
		TutorialPause = false;
		TutorialFail = false;
		TutorialYouReady = false;
		TutorialYouReady2 = false;
		CupRunPause = false;
		CupRunLooseWon = false;
		PractisePause = false;
		PracticeLoose = false;
		SaveMe = false;
		SaveMePractice = false;
		AdModulOn = false;


		prevTouchPos = Vector2.zero;
		currentTouchPos = Vector2.zero;
	}

	public static event Action RewriteBackBtnAction;



	void Update () {
		
		if(Input.touches.Length > 0){
			touch = Input.GetTouch(0); //touch definition
			
			
			if (touch.phase == TouchPhase.Began) { //if touch started
				//the first touch position
				prevTouchPos.x = touch.position.x;
				prevTouchPos.y = touch.position.y;
				wasSwipe = false;
			}
			
			if (touch.phase == TouchPhase.Moved) { //if is draging
				//the last touch position
				currentTouchPos.x = touch.position.x;
				currentTouchPos.y = touch.position.y;
				if (Mathf.Abs(currentTouchPos.x - prevTouchPos.x) > Mathf.Abs(currentTouchPos.y - prevTouchPos.y)){ // horizontal swipe
					

					if (!wasSwipe && (currentTouchPos.x - prevTouchPos.x) > Screen.width / 60) { // swipe right
						wasSwipe = true;
						OnSwipeRight ();
					} else if (!wasSwipe && (currentTouchPos.x - prevTouchPos.x) < -Screen.width / 60) { // swipe left
						wasSwipe = true;
						OnSwipeLeft ();
					} 
					
				}
				else{ // vertical swipe
					
					if (!wasSwipe && (currentTouchPos.y - prevTouchPos.y) > Screen.width/60){ // swipe up
						wasSwipe = true;
						OnSwipeUp();
					}
					else if (!wasSwipe && (currentTouchPos.y - prevTouchPos.y) < -Screen.width/60){ // swipe down
						wasSwipe = true;
						OnSwipeDown();
					}
				}
				prevTouchPos = currentTouchPos;
			}
			//Vector2 touchPosition = Input.GetTouch(0).position;
			if (touch.phase == TouchPhase.Ended){
				if(wasSwipe) return;
				ray = Camera.main.ScreenPointToRay(prevTouchPos);
				if (Physics.Raycast (ray, out hit )){  
					OnTap(hit);
				}
			}
		}
		
		

		if (Input.GetKeyUp ("up")) {
			OnSwipeUp();
		}
		if (Input.GetKeyUp ("left")) {
			OnSwipeLeft();
		}
		if (Input.GetKeyUp ("right")) {
			OnSwipeRight();
		}
		if (Input.GetKeyUp ("down")) {
			OnSwipeDown();
		}
		
		if(Input.GetMouseButtonDown(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hit )){  
				OnTap(hit);
			}
		}


		if (Input.GetKeyUp (KeyCode.B)) {
			PlayerController.instance.CollectBall ();
		}

		if ( Input.GetKeyDown(KeyCode.Escape) ) {
			HandleBackButton ();
		}
	}
	
	
	
	void OnTap(RaycastHit hit){


		switch (GameManager.curentGameState) {

			case GameManager.GameState.CUP_RUN :{
				if(!CupRunPause && hit.collider.tag == "Target"){
					PlayerController.instance.LaunchBall(hit.collider.gameObject);
				}
				break;
			}
			case GameManager.GameState.INFINITY_RUN :
			case GameManager.GameState.TEST_RUN :{
				//Debug.Log ("alon________________ OnTap() - GameObject: " + hit.collider.name + " - " + hit.collider.tag);
				if(!PractisePause && hit.collider.tag == "Target"){
					PlayerController.instance.LaunchBall(hit.collider.gameObject);
					//Debug.Log ("alon________________ OnTap() - GameObject: " + hit.collider.name + " - " + hit.collider.tag);
				}
				break;
			}
			case GameManager.GameState.LOCKER_ROOM :{

//				if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject (-1) || UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject ()) {
//					return;
//				}
//
				break;
			}
		case GameManager.GameState.TUTORIAL_RUN:{
				if (hit.collider.tag == "Target") {
					Tutorial.Instance.LaunchBall (hit.collider.gameObject);
				}
				break;
			}
		}
	}
	
	void OnSwipeLeft(){
		switch (GameManager.curentGameState) {
		case GameManager.GameState.CUP_RUN:{
				if (!CupRunPause) {
					PlayerController.instance.TurnLeft ();
				}
				break;
			}
		case GameManager.GameState.INFINITY_RUN:
		case GameManager.GameState.TEST_RUN :{
				if (!PractisePause) {
					PlayerController.instance.TurnLeft ();
				}
			break;
		}
		case GameManager.GameState.LOCKER_ROOM :{
				PlayerChooseManager.instance.Swipe(1);
			break;
		}
		case GameManager.GameState.TUTORIAL_RUN:{
				Tutorial.Instance.SwipeLeft ();
				break;
		}
		}
	}
	
	void OnSwipeRight(){
		switch (GameManager.curentGameState) {
		case GameManager.GameState.CUP_RUN:{
				if (!CupRunPause) {
					PlayerController.instance.TurnRight ();
				}
				break;
			}
		case GameManager.GameState.INFINITY_RUN:
		case GameManager.GameState.TEST_RUN :{
				if (!PractisePause) {
					PlayerController.instance.TurnRight ();
				}
			break;
		}
		case GameManager.GameState.LOCKER_ROOM :{
				PlayerChooseManager.instance.Swipe(-1);
			break;
		}
		case GameManager.GameState.TUTORIAL_RUN:{
				Tutorial.Instance.SwipeRight ();
				break;
			}
		}
	}
	
	void OnSwipeUp(){
		switch (GameManager.curentGameState) {
		case GameManager.GameState.CUP_RUN:{
				if (!CupRunPause) {
					PlayerController.instance.Jump ();
				}
				break;
			}
		case GameManager.GameState.INFINITY_RUN:
		case GameManager.GameState.TEST_RUN :{
				if (!PractisePause) {
					PlayerController.instance.Jump ();
				}
			break;
		}
		case GameManager.GameState.LOCKER_ROOM :{
			break;
		}
		case GameManager.GameState.TUTORIAL_RUN:{
				Tutorial.Instance.Jump ();
				break;
			}
		}
	}
	
	void OnSwipeDown(){
		switch (GameManager.curentGameState) {
		case GameManager.GameState.CUP_RUN:{
				if (!CupRunPause) {
					PlayerController.instance.Slide ();
				}
				break;
			}
		case GameManager.GameState.INFINITY_RUN:
		case GameManager.GameState.TEST_RUN :{
				if (!PractisePause) {
					PlayerController.instance.Slide ();
				}
			break;
		}
		case GameManager.GameState.LOCKER_ROOM :{
			break;
		}
		case GameManager.GameState.TUTORIAL_RUN:{
				Tutorial.Instance.Slide ();
				break;
			}
		}
	}
		
	void HandleBackButton(){

		if (Lock) {
			Log ("Back is Locked!");
			return;
		}

		// Cross Scene UI
		if (Store || CupLoose || GenericMessage || ChooseFriend || AdModulOn) {

//			if (CupLoose)
//				CrossSceneUIHandler.Instance.hideCupLooseScreen ();
			if (Store)
				CrossSceneUIHandler.Instance.hideStore ();
			else if (GenericMessage)
				CrossSceneUIHandler.Instance.hideGenericMessage ();
			else if (ChooseFriend)
				CrossSceneUIHandler.Instance.HideFriendChooseScreen ();
			else if (AdModulOn)
				Ads._.OnAdClosed ();

			return;
		}

		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) HandleBackOnLockerRoom ();
		//else if (GameManager.curentGameState == GameManager.GameState.MAIN_MENU) HandleBackOnMainMenu ();
		else if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) HandleBackOnTutorialRun ();
		else if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) HandleBackOnCupRun ();
		else if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) HandleBackOnPracticeRun ();	
	}



//	void HandleBackOnMainMenu(){
//
//		if (GiftCards) MainScreenUiManager.instance.CardScrnToggle (false);
//		else if (Settings) MainScreenUiManager.instance.SettingsScrnToggle (false);
//		else if (Stats) StatsController.Instance.StatsScrnAnimTogge (false);
//		else if (Facebook) MainMenu_Actions.instance.FacebookPopupToggle (false);
//	//	else if(GameManager.curentGameState == GameManager.GameState.MAIN_MENU) PromtAppQuit ();
//
//	}

	void HandleBackOnLockerRoom(){
		if (DAO.NumOfPurchasedPlayers < 1) return; 

		if (CupWon)
			MainScreenUiManager.instance.WinScrnToggle (false);
		else if (RunsScreen)
			MainScreenUiManager.instance.RunsScrnAnimToggle (false);
		else if (QuestMap)
			MainScreenUiManager.instance.CupScrnAnimToggle (false);
		else if (PlayersList)
			MainScreenUiManager.instance.PlayerChooseScrnAnim (false);
		else if (RateUsPopup) {
			if (RateUsThanksPopup) {
				MainScreenUiManager.instance.RateUsThanksToggle (false);
			} else {
				MainScreenUiManager.instance.OnRateUsClose (false);
			}
		}
		else if (Heal)
			MainScreenUiManager.instance.HealPopup (false);
		else if (LowEnergy) {
			//LowEnergy = false;
			MainScreenUiManager.instance.LowEnergyPopup (false);
		}
		else if (Recruit) MainScreenUiManager.instance.RecruitSecondPopup (false);
		else if (RecruitAnother) MainScreenUiManager.instance.RecruitThirdPopup (false, 0);
		else if (PracticePopup) MainScreenUiManager.instance.PracticePopup (false);
		else if (CupsPopup) MainScreenUiManager.instance.CupPopup (false);
		else if (WellcomeOffer) MainScreenUiManager.instance.OfferPopup(false);
		else if (MedikKits) MainScreenUiManager.instance.MidekKitsScreenToggle(false);
		else if (Perks) MainScreenUiManager.instance.PerksScrnAnim(false);

		//else GameManager.SwitchState (GameManager.GameState.MAIN_MENU);

		else if (GiftCards) MainScreenUiManager.instance.CardScrnToggle (false);
		else if (Settings) MainScreenUiManager.instance.SettingsScrnToggle (false);
		else if (Stats) StatsController.Instance.StatsScrnAnimTogge (false);
		else if (PlayerPreview) PlayerChooseManager.instance.CancelPlayerPreview ();
		//else if (Facebook) MainMenu_Actions.instance.FacebookPopupToggle (false);
		else if(GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM) PromtAppQuit ();
	}

	void HandleBackOnTutorialRun(){

//		if (TutorialPause) CupRunController.Instance.UnpauseCountDown ();
//		else if (TutorialFail) CupRunController.Instance.SkipTutorial ();
//		else if (TutorialYouReady) CupRunController.Instance.SkipTutorial ();
//		else if (TutorialYouReady2) CupRunController.Instance.SkipTutorial ();
//
//		else CupRunController.Instance.Pause ();
	}

	void HandleBackOnCupRun(){
		if (CupRunPause) CupRunController.Instance.UnpauseCountDown ();
		else if(SaveMe) CupRunController.Instance.HideSaveMeScreen ();
		else if (CupRunLooseWon) CupRunController.Instance.EndRun ();

		else if(!PlayerController.instance.isDead && !PlayerController.instance.onAction) CupRunController.Instance.Pause ();
	}

	void HandleBackOnPracticeRun(){
		if (PractisePause) PracticeRunController.Instance.UnpauseCountDown ();
		else if (PracticeLoose) PracticeRunController.Instance.GoToLockerRoom ();
		else if (SaveMePractice) PracticeRunController.Instance.HideSaveMeScreen ();
		else if (!PlayerController.instance.isDead && !PlayerController.instance.onAction) PracticeRunController.Instance.Pause ();
	}

	void PromtAppQuit(){
		Debug.Log ("alon__________ Quiting the game!");
		NativeDialogs.Instance.ShowMessageBox(DAO.Language.GetString ("qd-title"), DAO.Language.GetString ("qd-message"), new string[] {DAO.Language.GetString ("qd-quit"), DAO.Language.GetString ("qd-cancel")}, true, (string button) => {
			if(button == DAO.Language.GetString ("qd-quit")) Application.Quit();
		});

	}

}

