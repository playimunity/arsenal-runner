﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldSection : MonoBehaviour {

	float COIN_HEIGHT = 0.5f;

	public enum RegionType{HR, TB, OS, CT, MS , SJP};
	public RegionType currentRegionType;
    public bool dontSetActiveFalse;
	public enum SectionType{Straight , Alley , TurnL , TurnR , Cross , PreExit , Exit ,PreEnd, End , TB_Enter , TB_Empty , TB_PreExit , TB_Exit1 , TB_Exit2 , TB_Exit3 , Enter , SJP_Bridge , SJP_House};
	//public enum SectionType{G5, G5T , G5TR , G5TL , G5B , G5U , G5D , G5T3 , G3, G3T , G3TR , G3TL , G3B , G3U , G3D , G3T5 , GT3 , GT3T , GT3TR , GT3TL , GT3EN , GT3EX , GT3TS , S3 , S5 , S3EN , S3EX , S3O , R3EN , R3 , R3EX , GEND , P3G , P3W , P3G2W , P3W2G , P3EX , P3EN , ST5 , STEX };
	public SectionType currentSectionType;

	public Transform objectsContainer;
	//public Transform buildingsContainer;

	float[] heightMap = {5f, 4.964f, 4.923f, 4.872f, 4.767f, 4.643f, 4.487f, 4.309f, 4.107f, 3.892f, 
		3.66f, 3.433f, 3.19f, 2.97f, 2.729f, 2.507f, 2.285f, 2.07f, 1.865f, 1.621f, 
		1.397f, 1.177f, 0.96f, 0.753f, 0.555f, 0.383f, 0.252f, 0.135f, 0.054f, 0.015f, 0f};


	//List<GameObject> buildings = new List<GameObject>();

	//public List<Transform> Locators15M = new List<Transform>();

	//int zFactor;

	//Quaternion globalBuildingRotation;

//	float globalYRotation;
//	float globalAvalableSpace;
//	float globalStaticPosition;
//	float globalDinamicPosition;

	Vector3 vz = Vector3.zero;
	Vector3 vz2 = Vector3.zero;
	Vector3 startObstaclePos = new Vector3 (0, 3, 0);
	GameObject goz;


	public bool buildLow = true;
	Vector3 Rot;
	Vector3 TargetPos;
	Vector3 TMPos;

	public int sectionNumber;

	static WaitForEndOfFrame _weof;
	static WaitForEndOfFrame weof{
		get{
			if (_weof == null)
				_weof = new WaitForEndOfFrame ();

			return _weof;
		}
	}

	IEnumerator MoveUp(){

		while (TMPos.y < -20f) {
			TMPos.y += 10f * Time.deltaTime;
			transform.position = TMPos;
			yield return weof;
		}

		Rot = transform.eulerAngles;
		Rot.x += 40f;
		transform.eulerAngles = Rot;

		while (TMPos.y < TargetPos.y) {
			TMPos.y += 10f * Time.deltaTime;
			transform.position = TMPos;

			Rot.x -= 20f * Time.deltaTime;
			transform.eulerAngles = Rot;
			yield return weof;
		}

//		while (Rot.x > 0f) {
//			Rot.x -= 15f * Time.deltaTime;
//			transform.eulerAngles = Rot;
//
//			yield return new WaitForEndOfFrame ();
//		}

		Rot.x = 0f;
		transform.eulerAngles = Rot;


		TMPos.y = TargetPos.y;
		transform.position = TMPos;
	}






	float GetCollectableHeightByZ(float z){

//		if (currentSectionType == WorldSection.SectionType.G3U || currentSectionType == WorldSection.SectionType.G5U) {
//			return (heightMap [ (int)Mathf.Ceil (z) ] + heightMap [(int)Mathf.Floor (z)]) / 2f + COIN_HEIGHT;
//		} else if(currentSectionType == WorldSection.SectionType.G3D || currentSectionType == WorldSection.SectionType.G5D){
//			return (heightMap [ 30 - (int)Mathf.Ceil (z) ] + heightMap [ 30 - (int)Mathf.Floor (z)]) / 2f + COIN_HEIGHT;
//		}else{
//			return COIN_HEIGHT;
//		}

		return COIN_HEIGHT;
		//return 7f;
	}

	float GetObstacleHeightByZ(float z){
		
//		if (currentSectionType == WorldSection.SectionType.G3U || currentSectionType == WorldSection.SectionType.G5U) {
//			return (heightMap [ (int)Mathf.Ceil (z) ] + heightMap [(int)Mathf.Floor (z)]) / 2f + 0.2f;
//		} else if(currentSectionType == WorldSection.SectionType.G3D || currentSectionType == WorldSection.SectionType.G5D){
//			return (heightMap [ 30 - (int)Mathf.Ceil (z) ] + heightMap [ 30 - (int)Mathf.Floor (z)]) / 2f + 0.2f;
//		}else{
//			return 0.2f;
//		}

		return 2.0f;
	}


	public void PlaceObjects(List<CollectableAbstract> collectables, List<ObstacleAbstract> obstacles){
		if(collectables.Count > 0) PlaceCollectables (collectables);
		if(obstacles.Count > 0) PlaceObstacles (obstacles);
	}

	Transform TMP_TRANS;
	public void PlaceCollectables(List<CollectableAbstract> collectables){


		foreach (CollectableAbstract col in collectables) {

			if (col.type == Collectable.CollectableTypes.COIN) {

				//vz.x =  Mathf.Round(col.x/10f);
				vz.x = GetObjectXPos (col.x/10f);
				vz.z = - (col.z/10f);
				vz.y = GetCollectableHeightByZ(col.z/10f);

				col.ObjectOnScene = CoinPool._.GetCoin ();

				TMP_TRANS = col.ObjectOnScene.transform;
				TMP_TRANS.parent = objectsContainer;
				TMP_TRANS.localPosition = vz;
				TMP_TRANS.rotation = objectsContainer.transform.rotation;

				continue;
			}

			switch(col.type){
//				case Collectable.CollectableTypes.COIN : {
//					goz = PrefabManager.instanse.coin;
//					break;
//				}
				case Collectable.CollectableTypes.MAGNET : {
					if(!PlaceMagnetOrNot()) continue;
					goz = PrefabManager.instanse.magnet;
					break;
				}
//				case Collectable.CollectableTypes.ENERGY : {
//					goz = PrefabManager.instanse.energyDrink;
//					break;
//				}

				case Collectable.CollectableTypes.BALL_STACK : {
					goz = PrefabManager.instanse.ballStack;
					break;
				}

				case Collectable.CollectableTypes.WHISTLE : {
					goz = PrefabManager.instanse.whistle;
					break;
				}

				case Collectable.CollectableTypes.TUT : {
					goz = PrefabManager.instanse.tutorialPoint;
					break;
				}

				case Collectable.CollectableTypes.SPEEDBOOSTER : {
					goz = PrefabManager.instanse.speedBooster;
					break;
				}

				case Collectable.CollectableTypes.X2COINS : {
					goz = PrefabManager.instanse.x2Coins;
					break;
				}

//				case Collectable.CollectableTypes.STADIUM_PORTAL : {
//					goz = PrefabManager.instanse.StadiumPortal;
//					break;
//				}
                case Collectable.CollectableTypes.SHIELD : {
                    goz = PrefabManager.instanse.shield;
                    break;
                }

//				default:{
//					goz = PrefabManager.instanse.coin;
//					break;
//				}
			}

			//vz.x =  Mathf.Round(col.x/10f);
			vz.x = GetObjectXPos (col.x/10f);
			vz.z = - (col.z/10f);
			//vz.y = GetCollectableHeightByZ(col.z/10f);
			vz.y = 0f;
			
			col.ObjectOnScene = (GameObject)Instantiate (goz , Vector3.zero, objectsContainer.transform.rotation );
			col.ObjectOnScene.transform.parent = objectsContainer;
			col.ObjectOnScene.transform.localPosition = vz;

		}
	}

	float GetObjectXPos(float xPos){
		if (xPos > 11f && xPos < 13f) {
//			Debug.Log ("alon___________ previos x pos = " + xPos + " , return 12f");
			return 12f;
		} else if (xPos > 13f && xPos < 14f) {
//			Debug.Log ("alon___________ previos x pos = " + xPos + " , return 13.5f");
			return 13.5f;
		} else if (xPos > 14f && xPos < 16f) {
//			Debug.Log ("alon___________ previos x pos = " + xPos + " , return 15f");
			return 15f;
		} else if (xPos > 16f && xPos < 17f) {
//			Debug.Log ("alon___________ previos x pos = " + xPos + " , return 16.5f");
			return 16.5f;
		} else if (xPos > 17f && xPos < 19f) {
//			Debug.Log ("alon___________ previos x pos = " + xPos + " , return 18f");
			return 18f;
		}else{
//			Debug.Log ("alon___________ previos else xPos = " + xPos + " , return xPos");
			return xPos;
		}
	}

	public bool PlaceMagnetOrNot(){

		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) return true;

		if (Random.value < PlayerController.instance.Perk_ChanceToMeetMagnetFactor) return true;
		return false;
	
	}

	public void PlaceObstacles(List<ObstacleAbstract> obstacles){



		foreach (ObstacleAbstract obst in obstacles){

			switch(obst.type){
//				case Obstacle.ObstacleTypes.BCNETA 						: {goz = PrefabManager.instanse.BCNeta;break;}
//				case Obstacle.ObstacleTypes.STREET_LAMP 				: {goz = PrefabManager.instanse.StreetLamp;break;}
//				case Obstacle.ObstacleTypes.WHEELBARROW 				: {goz = PrefabManager.instanse.Wheelbarrow;break;}
//				case Obstacle.ObstacleTypes.WHEELBARROW2 				: {goz = PrefabManager.instanse.Wheelbarrow2;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_1 		: {goz = PrefabManager.instanse.UnderConstraction1;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_2 		: {goz = PrefabManager.instanse.UnderConstraction2;break;}
//				case Obstacle.ObstacleTypes.BICYCLE 					: {goz = PrefabManager.instanse.Bicycle;break;}
//				case Obstacle.ObstacleTypes.BICYCLE_ROW 				: {goz = PrefabManager.instanse.BicycleRow;break;}
//				case Obstacle.ObstacleTypes.BUS_STOP 					: {goz = PrefabManager.instanse.BusStop;break;}
//				case Obstacle.ObstacleTypes.CROWD 						: {goz = PrefabManager.instanse.crowd;break;}
//				case Obstacle.ObstacleTypes.CAFE1 						: {goz = PrefabManager.instanse.Cafe1;break;}
//				case Obstacle.ObstacleTypes.CAFE2 						: {goz = PrefabManager.instanse.Cafe2;break;}
//				case Obstacle.ObstacleTypes.Cafe3 						: {goz = PrefabManager.instanse.Cafe3;break;}
//				case Obstacle.ObstacleTypes.Cafe4 						: {goz = PrefabManager.instanse.Cafe4;break;}
//				case Obstacle.ObstacleTypes.MAP_SIGN1 					: {goz = PrefabManager.instanse.MapSign1;break;}
//				case Obstacle.ObstacleTypes.MAP_SIGN2 					: {goz = PrefabManager.instanse.MapSign2;break;}
//				case Obstacle.ObstacleTypes.MINIBUS 					: {goz = PrefabManager.instanse.Minibus;break;}
//				case Obstacle.ObstacleTypes.MOPED 						: {goz = PrefabManager.instanse.Moped;break;}
//				case Obstacle.ObstacleTypes.SEDAN 						: {goz = PrefabManager.instanse.Sedan;break;}
//				case Obstacle.ObstacleTypes.TRAFFIC_LIGHT1 				: {goz = PrefabManager.instanse.TrafficLight;break;}
//				case Obstacle.ObstacleTypes.TRASH_CAN_BIG 				: {goz = PrefabManager.instanse.TrashCanBig;break;}
//				case Obstacle.ObstacleTypes.TRASH_CAN_BIG2 				: {goz = PrefabManager.instanse.TrashCanBig2;break;}
//				case Obstacle.ObstacleTypes.TRASH_CAN_BIG3 				: {goz = PrefabManager.instanse.TrashCanBig3;break;}
//				case Obstacle.ObstacleTypes.TRASH_CAN_SMALL 			: {goz = PrefabManager.instanse.TrashCanSmall;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_3 		: {goz = PrefabManager.instanse.UnderConstraction3;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_4 		: {goz = PrefabManager.instanse.UnderConstraction4;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_5 		: {goz = PrefabManager.instanse.UnderConstraction5;break;}
//				case Obstacle.ObstacleTypes.UNDER_CONSTRACTION_6 		: {goz = PrefabManager.instanse.UnderConstraction6;break;}


//				case Obstacle.ObstacleTypes.Car_Hatchback 					: {goz = PrefabManager.instanse.Hatchback;break;}
//				case Obstacle.ObstacleTypes.Car_Taxi 				    	: {goz = PrefabManager.instanse.Taxi;break;}
//				case Obstacle.ObstacleTypes.Enemy_Run 				    	: {goz = PrefabManager.instanse.EnemyRun;break;}
//				case Obstacle.ObstacleTypes.Enemy_Slide 					: {goz = PrefabManager.instanse.enemySlide;break;}
//				case Obstacle.ObstacleTypes.Female 					    	: {goz = PrefabManager.instanse.Female;break;}
//				case Obstacle.ObstacleTypes.Male 				  			: {goz = PrefabManager.instanse.Male;break;}
//				case Obstacle.ObstacleTypes.Flower_Pot 						: {goz = PrefabManager.instanse.Flowers;break;}
//				case Obstacle.ObstacleTypes.Flower_Pot2 						: {goz = PrefabManager.instanse.Flowers2;break;}
//				case Obstacle.ObstacleTypes.Flower_Pot3 						: {goz = PrefabManager.instanse.Flowers3;break;}
//				case Obstacle.ObstacleTypes.Flower_Stall_1 					: {goz = PrefabManager.instanse.FlowerStall1;break;}
//				case Obstacle.ObstacleTypes.Flower_Stall_2 					: {goz = PrefabManager.instanse.FlowerStall2;break;}
//				case Obstacle.ObstacleTypes.Food_Stall_1 					: {goz = PrefabManager.instanse.FoodStall1;break;}
//				case Obstacle.ObstacleTypes.Food_Stall_2 					: {goz = PrefabManager.instanse.FoodStall2;break;}
//				case Obstacle.ObstacleTypes.Moped_Moving 					: {goz = PrefabManager.instanse.MopedMoving;break;}
//				case Obstacle.ObstacleTypes.People_With_Glass 				: {goz = PrefabManager.instanse.GlassPeople;break;}
//				case Obstacle.ObstacleTypes.Pillar 							: {goz = PrefabManager.instanse.Pillar;break;}
//				case Obstacle.ObstacleTypes.Truck_Food 						: {goz = PrefabManager.instanse.TruckFood;break;}
//				case Obstacle.ObstacleTypes.Truck_Small 					: {goz = PrefabManager.instanse.TruckSmall;break;}
//				case Obstacle.ObstacleTypes.T_Sign 							: {goz = PrefabManager.instanse.TSign;break;}
//				case Obstacle.ObstacleTypes.T_Sign2 						: {goz = PrefabManager.instanse.TSign2;break;}
//				case Obstacle.ObstacleTypes.Barrier1 						: {goz = PrefabManager.instanse.Barrier1;break;}
//				case Obstacle.ObstacleTypes.Barrier2 						: {goz = PrefabManager.instanse.Barrier2;break;}
//				case Obstacle.ObstacleTypes.Barrier3 						: {goz = PrefabManager.instanse.Barrier3;break;}
//				case Obstacle.ObstacleTypes.Blocking_Road 					: {goz = PrefabManager.instanse.BlockingRoad;break;}
//				case Obstacle.ObstacleTypes.FallingTree 					: {goz = PrefabManager.instanse.FallingTree;break;}
//				case Obstacle.ObstacleTypes.Gate2 							: {goz = PrefabManager.instanse.Gate2;break;}
//				case Obstacle.ObstacleTypes.Electric_Gate_Close 			: {goz = PrefabManager.instanse.ElectricGateClose;break;}
//				case Obstacle.ObstacleTypes.Electric_Gate_Open 				: {goz = PrefabManager.instanse.ElectricGateOpen;break;}
//				case Obstacle.ObstacleTypes.Door1 				        	: {goz = PrefabManager.instanse.door1;break;}
//				case Obstacle.ObstacleTypes.Door2 							: {goz = PrefabManager.instanse.door2;break;}
//				case Obstacle.ObstacleTypes.Half_Hanging1 					: {goz = PrefabManager.instanse.halfHanging1;break;}
//				case Obstacle.ObstacleTypes.Half_Hanging2 					: {goz = PrefabManager.instanse.halfHanging2;break;}
//				case Obstacle.ObstacleTypes.Hanging1 						: {goz = PrefabManager.instanse.hanging1;break;}
//				case Obstacle.ObstacleTypes.Hanging2 						: {goz = PrefabManager.instanse.hanging2;break;}
//				case Obstacle.ObstacleTypes.Side_Building 					: {goz = PrefabManager.instanse.sideBuilding;break;}
//				case Obstacle.ObstacleTypes.Sign1 							: {goz = PrefabManager.instanse.sign1;break;}
//				case Obstacle.ObstacleTypes.Sign2 							: {goz = PrefabManager.instanse.sign2;break;}
//				case Obstacle.ObstacleTypes.HuemanStatue1 					: {goz = PrefabManager.instanse.huemanStatue1;break;}
//				case Obstacle.ObstacleTypes.HuemanStatue2 					: {goz = PrefabManager.instanse.huemanStatue2;break;}
//				case Obstacle.ObstacleTypes.HuemanStatue3 					: {goz = PrefabManager.instanse.huemanStatue3;break;}
//				case Obstacle.ObstacleTypes.Train 							: {goz = PrefabManager.instanse.train;break;}
//				case Obstacle.ObstacleTypes.BenchMosaic1 				    : {goz = PrefabManager.instanse.BenchMosaic1;break;}
//				case Obstacle.ObstacleTypes.BenchMosaic2 					: {goz = PrefabManager.instanse.BenchMosaic2;break;}
//				case Obstacle.ObstacleTypes.BenchMosaic3 					: {goz = PrefabManager.instanse.BenchMosaic3;break;}
//				case Obstacle.ObstacleTypes.BenchMosaic4 					: {goz = PrefabManager.instanse.BenchMosaic4;break;}
//				case Obstacle.ObstacleTypes.Guell_Boulder 					: {goz = PrefabManager.instanse.Boulder;break;}
//				case Obstacle.ObstacleTypes.Guell_Bush1 					: {goz = PrefabManager.instanse.GuellBush1;break;}
//				case Obstacle.ObstacleTypes.Guell_Bush2 					: {goz = PrefabManager.instanse.GuellBush2;break;}
//				case Obstacle.ObstacleTypes.Guell_Bush3 					: {goz = PrefabManager.instanse.GuellBush3;break;}
//				case Obstacle.ObstacleTypes.Guell_Trash 					: {goz = PrefabManager.instanse.GuellTrash;break;}
//				case Obstacle.ObstacleTypes.GuellTrunk 						: {goz = PrefabManager.instanse.GuellTrunk;break;}
//				case Obstacle.ObstacleTypes.PalmTree 						: {goz = PrefabManager.instanse.PalmTree;break;}
//				case Obstacle.ObstacleTypes.StoneBench 						: {goz = PrefabManager.instanse.StoneBench;break;}

			case Obstacle.ObstacleTypes.BUSH 					    	: {goz = PrefabManager.instanse.Ars_Bush1;break;}
			case Obstacle.ObstacleTypes.Bush2 					    	: {goz = PrefabManager.instanse.Ars_Bush2;break;}
			case Obstacle.ObstacleTypes.Bush3 					    	: {goz = PrefabManager.instanse.Ars_Bush3;break;}
			case Obstacle.ObstacleTypes.BUSH4                           : {goz = PrefabManager.instanse.Ars_Bush4;break;}
				case Obstacle.ObstacleTypes.BUS 							: {goz = PrefabManager.instanse.Ars_Bus;break;}
				case Obstacle.ObstacleTypes.CAR1 							: {goz = PrefabManager.instanse.Ars_Car1;break;}
				case Obstacle.ObstacleTypes.CAR2 							: {goz = PrefabManager.instanse.Ars_Car2;break;}
				case Obstacle.ObstacleTypes.CAR3 							: {goz = PrefabManager.instanse.Ars_Car3;break;}
				case Obstacle.ObstacleTypes.CAR4 							: {goz = PrefabManager.instanse.Ars_Car4;break;}
				case Obstacle.ObstacleTypes.CAR_POLICE 						: {goz = PrefabManager.instanse.Ars_Car_Police;break;}
				case Obstacle.ObstacleTypes.TAXI 							: {goz = PrefabManager.instanse.Ars_Taxi;break;}
				case Obstacle.ObstacleTypes.TRUCK1 							: {goz = PrefabManager.instanse.Ars_Truck1;break;}
			    case Obstacle.ObstacleTypes.TRUCK2 							: {goz = PrefabManager.instanse.Ars_Truck2;break;}
                case Obstacle.ObstacleTypes.TRUCK3                          : {goz = PrefabManager.instanse.Ars_Truck3;break;}
                case Obstacle.ObstacleTypes.TRUCK4                          : {goz = PrefabManager.instanse.Ars_Truck4;break;}
                case Obstacle.ObstacleTypes.TRUCK5                          : {goz = PrefabManager.instanse.Ars_Truck5;break;}
				case Obstacle.ObstacleTypes.VAN1 							: {goz = PrefabManager.instanse.Ars_Van1;break;}
				case Obstacle.ObstacleTypes.VAN2 							: {goz = PrefabManager.instanse.Ars_Van2;break;}
				case Obstacle.ObstacleTypes.VAN_POLICE 						: {goz = PrefabManager.instanse.Ars_Van_Police;break;}
				case Obstacle.ObstacleTypes.GARDEN 							: {goz = PrefabManager.instanse.Ars_Garden;break;}
				case Obstacle.ObstacleTypes.CAMERA_POST 					: {goz = PrefabManager.instanse.Ars_Camera_Post;break;}
				case Obstacle.ObstacleTypes.FLAG_ROPE 						: {goz = PrefabManager.instanse.Ars_Flag_Rope;break;}
				case Obstacle.ObstacleTypes.ROADBLOCK_JUMP 					: {goz = PrefabManager.instanse.Ars_RoadBlock_Jump;break;}
				case Obstacle.ObstacleTypes.ROADBLOCK_3_LANE 				: {goz = PrefabManager.instanse.Ars_Roadblock_3_Lane;break;}
				case Obstacle.ObstacleTypes.ROADBLOCK_SLIDE 				: {goz = PrefabManager.instanse.Ars_Roadblock_Slide;break;}
				case Obstacle.ObstacleTypes.SPACESHIP 						: {goz = PrefabManager.instanse.Ars_Spaceship;break;}
                case Obstacle.ObstacleTypes.CLOTHES_CART                    : {goz = PrefabManager.instanse.Ars_Clothes_Cart;break;}
                case Obstacle.ObstacleTypes.CLOTHES_STALL                   : {goz = PrefabManager.instanse.Ars_Clothes_Stall;break;}
                case Obstacle.ObstacleTypes.CONCRETE_TRUCK1                 : {goz = PrefabManager.instanse.Ars_Concrete_Truck1;break;}
                case Obstacle.ObstacleTypes.CONCRETE_TRUCK2                 : {goz = PrefabManager.instanse.Ars_Concrete_Truck2;break;}
                case Obstacle.ObstacleTypes.FOOD_VAN                        : {goz = PrefabManager.instanse.Ars_FoodVan;break;}
                case Obstacle.ObstacleTypes.GARBAGE_TRUCK                   : {goz = PrefabManager.instanse.Ars_GarbageTruck;break;}
                case Obstacle.ObstacleTypes.GARDEN1                         : {goz = PrefabManager.instanse.Ars_Garden1;break;}
                case Obstacle.ObstacleTypes.HOTDOGS_STALL                   : {goz = PrefabManager.instanse.Ars_HotDogStall;break;}
                case Obstacle.ObstacleTypes.ICECREAME_STALL                 : {goz = PrefabManager.instanse.Ars_Icecream_Stall;break;}
                case Obstacle.ObstacleTypes.TREE1                           : {goz = PrefabManager.instanse.Ars_Tree1;break;}
                case Obstacle.ObstacleTypes.TREE2                           : {goz = PrefabManager.instanse.Ars_Tree2;break;}

			}



			// Align Goth Obstacles
//			if( obst.type ==  Obstacle.ObstacleTypes.Hanging1 || obst.type ==  Obstacle.ObstacleTypes.Hanging2
//				|| obst.type ==  Obstacle.ObstacleTypes.Half_Hanging2 || obst.type ==  Obstacle.ObstacleTypes.Half_Hanging1)
//			{
//
//				vz.x = 15f;
//
//			}else if(obst.type ==  Obstacle.ObstacleTypes.Side_Building
//			         || obst.type ==  Obstacle.ObstacleTypes.Door1
//			         || obst.type ==  Obstacle.ObstacleTypes.Door2
//			){
//
//				if(obst.x < 150f) vz.x = 13f;
//				else vz.x = 17f;
//
//			}else if(obst.type ==  Obstacle.ObstacleTypes.Sign1 || obst.type ==  Obstacle.ObstacleTypes.Sign2){
//
//				if(obst.x < 150f) vz.x = 12.5f;
//				else vz.x = 17.5f;
//
//			}

			//vz.x =  Mathf.Round(obst.x/10f);
			vz.x = GetObjectXPos (obst.x / 10f);


			vz.z = - (obst.z/10f);
			//vz.y = GetObstacleHeightByZ(obst.z/10f);
			//vz.y = 3f;
			vz.y = 0f;
			// rotation
			vz2.y = (int)obst.rotate;


			obst.ObjectOnScene = (GameObject)Instantiate (goz , startObstaclePos , Quaternion.Euler(transform.rotation.eulerAngles + vz2) );
			obst.ObjectOnScene.transform.parent = objectsContainer;
			//Debug.Log ("ron__________ obst.ObjectOnScene.transform.position: " + obst.ObjectOnScene.transform.localPosition + " - " + obst.ObjectOnScene.name);
			obst.ObjectOnScene.transform.localPosition = vz;
			//Debug.Log ("ron__________ obst.ObjectOnScene.transform.position: " + obst.ObjectOnScene.transform.localPosition + " - " + obst.ObjectOnScene.name);
			if(obst.move == Obstacle.MoveTypes.MOVE){
				obst.ObjectOnScene.GetComponent<Obstacle>().move = obst.move;
			}



			//if(obst.ObjectOnScene.GetComponent<Obstacle>().smashable == Obstacle.SmashableTypes.SMASHABLE)
		}

	}




}
