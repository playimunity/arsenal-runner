using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class Bench : MonoBehaviour {

	public static Bench instance;

	public enum BenchState { BENCH,PREVIEW };

	public BenchState benchState = BenchState.BENCH;

	int playerToAdd = 1;
	public int cellCount = 1;
	public int currentCell = 1;
	public int maxSits = 20;

	float benchWidth = 0.85f;

	Vector3 newCellPosition;
	Vector3 newPlayerPosition = new Vector3(0,0,-7.9f);
	Vector3 newPlayerScale = new Vector3 (1f,1f,1f);
	Vector3 plusBtnPosition;
	Vector3 currentCameraPosition;
	Vector3 targetCameraPosition;
	Vector3 additionPlusCellPosition;
	Vector3 voz;
	//Vector2 planeUvOffset = new Vector2 (0, 0);

	Quaternion newPlayerRotation = Quaternion.Euler(0,-180,0);

	public GameObject playerChooseScreen;
	public GameObject addBtn;
	public GameObject[] cellPrefabs;
	GameObject newPlayer;
	GameObject currentPreviewPlayer;
	public GameObject currentBenchPlayer;
	private GameObject lastBenchPlayer;
	GameObject cell;
	GameObject cellExtra;
	public GameObject myCamera;
	public GameObject playerToBuyInfo;
	public GameObject playerTapBtn;
	public GameObject homeBtn;
	public GameObject CancelBtn;
	public GameObject starGlowParent;

	public List<GameObject> playersOnBench = new List<GameObject>();

	public List<PlayerId> playersToBuyCards = new List<PlayerId> ();

	bool fullPlayers = false;
	public bool swiping = false;
	public bool inPreselectdRunMode = false;
	public bool additionCell = false;
	public bool playerIsStanding = false;
	public bool playingAnim = false;
	public bool startAnimation;
	public bool tutPopupsDisabled = false;
	public bool isFreePlayer = false;

	public Vector3 PlayerPositionInCell;

	public int CurrentPlayerPrice;
	//int cellPrefabRandomNum;
	public int currentPlayerCardIndex;

	public Image GoBTNImage;
	public Text GoBTNText;
	public Color GoBTNPositiveColor;
	public Color GoBTNNegativeColor;

	public GameObject PlayerCardsParent;
	public GameObject motionBlurPlane;
	public Animator motionPlaneAnim;

	public Transform playersCardsContent;
	public Transform cameraOriginalPosition;

	//public GameObject recruitBtn;


	public static void Log(string msg){
		if (AppSettings._ == null || !AppSettings._.DEBUG) return;
		Debug.Log("Bench | " + msg);
	}


	void Awake(){
		instance = this;
		PlayerPositionInCell = new Vector3 (0f, -0.2f, -0.9f);
		currentBenchPlayer = null;
		if (Tutorial.IsNeedToShowTutorialRun) { // if in tutorial, delete all Purchased Players.
			DAO.PurchasedPlayers = "";
			DAO.Instance.LOCAL.SaveAll ();
			homeBtn.SetActive (false);
		}

		CurrentPlayerPrice = DAO.GetPlayersPrice ( GetNumOfPurchasedPlayers() );
	}


	void Start () {
		startAnimation = true;
		swiping = true;
		playerChooseScreen.SetActive (true);
		PlayerChooseScrn_Actions_old.instance.CreatePlayersIdsList ();

		newCellPosition = transform.position;
		additionPlusCellPosition = transform.position;
		additionPlusCellPosition.x -= benchWidth;
		CreateFirstCell ();
		InstantiatePurchasedPlayers ();

		playerTapBtn.SetActive (false);
		LockerRoom_ActionsOld.instance.LeftArrow (false);
		LockerRoom_ActionsOld.instance.RightArrow (false);
		LockerRoom_ActionsOld.instance.GoToPlusToggle (false);


		CheckCoinsCollected ();

		Ads._.OnBench ();
	}


	#if UNITY_EDITOR
	void Update(){
		if (Input.GetKeyUp (KeyCode.R)) {
			LockerRoom_ActionsOld.instance.ShowRateUsPopup ();
		}
	}
	#endif


	public void CreateFirstCell(){
		if (cellCount == maxSits)
			return;

		//cellPrefabRandomNum = Random.Range (0, cellPrefabs.Length);
		cell = (GameObject)Instantiate(cellPrefabs[0] , transform.position , transform.rotation);
		cell.transform.parent = transform;
		cell.transform.position = newCellPosition;
		cell.GetComponent<CellOrginaizer> ().cellNumber = cellCount;
	}

	public GameObject GetPlayerOnBenchByPlayerId(int id){
		foreach (GameObject pl in playersOnBench) {
			if (pl.GetComponent<Player> ().PlayerID == id) {
				return pl;
			}
		}
		return playersOnBench [0];
	}

	public void InstantiatePurchasedPlayers(){
		if (DAO.PurchasedPlayers.Length <= 0) {
			StartCoroutine (CameraStartAnimation(false));
			//LockerRoom_Actions.instance.AddBtnAnimation (true);
			return;
		}

		string[] pp = DAO.PurchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries);

		for (int i=0; i<pp.Length; i++) {
			AddNewPlayerImideately( int.Parse(pp[i]) );
		}
			
		if (PrefabManager.instanse.chosenPlayer.PlayerName == "") {
			currentBenchPlayer = GetPlayerOnBenchByPlayerId (int.Parse (pp [0]));
			PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
		} else {
			bool goodChosenPlayer = false;
			foreach (string pid in pp) {
				if (PrefabManager.instanse.chosenPlayer.PlayerID.ToString () == pid) {
					currentBenchPlayer = GetPlayerOnBenchByPlayerId (PrefabManager.instanse.chosenPlayer.PlayerID);
					//PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
					goodChosenPlayer = true;
				}
			}
			if (!goodChosenPlayer) {
				currentBenchPlayer = GetPlayerOnBenchByPlayerId (int.Parse (pp [0]));
				PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
			}
			PlayerDataUIScreen.Instance.SetViewWithCurrentPlayer ();
		}

		voz = cameraOriginalPosition.position;
		if (!isFreePlayer) {
			voz.x = currentBenchPlayer.transform.parent.position.x;
			currentCell = currentBenchPlayer.GetComponent<Player> ().placeInBench;
		} else {
			voz.x = newCellPosition.x;
			currentCell = cellCount;
		}

		cameraOriginalPosition.position = voz;

		// handles the camera's animation on start:
		currentCameraPosition = myCamera.transform.position;
		if (cellCount < 6) {
			currentCameraPosition.x = newCellPosition.x;
		} else {
			currentCameraPosition.x = 5.0f;
		}
		myCamera.transform.position = currentCameraPosition;

		StartCoroutine (CameraStartAnimation(true));

	}

	public Player np;
	public void AddNewPlayerImideately(int playerId){

		// instantiate the player:
		newPlayer = (GameObject)Instantiate(PrefabManager.instanse.BenchPlayerPrefab , newPlayerPosition , newPlayerRotation);
		newPlayer.transform.parent = cell.transform;
		np = newPlayer.GetComponent<Player> ();
		np.SetPlayerStructure (playerId);
		np.placeInBench = cellCount;
		np.data.UpdateHealthCondition ();
		//np.Sit(true);
		np.ChangeWalkSpeed (false);

		playersOnBench.Add (newPlayer);
		cell.transform.FindChild ("shirt01").gameObject.SetActive (false);

//		if (np.playerStructure.isLedgend == true) {
//			cell.GetComponent<CellOrginaizer> ().ledgentStarsFX.SetActive (true);
//		}

		if (cellCount < maxSits) {
			newCellPosition.x += benchWidth;
			//cellPrefabRandomNum = Random.Range (0, cellPrefabs.Length);
			cell = (GameObject)Instantiate (cellPrefabs [0], transform.position, transform.rotation);
			cell.transform.parent = transform;
			cell.transform.position = newCellPosition;
			cell.GetComponent<CellOrginaizer> ().cellNumber = cellCount;

			cellCount++;
			plusBtnPosition.x += benchWidth;
			//addBtn.transform.position = plusBtnPosition;
			newPlayerPosition.x += benchWidth;
			if (cellCount > 2 && !additionCell) { // create additional plus cell:
				//cellPrefabRandomNum = Random.Range (0, cellPrefabs.Length);
				cellExtra = (GameObject)Instantiate (cellPrefabs [0], transform.position, transform.rotation);
				cellExtra.transform.parent = transform;
				cellExtra.transform.position = additionPlusCellPosition;
				additionCell = true;
			}
		} else {
			fullPlayers = true;
			LockerRoom_ActionsOld.instance.recruitBtnAnim.gameObject.SetActive (false);
			LockerRoom_ActionsOld.instance.AddBtnAnimation (false);
			LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
			cellExtra.SetActive(false);
			additionCell = false;
		}

		playerChooseScreen.SetActive (true);
		PlayerChooseScrn_Actions_old.instance.RemovePlayerId (playerId);
		playerChooseScreen.SetActive (false);
	}




//	public void AddPlayer(int playerNum){
//		swiping = true;
//
//		PrefabManager.instanse.ChoosePlayer (playerNum);
//
//		newPlayer = (GameObject)Instantiate(PrefabManager.instanse.BenchPlayerPrefab , newPlayerPosition , newPlayerRotation);
//		newPlayer.transform.localScale = newPlayerScale;
//		newPlayer.transform.parent = cell.transform;
//		np = newPlayer.GetComponent<Player> ();
//		np.SetPlayerStructure (playerNum);
//		np.placeInBench = cellCount;
//		np.WalkIn (false);
//
//		StartCoroutine (ContractSigned ());
//
//		}


	public void BuyPlayerFromPreveiw(bool isIAP){

		newPlayer.transform.parent = cell.transform;
		//newPlayer.GetComponent<Player> ().placeInBench = cellCount - 1;
		np = newPlayer.GetComponent<Player> ();
		np.placeInBench = currentCell;
		np.Yesss();
		//np.Sit (false);
		np.ChangeWalkSpeed (false);
		playersOnBench.Add (newPlayer);
		currentBenchPlayer = newPlayer;
		PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
		OnPlayerBuyGeneral2 ();

		if (playersOnBench.Count == 1) {
			UnitedAnalytics.LogEvent ("First player picked ", "purchased", np.name);
		}else if (playersOnBench.Count == 3) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_we_have_a_club);
		}else if (playersOnBench.Count == 10) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_top_ten);
		}else if (playersOnBench.Count == 12) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_full_squad);
		}

//		if (np.playerStructure.isLedgend == true) {
//			NativeSocial._.ReportAchievment (SocialConstants.achievement_old_school_style);
//		}

		if (np.PlayerID == 23) {   // Luis Enrique
			NativeSocial._.ReportAchievment (SocialConstants.achievement_big_boss);
		}

		//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Inventory_Increase ("Locker Room", np.playerName, "");
		GameAnalyticsWrapper.BuyNewPlayer(np.playerName,CurrentPlayerPrice);

		if (isFreePlayer) {
			isFreePlayer = false;
		}
	}

	public void BuyFirstPlayerFromPreveiw(bool isIAP){

		newPlayer.transform.parent = cell.transform;
		//newPlayer.GetComponent<Player> ().placeInBench = cellCount - 1;
		np = newPlayer.GetComponent<Player> ();
		np.placeInBench = currentCell;
//		np.Yesss();
//		np.Sit (false);
		np.ChangeWalkSpeed (false);
		playersOnBench.Add (newPlayer);
		currentBenchPlayer = newPlayer;
		PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
		OnPlayerBuyGeneral2 ();

		if (playersOnBench.Count == 1) {
			UnitedAnalytics.LogEvent ("First player picked ", "purchased", np.name);
		}

//		if (np.playerStructure.isLedgend == true) {
//			NativeSocial._.ReportAchievment (SocialConstants.achievement_old_school_style);
//		}

		if (np.PlayerID == 23) {   // Luis Enrique
			NativeSocial._.ReportAchievment (SocialConstants.achievement_big_boss);
		}

		//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Inventory_Increase ("Locker Room", np.playerName, "");
		GameAnalyticsWrapper.BuyNewPlayer(np.playerName, CurrentPlayerPrice);
	}

	IEnumerator ContractSignedPreview(bool isIAP = false){
		LockerRoom_ActionsOld.instance.SwipingHand (false);
		LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (false);
		LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
		yield return new WaitForSeconds (0.2f);
		LockerRoom_ActionsOld.instance.ContractAnim();
		//yield return new WaitForSeconds (1f);
		//OnPlayerBuyGeneral1 (); // for adding the extra sit
		yield return new WaitForSeconds (2.3f);
		previewCancelBtn.SetActive (false);
		if (playersOnBench.Count == 0) {
			BuyFirstPlayerFromPreveiw (isIAP);
			//Debug.Log ("alon___________ Bench -  buying first player !");
		} else {
			BuyPlayerFromPreveiw (isIAP);
		}
	}

//	IEnumerator ContractSigned(){
//		//LockerRoom_Actions.instance.SwipingHand (false);
//		//LockerRoom_Actions.instance.PlayerToBuyCardAnimToggle (false);
//		yield return new WaitForSeconds (1.5f);
//		LockerRoom_Actions.instance.ContractAnim();
//		yield return new WaitForSeconds (1f);
//		//OnPlayerBuyGeneral1 ();
//		yield return new WaitForSeconds (1.3f);
//		np.Yesss();
//		np.Sit (false);
//		playersOnBench.Add (newPlayer);
//		currentBenchPlayer = newPlayer;
//		OnPlayerBuyGeneral2 ();
//	}



	public void OnPlayerSit(){
		if (playerIsStanding) {
			playerIsStanding = false;
		} else {
			if (!startAnimation) {
				if (currentBenchPlayer != null) {
					MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);
					//MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (true);
				}

				playerTapBtn.SetActive (true);

//				if (GameManager.BenchInPreselectedRunMode) {
//					if (!fullPlayers) {
//						LockerRoom_Actions.instance.GoToPlusToggle (true);
//					} else {
//						LockerRoom_Actions.instance.GoToPlusToggle (false);
//					}
//					LockerRoom_Actions.instance.StartRunBtnAnim (true);
//					swiping = false;
//				} 

				if (!fullPlayers) {
					LockerRoom_ActionsOld.instance.GoToPlusToggle (true);
				} else {
					LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
				}
				LockerRoom_ActionsOld.instance.RunBtnAnim (true);
				swiping = false;

				if (IsNeedToShowRateUsPopup) {
					LockerRoom_ActionsOld.instance.ShowRateUsPopup ();
				}
			}
			//CheckFitnessOfSelectedPlayer ();
			if (benchState == BenchState.PREVIEW) {
				benchState = BenchState.BENCH;
			//	GM_Input._.PlayerPreview = false;
			}
		}
		playingAnim = false;
	}





	public void WatchPlayerInfo(int index , bool mirror){
		swiping = true;
					
		// find the current player index:
		foreach (PlayerId playerCard in playersToBuyCards) {
			if (playerCard.PlayerPrefabIndex == index) {
				currentPlayerCardIndex = playersToBuyCards.IndexOf (playerCard);
			}
		}
		PreviewPlayerEnter(index , mirror);
		LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
	}


	void PreviewPlayerEnter(int index , bool mirror){
		// Instantiate the player:
		PrefabManager.instanse.ChoosePlayer (index);
		newPlayer = (GameObject)Instantiate(PrefabManager.instanse.BenchPlayerPrefab , newPlayerPosition , newPlayerRotation);
		np = newPlayer.GetComponent<Player> ();
		np.SetPlayerStructure (index);

		newPlayer.transform.localScale = newPlayerScale;
		newPlayer.transform.parent = cell.transform.Find("temp player").transform;

		np.ChangeWalkSpeed (true);
		np.standType = 2;
		np.WalkIn (mirror);
		LockerRoom_ActionsOld.instance.GoToPlusToggle (false);

//		if (np.playerStructure.isLedgend == true) {
//			cell.GetComponent<CellOrginaizer> ().ledgentStarsFX.SetActive (true);
//		} else {
//			cell.GetComponent<CellOrginaizer> ().ledgentStarsFX.SetActive (false);
//		}
	}


	public void StandIdle(){
		PlayerToBuyInfoUIScreen.Instance.SetViewFromChoosenPlayer ();
		LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (true);
		previewCancelBtn.SetActive (true);
		if (!fullPlayers) {
			LockerRoom_ActionsOld.instance.GoToPlusToggle (true);
		} else {
			LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
		}

		if(currentPreviewPlayer != null)
			Destroy (currentPreviewPlayer);

		LockerRoom_ActionsOld.instance.TutorialRecruitBounce (true);

		swiping = false;
	}


	public void OnPlayerInfoClicked(int index){
		
		//LockerRoom_Actions.instance.PlayerChooseScrnAnim (false);
		ClosePlayerChooseScreen();

		if (index == np.PlayerID) {
			//Debug.Log ("alon___ index: " + index + "np.PlayerID: " + np.PlayerID);
			return;
		}
		swiping = true;
		StartCoroutine (OnPlayerInfoClickedCoroutine(index));
	}


	IEnumerator OnPlayerInfoClickedCoroutine(int index){
		yield return new WaitForSeconds (0.25f);
		LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (false);
		np.WalkOut (true);
		yield return new WaitForSeconds (1.0f);

		playersToBuyCards.Clear ();
		Destroy (newPlayer);
		newPlayer = null;

		foreach (Transform playerCard in playersCardsContent) {  // create a list of avalable players cards
			playersToBuyCards.Add (playerCard.GetComponent<PlayerId>());
		}

		WatchPlayerInfo (index , false);
	}


//	public void OnPlayerBuyClicked(int index){
//		swiping = true;
//		int price = CurrentPlayerPrice;
//
//		if (index == 24) {
//			price = DAO.GetCoachPrice();
//		}
//
//		if (price > DAO.TotalCoinsCollected) {
//			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
//			IAP.AdditionalIAPCallback += OnIAPMAdeAfterPlayerBuyClicked;
//		} else {
//			DAO.TotalCoinsCollected -= price;
//
//
//			CrossSceneUIHandler.Instance.UpdateCoinsAmount();
//			LockerRoom_Actions.instance.PlayerChooseScrnAnim (false);
//			LockerRoom_Actions.instance.AddBtnAnimation (false);
//			AddPlayer (index);
//		}
//	}


	void OnIAPMAdeAfterPlayerBuyClicked(string sku){
		IAP.AdditionalIAPCallback -= OnIAPMAdeAfterPlayerBuyClicked;

		UnitedAnalytics.LogEvent ("IAP from players", sku, UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers);
	}


	public void OnPlayerBuyClickedFromPreview(){
		if (swiping)
			return;

		if (np.PlayerID == 12) {		// Puyol
			IAP.Instance.OnPuyolClciked ();
			return;
		}

		if (np.PlayerID == 19) {		// Ronaldinho
			IAP.Instance.OnRonaldinhoClciked ();
			return;
		}

		if (np.PlayerID == 15) {		// Stoichkov
			IAP.Instance.OnStoichkovClciked ();
			return;
		}

		if (np.PlayerID == 20) {		// Rivaldo
			IAP.Instance.OnRivaldoClciked ();
			return;
		}

		if (np.PlayerID == 17) {		// Koeman
			IAP.Instance.OnKoemanClciked ();
			return;
		}

		if (np.PlayerID == 14) {		// Pep
			IAP.Instance.OnPepClciked ();

			return;
		}


		int price = CurrentPlayerPrice;

		if (np.playerNumber == 0) {
			price = DAO.GetCoachPrice();
		}

		if (price > DAO.TotalCoinsCollected) {
			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
			IAP.AdditionalIAPCallback += OnIAPMAdeAfterPlayerBuyClicked;
		} else {
			swiping = true;
			DAO.TotalCoinsCollected -= price;

			if (np.playerNumber == 0) {
				AppsFlyerManager._.ReportVirtualEconomy ("Manager Bought", np.playerName, price.ToString (), "1");
			} else {
				AppsFlyerManager._.ReportVirtualEconomy ("Players", np.playerName, price.ToString (), "1");
			}

			//if (GameManager.Instance.isPlayscapeLoaded) {
				if (price > 0)
				GameAnalyticsWrapper.BuyNewPlayer(np.playerName,price);
			///		BI._.Wallet_Withdraw (price, "Player Bought", np.playerName, "", "", "");
			//GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, saveMePrice, GameAnalyticsWrapper.SAVE_ME, "SaveMe");
				//BI._.Inventory_Upgrade (30, "Player Energy", "Player Bought", np.playerName);

				//BI._.Inventory_Upgrade (1, "Player ", "Bench", np.playerName);

				//BI._.Inventory_Upgrade (1, "Player Level", "Player Bought", np.playerName);
			//}

			GameManager.Instance.HandleRankScoreChange ();
			AudioManager.Instance.OnLevelUp ();
		
			//BuyPlayerFromPreveiw ();
			StartCoroutine (ContractSignedPreview());
			if (GameManager.Instance.isPlayscapeLoaded) {
				if (DAO.NumOfPurchasedPlayers == 1) {
					BI._.Flow_FTUE (BI.FLOW_STEP.ACQUIRED_2ND_PLAYER);
				} else if (DAO.NumOfPurchasedPlayers == 0) {
					BI._.Flow_FTUE (BI.FLOW_STEP.BOUGHT_FREE_PLAYER);
					//UnitedAnalytics.LogEvent ("First player picked ", "purchased", Player name , UserData.Instance.userType.ToString ());
					//CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
				}
			}
			if (DAO.NumOfPurchasedPlayers == 0)
				GameAnalyticsWrapper.DesignEvent (GameAnalyticsWrapper.FLOW_STEP.PRESS_BUY_PLAYER);
			CrossSceneUIHandler.Instance.UpdateCoinsAmount ();
		}

	}

	public void OnLegendPurchaseSuccess(){
		swiping = true;
		StartCoroutine (ContractSignedPreview(true));
	}


	public void OpenPlayerChooseScreen(){

		MainScreenUiManager.instance.PlayerChooseScrnAnim (true);
		swiping = true;
	}

	public void ClosePlayerChooseScreen(){
		
		MainScreenUiManager.instance.PlayerChooseScrnAnim (false);
		swiping = false;
	}

	public void CancelPlayerPreview(){
		benchState = BenchState.BENCH;
		np.ChangeWalkSpeed (false);
		MainScreenUiManager.instance.SwipingHand (false);
		LockerRoom_ActionsOld.instance.TutorialRecruitBounce (false);
		LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
		StartCoroutine (RemovePlayerToWatch());
//		GM_Input._.PlayerPreview = false;
	}

	IEnumerator RemovePlayerToWatch(){

//		if (Tutorial.IsNeedToShowTutorialRun || Tutorial.IsNeedToShowRunLockerRoomTutorial || Tutorial.IsNeedToShowRunLockerRoomSkipTutorial) {
//			LockerRoom_Actions.instance.TutorialChoosePlayer (false);
//		}

		LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (false);
		newPlayer.GetComponent<Player> ().WalkOut (false);

		cell.GetComponent<CellOrginaizer> ().ledgentStarsFX.SetActive (false);

		yield return new WaitForSeconds (0.7f);

		playersToBuyCards.Clear ();

		if (benchState == BenchState.PREVIEW) {
			MainScreenUiManager.instance.PlayerChooseScrnAnim (true);
			LockerRoom_ActionsOld.instance.AddBtnAnimation (false);
		}
//		else if (Tutorial.IsNeedToShowTutorialRun || Tutorial.IsNeedToShowRunLockerRoomTutorial || Tutorial.IsNeedToShowRunLockerRoomSkipTutorial) {
//			LockerRoom_Actions.instance.AddBtnAnimation (true);
//			if (LockerRoom_Actions.instance.connectedToFacebook) {
//				LockerRoom_Actions.instance.TutorialConnectedAnim (true);
//			} else {
//				LockerRoom_Actions.instance.TutorialNotConnectedAnim (true);
//			}
//		}
		else {
			previewCancelBtn.SetActive (false);
			LockerRoom_ActionsOld.instance.AddBtnAnimation (true);
		}
		yield return new WaitForSeconds (0.3f);	
		Destroy (newPlayer);
		newPlayer = null;
		if(cellCount > 4) LockerRoom_ActionsOld.instance.LeftArrow (true);

		if (!(Tutorial.IsNeedToShowTutorialRun || Tutorial.IsNeedToShowRunLockerRoomTutorial || Tutorial.IsNeedToShowRunLockerRoomSkipTutorial)) {
			swiping = false;
		}
	}



	WaitForSeconds _wfs_0_75;
	WaitForSeconds wfs_0_75{
		get{ 
			if (_wfs_0_75 == null)_wfs_0_75 = new WaitForSeconds (0.75f);
			return _wfs_0_75;
		}
	}

	IEnumerator PlayerToBuyCardShow(){
		yield return wfs_0_75;
		LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (true);
	}


	void SwipePlayerPreview(int direction){
		if (swiping || playersToBuyCards.Count < 2)
			return;
		
		if (direction < 0) {
			swiping = true;
			currentPreviewPlayer = newPlayer;
			LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (false);
//			if (Tutorial.IsNeedToShowTutorialRun) {
//				LockerRoom_Actions.instance.TutorialRecruitBounce (false);
//			}
			currentPreviewPlayer.GetComponent<Player> ().WalkOut (false);

			currentPlayerCardIndex -= 1;

			if (currentPlayerCardIndex < 0) currentPlayerCardIndex = playersToBuyCards.Count - 1;

//			PreviewPlayerEnter (playersToBuyCards [currentPlayerCardIndex].PlayerPrefabIndex , true);
		}
		else if (direction > 0) {
			swiping = true;
			currentPreviewPlayer = newPlayer;
			LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (false);
//			if (Tutorial.IsNeedToShowTutorialRun) {
//				LockerRoom_Actions.instance.TutorialRecruitBounce (false);
//			}
			currentPreviewPlayer.GetComponent<Player> ().WalkOut (true);

			currentPlayerCardIndex += 1;
			if (currentPlayerCardIndex > playersToBuyCards.Count - 1) currentPlayerCardIndex = 0;


//			PreviewPlayerEnter (playersToBuyCards [currentPlayerCardIndex].PlayerPrefabIndex , false);
		}

		MainScreenUiManager.instance.SwipingHand (false);
			
	}




	public void Swipe(int direction){
		if (!swiping) {
			if (benchState == BenchState.BENCH) {
				swiping = true;
				StartCoroutine (SwipeEnumerator (direction));
			}
			else if(benchState == BenchState.PREVIEW)
				SwipePlayerPreview (direction);
		}
	}

	WaitForEndOfFrame _weof;
	WaitForEndOfFrame weof{
		get{ 
			if(_weof == null) _weof = new WaitForEndOfFrame();
			return _weof;
		}
	}

	IEnumerator SwipeEnumerator(int direction){

		MainScreenUiManager.instance.MidekKitsScreenToggle (false);


		currentCameraPosition = myCamera.transform.position;
		targetCameraPosition = currentCameraPosition;

		if (direction > 0 && cellCount > currentCell) { // on swipe left and not at the end...

			if(playerIsStanding)
				playerSit ();
			
			LockerRoom_ActionsOld.instance.AddBtnAnimation (false);
			//MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (false);

			targetCameraPosition.x += benchWidth;

			for (float i=0f; i <= 1f; i += (2f * Time.deltaTime)) {
				myCamera.transform.position = Vector3.Lerp (myCamera.transform.position, targetCameraPosition, i);
				if (i >= 0.5f)
					i = 1f;
				yield return weof;
			}
			myCamera.transform.position = targetCameraPosition;

			currentCell++;

			if (currentCell == cellCount && !fullPlayers) { // if we're at the plus cell delete current player and activate the plus btn:
				currentBenchPlayer = null;

				MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (false);
				LockerRoom_ActionsOld.instance.AddBtnAnimation (true);
				LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
				playerTapBtn.SetActive (false);

//				if (GameManager.BenchInPreselectedRunMode) {
//					LockerRoom_Actions.instance.StartRunBtnAnim (false);
//				} else {
//					LockerRoom_Actions.instance.RunBtnAnim (false);
//				}
				LockerRoom_ActionsOld.instance.RunBtnAnim (false);

				swiping = false;

				//LockerRoom_Actions.instance.ArrowsToggle (-1);
				if (cellCount > 4)
					LockerRoom_ActionsOld.instance.LeftArrow (true);
				LockerRoom_ActionsOld.instance.RightArrow (false);
				
			} else { // if we're not at the plus cell, update current player:

				foreach (GameObject curPlayer in playersOnBench) {
					if (curPlayer.GetComponent<Player> ().placeInBench == currentCell) {
						currentBenchPlayer = curPlayer;
						PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
					}
				}
				
//				if (GameManager.BenchInPreselectedRunMode) {
//					LockerRoom_Actions.instance.StartRunBtnAnim (true);
//				} else {
//					LockerRoom_Actions.instance.RunBtnAnim (true);
//				}
				LockerRoom_ActionsOld.instance.RunBtnAnim (true);
				//currentBenchPlayer.GetComponent<Player> ().UpdateFitness ();
				//CheckFitnessOfSelectedPlayer ();

				MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);
			//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (true);

				swiping = false;

				playerTapBtn.SetActive (true);

				if (Tutorial.IsNeedToShowHealPopup) {  //heal popup:
					if (currentBenchPlayer.GetComponent<Player> ().data.curHealthState == PlayerData.HealthState.WOUNDED) {
						MainScreenUiManager.instance.LowEnergyPopup (true);
						DAO.HealPopoup = 1;
					} else {
						MainScreenUiManager.instance.LowEnergyPopup (false);
					}
				}

				if (currentCell == cellCount - 1) { // if we're at the last player:
					if(cellCount > 4) LockerRoom_ActionsOld.instance.LeftArrow (true);
					LockerRoom_ActionsOld.instance.RightArrow (false);
				} else if (currentCell == 1) { // if we're at the first player:
					LockerRoom_ActionsOld.instance.LeftArrow (false);
					if(cellCount > 4) LockerRoom_ActionsOld.instance.RightArrow (true);
				}else if (currentCell == cellCount && fullPlayers) { //on the last player and have all the players
					LockerRoom_ActionsOld.instance.RightArrow (false);
				} else {
					if(cellCount > 4) LockerRoom_ActionsOld.instance.LeftArrow (true);
					if(cellCount > 4) LockerRoom_ActionsOld.instance.RightArrow (true);
				}

				if (!fullPlayers) {
					LockerRoom_ActionsOld.instance.GoToPlusToggle (true);
				} else {
					LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
				}
			}

		} else if (direction > 0 && currentCell == cellCount) { // on swipe left and we're at the end...

			StartCoroutine (CameraCantMove (1));

		} else if (direction < 0 && ( currentCell > 1 || (currentCell == 1 && additionCell))) { // on swipe right and not at the start...
			
			if(playerIsStanding)
				playerSit ();
			
		//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (false);
			LockerRoom_ActionsOld.instance.AddBtnAnimation (false);

			targetCameraPosition.x -= benchWidth;

			for (float i=0f; i <= 1f; i += (2f * Time.deltaTime)) {
				myCamera.transform.position = Vector3.Lerp (myCamera.transform.position, targetCameraPosition, i);
				if (i >= 0.5f)
					i = 1f;
				yield return weof;
			}
			myCamera.transform.position = targetCameraPosition;
			currentCell--;

			if (currentCell == 0) { // if we're at the left plus:
				currentBenchPlayer = null;
				MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (false);
				LockerRoom_ActionsOld.instance.AddBtnAnimation (true);
				LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
				playerTapBtn.SetActive (false);
//				if (GameManager.BenchInPreselectedRunMode) {
//					LockerRoom_Actions.instance.StartRunBtnAnim (false);
//				} else {
//					LockerRoom_Actions.instance.RunBtnAnim (false);
//				}
				LockerRoom_ActionsOld.instance.RunBtnAnim (false);
				swiping = false;
				//LockerRoom_Actions.instance.ArrowsToggle (1);
				LockerRoom_ActionsOld.instance.LeftArrow (false);
				if(cellCount > 4) LockerRoom_ActionsOld.instance.RightArrow (true);
			} else {
				// update current player:
				foreach (GameObject curPlayer in playersOnBench) {
					if (curPlayer.GetComponent<Player> ().placeInBench == currentCell) {
						currentBenchPlayer = curPlayer;
						PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
					}
				}

				LockerRoom_ActionsOld.instance.AddBtnAnimation (false);

				swiping = false;

//				if (GameManager.BenchInPreselectedRunMode) {   
//					LockerRoom_Actions.instance.StartRunBtnAnim (true);
//				} else {
//					LockerRoom_Actions.instance.RunBtnAnim (true);
//				}
				//currentBenchPlayer.GetComponent<Player> ().UpdateFitness ();
				//CheckFitnessOfSelectedPlayer ();
			//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (true);
				MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);

				LockerRoom_ActionsOld.instance.RunBtnAnim (true);

				playerTapBtn.SetActive (true);

				if (Tutorial.IsNeedToShowHealPopup) {  //heal popup:
					if (currentBenchPlayer.GetComponent<Player> ().data.curHealthState == PlayerData.HealthState.WOUNDED) {
						MainScreenUiManager.instance.LowEnergyPopup (true);
						DAO.HealPopoup = 1;
					} else {
						MainScreenUiManager.instance.LowEnergyPopup (false);
					}
				}

				if (currentCell == 1) { // we're at the first player:
					//LockerRoom_Actions.instance.ArrowsToggle (1);
					LockerRoom_ActionsOld.instance.LeftArrow (false);
					if(cellCount > 4) LockerRoom_ActionsOld.instance.RightArrow (true);
				} else if (currentCell == cellCount - 1) { // we're at the last player:
					//LockerRoom_Actions.instance.ArrowsToggle (-1);
					if(cellCount > 4) LockerRoom_ActionsOld.instance.LeftArrow (true);
					LockerRoom_ActionsOld.instance.RightArrow (false);
				} else {
					//LockerRoom_Actions.instance.ArrowsToggle (2);
					if(cellCount > 4) LockerRoom_ActionsOld.instance.LeftArrow (true);
					if(cellCount > 4) LockerRoom_ActionsOld.instance.RightArrow (true);
				}

				if (!fullPlayers) {
					LockerRoom_ActionsOld.instance.GoToPlusToggle (true);
				} else {
					LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
				}
			}

		} else if (direction < 0 && (currentCell == 1 && !additionCell) || (currentCell == 0)) { // on swipe left and we're at the start...

			StartCoroutine (CameraCantMove (-1));

		}
		if (currentBenchPlayer != null)
			lastBenchPlayer = currentBenchPlayer;
			
			
	}

	IEnumerator CameraCantMove(int direction){ // camera animation when we're at the end or at the start:

		swiping = true;

		targetCameraPosition.x += 0.3f * direction;

		for (float i=0f; i <= 1f; i += (2f * Time.deltaTime)) {
			myCamera.transform.position = Vector3.Lerp (myCamera.transform.position, targetCameraPosition, i);
			if (i >= 0.5f)
				i = 1f;
			yield return weof;
		}
		for (float i=0f; i <= 1f; i += (2f * Time.deltaTime)) {
			myCamera.transform.position = Vector3.Lerp (myCamera.transform.position, currentCameraPosition, i);
			if (i >= 0.5f)
				i = 1f;
			yield return weof;
		}

		swiping = false;
	}


	void UpdateCurrentPlayer(){
		//update current player:
		MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);

		foreach (GameObject curPlayer in playersOnBench) {
			if (curPlayer.GetComponent<Player> ().placeInBench == currentCell) {
				currentBenchPlayer = curPlayer;
				PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
			}
		}

//		if (GameManager.BenchInPreselectedRunMode) {
//			LockerRoom_Actions.instance.StartRunBtnAnim (true);
//		} else {
//			LockerRoom_Actions.instance.RunBtnAnim (true);
//		}
		LockerRoom_ActionsOld.instance.RunBtnAnim (true);
		//currentBenchPlayer.GetComponent<Player> ().UpdateFitness ();
		//CheckFitnessOfSelectedPlayer ();

	//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (true);
		if (!fullPlayers) {
			LockerRoom_ActionsOld.instance.GoToPlusToggle (true);
		} else {
			LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
		}
		playerTapBtn.SetActive (true);

	//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (true);
		if (currentBenchPlayer != null)
			lastBenchPlayer = currentBenchPlayer;
	}

	//public void UpdateEnergyForGood

	public void CheckFitnessOfSelectedPlayer(){
		if (currentBenchPlayer != null) {
			currentBenchPlayer.GetComponent<Player> ().data.UpdateHealthCondition ();
			if (!playerIsStanding) {
				StartCoroutine (UpdateSitAnimation ());
			}
		}

	}



	IEnumerator UpdateSitAnimation(){
		yield return new WaitForFixedUpdate ();
		//currentBenchPlayer.GetComponent<Player> ().SitIdle ();
	}


	public bool IsSelectedPlayerCanRunCup(){
		if (GameManager.PreselectedGameState == GameManager.GameState.INFINITY_RUN || GameManager.PreselectedGameState == GameManager.GameState.TUTORIAL_RUN) return true;

		if (GameManager.ActiveCupCompleted) {
			return (currentBenchPlayer.GetComponent<Player> ().data.fitness >= GameManager.ActiveCompletedCup.cost);
		} else {
			return (currentBenchPlayer.GetComponent<Player> ().data.fitness >= GameManager.ActiveCup.cost);
		}
	}

	public int GetNumOfPurchasedPlayers(){
		return DAO.PurchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries).Length;
	}

	public void UpdatePlayerPrices(){
		CurrentPlayerPrice = DAO.GetPlayersPrice ( GetNumOfPurchasedPlayers() );

		foreach (PlayerId pl in PlayerCardsParent.GetComponentsInChildren<PlayerId>()) {
			//pl.SetPrice(CurrentPlayerPrice);
		}
	}

	int minCoinsAmountToThirdPlayer = 500;
	//int currentCoinsAmountToThirdPlayer;

	IEnumerator CameraStartAnimation(bool havePlayer){

//		targetCameraPosition = myCamera.transform.position;
//		targetCameraPosition.x = 0f;

		yield return new WaitForSeconds (0.5f);

		playerChooseScreen.SetActive (false);


		while (myCamera.transform.position.z < cameraOriginalPosition.position.z) {
			myCamera.transform.position = Vector3.MoveTowards (myCamera.transform.position, cameraOriginalPosition.position, 4f * Time.deltaTime);
			yield return weof;
		}


		myCamera.transform.position = cameraOriginalPosition.position;

		startAnimation = false;

		if (havePlayer && !isFreePlayer) {
			//currentBenchPlayer.GetComponent<Player> ().UpdateFitness ();
			//CheckFitnessOfSelectedPlayer ();
		//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (true);
			MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);
		
			LockerRoom_ActionsOld.instance.RunBtnAnim (true);

			if (cellCount - currentCell >= 4) {
				LockerRoom_ActionsOld.instance.RightArrow (true);
			}

			if (currentCell >= 4) {
				LockerRoom_ActionsOld.instance.LeftArrow (true);
			}
			
			playerTapBtn.SetActive (true);

			if (!fullPlayers) {
				LockerRoom_ActionsOld.instance.GoToPlusToggle (true);
				LockerRoom_ActionsOld.instance.recruitBtnAnim.gameObject.SetActive (true);
			} else {
				LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
				LockerRoom_ActionsOld.instance.recruitBtnAnim.gameObject.SetActive (false);
			}
			if (!GM_Input._.LowEnergy) {
				swiping = false;
			}
			CheckTutorialPopups ();

		} else if (isFreePlayer){
			if (currentCell == cellCount) { // if i'm on the right plus:
				//Debug.Log("alon_______ isFreePlayer = " + isFreePlayer);
				CurrentPlayerPrice = 0;
				BringPlayerToPreview ();
			}

		} else {
			LockerRoom_ActionsOld.instance.AddBtnAnimation (true);
			MainScreenUiManager.instance.TutorialPointerToggle (true);
			swiping = false;
			CupsManager.Instance.SetFirstCupAndRun ();
		}



		if (IsNeedToShowRateUsPopup && GameManager.Instance.needToShowRateUsPopup) {
			MainScreenUiManager.instance.ShowRateUsPopup ();
			GameManager.Instance.needToShowRateUsPopup = false;
		}
	}


	public void LeftRightArrows(int direction){
		
		if (swiping)
			return;

		if(playerIsStanding)
			playerSit ();

//		if(benchState == BenchState.PREVIEW){
//			Debug.Log ("preview mode");
//			swiping = false;

		swiping = true;
		if (direction < 0) {
			if (currentCell > 4) {
				StartCoroutine (FastScrollEffect (direction));
			} else {
				targetCameraPosition = myCamera.transform.position;
				targetCameraPosition.x = 0;
				currentCell = 1;
				StartCoroutine (MoveCameraToPoint());
			}
			LockerRoom_ActionsOld.instance.LeftArrow (false);
			if(cellCount > 4) LockerRoom_ActionsOld.instance.RightArrow (true);

		}else if (direction > 0) {
			if ((cellCount - currentCell) > 4) {
				StartCoroutine (FastScrollEffect (direction));
			} else {
				targetCameraPosition = myCamera.transform.position;
				targetCameraPosition.x = newCellPosition.x - benchWidth;
				currentCell = cellCount - 1;
				StartCoroutine (MoveCameraToPoint());
			}
			if(cellCount > 4) LockerRoom_ActionsOld.instance.LeftArrow (true);
			LockerRoom_ActionsOld.instance.RightArrow (false);

		}
		LockerRoom_ActionsOld.instance.AddBtnAnimation (false);
	//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (false);

	}

//	public void FocusOnPlayer(){
//		 PrefabManager.instanse.ChoosePlayer (currentBenchPlayer.GetComponent<Player> ().PlayerID);
//	}
		
	IEnumerator FastScrollEffect(int direction){
		motionBlurPlane.SetActive (true);
		if (direction > 0) {
			//motionPlaneAnim.SetFloat ("speed", 1);
			motionPlaneAnim.Play ("motion plane r");
			yield return new WaitForSeconds (0.3f);
			targetCameraPosition = myCamera.transform.position;
			if (!fullPlayers) {
				targetCameraPosition.x = newCellPosition.x - benchWidth;
			} else {
				targetCameraPosition.x = newCellPosition.x;
			}
			myCamera.transform.position = targetCameraPosition;
			currentCell = cellCount - 1;
			yield return new WaitForSeconds (0.4f);
			motionBlurPlane.SetActive (false);
			UpdateCurrentPlayer ();
			swiping = false;
		}else if (direction < 0) {
			//motionPlaneAnim.SetFloat ("speed", -1);
			motionPlaneAnim.Play ("motion plane l");
			yield return new WaitForSeconds (0.3f);
			targetCameraPosition = myCamera.transform.position;
			targetCameraPosition.x = 0;
			myCamera.transform.position = targetCameraPosition;
			currentCell = 1;
			yield return new WaitForSeconds (0.4f);
			motionBlurPlane.SetActive (false);
			UpdateCurrentPlayer ();
			swiping = false;
		}
	}

	IEnumerator MoveCameraToPoint(){
		for (float i=0f; i <= 1f; i += (1.5f * Time.deltaTime)) {
			myCamera.transform.position = Vector3.Lerp (myCamera.transform.position, targetCameraPosition, i);
			if (i >= 0.5f)
				i = 1f;
			yield return weof;
		}
		myCamera.transform.position = targetCameraPosition;
		motionBlurPlane.SetActive (false);
		UpdateCurrentPlayer ();
		swiping = false;
	}


	public void StartBackToPlus(){
		

		if (swiping || fullPlayers)
			return;

		swiping = true;
		MainScreenUiManager.instance.TutorialPointerToggle (false);
		playerTapBtn.SetActive (false);
		LockerRoom_ActionsOld.instance.AddBtnAnimation (false);

		if(playerIsStanding)
			playerSit ();

//		if (GameManager.BenchInPreselectedRunMode) {
//			LockerRoom_Actions.instance.StartRunBtnAnim (false);
//		} else {
//			LockerRoom_Actions.instance.RunBtnAnim (false);
//		}
		LockerRoom_ActionsOld.instance.RunBtnAnim (false);

		MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (false);
	//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (false);

		if (currentCell == cellCount) { // if i'm on the right plus:
			BringPlayerToPreview ();
		} else {
			if (cellCount < 5 || (cellCount - currentCell) < 4) {
				StartCoroutine (ScrollAllCells ());
			} else {
				motionBlurPlane.SetActive (true); 
				motionPlaneAnim.Play ("motion plane"); //starts the motion effect animation and methods
			}
		}
		MainScreenUiManager.instance.ArrowsToggle (-1);
		LockerRoom_ActionsOld.instance.LeftArrow (false);
		LockerRoom_ActionsOld.instance.RightArrow (false);

	}

	IEnumerator ScrollAllCells(){
		targetCameraPosition = myCamera.transform.position;
		targetCameraPosition.x = newCellPosition.x;

		for (float i=0f; i <= 1f; i += (1.5f * Time.deltaTime)) {
			myCamera.transform.position = Vector3.Lerp (myCamera.transform.position, targetCameraPosition, i);
			if (i >= 0.5f)
				i = 1f;
			yield return weof;
		}
		myCamera.transform.position = targetCameraPosition;
		BringPlayerToPreview ();
	}

	public void BackToPlusEffect(){
		targetCameraPosition = myCamera.transform.position;
		targetCameraPosition.x = newCellPosition.x;
		myCamera.transform.position = targetCameraPosition;
	}

	public void FinishBackToPlus(){
		motionBlurPlane.SetActive (false);
		BringPlayerToPreview ();
	}

	public GameObject previewCancelBtn;

	void BringPlayerToPreview(){
		currentCell = cellCount;
		currentBenchPlayer = null;

		benchState = BenchState.PREVIEW; // change the status for the swipe method...

//		GM_Input._.PlayerPreview = true;
		foreach (Transform playerCard in playersCardsContent) {  // create a list of avalable players cards
			playersToBuyCards.Add (playerCard.GetComponent<PlayerId>());
		}
		int index = playersToBuyCards [0].PlayerPrefabIndex;

		WatchPlayerInfo (index , false);

		if (Tutorial.IsNeedToShowSwipingHand) {
			MainScreenUiManager.instance.SwipingHand (true);
			LocalDataInterface.Instance.swipingHand++;
			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE( BI.FLOW_STEP.PRESS_BUY_PLAYER );
		}
		//Debug.Log ("alon___________ BringPlayerToPreview()");
	}
		

	public void TapOnPlayer(){
		//if (swiping || playingAnim) return;

		playingAnim = true;

		if (!playerIsStanding) {
			playerIsStanding = true;
			currentBenchPlayer.GetComponent<Player> ().standType = 1;
			//currentBenchPlayer.GetComponent<Player> ().StandUp ();
		} else {
			//currentBenchPlayer.GetComponent<Player> ().Sit(false);
		}
	}

	public void EnergyCallback()
	{
		Debug.Log("Ron_______________EnergyCallback start");
		GameObject updatePlayer = null;
		if (currentBenchPlayer != null)
			updatePlayer = currentBenchPlayer;
		else if (lastBenchPlayer != null)
			updatePlayer = lastBenchPlayer;
				
		if (updatePlayer != null && updatePlayer.GetComponent<Player> () != null) {
			Debug.Log("Ron_______________EnergyCallback good");
			var pl = updatePlayer.GetComponent<Player> ();
			//currentBenchPlayer.GetComponent<Player>().data.AddFitnes (1,true);
			PlayerDataUIScreen.Instance.energyToAdd = 1;
			pl.UpdateFitness ();
			PlayerDataUIScreen.Instance.UpdateProgressBar ();

			pl.OnFitnessUpdate += PlayerDataUIScreen.Instance.UpdateProgressBar;

			Bench.instance.CheckFitnessOfSelectedPlayer ();
		}
		else
			Debug.Log("Ron_______________EnergyCallback Error");
		//PlayerDataUIScreen.Instance.SetViewWithCurrentPlayer ();
	}
	public void playerSit(){
		//playerIsStanding = false;
		//if(currentBenchPlayer != null) currentBenchPlayer.GetComponent<Player> ().Sit(false);
	}

	bool perkBought = false;

	public void OnPerkBuyClicked(Perk perk){
		
		// Get Current Perk Prize
		Player pl = currentBenchPlayer.GetComponent<Player> ();
		int numOfPurchasedPlayer = pl.data.GetNumOfPurchasedPerks ();
		int perkPrize = 0;

		if (numOfPurchasedPlayer == 1) {
			perkPrize = DAO.Settings.Perk2Cost;
		} else {
			perkPrize = DAO.Settings.Perk3Cost;
		}


		// Check If There is enouth money
		if (DAO.TotalCoinsCollected >= perkPrize) {
			swiping = true;
			DAO.TotalCoinsCollected -= perkPrize;
			CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

			AppsFlyerManager._.ReportVirtualEconomy ("Skill", perk.type.ToString (), perkPrize.ToString(), "1");
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Withdraw (perkPrize, "Skill Bought", perk.type.ToString (), "", "", "");
			GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, perkPrize, GameAnalyticsWrapper.BUY_UPGREADE, "SkillBought");


			if (numOfPurchasedPlayer == 1) {
				pl.data.Perk_2 = perk;
				NativeSocial._.ReportAchievment (SocialConstants.achievement_in_shape);
			} else {
				pl.data.Perk_3 = perk;
			}

			PerksScrn_Actions.Instance.OnBeforeShow();

			pl.Save();
			DAO.Instance.LOCAL.SaveAll();

			PlayerDataUIScreen.Instance.SetViewWithCurrentPlayer();

			if (perk.type == Perk.PerkType.MANAGER || perk.type == Perk.PerkType.GAMBLER) {
				TimeEventsManager.Instance.SetPerksBonusTime();
			}

			perkBought = true;

			AudioManager.Instance.OnLevelUp ();

			UnitedAnalytics.LogEvent ("Perk Upgraded or Added", "added", perk.type.ToString (), perk.level);

		} else {
			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
			IAP.AdditionalIAPCallback += OnIAPMadeAfterPerkClicked;
		}
	}

	public void OnPerkUpgradeClicked(Perk perk){
		
		// Get Current Perk Prize
		Player pl = currentBenchPlayer.GetComponent<Player> ();
		int perkPrize = 0;
		
		if (perk.level == 1) {
			perkPrize = DAO.Settings.PerkUpgrade2Cost;
		} else {
			perkPrize = DAO.Settings.PerkUpgrade3Cost;
		}


		// Check If There is enouth money
		if (DAO.TotalCoinsCollected >= perkPrize) {
			swiping = true;
			DAO.TotalCoinsCollected -= perkPrize;
			CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Withdraw (perkPrize, "Skill Upgraded", perk.type.ToString (), "", "", "");
			GameAnalyticsWrapper.ResourceEvent (false, GameAnalyticsWrapper.CURR_COINS, perkPrize, GameAnalyticsWrapper.BUY_UPGREADE, "SkillBought");

			pl.data.LevelUpPerk( perk.type );
			
			PerksScrn_Actions.Instance.OnBeforeShow();

			pl.Save();
			DAO.Instance.LOCAL.SaveAll();

			perkBought = true;

			AudioManager.Instance.OnLevelUp ();

		} else {
			CrossSceneUIHandler.Instance.showStore(CrossSceneUIHandler.ShopStates.COINS);
			IAP.AdditionalIAPCallback += OnIAPMadeAfterPerkClicked;
		}
	}

	void OnIAPMadeAfterPerkClicked(string sku){
		IAP.AdditionalIAPCallback -= OnIAPMadeAfterPerkClicked;
		UnitedAnalytics.LogEvent ("IAP from Perks", sku, UserData.Instance.userType.ToString (), DAO.NumOfPurchasedPlayers);
	}


	public void PerkGlowFX(){
		if (perkBought) {
			
			LockerRoom_ActionsOld.instance.MainMenuBtnAnim ();
			LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
			MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (false);
		//	MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (false);
			LockerRoom_ActionsOld.instance.RunBtnAnim (false);
		
			perkBought = false;
			playerTapBtn.SetActive (false);
			StartCoroutine (PerkGlowFxCoroutine ());
		} else {
			swiping = false;
		}
	}

	public Color sparksColor;
	public Transform sparkTransform1;
	public Transform sparkTransform2;
	public Transform sparkTransform3;
	public Transform sparkTransform4;
	public Transform sparkTransform5;
	WaitForSeconds waitPerkSparksFX = new WaitForSeconds(0.25f);

	IEnumerator PerkGlowFxCoroutine(){
		currentBenchPlayer.GetComponent<Player> ().standType = 4;
		//currentBenchPlayer.GetComponent<Player> ().StandUp ();
		yield return new WaitForSeconds (1);
		starGlowParent.SetActive (false);
		starGlowParent.transform.position = currentBenchPlayer.transform.position;
		starGlowParent.SetActive (true);
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform1.position , sparksColor , 50 , 4 , 4 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform2.position , sparksColor , 50 , 4 , 4 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform3.position , sparksColor , 50 , 4 , 4 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform4.position , sparksColor , 50 , 4 , 4 , false);
		yield return waitPerkSparksFX;
		CrossSceneUIHandler.Instance.SparklesFX (1f, sparkTransform5.position , sparksColor , 50 , 4 , 4 , false);

		yield return new WaitForSeconds (1);
		LockerRoom_ActionsOld.instance.MainMenuBtnAnim ();
		LockerRoom_ActionsOld.instance.GoToPlusToggle (true);
		MainScreenUiManager.instance.PreviewPlayerCardAnimToggle (true);
		//MainScreenUiManager.instance.PreviewPlayerCardSwapAnim (true);
		LockerRoom_ActionsOld.instance.RunBtnAnim (true);
		swiping = false;
	}


	public void OnPlayerBuyGeneral1(){

		Player plr = newPlayer.GetComponent<Player> ();

		//DAO.PurchasedPlayers += newPlayer.GetComponent<Player> ().PlayerID + "|";
		if (plr.DefaultPerk == Perk.PerkType.MANAGER || newPlayer.GetComponent<Player> ().DefaultPerk == Perk.PerkType.GAMBLER) {
			TimeEventsManager.Instance.SetPerksBonusTime ();
		}


		if (plr.data.PlayerID == 12) { // Puyol
			plr.data.Perk_1.level = 3;
			plr.data.Perk_2 = new Perk (Perk.PerkType.EXELLENT_FITNESS.ToString(), "3");
			plr.data.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString(), "3");
			plr.Save ();
		}

		if (plr.data.PlayerID == 19) { // Ronaldinho
			plr.data.Perk_1.level = 3;
			plr.data.Perk_2 = new Perk (Perk.PerkType.JUMPER.ToString(), "3");
			plr.data.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString(), "3");
			plr.Save ();
		}

		if (plr.data.PlayerID == 14) { // Pep Guardiola
			plr.data.Perk_1.level = 3;
			plr.data.Perk_2 = new Perk (Perk.PerkType.MAGNETIC.ToString(), "3");
			plr.data.Perk_3 = new Perk (Perk.PerkType.EXELLENT_FITNESS.ToString(), "3");
			plr.Save ();
		}

		if (plr.data.PlayerID == 17) { // Koeman
			plr.data.Perk_1.level = 3;
			plr.data.Perk_2 = new Perk (Perk.PerkType.DODGER.ToString(), "3");
			plr.data.Perk_3 = new Perk (Perk.PerkType.MAGNETIC.ToString(), "3");
			plr.Save ();
		}

		if (plr.data.PlayerID == 20) { // Rivaldo
			plr.data.Perk_1.level = 3;
			plr.data.Perk_2 = new Perk (Perk.PerkType.GAMBLER.ToString(), "3");
			plr.data.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString(), "3");
			plr.Save ();
		}

		if (plr.data.PlayerID == 15) { // Stoichkov
			plr.data.Perk_1.level = 3;
			plr.data.Perk_2 = new Perk (Perk.PerkType.EXELLENT_FITNESS.ToString(), "3");
			plr.data.Perk_3 = new Perk (Perk.PerkType.FANS_FAVORITE.ToString(), "3");
			plr.Save ();
		}

		//Debug.Log ("alon_________ PurchasedPlayers list before buying: " + pp);
		if (!DAO.Instance.IsPlayerAllReadyPurchased(plr.PlayerID.ToString())) {
			//Debug.Log ("alon_________ player is not exist - adding to PurchasedPlayers");
			DAO.PurchasedPlayers += newPlayer.GetComponent<Player> ().PlayerID + "|";
			//Debug.Log ("alon_________ PurchasedPlayers list after buying: " + pp);
		}

//		if (DAO.PurchasedPlayers.IndexOf (newPlayer.GetComponent<Player> ().PlayerID + "|") < 0) {
//			//DAO.PurchasedPlayers += id+"|";
//			DAO.PurchasedPlayers += newPlayer.GetComponent<Player> ().PlayerID + "|";
//		}

		DAO.Instance.LOCAL.SaveAll ();
			

		cell.transform.FindChild ("shirt01").gameObject.SetActive (false);

//		if (newPlayer.GetComponent<Player> ().playerStructure.isLedgend == true) {
//			cell.GetComponent<CellOrginaizer> ().ledgentStarsFX.SetActive (true);
//		}

		if (cellCount < maxSits) {
			newPlayerPosition.x += benchWidth;
			cellCount++;

			//create a new random cell:
			newCellPosition.x += benchWidth;
			//cellPrefabRandomNum = Random.Range (0, cellPrefabs.Length);
			cell = (GameObject)Instantiate (cellPrefabs [0], transform.position, transform.rotation);
			cell.transform.parent = transform;
			cell.transform.position = newCellPosition;
			cell.GetComponent<CellOrginaizer> ().cellNumber = cellCount;
			if (cellCount > 2 && !additionCell) { // create additional plus cell:
				//cellPrefabRandomNum = Random.Range (0, cellPrefabs.Length);
				cellExtra = (GameObject)Instantiate (cellPrefabs [0], transform.position, transform.rotation);
				cellExtra.transform.parent = transform;
				cellExtra.transform.position = additionPlusCellPosition;
				additionCell = true;
			}
		} else {
			fullPlayers = true;
			LockerRoom_ActionsOld.instance.recruitBtnAnim.gameObject.SetActive (false);
			LockerRoom_ActionsOld.instance.AddBtnAnimation (false);
			LockerRoom_ActionsOld.instance.GoToPlusToggle (false);
			cellExtra.SetActive(false);
			additionCell = false;
		}
			
	}


	public void OnPlayerBuyGeneral2(){

		benchState = BenchState.BENCH;
//		GM_Input._.PlayerPreview = false;
		LockerRoom_ActionsOld.instance.PlayerToBuyCardAnimToggle (false);
		if (cellCount > 4)		LockerRoom_ActionsOld.instance.LeftArrow (true);

		CupsManager.Instance.ResetCupsAvailability ();
		UpdatePlayerPrices ();
		LockerRoom_ActionsOld.instance.TutorialRecruitBounce (false);

		//swiping = false;
		if (playersOnBench.Count >= 2) {
			DAO.BuySecondPlayerPopup1 = 1;
			DAO.BuySecondPlayerPopup2 = 1;
		} else if (playersOnBench.Count >= 3) {
			DAO.BuyThirdPlayerPopup = 1;
		}

		playerChooseScreen.SetActive (true);
	PlayerChooseScrn_Actions_old.instance.RemovePlayerId (currentBenchPlayer.GetComponent<Player> ().PlayerID);
		playerChooseScreen.SetActive (false);

		playersToBuyCards.Clear ();

		UnitedAnalytics.LogEvent ("Player bought " + playersOnBench.Count.ToString (), "buy", UserData.Instance.userType.ToString (), DAO.PlayTimeInMinutes);

		DAO.Instance.LOCAL.SaveAll ();

		if (playersOnBench.Count == 1) {
			playerIsStanding = true;
			StartCoroutine(CupsManager.Instance.StartFirstRun ());
		}
	}



	void CheckTutorialPopups(){

		if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE ( BI.FLOW_STEP.LOCKER_ROOM_ENTERED );

		//if (LockerRoom_Actions.instance.runScrnOn || LockerRoom_Actions.instance.cupScrnOn || LockerRoom_Actions.instance.winScrnOn)	return;
		if (tutPopupsDisabled)	return;

		if (Tutorial.IsNeedToShowHealPopup && currentBenchPlayer.GetComponent<Player> ().data.fitness <= 1) { //heal popup:
			MainScreenUiManager.instance.LowEnergyPopup (true);
			DAO.HealPopoup = 1;
		}
//		else if (LockerRoom_Actions.instance.healPopupOn) {
//			LockerRoom_Actions.instance.HealPopup (false); // close heal popup when switch player:
//		}
		else if (Tutorial.IsNeedToShowBuySecondPlayer1Popup && DAO.TotalCoinsCollected >= CurrentPlayerPrice) { //buy second player popup 1:
			MainScreenUiManager.instance.RecruitSecondPopup (true); // recruit
			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE ( BI.FLOW_STEP.ELIGIBLE_FOR_2ND_PLAYER );
		}
		else if (Tutorial.IsNeedToShowBuySecondPlayer2Popup && DAO.TotalCoinsCollected >= CurrentPlayerPrice * 1.25f) { //buy second player popup 2:
			MainScreenUiManager.instance.RecruitSecondPopup (true);
		}
		else if (playersOnBench.Count == 2 && Tutorial.IsNeedToShowBuyThirdPlayerPopup && DAO.TotalCoinsCollected < CurrentPlayerPrice && DAO.TotalCoinsCollected + minCoinsAmountToThirdPlayer >= CurrentPlayerPrice) { //buy third player popup:
			MainScreenUiManager.instance.RecruitThirdPopup (true , CurrentPlayerPrice - DAO.TotalCoinsCollected); // recruit another
		}
		else if(Tutorial.IsNeedToShowPlayPracticePopup){
			MainScreenUiManager.instance.PracticePopup (true);
		}
		else if(Tutorial.IsNeedToShowPlayCupPopup && !GameManager.BenchInPreselectedRunMode){
			MainScreenUiManager.instance.CupPopup (true);
		}
//		else if(Tutorial.IsNeedToShowSpecialOfferPopup && IAP.IsReady){
//			LockerRoom_Actions.instance.OfferPopup (true);
//		}


	}


	public static bool IsNeedToShowRateUsPopup{   
		get{ 
			return (DAO.RateUsPopup == 0 && (DateTime.Now - DAO.RateUsPopupDate).TotalMinutes >= DAO.RateUsTimeFactor);
			//return true;
		}
	}
		


	public void CheckCoinsCollected(){
		if (DAO.TotalCoinsCollected > 1000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_need_bigger_bag);
		}

		if (DAO.TotalCoinsCollected > 5000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_money_in_the_bank);
		}

		if (DAO.TotalCoinsCollected > 10000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_r_for_rich);
		}

		if (DAO.TotalCoinsCollected > 15000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_m_for_millioner);
		}

		if (DAO.TotalCoinsCollected > 25000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_fcbillion);
		}

		if (DAO.TotalCoinsCollected > 50000) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_rollin_in_the_deep);
		}
	}



}
