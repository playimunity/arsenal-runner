﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class cr_handler : MonoBehaviour {

	public static event Action GreenScreenFadedOut;

	public static bool IsDebug;

	public Color ColorBlack;
	public Color ColorGreen;

	public Image BG;
	public Text txt1;
	public Text txt2;
	public Text CounterTXT;
	public InputField Pass;

	public GameObject StagingBtn;
	public GameObject LiveBtn;

	string t1;
	string t2;

	bool NeedToUpdateT1 = true;
	WaitForEndOfFrame weof;
	WaitForSeconds wfs;
	int frame = 0;
	int taps = 0;
	bool PreventTap = false;
	int iPassTry = 0;

	void Awake(){
		weof = new WaitForEndOfFrame ();
		wfs = new WaitForSeconds (0.04f);
	}

	void OnEnable(){
		txt1.gameObject.SetActive (true);
		BG.color = ColorGreen;
		CounterTXT.text = "";
		StartCoroutine (FadeOutGreenScreen (0.05f));
		StartCoroutine ("updateT1");

		StagingBtn.SetActive (IsDebug);
		LiveBtn.SetActive (IsDebug);

	}
		
	public void OnTaped(){
		if (PreventTap) return;

		if (IsDebug) {
			CloseImidiately ();
			return;
		}

		if (taps > 7) {
			PreventTap = true;
			taps = 0;
			StopCoroutine ("CloseConsole");
			TransiteToPhase2 ();
			CounterTXT.text = "";
			return;
		}

		if (taps >= 4) {
			CounterTXT.text = (8 - taps) + "";
		}

		taps++;

		StopCoroutine ("CloseConsole");
		StartCoroutine ("CloseConsole");
	}

	public void OnStaginClicked(){
		if ((Pass.text.Trim () == "yeled0") && (iPassTry < 3) && (DAO.Instance.settings.debugStageOn == 1))
		{
			DAO.IsInDebugMode = true;
			DAO.Instance.LOCAL.SaveAll ();
			Ads._.GetCountryCode ();
			AnimationEvents.OnSceneTransitionShownEvent += OnSceneTransitionShown;
			SceneTransitionContainer.Instance.showTransition ();
		} 
		else 
		{
			iPassTry++;
		}

	}

	public void OnLiveClicked(){
		DAO.IsInDebugMode = false;
		DAO.Instance.LOCAL.SaveAll ();

		AnimationEvents.OnSceneTransitionShownEvent += OnSceneTransitionShown;
		SceneTransitionContainer.Instance.showTransition ();
	}

	void OnSceneTransitionShown(){
		AnimationEvents.OnSceneTransitionShownEvent -= OnSceneTransitionShown;
		DAO.Instance.SERVER.GetData ();
		CloseImidiately ();
	}

	IEnumerator CloseConsole(){
		yield return new WaitForSeconds (0.5f);
		taps = 0;
		frame = 0;
		NeedToUpdateT1 = true;
		txt2.text = "";
		t1 = "";
		t2 = "";
		PreventTap = false;
		CounterTXT.text = "";
		CrossSceneUIHandler.Instance.ConsoleHide ();
	}

	void CloseImidiately(){
		taps = 0;
		frame = 0;
		NeedToUpdateT1 = true;
		txt2.text = "";
		t1 = "";
		t2 = "";
		PreventTap = false;
		CounterTXT.text = "";
		CrossSceneUIHandler.Instance.ConsoleHide ();
	}


	IEnumerator updateT1(){
		t1 = "";

		for (int y = 0; y <= cr_constants.NUM_OF_LINES; y++) {
			
			for (int x = 0; x <= cr_constants.NUM_OF_CHARS; x++) {

				if( x!=0 && x%6 == 0) t1 += "  ";

				t1 += UnityEngine.Random.Range(0,2) + "";
			}

			t1 += "\n";
		}

		txt1.text = t1;

		yield return wfs;
		if(NeedToUpdateT1) StartCoroutine ("updateT1");
	}

	IEnumerator FadeOutGreenScreen(float speed){
		for (float i = 0f; i < 1f; i += speed) {
			BG.color = Color.Lerp (ColorGreen, ColorBlack, i);
			yield return weof;
		}

		BG.color = ColorBlack;
		if (GreenScreenFadedOut != null) GreenScreenFadedOut ();
	}

	void TransiteToPhase2(){
		BG.color = ColorGreen;
		NeedToUpdateT1 = false;
		txt1.gameObject.SetActive (false);
		GreenScreenFadedOut += StartPhase2;
		StartCoroutine (FadeOutGreenScreen (0.025f));
	}

	void StartPhase2(){
		GreenScreenFadedOut -= StartPhase2;
		frame = 1;
		StartCoroutine ( ProcessPhase2_Scene1() );
	}







	IEnumerator ProcessPhase2_Scene1(){
		t2 = "";

		for(int i=0; i<frame; i++){

			if( cr_constants.FINAL[i].Length > 1){

				for(var c=0; c<cr_constants.FINAL[i].Length; c++){
					t2 += cr_constants.RandomChar;
				}

			}else{
				t2 += cr_constants.RandomChar;
			}
			t2 += "\n";


		}

		txt2.text = t2;

		frame++;

		yield return wfs;

		if (frame < cr_constants.FINAL.Length) {
			StartCoroutine (ProcessPhase2_Scene1 ());
		}else {
			frame = 1;
			StartCoroutine (ProcessPhase2_Scene2 ());
		}

	}

	IEnumerator ProcessPhase2_Scene2(){
		t2 = "";

		for(int i=0; i<cr_constants.FINAL.Length; i++){

			if( i < frame ){
				t2 += cr_constants.FINAL[i];
			}else{
				if( cr_constants.FINAL[i].Length > 1){

					for(int c=0; c< cr_constants.FINAL[i].Length ; c++){
						t2 += cr_constants.RandomChar;
					}

				}else{
					t2 += cr_constants.RandomChar;
				}
			}
			t2 += "\n";
		}

		txt2.text = t2;
		frame++;

		yield return wfs;

		if (frame < cr_constants.FINAL.Length) {
			StartCoroutine (ProcessPhase2_Scene2 ());
		}

		PreventTap = false;
	}
}










//t2 = "";
//
//foreach (string str in cr_constants.FINAL) {
//	t2 += str;
//	t2 += "\n";
//}
//
//txt2.text = t2;
//
//yield return weof;