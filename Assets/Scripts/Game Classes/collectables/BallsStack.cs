﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallsStack : Collectable {

	bool bSCollided = false;
	public Rigidbody ballDropper;
	public Transform balls;
	Rigidbody[] bodies; 

	void Start () {
		type = CollectableTypes.BALL_STACK;
		bodies = balls.GetComponentsInChildren<Rigidbody> ();
	}


	void OnTriggerEnter(Collider col){
		if (!bSCollided && col.gameObject.tag == "Player") {
			bSCollided = true;

			//GetComponent<BoxCollider>().enabled = false;

			foreach(Rigidbody body in bodies){
				body.isKinematic = false;
			}
			ballDropper.velocity  = transform.forward * 600f * Time.deltaTime;
			//StartCoroutine(StopBallDropper ());

			if (PlayerController.instance.sliding) {
				PlayerController.instance.CollectBall ();
			} else if (!PlayerController.instance.speedBoostOn) {
					ParticleCloudeGenerator.instance.ParticleWhistle (transform.position, transform.eulerAngles);
					PlayerController.instance.Damage (1);
					AudioManager.Instance.OnSoftHit ();
			}
			Destroy (ballDropper, 0.3f);
		}

	}




}
