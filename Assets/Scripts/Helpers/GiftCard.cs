﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class GiftCard : MonoBehaviour {

	public bool IsSystemGenerated = false;

	public enum Prizes
	{
		COINS_S, COINS_M, COINS_L, COINS_XL,
		MAGNET_S, MAGNET_M, ENERGY_DRINK,
		SPEED_S, X2_S , SPEED_M,
		SHIELD_S, PLAYER, JACKPOT,
		SHIELD_M
	}

	public Prizes SelectedPrize;
	bool builded=false;
	public bool drawing = false;

	public RectTransform container;

	public RawImage img;
	public RawImage border;
	public Color StrokeColor;

	//public Image prizeImg;

	Texture2D texture;
	Texture2D borderTexture;

	int CIRCLE_RADIUS = 28;
	int randomImg;

	int I_WIDTH = 300;//256;//512;//754;//256;//128;//160;
	int I_HEIGHT = 480;//404;//820;//1220;//404;//202;//252;

	Color pixelColor;

	Color[] pxls;
	Color[] newPxls;
	int _t, _r, _b, _l;

	int RandomNum;

	public GameObject PrizesContainer;

	public GameObject Prize_CoinsS;
	public GameObject Prize_CoinsM;
	public GameObject Prize_CoinsL;
	public GameObject Prize_CoinsXL;
	public GameObject Prize_MagnetS;
	public GameObject Prize_MagnetM;
	public GameObject Prize_EnergyDring;
	public GameObject Prize_SpeedS;
	public GameObject Prize_X2S;
	public GameObject Prize_SpeedM;
	public GameObject Prize_Shield;
	public GameObject Prize_ShieldM;
	public GameObject Prize_Player;
	public GameObject Prize_Jackpot;

	List<GameObject> SelectedPrizesICons;
	GameObject TGO;
	List<GiftCardPrizeIcon> PrizesIconsWinners;
	GiftCardPrizeIcon tmpGCPI;
	 

	void Start () {

		texture = new Texture2D (I_WIDTH, I_HEIGHT);
		img.texture = texture;

		borderTexture = new Texture2D (I_WIDTH, I_HEIGHT);
		border.texture = borderTexture;
		
		for (int y = 0; y < texture.height; y++) {
			for (int x = 0; x < texture.width; x++) {
				texture.SetPixel(x,y, Color.clear);
				borderTexture.SetPixel(x,y, Color.clear);
			}
		}
		texture.Apply (); 
		borderTexture.Apply ();
	
	}
		


	public void Build(){
		SelectedPrize = Prizes.COINS_S;

		if (IsSystemGenerated) {
			RandomizePrizeGivenBySystem ();
		} else {
			RandomizePrizeGivenByPlayer ();
		}
	}


	void RandomizePrizeGivenByPlayer(){

		RandomNum = Random.Range (1, 100);

		if (RandomNum < 15) 	SelectedPrize = Prizes.COINS_S;
		else if(RandomNum < 25) SelectedPrize = Prizes.COINS_M;
		else if(RandomNum < 30) SelectedPrize = Prizes.COINS_L;
		else if(RandomNum < 40) SelectedPrize = Prizes.MAGNET_S;
		else if(RandomNum < 50) SelectedPrize = Prizes.MAGNET_M;
		else if(RandomNum < 55) SelectedPrize = Prizes.ENERGY_DRINK;
		else if(RandomNum < 65) SelectedPrize = Prizes.SPEED_S;
		else if(RandomNum < 80) SelectedPrize = Prizes.X2_S;
		else if(RandomNum < 85) SelectedPrize = Prizes.SPEED_M;
		else if(RandomNum < 90) SelectedPrize = Prizes.JACKPOT;
		else if(RandomNum < 95) SelectedPrize = Prizes.SHIELD_S;
		else 					SelectedPrize = Prizes.SHIELD_M;

		BuildCard(SelectedPrize);

	}

	void RandomizePrizeGivenBySystem(){

		RandomNum = Random.Range (1, 100);

		if (RandomNum < 5) 	SelectedPrize = Prizes.COINS_S;
		else if(RandomNum < 15) SelectedPrize = Prizes.COINS_M;
		else if(RandomNum < 25) SelectedPrize = Prizes.COINS_L;
		else if(RandomNum < 30) SelectedPrize = Prizes.MAGNET_S;
		else if(RandomNum < 45) SelectedPrize = Prizes.MAGNET_M;
		else if(RandomNum < 55) SelectedPrize = Prizes.ENERGY_DRINK;
		else if(RandomNum < 60) SelectedPrize = Prizes.SPEED_S;
		else if(RandomNum < 75) SelectedPrize = Prizes.X2_S;
		else if(RandomNum < 85) SelectedPrize = Prizes.SPEED_M;
		else if(RandomNum < 90) SelectedPrize = Prizes.JACKPOT;
		else if(RandomNum < 95) SelectedPrize = Prizes.SHIELD_S;
		else 					SelectedPrize = Prizes.SHIELD_M;

		BuildCard(SelectedPrize);

	}


	GameObject tempLoseIcon1;
	GameObject tempLoseIcon2;
	GameObject tempLoseIcon3;

	void BuildCard(GiftCard.Prizes sp){
		if (builded) return;

		builded = true;

		// BUILD PRIZES ICONS ARRAY

		SelectedPrizesICons = new List<GameObject> ();



		switch (sp) {
			case Prizes.COINS_S:{
				SelectedPrizesICons.Add (Prize_CoinsS);
				SelectedPrizesICons.Add (Prize_CoinsS);
				SelectedPrizesICons.Add (Prize_CoinsS);

				AddGOToClaimScreen (Prize_CoinsS);

				break;
			}
			case Prizes.COINS_M:{
				SelectedPrizesICons.Add (Prize_CoinsM);
				SelectedPrizesICons.Add (Prize_CoinsM);
				SelectedPrizesICons.Add (Prize_CoinsM);

				AddGOToClaimScreen (Prize_CoinsM);
				break;
			}
			case Prizes.COINS_L:{
				SelectedPrizesICons.Add (Prize_CoinsL);
				SelectedPrizesICons.Add (Prize_CoinsL);
				SelectedPrizesICons.Add (Prize_CoinsL);

				AddGOToClaimScreen (Prize_CoinsL);
				break;
			}
			case Prizes.COINS_XL:{
				SelectedPrizesICons.Add (Prize_CoinsXL);
				SelectedPrizesICons.Add (Prize_CoinsXL);
				SelectedPrizesICons.Add (Prize_CoinsXL);

				AddGOToClaimScreen (Prize_CoinsXL);
				break;
			}
			case Prizes.ENERGY_DRINK:{
				SelectedPrizesICons.Add (Prize_EnergyDring);
				SelectedPrizesICons.Add (Prize_EnergyDring);
				SelectedPrizesICons.Add (Prize_EnergyDring);

				AddGOToClaimScreen (Prize_EnergyDring);
				break;
			}
			case Prizes.MAGNET_M:{
				SelectedPrizesICons.Add (Prize_MagnetM);
				SelectedPrizesICons.Add (Prize_MagnetM);
				SelectedPrizesICons.Add (Prize_MagnetM);

				AddGOToClaimScreen (Prize_MagnetM);
				break;
			}
			case Prizes.MAGNET_S:{
				SelectedPrizesICons.Add (Prize_MagnetS);
				SelectedPrizesICons.Add (Prize_MagnetS);
				SelectedPrizesICons.Add (Prize_MagnetS);

				AddGOToClaimScreen (Prize_MagnetS);
				break;
			}
		case Prizes.SPEED_S:{
				SelectedPrizesICons.Add (Prize_SpeedS);
				SelectedPrizesICons.Add (Prize_SpeedS);
				SelectedPrizesICons.Add (Prize_SpeedS);

				AddGOToClaimScreen (Prize_SpeedS);
				break;
			}
			case Prizes.X2_S:{
				SelectedPrizesICons.Add (Prize_X2S);
				SelectedPrizesICons.Add (Prize_X2S);
				SelectedPrizesICons.Add (Prize_X2S);

				AddGOToClaimScreen (Prize_X2S);
				break;
			}
			case Prizes.SPEED_M:{
				SelectedPrizesICons.Add (Prize_SpeedM);
				SelectedPrizesICons.Add (Prize_SpeedM);
				SelectedPrizesICons.Add (Prize_SpeedM);

				AddGOToClaimScreen (Prize_SpeedM);
				break;
			}
			case Prizes.SHIELD_S:{
				SelectedPrizesICons.Add (Prize_Shield);
				SelectedPrizesICons.Add (Prize_Shield);
				SelectedPrizesICons.Add (Prize_Shield);

				AddGOToClaimScreen (Prize_Shield);
				break;
			}
		case Prizes.SHIELD_M:{
				SelectedPrizesICons.Add (Prize_Shield);
				SelectedPrizesICons.Add (Prize_Shield);
				SelectedPrizesICons.Add (Prize_Shield);

				AddGOToClaimScreen (Prize_Shield);
				break;
			}
		case Prizes.JACKPOT:{
				SelectedPrizesICons.Add (Prize_Jackpot);
				SelectedPrizesICons.Add (Prize_Jackpot);
				SelectedPrizesICons.Add (Prize_Jackpot);

				AddGOToClaimScreen (Prize_Jackpot);
				break;
			}
		case Prizes.PLAYER:{
				SelectedPrizesICons.Add (Prize_Player);
				SelectedPrizesICons.Add (Prize_Player);
				SelectedPrizesICons.Add (Prize_Player);

				AddGOToClaimScreen (Prize_Player);
				break;
			}

		}

		tempLoseIcon1 = GetRandomPrizeIconExcepsSelected ();
		tempLoseIcon2 = GetRandomPrizeIconExcepsSelected ();
		tempLoseIcon3 = GetRandomPrizeIconExcepsSelected ();

		if (tempLoseIcon3 == tempLoseIcon2) {
			while (tempLoseIcon3 == tempLoseIcon2) {
				tempLoseIcon3 = GetRandomPrizeIconExcepsSelected ();
			}
		}

		SelectedPrizesICons.Add (tempLoseIcon1);
		SelectedPrizesICons.Add (tempLoseIcon2);
		SelectedPrizesICons.Add (tempLoseIcon3);


		// INSTANTIATE ICONS
		PrizesIconsWinners = new List<GiftCardPrizeIcon>();
		foreach(GameObject icon in SelectedPrizesICons.Randomize ()){

			tmpGCPI = null;
			TGO = null;

			TGO = (GameObject)Instantiate (icon , transform.position , transform.rotation);
			TGO.transform.SetParent (PrizesContainer.transform , false);

			tmpGCPI = TGO.GetComponent<GiftCardPrizeIcon> ();

			if (tmpGCPI.PrizeType == sp) {
				tmpGCPI.Winner = true;
				PrizesIconsWinners.Add (tmpGCPI);
			} else {
				tmpGCPI.Winner = false;
			}


		}


	}

	void AddGOToClaimScreen(GameObject obj){
		return;

//		foreach(Transform child in CardsScrn_Actions.instance.GetClaimScreenIconContainer()) {
//			Destroy(child.gameObject);
//		}
//
//		TGO = (GameObject)Instantiate (obj , Vector3.zero , Quaternion.identity);
//		TGO.transform.SetParent (CardsScrn_Actions.instance.GetClaimScreenIconContainer() , false);
	}

	GameObject GetRandomPrizeIconExcepsSelected(){
	
		RandomNum = Random.Range (0, 12);

		while( RandomNum == (int)SelectedPrize ){
			RandomNum = Random.Range (0, 12);
		}

		if (RandomNum == 0) return Prize_CoinsS;
		if (RandomNum == 1) return Prize_CoinsM;
		if (RandomNum == 2) return Prize_CoinsL;
		if (RandomNum == 3) return Prize_CoinsXL;
		if (RandomNum == 4) return Prize_MagnetS;
		if (RandomNum == 5) return Prize_MagnetM;
		if (RandomNum == 6) return Prize_EnergyDring;
		if (RandomNum == 7) return Prize_SpeedS;
		if (RandomNum == 8) return Prize_X2S;
		if (RandomNum == 9) return Prize_SpeedM;
		if (RandomNum == 10) return Prize_Shield;
		if (RandomNum == 11) return Prize_Player;
		if (RandomNum == 12) return Prize_Jackpot;


		return Prize_Player;
	}
	
//	void OnEnable() {
//		//InvokeRepeating ("CheckScratch", 1, 1);
//	}
	
//	void OnDisable() {
//		//CancelInvoke("CheckScratch");
//	}


	public void Draw(){
		if (!CardsScrn_Actions.instance.isDrawAllowed)	return;

		drawing = true;
		//Debug.Log ("alon___________ Tutorial.IsNeedToShowCardsHandTut: " + Tutorial.IsNeedToShowCardsHandTut);
		if (Tutorial.IsNeedToShowCardsHandTut) {
			CardsScrn_Actions.instance.HideCardsHandTut ();
		}

		Vector2 lp;
		bool res = RectTransformUtility.ScreenPointToLocalPointInRectangle(container, Input.mousePosition, null, out lp);

		if(!res) return;
		
		int x = (int)(lp.x * ((float)texture.width/container.sizeDelta.x));
		int y = (int)(lp.y * ((float)texture.height/container.sizeDelta.y));
		
		Circle(x, y);
//		drawStroke ();
		ParticleCreator();

		AudioManager.Instance.ScratchFX (true);
	}


	public void Circle(int cx, int cy){
		
		int x, y, px, nx, py, ny, d, r = CIRCLE_RADIUS;
		for (x = 0; x <= r; x++){
			d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));

			for (y = 0; y <= d; y++){
				
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;

				if (px > 0 && px < texture.width && py > 0 && py < texture.height)texture.SetPixel (px, py, Color.red);

				if (nx > 0 && nx < texture.width && py > 0 && py < texture.height)texture.SetPixel (nx, py, Color.red);

				if (px > 0 && px < texture.width && ny > 0 && ny < texture.height)texture.SetPixel (px, ny, Color.red);

				if (nx > 0 && nx < texture.width && ny > 0 && ny < texture.height)texture.SetPixel (nx, ny, Color.red);

			}
		}    

		texture.Apply (); 
	}
		



	public void  drawStroke(){
		
		pxls = texture.GetPixels ();
		newPxls = new Color[ pxls.Length ];


		for (int i = 0; i < pxls.Length; i++) {

			_t = i - I_WIDTH;
			_r = i + 1;
			_b = i + I_WIDTH;
			_l = i - 1;

			if (_t < 0 || _t > pxls.Length - 1) _t = i;
			if (_r < 0 || _r > pxls.Length - 1) _r = i;
			if (_b < 0 || _b > pxls.Length - 1) _b = i;
			if (_l < 0 || _l > pxls.Length - 1) _l = i;



			if (pxls [i] != pxls [_t] || pxls [i] != pxls [_b] || pxls [i] != pxls [_l] || pxls [i] != pxls [_r]) {
				newPxls [i] = StrokeColor;

				newPxls [_l] = StrokeColor;
				newPxls [_b] = StrokeColor;
			}

		}

		borderTexture.SetPixels (newPxls);
		borderTexture.Apply ();
		ParticleCreator();
	}



	public Transform particlesParent;
	public GameObject particlePrefab;
	GameObject newParticle;
	Vector3 particleRotation;
	//float startTime;
	WaitForEndOfFrame _waitEndFrame = new WaitForEndOfFrame();
	//Vector3 particlePos;
	int generateIndex = 0;
	int randomIndex = 10;

	void ParticleCreator(){
		generateIndex += 1;
		if (generateIndex == randomIndex) {
			newParticle = (GameObject)Instantiate (particlePrefab, Input.mousePosition, transform.rotation);
			newParticle.transform.SetParent (particlesParent);
			particleRotation.z = Random.Range (0f, 360f);
			newParticle.transform.eulerAngles = particleRotation;
			StartCoroutine (ParticleMover (newParticle));
			generateIndex = 0;
			randomIndex = Random.Range (3, 12);
		}

	}

	IEnumerator ParticleMover(GameObject newParticle){
		Vector2 particleScale;
		particleScale.x = particleScale.y = Random.Range (1.5f, 3.5f);
		newParticle.transform.localScale = particleScale;
		Vector3 particlePos = newParticle.transform.position;
		float particleSpeedFactor = 200f;
		float startTime = Time.time;
		while ((Time.time - startTime) < 2f) {
			particleSpeedFactor *= 1.03f;
			particlePos.y -= particleSpeedFactor * Time.deltaTime;
			newParticle.transform.position = particlePos;
			yield return _waitEndFrame;
		}
		Destroy (newParticle);
	}


	public void StopDrawing(){
		if (!drawing)		return;

		drawing = false;
		AudioManager.Instance.ScratchFX (false);
		CheckScratch ();
	}
	


	void CheckScratch(){

		pixelColor = texture.GetPixel( (int)(I_WIDTH * 0.33f) , (int)(I_HEIGHT * 0.25f));
		if (pixelColor == Color.clear) {
			return;
		}
		pixelColor = texture.GetPixel((int)(I_WIDTH * 0.33f) , (int)(I_HEIGHT * 0.5f));
		if (pixelColor == Color.clear) {
			return;
		}
		pixelColor = texture.GetPixel((int)(I_WIDTH * 0.33f) , (int)(I_HEIGHT * 0.75f));
		if (pixelColor == Color.clear) {
			return;
		}
		pixelColor = texture.GetPixel((int)(I_WIDTH * 0.66f) , (int)(I_HEIGHT * 0.25f));
		if (pixelColor == Color.clear) {
			return;
		}
		pixelColor = texture.GetPixel((int)(I_WIDTH * 0.66f) , (int)(I_HEIGHT * 0.5f));
		if (pixelColor == Color.clear) {
			return;
		}
		pixelColor = texture.GetPixel((int)(I_WIDTH * 0.66f) , (int)(I_HEIGHT * 0.75f));
		if (pixelColor == Color.clear) {
			return;
		}



		OnImageDiscovered ();

	}


	public void OnImageDiscovered(){
		drawing = false;
		PrizesContainer.GetComponent<GridLayoutGroup> ().enabled = false;


		for (int y = 0; y < texture.height; y++) {
			for (int x = 0; x < texture.width; x++) {
				texture.SetPixel(x,y, Color.red);
			}
		}

		texture.Apply (); 
		borderTexture.SetPixels (new Color[I_WIDTH * I_HEIGHT]);
		borderTexture.Apply ();

		CardsScrn_Actions.instance.Win (SelectedPrize);

		foreach( GiftCardPrizeIcon gcpi in PrizesIconsWinners ){
			gcpi.Animate ();
		}
	}

	public static Prizes GetRandomPrize(){

		int rand = Random.Range (1, 100);
		Prizes tempPrize = Prizes.COINS_L;

		if (rand < 5) 	tempPrize = Prizes.COINS_S;
		else if(rand < 15) tempPrize = Prizes.COINS_M;
		else if(rand < 25) tempPrize = Prizes.COINS_L;
		else if(rand < 30) tempPrize = Prizes.MAGNET_S;
		else if(rand < 45) tempPrize = Prizes.MAGNET_M;
		else if(rand < 55) tempPrize = Prizes.ENERGY_DRINK;
		else if(rand < 60) tempPrize = Prizes.SPEED_S;
		else if(rand < 75) tempPrize = Prizes.X2_S;
		else if(rand < 85) tempPrize = Prizes.SPEED_M;
		else if(rand < 90) tempPrize = Prizes.JACKPOT;
		else if(rand < 95) tempPrize = Prizes.SHIELD_S;
		else 				tempPrize = Prizes.SHIELD_M;

		return tempPrize;

	}

}

