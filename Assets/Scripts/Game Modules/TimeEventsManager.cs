﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class TimeEventsManager : MonoBehaviour {

	public static TimeEventsManager Instance;

	string[] _tmpStrArr_1;
	string[] _tmpStrArr_2;

	WaitForSeconds _check_interval;
	JSONNode managerBonusesData;
	JSONNode gamblerBonusesData;

	public int BonusForManagers = 0;
	public double BonusForGamblers = 0;


	void Start () {
		Instance = this;
		_check_interval = new WaitForSeconds(60f);

		InitPerksData ();
		StartCoroutine ( CheckBonusTask() );
	}

	void InitPerksData(){
		for (int i=0; i<DAO.Perks.Count; i++) {
			if(DAO.Perks[i]["id"].Value == "mngr"){
				managerBonusesData = DAO.Perks[i]["effect"];
			}else if(DAO.Perks[i]["id"].Value == "gamblr"){
				gamblerBonusesData = DAO.Perks[i]["effect"];
			}
		}
	}

	void ResetPerksBonusesValues(){
		BonusForManagers = 0;
		BonusForGamblers = 0;

		// Purchased players
		_tmpStrArr_1 = DAO.PurchasedPlayers.Split (DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries);

		foreach (string pp in _tmpStrArr_1) {
			// Player Data - Perks
			_tmpStrArr_2 = DAO.GetPlayerData( int.Parse(pp) ).Split(DAO.char_separator, System.StringSplitOptions.RemoveEmptyEntries);

			// Check Perk 1 if manager or Gambler
			if( _tmpStrArr_2[7] == "MANAGER" ){


				switch( _tmpStrArr_2[8] ){
					case "1" : {
						BonusForManagers += int.Parse( managerBonusesData[0] ) * _tmpStrArr_1.Length;
						break;
					}
					case "2" : {
						BonusForManagers += int.Parse( managerBonusesData[1] ) * _tmpStrArr_1.Length;
						break;
					}
					case "3" : {
						BonusForManagers += int.Parse( managerBonusesData[2] ) * _tmpStrArr_1.Length;
						break;
					}
				}
			}else if( _tmpStrArr_2[7] == "GAMBLER" ){
				switch( _tmpStrArr_2[8] ){
				case "1" : {
					BonusForGamblers += double.Parse( gamblerBonusesData[0] );
					break;
				}
				case "2" : {
					BonusForGamblers += double.Parse( gamblerBonusesData[1] );
					break;
				}
				case "3" : {
					BonusForGamblers += double.Parse( gamblerBonusesData[2] );
					break;
				}
				}
			}




			// Check Perk 2 if manager or Gambler
			if( _tmpStrArr_2[9] == "MANAGER" ){
				switch( _tmpStrArr_2[10] ){
					case "1" : {
						BonusForManagers += int.Parse( managerBonusesData[0] ) * _tmpStrArr_1.Length;
						break;
					}
					case "2" : {
						BonusForManagers += int.Parse( managerBonusesData[1] ) * _tmpStrArr_1.Length;
						break;
					}
					case "3" : {
						BonusForManagers += int.Parse( managerBonusesData[2] ) * _tmpStrArr_1.Length;
						break;
					}
				}
			}else if( _tmpStrArr_2[9] == "GAMBLER" ){
				switch( _tmpStrArr_2[10] ){
					case "1" : {
					BonusForGamblers += double.Parse( gamblerBonusesData[0] );
						break;
					}
					case "2" : {
					BonusForGamblers += double.Parse( gamblerBonusesData[1] );
						break;
					}
					case "3" : {
					BonusForGamblers += double.Parse( gamblerBonusesData[2] );
						break;
					}
				}
			}





			// Check Perk 3 if manager or Gambler
			if( _tmpStrArr_2[11] == "MANAGER" ){
				switch( _tmpStrArr_2[12] ){
					case "1" : {
						BonusForManagers += int.Parse( managerBonusesData[0] ) * _tmpStrArr_1.Length;
						break;
					}
					case "2" : {
						BonusForManagers += int.Parse( managerBonusesData[1] ) * _tmpStrArr_1.Length;
						break;
					}
					case "3" : {
						BonusForManagers += int.Parse( managerBonusesData[2] ) * _tmpStrArr_1.Length;
						break;
					}
				}
			}else if( _tmpStrArr_2[11] == "GAMBLER" ){
				switch( _tmpStrArr_2[12] ){
					case "1" : {
						BonusForGamblers += double.Parse( gamblerBonusesData[0] );
						break;
					}
					case "2" : {
						BonusForGamblers += double.Parse( gamblerBonusesData[1] );
						break;
					}
					case "3" : {
						BonusForGamblers += double.Parse( gamblerBonusesData[2] );
						break;
					}
				}
			}
		}

	}

	public void SetPerksBonusTime(bool forceSet = false){
		if (DAO.PerksBonusTime == "0" || forceSet) {
			DAO.PerksBonusTime = new System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.AddDays(1).Day, 1, 0, 0).ToString();
		}
	}


	bool IsTimeForBonus(){
	
		if (DAO.PerksBonusTime == "0")  return false;

		if (System.DateTime.Now >= System.DateTime.Parse (DAO.PerksBonusTime)) return true;
		else return false;

		return false;

	}

	IEnumerator CheckBonusTask(){

		if (IsTimeForBonus ()) {
			GivePerksBonuses();
		}

		yield return _check_interval;

		StartCoroutine ( CheckBonusTask() );

	}

	void GivePerksBonuses(){

		if (GameManager.curentGameState == GameManager.GameState.TEST_RUN
			|| GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN
			|| GameManager.curentGameState == GameManager.GameState.CUP_RUN
			|| GameManager.curentGameState == GameManager.GameState.INFINITY_RUN
		   ) {
			return;
		}

		SetPerksBonusTime(true);
		ResetPerksBonusesValues ();

		// -------------

		if (BonusForManagers <= 0 || BonusForGamblers <= 0) return;

		GenericUIMessage msg = new GenericUIMessage ();
		msg.Title = "You Have a bonus!";
		msg.Message = "";

		if(BonusForManagers > 0) msg.Message += "You have earned a " + BonusForManagers + " bonus coins\n becouse you have a manager/s in your team!\n";
		if(BonusForGamblers > 0) msg.Message += "You have earned a " + BonusForGamblers + " bonus scratch cards\n becouse you have a gamblers/s in your team!\n";

		msg.PositiveButtonText = "Claim";

		msg.PositiveBTNAction = () => {

			CrossSceneUIHandler.Instance.hideGenericMessage ();

			if(BonusForManagers > 0) {
				DAO.TotalCoinsCollected += BonusForManagers;
				CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

				//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Wallet_Deposit(BonusForManagers, "Daily Reward from Skill", "FCB Coins", "","","");
				GameAnalyticsWrapper.ResourceEvent(true,GameAnalyticsWrapper.CURR_COINS,BonusForManagers, GameAnalyticsWrapper.FINISHED_RUN,"SkillBonus");
			}

			if(BonusForGamblers > 0) DAO.TotalGiftCardsCollected += (float)BonusForGamblers;

			DAO.Instance.LOCAL.SaveAll();
		};

		CrossSceneUIHandler.Instance.showGennericMessage (msg);




	}











}
