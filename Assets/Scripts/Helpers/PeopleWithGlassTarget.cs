﻿using UnityEngine;
using System.Collections;

public class PeopleWithGlassTarget : MonoBehaviour {


	bool collided = false;

	public PeopleWithGlass peopleWithGlassScript;

	void Start () {
	
	}

	void OnTriggerEnter(Collider col) {
		
		if (!collided && col.gameObject.tag == "Ball") {
			collided = true;
			PlayerController.instance.ballHitTheTarget = true;
			peopleWithGlassScript.Crash ();
			GetComponent<Collider> ().enabled = false;
			RunManager.OnTargetShotByBall();
		}

	}
}
