﻿using UnityEngine;
using System.Collections;

public class EnvoirmentFollow : MonoBehaviour {

	public static EnvoirmentFollow instanse;

	public Transform target;

	public Transform bGPlane;

	Vector3 pos;
	Vector3 planePos;
	Vector3 rot;


	void Awake(){
		instanse = this;
	}

	void Start(){
		pos.y = transform.position.y;

	}

	void FixedUpdate () {
		pos.x = target.position.x;
		pos.z = target.position.z;
		rot.y = target.eulerAngles.y;

		transform.position = pos;
		bGPlane.eulerAngles = rot;

		transform.Rotate( 0f, 0.01f, 0f);
	}

	public void PlaneUp(){
//		pos
//		transform.position

//		planePos = bGPlane.position;
//		planePos.y += 5;
//		bGPlane.position = planePos;
	}

	public void PlaneDown(){
//		planePos = bGPlane.position;
//		planePos.y -= 5;
//		bGPlane.position = planePos;
	}
}
