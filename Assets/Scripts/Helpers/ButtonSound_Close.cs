﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSound_Close : MonoBehaviour {

	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => {
			OnClicked();
		});
	}


	public void OnClicked(){
		AudioManager.Instance.OnClosePopup ();
	}

}
