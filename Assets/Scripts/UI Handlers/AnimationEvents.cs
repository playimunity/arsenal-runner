using UnityEngine;
using System.Collections;
using System;
using Facebook.Unity;

public class AnimationEvents : MonoBehaviour {

	public static event Action OnSceneTransitionShownEvent;

	public void OnSceneTransitionHidded(){
		//CrossSceneUIHandler.Instance.showCoinsCounter ();
		GM_Input._.Lock = false;
		gameObject.SetActive (false);
	}

	public void OnSceneTransitionShown(){
		if (OnSceneTransitionShownEvent != null) OnSceneTransitionShownEvent ();
		if(UIManager.asyncLeveLoad != null) UIManager.asyncLeveLoad.allowSceneActivation = true;
	}

	public void OnStoreHidden(){
		GameManager.RequestUnpause ();
		gameObject.SetActive (false);

		if (GameManager.curentGameState == GameManager.GameState.LOCKER_ROOM && GM_Input._.MedikKits) {
			MedikKits_Actions.Instance.RefreshUI ();
		}
	}

	public void onGenericMessageHidden(){
		GameManager.RequestUnpause ();
		gameObject.SetActive (false);
	}

	public void onGenericAdHidden(){
		GameManager.RequestUnpause ();
		gameObject.SetActive (false);
	}

	WaitForSeconds waitASecond;


	public void ResetBenchAnimatorSpeed(){
		MainScreenUiManager.instance.uiAnimator.speed = 1f;
	}


	public void OnClaimScreenHidden(){
		if (!ClaimScreenHandler._.isGiftCard)
			return;
		
		CardsScrn_Actions.instance.OnPrizeClaimed ();
	}
		

	public void Shwap(){
		AudioManager.Instance.OnHitGod ();
	}

	public void ShowCardsTut(){
		CardsScrn_Actions.instance.ShowTutFillLuckyPopup ();
	}

//	public void FacebookPopupCheck(){
//		if (DAO.FacebookPopup == 0 && !FB.IsLoggedIn) {
//			DAO.FacebookPopup++;
//			MainMenu_Actions.instance.FacebookPopupToggle(true);
//		}
//	}


	public void Destroy(){
		Destroy (gameObject);
	}

	public void PlayerLevelUpFX(){
		PracticeRunController.Instance.LevelUpIconsAnim ();
	}

	public void CreateSecretArea(){
		SecterAreaHandler.instance.CreateStadium ();
	}

	public void ShowCardsHandTut(){
		CardsScrn_Actions.instance.ShowCardsHandTut ();
//		CardsScrn_Actions.instance.isDrawAllowed = true;
	}

	public void UpdatePreviewPlayerInfo(){
		MainScreenUiManager.instance.PreviewPlayerInfoUpdate ();
	}

	public void UpdatePlayerInfo(){
		CrossSceneUIHandler.Instance.PlayerInfoUpdate ();
	}

	public void UpdatePlayerName(){
		MainScreenUiManager.instance.PlayerNameUpdate ();
	}

	public void ShowCoinsCounter(){
		CrossSceneUIHandler.Instance.showCoinsCounter ();
	}
		

}
