﻿using UnityEngine;
using System.Collections;

public class CameraLookAt : MonoBehaviour {

	//public Transform lookAtTarget;
	public Transform cameraTarget;

	public bool turnLeft = false;
	public bool turning = false;
	bool lookAtOn = false;

	Vector3 cameraCurrentRotation;
	//Vector3 cameraOriginalRotation;
	Vector3 targetPos;
	Vector3 pos;
	Vector3 rot;

	float startTime;
	float camNewY;
	float camNewZ;
	float camXDirection;
//	public float additionalHeightFactor = 0f;
	float currentYPos;

	WaitForEndOfFrame waitForEndFrame = new WaitForEndOfFrame();

	public static CameraLookAt instance;



	void Awake(){
		instance = this;
	}

	void Start(){
		camNewY = transform.localPosition.y;
		camNewZ = transform.localPosition.z;
		camXDirection = transform.localEulerAngles.x;
		//SetCameraPosition (4f, -4.5f, 18.45f);
	}


//	public void SetCameraPosition(){
//		StopCoroutine ("SetCameraPositionCoroutine");
//		camNewY = currentYPos + additionalHeightFactor;
//		pos = transform.localPosition;
//		rot = transform.localEulerAngles;
//		startTime = Time.time;
//		StartCoroutine ("SetCameraPositionCoroutine");
//	}


	public void SetCameraPosition(float newY , float newZ , float xDirection){
		if (camNewY != newY || camNewZ != newZ) {
			StopCoroutine ("SetCameraPositionCoroutine");
			currentYPos = newY;
			camNewY = newY;
			camNewZ = newZ;
			camXDirection = xDirection;
			//targetPos = cameraTarget.localPosition;
			pos = transform.localPosition;
			rot = transform.localEulerAngles;
			startTime = Time.time;
			StartCoroutine ("SetCameraPositionCoroutine");
		}
	}


	IEnumerator SetCameraPositionCoroutine(){
		while(Time.time - startTime < 0.7f){
			pos.y = Mathf.Lerp (pos.y, camNewY, 6f * Time.deltaTime);
			pos.z = Mathf.Lerp (pos.z, camNewZ, 6f * Time.deltaTime);
			pos.x = transform.localPosition.x;
			transform.localPosition = pos;
		//	Debug.Log ("alon__________ CAMERA position is changing !!!");
			rot.x = Mathf.Lerp (rot.x, camXDirection, 6f * Time.deltaTime);
			rot.y = transform.localEulerAngles.y;
			rot.z = transform.localEulerAngles.z;
			transform.localEulerAngles = rot;
			yield return waitForEndFrame;
		}

		pos.y = camNewY;
		pos.z = camNewZ;
		pos.x = transform.localPosition.x;
		transform.localPosition = pos;
		rot.x = camXDirection;
		rot.y = transform.localEulerAngles.y;
		rot.z = transform.localEulerAngles.z;
		transform.localEulerAngles = rot;
	}




}
