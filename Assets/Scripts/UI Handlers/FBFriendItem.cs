using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RTLService;

public class FBFriendItem : MonoBehaviour {

	public bool Selected = true;
	public bool Claimed = false;
	public bool Invited = false;

	public RawImage Photo;
	public Text Name;

	public string FriendId;

	public GameObject CTA_Gift;
	public GameObject CTA_Gift_Selected;
	public GameObject CTA_Gift_Unselected;



	public GameObject CTA_Claim;
	//public Text CTA_Claim_TXT;
	public GameObject CTA_Claim_Unclaimed;
	public GameObject CTA_Claim_Claimed;

	public GameObject CTA_Request;
	public GameObject CTA_Request_Unrequested;
	public GameObject CTA_Request_Requested;


	public GameObject CTA_Invite;
	//public Text CTA_Invite_TXT;
	public GameObject CTA_Invite_Uninvited;
	public GameObject CTA_Invite_Invited;

	public void Awake(){
		Name.text = "...";

		CTA_Gift.SetActive (false);
		CTA_Claim.SetActive (false);
		CTA_Invite.SetActive (false);

		// If SEND_GIFTCARDS || SEND_GIFTCARD_REQUEST
		if (FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.SEND_GIFTCARDS
			|| FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.GIFTCARDS_REQUESTS) {
			CTA_Gift.SetActive (true);
			CTA_Gift_Selected.SetActive (true);
			CTA_Gift_Unselected.SetActive (false);
		}

		// If GIFTCARDS_REQUESTS
		if( FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.ASK_FOR_GIFTCARDS ){
			CTA_Request.SetActive (true);
			CTA_Request_Unrequested.SetActive (false);
			CTA_Request_Requested.SetActive (true);
		}

		// If CLAIM_GIFTCARDS
		if (FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.CLAIM_GIFTCARDS) {
			CTA_Claim.SetActive (true);
			CTA_Claim_Unclaimed.SetActive (true);
			CTA_Claim_Claimed.SetActive (false);
		}

		// If INVITE_FRIENDS
		if(FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.INVITE_FRIENDS){
			CTA_Invite.SetActive (true);
			CTA_Invite_Uninvited.SetActive (false);
			CTA_Invite_Invited.SetActive (true);

			//CTA_Invite_TXT.text = DAO.Language.GetString ("invite");

		}

		// CHOOSE FRIEND
		if(FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.CHOOSE_FRIEND){


		}
	}

	// -------------------------------------

	public void OnClicked(){
		

		// If SEND_GIFTCARDS || SEND_GIFTCARD_REQUEST
		if (FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.SEND_GIFTCARDS
			|| FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.GIFTCARDS_REQUESTS
			|| FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.ASK_FOR_GIFTCARDS
			||FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.INVITE_FRIENDS) {
			Selected = !Selected;
			resetSelectedStatus ();
		}
	

		// CLAIM_GIFTCARDS
		if (FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.CLAIM_GIFTCARDS) {
			Claimed = true;

			CTA_Claim_Claimed.SetActive (true);
			CTA_Claim_Unclaimed.SetActive (false);
			GetComponent<Button> ().interactable = false;

			DDB._.ClaimGiftCard (FriendId);
		}

		// CHOOSE FRIEND
		if(FriendChooseScreen._.CurrentState == FriendChooseScreen.ScreenStates.CHOOSE_FRIEND){
			StatsController.Instance.OnFriendForStatsClicked (FriendId);
		}

	}

	public void resetSelectedStatus(){
		
		if (Selected) {
			CTA_Gift_Selected.SetActive (true);
			CTA_Gift_Unselected.SetActive (false);

			CTA_Request_Requested.SetActive (true);
			CTA_Request_Unrequested.SetActive (false);

			CTA_Invite_Invited.SetActive (true);
			CTA_Invite_Uninvited.SetActive (false);
		} else {
			CTA_Gift_Selected.SetActive (false);
			CTA_Gift_Unselected.SetActive (true);

			CTA_Request_Requested.SetActive (false);
			CTA_Request_Unrequested.SetActive (true);

			CTA_Invite_Invited.SetActive (false);
			CTA_Invite_Uninvited.SetActive (true);
		}


	}

	public void SetFriend(FBFriend friend){
		Name.text = RTL.Convert(friend.name);
		FriendId = friend.id;
		Photo.texture = friend.photo;
	}


}
