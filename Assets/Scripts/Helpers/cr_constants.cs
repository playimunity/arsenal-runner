﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

public class cr_constants{

	public static int NUM_OF_LINES = 40;
	public static int NUM_OF_CHARS = 41;
	public static string[] CHARS = {"E","F","G","J","K","L","M","N","R","S","X","0","1","2","3","4","5","6","7","8","9"};

	static string[] _finalText;
	static string[] HASHES = {
		"LT0gQ1JFRElUUyA9LQ==",

		"IyBQUk9EVUNUIE1BTkFHRU1FTlQgIw==",
		"U2hpbW9uIFNob21yb24=",

		"IyBVSS9VWCBEZXNpZ24gIw==",
		"U2hpbW9uIFNob21yb24=",

		"IyBHQU1FIFBST0RVQ0VSICM=",
		"R3V5IFphaW5kZW5iZXJn",

		"IyBMRVZFTCBERVNJR04gIw==",
		"R3V5IFphaW5kZW5iZXJn",
		"VHp2aWthIFR6YXJmYXRp",
		"Wml2IFNvZnJpbg==",

		"IyAzRCBBUlRJU1QgIw==",
		"SWdvciBBa29waWFu",

		"IyBHQU1FIERFVkVMT1BFUiAj",
		"QWxvbiBEb3Ju",

		"IyBDSElFRiBHQU1FIERFVkVMT1BFUiAj",
		"SWdvciBNYXNsbw==",

		"IyBEQiAmIFNFUlZFUiBJTkZSQVNUUlVDVFVSRSAj",
		"SWdvciBNYXNsbw=="
	}; // 18


	public static int TOTAL_CHARS{
		get{ return NUM_OF_CHARS * NUM_OF_LINES;}
	}

	public static string RandomChar{
		get{
			return CHARS[ UnityEngine.Random.Range(0, CHARS.Length-1)];
		}
	}

	public static string[] FINAL{
		get{

			if (_finalText == null) buildHashes ();

			return _finalText;

		}
	}

	// -----------

	static string Decode(string encodedText){
		byte[] decodedBytes = Convert.FromBase64String (encodedText);
		return Encoding.UTF8.GetString (decodedBytes);
	}

	static void buildHashes(){
		_finalText = new string[NUM_OF_LINES];

		int index = 0;

		while (index <= 5) {
			if(index == 1) _finalText[index] = Decode(HASHES[0]);
			else _finalText[index] = "";
			index++;
		}

		_finalText [index++] = Decode(HASHES[1]);
		_finalText [index++] = Decode(HASHES[2]);
		_finalText[index++] = "";

		_finalText [index++] = Decode(HASHES[3]);
		_finalText [index++] = Decode(HASHES[4]);
		_finalText[index++] = "";

		_finalText [index++] = Decode(HASHES[5]);
		_finalText [index++] = Decode(HASHES[6]);
		_finalText[index++] = "";

		_finalText [index++] = Decode(HASHES[7]);
		_finalText [index++] = Decode(HASHES[8]);
		_finalText [index++] = Decode(HASHES[9]);
		_finalText [index++] = Decode(HASHES[10]);
		_finalText[index++] = "";

		_finalText [index++] = Decode(HASHES[11]);
		_finalText [index++] = Decode(HASHES[12]);
		_finalText[index++] = "";

		_finalText [index++] = Decode(HASHES[13]);
		_finalText [index++] = Decode(HASHES[14]);
		_finalText[index++] = "";

		_finalText [index++] = Decode(HASHES[15]);
		_finalText [index++] = Decode(HASHES[16]);
		_finalText[index++] = "";

		_finalText [index++] = Decode(HASHES[17]);
		_finalText [index++] = Decode(HASHES[18]);
		_finalText[index++] = "";

		while (index < _finalText.Length) {
			_finalText[index++] = "";
		}


	}


}
