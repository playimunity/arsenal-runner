﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActivateBtns : MonoBehaviour {

	public GameObject btnActive;
	public GameObject btnDeactive;
	public GameObject iconActive;
	public GameObject iconDeactive;

	public Text text;

	public Color textActiveCol;
	public Color textDeactiveCol;



	public void Activate(){
		btnActive.SetActive (true);
		btnDeactive.SetActive (false);
		iconActive.SetActive (true);
		iconDeactive.SetActive (false);
		text.color = textActiveCol;
	}

	public void Deactivate(){
		btnActive.SetActive (false);
		btnDeactive.SetActive (true);
		iconActive.SetActive (false);
		iconDeactive.SetActive (true);
		text.color = textDeactiveCol;
	}
}
