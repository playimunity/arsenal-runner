using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using SimpleJSON;


public class PlayerController : MonoBehaviour
{

	public static PlayerController instance;

	public PlayerData data;

	public Animator playerAnimator;
	public Animator citySilhouetteAnim;
	//public GameObject forceColider;

	public Animator ballAnimation;

	//GameObject selectedPlayerPrefab;
	public GameObject player;
	public GameObject playerCollider;
	public GameObject ball;
	public GameObject ballFlying;
	//public GameObject regDollPlayer;
	GameObject ballTarget;
	public GameObject[] winningEffects;
	public GameObject medal;
	public GameObject godFX;
	public GameObject tornadoFX;

	//public Transform confety1Pos;
	//public Transform confety2Pos;

	Rigidbody playerRigidbody;
	Rigidbody ballFlyingRigidbody;

	public ParticleSystem starWarsFx;
	ParticleSystem tornadoParticles;
	public TrailRenderer[] godSmallTrails;
	public TrailRenderer godTrail;

	Color starWarsColor;
	Color playerTrailColor;

	public bool canJump = true;
	public bool onAction = false;
	public bool jumping = false;
	public bool withBall = false;
	public bool ballShooted = false;
	public bool sliding = false;
	public bool isRunning = true;
	//public bool leftBlocked = false;
	//public bool rightBlocked = false;
	public bool isDead = true;
	public bool isRunEnd = false;
	public bool preperToEnd = false;
	public bool ballShooter = false;
	public bool sheildOn = false;
	public bool dontRun = false;
	public bool hardDamage = false;
	public bool sideDamage = false;
	//public bool shooting = false;
	public bool tiltingRight = false;
	public bool tiltingLeft = false;
	public bool turning = false;
	//public bool sectionOf3 = true;
	public bool stumbled = false;
	public bool upperBodyDamage = false;
	public bool slowingDown = false;
	public bool ballHitTheTarget = false;
	public bool magnetOn = false;
	public bool sheildModeDamage = false;
	public bool speedBoostOn = false;
	public bool secretArea = false;
	public bool secretAreaTutPoint = false;
	public bool deadlyCrush = false;
	public bool shootTapOn = false;
	public bool runningHight = false;

	//public float runningSpeed = DAO.Settings.PlayerRunSpeed;
	public float runningSpeed = 18f;
	public float currentSpeed;
	//float runningSpeed = 25f;
	//float speedBoostAdditionalSpeed = 1f;
	public float jumpFactor = DAO.Settings.PlayerJumpHeight;
	float jumpPerkFactor = 0.75f;
	//float jumpLengthFactor = 1f;
	float ballShootDistance = DAO.Settings.BallShotDistance;
	public float overallSpeed;
	public float runningSpeedMirror = 0f;
	float currentRunType = 0;
	//float currentDis = 10;
	//float tempDis = 0;
	//float midDis;
	//float yFactor = 0.1f;
	//float ballOriginalY;
	float ballSpeed = 100;
	float ballStartTime;
	//float currentTime;
	float endTime;
	float totalTime;
	public float speedBoostFactor = DAO.Settings.SpeedBoostFactor;
	public float speedBoostAdditionalSpeed = 1.0f;

	//float currentXposition = 0f;
	//float targetXposition = 0f;
	float OriginalXposition = 0f;
	public float staticMinYPos = -0.4f;

	int hits = 2;
	int selectedPlayer = 3;
	//int ballMoveCount = 0;
	int randomAnim;

	Vector3 targetPosition;
	Vector3 colliderCenter;
	//Vector3 ballTargetPosition;
	Vector3 playerLocalPosition;
	Vector3 playerPosition;
	Vector3 playerRotation = new Vector3 (0, 180, 0);
	//Vector3 ballYpos;
	//Vector3 jumpVelocity;
	//Vector3 vectorZona;

	public Text fitnessTxt;

	//private Vector3 posFixer;

	//private Transform playerColiderTranform;
	public Transform ballParent;
	Transform plTrans;
	Transform ballFlyTransform;

	//private float myZ;

	//public int PlayerNumber = 1;

	int prevMetersPassedMark = 0;
	float metersPassed = 0f;
	int notifyWhenMetersPassed = 5;

	public GameObject nearFieldCollider;
	public ParticleSystem whistleParticle;

	public Texture medalTextureSilver;
	public Texture medalTextureGold;

	//Coroutine turnCoroutine;

	WaitForEndOfFrame _waitForEndOfFrame = new WaitForEndOfFrame ();
	WaitForSeconds _waitForLanding = new WaitForSeconds (0.15f);
	WaitForSeconds _waitForRising = new WaitForSeconds (0.07f);

	public Obstacle obstacleToSmash;
	public Player playerScript;
	RagDollToggle ragDollScript;
	// Perk Related

	public float Perk_ChanceToMeetMagnetFactor = 0.5f;
	public float Perk_JumpHeightFactor = 1f;
	public float Perk_AdditionalSoftHits = 0f;
	public float Perk_ManOfSteelFactor = 1f;
	public float Perk_FitnessFactor = 1f;
	public float Perk_MagnetTimeFactor = 1f;
	public float Perk_EnergyTimeFactor = 1f;
	public int Perk_MoneyBonusIfRunCompleted = 0;

	public CapsuleCollider playerCapsulCollider;
	//public SphereCollider playerSphereCollider;

	public int HitsRemain {
		get {
			if (hits + (int)Perk_AdditionalSoftHits > 4) {
				return 5;
			} else {
				return hits + (int)Perk_AdditionalSoftHits;
			}
		}
		set { 
			hits = value;
		}
	}


	//The increesing variables:

	public int SectionsIndex = 0;
	public float increaseAdditionalSpeed = 0;

	public float speedIncreeseFactor = 0.5f;
	public float increaseMaxSpeed = 30f;
	public int numberOfSections = 5;



	void Awake ()
	{
		instance = this;

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			if (GameManager.ActiveCupCompleted) {
				runningSpeed = GameManager.ActiveCompletedCup.playerStartSpeed;
			} else {
				runningSpeed = GameManager.ActiveCup.playerStartSpeed;
			}
			if (runningSpeed < 10)
				runningSpeed = DAO.Settings.PlayerRunSpeed;
		}
		else 
			runningSpeed = DAO.Settings.PlayerRunSpeed;

		//Debug.Log ("RON____________________runningSpeed AWAKE:" + runningSpeed);
		//Debug.Log ("RON____________________DAO.Settings.PlayerRunSpeed AWAKE:" + DAO.Settings.PlayerRunSpeed);

		plTrans = playerCollider.transform;
		ballFlyTransform = ballFlying.transform;

		//		data = new PlayerData (PlayerNumber);
		InstantiatePlayer ();
		//_waitForEndOfFrame = new WaitForEndOfFrame ();
		//_waitForLanding = new WaitForSeconds (0.25f);
		InitPerksPower ();

		speedIncreeseFactor = DAO.Settings.SpeedIncreaseStep;
		increaseMaxSpeed = DAO.Settings.PlayerMaxSpeed;
		numberOfSections = DAO.Settings.NumOfSectionsToIncreaseSpeed;
	}

	public void ShowVideoAd ()
	{
		print ("SHOWING AD!");
	}

	void Start ()
	{		
		onAction = true;
		isDead = true;
		hits = DAO.Settings.DefaultAmountOfHitsToDie;

		playerRigidbody = playerCollider.GetComponent<Rigidbody> ();
		ballFlyingRigidbody = ballFlying.GetComponent<Rigidbody> ();

//		playerCapsulCollider = playerCollider.GetComponent<CapsuleCollider> ();
//		playerSphereCollider = playerCollider.GetComponent<SphereCollider> ();

		colliderCenter = playerCapsulCollider.center;

		citySilhouetteAnim.speed = 0f;

		//myZ = playerColiderTranform.localPosition.z;
		runningSpeedMirror = runningSpeed;
		currentSpeed = 0;
		citySilhouetteAnim.speed = 0f;

		targetPosition = playerCollider.transform.localPosition;
		targetPosition.x = 0f;

		// for the god effect:
		fadeDecreaseFactor = fadeStartColor / fadeSteps;
		fadeOneStep = timeToFade / fadeSteps;
		wait = new WaitForSeconds (fadeOneStep);

		//sheildFxColor = sheildFXParticle.startColor;

		//jumpFactor *= jumpPerkFactor;
		//jumpVelocity = new Vector3 (0f, jumpFactor * 2f, 0f);

		if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
			turnAroundFactor = 500f;
		} else {
			turnAroundFactor = 700f;
		}
	}


	void Update ()
	{

		#if UNITY_EDITOR
		if (Input.GetKeyUp (KeyCode.C)) { // just for testing
			CollectBall ();
		}




		if (Input.GetKeyUp (KeyCode.H)) {
			// just for testing
			PracticeRunController.Instance.NewHighScore();
		}

		if (Input.GetKeyUp (KeyCode.F)) { // just for testing
			FixedPlayerXPosition ();
		}

		if (Input.GetKeyUp (KeyCode.L)) {
			playerScript.data.LevelUp (false);
			//Debug.Log ("alon_______________ Sould LevelUp () !!! ");
		}


		#endif

		if (dontRun)
			return;

		if (isRunning) {

			//if (playerCollider.transform.localPosition.x != targetPosition.x) {
			if (tiltingLeft || tiltingRight) { // the tilting handler
				playerLocalPosition = plTrans.localPosition;

				if (playerLocalPosition.x < targetPosition.x) { // tilting right
					playerLocalPosition.x += 10f * Time.deltaTime;
					if (playerLocalPosition.x >= targetPosition.x) {
						playerLocalPosition.x = targetPosition.x;
						tiltingRight = false;
						tiltingLeft = false;
					}

				} else if (playerLocalPosition.x > targetPosition.x) { // tilting left
					playerLocalPosition.x -= 10f * Time.deltaTime;
					if (playerLocalPosition.x <= targetPosition.x) {
						playerLocalPosition.x = targetPosition.x;
						tiltingLeft = false;
						tiltingRight = false;
					}
				}

				plTrans.localPosition = playerLocalPosition;
			}

			metersPassed += currentSpeed * speedBoostAdditionalSpeed * Time.deltaTime;

			//			posFixer = playerColiderTranform.localPosition;
			//			posFixer.z = myZ;
			//			playerColiderTranform.localPosition = posFixer;
		}

		if ((int)Mathf.Ceil (metersPassed) > prevMetersPassedMark + notifyWhenMetersPassed) {
			prevMetersPassedMark += notifyWhenMetersPassed;

			RunManager.OnDistancePassed (notifyWhenMetersPassed);
			data.totalDistance += notifyWhenMetersPassed;
		}

	}

	Vector3 v3forward = Vector3.forward;
	void FixedUpdate ()
	{
		if (!dontRun && isRunning && !turning) {
			transform.Translate (v3forward * currentSpeed * speedBoostAdditionalSpeed * 0.02f);  // move forward
			//*0.02f

			overallSpeed = currentSpeed * speedBoostAdditionalSpeed * 0.02f;
			if (plTrans.localPosition.y < staticMinYPos) {
				//Debug.Log ("alon___ player is falling under 0 - " + plTrans.localPosition.y);
				playerLocalPosition = plTrans.localPosition;
				playerLocalPosition.y = staticMinYPos;
				plTrans.localPosition = playerLocalPosition;
			}
		}
	}


	public void InstantiatePlayer ()
	{ // create the player
		player = (GameObject)Instantiate (PrefabManager.instanse.PlayerPrefab, new Vector3 (-10, 0, 0), playerCollider.transform.rotation);
		playerScript = player.GetComponent<Player> ();
		playerScript.SetPlayerStructure (PrefabManager.instanse.chosenPlayer.PlayerID);

		player.transform.localScale *= 1.4f;
		if (GameManager.curentGameState != GameManager.GameState.TUTORIAL_RUN) {
			player.transform.eulerAngles = playerRotation;
		}
		player.transform.parent = playerCollider.transform;

		data = playerScript.data;

		playerAnimator = playerScript.playerAnimator;

		starWarsColor = starWarsFx.startColor;
		//playerTrailColor = godSmallTrails [0].material.GetColor;
		playerTrailColor = Color.white;
		playerTrailColor.a = 1;


		starWarsFx.transform.parent = playerCollider.transform;
		godSmallTrails [0].transform.parent = playerScript.SholderL;
		godSmallTrails [0].transform.localPosition = Vector3.zero;
		godSmallTrails [1].transform.parent = playerScript.SholderR;
		godSmallTrails [1].transform.localPosition = Vector3.zero;
		godTrail.transform.parent = playerCollider.transform;
		godTrail.transform.localPosition = Vector3.zero;
	}

	public void PlayerStartAnimation ()
	{
		if (GameManager.curentGameState == GameManager.GameState.TUTORIAL_RUN) {
			playerAnimator.Play ("walk in tut");
		} else {
			playerAnimator.SetTrigger ("Start");
			playerScript.standType = 6;
		}
		player.transform.localPosition = Vector3.zero;
	}

	public void StartRun ()
	{
		if (!isDead)
			return;

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			Run.Instance.running = true;
			//if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE (BI.FLOW_STEP.ENTERED_QUEST_RUN);
            Cup activeCup;
        if (GameManager.ActiveCupCompleted) {
            activeCup = GameManager.ActiveCompletedCup;
        } else {
            activeCup = GameManager.ActiveCup;
        }
        data.fitness -= activeCup.cost;
		} else if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
			if(GameManager.Instance.isPlayscapeLoaded)	BI._.Flow_FTUE (BI.FLOW_STEP.ENTERED_PRACTICE_GAMEPLAY);
			//PracticeRunController.Instance.OnPracticeStarted ();
		}
		playerScript.StopAllCoroutines ();
		isDead = false;
		//currentSpeed = runningSpeed;
		onAction = false;

		StartCoroutine ("TurnAroundCoroutine");
		StartCoroutine ("StartRunCoroutine");
	}

	public void Respown ()
	{
		hits = DAO.Settings.DefaultAmountOfHitsToDie;
		transform.Translate (Vector3.back * 3f);
		playerAnimator.Play ("Stand Idle Respown");
		if (withBall) {
			withBall = false;
			ball.SetActive (false);
			ballFlying.SetActive (true);
			ballFlyTransform.parent = null;
			ballFlyingRigidbody.isKinematic = false;
			ballFlyingRigidbody.useGravity = true;
		}
		if (deadlyCrush) {
			deadlyCrush = false;
			if (Mathf.Abs (plTrans.localPosition.x) > 1f) {
				MovePlayerToCenter ();
			} else {
				StartCoroutine (MovePlayerAside ());
			}
		}
	}

	public void StartRunAfterDie ()
	{
		StartCoroutine ("StartRunAfterDieCoroutine");
	}


	IEnumerator StartRunAfterDieCoroutine ()
	{
		isDead = false;
		stumbled = false;
		RunManager.instance.saveMeOn = true;
		onAction = true;
		//dontGetHitFirstHit = false;

		yield return new WaitForSeconds (0.3f);

		//		if(GameManager.curentGameState == GameManager.GameState.CUP_RUN){
		//			CupMapBuilder.FreezeBuilder = false;
		//		}
		// put obstacle smasher here:
		if (obstacleToSmash != null) {
			obstacleToSmash.SmashObstacle ();
			obstacleToSmash = null;
		}
		isRunning = true;
		playerAnimator.SetTrigger ("StartRun");
		RunManager.instance.ActivateSheild (false);

//		if (RunManager.instance.fiveSectionsToEnd == true) {
//			AudioManager.Instance.PreperToEndRunCrowd (true);
//		}
		while (currentSpeed < runningSpeed) {
			currentSpeed += 40f * Time.deltaTime;
			yield return _waitForEndOfFrame;
		}
		AudioManager.Instance.OnRun ();
		currentSpeed = runningSpeed;
		citySilhouetteAnim.speed = 1f;
		//yield return new WaitForSeconds (0.2f);
		onAction = false;
	}

	public void StartTutorialRun ()
	{
		if (!isDead || GameManager.curentGameState != GameManager.GameState.TUTORIAL_RUN)
			return;
		playerScript.StopAllCoroutines ();
		isDead = false;
		onAction = false;

		StartCoroutine ("StartTutorialRunCoroutine");
	}

	IEnumerator StartRunCoroutine ()
	{

		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN || GameManager.curentGameState == GameManager.GameState.TEST_RUN) {
//			CupRunController.Instance.RunBtnAnimToggle (false);
//			yield return new WaitForSeconds (0.1f);
			CupRunController.Instance.RunGoalsAnimToggle (false);
			yield return new WaitForSeconds (0.3f);
			CupRunController.Instance.RunUiAnimToggle (true);
		} else if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
			PracticeRunController.Instance.RunBtnAnimToggle (false);
			yield return new WaitForSeconds (0.4f);
			PracticeRunController.Instance.RunUiAnimToggle (true);
		}
		FXCamera.instance.CameraMoveToOriginal ();
		playerAnimator.StopPlayback ();
		playerAnimator.CrossFade ("turn_and_run", 0.06f);
		yield return new WaitForSeconds (0.3f);
		while (currentSpeed < runningSpeed) {
			currentSpeed += 40f * Time.deltaTime;
			yield return _waitForEndOfFrame;
		}
		currentSpeed = runningSpeed;
		citySilhouetteAnim.speed = 1f;
		playerScript.StopAllCoroutines ();
		AudioManager.Instance.OnRun ();
	}

	IEnumerator StartTutorialRunCoroutine ()
	{

		CupRunController.Instance.RunUiAnimToggle (true);
		FXCamera.instance.CameraMoveToOriginal ();
		playerAnimator.CrossFade ("HumanoidRun", 0.1f);
		while (currentSpeed < runningSpeed) {
			currentSpeed += 60f * Time.deltaTime;
			yield return _waitForEndOfFrame;
		}
		AudioManager.Instance.OnRun ();
		currentSpeed = runningSpeed;
		citySilhouetteAnim.speed = 1f;
		playerScript.StopAllCoroutines ();
	}

	float turnAroundFactor;

	IEnumerator TurnAroundCoroutine ()
	{
		yield return new WaitForSeconds (0.8f);
		//ballStartTime = Time.time;
		while (playerRotation.y > 0f) {
			//playerRosition.y = Mathf.Lerp (playerRosition.y, 0f, 4f * Time.deltaTime);
			playerRotation.y -= turnAroundFactor * Time.deltaTime;
			player.transform.eulerAngles = playerRotation;
			yield return _waitForEndOfFrame;
		}
		playerRotation.y = 0f;
		player.transform.eulerAngles = playerRotation;
		//endTime = Time.time;
		//totalTime = endTime - ballStartTime;
		//		playerRosition.y = 0f;
		//		player.transform.eulerAngles = playerRosition;
	}

	public void TurnRight ()
	{

		if (onAction || targetPosition.x == 3f || isDead || secretAreaTutPoint)
			return;
		
//		if (sectionOf3 && targetPosition.x == 2f)
//			return;

		AudioManager.Instance.OnTurnRight ();

		//StopCoroutine ("WaitForTurn");

		if (!sliding && !jumping && !magnetOn) {
			playerAnimator.Play ("tilt r");
		}



		if (tiltingLeft) {
			tiltingLeft = false;
			tiltingRight = true;
			targetPosition.x = OriginalXposition;
		} else if (tiltingRight) {
			return;
		} else {
			tiltingLeft = false;
			tiltingRight = true;
			OriginalXposition = targetPosition.x;
			targetPosition.x += 3;
		}

		//StartCoroutine ("WaitForTurn");

	}

	public void TurnLeft ()
	{

		if (onAction || targetPosition.x == -3f || isDead || secretAreaTutPoint)
			return;

//		if (sectionOf3 && targetPosition.x == -2f)
//			return;

		AudioManager.Instance.OnTurnLeft ();

		//StopCoroutine ("WaitForTurn");

		if (!sliding && !jumping && !magnetOn) {
			playerAnimator.Play ("tilt l");
		}



		if (tiltingRight) {
			tiltingRight = false;
			tiltingLeft = true;
			targetPosition.x = OriginalXposition;
		} else if (tiltingLeft) {
			return;
		} else {
			tiltingRight = false;
			tiltingLeft = true;
			OriginalXposition = targetPosition.x;
			targetPosition.x -= 3;
		}

		//StartCoroutine ("WaitForTurn");
	}




	public void Jump()
	{
		if (jumping || onAction || !canJump || isDead) {
			return;
		}
		AudioManager.Instance.MuteRun ();
		AudioManager.Instance.OnJump ();

		if (secretAreaTutPoint) {
			SecterAreaHandler.instance.FinishTutorial ();
		}

		if (secretArea) {
			if (sliding) {
				sliding = false;

				playerCapsulCollider.height = 2.2f;
				colliderCenter.y = 1.1f;
				//colliderCenter.z = 0.1f;
				playerCapsulCollider.center = colliderCenter;
				//playerSphereCollider.enabled = false;
			}

			playerCapsulCollider.height = 2.2f;

			playerAnimator.Play ("header");

			jumping = true;
			return;
		}


		if (sliding) {
			sliding = false;

			playerCapsulCollider.height = 1.3f;
			colliderCenter.y = 1.1f;
			//colliderCenter.z = 0.1f;
			playerCapsulCollider.center = colliderCenter;
			//playerSphereCollider.enabled = false;
		}
		playerRigidbody.velocity = Vector3.up * jumpFactor * Perk_JumpHeightFactor * 2f;

		playerCapsulCollider.height = 1.3f;
		jumping = true;
		onAction = true;
		playerAnimator.Play ("jumping");
		//CameraFollow.instance.JumpOn();
		StartCoroutine (CallGroundDetector ());

		if (withBall) {
			ballAnimation.Play ("ball jump");
		}

	}

	IEnumerator CallGroundDetector ()
	{
		yield return _waitForRising;
		onAction = false;
		if (jumping) {
			GroundDetector.instance.isJumping = true;
		}
	}

	public void JumpEndEvent ()
	{ // called from the GroundDetector
		if (sliding || isDead)
			return;

		jumping = false;
		//GroundDetector.instance.isJumping = false;
		playerCapsulCollider.height = 2.2f;
		//jumpLengthFactor = 1;
		playerAnimator.Play ("HumanoidRun");
		if (speedBoostOn) {
			AudioManager.Instance.OnRunGodeMode();
		} else {
			AudioManager.Instance.OnRun();
		}

		if (withBall) {
			ballAnimation.Play ("ball run anim");
		}

//		if (plTrans.localPosition.y > 3.0f && !runningHight) {
//			runningHight = true;
//			CameraLookAt.instance.additionalHeightFactor = 4.8f;
//			CameraLookAt.instance.SetCameraPosition ();
//		} else if (plTrans.localPosition.y < 3.0f && runningHight) {
//			runningHight = false;
//			CameraLookAt.instance.additionalHeightFactor = 0f;
//			CameraLookAt.instance.SetCameraPosition ();
//		}
	}

	public void ForceJumpEnd ()
	{
		if (jumping) {
			jumping = false;
			playerRigidbody.velocity = new Vector3 (0, -jumpFactor * Perk_JumpHeightFactor * 7f, 0);
			if (speedBoostOn) {
				AudioManager.Instance.OnRunGodeMode();
			} else {
				AudioManager.Instance.OnRun();
			}
		}
	}

	IEnumerator WaitForLanding ()
	{
		yield return _waitForLanding;
		onAction = false;
	}


	public void BackFromHeader ()
	{
		jumping = false;
		playerAnimator.Play ("HumanoidRun");
	}

	public void Slide ()
	{
		if (sliding || onAction || isDead || secretAreaTutPoint || preperToEnd) {
			return;
		}

		AudioManager.Instance.MuteRun ();
		AudioManager.Instance.OnSlide ();

		if (jumping) {
			jumping = false;
			playerRigidbody.velocity = new Vector3 (0, -jumpFactor * Perk_JumpHeightFactor * 2f, 0);
			onAction = true;
			StartCoroutine (WaitForLanding ());
		}

		sliding = true;
		//playerSphereCollider.enabled = true;
		playerCapsulCollider.height = 0.6f;
		colliderCenter.y = 0.24f;
		//colliderCenter.z = 0.5f;
		playerCapsulCollider.center = colliderCenter;
		playerAnimator.Play ("sliding");

		if (withBall) {
			ballAnimation.Play ("ball slide");
		}
	}


	public void SlideEndEvent ()
	{
		if (jumping)
			return;

		playerCapsulCollider.height = 2.2f;
		colliderCenter.y = 1.1f;
		//colliderCenter.z = 0.1f;
		playerCapsulCollider.center = colliderCenter;
		//playerSphereCollider.enabled = false;
		sliding = false;
		onAction = false;

		if (speedBoostOn) {
			AudioManager.Instance.OnRunGodeMode();
		} else {
			AudioManager.Instance.OnRun();
		}

		if (withBall) {
			ballAnimation.Play ("ball run anim");
		}
	}

	//public bool dontGetHitFirstHit = false;
	public void Damage (int points)
	{
		//return;
		if (isDead) {
			return;
		}

		if (sheildOn) {
			sheildModeDamage = true;
			return;
		}

		stumbled = true;

		hits -= points;

		RunManager.OnHitPlayer ();

		if (withBall) {
			withBall = false;
			ball.SetActive (false);
			ballFlying.SetActive (true);
			ballFlyTransform.parent = null;
			ballFlyingRigidbody.isKinematic = false;
			ballFlyingRigidbody.useGravity = true;
		}

		if (jumping) {
			jumping = false;
			playerCapsulCollider.height = 2.2f;
		} else if (sliding) {
			sliding = false;
			playerCapsulCollider.height = 2.2f;
			colliderCenter.y = 1.1f;
			//colliderCenter.z = 0.1f;
			playerCapsulCollider.center = colliderCenter;
		//	playerSphereCollider.enabled = false;
		}

		StartCoroutine (WaitForDamage ());
	}

	IEnumerator WaitForDamage ()
	{
		yield return new WaitForFixedUpdate ();
        
		if (HitsRemain > 0) {

			if (sideDamage)
				playerAnimator.Play ("side stumble");
			else
				playerAnimator.Play ("stumbleAnim");

			isDead = false;
			StartCoroutine ("SlowDown" , (runningSpeed / 2f));
			sideDamage = false;

		} else {
			if (hardDamage) {
				DecreaseFitnessOnHardDamage ();
				if (turning)
					transform.parent = null;

				currentSpeed = 0;
				citySilhouetteAnim.speed = 0f;
				isRunning = false;
				onAction = true;
				isDead = true;

				AudioManager.Instance.MuteRun ();
				//playerScript.data.fitness -= 50; // for debugging

				if (upperBodyDamage) {
					playerAnimator.Play ("failAnim");
				} else {
					playerAnimator.Play ("fall back");
				}
				playerAnimator.StopPlayback ();

				hardDamage = false;
				StartCoroutine (GameOver ());
			} else {
				isRunning = true;
				if (turning)
					transform.parent = null;
				onAction = true;
				isDead = true;
				AudioManager.Instance.MuteRun ();
				if (sideDamage)
					playerAnimator.Play ("side stumble");
				else
					playerAnimator.Play ("stumbleAnim");

				playerAnimator.SetTrigger ("DeathTrigger");

				StopCoroutine ("SlowDown");
				StopCoroutine ("SpeedUp");
				StartCoroutine (SlowDown (0));
				citySilhouetteAnim.speed = 0f;
				//citySilhouetteAnim.set
				//city_silouete_movement
//				Debug.Log("alon___________ citySilhouetteAnim.speed = 0f");
				sideDamage = false;
			}
			RunManager.instance.StopProcesses ();
//			AudioManager.Instance.PreperToEndRunCrowd (false);
		}
	}

	public void IncreaseHits ()
	{
		if (!sliding) {
			playerAnimator.Play ("collect_feedback");
		}
		//if (hits == DAO.Settings.DefaultAmountOfHitsToDie)	return;
		if (HitsRemain > 4)
			return;

		hits++;
		//Run.Instance.runGoal.hits++;
	}



	public void DeadlyCrush ()
	{
		stumbled = true;
		DecreaseFitnessOnHardDamage ();
		if (turning)
			transform.parent = null;
		currentSpeed = 0;
		citySilhouetteAnim.speed = 0f;
		isRunning = false;
		onAction = true;
		isDead = true;

		if (withBall) {
			withBall = false;
			ball.SetActive (false);
			ballFlying.SetActive (true);
			ballFlyTransform.parent = null;
			ballFlyingRigidbody.isKinematic = false;
			ballFlyingRigidbody.useGravity = true;
		}

		//		playerPosition = playerCollider.transform.localPosition;
		//		playerPosition.z -= 0.3f;
		//		playerCollider.transform.localPosition = playerPosition;

		if (upperBodyDamage)
			playerAnimator.Play ("failAnim");
		else
			playerAnimator.Play ("fall back");


		hits = 0;
		hardDamage = true;
		deadlyCrush = true;
        print("BECAUSE OF A DEADLY CRUSH!");
		StartCoroutine (GameOver ());

		RunManager.instance.StopProcesses ();
//		AudioManager.Instance.PreperToEndRunCrowd (false);
	}


	public void SideTackle ()
	{
		if (tiltingRight || tiltingLeft) {
			StartCoroutine (SideTackleWait ());
		}
	}

	IEnumerator SideTackleWait ()
	{
		yield return new WaitForFixedUpdate ();

		if (sheildOn || stumbled || isDead)
			yield break;


		if (targetPosition.x > OriginalXposition) { // tackle from right
			playerAnimator.SetBool ("side tackle mirror", true);
		} else if (targetPosition.x < OriginalXposition) { // tackle from left
			playerAnimator.SetBool ("side tackle mirror", false);
		}
		targetPosition.x = OriginalXposition;
		sideDamage = true;
		AudioManager.Instance.OnSoftHit ();
		//ParticleCloudeGenerator.instance.ParticleOnlyWhistle(transform.position, transform.eulerAngles);
		whistleParticle.gameObject.SetActive (true);
		Damage (1);
		yield return new WaitForSeconds (1.5f);
		whistleParticle.gameObject.SetActive (false);
	}





	float timeToFade = 50f;
	float fadeStartColor = 0.15f;
	float fadeSteps = 25f;
	WaitForSeconds wait;
	float fadeDecreaseFactor;
	float fadeOneStep;
	bool speedBoostEffectOn = false;

	public void SpeedBoost(){
		if (isDead || preperToEnd) return;

		if (speedBoostOn) {
			StopCoroutine ("StartSpeedBoostEndCoroutine");
			StopCoroutine ("TornadoEndCorutine");
			StopCoroutine ("UnSpeedBoostDelay");
			tornadoFX.SetActive (false);
			//NativeSocial._.ReportAchievment (SocialConstants.achievement_godmode);
		}

		if (magnetOn) {
			//NativeSocial._.ReportAchievment (SocialConstants.achievement_double_trouble);
		} else {
			StartCoroutine (RunSwitcher (1));
		}

		speedBoostOn = true;
		speedBoostEffectOn = true;

		AudioManager.Instance.OnRunGodeMode ();

		starWarsColor.a = fadeStartColor;
		starWarsFx.startColor = starWarsColor;
		starWarsFx.gameObject.SetActive (true);
		playerTrailColor.a = fadeStartColor;
		foreach (TrailRenderer trail in godSmallTrails) {
			trail.material.color = playerTrailColor;
			trail.gameObject.SetActive (true);
		}
		godTrail.material.color = playerTrailColor;
		godTrail.gameObject.SetActive (true);

		tornadoFX.SetActive (true);
		tornadoParticles = tornadoFX.GetComponent<ParticleSystem> ();
		tornadoParticles.maxParticles = 30;

		if (!RunManager.instance.saveMeOn) {
			speedBoostAdditionalSpeed = DAO.Settings.PlayerGodModeAdditionalSpeed;
			//godmodeAdditionalSpeed = 1.5f;
		}
	}

	public void StartSpeedBoostEnd ()
	{
		StartCoroutine ("StartSpeedBoostEndCoroutine");
		StartCoroutine ("TornadoEndCorutine");
	}

	IEnumerator StartSpeedBoostEndCoroutine ()
	{
		//float startTime = Time.time;
		while (starWarsColor.a > 0.01 && speedBoostOn) {
			starWarsColor.a -= 0.002f;
			starWarsFx.startColor = starWarsColor;

			playerTrailColor.a -= 0.002f;
			foreach (TrailRenderer trail in godSmallTrails) {
				trail.material.color = playerTrailColor;
			}
			godTrail.material.color = playerTrailColor;
		
			//yield return wait;
			yield return _waitForEndOfFrame;
		}
		StartCoroutine (RunSwitcher (0));
	}

	IEnumerator TornadoEndCorutine(){
		while (tornadoParticles.maxParticles > 0 && sheildOn) {
			tornadoParticles.maxParticles -= 1;

			yield return _waitForLanding;
		}
	}


	public void UnSpeedBoost ()
	{

		AudioManager.Instance.OnRun ();

		starWarsFx.gameObject.SetActive (false);
		foreach (TrailRenderer trail in godSmallTrails) {
			trail.gameObject.SetActive (false);
		}
		godTrail.gameObject.SetActive (false);
		starWarsColor.a = fadeStartColor;
		starWarsFx.startColor = starWarsColor;

		speedBoostAdditionalSpeed = 1f;

		speedBoostEffectOn = false;
		speedBoostOn = false;
		tornadoFX.SetActive (false);

	}





//	public void SpeedBoost (bool on)
//	{
//		if (on) {
//			starWarsFx.gameObject.SetActive (true);
//			speedBoostOn = true;
//			speedBoostAdditionalSpeed = speedBoostFactor;
//		} else {
//			EndStarWarsFX ();
//			speedBoostOn = false;
//			speedBoostAdditionalSpeed = 1f;
//		}
//	}




	public void EndStarWarsFX ()
	{
		StartCoroutine (EndStarWarsFXCoroutine ());
	}


	IEnumerator EndStarWarsFXCoroutine ()
	{
		while (starWarsColor.a > 0.01) {
			starWarsColor.a -= 0.002f;
			starWarsFx.startColor = starWarsColor;
			yield return _waitForEndOfFrame;
		}
		starWarsFx.gameObject.SetActive (false);
		starWarsColor.a = fadeStartColor;
		starWarsFx.startColor = starWarsColor;
	}





	public void Sheild(){
		if (isDead || preperToEnd) return;

		//if (sheildOn) {
			//StopCoroutine ("StartSpeedBoostEndCoroutine");
			//StopCoroutine ("UnSpeedBoostDelay");
			//tornadoFX.SetActive (false);
			//NativeSocial._.ReportAchievment (SocialConstants.achievement_godmode);
		//}

		sheildOn = true;
		//speedBoostEffectOn = true;

		AudioManager.Instance.SpeedBoost ();

		StartCoroutine ("StartSheildFX");

//		if (!RunManager.instance.saveMeOn) {
//			speedBoostAdditionalSpeed = DAO.Settings.PlayerGodModeAdditionalSpeed;
//			//godmodeAdditionalSpeed = 1.5f;
//		}
	}

	public void StartSheildEnd ()
	{
		StartCoroutine ("StopSheildFX");
	}

//	IEnumerator StartSheildEndCoroutine ()
//	{
//		//float startTime = Time.time;
//		while (tornadoParticles.maxParticles > 0 && sheildOn) {
//			tornadoParticles.maxParticles -= 1;
//
//			yield return _waitForLanding;
//		}
//	}


	public void UnSheild ()
	{
		AudioManager.Instance.OnRun ();
		sheildFX.SetActive (false);
		sheildModeDamage = false;
		StartCoroutine ("UnSheildDelay");
	}


	IEnumerator UnSheildDelay ()
	{
		yield return new WaitForSeconds (1);
//		if (!speedBoostEffectOn) {
//			speedBoostOn = false;
//		}
		sheildOn = false;
	}


	public GameObject sheildFX;
	//public ParticleSystem sheildFXParticle;
	//public Color sheildFxColor;
	public IEnumerator StartSheildFX(){
		StopCoroutine ("StopSheildFX");
		//sheildFxColor.a = 1f;   // should be changed
		//sheildFXParticle.startColor = sheildFxColor;
		sheildFX.SetActive (true);
//		while (sheildFxColor.a < 1f) {
//			sheildFxColor.a += 0.01f;
//			Debug.Log ("alon_________ StartSheildFX() - sheildFxColor.a = " + sheildFxColor.a);
//			sheildFXParticle.startColor = sheildFxColor;
//			yield return _waitForEndOfFrame;
//		}
//		sheildFxColor.a = 1f;
//		sheildFXParticle.startColor = sheildFxColor;
		yield return null;
	}


	public IEnumerator StopSheildFX(){
		StopCoroutine ("StartSheildFX");
//		while (sheildFxColor.a > 0f) {
//			sheildFxColor.a -= 0.005f;
//			Debug.Log ("alon_________ StopSheildFX() - sheildFxColor.a = " + sheildFxColor.a);
//			sheildFXParticle.startColor = sheildFxColor;
//			yield return _waitForEndOfFrame;
//		}
//		sheildFxColor.a = 0f;
//		sheildFXParticle.startColor = sheildFxColor;
		sheildFX.SetActive (false);
		UnSheild ();
		yield return null;
	}






	IEnumerator SlowDown (float minSpeed)
	{
		//if (slowingDown)	return;
		StopCoroutine ("SpeedUp");
		//Debug.Log("alon__________ PlayerController - SlowDown() - start - currentSpeed = " + currentSpeed + " , overallspeed = " + overallSpeed);
		slowingDown = true;
		while (currentSpeed > minSpeed) {
			currentSpeed -= 40f * Time.deltaTime;
			//Debug.Log("alon__________ PlayerController - SlowDown() - loop - " + currentSpeed + " , overallspeed = " + overallSpeed);
			yield return _waitForEndOfFrame;
		}
		currentSpeed = minSpeed;

		if (!isDead && !isRunEnd) {
			//StartCoroutine (SpeedUp ());
			//Debug.Log("alon__________ PlayerController - SlowDown() - end - currentSpeed = " + currentSpeed + " , overallspeed = " + overallSpeed);
			StartCoroutine ("SpeedUp");
		}
		else {
			currentSpeed = 0;
			citySilhouetteAnim.speed = 0f;
			isRunning = false;
			//			playerAnimator.SetTrigger ("DeathTrigger");
            print("BECAUSE SLOWDOWN!");
			StartCoroutine (GameOver ());
		}
	}

	IEnumerator SpeedUp ()
	{
		StopCoroutine ("SlowDown");
		//Debug.Log("alon__________ PlayerController - SpeedUp() - start - currentSpeed = " + currentSpeed + " , overallspeed = " + overallSpeed);
		stumbled = false;
		while (currentSpeed < runningSpeed) {
			currentSpeed += 40f * Time.deltaTime;
			//Debug.Log("alon__________ PlayerController - SpeedUp() - loop - " + currentSpeed + " , overallspeed = " + overallSpeed);
			yield return _waitForEndOfFrame;
		}
		currentSpeed = runningSpeed;
		slowingDown = false;
		//StartCoroutine (BackFromCoolDown());
		//Debug.Log("alon__________ PlayerController - SpeedUp() - end - currentSpeed = " + currentSpeed + " , overallspeed = " + overallSpeed);
	}

	public void CollectBall ()
	{

		if (withBall)
			return;

		if (!ballShooted) {
			ballFlyingRigidbody.useGravity = false;
			ballFlyingRigidbody.isKinematic = true;
			ballFlyingRigidbody.velocity = Vector3.zero;
			ballFlyingRigidbody.IsSleeping ();
			ballFlyTransform.parent = ballParent;
			ballFlyTransform.position = ball.transform.position;
			ballFlying.SetActive (false);
		}

		ball.SetActive (true);
		ballAnimation.Play ("ball run anim");

		withBall = true;
		shootTapOn = false;
	}


	public void LaunchBall (GameObject target)
	{
		//Debug.Log ("alon____________ PlayerController - LaunchBall()");
		if (!shootTapOn && !onAction && withBall && target != null) {
			shootTapOn = true;
			StartCoroutine (DisableBallShootedOn());

			if (sliding) {
				SlideEndEvent ();
			}

			ballTarget = target;

			StopCoroutine ("SlowDown");
			StopCoroutine ("SpeedUp");
			StartCoroutine ("SlowDown" , (runningSpeed / 1.3f));
			//playerAnimator.CrossFade("kickAnim" , 0.15f);

			if (ballShooted) {
				StopCoroutine ("BallHandler");

				ballHitTheTarget = false;
				ballFlyingRigidbody.useGravity = false;
				ballFlyingRigidbody.isKinematic = true;
				ballFlyingRigidbody.velocity = Vector3.zero;
				ballFlyingRigidbody.IsSleeping ();

				// bring the flying ball back to the ball position:
				ballFlyTransform.parent = ballParent;
				ballFlyTransform.position = ball.transform.position;
				ballFlying.SetActive (false);
			}

			playerAnimator.Play ("kickAnim");

			DAO.TotalBallShooted++;
			if (DAO.TotalBallShooted == 5) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_goaler);
			} else if (DAO.TotalBallShooted == 25) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_game_changer);
			} else if (DAO.TotalBallShooted == 50) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_striker);
			} else if (DAO.TotalBallShooted == 250) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_world_record);
			} else if (DAO.TotalBallShooted == 500) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_true_gunner);
			}
		} 
	}

	IEnumerator DisableBallShootedOn(){
		yield return _waitForEndOfFrame;
		shootTapOn = false;
	}

	public void CallBallHandler ()
	{   //driven by animation event
		StartCoroutine ("BallHandler");
	}

	IEnumerator BallHandler ()
	{
		ballShooted = true;
		shootTapOn = false;
		withBall = false;

		AudioManager.Instance.OnKick ();

		ball.SetActive (false); // switch between the tow balls
		ballFlying.SetActive (true);

		//GameManager.Instance.SlowMo_Start ();

		ballFlyTransform.parent = null;
		ballFlyTransform.LookAt (ballTarget.transform);
		ballStartTime = Time.time;
		while (!ballHitTheTarget) {
			//currentDis = Vector3.Distance (ballFlyTransformposition, ballTarget.transform.position);
			ballFlyTransform.Translate (Vector3.forward * ballSpeed * Time.deltaTime);
			if (Time.time - ballStartTime > 2f) {
				ballHitTheTarget = true;
			}
			yield return _waitForEndOfFrame;
		}

		ballFlyingRigidbody.isKinematic = false;
		ballFlyingRigidbody.useGravity = true;

		ballFlyingRigidbody.AddForce (-transform.forward * ballSpeed);
		//ballFlyingRigidbody.velocity = -transform.forward * ballSpeed;

		if (!isDead) {
			if (!sliding) {
				playerAnimator.Play ("collect_feedback");
			}
			onAction = false; //let the player move
		}

		// start taking back the ball by disable its phisics
		yield return new WaitForSeconds (1.5f);

		ballHitTheTarget = false;
		ballFlyingRigidbody.useGravity = false;
		ballFlyingRigidbody.isKinematic = true;
		ballFlyingRigidbody.velocity = Vector3.zero;
		ballFlyingRigidbody.IsSleeping ();
		//ballFlyingRigidbody.constraints = RigidbodyConstraints.FreezeAll;

		// bring the flying ball back to the ball position:
		ballFlyTransform.parent = ballParent;
		ballFlyTransform.position = ball.transform.position;
		ballFlying.SetActive (false);
		ballShooted = false;
	}





	IEnumerator GameOver ()
	{
		currentSpeed = 0;
		yield return new WaitForSeconds (1f);
		if (!isRunEnd)
			RunManager.instance.OnPlayerDie ();
		slowingDown = false;
		upperBodyDamage = false;
	}


	public void PrepareToEnd ()
	{
		onAction = true;
		preperToEnd = true;

		if (withBall) {
			withBall = false;
			ball.SetActive (false);
			ballFlying.SetActive (true);
			ballFlyTransform.parent = null;
			ballFlyingRigidbody.isKinematic = false;
			ballFlyingRigidbody.useGravity = true;
		}

		if (sliding) {
			SlideEndEvent ();
		}

		if (jumping) {
			ForceJumpEnd ();
		}

		//playerAnimator.SetTrigger ("victory run");
		//playerAnimator.Play ("Victory_run");

		StartCoroutine (Centralize ());
	}

	public GameObject endGlowFx;

	public void EndRun (bool victory = true)
	{
		if (withBall) {
			withBall = false;
			ball.SetActive (false);
			ballFlying.SetActive (true);
			ballFlyTransform.parent = null;
			ballFlyingRigidbody.isKinematic = false;
			ballFlyingRigidbody.useGravity = true;
		}

		if (sliding) {
			SlideEndEvent ();
		}

		if (jumping) {
			ForceJumpEnd ();
		}

		isRunEnd = true;

		StopCoroutine ("SlowDown");
		StopCoroutine ("SpeedUp");
		//StartCoroutine (SlowDown (0));
		currentSpeed = 0f;
		citySilhouetteAnim.speed = 0f;
		AudioManager.Instance.MuteRun ();
		if (victory) {
			StartCoroutine (FXCamera.instance.EndCameraAnimationWinCoroutine ());
//			if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
//				medal.transform.parent = playerScript.neck;
//				medal.transform.localPosition = Vector3.zero;
//				medal.transform.localEulerAngles = Vector3.zero;
//				medal.transform.localScale = Vector3.one;
//				if (Run.Instance.runGoal.medalsWon () == 1) {
//					medal.GetComponentInChildren<Renderer> ().material.mainTexture = medalTextureSilver;
//					medal.SetActive (true);
//					DAO.TotalSilverMedals++;
//					if (DAO.TotalSilverMedals == 10) {
//						NativeSocial._.ReportAchievment (SocialConstants.achievement_2nd_is_the_new_1st);
//					} else if (DAO.TotalSilverMedals == 50) {
//						NativeSocial._.ReportAchievment (SocialConstants.achievement_mid_table_kings);
//					}
//
//					GameManager.Instance.HandleRankScoreChange ();
//				} else if (Run.Instance.runGoal.medalsWon () == 2) {
//					medal.GetComponentInChildren<Renderer> ().material.mainTexture = medalTextureGold;
//					medal.SetActive (true);
//					DAO.TotalGoldMedals++;
//					if (DAO.TotalGoldMedals == 10) {
//						NativeSocial._.ReportAchievment (SocialConstants.achievement_nice_and_easy);
//					} else if (DAO.TotalGoldMedals == 50) {
//						NativeSocial._.ReportAchievment (SocialConstants.achievement_title_hunter);
//					}
//					GameManager.Instance.HandleRankScoreChange ();
//				} else {
//					medal.SetActive (false);
//					DAO.TotalFailedRuns++;
//				}
//			}

//			playerAnimator.SetTrigger ("victory turn");

//			randomAnim = Random.Range (0, 3);
//
//			switch (randomAnim) {
//			case 0:
//				{
//					playerAnimator.SetFloat ("positive feedback", 0f);
//					break;
//				}
//			case 1:
//				{
//					playerAnimator.SetFloat ("positive feedback", 1f);
//					break;
//				}
//			case 2:
//				{
//					playerAnimator.SetFloat ("positive feedback", 2f);
//					break;
//				}
//			default :
//				{
//					playerAnimator.SetFloat ("positive feedback", 1f);
//					break;
//				}
//			}
//			playerScript.standType = 6;	
//			StartCoroutine (VictoryScene ());
		} else {
			
			playerAnimator.SetTrigger ("DeathTrigger");
//			AudioManager.Instance.PreperToEndRunCrowd (false);
			StartCoroutine (FXCamera.instance.EndCameraAnimationLooseCoroutine ());
		}

		isDead = true;
		currentSpeed = 0f;
	}



	IEnumerator VictoryScene ()
	{

		yield return new WaitForSeconds (0.3f);
		FXCamera.instance.EndCameraAnimation ();
		yield return new WaitForSeconds (0.5f);
		endGlowFx.SetActive (true);
		//endGlowFx.transform.RotateAround (Vector3.forward , 4f);

		//the confety:
		//		yield return new WaitForSeconds (1.0f);
		//		winningEffects [0].transform.position = confety1Pos.position;
		//		winningEffects [0].transform.rotation = confety1Pos.rotation;
		//		winningEffects [0].SetActive (true);
		//		winningEffects [1].transform.position = confety2Pos.position;
		//		winningEffects [1].transform.rotation = confety2Pos.rotation;
		//		yield return new WaitForSeconds (0.25f);
		//		winningEffects [1].SetActive (true);
	}

	public void MovePlayerToCenter ()
	{
		tiltingRight = tiltingLeft = true;
		StartCoroutine (Centralize ());
	}


	IEnumerator Centralize ()
	{

		for (float i = 0f; i <= 1f; i += (Time.deltaTime)) {
			targetPosition = playerCollider.transform.localPosition;
			targetPosition.x = 0;
			;
			playerCollider.transform.localPosition = Vector3.Lerp (playerCollider.transform.localPosition, targetPosition, i);
			if (i >= 0.4f)
				i = 1f;
			yield return _waitForEndOfFrame;
		}

		if (!isDead) {
			targetPosition.x = Mathf.Round (targetPosition.x);
			plTrans.localPosition = targetPosition;

		}

		tiltingRight = tiltingLeft = false;
	}

	float randomXPos;

	IEnumerator MovePlayerAside ()
	{
		int randomAnim = Random.Range (0, 2);
		if (randomAnim == 0) {
			randomXPos = 3f;
		} else {
			randomXPos = -3f;
		}

		for (float i = 0f; i <= 1f; i += (Time.deltaTime)) {
			targetPosition = playerCollider.transform.localPosition;
			targetPosition.x = randomXPos;
			plTrans.localPosition = Vector3.Lerp (plTrans.localPosition, targetPosition, i);
			if (i >= 0.4f)
				i = 1f;
			yield return _waitForEndOfFrame;
		}
		plTrans.localPosition = targetPosition;
	}


	public void ActivateMagnet (bool on)
	{

		if (on) {
			magnetOn = true;
			playerScript.ShowGlove ();
			nearFieldCollider.SetActive (true);
			StartCoroutine (RunSwitcher (-1));
		} else {
			playerScript.HideGlove ();
			nearFieldCollider.SetActive (false);
			if (!speedBoostOn) {
				StartCoroutine (RunSwitcher (0));
			}
			magnetOn = false;
		}
	}


	IEnumerator RunSwitcher (float targetRunType)
	{
		if (currentRunType < targetRunType) {
			while (currentRunType < targetRunType) {
				currentRunType += 4f * Time.deltaTime;
				playerAnimator.SetFloat ("run type", currentRunType);
				yield return _waitForEndOfFrame;
			}
			currentRunType = targetRunType;
		} else if (currentRunType > targetRunType) {
			while (currentRunType > targetRunType) {
				currentRunType -= 4f * Time.deltaTime;
				playerAnimator.SetFloat ("run type", currentRunType);
				yield return _waitForEndOfFrame;
			}
			currentRunType = targetRunType;
		}
	}


	public void GodSphereFX ()
	{
		godFX.SetActive (false);
		godFX.SetActive (true);
	}



	public void MakeHardDamage ()
	{
		Damage (1);
	}

	void DecreaseFitnessOnHardDamage ()
	{
		if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			RunManager.DecreaseFitnessToPlayer = (int)Mathf.Round (data.maxFitness * Perk_ManOfSteelFactor);
		}
	}

	public void InitPerksPower ()
	{

		if (data.PerksDisabled)
			return;

		ProccessPerk (data.Perk_1);
		if (data.Perk_2.type != Perk.PerkType.NONE)
			ProccessPerk (data.Perk_2);
		if (data.Perk_3.type != Perk.PerkType.NONE)
			ProccessPerk (data.Perk_3);


		// PERKS DEBUG

		//		string msg = " Perk 1 | " + data.Perk_1.type +"\n";
		//		msg += 		 "        | Level: " + data.Perk_1.level +"\n";	
		//		msg += 		 "        | Effect: " + data.Perks_Effect [Perk.GetPerkIDByPerkType (data.Perk_1.type)] [data.Perk_1.level - 1] +"\n";	
		//		msg += 		 "\n";
		//
		//		if(data.Perk_2.type != Perk.PerkType.NONE){
		//			msg += 		 " Perk 2 | " + data.Perk_2.type +"\n";
		//			msg += 		 "        | Level: " + data.Perk_2.level +"\n";	
		//			msg += 		 "        | Effect: " + data.Perks_Effect [Perk.GetPerkIDByPerkType (data.Perk_2.type)] [data.Perk_2.level - 1] +"\n";	
		//			msg += 		 "\n";
		//		}
		//
		//		if (data.Perk_3.type != Perk.PerkType.NONE) {
		//			msg += " Perk 3 | " + data.Perk_3.type + "\n";
		//			msg += "        | Level: " + data.Perk_3.level + "\n";	
		//			msg += "        | Effect: " + data.Perks_Effect [Perk.GetPerkIDByPerkType (data.Perk_3.type)] [data.Perk_3.level - 1] + "\n";	
		//			msg += "\n";
		//		}
		//
		//		msg += 		 "--------------------------";
		//
		//
		//		string msg2 = "Perks Effect:\n";
		//		msg2 += "Perk_ChanceToMeetMagnetFactor: " + Perk_ChanceToMeetMagnetFactor +"\n";
		//		msg2 += "Perk_JumpHeightFactor: " + Perk_JumpHeightFactor +"\n";
		//		msg2 += "Perk_AdditionalSoftHits: " + Perk_AdditionalSoftHits +"\n";
		//		msg2 += "Perk_ManOfSteelFactor: " + Perk_ManOfSteelFactor +"\n";
		//		msg2 += "Perk_FitnessFactor: " + Perk_FitnessFactor +"\n";
		//		msg2 += "Perk_MagnetTimeFactor: " + Perk_MagnetTimeFactor +"\n";
		//		msg2 += "Perk_EnergyTimeFactor: " + Perk_EnergyTimeFactor +"\n";
		//		msg2 += "Perk_MoneyBonusIfRunCompleted: " + Perk_MoneyBonusIfRunCompleted +"\n";
		//

		if (Perk_JumpHeightFactor < 1f)
			Perk_JumpHeightFactor = 1f;

	}

	void ProccessPerk (Perk prk)
	{

		switch (prk.type) {
		case Perk.PerkType.DODGER:
			{
				Perk_AdditionalSoftHits = data.Perks_Effect [Perk.GetPerkIDByPerkType (Perk.PerkType.DODGER)] [prk.level - 1].AsInt;
				break;
			}
		case Perk.PerkType.ENERGY_FIEND:
			{
				Perk_EnergyTimeFactor = data.Perks_Effect [Perk.GetPerkIDByPerkType (Perk.PerkType.ENERGY_FIEND)] [prk.level - 1].AsFloat;
				break;
			}
		case Perk.PerkType.FANS_FAVORITE:
			{
				Perk_MoneyBonusIfRunCompleted = data.Perks_Effect [Perk.GetPerkIDByPerkType (Perk.PerkType.FANS_FAVORITE)] [prk.level - 1].AsInt;
				break;
			}
		case Perk.PerkType.JUMPER:
			{
				//				jumpPerkFactor = 1f;
				Perk_JumpHeightFactor = data.Perks_Effect [Perk.GetPerkIDByPerkType (Perk.PerkType.JUMPER)] [prk.level - 1].AsFloat;
				//				switch (prk.level){
				//				case 1:
				//					{
				//						jumpPerkFactor = Perk_JumpHeightFactor;
				//						break;
				//					}
				//				case 2:
				//					{
				//						jumpPerkFactor = 1.1f;
				//						break;
				//					}
				//				case 3:
				//					{
				//						jumpPerkFactor = 1.22f;
				//						break;
				//					}
				//				default:
				//					{
				//						jumpPerkFactor = 1f;
				//						break;
				//					}
				//				}
				break;
			}
		case Perk.PerkType.MAGNETIC:
			{
				Perk_ChanceToMeetMagnetFactor = Perk_ChanceToMeetMagnetFactor * data.Perks_Effect [Perk.GetPerkIDByPerkType (Perk.PerkType.MAGNETIC)] [prk.level - 1].AsFloat;
				break;
			}
		case Perk.PerkType.MAN_OF_STEEL:
			{
				Perk_ManOfSteelFactor = data.Perks_Effect [Perk.GetPerkIDByPerkType (Perk.PerkType.MAN_OF_STEEL)] [prk.level - 1].AsFloat;
				break;
			}
		case Perk.PerkType.SUPER_MAGNET:
			{
				Perk_MagnetTimeFactor = data.Perks_Effect [Perk.GetPerkIDByPerkType (Perk.PerkType.SUPER_MAGNET)] [prk.level - 1].AsFloat;
				break;
			}
		}

	}



	float SpeedBeforePause;

	public void Pause ()
	{
		SpeedBeforePause = currentSpeed;
		//StartCoroutine(_pause());

		currentSpeed = 0f;
		playerAnimator.speed = 0f;
		ballAnimation.speed = 0f;
		GameManager.TutorialPaused = true;
	}

	public void Unpause ()
	{
		//StartCoroutine(_unpause());

		currentSpeed = SpeedBeforePause;
		playerAnimator.speed = 1f;
		ballAnimation.speed = 1f;
		GameManager.TutorialPaused = false;
	}

	IEnumerator _pause ()
	{
		while (currentSpeed > 0.01) {
			currentSpeed = Mathf.Lerp (currentSpeed, 0f, 10f * Time.deltaTime);
			playerAnimator.speed = Mathf.Lerp (playerAnimator.speed, 0f, 5f * Time.deltaTime);
			ballAnimation.speed = Mathf.Lerp (ballAnimation.speed, 0f, 5f * Time.deltaTime);
			yield return null;
		}

		currentSpeed = 0f;
		playerAnimator.speed = 0f;
		ballAnimation.speed = 0f;

		GameManager.TutorialPaused = true;
	}

	IEnumerator _unpause ()
	{

		while (currentSpeed < SpeedBeforePause - 0.01f) {
			currentSpeed = Mathf.Lerp (currentSpeed, SpeedBeforePause, 20f * Time.deltaTime);
			playerAnimator.speed = Mathf.Lerp (playerAnimator.speed, 1f, 10f * Time.deltaTime);
			ballAnimation.speed = Mathf.Lerp (ballAnimation.speed, 1f, 10f * Time.deltaTime);
			yield return null;
		}

		currentSpeed = SpeedBeforePause;
		playerAnimator.speed = 1f;
		ballAnimation.speed = 1f;

		GameManager.TutorialPaused = false;
	}



	public void IncreaseSpeed ()
	{
		SectionsIndex++;
		if (SectionsIndex >= numberOfSections && runningSpeed < increaseMaxSpeed) {
			SectionsIndex = 0;
			increaseAdditionalSpeed += speedIncreeseFactor;
			currentSpeed += speedIncreeseFactor;
			runningSpeed += speedIncreeseFactor;
			runningSpeedMirror += speedIncreeseFactor;
			//Debug.Log ("alon_____________ IncreaseSpeed() - start ");

			if (GameManager.curentGameState == GameManager.GameState.INFINITY_RUN) {
				InfinityMapBuilder.instance.DecreaseWaitInterval ();
			}
			//			else if (GameManager.curentGameState == GameManager.GameState.CUP_RUN) {
			//				CupMapBuilder.Instance.DecreaseWaitInterval();
			//			}
		}
	}





	Vector3 playerPos;
	Vector3 playerDirection;

	public void FixedPlayerXPosition ()
	{
		playerDirection = transform.forward;
		playerPos = transform.position;
		if (Mathf.Round (playerDirection.z) != 0f) {
			playerPos.x = Mathf.Round (playerPos.x);
			transform.position = playerPos;
		} else if (Mathf.Round (playerDirection.x) != 0f) {
			playerPos.z = Mathf.Round (playerPos.z);
			transform.position = playerPos;
		}
	}



	public void EndSceneCannonEvent(Transform playerLocation){
		//player.transform.position = playerLocation.position;
		player.transform.parent = playerLocation;
		player.transform.localPosition = Vector3.zero;
		player.transform.localEulerAngles = Vector3.zero;
		playerAnimator.CrossFade ("Winning_Scene_Cannon_Anim", 0.06f);
	}

	public void EndSceneShootingEvent(Transform playerLocation){
		//plTrans.position = playerLocation.position;
		player.transform.parent = playerLocation;
		player.transform.localPosition = Vector3.zero;
		player.transform.localEulerAngles = Vector3.zero;
		playerAnimator.CrossFade ("Winning_Scene_ball_Player_Anim", 0.06f);
	}




}
