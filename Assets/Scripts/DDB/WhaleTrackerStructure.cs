using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;
using Facebook.Unity;
using System;

[DynamoDBTable("ars_bd_whale_tracker")]
public class WhaleTrackerStructure{

	[DynamoDBHashKey] 
	public string whale_id { get; set; }

	[DynamoDBProperty]
	public string user_id { get; set; }

	[DynamoDBProperty]
	public string firstName{ get; set; }

	[DynamoDBProperty]
	public string lastName{ get; set; }

	[DynamoDBProperty]
	public string email{ get; set; }

	[DynamoDBProperty]
	public int numberOfIAP{ get; set; }

	[DynamoDBProperty]
	public float revenue { get; set; }

	[DynamoDBProperty]
	public float revenueInUSD { get; set; }

	[DynamoDBProperty]
	public string ftd { get; set; }

	[DynamoDBProperty]
	public string lastLoginDate { get; set; }

	[DynamoDBProperty]
	public int isFBConnected { get; set; }

	[DynamoDBProperty]
	public string currency { get; set; }

	[DynamoDBProperty]
	public string storeType { get; set; }

	[DynamoDBProperty]
	public int coinsAmount { get; set; }

	[DynamoDBProperty]
	public string playersAmount{ get; set; }

	[DynamoDBProperty]
	public int maxRunID { get; set; }

	[DynamoDBProperty]
	public int totalMeters{ get; set; }

	// -----------------------------------------------------------------------------

	public WhaleTrackerStructure(){
		DateTime dtNow = DateTime.Now;
		if (String.IsNullOrEmpty (DAO.whaleID)) {
			DAO.createWhaleID ();
		}

		whale_id = DAO.whaleID;

		if (FB.IsLoggedIn) {
			isFBConnected = 1;
			user_id = UserData.Instance.userID;
		} else {
			isFBConnected = 0;
			user_id = "";
		}
	//	sDate = dtNow.ToString("yyyyMMdd");
	//	sDateTime = dtNow.ToString("yyyyMMddHHmmss");
	//	System.DateTime epochStart = new System.DateTime(2000, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
//		date = (int)(dtNow - epochStart).TotalSeconds;
		#if UNITY_ANDROID
		storeType = DAO.Instance.GoogleStore;
		#elif UNITY_IOS 
		storeType = DAO.Instance.IOSStore;
		#endif
		coinsAmount = DAO.TotalCoinsCollected;
		ftd = DAO.Ftd;
		playersAmount = DAO.NumOfPurchasedPlayers.ToString ();
		totalMeters = (DAO.BILevelUp * 1000) + DAO.BIDistance;
		email = DAO.Email;
		firstName = DAO.FirstName;
		lastName = DAO.LastName;
		numberOfIAP = DAO.IAPPoints;
		lastLoginDate = dtNow.ToString ("yyyyMMdd");
		maxRunID = DAO.maxRunID;
		revenueInUSD = 0;

		//totalMeters = DAO.
			
	}
	/*
	public void ReplaceLocalData(){
		DAO.IAPPoints = iapPoints;
		DAO.PlayTimeInMinutes = playTimeInMinutes;
		DAO.TotalCoinsCollected = totalCoins;
		DAO.TotalMagnetsCollected = totalMagnets;
		DAO.TotalEnergyDrinksCollected = totalEnergyDrinks;
		DAO.TotalGiftCardsCollected = totalGiftCards;
		//if(totalMedikKits != null) DAO.TotalMedikKitsCollected = totalMedikKits;
		DAO.TotalMedikKitsCollected = totalMedikKits;
		DAO.ActiveRunID = activeCupId;
		DAO.ActiveRunData = activeCupData;
		DAO.CompletedRunsData = completedRunsData;
		DAO.WinnedCups = winnedCups;
		DAO.PurchasedPlayers = purchasedPlayers;
		DAO.OpenedRegions = openedRegions;

		for(int i=0; i<players.Count; i++){
			DAO.Instance.LOCAL.playerData [i] = players [i].data;
		}

		DAO.BestPracticeScore = highScore;
		DAO.TotalPracticeTime = trainingTime;
		DAO.TotalGoldMedals = totalGoldMedals;
		DAO.TotalSilverMedals = totalSilverMedals;
		DAO.TotalFailedRuns = totalFailedRuns;
		DAO.RankScore = rankScore;

		if (!string.IsNullOrEmpty(userType)) {
			DAO.UserType = (UserData.UserType)Enum.Parse (typeof(UserData.UserType), userType);
			UserData.UpdateUserType (DAO.UserType);
		} else {
			DAO.UserType = UserData.UserType.NEW_USER;
			UserData.UpdateUserType (DAO.UserType);
		}

		DAO.Email = email;
		DAO.Gender = gender;
		DAO.FirstName = firstName;
		DAO.LastName = lastName;
        DAO.Lang = lang;
		DAO.Country = country;
		DAO.CanSendEmail = (canSendEmail == 1);

//		switch (userType) {
//		//case "
//		}

//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;
//		UserData.UserType.ENGAGED_A;

		DAO.TutorialState = 5;
		DAO.Instance.LOCAL.SaveAll (false);

		DAO.BILevelUp = bILevelUp;

		CrossSceneUIHandler.Instance.UpdateCoinsAmount ();

		GameManager.Instance.ReadRegions ();
		GameManager.Instance.ReadWinnedCups ();
	}
*/

}
	
/*
public class PlayerItem{
	[DynamoDBProperty]
	public string name;
	[DynamoDBProperty]
	public string data;

	public void Set(int index){
		this.data = DAO.Instance.LOCAL.playerData[index];
		name = PrefabManager.instanse.PlayerStructures [index].PlayerName;
	}

}
*/