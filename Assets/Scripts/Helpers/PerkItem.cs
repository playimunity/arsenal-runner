﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;
using RTLService;

public class PerkItem : MonoBehaviour {

	public bool owned = false;
	public Perk.PerkType type;
	public int level;
	public int skillNum;

	public Text levelText;
	public Button CTAButton;
	public Image PerkIcon;

	public Text PerkName;
	public Text Desc_1;
//	public Text Desc_2;
//	public Text Desc_3;

	public Image levelrHombus1;
	public Image levelrHombus2;
	public Image levelrHombus3;

	public Text priceText;
	//public Text BuyPrice;
	public Text upgradeBtnTxt;
	public Text getBtnTxt;

	public Color ColorOwned;
	public Color ColorUnowned;

	public GameObject upgradeArea;
	public GameObject getArea;

	public GameObject BG_Blue;
	public GameObject BG_Black;

	public Perk TMP_Perk;

	public ButtonClickAnimation BCAnimation;

	JSONNode perkJson;


	public void SetGenericView(string id){
//		if (id == "nrg" || id == "smagnet") {
//			return;
//		}
		type = Perk.GetPerkTypeByPerkID(id);

//		foreach (Sprite icon in MainScreenUiManager.instance.skillsIcons) {
//			Debug.Log ("alon_______ perk icon name: " + icon.name + " , type name: " + type.ToString ());
//			if (icon.name == type.ToString ()) {
//				PerkIcon.sprite = icon;
//			}
//		}
		PerkIcon.sprite = MainScreenUiManager.instance.GetSkillIconBySkillType( type );

		perkJson = DAO.Language.Perks["prk_" + id];

		PerkName.text = Utils.FixRTLString(perkJson ["name"].Value);

		Desc_1.text = "" + perkJson ["desc_1"].Value;
		//Desc_1.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + Perk.GetPerkIDByPerkType( pl.DefaultPerk )]["desc_1"]);
		//Desc_1.text = Utils.FixRTLString(DAO.Language.Perks["prk_" + id]["desc_1"]);

//		Desc_1.text = Utils.FixRTLString(DAO.Language.GetString("level")) + " 1 - " + perkJson ["desc_1"].Value;
//		Desc_2.text = Utils.FixRTLString(DAO.Language.GetString("level")) + " 2 - " + perkJson ["desc_2"].Value;
//		Desc_3.text = Utils.FixRTLString( DAO.Language.GetString("level")) + " 3 - " + perkJson ["desc_3"].Value;

		upgradeBtnTxt.text = DAO.Language.GetString ("upgrade");
		getBtnTxt.text = DAO.Language.GetString ("get");
	}


	public void SetPlayerCpecificView(Player pl){

		TMP_Perk = pl.data.GetPerkIfPurchased (this.type);
		if (TMP_Perk.type != Perk.PerkType.NONE) {
			owned = true;
			//Debug.Log ("alon_______ Perk Is Owned!   -   perk: " + TMP_Perk.type.ToString());
		} else {
			owned = false;
			//Debug.Log ("alon_______ Perk Is NOT Owned   -   perk: " + TMP_Perk.type.ToString());
		}

		if (owned) {
			level = TMP_Perk.level;

			Desc_1.text = "" + perkJson ["desc_"+level].Value;

			if(level < 3){
				if(level == 1) priceText.text = DAO.Settings.PerkUpgrade2Cost.ToString();
				else priceText.text = DAO.Settings.PerkUpgrade3Cost.ToString();
				SetOwnedView ();

				CTAButton.onClick.RemoveAllListeners();
				CTAButton.onClick.AddListener(()=>{
					BCAnimation.OnClicked();
					AudioManager.Instance.OnBTNClick();
					TMP_Perk.type = type;
					TMP_Perk.level = level;
					PlayerChooseManager.instance.OnPerkUpgradeClicked(TMP_Perk);
				});

			}else{
				SetOwnedView (false);
				CTAButton.onClick.RemoveAllListeners();
			}

			PerkIcon.sprite = MainScreenUiManager.instance.GetSkillIconBySkillType( type );

		} else {
			
			level = 1;

			PerkIcon.sprite = MainScreenUiManager.instance.GetSkillIconBySkillType( type,false );

			skillNum = pl.data.GetNumOfPurchasedPerks() + 1;

			if(skillNum == 2) priceText.text = DAO.Settings.Perk2Cost.ToString();
			else priceText.text = DAO.Settings.Perk3Cost.ToString();

			SetUnownedView ();

			CTAButton.onClick.RemoveAllListeners();
			CTAButton.onClick.AddListener(()=>{
				BCAnimation.OnClicked();
				AudioManager.Instance.OnBTNClick();
				TMP_Perk.type = type;
				TMP_Perk.level = level;
				PlayerChooseManager.instance.OnPerkBuyClicked(TMP_Perk);
			});
		}

		levelText.text = DAO.Language.GetString("level");

		switch (level) {
			case 1:
			{
				levelrHombus1.color = ColorOwned;
				levelrHombus2.color = ColorUnowned;
				levelrHombus3.color = ColorUnowned;
				break;
			}
			case 2:
			{
				//levelText.text = DAO.Language.GetString("level") + " 2";
				levelrHombus1.color = ColorOwned;
				levelrHombus2.color = ColorOwned;
				levelrHombus3.color = ColorUnowned;
				break;
			}
			case 3:
			{
				//levelText.text = DAO.Language.GetString("level") + " 3";
				levelrHombus1.color = ColorOwned;
				levelrHombus2.color = ColorOwned;
				levelrHombus3.color = ColorOwned;
				break;
			}
		}

		if (!owned) {
			levelrHombus1.color = ColorUnowned;
		}

	}



	void SetOwnedView(bool canUpgrade = true){

		BG_Blue.SetActive (true);
		BG_Black.SetActive (false);

		if (canUpgrade) {
			getArea.SetActive (false);
			upgradeArea.SetActive (true);
		} else {
			getArea.SetActive (false);
			upgradeArea.SetActive (false);
		}

		// Colors
		//PerkIcon.color = ColorOwned;
		//PerkName.color = ColorOwned;
		//levelText.color = ColorOwned;
//		levelrHombus1.color = ColorOwned;
//		levelrHombus2.color = ColorOwned;
//		levelrHombus3.color = ColorOwned;

	}

	void SetUnownedView(){
		BG_Blue.SetActive (false);
		BG_Black.SetActive (true);

		getArea.SetActive (true);
		upgradeArea.SetActive (false);

		// Colors
		//PerkIcon.color = ColorUnowned;
		//PerkName.color = ColorUnowned;
		//levelText.color = ColorUnowned;
		levelrHombus1.color = ColorUnowned;
		levelrHombus2.color = ColorUnowned;
		levelrHombus3.color = ColorUnowned;



	}


}
