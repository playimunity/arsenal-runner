﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class SceneTransitionContainer : MonoBehaviour {

	public StringProvider_CrossScene cssp;
	public GameObject transitionObject;
	public Animator transitionAnimator;
	public Text PreloaderTips;



	WaitForSeconds waitSecond = new WaitForSeconds(0.5f);

	bool ccShown = false;

	//public List


	// ----------------------------------------------------------
	public Text loading;
	public static Text staticLoading;
	static SceneTransitionContainer _instance;
	public static SceneTransitionContainer Instance{
		get{ return _instance; }
	}

	// ====================================================================================

	void Awake () {
		_instance = this;

		transitionObject.SetActive (true);

		staticLoading = loading;
		//waitSecond = new WaitForSeconds(0.5f);


	}


	void OnLevelWasLoaded(int level) {
        print("Level was loaded");
		if (level != 0) {
			StartCoroutine(hideTransitionAfterSecond());
			//if(GameManager.curentSubGameState == GameManager.GameSubState.NO)  showCoinsCounter ();
            
			GameManager.RequestUnpause ();
		}
	}

	IEnumerator hideTransitionAfterSecond(){
		yield return waitSecond;
		hideTransition ();
	}

	// ====================================================================================



//	public void SetStrings(){
//		cssp.SetStrings ();
//	}



	public void showTransition(){
		PreloaderTips.text = "";
		PreloaderTips.text = DAO.Language.GetRandomTip ();


		//hideCoinsCounter ();
		transitionObject.SetActive (true);
		transitionAnimator.Play ("scene_transition_show");
	}

	public void hideTransition(){
		Preloader.Instance.FillLoadingBar (10,"end");
		//Preloader.Instance.LoadingBar.SetActive (false);
		transitionAnimator.Play ("scene_transition_hide");
	}


}










