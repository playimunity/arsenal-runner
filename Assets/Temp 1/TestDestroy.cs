﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestDestroy : MonoBehaviour {

	public List<GameObject> Objects;
	public List<GameObject> InstantiatedObjs;

	GameObject TMP_OBJ;
	Vector3 randomPos;

	int RandomObj;

	void Start () {
		randomPos = transform.position;

		InvokeRepeating ("RemoveBulkOfObjects", 0f, 0.5f);
	}


	void RemoveBulkOfObjects(){
		if (InstantiatedObjs.Count == 0 || InstantiatedObjs == null) {
			CreateBulkOfObjects ();
			return;
		};

		for (int i = 0; i < 10; i++) {
			RandomObj = Random.Range (0, InstantiatedObjs.Count);
			//Destroy (InstantiatedObjs [RandomObj]);
			InstantiatedObjs [RandomObj].SetActive (false);
			InstantiatedObjs.RemoveAt (RandomObj);
		}
	}


	void CreateBulkOfObjects(){
		InstantiatedObjs = new List<GameObject> ();

		for (int i = 0; i < 100; i++) {
			randomPos.x = (float)Random.Range (-25f, 25f);
			randomPos.z = (float)Random.Range (-25f, 25f);
			TMP_OBJ = (GameObject)Instantiate (Objects [Random.Range (0, Objects.Count)], randomPos, transform.rotation);
			InstantiatedObjs.Add(TMP_OBJ);
		}

	}


}
