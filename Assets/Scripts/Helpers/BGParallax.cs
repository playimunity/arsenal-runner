﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BGParallax : MonoBehaviour {

	public float XRotation;
	Vector2 BGPos;
	Vector2 BGSize;

	Image BG;
	public RectTransform _Canvas;

	[Range(-1f,1f)]
	public float AccelerationSimulation = 0f;

	void Start () {
		BG = GetComponent<Image> ();

		BGPos = BG.rectTransform.anchoredPosition;
		BGSize = BG.rectTransform.sizeDelta;

		BGSize.x = _Canvas.sizeDelta.x + 40f;
		BGSize.y = BGSize.x * 2f;

		BG.rectTransform.sizeDelta = BGSize;


	}
	

	void Update () {
		BGPos.x = XRotation;
	
#if UNITY_EDITOR
		BGPos.x = (Mathf.Round (AccelerationSimulation * 100f) / 100f) * 20f;
#else

		BGPos.x = (Mathf.Round (Input.acceleration.x * 100f) / 100f) * 20f;
#endif

		BG.rectTransform.anchoredPosition = BGPos;
	}
}
