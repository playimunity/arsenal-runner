using UnityEngine;
using System.Collections;

public class Coin : Collectable {

	public static Coin instance;

	public GameObject meshParent;
	public GameObject meshObject;
	public GameObject x2MeshParent;
	public Transform x2Mesh1;
	public Transform x2Mesh2;
	public Transform meshTransform;
	//public GameObject FlarePlane;

	bool moveToPlayer = false;
	bool coinCollided = false;
	public static bool isX2 = false;
	public bool x2Test = false;

	Vector3 coinMaxSize;
	Vector3 coinPos;

	float rotateFactor = 5f;


	static WaitForSeconds _wfs;
	static WaitForSeconds wfs{
		get{
			if( _wfs == null ) _wfs = new WaitForSeconds (0.04f);
			return _wfs;
		}
	}

	void Awake(){
		instance = this;
	}

	Transform glove;


	void Start () {
		glove = PlayerController.instance.playerScript.glove.transform;
		//meshTransform = meshObject.transform;

		coinPos = transform.position;

		if (x2Test) {
			isX2 = true;
		} else {
			isX2 = false;
		}

		//coinMaxSize = meshTransform.localScale;
		coinMaxSize = Vector3.one;
		//type = CollectableTypes.COIN;

		if (RunManager.instance.X2CoinsIsActive) {
			isX2 = true;
		} else {
			isX2 = false;
		}
	}

	void OnDisable(){
		coinCollided = false;
		moveToPlayer = false;
		x2Test = false;
		if (meshParent != null) {
			meshParent.transform.localScale = Vector3.one;
			meshParent.SetActive (true);
		}
		if (CoinPool._ != null)
			CoinPool._.ReturnCoin (gameObject);
	}

	void OnEnable(){
		//FlarePlane.SetActive (true);
		StartCoroutine ( Rotate() );

		coinMaxSize = Vector3.one;
		x2Mesh1.localScale = coinMaxSize;
		x2Mesh2.localScale = coinMaxSize;
		meshTransform.localScale = coinMaxSize;

		if (x2Test) {
			isX2 = true;
		} else {
			isX2 = false;
		}

		if (RunManager.instance.X2CoinsIsActive) {
			isX2 = true;
		} else {
			isX2 = false;
		}

//		StartCoroutine(DefinePosition ());
	}

	IEnumerator Rotate(){

		while (true) {
			yield return wfs;

			if (moveToPlayer) {
				//if(FlarePlane.activeSelf) FlarePlane.SetActive (false);
				transform.position = Vector3.MoveTowards(transform.position, glove.position , 2f);

				if (coinMaxSize.x > 0.3f) {
					coinMaxSize.x -= 0.1f;
					coinMaxSize.y -= 0.1f;
					coinMaxSize.z -= 0.1f;
					if (isX2) {
						x2Mesh1.localScale = coinMaxSize;
						x2Mesh2.localScale = coinMaxSize;
					} else {
						meshTransform.localScale = coinMaxSize;
					}
				}
			}

			if (isX2) {
				x2MeshParent.SetActive (true);
				meshObject.SetActive (false);
				x2Mesh1.Rotate (0f, rotateFactor, 0f);
				x2Mesh2.Rotate (0f, rotateFactor, 0f);
			} else {
				meshObject.SetActive (true);
				x2MeshParent.SetActive (false);
				meshTransform.Rotate (0f, rotateFactor, 0f );
			}

		}

	}

	void OnTriggerEnter(Collider col){
		if (!coinCollided && col.transform.gameObject.tag == "Player") {
			
			coinCollided = true;

			//rotateFactor = 4f;

			if (isX2) {
				RunManager.OnCoinCollected (2);
				AudioManager.Instance.DoubleCoin();
			} else {
				RunManager.OnCoinCollected (1);
				AudioManager.Instance.OnCoinPick();
			}

			if (moveToPlayer) {
				RunManager.instance.CoinCounter ();
			}

			ParticleCoinGenerator.instance.PlayParticle(transform.position , transform.eulerAngles);

			gameObject.SetActive (false);
		} 

		if (RunManager.instance.MagnetIsActive && !moveToPlayer && col.transform.gameObject.tag == "PlayerNearField") {
			moveToPlayer = true;
			//coinAnimator.Play ("coin_anim");
		}
	}

//	bool raycasting = true;
//	RaycastHit hit;
//	Vector3 dir = new Vector3(0,-1,0);
//	float dis = 30f;

//	IEnumerator DefinePosition(){
//		Debug.Log ("alon_________ coin position: " + transform.position);
//		yield return new WaitForSeconds (1f);
//		Debug.Log ("alon_________ coin position: " + transform.position);
//		while (raycasting) {
//			yield return new WaitForEndOfFrame ();
//			Debug.DrawRay(transform.position,dir * dis,Color.green);
//			if (Physics.Raycast (transform.position, dir, out hit, dis)) {
//				Debug.Log ("alon_________ hit.collider.tag");
//				if (hit.collider.tag == "Section") {
//					transform.position = hit.point;
//					raycasting = false;
//				}
//			}
//		}
//	}
		

}
