﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

public class MedikKits_Actions : MonoBehaviour {

	public static MedikKits_Actions Instance;
    public List<Text> energyPrices;
	Player pl;

	public Button useBtn;
	public Text EnergyCount;

	void Start () {
		Instance = this;
        for (int i = 0; i < energyPrices.Count; i++)
        {
            energyPrices[i].text = DAO.Settings.MedikKits[i]["p"].AsInt.ToString();//.ToString();
        }
	}

	void OnEnable(){
		pl = PlayerChooseManager.instance.currentPlayerScript;
		RefreshUI ();
	}

	public void RefreshUI(){
		EnergyCount.text = "" + DAO.TotalMedikKitsCollected;

		if (DAO.TotalMedikKitsCollected <= 0 || pl.data.curHealthState == PlayerData.HealthState.ENERGETIC) {
			useBtn.interactable = false;
			useBtn.GetComponent<Image> ().color = Color.gray;
		} else {
			useBtn.interactable = true;
			useBtn.GetComponent<Image> ().color = Color.white;
		}
	}


	public void OnUseClicked(){
		MainScreenUiManager.instance.MidekKitsScreenToggle (false);
		MainScreenUiManager.instance.LowEnergyPopup (false);
		if (pl.data.curHealthState == PlayerData.HealthState.WOUNDED) {
			NativeSocial._.ReportAchievment (SocialConstants.achievement_doctor_who);
			DAO.WoundedHealed++;
			if (DAO.WoundedHealed == 10) {
				NativeSocial._.ReportAchievment (SocialConstants.achievement_doctor_you);
			}
		}

		pl.data.AddFitnes(30);
		pl.InvokeFitnessUpdate ();
		pl.UpdateFitness ();
		PlayerChooseManager.instance.CheckFitnessOfSelectedPlayer ();

		if (Tutorial.IsNeedToShowHealPopup) {
			UnitedAnalytics.LogEvent ("health tutroail", "bought", UserData.Instance.userType.ToString (), 30);
		} else {
			UnitedAnalytics.LogEvent ("Medkit bought", "purchased", UserData.Instance.userType.ToString (), 30);
		}

		DAO.TotalMedikKitsCollected--;
		RefreshUI ();

	}

	public void OnGetClicked(){
		CrossSceneUIHandler.Instance.showStore (CrossSceneUIHandler.ShopStates.ENERGY);
	}

	public void BuyEnergy(int id){
		CrossSceneUIHandler.Instance.OnBuyMedicKitClicked (id);
		RefreshUI ();
	}

}
