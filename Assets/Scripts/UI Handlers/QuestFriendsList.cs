﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class QuestFriendsList : MonoBehaviour {

	public GameObject FriendItemPrefab;
	public RectTransform ContentRect;

	Vector2 contentSize;
	Vector2 nextItemPosition;
	GameObject TMPOBJ;

	float ItemHeight = (140f+30f);



	public void SetFriends(List<FBFriend> friends){
		nextItemPosition = new Vector3 (317f, -70f, 0f);
		ContentRect.sizeDelta = Vector2.zero;

		foreach (Transform child in ContentRect) {
			GameObject.Destroy(child.gameObject);
		}

		foreach (FBFriend friend in friends) {
			AddItem (friend.photo, friend.name);
		}
	}


	public void AddItem(Texture img, string name){
		contentSize = ContentRect.sizeDelta;
		contentSize.y += ItemHeight;
		ContentRect.sizeDelta = contentSize;



		TMPOBJ = (GameObject)Instantiate (FriendItemPrefab , transform.position , transform.rotation);
		TMPOBJ.GetComponentInChildren<Text>().text = name;
		TMPOBJ.GetComponentInChildren<RawImage> ().texture = img;
		TMPOBJ.GetComponent<RectTransform> ().anchoredPosition = nextItemPosition;
		TMPOBJ.transform.SetParent (ContentRect.transform , false);

		nextItemPosition.y -= ItemHeight;
	}
		

}
