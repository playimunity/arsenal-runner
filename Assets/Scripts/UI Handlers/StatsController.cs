using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Facebook.Unity;
using System;

public class StatsController : MonoBehaviour {
	
	public static StatsController Instance;

	public GameObject statsScreen;

	public Animator uiAnimator;

	bool statsScrnOn = false;

	public GameObject shareCameraObj;

	public GameObject favoritePlayerParent;
	public GameObject favoritePlayerShareParent;

//	public Image myStatsBtn;
//	public Image friendsStatsBtn;

	public RawImage userImageShare;

	public Text completedCups;
	public Text completedCupsShare;

	public Button MyStatsBtn;
	public Text myStatsBtnTxt;
	public Button FriendStatsBtn;
	public Text friendStatsBtnTxt;
	public Button ShareBtn;

	//public Text rankText;
	//public Text rankTextShare;

	//string foundationDate = "25/10/2015";
	public Text foundationDateText;
	public Text foundationDateTextShare;

	public Text teamSizeText;
	public Text teamSizeTextShare;

	//public Dictionary<string , GameObject> rankings;

	public Text teamMaxSizeText;
	public Text teamMaxSizeTextShare;

	public GameObject[] ownedPlayers;
	public GameObject[] ownedPlayersShare;
	public GameObject[] emptyPlayers;
	public GameObject[] emptyPlayersShare;

	public Color greyColor;

	public Text totalMetersText;
	public Text totalMetersTextShare;

	public Text totalPractiseText;
	public Text totalPractiseTextShare;

//	public GameObject[] cupItems;
//	public GameObject[] cupItemsShare;

	public Text goldMedalsText;
	public Text goldMedalsTextShare;

	public Text silverMedalText;
	public Text silverMedalShare;

	public Text failedValueText;
	public Text failedValueTextShare;

	public Image favoritePlayerImage;
	public Image favoritePlayerImageShare;

	public Text favoritePlayerNumText;
	public Text favoritePlayerNumTextShare;

	public Text favoritePlayerNameText;
	public Text favoritePlayerNameTextShare;

	public Text totalDistanceText;
	public Text totalDistanceTextShare;

	public Text levelText;
	public Text levelTextShare;

	//string[] ownedSkills;

	[Serializable]
	public struct SkillsImages {
		public string name;
		public GameObject image;
		public GameObject imageShare;
	}

	public SkillsImages[] skillImgs;
	//public SkillsImages[] skillImgsShare;


//	public Text perk1Share;
//	public Text perk2Share;
//	public Text perk3Share;

	int teamSize;
	int teamMaxSize = DAO.Instance.teamMaxSize;
	public bool isUserPictureReady;



	public RankingObject[] rankingObjs;
	//public RankingObject[] rankingObjsShare;


	void Awake(){
		Instance = this;

	}


	public void StatsScrnAnimTogge(bool on){
		if (on && !statsScrnOn) {
			statsScreen.SetActive (true);
			uiAnimator.Play ("stats_scrn_in");
			statsScrnOn = true;
			GM_Input._.Stats = true;
		} else if (!on && statsScrnOn) {
			uiAnimator.Play ("stats_scrn_out");
			StartCoroutine (DeActivateStatsScrn());
			statsScrnOn = false;
			GM_Input._.Stats = false;
		}
	}
	IEnumerator DeActivateStatsScrn(){
		yield return new WaitForSeconds (1f);
		statsScreen.SetActive (false);
	}


	public void BigImage(bool on){
		if (on) {
			uiAnimator.Play ("big_picture_open");
		} else {
			uiAnimator.Play ("big_picture_close");
		}
	}
	

	public void UpdateStats (PlayerStatsStructure pss) {

		// Set Buttons
		MyStatsBtn.interactable = !pss.IsLocalStats;
		FriendStatsBtn.interactable = pss.IsLocalStats;

		if (pss.IsLocalStats) {
			MyStatsBtn.GetComponent<Image> ().color = greyColor;
			myStatsBtnTxt.color = Color.white;
			FriendStatsBtn.GetComponent<Image> ().color = Color.white;
			friendStatsBtnTxt.color = Color.black;
		} else {
			MyStatsBtn.GetComponent<Image> ().color = Color.white;
			myStatsBtnTxt.color = Color.black;
			FriendStatsBtn.GetComponent<Image> ().color = greyColor;
			friendStatsBtnTxt.color = Color.white;
		}


		// Set Player Data
		userImageShare.texture = pss.UserPhoto;
		isUserPictureReady = true;

		foreach (RankingObject rankObj in rankingObjs) {
			rankObj.rankingImage.SetActive (false);
			rankObj.rankingImageShare.SetActive (false);
			if (rankObj.rankName == pss.Rank) {
				rankObj.rankingImage.SetActive (true);
				rankObj.rankingImageShare.SetActive (true);
				//break;
			}
//			else {
//				rankObj.rankingImage.SetActive (false);
//				rankObj.rankingImageShare.SetActive (false);
//			}
		}

		foundationDateText.text = foundationDateTextShare.text = pss.InstallDate;

		teamSize = pss.TeamSize;
		teamSizeText.text = teamSizeTextShare.text = teamSize.ToString ();
		teamMaxSizeText.text = teamMaxSizeTextShare.text = teamMaxSize.ToString ();

		string[] ownedPlayersIds = PlayerChooseManager.instance.pp;

		foreach (string id in ownedPlayersIds) {
			int intId = int.Parse (id);
			ownedPlayers [intId].SetActive (true);
			emptyPlayers [intId].SetActive (false);
			ownedPlayersShare [intId].SetActive (true);
			emptyPlayersShare [intId].SetActive (false);

		}

//		for (int i = 0; i < ownedPlayers.Length; i++) {
//			if (i < ownedPlayersIds.Length) {
//				ownedPlayers [i].SetActive (true);
//				emptyPlayers [i].SetActive (false);
//				ownedPlayersShare [i].SetActive (true);
//				emptyPlayersShare [i].SetActive (false);
//			}
//		}

		totalMetersText.text = totalMetersTextShare.text = pss.TotalMeters;
		totalPractiseText.text = totalPractiseTextShare.text = pss.TotalPracticeTime;

		// cups
		completedCups.text = completedCupsShare.text =pss.CompletedCups.ToString();


		goldMedalsText.text = goldMedalsTextShare.text = pss.TotalGoldMedals;
		silverMedalText.text = silverMedalShare.text = pss.TotalSilverMedals;
		failedValueText.text = failedValueTextShare.text = pss.TotalFailedRuns;

		if (teamSize < 1) {
			favoritePlayerParent.SetActive (false);
			favoritePlayerShareParent.SetActive (false);
		} else {
			favoritePlayerParent.SetActive (true);
			favoritePlayerShareParent.SetActive (true);
			favoritePlayerNumText.text = favoritePlayerNumTextShare.text = pss.FavoritePlayer.number;
			//favoritePlayerImage.sprite = favoritePlayerImageShare.sprite = pss.FavoritePlayer.img;
			favoritePlayerNameText.text = favoritePlayerNameTextShare.text = pss.FavoritePlayer.name;
			totalDistanceText.text = totalDistanceTextShare.text = pss.FavoritePlayer.totalDistance + " m";
			levelText.text = levelTextShare.text = "" + DAO.Language.GetString ("level") + " " + pss.FavoritePlayer.level;

			foreach (SkillsImages skillImg in skillImgs) {
				skillImg.image.SetActive (false);
				skillImg.imageShare.SetActive (false);
				if(skillImg.name == pss.FavoritePlayer.perk1 || skillImg.name == pss.FavoritePlayer.perk2 || skillImg.name == pss.FavoritePlayer.perk3){
					skillImg.image.SetActive (true);
					skillImg.imageShare.SetActive (true);
				}
			}

//			foreach (SkillsImages skillImg in skillImgsShare) {
//				if(skillImg.name == pss.FavoritePlayer.perk1 || skillImg.name == pss.FavoritePlayer.perk2 || skillImg.name == pss.FavoritePlayer.perk3){
//					skillImg.image.SetActive (true);
//				}
//			}

		}
	}

	public void OnFriendForStatsClicked(string id){
		CrossSceneUIHandler.Instance.HideFriendChooseScreen ();
		CrossSceneUIHandler.Instance.ShowPreloader ();
		DDB._.LoadFriendData ( id );
	}
		

	public void Share()
	{
//    #if UNITY_ANDROID
//		if (!UniAndroidPermission.IsPermitted (AndroidPermission.CAMERA))
//			UniAndroidPermission.RequestPremission (AndroidPermission.CAMERA, () => {
//    			// add permit action
//    			print ("Permission true");
//    			ImplementShare();
//			}, () => {
//    			// add not permit action
//				print ("Permission false");
//			});
//		else
//		{
//			ImplementShare ();
//		}
//    #else
//        ImplementShare ();
//    #endif
		ImplementShare ();
		//ShareBtn.transform.FindChild ("line").gameObject.SetActive (true);
		//ShareBtn.GetComponentInChildren<Text> ().color = littleManFillColor;
		//StartCoroutine (DisableShareBtnColor());
	}

//	IEnumerator DisableShareBtnColor(){
//		yield return new WaitForFixedUpdate ();
//		ShareBtn.transform.FindChild ("line").gameObject.SetActive (false);
//		ShareBtn.GetComponentInChildren<Text> ().color = Color.white;
//	}
	public void ImplementShare()
	{
		shareCameraObj.SetActive (true);
		return;
		if (FB.IsLoggedIn) {
			shareCameraObj.SetActive (true);
		} else {
			FBModule.OnFacebookLoggedIn += OnAfterLogInShare;
			FBModule.Instance.FBButonClicked = true;
			FBModule.Instance.Login ();
		}
	}
	public void OnAfterLogInShare(){
		FBModule.OnFacebookLoggedIn -= OnAfterLogInShare;
		shareCameraObj.SetActive (true);
	}

	public void OnAfterLogInFriendStats(){
		FBModule.OnFacebookProfileRecieved -= OnAfterLogInFriendStats;
		CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.CHOOSE_FRIEND);
	}



	public void FriendStatsClicked(){
		if (FB.IsLoggedIn) {
			CrossSceneUIHandler.Instance.ShowFriendChooseScreen (FriendChooseScreen.ScreenStates.CHOOSE_FRIEND);
		} else {
			FBModule.OnFacebookProfileRecieved += OnAfterLogInFriendStats;
			FBModule.Instance.FBButonClicked = true;
			FBModule.Instance.Login ();
		}
	}

	public void MyStatsClicked(){
		PlayerStatsStructure lpss = new PlayerStatsStructure ();
		lpss.SetToLocalPlayer ();
		StatsController.Instance.UpdateStats (lpss);
		isUserPictureReady = true;
	}
}


public class FavoritePlayerStructure{

	public Sprite img;
	public string name;
	public string totalDistance;
	public string level;
	public string number;

	public string perk1;
	public string perk2;
	public string perk3;

	public FavoritePlayerStructure(string[] plData, string playerId){
		PlayerStructure ps = PrefabManager.instanse.PlayerStructures [int.Parse (playerId)];
		img = ps.PlayerPhoto;
		name = ps.PlayerName;
		totalDistance = plData[13];
		level = plData [4];
		number = ps.PlayerNumber.ToString();
		perk1 = plData [7];
		perk2 = plData [9];
		perk3 = plData [11];

//		perk1 = Perk.GetIconByStringType (plData [7]);
//		perk2 = Perk.GetIconByStringType (plData [9]);
//		perk3 = Perk.GetIconByStringType (plData [11]);
	}

}


public class PlayerStatsStructure{

	public bool IsLocalStats;

	public Texture2D UserPhoto;
	public string Rank;				// TBD
	public string InstallDate;
	public int TeamSize;
	public int CompletedCups;
	public string TotalMeters;
	public string TotalPracticeTime;
	public string TotalGoldMedals;
	public string TotalSilverMedals;
	public string TotalFailedRuns;
	public FavoritePlayerStructure FavoritePlayer;

	public void SetToLocalPlayer(){
		//Debug.Log("alon_______setting local data!");
		IsLocalStats = true;
		Rank = GetRankByScore(DAO.RankScore);
		UserPhoto = UserData.Instance.UserImageTexture;
		InstallDate = DAO.InstallDate.ToShortDateString ();
		TeamSize = DAO.NumOfPurchasedPlayers;
		CompletedCups = DAO.NumOfCompletedCups;
		//Debug.Log("alon_______local completed cups: " + CompletedCups);
		TotalMeters = DAO.BestPracticeScore.ToString() + " m";
		TotalPracticeTime =  DAO.TotalPracticeTime + " m";
		TotalGoldMedals = DAO.TotalGoldMedals.ToString ();
		TotalSilverMedals = DAO.TotalSilverMedals.ToString ();
		TotalFailedRuns = DAO.TotalFailedRuns.ToString ();
		FavoritePlayer = FindLocalFavoritePlayer();


	}

	public void SetToFriend(UserDataStructure fd){
		//Debug.Log("alon_______setting data to friend!");
		IsLocalStats = false;
		Rank = GetRankByScore(fd.rankScore);
		InstallDate = fd.foundationDate;
		TeamSize = fd.NumOfPurchasedPlayers;
		CompletedCups = fd.NumOfWinnedCups;
		//Debug.Log("alon_______friend completed cups: " + CompletedCups);
		TotalMeters = "" + fd.highScore + " m";
		TotalPracticeTime = "" + fd.trainingTime + " m";
		TotalGoldMedals = fd.totalGoldMedals.ToString();
		TotalSilverMedals = fd.totalSilverMedals.ToString ();
		TotalFailedRuns = fd.totalFailedRuns.ToString ();
		FavoritePlayer = FindFriendsFavoritePlayer (fd.players, fd.purchasedPlayers);
	}

	// -------

	string GetRankByScore(int score){
		string rank = "D";
	
		if(score > 30) rank = "D+";
		if(score > 100) rank = "C-";
		if(score > 1000) rank = "C";
		if(score > 2000) rank = "C+";
		if(score > 3000) rank = "B-";
		if(score > 4000) rank = "B";
		if(score > 5000) rank = "B+";
		if(score > 6000) rank = "A-";
		if(score > 8000) rank = "A";
		if(score > 10000) rank = "A+";

		return rank;
	}

	public FavoritePlayerStructure FindLocalFavoritePlayer(){
		string[] plData = DAO.GetPlayerData (0).Split (new char[1]{ '|' }, StringSplitOptions.RemoveEmptyEntries);
		string[] plData2;
		string selectedPid = "0";

		foreach (string pid in DAO.PurchasedPlayers.Split (new char[1]{'|'}, StringSplitOptions.RemoveEmptyEntries)) {
			plData2 = DAO.GetPlayerData (int.Parse (pid)).Split (new char[1]{ '|' }, StringSplitOptions.RemoveEmptyEntries);

			if (int.Parse (plData2 [13]) > int.Parse (plData [13])) {
				plData = plData2;
				selectedPid = pid;
			}
		}

		return new FavoritePlayerStructure(plData, selectedPid);
	}

	public FavoritePlayerStructure FindFriendsFavoritePlayer(List<PlayerItem> players, string purchased_players){

		string[] plData = players[0].data.Split (new char[1]{ '|' }, StringSplitOptions.RemoveEmptyEntries);
		string[] plData2;
		string selectedPid = "0";

		foreach (string pid in purchased_players.Split (new char[1]{'|'}, StringSplitOptions.RemoveEmptyEntries)) {
            if (int.Parse(pid) < 0)
                break;
			plData2 = players[ int.Parse(pid) ].data.Split (new char[1]{ '|' }, StringSplitOptions.RemoveEmptyEntries);
           
                if (int.Parse(plData2[13]) > int.Parse(plData[13]))
                {
                    plData = plData2;
                    selectedPid = pid;
                }
		}

		return new FavoritePlayerStructure(plData, selectedPid);
	}
}

[System.Serializable]
public class RankingObject{
	public string rankName;
	public GameObject rankingImage;
	public GameObject rankingImageShare;

	public RankingObject(string rankName, GameObject img){
		this.rankName = rankName;
		rankingImage = img;
	}
}
